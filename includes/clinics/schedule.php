 <?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
 <?php

    $fileId = $arResult['PROPERTIES']['SCHEDULE_FILE']['VALUE'];
    
    
    $equalsCount = 5;
    $checkEquals = true;
    $values = $arResult["PROPERTIES"]["SCHEDULE"]["VALUE"];
    for($i = 1; $i < $equalsCount; $i++) {
        if ($values[$i] != $values[$i - 1]) {
            $checkEquals = false;
            break;
        }
    }
    $arScheduleFiles = array();
    $arScheduleFileProperty = $arResult['PROPERTIES']['SCHEDULE_FILE'];
    if (is_array($arScheduleFileProperty)) {
        foreach($arScheduleFileProperty['VALUE']  as $index => $fileId) {
            $fileDescription = empty($arScheduleFileProperty['DESCRIPTION'][$index]) ? 'Скачать расписание' : $arScheduleFileProperty['DESCRIPTION'][$index];
            $filePath = CFile::GetPath($fileId);
            $arScheduleFiles[] = array(
                'path' => $filePath, 
                'description' => $fileDescription,
            );
        }
    }
    $scheduleText = trim($arResult['PROPERTIES']['SCHEDULE_TEXT']['~VALUE']['TEXT']);
    if (!empty($scheduleText)) {
        $arDayOfWeek = array(
            0 => "Пн - Пт", 
            5 => array("Сб", 'b-clinic_schedule__holiday b-clinic_schedule__sunday', false), 
            6 => array("Вс", 'b-clinic_schedule__holiday', false)
        );
        $checkEquals = false;
        $equalsCount = 0;
    } else {
        $arDayOfWeek = array(
            "Пн","Вт","Ср","Чт","Пт", 
            array("Сб", 'b-clinic_schedule__holiday b-clinic_schedule__sunday', false), 
            array("Вс", 'b-clinic_schedule__holiday', false)
        );

    }
?>
<?php if (!empty($values) || !empty($scheduleText)): ?>
    <div class="b-clinic_schedule__wrap<?php if (!empty($scheduleText)): ?> b-clinic_schedule__more<?php endif; ?>">
        <div class="b-header_h2 align-center">Расписание</div>
        
        <?php if (!empty($scheduleText)): ?>
            <div class="b-clinic_schedule b-clinic_schedule__24">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>Пн-Вс</th>
                        <th><strong>круглосуточно</strong></th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <?php echo $scheduleText; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="b-clinic_schedule__subhead">
                Поликлиника
            </div>
        <?php endif; ?>
        <?php if (is_array($values) && !empty($values)):?>
            <div class="b-clinic_schedule">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <?php foreach ($values as $key => $value):?>
                        <?php
                        if (!isset($arDayOfWeek[$key])) {
                            continue;
                        }
                        $day = $arDayOfWeek[$key];
                        if (is_array($day)) {
                            list($day, $class) = $day;    
                        } else {
                            $class = '';
                        }
                        ?>
                        <tr class="<?php echo $class; ?>">
                            <td><?php echo $day; ?></td>
                            <?php if (($key >= $equalsCount) || !($checkEquals)): ?>
                                <td class="b-clinic_schedule__time"><?php echo $value; ?></td>
                            <?php elseif ($checkEquals && $key == 0): ?>
                                <td class="b-clinic_schedule__time" rowspan="<?php echo $equalsCount; ?>"><?php echo $value; ?></td>
                            <?php endif; ?>
                        </tr>
                    <?endforeach?>
                </table>
            </div>
        <?endif?>
        
        <?php foreach($arScheduleFiles as $arFile): ?>
            <div class="b-clinic_schedule__download-wrap"><a href="<?php echo $arFile['path']; ?>" class="b-clinic_schedule__download"><?php echo $arFile['description']; ?></a></div>
        <?php endforeach; ?>
    </div>

<?php endif; ?>