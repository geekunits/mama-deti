<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	$arSort = [
		'RAND' => 'ASC',
	];
	$arFilter = [
		'IBLOCK_ID' => GALLERY_IBLOCK_ID,
		'PROPERTY_CLINIC.ID' => $arResult['ID'],
	];
	$arNavStartParams = [
		'nTopCount' => 4,
	];
	$arSelect = [
		'ID',
		'NAME',
		'CODE',
		'IBLOCK_ID',
		'IBLOCK_SECTION_ID',
		'PROPERTY_PHOTO',
		'PROPERTY_CLINIC',
	];
	$arItems = array();
	$dbResult = CIBlockElement::GetList($arSort, $arFilter, false, $arNavStartParams, $arSelect);
	while($arItem = $dbResult->GetNext()) {
		$imageId = intval($arItem['PROPERTY_PHOTO_VALUE']);
		$arItem['RESIZED'] = CFile::ResizeImageGet($imageId, array('width' => 151, 'height' => 150), BX_RESIZE_IMAGE_EXACT);
		$arItems[] = $arItem;
	}
?>
<?php if ($arItems): ?>
<a href="<?php echo '/gallery/?clinic=' . $arResult['ID']; ?>" class="b-header_h2 align-center">Фотогалерея</a>
<?php foreach($arItems as $arItem): ?><a href="<?php echo '/gallery/?clinic=' . $arResult['ID']; ?>" class="b-clinic_gallery__item in-bl"><img alt="<?php echo $arItem['NAME']; ?>" title="<?php echo $arItem['NAME']; ?>" src="<?php echo $arItem['RESIZED']['src']; ?>"></a><?php endforeach; ?>
<?endif;?>