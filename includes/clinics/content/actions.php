<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $arrActionsFilter;
$arrActionsFilter = array('PROPERTY_CLINIC' => $arResult['ID']);
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "clinic-actions-new",
    Array(
        "IBLOCK_TYPE"   =>      'contentsite',
        "IBLOCK_ID"     =>      32,
        "NEWS_COUNT"    =>      12,
        'FILTER_NAME'	=>		'arrActionsFilter',
        "SORT_BY1"      =>      'ACTIVE_FROM',
        "SORT_ORDER1"   =>      'DESC',
        "FIELD_CODE"    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
        "PROPERTY_CODE" =>      array('CLINIC', 'DATE_FROM', 'DATE_TO'),
        "SET_TITLE"     =>      'N',
        "SET_STATUS_404" => 'N',
        "INCLUDE_IBLOCK_INTO_CHAIN"     =>      'N',
        "DISPLAY_BOTTOM_PAGER"  =>      'Y',

        "DISPLAY_DATE"  =>      'N',
        "DISPLAY_NAME"  =>      "Y",
        "DISPLAY_PICTURE"       =>      'Y',
        "DISPLAY_PREVIEW_TEXT"  =>      'Y',
        //'CHECK_DATES' => 'Y',
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "mamadeti",
    ),
    $component
);