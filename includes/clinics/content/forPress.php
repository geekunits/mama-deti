<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$pictureWidth = 300;
$pictureHeight = 300;

$section = $arResult['FOR_PRESS'];
$file = CFile::GetFileArray($section->getRawField('UF_FILE'));
$fileName = $section->getRawField('UF_FILE_NAME');

?>
<div class="fopress-column-right">
	<div class="forpress-list">
		<div class="forpress-list__clinic b-header_h1">
			<?php echo $arResult['NAME']; ?>
		</div>
		<div class="forpress-list__desc">
			<div class="forpress-list__desc-txt"><?php echo $section->getRawField('DESCRIPTION'); ?></div>
			<?php if (!empty($file)): ?>
				<div>
					<a href="<?php echo $file['SRC']; ?>" target="_blank" class="forpress-list__download">Скачать заявку</a>
				</div>
			<?php endif; ?>
		</div>
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"for-press",
				Array(
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "contentsite",
					"IBLOCK_ID" => "43",
					"NEWS_COUNT" => "18",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array("ID", 
						"CODE", 
						"XML_ID", 
						"NAME", 
						"TAGS", 
						"SORT", 
						"PREVIEW_TEXT", "PREVIEW_PICTURE", 
						"DETAIL_TEXT", "DETAIL_PICTURE", 
						"DATE_ACTIVE_FROM", 
						"ACTIVE_FROM", "DATE_ACTIVE_TO", 
						"ACTIVE_TO", 
						"SHOW_COUNTER", "SHOW_COUNTER_START", 
						"IBLOCK_TYPE_ID", "IBLOCK_ID", 
						"IBLOCK_CODE", "IBLOCK_NAME", 
						"IBLOCK_EXTERNAL_ID", "DATE_CREATE", 
						"CREATED_BY", "CREATED_USER_NAME", "TIMESTAMP_X"),
					"PROPERTY_CODE" => array("FILE_LINK"),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "Y",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => $section->getRawField('ID'),
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PAGER_TEMPLATE" => "mamadeti",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Новости",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N"
				),
			false); ?>
	</div>
</div>

<?php /*
<div class="fopress-column-right">
	<div class="forpress-list">
		<div class="forpress-list__clinic b-header_h1">
			<?php echo $arResult['NAME']; ?>
		</div>
		<div class="forpress-list__desc">
			<div class="forpress-list__desc-txt"><?php echo $section->getRawField('DESCRIPTION'); ?></div>
			<?php if (!empty($file)): ?>
				<div>
					<a href="<?php echo $file['SRC']; ?>" target="_blank" class="forpress-list__download">Скачать заявку</a>
				</div>
			<?php endif; ?>
		</div>

		<?php if (count($sectionItems) > 0): ?>
			<div class="b-columns_three">
				<?php foreach($sectionItems as $index => $arItem): ?>
					<?php if ($index % $columnCount === 0): ?>
						<div class="forpress-list__item clearfix">
					<?php endif; ?>
					<div class="b-column">
						<div class="forpress-list__pic">
							<?php if (!empty($arItem['PICTURE'])): ?>
								<img 
									src="<?php echo $arItem['PICTURE']['SRC']; ?>" 
									alt="<?php echo $arItem['NAME']; ?>"
									title="<?php echo $arItem['NAME']; ?>">
							<?php endif; ?>
						</div>
						<div class="forpress-list__name">
							<?php echo $arItem['NAME']; ?>
						</div>
						<div class="forpress-list__txt">
							<?php echo $arItem['PREVIEW_TEXT']; ?>
						</div>
						<?php if (!empty($arItem['PROPERTIES']['FILE_LINK']['VALUE'])): ?>
							<div>
								<a href="<?php echo $arItem['PROPERTIES']['FILE_LINK']['VALUE']; ?>" target="_blank" class="forpress-list__download">
									Скачать альбом <?php echo $arItem['PROPERTIES']['FILE_LINK']['DESCRIPTION']; ?>
								</a>
							</div>
						<?php endif; ?>
						<?php if (!empty($arItem['PRESS_RELEASE_FILE'])): ?>
							<div>
								<a href="<?php echo $arItem['PRESS_RELEASE_FILE']['SRC']; ?>" target="_blank" class="forpress-list__download">
									Пресс-релиз
								</a>
							</div>
						<?php endif; ?>
					</div>
					<?php if ($index % $columnCount === $columnCount - 1): ?>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
				<?php if ($index % $columnCount !== $columnCount - 1): ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>