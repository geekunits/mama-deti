<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    $filter = ['PROPERTY_CLINIC.ID' => $arResult['ID']];
    $doctorIds = CMamaDetiAPI::getDoctorsID($filter);

    global $arrArticlesFilter;
    
    if(!empty($doctorIds)){
        $arrArticlesFilter[] = array(
            'LOGIC' => 'OR',
            array('PROPERTY_CLINIC.ID' => $arResult['ID']),
            array('PROPERTY_DOCTOR.ID' => $doctorIds),
        );
    }else{
        $arrArticlesFilter[] = array('PROPERTY_CLINIC.ID' => $arResult['ID']);
    }

    $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "clinic-tab-articles",
        Array(
            "IBLOCK_TYPE"   =>      'contentsite',
            "IBLOCK_ID"     =>      ARTICLE_IBLOCK_ID,
            "NEWS_COUNT"    =>      0,
            'FILTER_NAME'   =>      'arrArticlesFilter',
            "SORT_BY1"      =>      'PROPERTY_CLINIC',
            "SORT_ORDER1"   =>      'DESC',
            "SORT_BY2"      =>      'PROPERTY_DOCTOR',
            "SORT_ORDER2"   =>      'DESC',
            "FIELD_CODE"    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
            "PROPERTY_CODE" =>      array('DOCTOR'),
            "SET_TITLE"     =>      'N',
            "SET_STATUS_404" => 'N',
            "INCLUDE_IBLOCK_INTO_CHAIN" => 'N',
            "DISPLAY_BOTTOM_PAGER"  =>      'Y',
            "DISPLAY_DATE"  =>      'N',
            "DISPLAY_NAME"  =>      "Y",
            "DISPLAY_PICTURE"       =>      'Y',
            "DISPLAY_PREVIEW_TEXT"  =>      'Y',
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "mamadeti",
            "CLINIC_ID" => $arResult["ID"],
        ),
        $component
    );
?>