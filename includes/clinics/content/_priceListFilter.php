<?php

$sectionId = (int) $_REQUEST['filter_section'];
$searchText = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';

$clinicId = $arResult['ID'];

$filterSections = [];

$filter = [
    'IBLOCK_ID' => PRICELIST_IBLOCK_ID,
    'UF_CLINIC' => $clinicId,
];

$subFilter = [
    'IBLOCK_ID' => PRICELIST_IBLOCK_ID,
    'ACTIVE' => 'Y',
    'DEPTH_LEVEL' => 2,
];

$countFilter = [
    'IBLOCK_ID' => PRICELIST_IBLOCK_ID,
    'ACTIVE' => 'Y',
    '!PROPERTY_PRICE' => false,
    'INCLUDE_SUBSECTIONS' => 'Y',
];

$subSelect = ['ID', 'NAME'];

$select = ['ID', 'LEFT_MARGIN', 'RIGHT_MARGIN'];

$result = CIBlockSection::GetList([], $filter, false, $select);

$currentSectionId = null;

while($section = $result->Fetch()) {
    $subFilter['LEFT_MARGIN'] = $section['LEFT_MARGIN'] + 1;
    $subFilter['RIGHT_MARGIN'] = $section['RIGHT_MARGIN'];
    $subResult = CIBlockSection::GetList(['NAME' => 'ASC'], $subFilter, false, $subSelect);
    while($subSection = $subResult->GetNext()) {
        $subSectionId = (int) $subSection['ID'];
        $countFilter['SECTION_ID'] = $subSectionId;
        $count = (int) CIBlockElement::GetList([], $countFilter, []);
        if ($count > 0) {
            $filterSections[$subSectionId] = $subSection['NAME'];
            if ($sectionId === $subSectionId) {
                $currentSectionId = $subSectionId;
            }
        }
    }
}

$action = $APPLICATION->GetCurPage();


global $priceFilter;
$priceFilter = array(
    "!PROPERTY_PRICE" => false,
);

if (!empty($currentSectionId)) {
    $priceFilter['SECTION_ID'] = $currentSectionId;
    $priceFilter['INCLUDE_SUBSECTIONS'] = 'Y';
}

if (!empty($searchText)) {
    $priceFilter['?NAME'] = mysql_real_escape_string($searchText);
}

?>
<form id="price-filter" action="<?php echo $action; ?>" class="search-price-list">
    <div class="clearfix"></div>
    <div class="clinic-price-filter clearfix">
        <div class="clinic-price-filter__select">
            <select name="filter_section">
                <option value="">Все разделы</option>
                <?php foreach($filterSections as $value => $name): ?>
                    <option 
                        value="<?php echo $value; ?>"
                        <?php if ($value === $currentSectionId): ?>
                            selected="selected"
                        <?php endif; ?>
                        >
                        <?php echo $name; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="clinic-price-filter__search">
            <div class="search-input">
                <div class="text03">
                    <input type="text" class="input_search_text" name="q" placeholder="Название услуги" autocomplete="off"
                           value="<?php echo htmlspecialchars($searchText); ?>">
                </div>
            </div>
            <div id="search_result" class="search-drop"
                 style="display: none; margin: 0px 0px 0px -9999px; z-index: auto;">
                <div class="drop-frame">
                    <div class="scrollpane">
                        <ul class="search-listing"></ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clinic-price-filter__submit">
            <div class="button02"><input type="submit">найти</div>
        </div>
    </div>
</form>