<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?php
global $arrProgramFilter;
$arrProgramFilter = array('PROPERTY_CLINIC' => $arResult['ID']);
$currentDirectionId = null;
if (isset($_REQUEST['DIRECTION'])) {
    $currentDirectionId = intval($_REQUEST['DIRECTION']);
}

$arPrograms = ProgramHelper::findByClinicId($arResult['ID']);

$arGroups = ProgramHelper::groupByDirections($arPrograms);
$arAgeGroups = ProgramHelper::groupByAges($arGroups);
$arList = ProgramHelper::getList($arAgeGroups, $currentDirectionId);
$arPagination = ProgramHelper::applyPagination($arList);
$arList = $arPagination['LIST'];
$pagination = $arPagination['PAGINATION'];

$previousGroupId = null;
$previousAgeName = null;
$index = 0;


?>
<div class="b-clinic_program__wrap">
    <div class="jq-selectbox__select__red b-clinic_program__select">
        <select name="direction">
            <option value="-1">Все направления</option>
                <?php foreach($arGroups as $arGroup): ?>
                    <?php
                        $directionId = $arGroup['ID'];
                        $isSelected =  $currentDirectionId && ($currentDirectionId == $directionId);
                    ?>
                <option value="<?php echo $directionId; ?>" <?php echo $isSelected ? 'selected="selected"' : ''; ?>><?php echo $arGroup['NAME']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <?php foreach($arList as $arElement): ?>
        <?php
            $arGroup = $arElement['GROUP'];
            $arItem = $arElement['ITEM'];
            $groupId = $arGroup['ID'];
            $ageName = $arElement['AGE'];
        ?>
            <?php if ($groupId !== $previousGroupId): ?>
                <?php if($index % 2 === 1): ?>
                    </div>
                <?php endif; ?>
                <?php if ($previousGroupId !== null): ?>
                    </div>
                <?php endif; ?>
                <div class="b-clinic_program b-columns_two">
                <div class="b-header_h2"><?php echo $arGroup['NAME']; ?></div>
                <?php if (!empty($ageName)): ?>
                    <div class="b-clinic_program__subhead"><?php echo $ageName; ?></div>
                <?php endif; ?>
                <?php $index = 0; ?>
            <?php elseif (!empty($ageName) && $ageName !== $previousAgeName): ?>
                <?php if ($index % 2 === 1): ?>
                    </div>
                <?php endif; ?>
                <div class="b-clinic_program__subhead"><?php echo $ageName; ?></div>
                <?php $index = 0; ?>
            <?php endif; ?>

            <?php if ($index % 2 === 0): ?>
                <div class="b-clinic_program__item clearfix">
            <?php endif; ?>
                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="b-clinic_program__link b-column" target="_blank">
                    <?php echo $arItem['NAME']; ?>
                    <?
                        if ($arItem["PROPERTY_TYPE_XML_ID"]){
                            $xmlId = $arItem["PROPERTY_TYPE_XML_ID"];

                            if(isset(ProgramHelper::$types[$xmlId])){
                                $type = ProgramHelper::$types[$xmlId];
                                ?>
                                    <span class="programm-label programm-label--<? echo $xmlId; ?> in-bl"><? echo $type['label']; ?>
                                        <span class="programm-label__popup"><? echo $type['description']; ?></span>
                                    </span>
                                <?php
                            }
                        }
                    ?>
                    <?php $price = number_format($arItem['PROPERTY_PRICE_VALUE'], 0, ',', ' '); ?>
                    <?php if (!empty($price)): ?>
                        <span class="b-clinic_program__price"><?php echo $price; ?> р</span>
                    <?php endif; ?>
                </a>
            <?php if ($index % 2 === 1): ?>
                </div>
            <?php endif; ?>

        <?php
            $previousGroupId = $groupId;
            $previousAgeName = $ageName;
            $index++;
        ?>
    <?php endforeach; ?>
    <?php if ($index % 2 === 1): ?>
        </div>
    <?php endif; ?>
    <?php if($index > 0): ?>
        </div>
    <?php endif; ?>
    <?php if ($pagination->NavPageCount > 1): ?>
        <?php
            $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', 'mamadeti', array(
                'NAV_RESULT' => $pagination,
            ));
        ?>
    <?php endif; ?>
</div>
<?/*

$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "clinic-program",
    array(
        "IBLOCK_TYPE"   =>      'contentsite',
        "IBLOCK_ID"     =>      27,
        "NEWS_COUNT" => 20,
        'FILTER_NAME' => 'arrProgramFilter',
        "SORT_BY1" => "PROPERTY_DIRECTION.ID",
        "SORT_ORDER1" => "DESC",
        "FIELD_CODE" => array(
            'ID',
            'NAME',
            'PREVIEW_TEXT',
            'PREVIEW_PICTURE',
            'DETAIL_PICTURE',
            'DETAIL_TEXT',
            'DETAIL_PAGE_URL',
            'ACTIVE_FROM',
            'ACTIVE_TO',
        ),
        "PROPERTY_CODE" => array(
            'CLINIC',
            'DIRECTION',
            'SERVICES',
            'PRICE',
        ),
        "SET_TITLE"     => 'N',
        "SET_STATUS_404" => 'N',
        "INCLUDE_IBLOCK_INTO_CHAIN"     =>      'N',
        "DISPLAY_BOTTOM_PAGER"  =>      'Y',

        "DISPLAY_DATE"  =>      'N',
        "DISPLAY_NAME"  =>      "Y",
        "DISPLAY_PICTURE"       =>      'Y',
        "DISPLAY_PREVIEW_TEXT"  =>      'Y',
        //'CHECK_DATES' => 'Y',
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "mamadeti",
    ),
    $component
);
/*
<?$APPLICATION->IncludeComponent("melcosoft:catalog", "program-new", array(
    "IBLOCK_TYPE" => "contentsite",
    "IBLOCK_ID" => "27",
    "BRAND_IBLOCK_TYPE" => "content",
    "BRAND_IBLOCK_ID" => "",
    "HIDE_NOT_AVAILABLE" => "N",
    "BASKET_URL" => "/personal/basket.php",
    "ACTION_VARIABLE" => "action",
    "PRODUCT_ID_VARIABLE" => "id",
    "SECTION_ID_VARIABLE" => "SECTION_ID",
    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
    "PRODUCT_PROPS_VARIABLE" => "prop",
    "SEF_MODE" => "Y",
    "SEF_FOLDER" => "/program/",
    //"AJAX_MODE" => "N",
    //"AJAX_OPTION_JUMP" => "N",
    //"AJAX_OPTION_STYLE" => "Y",
    //"AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "Y",
    "CACHE_GROUPS" => "Y",
    "SET_TITLE" => "Y",
    "SET_STATUS_404" => "N",
    "USE_ELEMENT_COUNTER" => "Y",
    "USE_FILTER" => "Y",
    "FILTER_NAME" => "arrProgramFilter",
    "FILTER_FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "FILTER_PROPERTY_CODE" => array(
        0 => "",
        1 => "",
    ),
    "FILTER_PRICE_CODE" => array(
    ),
    "USE_REVIEW" => "N",
    "USE_COMPARE" => "N",
    "PRICE_CODE" => array(
    ),
    "USE_PRICE_COUNT" => "N",
    "SHOW_PRICE_COUNT" => "1",
    "PRICE_VAT_INCLUDE" => "Y",
    "PRICE_VAT_SHOW_VALUE" => "N",
    "PRODUCT_PROPERTIES" => array(
    ),
    "USE_PRODUCT_QUANTITY" => "N",
    "CONVERT_CURRENCY" => "N",
    "SHOW_TOP_ELEMENTS" => "N",
    "SECTION_COUNT_ELEMENTS" => "Y",
    "SECTION_TOP_DEPTH" => "2",
    "PAGE_ELEMENT_COUNT" => "30",
    "LINE_ELEMENT_COUNT" => "3",
    "ELEMENT_SORT_FIELD" => "sort",
    "ELEMENT_SORT_ORDER" => "asc",
    "ELEMENT_SORT_FIELD2" => "id",
    "ELEMENT_SORT_ORDER2" => "desc",
    "LIST_PROPERTY_CODE" => array(
        0 => "CLINIC",
        1 => "DIRECTION",
        2 => "",
    ),
    "INCLUDE_SUBSECTIONS" => "Y",
    "LIST_META_KEYWORDS" => "-",
    "LIST_META_DESCRIPTION" => "-",
    "LIST_BROWSER_TITLE" => "-",
    "DETAIL_PROPERTY_CODE" => array(
        0 => "",
        1 => "URL",
        2 => "",
    ),
    "DETAIL_META_KEYWORDS" => "-",
    "DETAIL_META_DESCRIPTION" => "-",
    "DETAIL_BROWSER_TITLE" => "-",
    "LINK_IBLOCK_TYPE" => "",
    "LINK_IBLOCK_ID" => "",
    "LINK_PROPERTY_SID" => "",
    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
    "USE_ALSO_BUY" => "N",
    "USE_STORE" => "N",
    "PAGER_TEMPLATE" => "mamadeti",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Товары",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "SEF_URL_TEMPLATES" => array(
        "sections" => "",
        "brand" => "",
        "filter" => "",
        "section" => "#SECTION_CODE#/",
        "element" => "#ELEMENT_CODE#/",
        "compare" => "",
    )
    ),
    false
); ?>