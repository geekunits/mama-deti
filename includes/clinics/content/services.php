<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
    $previousLevel = 0;
?>

<div class="b-tree_links__wrap">
    <?php for ($index = 0, $arItems = $arResult['SERVICES_MENU'], $count = count($arResult['SERVICES_MENU']); $index < $count; $index++): ?>
        <?php
            $arItem = $arItems[$index];
            $arNextItem = $index + 1 < $count ? $arItems[$index + 1] : null;
            $hasSubItems = $arNextItem && $arNextItem['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'];
        ?>
        <?php for ($depth = $previousLevel; $depth > $arItem['DEPTH_LEVEL']; $depth--): ?>
            </div></div>
        <?php endfor; ?>
        <div class="b-tree_link__item">
            <div class="b-tree_link">
                <?php if ($hasSubItems): ?>
                    <div class="b-tree_link__plus js-tree-click">+</div>
                <?php endif; ?>
                <a href="<?php echo $arItem['LINK'].'clinic/'.$arResult['CODE'].'/'; ?>"><?php echo $arItem['TEXT']; ?></a>
            </div>
            <?php if ($hasSubItems): ?>
                <div class="b-tree_links__sub b-tree_links__sub-<?php echo $arItem['DEPTH_LEVEL']; ?>">
            <?php else: ?>
                </div>
            <?php endif; ?>
        <?php $previousLevel = $arItem['DEPTH_LEVEL']; ?>
    <?php endfor; ?>
    <?php while($previousLevel-- > 0): ?>
        </div>
    <?php endwhile; ?>
</div>
