<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?php
	$arVideos = array_reverse($arResult['PROPERTIES']['VIDEO']['VALUE']);
	$arDescriptions = array_reverse($arResult['PROPERTIES']['VIDEO']['DESCRIPTION']);
?>
<?php if (!empty($arVideos)): ?>
	<div class="videos">
		<div class="videos__list">
			<?php foreach($arVideos as $index => $link):?>
				<div class="videos__item">
					<iframe src="<?php echo $link; ?>" width="100%" height="" frameborder="0" scrolling="no" allowfullscreen></iframe>
					<?php if (!empty($arDescriptions[$index])): ?>
						<div class="videos__title"><?php echo $arDescriptions[$index]; ?></div>
					<?php endif; ?>
				</div>
				<?php if (($index+1) % 3 == 0): ?><div class="clearfix"></div><?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>