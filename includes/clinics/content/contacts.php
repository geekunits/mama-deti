<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arCoord = explode(',',$arResult["PROPERTIES"]["COORD"]["~VALUE"]);
$jsAddress = json_encode($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"]);
?>
<?if (count($arCoord) == 2):?>
    <div class="right-block fl-r">
        <div id="clinic_map" style="width:300px;height:300px;"></div>
        <script>

            var clinicMap;

            ymaps.ready(clinic_map_init);

            function clinic_map_init()
            {
                clinicMap = new ymaps.Map('clinic_map', {
                    center: [<?=$arCoord[0]?>, <?=$arCoord[1]?>], // Москва
                    zoom: 16,
                    controls: ['zoomControl']
                });

                var firstButton = new ymaps.control.Button({data: {content: "Печать"}, options: {selectOnClick: false}});

                firstButton.events.add(
                    'press',
                    function () {
                        $.fn.clinicMapPrint(<?php echo $jsAddress; ?>,'<?=$arCoord[1].','.$arCoord[0]?>');
                    }
                );

                clinicMap.controls.add(firstButton, {float: 'right'});

                myPlacemark = new ymaps.Placemark(clinicMap.getCenter(), {
                    hintContent: '<?=$arResult["NAME"]?>'
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: '<?=SITE_TEMPLATE_PATH.'/images/hospital_ya_map.png'?>',
                    // Размеры метки.
                    iconImageSize: [87, 53],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    //iconImageOffset: [-3, -42]
                    iconImageOffset: [-43, -26]
                });

                clinicMap.geoObjects.add(myPlacemark);

                CustomControlClass = function (options) {
                    CustomControlClass.superclass.constructor.call(this, options);
                    this._$content = null;
                    this._geocoderDeferred = null;
                };

                ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
                    onAddToMap: function (map) {
                        CustomControlClass.superclass.onAddToMap.call(this, map);
                        this._lastCenter = null;
                       // this.getParent().getChildElement(this).then(this._onGetChildElement, this);
                    },

                    onRemoveFromMap: function (oldMap) {
                        this._lastCenter = null;
                        if (this._$content) {
                            this._$content.remove();
                            this._mapEventGroup.removeAll();
                        }
                        CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
                    },

                    _onGetChildElement: function (parentDomContainer) {
                        // Создаем HTML-элемент с текстом.
                        this._$content = $('<div class="customMapControl">' + <?php echo $jsAddress; ?> + '</div>').appendTo(parentDomContainer);
                        this._mapEventGroup = this.getMap().events.group();
                        // Запрашиваем данные после изменения положения карты.
                        //this._mapEventGroup.add('boundschange', this._createRequest, this);
                        // Сразу же запрашиваем название места.
                        //this._createRequest();
                    }
                });

                var customControl = new CustomControlClass();
                clinicMap.controls.add(customControl, {
                    float: 'none',
                    position: {
                        top: 10,
                        left: 10
                    }
                });
            };
        </script>
    </div>
<?endif;?>
<?if ($arResult["PROPERTIES"]["CONTACTS"]["VALUE"]["TYPE"] == "text"):?>
    <?=$arResult["PROPERTIES"]["CONTACTS"]["VALUE"]["TEXT"]?>
<?else:?>
    <?=$arResult["PROPERTIES"]["CONTACTS"]["~VALUE"]["TEXT"]?>
<?endif?>