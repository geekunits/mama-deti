<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<?if ($arResult["PROPERTIES"]["SCHEDULE_TEXT"]["VALUE"]["TYPE"] == "html"):?>
    <?=$arResult["PROPERTIES"]["SCHEDULE_TEXT"]["~VALUE"]["TEXT"]?>
<?else:?>
    <?=$arResult["PROPERTIES"]["SCHEDULE_TEXT"]["VALUE"]["TEXT"]?>
<?endif?>
<div class="tab-t-title">Прием врачей в клинике осуществляется по предварительной записи</div>
<br>
<?if (is_array($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"])):?>
    <table class="web-table schedule-table" border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <th>День недели</th>
            <th>Время работы</th>
        </tr>
        <?$arDayOfWeek = array("Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье");
        foreach ($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"] as $key=>$value):?>
            <tr>
                <td><?=$arDayOfWeek[$key]?></td>
                <td><?=$value?></td>
            </tr>
        <?endforeach?>
    </table>
<?endif?>