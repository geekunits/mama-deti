<?php $APPLICATION->IncludeFile('/includes/clinics/schedule.php', compact('arResult')); ?>
<?if ($arResult["PREVIEW_TEXT"]): ?>
<div class="b-clinic_card__text">
    <div class="js-clinic-text-cut">
        <?php echo $arResult["PREVIEW_TEXT"]; ?>
        <?= $arResult["DETAIL_TEXT"]; ?>
    </div>
    <div class="js-clinic-text-cut-more align-center b-clinic_card__text-more">Читать дальше</div>
    <div class="js-clinic-text-cut-less align-center b-clinic_card__text-more">Скрыть полное описание</div>
</div>
<?endif;?>
<div class="clearfix"></div>
<div class="b-clinic_address__wrap b-clinic_address__right">
    <div class="b-clinic_address__padding">
        <?php $APPLICATION->IncludeFile('/includes/clinics/address.php', array(
            'arResult' => $arResult,
            'usePhoneColumns' => true,
        )); ?>
        <?php $APPLICATION->IncludeFile('/includes/clinics/map.php', compact('arResult')); ?>
    </div>
</div>