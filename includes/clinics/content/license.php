<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="b-clinic_license__wrap b-slider_content b-slider__arrow__center">
    <ul class="js-slider-carousel b-slider_no" data-countrow="3">
        <?php foreach($arResult["PROPERTIES"]["LICENSE"]["VALUE"] as $key => $photo):?>
            <li class="b-clinic_license__item">
                <a href="<?=CFile::GetPath($photo); ?>" rel="group1" class="fancybox">
                    <img alt="" src="<?=MakeImage($photo, array("z"=>1,"w"=>260,"h"=>230)); ?>">
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>