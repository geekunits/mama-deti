<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="align-center b-header_h2">Новости</div>
<div data-id="1" class="b-news js-news">
	<?php
		global $arrNewsFilter;
		$arrNewsFilter['PROPERTY_CLINIC'] = $arResult['ID'];
		$APPLICATION->IncludeComponent(
		    "bitrix:news.list",
		    "clinic-card",
		    Array(
		        "IBLOCK_TYPE"   =>      'contentsite',
		        "IBLOCK_ID"     =>      8,
		        "NEWS_COUNT"    =>      3,
		        'FILTER_NAME'	=>		'arrNewsFilter',
		        "SORT_BY1"      =>      'ACTIVE_FROM',
		        "SORT_ORDER1"   =>      'DESC',
		        "FIELD_CODE"    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
		        "PROPERTY_CODE" =>      array('CLINIC', 'DATE_FROM', 'DATE_TO'),
		        "SET_TITLE"     =>      'N',
		        "SET_STATUS_404" => 'N',
		        "INCLUDE_IBLOCK_INTO_CHAIN" => 'N',
		        "DISPLAY_BOTTOM_PAGER"  =>      'N',
		        "DISPLAY_DATE"  =>      'N',
		        "DISPLAY_NAME"  =>      "Y",
		        "DISPLAY_PICTURE"       =>      'Y',
		        "DISPLAY_PREVIEW_TEXT"  =>      'Y',
		        "PAGER_SHOW_ALWAYS" => "N",
		        "PAGER_TEMPLATE" => "mamadeti",
                   "CLINIC_URL" => $arResult['DETAIL_PAGE_URL'] . 'news/',
		    ),
		    $component
		);
	?>
</div>