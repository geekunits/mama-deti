<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
$arCoord = explode(',',$arResult["PROPERTIES"]["COORD"]["~VALUE"]);
$clinicUrl = $arResult['DETAIL_PAGE_URL'];
$address = ($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TYPE"] == "text") ? 
    $arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"] : $arResult["PROPERTIES"]["CONTACTS"]["~VALUE"]["TEXT"];
$clinicName = $arResult['NAME'];
$balloonContent = '<a href="' . $clinicUrl . '">' . $clinicName . '</a>';
if (!empty($phone)) {
    $balloonContent .= '<br><span>' . $phone . '</span>';
}
if (!empty($address)) {
    $balloonContent .= '<br>' . $address;
}
?>
<?php if (count($arCoord) == 2):?>
    <?php 
        list($lat, $lon) = $arCoord;
    ?>
    <script type="text/javascript">
        ymaps.ready(function () {
            var map;

            map = new ymaps.Map ("clinic-map-card", {
                center: [<?php echo $lat; ?>, <?php echo $lon; ?>],
                zoom: 13,
                controls: ["smallMapDefaultSet"]
            });

            map.geoObjects.add(new ymaps.Placemark([<?php echo $lat; ?>, <?php echo $lon; ?>], {
                content: <?php echo json_encode($clinicName); ?>,
                balloonContent: <?php echo json_encode($balloonContent); ?>                 
            }));

        });
    </script>
    <div class="b-clinic_address__map" id="clinic-map-card"></div>
<?php endif; ?>