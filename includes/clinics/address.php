<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
$address = ($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TYPE"] == "text") ?
    $arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"] : $arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["~TEXT"];
$phones = is_array($arResult["PROPERTIES"]["PHONE"]["VALUE"]) ? $arResult["PROPERTIES"]["PHONE"]["VALUE"] : array($arResult["PROPERTIES"]["PHONE"]["VALUE"]);
$phoneDescriptions = is_array($arResult["PROPERTIES"]["PHONE"]["DESCRIPTION"]) ? $arResult["PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($arResult["PROPERTIES"]["PHONE"]["DESCRIPTION"]);
$email = trim($arResult["PROPERTIES"]["CONTACT_EMAIL"]["VALUE"]);
$clinicId = $arResult["ID"];
$clinicName = $arResult['NAME'];
$usePhoneColumns = isset($usePhoneColumns) ? $usePhoneColumns : false;
$usePhoneColumns = $usePhoneColumns && count($phones) > 1;
?>
<div class="b-header_h2 align-center">Контакты и запись на прием</div>
<?php if (!empty($address)): ?>
    <div class="b-clinic_address__txt">
        <strong>Адрес:</strong>
        <br>
        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress"><?php echo $address; ?></span></span>
    </div>
<?php endif; ?>
<?php if (!empty($phones)): ?>
    <div class="b-clinic_address__txt">
        <strong>Телефон:</strong>
        <div>
            <?php foreach($phones as $index => $phone): ?>
                <?php
                $isEqual = PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phone);
                if($isEqual){
                    $phone = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                }
                ?>
                <div>
                    <span class="js-phone" itemprop="telephone" data-clinic-id="<?=$arResult['ID'];?>"><?php echo $phone; ?></span>
                    <?php echo empty($phoneDescriptions[$index]) ? '' : '<span class="b-clinic_address__phone-note">'.$phoneDescriptions[$index].'</span>'; ?>
                    <?php if ($isEqual): ?>
                        <?php if(CLIENT_FROM_ABROAD): ?>
                            <span class="phone-free">звонок из-за рубежа</span>
                        <?php else: ?>
                            <span class="phone-free">звонок бесплатный</span>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if (!$arResult['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']): ?>
            <span data-clinic-id="<?php echo $clinicId; ?>" class="b-clinic_address__txt-call js-show-call-me js-goal" data-target="<?php echo $arResult['PROPERTIES']['METRIC_ID']['VALUE']; ?>">Перезвоните мне</span>
        <?php endif; ?>
    </div>
<?php endif; ?>
<?php if (!empty($email)): ?>
    <div class="b-clinic_address__txt">
        <strong>E-mail:</strong>
        <br>
        <a href="mailto:<?php echo $email; ?>" class="b-clinic_address__mail" itemprop="email"><?php echo $email; ?></a>
    </div>
<?php endif; ?>

<?php if (!$arResult['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']): ?>
    <div class="b-btn_white in-bl link_reception_clinic js-goal" data-clinic="<?php echo $clinicId; ?>" data-name="<?php echo $clinicName; ?>" data-target="Appointment_<?php echo $arResult['PROPERTIES']['METRIC_ID']['VALUE']; ?>">
        Записаться на приём
    </div>
    <div class="b-btn_white in-bl link_review js-goal" data-clinic="<?php echo $clinicId; ?>" data-type="clinic" data-target="REVIEW_<?php echo $arResult['PROPERTIES']['METRIC_ID']['VALUE']; ?>">Оставить отзыв</div>
<?php endif; ?>
