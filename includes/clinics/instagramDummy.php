<div class="instagram-widjet">
	<div class="instagram-widjet__head">
		Мы в инстаграм
	</div>
	<div class="instagram-widjet__about">
		<a href="<?php echo $instagramUrl; ?>" target="_blank" class="instagram-widjet__about-logo"></a>
		<a href="<?php echo $instagramUrl; ?>" class="instagram-widjet__about-link" target="_blank">Перейти в Instagram</a>
	</div>
</div>
