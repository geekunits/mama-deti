<?php
	$maxCount = 8;
	$counter = 0;
?>
<div class="instagram-widjet">
	<div class="instagram-widjet__head">
		Мы в инстаграм
	</div>
	<div class="instagram-widjet__about">
		<a href="http://instagram.com/<?php echo $instagramUser->username; ?>" target="_blank" class="instagram-widjet__about-logo">
			<img src="<?php echo $instagramUser->profile_picture; ?>" />
		</a>
		<a href="http://instagram.com/<?php echo $instagramUser->username; ?>" class="instagram-widjet__about-link" target="_blank">Перейти в Instagram</a>
	</div>
	<?php if (!empty($instagramImages)): ?>
		<div class="instagram-widjet__photos clearfix">
			<?php foreach($instagramImages as $image): ?>
				<a href="<?php echo $image->link; ?>" target="_blank" class="instagram-widjet__photos-item"><img src="<?php echo $image->images->thumbnail->url; ?>" alt="" /></a>
				<?php 
					$counter++;
					if ($counter >= $maxCount) {
						break;
					} 
				?>
			<?php endforeach; ?>
		</div>
	<?php else: ?>
		ПУСТОТА
	<?php endif; ?>
</div>
