<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php $showContent = false; ?>
<?php if (!empty($arServices)): ?>
    <div class="b-clinic_social__padding">
        <div class="align-center b-header_h2">Присоединяйтесь к нам</div>
        <div class="b-clinic_social__tabs">
            <?php foreach($arServices as $i => $arService): ?>
                <?php
                    $i++;
                    list($name, $html, $url) = $arService;
                    if (!empty($html)) {
                        $showContent = true;
                    }
                ?>
                <div class="b-clinic_social__tab b-clinic_social__tab<?php echo $i == 1 ? '-active' : ''; ?> in-bl" data-id="<?php echo $i; ?>" data-url="<?php echo htmlspecialchars($url); ?>">
                    <?= $name; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if ($showContent): ?>
            <div class="b-clinic_social__items">
                <?php foreach($arServices as $i => $arService): ?>
                    <?php
                        $i++;
                        list($name, $html) = $arService;
                    ?>
                    <div class="b-clinic_social__item b-clinic_social__item<?php echo $i == 1 ? '-active' : ''; ?>" data-id="<?php echo $i; ?>">
                        <?php echo $html; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
