<?php
$socialServices = [];

if (!empty($clinicProperties)) {
    $socialNames = [
        'FB' => 'Facebook',
        'VK' => 'Vkontakte',
        'OK' => 'Одноклассники',
        'INST' => 'Instagram',
        'YOUTUBE' => 'Youtube',
        'TW' => 'Twitter',
    ];

    foreach($socialNames as $socialKey => $socialName) {
        if (!empty($clinicProperties[$socialKey . '_URL']['VALUE'])) {
            $socialServices[strtolower($socialKey)] = [
                'url' => $clinic['PROPERTIES'][$socialKey . '_URL']['VALUE'],
                'name' => $socialName,
            ];
        }
    }
}
?>
<?php if (count($socialServices)): ?>
    <div class="header__social">
        <?php foreach($socialServices as $socialCode => $socialService): ?>
            <a
                target="_blank"
                href="<?php echo $socialService['url']; ?>"
                rel="nofollow"
                class="in-bl social-link social-link--<?php echo $socialCode; ?>"></a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>