<div class="header__phone-current in-bl">
    <div class="header__phone-current__phone">
        <span class="js-clinic-phone" data-clinic-id="<?=$clinicPhone['clinic']['ID'];?>"><?=PhoneFormatter::prepare($clinicPhone['phone']); ?></span>
        <?php if (PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $clinicPhone['phone'])): ?>
            <?php if(CLIENT_FROM_ABROAD): ?>
                <div class="phone-free">звонок из-за рубежа</div>
            <?php else: ?>
                <div class="phone-free">звонок бесплатный</div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="header__phone-current__clinic">
        <?php if (!empty($clinic)): ?>
            <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>">
                <?php echo $clinicPhone['description']; ?>
            </a>
        <?php else: ?>
            <?php echo $clinicPhone['description']; ?>
        <?php endif; ?>
        <?php if (!empty($clinic) && $clinic['CODE'] === 'clinical-hospital-lapino'): ?>
            <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>contacts/" class="lapino-mkad"><span class="lapino-mkad__time"><span>15</span>мин</span> от МКАД</a>
        <?php endif; ?>
    </div>
</div>