<div class="header__phone-select">
    <div class="header__phone-select__label in-bl">Другие клиники</div>
    <div class="header__phone-popup">
        <div class="header__phone-column in-bl">
            <?php foreach($cityPhones as $cityPhone): ?>
                <div class="header__phone-item">
                    <div class="header__phone-num header__phone-bg">
                        <?php if (PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $cityPhone['phone'])): ?>
                            <?php if(CLIENT_FROM_ABROAD): ?>
                                <div class="phone-free">звонок из-за рубежа</div>
                            <?php else: ?>
                                <div class="phone-free">звонок бесплатный</div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <span class="js-clinic-phone" data-clinic-id="<?=$cityPhone['clinic']['ID'];?>"><?=PhoneFormatter::prepare($cityPhone['phone']); ?></span>
                    </div>
                    <?php if (!empty($cityPhone['clinic']) && $cityPhone['clinic']['ACTIVE'] === 'Y'): ?>
                        <a href="<?php echo $cityPhone['clinic']['DETAIL_PAGE_URL']; ?>" class="header__phone-bg">
                            <?php echo $cityPhone['description']; ?>
                        </a>
                    <?php else: ?>
                        <span class="header__phone-bg">
                            <?php echo $cityPhone['description']; ?>
                        </span>
                    <?php endif; ?>
                </div>
                <?php $index++; ?>
                <?php if ($index === $phonesPerColumn): ?>
                    </div><div class="header__phone-column in-bl">
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>