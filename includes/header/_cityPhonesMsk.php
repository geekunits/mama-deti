<?php
//Клиники с номером, совпадающим с 8-800... (константа HOTLINE_PHONE_NUMBER)
$clinicPhonesWithoutPhones = array_filter($cityPhones, function($cityPhone){
    return PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $cityPhone['phone']);
});

//Клиники с номером, отличающимся от 8-800... (константа HOTLINE_PHONE_NUMBER)
$clinicPhonesWithPhones = array_filter($cityPhones, function($cityPhone){
    return !PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $cityPhone['phone']);
});

$phone = GLOBAL_PHONE_NUMBER;
?>
<div class="header__hotline">
    <div class="hotline">
        <div class="hotline__note"><?=(CLIENT_FROM_ABROAD ? 'ЗВОНКИ ИЗ-ЗА РУБЕЖА' : 'ЕДИНЫЙ НОМЕР БЕСПЛАТНО по РОССИИ');?></div>
        <div class="hotline__phone">
            <a href="tel:<?=PhoneFormatter::getOnlyPhoneDigits($phone);?>" class="comagic-phone comagic_phone_1"><?=PhoneFormatter::formatPhoneForDisplay($phone);?></a>
        </div>
    </div>
</div>
<div class="header__phone-nophone clearfix">
    <div class="header__phone-column in-bl">
        <?php foreach($clinicPhonesWithoutPhones as $cityPhone): ?>
            <div class="header__phone-item">
                <?php if (!empty($cityPhone['clinic']) && $cityPhone['clinic']['ACTIVE'] === 'Y'): ?>
                    <a href="<?php echo $cityPhone['clinic']['DETAIL_PAGE_URL']; ?>" class="header__phone-bg">
                        <?php echo $cityPhone['description']; ?>
                    </a>
                <?php else: ?>
                    <span class="header__phone-bg">
                        <?php echo $cityPhone['description']; ?>
                    </span>
                <?php endif; ?>
            </div>
            <?php $index++; ?>
            <?php if ($index === $phonesPerColumn): ?>
                </div><div class="header__phone-column in-bl">
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
<div class="header__phone-others">
    <?php $index = 0; foreach($clinicPhonesWithPhones as $cityPhone): ?>
        <div class="header__phone-item <?php if ($index == 1):?> header__phone-others-last<?php endif;?> ">
            <?php if (!empty($cityPhone['clinic']) && $cityPhone['clinic']['ACTIVE'] === 'Y'): ?>
                <a href="<?php echo $cityPhone['clinic']['DETAIL_PAGE_URL']; ?>" class="header__phone-bg">
                    <?php echo $cityPhone['description']; ?>
                </a>
            <?php else: ?>
                <span class="header__phone-bg">
                    <?php echo $cityPhone['description']; ?>
                </span>
            <?php endif; ?>
            <div class="header__phone-num header__phone-bg">
                <span class="js-clinic-phone" data-clinic-id="<?=$cityPhone['clinic']['ID'];?>"><?php echo $cityPhone['phone']; ?></span>
            </div>
        </div>
        <?php $index++; ?>
    <?php endforeach; ?>
</div>
