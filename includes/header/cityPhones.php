<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?php 
$clinicPhoneIndex = null;

$cm = CityManager::getInstance();
$city = $cm->getCurrentCity();

$clinicProperties = null;

if (!empty($clinic)) {
	foreach($cityPhones as $index => $cityPhone) {
		if (empty($cityPhone['clinic'])) {
			continue;
		}
		if ($cityPhone['clinic']['ID'] === $clinic['ID']) {
			$clinicPhoneIndex = $index;
			break;
		}
	}

	$clinicProperties = $clinic['PROPERTIES'];

} elseif (count($cityPhones) === 1) {
	$clinicPhoneIndex = 0;
}

$clinicPhone = null;

if (!is_null($clinicPhoneIndex)) {
	$clinicPhone = $cityPhones[$clinicPhoneIndex];
	unset($cityPhones[$clinicPhoneIndex]);
	$cityPhones = array_values($cityPhones);
	// array_splice($cityPhones, $cityPhoneIndex, 1);
	$clinic = $clinicPhone['clinic'];
	if (!empty($clinic)) {
 		$clinic['PROPERTIES'] = [];
 		$dbProperties = CIBlockElement::GetProperty(CLINIC_IBLOCK_ID, $clinic['ID']);
 		while($clinicProperty = $dbProperties->GetNext()) {
 			$clinic['PROPERTIES'][$clinicProperty['CODE']] = $clinicProperty;
 		}
 	}

 	$clinicProperties = $clinic['PROPERTIES'];
}

$index = 0;
$phonesPerColumn = (count($cityPhones) > 6) ? (($city['CODE'] == 'moskva') ? 4 : 6) : 0;

$headerPhoneClass = [];

if ($phonesPerColumn === 0){
    $headerPhoneClass[] = 'header__phone--onecol';
}

if (!empty($clinicPhone)){
    $headerPhoneClass[] = 'header__phone--clinic';
}else{
    switch($city['CODE']){
        case 'novosibirsk':
            $headerPhoneClass[] = 'header__phone--wide';
            break;
        case 'moskva':
            $headerPhoneClass[] = 'header__phone--msk';
            break;
    }
}

$headerPhoneClass = implode(' ', $headerPhoneClass);

$currentCityCode = CityManager::getInstance()->getCurrentCity()['CODE'];
?>

<div class="header__phone <?php echo $headerPhoneClass; ?>">
	<?php if (!empty($clinicPhone)): ?>
        <?php include(__DIR__ . '/_cityPhoneMain.php');?>
        <?php include(__DIR__ . '/_socials.php');?>
	<?php endif; ?>
	<?php if (count($cityPhones) > 0): ?>
        <?php if(!empty($clinicPhone)): ?>
            <?php include(__DIR__ . '/_cityPhonesInClinic.php');?>
        <?php else: ?>
            <?php
                switch($currentCityCode){
                    case 'moskva':
                        include(__DIR__ . '/_cityPhonesMsk.php');
                        break;
                    default:
                        include(__DIR__ . '/_cityPhonesAll.php');
                        break;
                }
            ?>
        <?php endif;?>
	<?php endif; ?>
    <?php if (!$clinic['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']): ?>
        <div class="header__buttons">
            <a href="#" class="b-btn_white in-bl js-show-call-me js-goal" <?php if (!empty($clinic)): ?>data-clinic-id="<?php echo $clinic['ID']; ?>"<?php endif; ?> data-target="CallBack">Перезвоните мне</a><a href="#" class="b-btn_white in-bl js-show-appointment js-goal" data-target="Appointment" <?php if (!empty($clinic)): ?>data-clinic="<?php echo $clinic['ID']; ?>"<?php endif; ?>>Запись на прием</a>
        </div>
    <?php endif; ?>
</div>