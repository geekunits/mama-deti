<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?php 

$cm = CityManager::getInstance();
$cityId = $cm->getCurrentCityId();
$cityPhones = fetchCityPhones($cityId);

?>
<?php if (!empty($cityPhones)): ?>
	<?php foreach($cityPhones as $cityPhone): ?>
		<div class="footer__phone-item">
			<div class="footer__phone-num footer__phone-bg"><?=PhoneFormatter::prepare($cityPhone['phone']); ?></div>
			<?php if (!empty($cityPhone['clinic'])): ?>
				<a class="footer__phone-bg" href="<?php echo $cityPhone['clinic']['DETAIL_PAGE_URL']; ?>">
					<?php echo $cityPhone['description']; ?>
				</a>
			<?php else: ?>
				<span class="footer__phone-bg">
					<?php echo $cityPhone['description']; ?>
				</span>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
<?php endif; ?>