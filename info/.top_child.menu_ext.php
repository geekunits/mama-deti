<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $APPLICATION;
global $menuSectionFilter;
global $menuElementFilter;

$menuSectionFilter = ['ID' => 186];
$menuElementFilter = ['SECTION_ID' => 186];

$aMenuLinksExt = $APPLICATION->IncludeComponent("melcosoft:menu.sections", "", array(
    "IS_SEF" => "Y",
    "SEF_BASE_URL" => "/info/",
    "SECTION_PAGE_URL" => "#SECTION_CODE#/",
    "DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#/",
    "IBLOCK_TYPE" => "contentsite",
    "IBLOCK_ID" => "24",
    "DEPTH_LEVEL" => "4",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "ELEMENT_FILTER_NAME" => 'menuElementFilter',
    "SECTION_FILTER_NAME" => 'menuSectionFilter',
    ),
    false
);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
