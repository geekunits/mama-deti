<?php
$GLOBALS["arrFilterClinics"] = array("PROPERTY_CITY.ID" => $GLOBALS["CURRENT_CITY"]["ID"]);

$arRegionClinic = CMamaDetiAPI::getClinicsID(array("PROPERTY_CITY" => $GLOBALS["CURRENT_CITY"]["ID"]));
$arRegionDoctor = CMamaDetiAPI::getDoctorsID(array("PROPERTY_CLINIC" => $arRegionClinic));
$arRegionService = CMamaDetiAPI::getClinicsServicesID(array("PROPERTY_CITY" => $GLOBALS["CURRENT_CITY"]["ID"]));

$GLOBALS["arrFilterDoctors"] = array("!PROPERTY_SHOW_HOME" => false,"PROPERTY_CLINIC" => $arRegionClinic);

$GLOBALS["arrFilterReview"] = array(
    array(
        "LOGIC" => "OR",
        array("PROPERTY_DOCTOR" => $arRegionDoctor),
        array("PROPERTY_CLINIC" => $arRegionClinic),
    ), );

$GLOBALS["arrFilterStory"] = array(
    array('LOGIC' => 'AND',array("PROPERTY_DOCTOR" => array_merge(array(false), $arRegionDoctor)),array('!=PROPERTY_DOCTOR' => false)),
);

$GLOBALS["arFilterQa"] = array(
    "!PREVIEW_TEXT" => false,
    array(
        "LOGIC" => "OR",
        array("PROPERTY_DOCTOR" => $arRegionDoctor),
        array("PROPERTY_CLINIC" => $arRegionClinic),
    ), );

$GLOBALS["arrFilterNews"] = array(
    array("LOGIC" => "OR",
        array("PROPERTY_CLINIC" => $arRegionClinic),
        array("PROPERTY_CLINIC" => false,"PROPERTY_SERVICES" => $arRegionService),
        array("PROPERTY_GLOBAL" => 1),
    ),
);
$GLOBALS["arrFilterEventNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 4,
));
$GLOBALS["arrFilterEduNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 5,
));
$GLOBALS["arrFilterGallery"] = array(
    array(
        "LOGIC" => "OR",
        array("PROPERTY_DOCTORS" => $arRegionDoctor),
        array("PROPERTY_CLINIC" => $arRegionClinic),
    ), );

?>
<?php
/*$arDocClinics = $APPLICATION->GetProperty('CURRENT_DOCTOR', 0) ?
    array_keys(CMamaDetiAPI::getDoctorsClinics(array('ID' => $APPLICATION->GetProperty('CURRENT_DOCTOR', 0)))) :
    array();

$GLOBALS["arrFilterNews"] = array(
    array("LOGIC" => "OR",
        array("PROPERTY_CLINIC" => $arRegionClinic),
        array("PROPERTY_CLINIC" => false,"PROPERTY_SERVICES" => $arRegionService),
        array("PROPERTY_GLOBAL" => 1),
    ),
);
if ($arDocClinics) {
    $GLOBALS['arrFilterNews'][0][] = array("PROPERTY_CLINIC" => $arDocClinics);
}
$GLOBALS["arrFilterEventNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 4,
));
$GLOBALS["arrFilterEduNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 5,
));*/

if (!function_exists('__ShowClinicFooterInfo')) {
    function __ShowClinicFooterInfo()
    {
        global $APPLICATION;

        $doctor = intval($APPLICATION->GetProperty('CURRENT_DOCTOR', 0));

        if ($doctor <= 0) {
            return '';
        }

        $GLOBALS["arrFilterDoctor"] = array("PROPERTY_DOCTOR" => $doctor);

        ob_start();
        ?>
<div class="b-widgets_footer__wrap">
    <div class="b-widgets_footer clearfix">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-review", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "11",
            "NEWS_COUNT" => "4",
            "SORT_BY1" => "RAND",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterReview",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "CLINIC",
                1 => "DOCTOR",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "80",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>

        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-story", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "12",
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "RAND",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterStory",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "CLINIC",
                1 => "DOCTOR",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "80",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>

        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-qa", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "10",
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "RAND",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arFilterQa",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "80",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
    </div>
</div>

<?php
    $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}

$APPLICATION->AddBufferContent('__ShowClinicFooterInfo');

?>
