<?if ($APPLICATION->GetCurDir() == "/"):?>
<?php
$GLOBALS["arrFilterSlider"] = array("PROPERTY_CITY.ID"=>array(false,$GLOBALS["CURRENT_CITY"]["ID"]));
$CITY_ID = GetCurrentCity();
?>
<?$APPLICATION->IncludeComponent("bitrix:news.list", "slider-new", array(
    "IBLOCK_TYPE" => "contentsite",
    "IBLOCK_ID" => "4",
    "NEWS_COUNT" => "100",
    "SORT_BY1" => "SORT",
    "SORT_ORDER1" => "ASC",
    "SORT_BY2" => "ID",
    "SORT_ORDER2" => "DESC",
    "FILTER_NAME" => "arrFilterSlider",
    "FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "PROPERTY_CODE" => array(
        0 => "LIST",
        1 => "LINK",
    ),
    "CHECK_DATES" => "N",
    "DETAIL_URL" => "",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "PREVIEW_TRUNCATE_LEN" => "",
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "SET_TITLE" => "N",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "INCLUDE_SUBSECTIONS" => "Y",
    "PAGER_TEMPLATE" => ".default",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "PAGER_TITLE" => "",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "AJAX_OPTION_ADDITIONAL" => ""
    ),
    false
);?>
<?else: ?>
    <div class="b-banner_inner__wrap">
        <div class="b-banner_inner l-width">
            <ul class="b-banner_inner__slider js-banner-slider">
            <?php
                CModule::IncludeModule("iblock");

                $searchUrls = [];

                $match = null;
                $isServiceInClinic = preg_match('#^/services/.+/clinic/(.+)/$#', $APPLICATION->GetCurDir(), $match);
                if ($isServiceInClinic) {
                    $searchUrls[] = $APPLICATION->GetCurDir();
                    $clinicCode = $match[1];
                } else {
                    $currentUrl = '/';
                    $urlParts = array_filter(explode('/', $APPLICATION->GetCurDir()));
                    foreach ($urlParts as $urlPart) {
                        $currentUrl .= $urlPart . '/';
                        $searchUrls[] = $currentUrl;
                    }
                }
                $currentPage = $APPLICATION->GetCurPageParam('', [
                    'CLEAR_CACHE',
                    'SECTION_CODE',
                    'ELEMENT_CODE',
                ]);

                if (!in_array($currentPage, $searchUrls)) {
                    $searchUrls[] = $currentPage;
                }

                $arSelect = [
                    'ID',
                    'IBLOCK_ID',
                    'NAME',
                    'PREVIEW_PICTURE',
                    'PREVIEW_TEXT',
                    'PROPERTY_URL_TO',
                    'PROPERTY_CLINIC', // Множественное !
                    'PROPERTY_ISFLOAT_BUTTON',
                    'PROPERTY_ISFLOAT',
                ];

                $arFilter = [
                    'IBLOCK_ID' => SECTION_BANNER_IBLOCK_ID,
                    'ACTIVE' => 'Y',
                    'ACTIVE_DATE' => 'Y',
                    'PROPERTY_CITY' => [
                        false,
                        CityManager::getInstance()->getSavedCityId(),
                    ],
                ];

                // Важно - добавляем условие по урлу или по клинике и урлу последним.
                // Если баннеры не будут найдены, мы пробуем выполнить более общий поиск баннеров,
                // откинув последнее условие из arFilter через array_pop.
                if ($isServiceInClinic) {
                    $arFilter[] = [
                        'LOGIC' => 'OR',
                        ['PROPERTY_URL' => $searchUrls],
                        ['PROPERTY_CLINIC.CODE' => $clinicCode],
                    ];
                } else {
                    $arFilter['PROPERTY_URL'] = $searchUrls;
                }

                $arBanners = [];

                $rsBanners = CIBlockElement::GetList(
                    ['SORT' => 'ASC'],
                    $arFilter,
                    false,
                    false,
                    $arSelect
                );

                while ($arBanner = $rsBanners->GetNext()) {
                    $arBanners[$arBanner['ID']] = $arBanner;
                }

                $onlyFloatBannerButton = false;
                if (!empty($arBanners)) {
                    $onlyFloatBannerButton = true;
                    foreach($arBanners as $arBanner) {
                        if (!$arBanner["PROPERTY_ISFLOAT_BUTTON_VALUE"] && !$arBanner["PROPERTY_ISFLOAT_VALUE"] ) {
                            $onlyFloatBannerButton = false;
                        }
                    }
                }

                if ($onlyFloatBannerButton || empty($arBanners)) {
                    // Откидываем последнее условие на выборку баннера по урлу или клинике.
                    array_pop($arFilter);

                    $arFilter['PROPERTY_URL'] = false;

                    $rsBanners = CIBlockElement::GetList(
                        ['SORT' => 'ASC'],
                        $arFilter,
                        false,
                        false,
                        $arSelect
                    );
                    if (!$onlyFloatBannerButton) {
                        $arBanners = [];
                    }
                    while ($arBanner = $rsBanners->GetNext()) {
                        $arBanners[$arBanner['ID']] = $arBanner;
                    }
                }

            $floatButton = null;
            $floatBanner = null;
            foreach($arBanners as $arBanner):
                $isShowLink = !empty($arBanner['PROPERTY_URL_TO_VALUE']);
                $bannerName = htmlentities($arBanner['NAME']);
                $bannerImageSrc = CFile::GetPath($arBanner['PREVIEW_PICTURE']);
                if ($arBanner["PROPERTY_ISFLOAT_BUTTON_VALUE"]) :
                    $floatButton = $arBanner;
                elseif ($arBanner["PROPERTY_ISFLOAT_VALUE"]) :
                    $floatBanner = $arBanner;
                else :
                ?>
                    <li>
                        <?php if ($isShowLink): ?>
                            <a href="<?php echo $arBanner['PROPERTY_URL_TO_VALUE']; ?>" title="<?php echo $bannerName; ?>">
                        <?php endif; ?>
                        <img src="<?php echo $bannerImageSrc; ?>" alt="<?php echo $bannerName; ?>">
                        <div class="b-banner_inner__txt">
                            <?php echo $arBanner['PREVIEW_TEXT']; ?>
                        </div>
                        <?php if ($isShowLink): ?>
                            </a>
                        <?php endif; ?>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php if ($floatButton) : ?>
            <div class="banner-callback js-callback-fixed"><div class="banner-callback__ico in-bl"></div></div>
        <?php endif; ?>
        <?php if ($floatBanner) : ?>
            <div class="banner-fix js-banner-fix">
                <div class="banner-fix__close js-banner-fix-close"></div>
                <a href="<?php echo $floatBanner['PROPERTY_URL_TO_VALUE']; ?>">
                    <img alt="<?php echo htmlentities($floatBanner['NAME']); ?>" src="<?php echo CFile::GetPath($floatBanner['PREVIEW_PICTURE']); ?>">
                </a>
            </div>
            <script>
                $(function() {
                    setTimeout(function() {
                        $('.js-banner-fix').css('display','block').animate({bottom: 30, opacity: 1}, 500)
                    }, 3000);
                    $('.js-banner-fix-close').click(function() {
                        $(this).parents('.js-banner-fix').fadeOut(300);
                    });
                });
            </script>
        <?php endif; ?>
    </div>
<?endif;?>
