<?php

$isDefault = CityManager::getInstance()->isCurrentCityDefault();
$doctorsHeader = 'Врачи';
$clinicsHeader = 'Клиники';
$servicesHeader = 'Услуги';
if ($isDefault) {
    $doctorsHeader .= ' Москвы';
    $clinicsHeader .= ' Москвы';
    $servicesHeader .= ' в Москве';
}

$aMenuLinks = Array(
    Array(
        "О «Мать и дитя»",
        "/about/",
        Array(),
        Array(),
        ""
    ),
    Array(
        $doctorsHeader,
        "/doctors/",
        Array(),
        Array(),
        ""
    ),
    Array(
        $clinicsHeader,
        "/clinics/",
        Array(),
        Array(),
        ""
    ),
    Array(
        $servicesHeader,
        "/services/",
        Array(),
        Array("EXTENDED"=>"1"),
        ""
    ),
);
if (!$isDefault) {
    $aMenuLinks[] = Array(
        "Цены",
        "/price-list2/",
        Array(),
        Array(),
        ""
    );
}

$aMenuLinks = array_merge($aMenuLinks, array(
    Array(
        "Пациентам",
        "/info/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Инвесторам",
        "http://www.mcclinics.ru/",
        Array(),
        Array("target"=>"_blank", 'rel' => 'nofollow'),
        ""
    )
));
?>
