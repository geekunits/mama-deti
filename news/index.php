<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?>
<?php
$CITY_ID = GetCurrentCity();
$arClinicsID = CMamaDetiAPI::getClinicsID(array("PROPERTY_CITY" => $CITY_ID));
$arServicesID = CMamaDetiAPI::getClinicsServicesID(array("PROPERTY_CITY" => $CITY_ID));

$GLOBALS["arrFilterNews"] = array(
    array("LOGIC" => "OR",
        array("PROPERTY_CLINIC" => $arClinicsID),
        array("PROPERTY_CLINIC" => false,"PROPERTY_SERVICES" => $arServicesID),
        array("PROPERTY_GLOBAL" => 1),
    ),
);
$GLOBALS["arrFilterEventNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 4,
));
$GLOBALS["arrFilterEduNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 5,
));
?>
<div class="b-bg_white" style="padding:0 40px;">
    <div class="b-news_switch js-news-switch" data-id="1" style="margin-top:-45px;">Образовательные</div>
    <div class="b-news_switch js-news-switch" data-id="2" style="display:none; margin-top:-45px;">Событийные</div>
    <div class="b-news js-news" data-id="1">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-news", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "8",
            "NEWS_COUNT" => "12",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterEventNews",
            "FIELD_CODE" => array(
            0 => "",
            1 => "",
            ),
            "PROPERTY_CODE" => array(
            0 => "",
            1 => "CLINIC",
            2 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "Y",

            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "180",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => "mamadeti",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "RESIZE_CATALOG_METHOD" => "2",
            "RESIZE_CATALOG_WIDTH" => "325",
            "RESIZE_CATALOG_HEIGHT" => "183",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
    </div>
    <div class="b-news js-news" data-id="2" style="display:none;">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-news", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "8",
            "NEWS_COUNT" => "12",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterEduNews",
            "FIELD_CODE" => array(
            0 => "",
            1 => "",
            ),
            "PROPERTY_CODE" => array(
            0 => "",
            1 => "CLINIC",
            2 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "Y",

            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "180",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => "mamadeti",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "RESIZE_CATALOG_METHOD" => "2",
            "RESIZE_CATALOG_WIDTH" => "325",
            "RESIZE_CATALOG_HEIGHT" => "183",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>