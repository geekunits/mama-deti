<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Регистрация");
?>
<p>Форма регистрации на конференцию &quot;Современные тенденции в профилактике, диагностике и лечении невынашивания беременности&quot;</p>
<div class="form-register-conf">
	<?
	require($_SERVER["DOCUMENT_ROOT"]."/ajax/registerConf.php");
	?>
</div>
<style>
	.form-register-conf .popup-frame {
		padding: 0;
	}
	.form-register-conf .popup-title,
	.form-register-conf .close {
		display: none;
	}
	.form-register-conf .popup {
		margin: 0 !important;
		width: 440px;
	}
	.form-register-conf .label-block {
		width: 39%;
	}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>