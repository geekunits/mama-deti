<?
@define("PAGE_404","Y");

include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");
?>
<div class="page-404">
	<div class="page-404__txt">
		<br>
		Страница, которую вы запрашиваете, не найдена. <br><br>

		Возможно она была перемещена или<br> вы ошиблись при наборе адреса.
	</div>
</div>
<div style="width: 0; height: 0; font-size: 0;">
	<script type="text/javascript">
		document.write('<img src="http://404.sabit.su/counter/?x=' + Math.random()
				+ "&s=75&u=" + escape(window.location.href || document.URL)
				+ "&r=" + escape(document.referrer)
				+ '" />'
		);
	</script>
	<noscript>
		<img src="http://404.sabit.su/counter/?s=75" />
	</noscript>
</div>
<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>