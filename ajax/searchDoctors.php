<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("search");

$_REQUEST["txt"] = trim($_REQUEST["txt"]);

if (!empty($_REQUEST["txt"])){
	$arResult = array();
	$arResult["count"] = 0;

	$CITY_ID = GetCurrentCity();

    $arFilter = array(
        "IBLOCK_ID"=>3,
        "ACTIVE"=>"Y",
        "PROPERTY_CLINIC_PROPERTY_CITY"=>$CITY_ID,
    );

    $arSubFilter = array(
        "LOGIC" => "AND",
    );

    $arTxt = explode(' ',$_REQUEST["txt"]);
    foreach ($arTxt as $txt)
    {
        if (!empty($txt))
            $arSubFilter[] = array("?NAME"=>$txt);
    }
    $arFilter[] = $arSubFilter;

    $rsElement = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_CLINIC.PROPERTY_CITY"));
    while ($arElement = $rsElement->GetNext()) {
        if ($arElement['PROPERTY_CLINIC_PROPERTY_CITY_VALUE'] !== $CITY_ID) {
            continue;
        }
        $arResult["str"] .= '<li><a href="#" class="title"><span class="black-color">'.$arElement["NAME"].'</span></a></li>';
        $arResult["count"]++;
    }
    /*
	$arAllowItems = array();
	$arDoctors = array();
	$rsElement = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y", "PROPERTY_CITY"=>$CITY_ID), false, false, array("ID","NAME","DETAIL_PAGE_URL"));
	while ($arElement = $rsElement->GetNext()) {
		$arAllowItems[] = $arElement["ID"];
		$arDoctors[$arElement["ID"]] = array(
			"ID" => $arElement["ID"],
			"NAME" => $arElement["NAME"],
			"DETAIL_PAGE_URL" => $arElement["DETAIL_PAGE_URL"],
			);
	}

	$arParams = array(
		"MODULE_ID" => "iblock",
		"ITEM_ID" => $arAllowItems,
		"PARAM2" => 3, //инфоблок
		"QUERY" => $_REQUEST["txt"],
		
	);

	$obSearch = new CSearch;
	$obSearch->Search($arParams);


	$tags_get = explode(",", $_REQUEST["tags"]);
	$arTags = array();

	if ($obSearch->errorno==0) {
		while ($arSearch =  $obSearch->GetNext())
			foreach ($arSearch["TAGS_FORMATED"] as $value)
			{
				if (!in_array($value, $arTags) && !in_array($value,$tags_get))
					$arTags[] = $value;
			}
	} else
	{
	}

	foreach ($arTags as $value)
	{
		$arr["str"] .= '<li><a href="#" class="title"><span class="black-color">'.$value.'</span></a></li>';
		$arr["count"]++;
	}*/

	echo json_encode($arResult);
}
?>