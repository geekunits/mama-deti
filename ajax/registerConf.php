<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>

<?php
$APPLICATION->IncludeComponent(
    "melcosoft:feedback3",
    "register-conf",
    Array(
        "AJAX_MODE" => "Y",
        "EVENT_ID" => "68",
        "EMAIL_FIELDS" => array("P_EMAIL"),
        "USE_GUEST_CAPTCHA" => "Y",
        "REQUIRED_FIELDS" => array("NAME", "P_PHONE", "P_EMAIL", "P_JOB", "P_SPEC"),
        "WRITE_IBLOCK" => "Y",
        "IBLOCK_ID" => "46",
        "ELEMENT_NAME" => "#NAME#",
        "FORM_VARS_PREFIX" => "MS_",
        "FORM_ID" => "FORM_REGISTER_CONF",
        "FORM_CLASS" => "",
        "REDIRECT_IF_SUCCESS" => "N",
        "REDIRECT_URL" => "",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "NEWS_ID" => 13927138,
    ),
    false
);
?>