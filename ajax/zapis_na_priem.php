<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
$requiredFields = array(
    "NAME",
    "P_PHONE",
    "P_CLINIC",
    "P_DATE",
    "P_TIME",
);

$clinicId = isset($_REQUEST['CLINIC']) ? (int) $_REQUEST['CLINIC'] : (int) $_REQUEST['MS_P_CLINIC'];

if ($clinicId > 0) {
    $clinics = CMamaDetiAPI::GetClinics(['ID' => $clinicId]);

    $clinic = null;

    if (!empty($clinics)) {
        $clinic = $clinics[0];
    }

    if($clinic && $clinic['PROPERTIES']['SHOW_CONFIDENTIAL_CONFIRM']['VALUE']){
        $requiredFields[] = "P_SHOW_CONFIDENTIAL_CONFIRM";
    }
}
?>
<?$APPLICATION->SetTitle("");?> <?$APPLICATION->IncludeComponent(
	"melcosoft:feedback3",
	"",
	Array(
		"AJAX_MODE" => "Y",
		"EVENT_ID" => "39",
		"EMAIL_FIELDS" => array(),
		"REQUIRED_FIELDS" => $requiredFields,
		"WRITE_IBLOCK" => "Y",
		"IBLOCK_ELEMENT_ACTIVE" => "Y",
		"IBLOCK_ID" => "14",
		"ELEMENT_NAME" => "#NAME#",
		"USE_GUEST_CAPTCHA" => "Y",
		"FORM_VARS_PREFIX" => "MS_",
		"FORM_ID" => "zapis_na_priem",
		"FORM_CLASS" => "",
		"REDIRECT_IF_SUCCESS" => "N",
		"REDIRECT_URL" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);?>