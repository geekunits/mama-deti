<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?php
    $APPLICATION->IncludeComponent(
        "melcosoft:feedback3",
        "fourteen-may-event",
        Array(
            "AJAX_MODE" => "Y",
            "EVENT_ID" => "50",
            "EMAIL_FIELDS" => array("P_EMAIL"),
            "USE_GUEST_CAPTCHA" => "Y",
            "REQUIRED_FIELDS" => array("NAME", "P_PHONE", "P_EMAIL"),
            "WRITE_IBLOCK" => "Y",
            "IBLOCK_ID" => "46",
            "ELEMENT_NAME" => "#NAME#",
            "FORM_VARS_PREFIX" => "MS_",
            "FORM_ID" => "MAY_14_EVENT",
            "FORM_CLASS" => "",
            "REDIRECT_IF_SUCCESS" => "N",
            "REDIRECT_URL" => "",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
        ),
    false
    );
?>