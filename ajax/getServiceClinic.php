<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

if (!empty($_REQUEST["name"])){
	$arr = array();
	$arr["count"] = 0;
	$name = htmlspecialcharsEx($_REQUEST["name"]);
	
	$service = array(); $str = "";
	$res = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "=NAME"=>$name), false, false, array("ID"));
	if ($arres = $res->GetNext())
	{
		$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y", "PROPERTY_CLINICS"=>$arres["ID"]);
		$resserv = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("IBLOCK_SECTION_ID"));
		while($arresserv = $resserv->GetNext())
		{		
			$ressect = CIBlockSection::GetByID($arresserv["IBLOCK_SECTION_ID"]);
			if ($arressect = $ressect->GetNext()){
				if ($arressect["DEPTH_LEVEL"]==1) $service[$arressect["NAME"]] = $arressect["ID"];
				else $service[$arressect["NAME"]] = $arressect["IBLOCK_SECTION_ID"];
			}
		}
	}
	
	if (empty($service)) {
		$arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y");
		$ressect = CIBlockSection::GetList(Array("NAME"=>"asc"), $arFilter, false, array("ID", "NAME"));
		while($arressect = $ressect->GetNext())
		{		
			$service[$arressect["NAME"]] = $arressect["ID"];
		}
	}
	
	ksort($service);
	$str .= '<ul class="search-listing"><li><a href="#" class="title"><span class="black-color">Любое направление</span></a></li>';
	$arr["count"]++;
	
	foreach ($service as $key=>$s){
		$str .= '<li><a href="#" class="title"><span class="black-color">'.$key.'</span></a></li>';
		$arr["count"]++;
	}	
	$str .= '</ul>';
	$arr["str"] = $str;
	
	echo json_encode($arr);
}
?>