<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?php

$clinics = [
    [
        'name' => 'Клинический Госпиталь Лапино "Мать и дитя"', 
        'email' => 'e.malukova@mcclinics.ru',
        'id' => 421,
        'address' => 'Московская область, Одинцовский район, 1-е Успенское шоссе, д. Лапино, д. 111, подъезд 10',
    ],
    [
         'name' => 'Перинатальный Медицинский Центр "Мать и дитя"', 
         'email' => 'eko@mcclinics.ru',
         'id' => 422,
         'address' => 'г. Москва, Севастопольский проспект, д. 24, к. 1.',
    ],
    // [
    //     'name' => 'Клиника "Мать и дитя" Савеловская', 
    //     'email' => 'n.yakusheva@mcclinics.ru',
    //     'id' => 425,
    //     'address' => 'г. Москва, ул. Б. Новодмитровская, д.23, стр.2. Вход в клинику со стороны ул. Бутырская, д. 46',
    // ],
    // [
    //     'name' => 'Клиника "Мать и дитя" Кунцево', 
    //     'email' => 'kuntsevo@mcclinics.ru',
    //     'id' => 423,
    //     'address' => 'г. Москва, Можайское шоссе, д. 2',
    // ],
    /*[
        'name' => 'Клиника "Мать и дитя" Юго-Запад', 
        'email' => 'uz.customer@mcclinics.ru',
        'id' => 424,
        'address' => 'г. Москва, ул. Островитянова, отделение ЭКО - вход со двора',
    ],*/
    [
        'name' => 'Клиника "Мать и дитя" Санкт-Петербург', 
        'email' => 'dod-mid@yandex.ru',
        'id' => 429,
        'address' => 'г. Санкт-Петербург, Средний проспект, д. 88',
    ],
    [
        'name' => 'Клиника "Мать и дитя" Рязань', 
        'email' => 'd.habarova@mcclinics.ru',
        'id' => 5129,
        'address' => 'г. Рязань, ул. Шереметьевская, 16',
    ],
    [
        'name' => 'Клиника "Мать и дитя" Пермь', 
        'email' => 'perm.customer@mcclinics.ru',
        'id' => 430,
        'address' => 'г. Пермь, ул. Екатерининская, д. 64',
    ],
    // [
    //     'name' => 'Госпиталь "Мать и дитя" Уфа', 
    //     'email' => 'd.shaihutdinova@mcclinics.ru',
    //     'id' => 5126,
    //     'address' => 'г. Уфа, Лесной проезд, д. 4',
    // ],
    // [
    //     'name' => 'Клиника "Мать и дитя" Уфа', 
    //     'email' => 'hospital@mamadetiufa.ru',
    //     'id' => 428,
    //     'address' => 'г. Уфа, Королева, д.24',
    // ],
    [
        'name' => 'Клиника "Мать и дитя" Ярославль', 
        'email' => 'klinika.700-308@yandex.ru',
        'id' => 434,
        'address' => 'г. Ярославль, ул. 5-я Яковлевская, д. 17 место проведения: конференц-зал Ринг Премьер Отеля (Ярославль, ул. Свободы, 55)',
    ],
    /*[
         'name' => 'Клиника "Мать и дитя" Иркутск', 
         'email' => null,
         'id' => 431,
    ],
    [
        'name' => 'Клиника "Мать и дитя" Тольятти', 
        'email' => null,
        'id' => null,
    ],
    [
         'name' => 'Авиценна', 
         'email' => null,
         'id' => null,
    ],*/
    [
        'name' => 'Новосибирский Центр Репродуктивной Медицины', 
        'email' => 'sviridovama24@yandex.ru',
        'id' => null,
        'address' => 'г. Новосибирск, ул. Героев революции, д. 3',
    ],
    [
        'name' => 'Барнаульский Центр Репродуктивной медицины', 
        'email' => 'mamadeti.barnaul@yandex.ru',
        'id' => null,
        'address' => 'г. Барнаул, ул. Папанинцев, д.165 
место проведения: г.Барнаул, пр.Красноармейский 72 (Гостиница "Турист", 16 этаж, конференц-зал)',
    ],
    [
        'name' => 'Омский Центр Репродуктивной Медицины', 
        'email' => 'trubitsinaonline@gmail.com',
        'id' => null,
        'address' => 'г. Омск, ул. Березовая, 3 (здание пансионата) 
место проведения: г. Омск, ул . 70 лет Октября, д. 25, к 2, Омский областной экспоцентр, Синий зал',
    ],
    [
         'name' => 'Красноярский Центр Репродуктивной Медицины', 
         'email' => 'info@artmedgroup.ru',
         'id' => null,
         'address' => 'г. Красноярск, ул. Коломенская, д. 26, корп. №1',
    ],
    /*[
         'name' => 'Клиника «Мать и дитя» Новокузнецк', 
         'email' => null,
         'id' => null,
    ],*/

];

$APPLICATION->IncludeComponent(
    "melcosoft:feedback3",
    "form-eko",
    Array(
        "AJAX_MODE" => "Y",
        "EVENT_ID" => "52",
        "EMAIL_FIELDS" => array("P_EMAIL"),
        "USE_GUEST_CAPTCHA" => "Y",
        "REQUIRED_FIELDS" => array("NAME", "P_SURNAME", "P_PHONE", "P_EMAIL", "P_CLINIC", "P_INFO_SOURCE"),
        "WRITE_IBLOCK" => "Y",
        "IBLOCK_ID" => "47",
        "ELEMENT_NAME" => "#NAME#",
        "FORM_VARS_PREFIX" => "MS_",
        "FORM_ID" => "FORM_ECO",
        "FORM_CLASS" => "",
        "REDIRECT_IF_SUCCESS" => "N",
        "REDIRECT_URL" => "",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        '_CLINICS' => $clinics,
        '_DEFAULT_EMAIL' => 'n.beseda@mcclinics.ru',
    ),
false
);
?>