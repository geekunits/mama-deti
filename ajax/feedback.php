<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<p>Чтобы отправить нам сообщение, пожалуйста, воспользуйтесь приведённой формой:</p>
<?php
    $APPLICATION->IncludeComponent("mamadeti:main.feedback", ".default", array(
    "USE_CAPTCHA" => "Y",
    "OK_TEXT" => "Спасибо, ваше сообщение принято.",
    "EMAIL_TO" => "quality@mcclinics.ru, n.beseda@mcclinics.ru",
    "REQUIRED_FIELDS" => array(
    ),
    "EVENT_MESSAGE_ID" => array(
    ),
    '_CITY' => $GLOBALS['CURRENT_CITY'],
    'IBLOCK_ID' => '34',
    'CLINIC_EMAIL_PROPERTY' => 'FEEDBACK_EMAIL',
    ),

    false
);?>