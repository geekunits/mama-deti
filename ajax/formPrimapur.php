<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?php
$APPLICATION->IncludeComponent(
    "melcosoft:feedback3",
    "form-primapur",
    Array(
        "AJAX_MODE" => "Y",
        "EVENT_ID" => "60",
        "EMAIL_FIELDS" => array("P_EMAIL"),
        "USE_GUEST_CAPTCHA" => "Y",
        "REQUIRED_FIELDS" => array(
            "P_FIO",
            "P_PHONE",
            "P_EMAIL",
            "P_CLINIC"
        ),
        "WRITE_IBLOCK" => "Y",
        "IBLOCK_ID" => "53",
        "ELEMENT_NAME" => "#P_FIO#",
        "FORM_VARS_PREFIX" => "MS_",
        "FORM_ID" => "FORM_ECO",
        "FORM_CLASS" => "",
        "REDIRECT_IF_SUCCESS" => "N",
        "REDIRECT_URL" => "",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "SEND_CLIENT_EMAIL_MESSAGE" => "Y",
        "CLIENT_EMAIL_EVENT" => "63",
        "SUCCESS_MESSAGE" => "Благодарим Вас, Ваша анкета принята к рассмотрению."
    ),
    false
);
?>