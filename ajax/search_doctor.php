<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("search");

$_REQUEST["txt"] = trim($_REQUEST["txt"]);
$arResult = array();
$arResult["DOCTORS"] = array();
$arResult["TXT"] = $_REQUEST["txt"];

if (strlen($_REQUEST["txt"])>0){

	$CITY_ID = GetCurrentCity();

	$arAllowItems = array();
	$arDoctors = array();

	$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y", "PROPERTY_CITY"=>$CITY_ID);
	if (intval($_REQUEST["clinic"])>0)
		$arFilter["PROPERTY_CLINIC"] = $_REQUEST["clinic"];

	$rsElement = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("ID","NAME","DETAIL_PAGE_URL"));
	while ($arElement = $rsElement->GetNext()) {
		$arAllowItems[] = $arElement["ID"];
		$arDoctors[$arElement["ID"]] = array(
			"ID" => $arElement["ID"],
			"NAME" => $arElement["NAME"],
			"DETAIL_PAGE_URL" => $arElement["DETAIL_PAGE_URL"],
			);
	}

	$arParams = array(
		"MODULE_ID" => "iblock",
		"ITEM_ID" => $arAllowItems,
		"PARAM2" => 3, //инфоблок
		"QUERY" => $_REQUEST["txt"],
		
	);

	$obSearch = new CSearch;
	$obSearch->Search($arParams);

	if ($obSearch->errorno==0) {
		while ($arSearch =  $obSearch->GetNext())
			$arResult["DOCTORS"][] = $arDoctors[$arSearch["ITEM_ID"]];
	} else
	{ 
	}

} else
{
	$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y");
	if (intval($_REQUEST["clinic"])>0)
		$arFilter["PROPERTY_CLINIC"] = $_REQUEST["clinic"];

	$rsElement = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("ID","NAME","DETAIL_PAGE_URL"));
	while ($arElement = $rsElement->GetNext()) {
		$arResult["DOCTORS"][] = array(
			"ID" => $arElement["ID"],
			"NAME" => $arElement["NAME"],
			"DETAIL_PAGE_URL" => $arElement["DETAIL_PAGE_URL"],
			);
	}
}

echo json_encode($arResult);

?>