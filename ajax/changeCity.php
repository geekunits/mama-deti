<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$data = [];

if (!empty($_REQUEST["name"])) {
    global $APPLICATION;

    $res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>7, "=NAME"=>$_REQUEST["name"]), false, Array("nPageSize"=>1), array("ID"));
    if ($arres = $res->GetNext()) {
        $APPLICATION->set_cookie(CITY_COOKIE_NAME, $arres["ID"], time()+60*60*24*30*12, '/', COOKIE_DOMAIN);

        if (in_array($arres["ID"], [402, 404, 35])) {
            $data['url'] = 'http://samara.mamadeti.ru/';
        }
    }
}

header('Content-Type: application/json');

echo json_encode($data);
