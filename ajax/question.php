<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?

CModule::IncludeModule("iblock");

if (isset($_REQUEST["DOCTOR"]) && intval($_REQUEST["DOCTOR"])>0)
{
	$_REQUEST["DOCTOR"] = intval($_REQUEST["DOCTOR"]);
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ACTIVE"=>"Y","ID"=>$_REQUEST["DOCTOR"],"PROPERTY_BUTTON_QUESTION"=>1),false,false,array("ID","NAME","PROPERTY_CLINIC"));
	if ($arElement = $rsElement->Fetch())
	{
		$_REQUEST["CLINIC"] = $arElement["PROPERTY_CLINIC_VALUE"];
	}
}

$_REQUEST["CLINIC"] = intval(is_array($_REQUEST['CLINIC']) ? $_REQUEST["CLINIC"][0] : $_REQUEST["CLINIC"]);
$_REQUEST["DOCTOR"] = intval($_REQUEST["DOCTOR"]);
$_REQUEST["SERVICE"] = intval($_REQUEST["SERVICE"]);
if ($_REQUEST["CLINIC"]>0)
{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$_REQUEST["CLINIC"],"ACTIVE"=>"Y"));
	if (!$arElement = $rsElement->Fetch())
		die();
}

$APPLICATION->IncludeComponent(
	"melcosoft:feedback3",
	"question",
	Array(
		"ASET_P_CLIENT_CLINIC" => $_REQUEST["CLINIC"],
		"ASET_P_CLIENT_DOCTOR" => $_REQUEST["DOCTOR"],
		"ASET_P_CITY" => $GLOBALS["CURRENT_CITY"]["ID"],
		"ASET_P_CLIENT_CITY" => $GLOBALS["CURRENT_CITY"]["NAME"],
		"ASET_P_CLIENT_DIRECTION" => $_REQUEST['SERVICE'],
		"AJAX_MODE" => "Y",
		"EVENT_ID" => "42",
		"EMAIL_FIELDS" => array(),
		"REQUIRED_FIELDS" => array("NAME", "P_EMAIL", "PREVIEW_TEXT", "P_CLIENT_CLINIC"),
		"WRITE_IBLOCK" => "Y",
		"IBLOCK_ELEMENT_ACTIVE" => "N",
		"IBLOCK_ID" => "10",
		"ELEMENT_NAME" => "#NAME#",
		"USE_GUEST_CAPTCHA" => "Y",
		"FORM_VARS_PREFIX" => "MS_",
		"FORM_ID" => "",
		"FORM_CLASS" => "",
		"REDIRECT_IF_SUCCESS" => "N",
		"REDIRECT_URL" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
	),
false
);?>