<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
$requiredFields = array("NAME", "PREVIEW_TEXT", "P_CLINIC");


if($_REQUEST['review_type'] == 'doctor'){
    $requiredFields[] = 'P_DOCTOR';
}

?>
<?$APPLICATION->IncludeComponent(
	"melcosoft:feedback3",
	"review",
	Array(
		"AJAX_MODE" => "Y",
		"EVENT_ID" => "43",
		"EMAIL_FIELDS" => array(),
		"REQUIRED_FIELDS" => $requiredFields,
		"WRITE_IBLOCK" => "Y",
		"IBLOCK_ELEMENT_ACTIVE" => "N",
		"IBLOCK_ID" => "11",
		"ELEMENT_NAME" => "#NAME#",
		"USE_GUEST_CAPTCHA" => "Y",
		"FORM_VARS_PREFIX" => "MS_",
        "FORM_ID" => "review",
		"FORM_CLASS" => "",
		"REDIRECT_IF_SUCCESS" => "N",
		"REDIRECT_URL" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	),
false
);?>