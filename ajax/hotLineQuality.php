<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?php 
$APPLICATION->IncludeComponent(
	"mamadeti:main.feedback",
	".default",
	Array(
		"USE_CAPTCHA" => "Y",
		"OK_TEXT" => "Спасибо, ваше сообщение принято.",
		"EMAIL_TO" => "quality@mcclinics.ru",
		"REQUIRED_FIELDS" => array(),
		"EVENT_MESSAGE_ID" => array(),
		'IBLOCK_ID' => '36',
		'EVENT_NAME' => 'HOTLINE_FORM',
		'CLINIC_EMAIL_PROPERTY' => '',
	)
);?>