<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
CModule::IncludeModule("iblock");

if (intval($_REQUEST["id"])<=0)
    die();

$arElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$_REQUEST["id"],"ACTIVE"=>"Y"),false,false,array('PROPERTY_ADDRESS','PROPERTY_COORD','NAME'))->GetNext();
if (!$arElement)
    die();

//$arElement = $obElement->GetFields();
//$arElement["PROPERTIES"] = $obElement->GetProperties();

$arCoord = explode(',',$arElement["PROPERTY_COORD_VALUE"]);
?>

<div id="ajax_clinic_map" style="width:600px;height:500px;"></div>
<script>

var clinicMap;

ymaps.ready(ajax_clinic_map_init);

function ajax_clinic_map_init()
{
    clinicMap = new ymaps.Map('ajax_clinic_map', {
        center: [<?=$arCoord[0]?>, <?=$arCoord[1]?>], // Москва
        zoom: 16,
    controls: ['zoomControl','typeSelector']
    });

    var firstButton = new ymaps.control.Button({data: {content: "Печать"}, options: {selectOnClick: false}});

   firstButton.events.add(
    'press',
    function () {
      $.fn.clinicMapPrint('<?=$arElement["PROPERTY_ADDRESS_VALUE"]['TEXT']?>','<?=$arCoord[1].','.$arCoord[0]?>');
    }
  );

    clinicMap.controls.add(firstButton, {float: 'right'});

    myPlacemark = new ymaps.Placemark(clinicMap.getCenter(), {
            hintContent: '<?=$arElement["NAME"]?>'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '<?=SITE_TEMPLATE_PATH.'/images/hospital_ya_map.png'?>',
            // Размеры метки.
            iconImageSize: [87, 53],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            //iconImageOffset: [-3, -42]
            iconImageOffset: [-43, -26]
        });

    clinicMap.geoObjects.add(myPlacemark);

CustomControlClass = function (options) {
            CustomControlClass.superclass.constructor.call(this, options);
            this._$content = null;
            this._geocoderDeferred = null;
        };

ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
        onAddToMap: function (map) {
            CustomControlClass.superclass.onAddToMap.call(this, map);
            this._lastCenter = null;
            this.getParent().getChildElement(this).then(this._onGetChildElement, this);
        },

        onRemoveFromMap: function (oldMap) {
            this._lastCenter = null;
            if (this._$content) {
                this._$content.remove();
                this._mapEventGroup.removeAll();
            }
            CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
        },

        _onGetChildElement: function (parentDomContainer) {
            // Создаем HTML-элемент с текстом.
            this._$content = $('<div class="customMapControl"><?=$arElement["PROPERTY_ADDRESS_VALUE"]['TEXT']?></div>').appendTo(parentDomContainer);
            this._mapEventGroup = this.getMap().events.group();
            // Запрашиваем данные после изменения положения карты.
            //this._mapEventGroup.add('boundschange', this._createRequest, this);
            // Сразу же запрашиваем название места.
            //this._createRequest();
        }
    });

var customControl = new CustomControlClass();
    clinicMap.controls.add(customControl, {
        float: 'none',
        position: {
            top: 10,
            left: 10
        }
    });
};
</script>

<?php
/*
$APPLICATION->ShowHeadScripts();
$APPLICATION->ShowHeadStrings();

$arCoord = explode(',',$arElement["PROPERTY_COORD_VALUE"]);
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:map.yandex.view",
    "",
    Array(
        "INIT_MAP_TYPE" => "MAP",
        "MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:".$arCoord[0].";s:10:\"yandex_lon\";d:".$arCoord[1].";s:12:\"yandex_scale\";i:10;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:".$arCoord[1].";s:3:\"LAT\";d:".$arCoord[0].";s:4:\"TEXT\";s:0:\"\";}}}",
        "MAP_WIDTH" => "600",
        "MAP_HEIGHT" => "500",
        "CONTROLS" => array("ZOOM", "MINIMAP", "TYPECONTROL", "SCALELINE"),
        "OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING"),
        "MAP_ID" => ""
    ),
false
);*/?>
