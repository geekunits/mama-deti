<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
CModule::IncludeModule("search");

$_REQUEST["txt"] = trim($_REQUEST["txt"]);

if (!empty($_REQUEST["txt"])){
	$arr = array();
	$arr["count"] = 0;

/*	$arAllowItems = array();
	$rsElement = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), Array("IBLOCK_ID"=>9, "ACTIVE"=>"Y"), false, false, array("ID"));
	while ($arElement = $rsElement->GetNext()) {
		$arAllowItems[] = $arElement["ID"];
	}*/

	$arParams = array(
		"MODULE_ID" => "iblock",
		//"ITEM_ID" => $arAllowItems,
		"PARAM2" => 9, //инфоблок
		"QUERY" => $_REQUEST["txt"],
		
	);

	$obSearch = new CSearch;
	$obSearch->Search($arParams);


	$tags_get = explode(",", $_REQUEST["tags"]);
	$arTags = array();

	if ($obSearch->errorno==0) {
		while ($arSearch =  $obSearch->GetNext())
			foreach ($arSearch["TAGS_FORMATED"] as $value)
			{
				if (!in_array($value, $arTags) && !in_array($value,$tags_get))
					$arTags[] = $value;
			}
	} else
	{
	}

	foreach ($arTags as $value)
	{
		$arr["str"] .= '<li><a href="#" class="title"><span class="black-color">'.$value.'</span></a></li>';
		$arr["count"]++;
	}
	
	echo json_encode($arr);
}
?>