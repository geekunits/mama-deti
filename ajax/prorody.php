<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$availableIds = array(55, 66, 67);
$defaultId = $availableIds[0];
$eventId = isset($_GET['eventReplace']) ? (int) $_GET['eventReplace'] : $defaultId;
$realEventId = in_array($eventId, $availableIds) ? $eventId : $defaultId;

$newsId = isset($_GET['newsId']) ? (int) $_GET['newsId'] : 1;
if ($newsId < 1) {
    $newsId = 1;
}

?>
<?php
$APPLICATION->IncludeComponent(
    "melcosoft:feedback3",
    "fourteen-may-event",
    Array(
        "AJAX_MODE" => "Y",
        "EVENT_ID" => $realEventId,
        "EMAIL_FIELDS" => array("P_EMAIL"),
        "USE_GUEST_CAPTCHA" => "Y",
        "REQUIRED_FIELDS" => array("NAME", "P_PHONE", "P_EMAIL"),
        "WRITE_IBLOCK" => "Y",
        "IBLOCK_ID" => "46",
        "ELEMENT_NAME" => "#NAME#",
        "FORM_VARS_PREFIX" => "MS_",
        "FORM_ID" => "MAY_14_EVENT",
        "FORM_CLASS" => "",
        "REDIRECT_IF_SUCCESS" => "N",
        "REDIRECT_URL" => "",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "NEWS_ID" => $newsId,
    ),
    false
);
?>