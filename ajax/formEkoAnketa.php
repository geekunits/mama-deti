<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
?>
<?php
$APPLICATION->IncludeComponent(
    "melcosoft:feedback3",
    "form-eko-anketa",
    Array(
        "AJAX_MODE" => "Y",
        "EVENT_ID" => "57",
        "EMAIL_FIELDS" => array("P_EMAIL"),
        "USE_GUEST_CAPTCHA" => "Y",
        "REQUIRED_FIELDS" => array("P_FIO", "P_PHONE", "P_EMAIL", "P_PACIENT"),
        "WRITE_IBLOCK" => "Y",
        "IBLOCK_ID" => "51",
        "ELEMENT_NAME" => "#P_FIO#",
        "FORM_VARS_PREFIX" => "MS_",
        "FORM_ID" => "FORM_ECO",
        "FORM_CLASS" => "",
        "REDIRECT_IF_SUCCESS" => "N",
        "REDIRECT_URL" => "",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "SEND_CLIENT_EMAIL_MESSAGE" => "Y",
        "CLIENT_EMAIL_EVENT" => "58",
        "SUCCESS_MESSAGE" => "Ваша заявка принята!<br><br>Мы с вами обязательно свяжемся."
    ),
false
);
?>