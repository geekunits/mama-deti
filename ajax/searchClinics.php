<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");

$_REQUEST["txt"] = trim($_REQUEST["txt"]);

if (!empty($_REQUEST["txt"])){
	$arr = array();
	$arr["count"] = 0;

	$arFilter = array(
		"IBLOCK_ID"=>3,
		"ACTIVE"=>"Y",
		"NAME" => array(),
	);

	$arTxt = explode(' ',$_REQUEST["txt"]);
	foreach ($arTxt as $txt)
	{
		$arFilter["NAME"][] = '%'.$txt.'%';
	}

	if (isset($_REQUEST["city"]) && intval($_REQUEST["city"])>0)
	{
		$arFilter["PROPERTY_CITY"] = intval($_REQUEST["city"]);
	}

	$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("IBLOCK_ID","ID","NAME"));
	while ($arElement = $rsElement->GetNext())
	{
		$arr["str"] .= '<li><a href="#" class="title"><span class="black-color">'.$arElement["NAME"].'</span></a></li>';
		$arr["count"]++;
	}

	echo json_encode($arr);
}
?>
