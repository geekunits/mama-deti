<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?

CModule::IncludeModule("iblock");

if (isset($_REQUEST["DOCTOR"]) && intval($_REQUEST["DOCTOR"])>0)
{
	$_REQUEST["DOCTOR"] = intval($_REQUEST["DOCTOR"]);
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ACTIVE"=>"Y","ID"=>$_REQUEST["DOCTOR"],"PROPERTY_BUTTON_QUESTION"=>1),false,false,array("ID","NAME","PROPERTY_CLINIC"));
	if ($arElement = $rsElement->Fetch())
	{
		$_REQUEST["CLINIC"] = is_array($arElement["PROPERTY_CLINIC_VALUE"]) ? $arElement["PROPERTY_CLINIC_VALUE"][0] : $arElement["PROPERTY_CLINIC_VALUE"];
	}
}

$_REQUEST["CLINIC"] = intval($_REQUEST["CLINIC"]);
$_REQUEST["SERVICE"] = intval($_REQUEST["SERVICE"]);

if ($_REQUEST["CLINIC"]<=0 && $_REQUEST["SERVICE"]<=0)
	die();

if ($_REQUEST["CLINIC"]>0)
	{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$_REQUEST["CLINIC"],"ACTIVE"=>"Y"));
	if (!$arElement = $rsElement->Fetch())
		die();
	}

if ($_REQUEST["SERVICE"]>0)
	{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>1,"ID"=>$_REQUEST["SERVICE"],"ACTIVE"=>"Y"));
	if (!$arElement = $rsElement->Fetch())
		die();
	}

?>
<?$APPLICATION->IncludeComponent(
	"melcosoft:feedback3",
	"doctor-question",
	Array(
		"ASET_P_CLINIC" => $_REQUEST["CLINIC"],
		"ASET_P_CLIENT_DOCTOR" => $_REQUEST["DOCTOR"],
		"ASET_P_CITY" => $GLOBALS["CURRENT_CITY"]["ID"],
		"AJAX_MODE" => "Y",
		"EVENT_ID" => "41",
		"EMAIL_FIELDS" => array(),
		"REQUIRED_FIELDS" => array("P_PHONE", "PREVIEW_TEXT"),
		"WRITE_IBLOCK" => "Y",
		"IBLOCK_ELEMENT_ACTIVE" => "N",
		"IBLOCK_ID" => "10",
		"ELEMENT_NAME" => "#NAME#",
		"USE_GUEST_CAPTCHA" => "N",
		"FORM_VARS_PREFIX" => "MS_",
		"FORM_ID" => "",
		"FORM_CLASS" => "",
		"REDIRECT_IF_SUCCESS" => "N",
		"REDIRECT_URL" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	),
false
);?>