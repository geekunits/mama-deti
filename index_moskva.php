<?php
global $arClinicFilter;

$arClinicFilter[] = CLIENT_FROM_ABROAD;
?>
<?$APPLICATION->IncludeComponent("bitrix:news.list", "clinics-with-advantages", array(
    "IBLOCK_TYPE" => "content",
    "IBLOCK_ID" => "2",
    "NEWS_COUNT" => "100",
    "SORT_BY1" => "NAME",
    "SORT_ORDER1" => "ASC",
    "SORT_BY2" => "SORT",
    "SORT_ORDER2" => "ASC",
    "FILTER_NAME" => "arClinicFilter",
    "FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "PROPERTY_CODE" => array(
        0 => "ADDRESS",
        1 => "PHONE",
        2 => "COORD",
        3 => "IS_HOSPITAL",
        4 => "CITY",
        5 => "CONTACT_EMAIL",
    ),
    "CHECK_DATES" => "Y",
    "DETAIL_URL" => "",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "60",
    "CACHE_FILTER" => "Y",

    "CACHE_GROUPS" => "Y",
    "PREVIEW_TRUNCATE_LEN" => "180",
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "SET_TITLE" => "N",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "INCLUDE_SUBSECTIONS" => "Y",
    "PAGER_TEMPLATE" => ".default",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "RESIZE_CATALOG_METHOD" => "2",
    "RESIZE_CATALOG_WIDTH" => "325",
    "RESIZE_CATALOG_HEIGHT" => "155",
    "AJAX_OPTION_ADDITIONAL" => ""
    ),
    false
);?>
<?php
global $arHomeClinicFilter;

$arHomeClinicFilter[] = CLIENT_FROM_ABROAD;
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "home",
    array(
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => "1",
        "FILTER_NAME" => "arHomeClinicFilter",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "COUNT_ELEMENTS" => "N",
        "TOP_DEPTH" => "1",
           "_CITY_ID" => GetCurrentCity(),

    ),
    $component
);
?>
