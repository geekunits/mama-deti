<?php
$GLOBALS["arrFilterClinics"] = array("PROPERTY_CITY.ID" => $GLOBALS["CURRENT_CITY"]["ID"]);

$arRegionClinic = CMamaDetiAPI::getClinicsID(array("PROPERTY_CITY" => $GLOBALS["CURRENT_CITY"]["ID"]));
$arRegionDoctor = CMamaDetiAPI::getDoctorsID(array("PROPERTY_CLINIC" => $arRegionClinic, 'ACTIVE' => 'Y',));
$arRegionService = CMamaDetiAPI::getClinicsServicesID(array("PROPERTY_CITY" => $GLOBALS["CURRENT_CITY"]["ID"]));

$GLOBALS["arrFilterDoctors"] = array("!PROPERTY_SHOW_HOME" => false,"PROPERTY_CLINIC" => $arRegionClinic);

$GLOBALS["arrFilterReview"] = array(
    array(
        "LOGIC" => "OR",
        array("PROPERTY_DOCTOR" => $arRegionDoctor),
        array("PROPERTY_CLINIC" => $arRegionClinic, 'PROPERTY_DOCTOR' => false),
    ), );

$GLOBALS["arrFilterStory"] = array(
    array('LOGIC' => 'AND',array("PROPERTY_DOCTOR" => array_merge(array(false), $arRegionDoctor)),array('!=PROPERTY_DOCTOR' => false)),
);

$GLOBALS["arFilterQa"] = array(
    "!PREVIEW_TEXT" => false,
    array(
        "LOGIC" => "OR",
        array("PROPERTY_DOCTOR" => $arRegionDoctor),
        array("PROPERTY_CLINIC" => $arRegionClinic),
    ), );

$GLOBALS["arrFilterNews"] = array(
    array("LOGIC" => "OR",
        array("PROPERTY_CLINIC" => $arRegionClinic),
        array("PROPERTY_CLINIC" => false,"PROPERTY_SERVICES" => $arRegionService),
        array("PROPERTY_GLOBAL" => 1),
    ),
);
$GLOBALS["arrFilterEventNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 4,
));
$GLOBALS["arrFilterEduNews"] = array_merge($GLOBALS['arrFilterNews'], array(
    'PROPERTY_TYPE' => 5,
));
$GLOBALS["arrFilterGallery"] = array(
    array(
        "LOGIC" => "OR",
        array("PROPERTY_DOCTORS" => $arRegionDoctor),
        array("PROPERTY_CLINIC" => $arRegionClinic),
    ), );

?>
<div class="b-bg_white b-news_wrap b-news_footer">
    <div class="b-news_switch js-news-switch" data-id="1">Образовательные</div>
    <div class="b-news_switch js-news-switch" data-id="2" style="display:none;">Событийные</div>
    <div class="align-center b-header_h2">Новости</div>
    <div class="b-news js-news" data-id="1">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-news", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "8",
            "NEWS_COUNT" => "6",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterEventNews",
            "FIELD_CODE" => array(
            0 => "",
            1 => "",
            ),
            "PROPERTY_CODE" => array(
            0 => "",
            1 => "CLINIC",
            2 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "600",
            "CACHE_FILTER" => "Y",

            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "180",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "RESIZE_CATALOG_METHOD" => "2",
            "RESIZE_CATALOG_WIDTH" => "325",
            "RESIZE_CATALOG_HEIGHT" => "183",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
    </div>
    <div class="b-news js-news" data-id="2" style="display:none;">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-news", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "8",
            "NEWS_COUNT" => "6",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterEduNews",
            "FIELD_CODE" => array(
            0 => "",
            1 => "",
            ),
            "PROPERTY_CODE" => array(
            0 => "",
            1 => "CLINIC",
            2 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "600",
            "CACHE_FILTER" => "Y",

            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "180",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "RESIZE_CATALOG_METHOD" => "2",
            "RESIZE_CATALOG_WIDTH" => "325",
            "RESIZE_CATALOG_HEIGHT" => "183",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
    </div>
    <div class="align-center">
        <a href="/news/" class="b-link_more in-bl">Показать еще</a>
    </div>
</div>
<div class="b-widgets_footer__wrap">
    <div class="b-widgets_footer clearfix">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-review", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "11",
            "NEWS_COUNT" => "4",
            "SORT_BY1" => "RAND",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterReview",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "CLINIC",
                1 => "DOCTOR",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "80",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>

        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-story", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "12",
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "RAND",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrFilterStory",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "CLINIC",
                1 => "DOCTOR",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "600",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "80",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>

        <?$APPLICATION->IncludeComponent("bitrix:news.list", "home-qa", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "10",
            "NEWS_COUNT" => "3",
            "SORT_BY1" => "RAND",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arFilterQa",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "600",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "80",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => ""
            ),
            false
        );?>
    </div>
</div>