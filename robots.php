<?php
	$hostUrl = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'];
?>
<?php header('Content-Type: text/plain'); ?>
User-Agent: *
Allow: /bitrix/js
Allow: /bitrix/templates/
Disallow: /*index
Disallow: /*search
Disallow: /*slide_show
Disallow: /*?
Allow: /*?PAGEN_1
Disallow: /access.log
Disallow: /ajax
Disallow: /auth
Disallow: /bitrix
Disallow: /cgi-bin/
Sitemap: <?php echo $hostUrl; ?>/sitemap.xml

User-Agent: Yandex
Disallow: /*index
Disallow: /*search
Disallow: /*slide_show
Disallow: /*?
Allow: /*?PAGEN_1
Disallow: /access.log
Disallow: /ajax
Disallow: /auth
Disallow: /bitrix
Disallow: /cgi-bin/
Crawl-delay: 5
Host: <?php echo $_SERVER['SERVER_NAME']."\n"; ?>