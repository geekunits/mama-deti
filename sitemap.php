<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$cm = CityManager::getInstance();
$city = $cm->getCurrentCity();
$cityCode = $city['CODE'];
$fileUri = '/sitemaps/' . $cityCode . '/sitemap' . $_GET['file'] . '.xml';
$filePath = $_SERVER['DOCUMENT_ROOT'] . $fileUri;
if (file_exists($filePath)) {
	header('X-Accel-Redirect:' . $fileUri);
} else {
	header('HTTP/1.0 404 Not Found');
}
die();