                    <?php

                    $cm = CityManager::getInstance();
                    $socialUrls = $cm->getCitySocialUrlsByCityId($cm->getCurrentCityId());
                    $currentCity = $cm->GetCurrentCity();
                    $currentCityCode = $currentCity['CODE'];
                    $currentCityName = empty($currentCity['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE']) ? 
                        $currentCity['NAME'] : 
                        'в ' . $currentCity['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'];

                    if ($APPLICATION->GetCurPage(false) != SITE_DIR) {
                        if (!function_exists('__footerBlock')) {
                            function __footerBlock()
                            {
                                global $APPLICATION;

                                ob_start();

                                if ($APPLICATION->GetProperty('SHOW_LEFT_MENU','N') == 'Y' && ERROR_404 != 'Y'):?>
                                        </div>
                                    </div>
                                <?endif?>
                                <?php
                                    if ($APPLICATION->GetProperty('CONTENT_BLOCK','Y') == 'Y'  || ERROR_404 == 'Y') {
                                        echo '</div></div>';
                                    }

                                    $html = ob_get_contents();
                                    ob_end_clean();

                                    return $html;
                            }
                        }

                        $APPLICATION->AddBufferContent('__footerBlock');
                    }
                    ?>
                </div>

                <?php if(ERROR_404 != 'Y'): ?>
                    <?$APPLICATION->IncludeComponent(
                        "mamadeti:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "sect",
                            "AREA_FILE_SUFFIX" => $APPLICATION->GetCurDir() === '/' ? "main_bottom_inc" : "bottom_inc",
                            "AREA_FILE_RECURSIVE" => "Y",
                            "EDIT_TEMPLATE" => "",
                            "CITY_ID" => GetCurrentCity(),
                        ),
                    false
                    );?>
                <?php endif; ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="footer">
            <div class="l-width">
                <div class="footer__social">
                    <noindex>
                    <?php if ($currentCity === $cm->getDefaultCity()): ?>
                        Присоединяйтесь
                        <?php else: ?>
                        Группа компаний &laquo;Мать и дитя&raquo;
                        <?php endif; ?>&nbsp;<!--
                     --><a target="_blank" href="http://vk.com/gc.motherandchild" rel="nofollow" class="in-bl social-link social-link--vk"></a><!--
                     --><a target="_blank" href="http://facebook.com/gc.motherandchild" rel="nofollow" class="in-bl social-link social-link--fb"></a><!--
                     --><a target="_blank" href="https://instagram.com/mamadeti_club/" rel="nofollow" class="in-bl social-link social-link--inst"></a><!--
                     --><?php/*<a target="_blank" href="http://www.youtube.com/channel/UCGz14xsIW0mx2IpmWznRV6w" rel="nofollow" class="in-bl social-link social-link--youtube"></a>*/?> 
                    </noindex>
                    <?php if (!empty($socialUrls)): ?>
                        <noindex>
                            &nbsp;&nbsp;&nbsp;&nbsp;&laquo;Мать и дитя&raquo; <?php echo $currentCityName; ?>&nbsp;
                            <?php foreach($socialUrls as $socialUrl): 
                                ?><a target="_blank" href="<?php echo $socialUrl->url; ?>" rel="nofollow" class="in-bl social-link social-link--<?php echo $socialUrl->name; ?>"></a><?php
                            endforeach; ?>
                        </noindex>
                    <?php endif; ?>
                </div>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                    "ROOT_MENU_TYPE" => "bottom",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => "",
                    ),
                    false
                );?>
            </div>
            <div class="footer__info">
                <div class="l-width">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="205" class="footer__logo">
                                <a href="/" class="footer__logo-pic">мать и дитя</a>
                            </td>
                            <td width="405" class="footer__subscribe">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:subscribe.form",
                                    "footer",
                                    Array(
                                        "USE_PERSONALIZATION" => "Y",
                                        "PAGE" => "#SITE_DIR#about/subscr_edit.php",
                                        "SHOW_HIDDEN" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "3600",
                                        "CACHE_NOTES" => ""
                                    ),
                                false
                                );?>
                            </td>
                            <td class="footer__phone">
                                <?php $APPLICATION->IncludeFile('/includes/footer/cityPhones.php'); ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="footer__copy">
                <div class="l-width">
                    <a href="http://<?php echo $cm->getDomainByCityId($cm->getCurrentCityId(), true) . $APPLICATION->GetCurPage(); ?>" class="footer__mobile">Мобильная версия</a>
                    Мать и Дитя  &copy;<?php echo date('Y'); ?>. Все права защищены. ООО «ХАВЕН»
                </div>
            </div>
            <div class="b-line_topbottom"></div>
        </div>
    </div>

    <div class="scroll-top js-scroll-top"></div>

    <div class="js-popups">
        <div class="popup-holder active" id="callback-popup" style="display:none;"></div>
        <div class="popup-holder active" id="zapisnapriem-popup" style="display:none;"></div>
        <div class="popup-holder active" id="ask-popup" style="display:none;"></div>
        <div class="popup-holder active" id="open-day-popup" style="display:none;"></div>
        <div class="popup-holder active" id="open-day-spb" style="display:none;"></div>
        <div class="popup-holder active" id="fourteen-may-competition-popup" style="display:none;"></div>
        <div class="popup-holder active" id="fourteen-may-event-popup" style="display:none;"></div>
        <div class="popup-holder active" id="form-eko-popup" style="display:none;"></div>
        <div class="popup-holder active" id="form-cikl-popup" style="display:none;"></div>
        <div class="popup-holder active" id="custom-request-popup" style="display:none;"></div>
        <div class="popup-holder active" id="form-register-conf" style="display:none;"></div>
    </div>

    <noindex>
    <?php if (IS_MOBILE && !IS_TABLET) { ?>
        <?php if (count($GLOBALS["CITY_LIST"][GetCurrentCity()]["PROPERTIES"]["PHONE"]["VALUE"]) == 1) { ?>
            <?php $phone = strip_tags(html_entity_decode($GLOBALS["CITY_LIST"][GetCurrentCity()]["PROPERTIES"]["PHONE"]["VALUE"][0])); ?>
            <a href="tel:<?php echo preg_replace('/[^\d\+]+/', '', $phone); ?>" class="mobile-panel-phone" id="PhoneСall">
                Связаться с нами <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/ico-phone-panel.png" class="in-bl" width="150" alt="">
            </a>
        <?php } else { ?>
            <div class="mobile-panel-phone" id="PhoneСall">
                <div class="mobile-panel-overlay"></div>
                <div class="mobile-panel-phone-select">
                <?php $arPhones = $GLOBALS["CITY_LIST"][GetCurrentCity()]["PROPERTIES"]["PHONE"]; ?>
                <?php foreach ($arPhones["VALUE"] as $key => $item) { ?>
                    <?php $phone = strip_tags(html_entity_decode($item)); ?>
                    <a href="tel:<?php echo preg_replace('/[^\d\+]+/', '', $phone); ?>"><?php echo $arPhones["DESCRIPTION"][$key];?></a>
                <?php } ?>
                </div>
                <div class="mobile-panel-click">Связаться с нами <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/ico-phone-panel.png" class="in-bl" width="150" alt=""></div>
            </div>
        <?php } ?>
    <?php } ?>
    
    <?php PageManager::getInstance()->renderVariables(); ?>

    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.maskedinput.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.fancybox.pack.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.fancybox-media.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.placeholder.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/social-likes.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.formstyler.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.tooltipster.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script type="text/javascript">
        CITY_COOKIE_NAME = '<?php echo CITY_COOKIE_NAME; ?>';
    </script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/customTabs.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.bxslider.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.dotdotdot.min.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.cookie.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/main.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/slider.js?<?php echo ASSET_VERSION; ?>"></script>
    <!--[if lte IE 8]><script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/ie.js?<?php echo ASSET_VERSION; ?>"></script><![endif]-->
    <!--[if lte IE 9]><script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/pie.js?<?php echo ASSET_VERSION; ?>"></script><![endif]-->

    <?php if (EnvManager::getInstance()->isProd()): ?>
        <?php
            $metrikaDirectory = $_SERVER['DOCUMENT_ROOT'] . '/share/includes/metrika/';
            include($metrikaDirectory . 'default_metrika.php');
            $metrikaFile = $currentCityCode . '_metrika.php';
            if (file_exists($metrikaDirectory . $metrikaFile)) {
                include($metrikaDirectory . $metrikaFile);
            }
            
        ?>
    <?php endif; ?>
    </noindex>
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 957598683;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "FzupCJeFw1wQ25fPyAM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <div style="position:absolute; left:-9999px; bottom:0;">
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    </div>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/957598683/?label=FzupCJeFw1wQ25fPyAM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>
</body>
</html>
