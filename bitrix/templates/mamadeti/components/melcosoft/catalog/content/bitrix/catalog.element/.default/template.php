<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php if ($arResult['ID'] != VACANCIES_ELEMENT_ID || strpos($APPLICATION->GetCurDir(), '/vacancy/') === false): ?>
    <?if ($arResult["DETAIL_TEXT"]):?>
        <?=$arResult["DETAIL_TEXT"]?>
    <?else:?>
        <?=$arResult["PREVIEW_TEXT"]?>
    <?endif?>
<?php endif; ?>
