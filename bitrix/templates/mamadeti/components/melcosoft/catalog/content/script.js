function submitForm()
{
	var reviewsForm = BX('REVIEWS_FORM');

	BX.ajax.submitComponentForm(reviewsForm, 'reviews_form_content', true);
	BX.submit(reviewsForm);

	return true;
}

function refreshForm()
{
	console.log('setform');
	$('form#REVIEWS_FORM select').change(function()
	{
		submitForm();
	}
	);
}

BX.addCustomEvent("onAjaxSuccess", refreshForm);

$(function(){
	refreshForm();
});
