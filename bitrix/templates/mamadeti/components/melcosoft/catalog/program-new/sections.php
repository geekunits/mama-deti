<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->IncludeFile(
                        "include/content".$arParams["SEF_FOLDER"]."index.php",
                        Array(),
                        Array("MODE"=>"html","NAME"=>"текст","TEMPLATE"=>"clean.php")
                    );?>
<?php
$ajax_filter = isset($_REQUEST["ajax_filter"]) && $_REQUEST["ajax_filter"] == "y" && (!isset($_REQUEST['PAGEN_2']) || isset($_REQUEST['HTTP_X_REQUESTED_WITH']));

if ($ajax_filter)
    $APPLICATION->RestartBuffer();

$arrFilter = & $GLOBALS[$arParams['~FILTER_NAME']];
if (!is_array($arrFilter))
    $arrFilter = array();

$CITY_ID = GetCurrentCity();

$arRegionClinic = CMamaDetiAPI::getClinicsID(array("PROPERTY_CITY"=>$CITY_ID));

CModule::IncludeModule("iblock");

if ($arrFilter['PROPERTY_CLINIC']) {
    $selClinic = $arrFilter['PROPERTY_CLINIC'];
} else {
    $selClinic = isset($_REQUEST["clinic"]) ? intval($_REQUEST["clinic"]) : -1;
}

$selDirection = isset($_REQUEST["direction"]) ? intval($_REQUEST["direction"]) : -1;

if (($selClinic>0 && $selDirection>0) || ($selClinic == -1 && $selDirection == -1)) {
    $sort = "sort";
    $sort_order = "asc";
} elseif ($selClinic>0) {
    $sort = "PROPERTY_DIRECTION.NAME";
    $osrt_order = "asc";
} else {
    $sort = "PROPERTY_CLINIC.NAME";
    $osrt_order = "asc";
}

/*********************************/

$arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y","PROPERTY_CLINIC"=>$arRegionClinic);
if ($selDirection>0)
    $arFilter["PROPERTY_DIRECTION"] = $selDirection;

$arClinic = array();

$rsElement = CIBlockElement::GetList(array("PROPERTY_CLINIC.NAME"=>"asc"),$arFilter,array("PROPERTY_CLINIC.ID","PROPERTY_CLINIC.NAME"));
while ($arElement = $rsElement->GetNext()) {
    $arClinic[$arElement["PROPERTY_CLINIC_ID"]] = $arElement["PROPERTY_CLINIC_NAME"];
}

/*********************************/

$arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y");
if ($selClinic>0)
    $arFilter["PROPERTY_CLINIC"] = $selClinic;
else
    $arFilter["PROPERTY_CLINIC"] = $arRegionClinic;

$arDirection = array();

$rsElement = CIBlockElement::GetList(array("PROPERTY_DIRECTION.NAME"=>"asc"),$arFilter,array("PROPERTY_DIRECTION.ID","PROPERTY_DIRECTION.NAME"));
while ($arElement = $rsElement->GetNext()) {
    $arDirection[$arElement["PROPERTY_DIRECTION_ID"]] = $arElement["PROPERTY_DIRECTION_NAME"];
}

if ($selClinic>0)
    $arrFilter["PROPERTY_CLINIC"] = $selClinic;
else
    $arrFilter["PROPERTY_CLINIC"] = $arRegionClinic;

if ($selDirection>0)
    $arrFilter["PROPERTY_DIRECTION"] = $selDirection;
?>

<?php if ($ajax_filter): ?>
<?php
    $jsClinic = array();
    foreach ($arClinic as $id => $name)
        $jsClinic[] = array("ID" => $id, "NAME" => htmlspecialcharsbx($name));

    $jsDirection = array();
    foreach ($arDirection as $id => $name)
        $jsDirection[] = array("ID" => $id, "NAME" => htmlspecialcharsbx($name));
?>
    <?php
        $filterUrl = $APPLICATION->GetCurDir();
        $params = array_filter(array('clinic' => $selClinic, 'direction' => $selDirection), function ($v) { return $v > 0; });
        if ($params) {
            $filterUrl .= '?'.http_build_query($params);
        }
    ?>
    <div class="ajax_script">
        <script>
            var curClinic = <?=CUtil::PhpToJSObject($selClinic)?>;
            var arClinic = <?=CUtil::PhpToJSObject($jsClinic)?>;
            var curDirection = <?=CUtil::PhpToJSObject($selDirection)?>;
            var arDirection = <?=CUtil::PhpToJSObject($jsDirection)?>;

            if (location.pathname.indexOf('/program/') === 0) {
                document.title = '<?=CUtil::JSEscape($APPLICATION->GetTitle())?>';
                window.history.pushState(
                    'page2',
                    '<?=CUtil::JSEscape($APPLICATION->GetTitle())?>',
                    '<?=CUtil::JSEscape($filterUrl)?>');
            }

            var selClinic = $('select[name="clinic"]');

            selClinic.find('option:not(:first)').remove();
            $(arClinic).each(function () {
                if (this.ID == curClinic)
                    selClinic.append('<option value="' + this.ID + '" selected="selected">' + this.NAME + '</option>'); else
                    selClinic.append('<option value="' + this.ID + '">' + this.NAME + '</option>');
            });
            selClinic.trigger('refresh');

            var selDirection = $('select[name="direction"]');

            selDirection.find('option:not(:first)').remove();
            $(arDirection).each(function () {
                if (this.ID == curDirection)
                    selDirection.append('<option value="' + this.ID + '" selected="selected">' + this.NAME + '</option>'); else
                    selDirection.append('<option value="' + this.ID + '">' + this.NAME + '</option>');
            });
            selDirection.trigger('refresh');

    </script>
    </div>
<?else:?>
    <?$dir = split('/', $APPLICATION->GetCurDir()); $dir = $dir[count($dir)-2];?>
    <?if($dir !== 'children-s-center'):?>
    <?//var_dump($arParams,$arResult,$arrFilter);?>
    <form id="search-filter" action="<?=$APPLICATION->GetCurPageParam()?>">
        <input type="hidden" name="ajax_filter" value="y">
        <?php if (!is_numeric($GLOBALS[$arParams['~FILTER_NAME']]['PROPERTY_CLINIC'])): ?>
            Выберите клинику:
            <select name="clinic">
                <option value="-1">Все клиники</option>
            <?foreach ($arClinic as $id=>$name):?>
                <option value="<?=$id?>"<?if ($id==$selClinic):?> selected="selected"<?endif?>><?=$name?></option>
            <?endforeach?>
            </select>
        <?php else: ?>
            <input type="hidden" name="clinic" value="<?php echo htmlspecialchars($arrFilter['PROPERTY_CLINIC']); ?>">
        <?php endif; ?>

        Выберите направление:
        <select name="direction">
            <option value="-1">Все направления</option>
        <?foreach ($arDirection as $id=>$name):?>
            <option value="<?=$id?>"<?if ($id==$selDirection):?> selected="selected"<?endif?>><?=$name?></option>
        <?endforeach?>
        </select>
    </form>
    <script>
            var form = $('#search-filter').submit(
                function (e) {

                    BX.showWait(BX('search-filter'));

                    $.ajax({url: location.href, dataType: 'html', type: 'GET', data: $(this).serialize()}).done(function (data) {
                        data = '<div>' + data + '</div>';
                        $('.ajax_section:visible').html($(data).find('.ajax_section'));

                        eval($(data).find(".ajax_script script").html());

                    }).always(function () {
                        BX.closeWait(BX('search-filter'));
                    });

                    return false;
                }
            );

            form.find('input[type=checkbox], select').change(function () {
                //form.submit();
                $(this).closest('form').submit();
            });

    </script>
    <?endif;?>
<?endif?>

<div class="ajax_section">

<?php
global $programFilter;
$programFilter = $arrFilter;
if ($dir == 'children-s-center') {
    $servicesId = array();
    $els = CIBlockElement::GetList(array(),array(
        'IBLOCK_ID' => 1,
        'SECTION_CODE' => $dir
    ),false,false,array('ID','IBLOCK_ID'));
    while ($el = $els->GetNext()) {
        $services[] = $el['ID'];
    }
    $programFilter['=PROPERTY_SERVICES'] = $services;
}
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "",
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_SORT_FIELD" => $sort,
        "ELEMENT_SORT_ORDER" => $sort_order,
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
        "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
        "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
        "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
        "SHOW_ALL_WO_SECTION" => "Y",
        "BASKET_URL" => $arParams["BASKET_URL"],
        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
        //"FILTER_NAME" => $arParams["FILTER_NAME"],
        "FILTER_NAME" => 'programFilter',
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_STATUS_404" => 'N',
        "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
        "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
        "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
        "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
        "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        '_REQUEST' => $_REQUEST,
    ),
    $component
);
?>
</div>
<?if ($ajax_filter) die();?>
