<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$arResult["SECTION"]["PATH"][] = array(
    "NAME" => $arResult["NAME"]
    );

CModule::IncludeModule("currency");

$arResult["CLINIC"] = array();
	if (is_array($arResult["PROPERTIES"]["CLINIC"]["VALUE"]) && !empty($arResult["PROPERTIES"]["CLINIC"]["VALUE"]))
	{
		$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arResult["PROPERTIES"]["CLINIC"]["LINK_IBLOCK_ID"],"ID"=>$arResult["PROPERTIES"]["CLINIC"]["VALUE"]), false, false, array('IBLOCK_ID', 'ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_CITY'));
		while ($arElement = $rsElement->GetNext()) {
			if ($GLOBALS['CITY_ID'] == $arElement['PROPERTY_CITY_VALUE']) {
				$arResult["CLINIC"][] = $arElement;
			}
		}
	}

if (is_array($arResult["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SERVICES"]["VALUE"]))
{
    $arFilter = array(
        "IBLOCK_ID"=>$arResult["PROPERTIES"]["SERVICES"]["LINK_IBLOCK_ID"],
        "ACTIVE"=>"Y",
        "ID" => array_values($arResult["PROPERTIES"]["SERVICES"]["VALUE"]),
        );

    $arTable = array(0,0,0);
    $row = 0;
    $arResult["PROPERTIES"]["SERVICES"]["VALUE"] = array();

    $rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL"));
    while ($arElement = $rsElement->GetNext())
    {
        $arTable[$row++]++;
        if ($row == 3) $row = 0;
        $arResult["PROPERTIES"]["SERVICES"]["VALUE"][] = $arElement;
    }

    $arResult["PROPERTIES"]["SERVICES_TABLE"] = $arTable;
}

if ($arResult["PROPERTIES"]["PRICE"]["VALUE"])
    $arResult["PROPERTIES"]["PRICE"]["VALUE"] = CCurrencyLang::CurrencyFormat($arResult["PROPERTIES"]["PRICE"]["VALUE"], "RUB", true);

$APPLICATION->SetPageProperty('title', $arResult['NAME']);