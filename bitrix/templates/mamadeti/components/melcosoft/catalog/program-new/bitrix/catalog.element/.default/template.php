<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="program-detail">
	<h3 class="sub-title"><?php echo $arResult['NAME']; ?></h3>

	<ul class="info-list">
	<?php if ($arResult['CLINIC']): ?>
	<?php
		$clinicList = array();
	foreach ($arResult['CLINIC'] as $arClinic) {
		$clinicList[] = '<a href="'.$arClinic['DETAIL_PAGE_URL'].'">'.$arClinic['NAME'].'</a>';
	}
	?>
	<li><strong class="title">Клиника:</strong> <?php echo implode(', ', $clinicList); ?></li>
	<?php endif; ?>
				<?if (is_array($arResult["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SERVICES"]["VALUE"])):?>
					<li>
							<strong class="title">Услуги:</strong>
					</li>
					<li>
					<?$rowCount = 0;?>
					<?foreach ($arResult["PROPERTIES"]["SERVICES"]["VALUE"] as $arValue):?>
					<?
					if ($rowCount == 0)
					{
						$rowCount = array_shift($arResult["PROPERTIES"]["SERVICES_TABLE"]);
						echo '<ul>';
					}
					?>
					<li>
							<a href="<?=$arValue["DETAIL_PAGE_URL"]?>"><?=$arValue["NAME"]?></a>
					</li>
					<?
					if (--$rowCount == 0)
						echo '</ul>';
					?>
					
					<?endforeach?>
					</li>
					<li>&nbsp;</li>
				<?endif?>
	<li class="list-item-price">
	<?if ($arResult["PROPERTIES"]["PRICE"]["VALUE"]):?>
	<strong class="title">Стоимость:</strong> <span class="val"><?=$arResult["PROPERTIES"]["PRICE"]["VALUE"]?></span>
	<?endif?>
	</li>
</ul>
<br/>
<?if ($arResult["DETAIL_TEXT"]):?>
	<?=$arResult["DETAIL_TEXT"]?>
<?else:?>
	<?=$arResult["PREVIEW_TEXT"]?>
<?endif?>
</div>

<?php if (preg_match('/\/program\/(\?|$)/', $_SERVER['HTTP_REFERER'])): ?>
	<p style="clear: both;"><a class="qa-view" href="<?php echo htmlspecialchars($_SERVER['HTTP_REFERER']); ?>">Возврат к списку</a></p>
<?php endif; ?>