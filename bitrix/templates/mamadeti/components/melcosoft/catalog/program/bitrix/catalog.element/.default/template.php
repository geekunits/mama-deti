<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="program-detail">
		<h3 class="sub-title">
			<?php echo $arResult['NAME']; ?>
			<?php
				if ($arResult["DISPLAY_PROPERTIES"]["TYPE"]){
					$xmlId = $arResult["DISPLAY_PROPERTIES"]["TYPE"]["VALUE_XML_ID"];

					if(isset(ProgramHelper::$types[$xmlId])){
						$type = ProgramHelper::$types[$xmlId];
						?>
							<div class="programm-label programm-label--<? echo $xmlId; ?> in-bl" data-type="<? echo $xmlId; ?>"><? echo $type['label']; ?>
								<div class="programm-label__popup"><? echo $type['description']; ?></div>
							</div>
						<?php
					}
				}
			?>
		</h3>
		<?php if ($arResult['CLINIC']): ?>
		<?php
			$clinicList = array();
		foreach ($arResult['CLINIC'] as $arClinic) {
			$clinicList[] = '<a href="'.$arClinic['DETAIL_PAGE_URL'].'">'.$arClinic['NAME'].'</a>';
		}
		?>
		<p><strong class="title">Клиника:</strong> <?php echo implode(', ', $clinicList); ?></p>
		<?php endif; ?>

		<p class="list-item-price">
		<?if ($arResult["PROPERTIES"]["PRICE"]["VALUE"]):?>
		<strong class="title">Стоимость:</strong> <span class="val"><?=$arResult["PROPERTIES"]["PRICE"]["VALUE"]?></span>
		<?endif?>
		</p>
	<?if ($arResult["DETAIL_TEXT"]):?>
		<?=$arResult["DETAIL_TEXT"]?>
	<?else:?>
		<?=$arResult["PREVIEW_TEXT"]?>
	<?endif?>
	<?if (is_array($arResult["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SERVICES"]["VALUE"])):?>
	<br/>
	<strong class="title">Услуги:</strong>
	<ul class="info-list">
		<?foreach ($arResult["PROPERTIES"]["SERVICES"]["VALUE"] as $arValue):?>
		<li>
				<a href="<?=$arValue["DETAIL_PAGE_URL"]?>"><?=$arValue["NAME"]?></a>
		</li>
		<?endforeach?>
	</ul>
	<?endif?>
</div>

<?php if (preg_match('/\/program\/(\?|$)/', $_SERVER['HTTP_REFERER'])): ?>
	<p style="clear: both;"><a class="qa-view" href="<?php echo htmlspecialchars($_SERVER['HTTP_REFERER']); ?>">Возврат к списку</a></p>
<?php endif; ?>