<?php 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$clinicId = !empty($arResult["PROPERTIES"]["CLINIC"]["VALUE"]) ? $arResult["PROPERTIES"]["CLINIC"]["VALUE"][0] : null;

if (!empty($clinicId)) {
	$formatter = new ClinicResultFormatter($clinicId);
	$clinicMenu = new ClinicMenu($formatter->format(), [
	    'useAjax' => false,
	    'detectCurrentTab' => false,
	    'currentTabName' => 'programs',
	]);
	$APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());
}
