<?

foreach($arResult["ITEMS"] as &$arItem)
{
	$arItem["CLINIC"] = array();
	if (is_array($arItem["PROPERTIES"]["CLINIC"]["VALUE"]) && !empty($arItem["PROPERTIES"]["CLINIC"]["VALUE"]))
	{
		$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arItem["PROPERTIES"]["CLINIC"]["LINK_IBLOCK_ID"],"ID"=>$arItem["PROPERTIES"]["CLINIC"]["VALUE"]), false, false, array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL', 'PROPERTY_CITY'));
		while ($arElement = $rsElement->GetNext())
			$arItem["CLINIC"][] = $arElement;
	}

	$arItem["DIRECTION"] = array();
	if (is_array($arItem["PROPERTIES"]["DIRECTION"]["VALUE"]) && !empty($arItem["PROPERTIES"]["DIRECTION"]["VALUE"]))
	{
		$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arItem["PROPERTIES"]["DIRECTION"]["LINK_IBLOCK_ID"],"ID"=>$arItem["PROPERTIES"]["DIRECTION"]["VALUE"]));
		while ($arElement = $rsElement->GetNext())
			$arItem["DIRECTION"][] = $arElement;
	}
}

if (isset($arItem))
	unset($arItem);

?>