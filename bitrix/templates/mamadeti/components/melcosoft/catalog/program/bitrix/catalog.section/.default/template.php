<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<ul class="workers-list programm">
    <?
    $prevSecTitle="";
    foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

        switch ($arParams["ELEMENT_SORT_FIELD"])
        {
            case 'PROPERTY_CLINIC.NAME': $curSecTitle = $arItem["PROPERTY_CLINIC_NAME"]; break;
            case 'PROPERTY_DIRECTION.NAME': $curSecTitle = $arItem["PROPERTY_DIRECTION_NAME"]; break;
            default: $curSecTitle=''; break;
        }

        if ($prevSecTitle != $curSecTitle)
        {
            echo '<li><h2 class="name-title red-color">'.$curSecTitle."</h2></li>";
            $prevSecTitle = $curSecTitle;
        }

        ?>
        <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <h2 class="name-title red-color">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                <?
                    if ($arItem["DISPLAY_PROPERTIES"]["TYPE"]){
                        $xmlId = $arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE_XML_ID"];
    
                        if(isset(ProgramHelper::$types[$xmlId])){
                            $type = ProgramHelper::$types[$xmlId];
                            ?>
                            <div class="programm-label programm-label--<? echo $xmlId; ?> in-bl"><? echo $type['label']; ?>
                                <div class="programm-label__popup"><? echo $type['description']; ?></div>
                            </div>
                            <?php
                        }
                    }
                ?>
            </h2>
            <?=$arItem["PREVIEW_TEXT"]?>
            <?
            $ar = array();
            foreach ($arItem["CLINIC"] as $arClinic) {
                if ($arClinic['PROPERTY_CITY_VALUE'] == $GLOBALS['CITY_ID']) {
                    $ar[] = '<a href="'.$arClinic["DETAIL_PAGE_URL"].'">'.$arClinic["NAME"].'</a>';
                }
            }
            ?>
            Клиника: <?=implode(',&nbsp;',$ar)?>
            <br>
            <?
            $ar = array();
            foreach ($arItem["DIRECTION"] as $arDirection) {
                $ar[] = $arDirection["NAME"];
            }
            ?>
            Направление: <?=implode(',&nbsp;',$ar)?>
            <br>
        </li>
    <?endforeach;?>
</ul>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
