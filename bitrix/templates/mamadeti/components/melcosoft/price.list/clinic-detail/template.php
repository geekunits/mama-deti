<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?php if (count($arResult["ITEMS"])): ?>
<?php
    $prevTopSectionId = null;
    $prevSubSectionId = null;
?>
<div class="b-tree_links__wrap b-tree_price__wrap">
    <?php foreach($arResult['ITEMS'] as $arItem):  ?>
        <?php
            $arSectionNav = $arItem['SECTION_NAV'];
            if (count($arSectionNav) === 2) {
                list($topSectionId, $subSectionId) = $arSectionNav;
                $arSubSection = $arResult['SECTIONS'][$subSectionId];
            } else {
                $topSectionId = array_pop($arSectionNav);
                $subSectionId = null;
            }
            $arTopSection = $arResult['SECTIONS'][$topSectionId];
        ?>
        <?php if ($prevTopSectionId !== $topSectionId): ?>
            <?php if ($prevTopSectionId !== null): ?>
                </table></div></div>
            <?php endif; ?>
            <div class="b-tree_link__item">
                <div class="b-tree_link js-tree-click">
                    <div class="b-tree_link__plus">
                        <div class="js-tree-plus">+</div>
                    </div>
                    <?php echo upperCaseFirst(mb_strtolower($arTopSection['NAME'])); ?>
                </div>
                <div class="b-tree_links__sub">
                <?php if ($subSectionId): ?>
                    <strong><?php echo upperCaseFirst(mb_strtolower($arSubSection['NAME'])); ?></strong>
                <?php endif; ?>
                <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
        <?php elseif ($prevSubSectionId !== $subSectionId): ?>
            </table>
            <?php if ($subSectionId): ?>
                <strong><?php echo upperCaseFirst(mb_strtolower($arSubSection['NAME'])); ?></strong>
            <?php endif; ?>
            <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
        <?php endif; ?>
        <tr>
            <td class="b-price_dot">
                <span class="b-price_bg"><?php echo $arItem['NAME']; ?></span>
            </td>
            <td class="b-price_dot b-price_cost">
                <span class="b-price_bg"><?php echo number_format($arItem['PROPERTY_PRICE_VALUE'], 0, ',', ' ')?> р</span>
            </td>
        </tr>
        <?php
            $prevTopSectionId = $topSectionId;
            $prevSubSectionId = $subSectionId;
        ?>
    <?php endforeach; ?>
    </table>
    </div>
</div>
<?php else: ?>
    <b>По данному запросу услуг не найдено</b>
<?php endif; ?>
<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <br /><?php echo $arResult["NAV_STRING"]; ?>
<?php endif; ?>
