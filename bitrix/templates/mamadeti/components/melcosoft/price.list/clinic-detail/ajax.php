<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$arJSResult = array();
$arJSResult["ITEMS"] = array();

foreach ($arResult["ITEMS"] as $arItem)
{
	if (!in_array($arItem["NAME"], $arJSResult["ITEMS"]))
	{
		$arJSResult["ITEMS"][] = $arItem["NAME"];
		if (count($arJSResult["ITEMS"])>=10)
			break;
	}
}

echo json_encode($arJSResult);

?>