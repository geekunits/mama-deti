<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "SHOW_CLINIC_NAMES" => array(
        "PARENT" => "VISUAL",
        "NAME" => "Показывать названия клиник",
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N"
    ),
    "SHOW_MORE_BUTTON" => array(
        "PARENT" => "VISUAL",
        "NAME" => "Показывать кнопку Показать все",
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N"
    ),
    "SHOW_PRINT_BUTTON" => array(
        "PARENT" => "VISUAL",
        "NAME" => "Показывать кнопку Распечатать",
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N"
    ),
    "SHOW_EXPANDED" => array(
        "PARENT" => "VISUAL",
        "NAME" => "Показывать все списки раскрытыми",
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N"
    )
);
