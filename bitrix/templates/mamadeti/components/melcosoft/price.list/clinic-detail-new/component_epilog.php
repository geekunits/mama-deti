<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$currentClinicName = null; 
$oldClinic = null;
global $priceListClinicId;
global $APPLICATION;
if (!empty($priceListClinicId)) {
    $formatter = new ClinicResultFormatter($priceListClinicId);
    $clinicMenu = new ClinicMenu($formatter->format(), [
        'useAjax' => false,
        'detectCurrentTab' => false,
        'currentTabName' => 'pricelist',
    ]);
    $APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());
}