<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php 
    $currentClinicName = null; 
    $oldClinic = null;
    global $APPLICATION;

?>
<?php if (count($arResult["ITEMS"])): ?>
    <?php

        $arResult['ITEMS'] = CMamaDetiAPI::resortPriceList($arResult['ITEMS'], $arResult['SECTIONS']);
        $prevTopSectionId = null;
        $prevSubSectionId = null;

        $showExpanded = $arParams['SHOW_EXPANDED'] == 'Y';
    ?>
    <?php if ($arParams['SHOW_PRINT_BUTTON'] == 'Y'): ?>
        <a href="#" class="price-list-print js-print-page"><span class="price-list-print__ico"></span>Распечатать</a>
    <?php endif; ?>
    <div class="b-tree_links__wrap b-tree_price__wrap">
        <?php foreach($arResult['ITEMS'] as $arItem):  ?>
            <?php
                $showClinicName = false;

                $clinic = $arResult['CLINICS'][$arItem['CLINIC_SECTION']];
/*
                if(!$clinic){
                    echo '<!--113322' . print_r($arItem['SECTION_NAV'], true) . '-->';
                }
*/
                if ($clinic && $clinic['CLINIC']['NAME'] !== $currentClinicName) {
                    $oldClinic = $clinic;
                    $currentClinicName = $clinic['CLINIC']['NAME'];
                    $showClinicName = $arParams['SHOW_CLINIC_NAMES'] == 'Y';
                }
            ?>
            <?php
                $arSectionNav = $arItem['SECTION_NAV'];
                if (count($arSectionNav) === 2) {
                    list($topSectionId, $subSectionId) = $arSectionNav;
                    $arSubSection = $arResult['SECTIONS'][$subSectionId];
                } else {
                    $topSectionId = array_pop($arSectionNav);
                    $subSectionId = null;
                }
                $arTopSection = $arResult['SECTIONS'][$topSectionId];
            ?>
            <?php if ($prevTopSectionId !== $topSectionId): ?>
                <?php if ($prevTopSectionId !== null): ?>
                    </table></div></div>
                    <?php if ($arParams['SHOW_MORE_BUTTON'] && $showClinicName): ?>
                        <a class="b-link_more in-bl" href="/price-list2/<?php echo $oldClinic['CLINIC']['CODE']; ?>/price/">Посмотреть весь прайс</a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($showClinicName): ?>
                    <h2 class="name-title"><a href="/price-list2/<?php echo $clinic['CLINIC']['CODE']; ?>/price/"><?php echo $currentClinicName; ?></a></h2>
                <?php endif; ?>
                <div class="b-tree_link__item">
                    <div class="b-tree_link js-price-click<?php if ($showExpanded): ?> b-tree_link__plus-active<?php endif; ?>">
                        <div class="b-tree_link__plus">
                            <div class="js-tree-plus"><?php if ($showExpanded): ?>&ndash;<?php else: ?>+<?php endif; ?></div>
                        </div>
                        <?php echo upperCaseFirst(mb_strtolower($arTopSection['NAME'])); ?>
                    </div>
                    <div class="b-tree_links__sub" <?php if ($showExpanded): ?>style="display: block;"<?php endif; ?>>
                        <?php if ($subSectionId): ?>
                            <strong><?php echo upperCaseFirst(mb_strtolower($arSubSection['NAME'])); ?></strong>
                        <?php endif; ?>
                        <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
            <?php elseif ($prevSubSectionId !== $subSectionId): ?>
                </table>
                <?php if ($subSectionId): ?>
                    <strong><?php echo upperCaseFirst(mb_strtolower($arSubSection['NAME'])); ?></strong>
                <?php endif; ?>
                <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
            <?php endif; ?>
            <tr>
                <td class="b-price_dot">
                    <span class="b-price_bg"><?php echo $arItem['NAME']; ?></span>
                </td>
                <td class="b-price_dot b-price_cost">
                    <span class="b-price_bg"><?php echo number_format($arItem['PROPERTY_PRICE_VALUE'], 0, ',', ' ')?> р</span>
                </td>
            </tr>
            <?php
                $prevTopSectionId = $topSectionId;
                $prevSubSectionId = $subSectionId;
            ?>
        <?php endforeach; ?>
        </table>
        </div>
    </div>

    <?php if ($arParams['SHOW_MORE_BUTTON']): ?>
        <br>
        <a class="b-link_more in-bl" href="/price-list2/<?php echo $oldClinic['CLINIC']['CODE']; ?>/price/">Посмотреть весь прайс</a>
    <?php endif; ?>
    </div>

<?php else: ?>
    <b>По данному запросу услуг не найдено</b>
<?php endif; ?>
<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <br /><?php echo $arResult["NAV_STRING"]; ?>
<?php endif; ?>
