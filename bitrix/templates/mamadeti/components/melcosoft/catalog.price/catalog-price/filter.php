<?php

$filter_clinic = $_REQUEST["filter_clinic"];
$filter_section = $_REQUEST["filter_section"];

/********КЭШ***********/
$obCache = new CPHPCache();
if ($obCache->InitCache(60 * 60 * 24, "filter-data-".GetCurrentCity(), "/mamadeti/pricefilter")) {
    $vars = $obCache->GetVars();
    $arClinic = $vars[0];
    $arClinicSection = $vars[1];
} else {
    $obCache->StartDataCache();

    CModule::IncludeModule("iblock");

    $arClinic = array();

    $rsSection = CIBlockSection::GetList(
        array("SORT" => "ASC"),
        array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ACTIVE" => "Y",
            "DEPTH_LEVEL" => 1,
            "UF_CLINIC" => CMamaDetiAPI::GetClinicsID(['PROPERTY_CITY' => GetCurrentCity()])
        ),
        false,
        array("ID", "UF_CLINIC")
    );

    // Если в городе не нашлось ни одной клиники - 
    // достаем все клиники у которых есть цены
    if ($rsSection->SelectedRowsCount() === 0) {
        $rsSection = CIBlockSection::GetList(
            array("SORT" => "ASC"),
            array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "DEPTH_LEVEL" => 1,
                '!UF_CLINIC' => false,
            ),
            false,
            array("ID", "UF_CLINIC")
        );
    }

    while ($arSection = $rsSection->Fetch()) {
        if (empty($arSection["UF_CLINIC"])) {
            continue;
        }

        $arClinic[$arSection["UF_CLINIC"]] = "";
    }


    $dbRes = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_CLINICS_ID"], "ID" => array_keys($arClinic)), false, false, array("ID", "NAME"));
    while ($arRes = $dbRes->Fetch()) {
        $arClinic[$arRes["ID"]] = $arRes["NAME"];
    }

    $arClinicSection = array();

    foreach ($arClinic as $ID => $name) {
        $dbRes = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "UF_CLINIC" => $ID), false, array("ID", "LEFT_MARGIN", "RIGHT_MARGIN"));
        if ($arRes = $dbRes->Fetch()) {
            $arFilter = array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "LEFT_MARGIN" => $arRes["LEFT_MARGIN"] + 1,
                "RIGHT_MARGIN" => $arRes["RIGHT_MARGIN"],
                "DEPTH_LEVEL" => 2,
            );
            $rsSection = CIBlockSection::GetList(array("NAME" => "ASC"), $arFilter, false, array("ID", "NAME"));
            while ($arSection = $rsSection->Fetch()) {
                $arFilter = array(
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "!PROPERTY_PRICE" => false,
                    "SECTION_ID" => $arSection["ID"],
                    "INCLUDE_SUBSECTIONS" => "Y");

                $CNT = CIBlockElement::GetList(array(), $arFilter, array());
                if ($CNT > 0)
                    $arClinicSection[$ID][$arSection["ID"]] = $arSection["NAME"];
            }
        }
    }

    if (defined("BX_COMP_MANAGED_CACHE")) {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache("/mamadeti/pricefilter");
        $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);
        $CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_CLINICS_ID"]);
        $CACHE_MANAGER->EndTagCache();
    }
    $obCache->EndDataCache(array($arClinic,$arClinicSection));
}
/********КЭШ ЗАКОНЧЕН************/

if (!array_key_exists($filter_clinic, $arClinic))
    $filter_clinic = false;

if (count($arClinic) === 1) {
    $filter_clinic = array_keys($arClinic)[0];
}

if (!$filter_clinic) {
    if (array_key_exists($arResult["VARIABLES"]["CLINIC_ID"], $arClinic))
        $filter_clinic = $arResult["VARIABLES"]["CLINIC_ID"];
}

if ($filter_clinic) {
    if (!array_key_exists($filter_section,$arClinicSection[$filter_clinic]))
        $filter_section = false;
} else
    $filter_section = false;

if ($filter_clinic && !$filter_section) {
    if (array_key_exists($arResult["VARIABLES"]["SECTION_ID"],$arClinicSection[$filter_clinic]))
        $filter_section = $arResult["VARIABLES"]["SECTION_ID"];
}

?>
<form id="price-filter" action="<?= $arParams["SEF_FOLDER"] ?>">
    <div class="search-panel search-price-list">
        <fieldset>
            <?php if (count($arClinic) > 1): ?>
                <div class="search-field selectField">
                    <select name="filter_clinic">
                        <option value="">Все клиники</option>
                        <?php foreach ($arClinic as $key => $val): ?>
                            <option
                                value="<?= $key ?>"<?php if ($filter_clinic == $key): ?> selected="selected"<?php endif ?>><?= htmlspecialcharsbx($val) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            <?php else: ?>
                <input type="hidden" name="filter_clinic" value="<?php echo $filter_clinic; ?>" />
            <?php endif; ?>
            <div class="search-field selectField search-filter-section"<?if (!$filter_clinic):?> style="display:none;"<?endif?>>
                <select name="filter_section">
                    <option value="">Все разделы</option>
                    <?php
                    if ($filter_clinic) {
                        foreach ($arClinicSection[$filter_clinic] as $key => $val) {
                            ?>
                            <option
                                value="<?= $key ?>"<?php if ($filter_section == $key): ?> selected="selected"<?php endif ?>><?= htmlspecialcharsbx($val) ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <br>

            <div class="search-field">

                <div class="search-sector active">
                    <div class="search-input">
                        <div class="align-left">
                            <ul class="search-list">
                            </ul>
                        </div>
                        <?php /*<input type="button" class="button06 bt_search_doctor" value="">*/ ?>

                        <div class="text03">
                            <input type="text" class="input_search_text" name="q" placeholder="Название услуги" autocomplete="off"
                                   value="<?=isset($_GET['q']) ? $_GET['q'] : ''?>">
                        </div>
                    </div>
                    <div id="search_result" class="search-drop"
                         style="display: none; margin: 0px 0px 0px -9999px; z-index: auto;">
                        <div class="drop-frame">
                            <div class="scrollpane">
                                <ul class="search-listing"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="button-holder">
                <span class="button02 block-button"><input type="submit">найти</span>
            </div>
        </fieldset>
    </div>
</form>

<script>
    jQuery(document).ready(function ($) {

        var filterClinicSection = <?=json_encode($arClinicSection)?>;
        var selClinicSection = $('.search-filter-section select');
        //console.log(filterClinicSection);

        $('#price-filter').submit(function (e) {
            if (selClinicSection.filter(':visible').val() || $('#price-filter select[name="filter_clinic"]').val() || $('#price-filter input[name="q"]').val()) {
                return;
            }

            e.preventDefault();

            location.href = '/price-list2/';
        });

        $('select[name="filter_clinic"]').change(function () {
            selClinicSection.find('option:not(:first)').remove();
            var clinicSections = filterClinicSection[$(this).val()];
            if (typeof clinicSections === 'undefined') {
                $('.search-filter-section').hide();
            } else {
                $.each(clinicSections, function (key,val) {
                    selClinicSection.append('<option value="' + key + '">' + val + '</option>');
                });
                $('.search-filter-section').show();
            }

            selClinicSection.trigger('refresh');
        });
    });
</script>
