<?php
include($_SERVER["DOCUMENT_ROOT"].$templateFolder.'/filter.php');

/*echo '<pre>';
print_r($arParams);
print_r($arResult);
echo '</pre>';*/

if (isset($arResult["VARIABLES"]["CLINIC_CODE"])) {
    $arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_CLINICS_ID"],"ACTIVE"=>"Y","=CODE"=>$arResult["VARIABLES"]["CLINIC_CODE"]);
} else {
    $arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_CLINICS_ID"],"ACTIVE"=>"Y","=ID"=>$arResult["VARIABLES"]["CLINIC_ID"]);
}

$rsElement = CIBlockElement::GetList(array(),$arFilter,false,false,array("ID","NAME","CODE"));
if (!$arCurClinic = $rsElement->Fetch()) {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");

    return;
}

$APPLICATION->AddCHainItem($arCurClinic["NAME"]);

global $arrFilter;
global $priceListClinicId;
$priceListClinicId = $arResult["VARIABLES"]["CLINIC_ID"];
$arrFilter["!PROPERTY_PRICE"] = false;

?>
<?$APPLICATION->IncludeComponent("melcosoft:price.list", "clinic-detail-new", Array(
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],	// Тип инфоблока
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],	// Инфоблок
    "FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
    "CLINIC_ID" => $arCurClinic["ID"],	// ID Клиники
    "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],	// Количество элементов на странице
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "SEF_FOLDER" => $arParams["SEF_FOLDER"],
    "SHOW_CLINIC_NAMES" => 'Y',
    'SHOW_PRINT_BUTTON' => 'Y',
    'SHOW_EXPANDED' => 'N',
    ),
    $component
);
