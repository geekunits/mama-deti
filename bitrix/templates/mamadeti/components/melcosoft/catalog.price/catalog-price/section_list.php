<?
$ajax = $_REQUEST["ajax_price"] == "y";

if (!$ajax)
	include($_SERVER["DOCUMENT_ROOT"].$templateFolder.'/filter.php');

if (isset($arResult["VARIABLES"]["CLINIC_CODE"]))
{
	$arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_CLINICS_ID"],"=CODE"=>$arResult["VARIABLES"]["CLINIC_CODE"]);
} else
{
	$arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_CLINICS_ID"],"=ID"=>$arResult["VARIABLES"]["CLINIC_ID"]);
}

$rsElement = CIBlockElement::GetList(array(),$arFilter,false,false,array("ID","NAME","CODE"));
if (!$arCurClinic = $rsElement->Fetch())
{
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return;
}

$APPLICATION->AddCHainItem($arCurClinic["NAME"]);

$rsSection = CIBlockSection::GetList(array("SORT"=>"ASC"),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"UF_CLINIC"=>$arCurClinic["ID"]),false,array("ID"));
if (!$arCurSection = $rsSection->Fetch())
{
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return;
}

if ($ajax)
{
	$APPLICATION->RestartBuffer();
}
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"list",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"COUNT_ELEMENTS" => "N",
		"SECTION_ID" => $arCurSection["ID"],
		"TOP_DEPTH" => 1,
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section_price"],
		"SEF_FOLDER" => $arResult["FOLDER"],
		"ADD_SECTIONS_CHAIN" => "N",
		"SECTION_USER_FIELDS" => array("UF_CLINIC"),
	),
	$component
);
?>
<?
if ($ajax)
	{
	die();
	}
?>