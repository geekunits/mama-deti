<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
include($_SERVER["DOCUMENT_ROOT"].$templateFolder.'/filter.php');

$filter_clinic = intval($_REQUEST["filter_clinic"]);
$filter_section = intval($_REQUEST["filter_section"]);

$GLOBALS["arrFilter"]["!PROPERTY_PRICE"] = false;
if ($filter_section>0) {
    $GLOBALS["arrFilter"]["SECTION_ID"] = $filter_section;
    $GLOBALS["arrFilter"]["INCLUDE_SUBSECTIONS"] = "Y";
}

if (isset($_REQUEST["q"]))
    $GLOBALS["arrFilter"]["?NAME"] = $_REQUEST["q"];
?>
<?$APPLICATION->IncludeComponent("melcosoft:price.list", "clinic-detail-new", Array(
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],	// ��� ���������
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],	// ��������
    "FILTER_NAME" => "arrFilter",	// ��� ������� �� ���������� ������� ��� ���������� ���������
    "CLINIC_ID" => (($filter_section<=0 && $filter_clinic>0) ? $filter_clinic : CMamaDetiAPI::GetClinicsID(['PROPERTY_CITY' => GetCurrentCity()])),	// ID �������
    "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],	// ���������� ��������� �� ��������
    "AJAX_MODE" => "N",	// �������� ����� AJAX
    "AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
    "AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
    "AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
    "CACHE_TIME" => $arParams["CACHE_TIME"],
    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    "CACHE_FILTER" => "Y",	// ���������� ��� ������������� �������
    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
    "AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
    "SEF_FOLDER" => $arParams["SEF_FOLDER"],
    "SHOW_CLINIC_NAMES" => 'Y',
    'SHOW_PRINT_BUTTON' => $filter_clinic ? 'Y' : 'N',
    'SHOW_EXPANDED' => $filter_clinic ? 'Y' : 'N',
    ),
    $component
);?>
