jQuery(document).ready(function($){
	$('.js-load-clinic-pricelist').click(function(e)
	{
		e.preventDefault();

        var 
            $this = $(this), 
            isCollapsed = $this.data('collapsed'), 
            $wrapper = $this.closest('.js-clinic-pricelist-wrapper'),
            $pricelist = $wrapper.find('.js-clinic-pricelist');

        $this.data('collapsed', !isCollapsed);

        if (!isCollapsed && !$this.data('loaded')) {
            $this.data('loaded', true);
            $this.find('a').addClass('l-loader');
            $.ajax({
                url:'/ajax/clinic_price.php',
                data:{id:$this.data('id')}
            }).done(function(data){
                $pricelist.html(data);
                $this.find('a').removeClass('l-loader');
            });
        }
	});
});