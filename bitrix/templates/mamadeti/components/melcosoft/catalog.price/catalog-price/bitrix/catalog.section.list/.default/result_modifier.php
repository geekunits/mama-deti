<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php

$rsCities = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 7, 'IBLOCK_TYPE' => 'directories'));
$cities = array();
while ($arCity = $rsCities->GetNext()) {
    $cities[$arCity['ID']] = $arCity['NAME'];
}

foreach ($arResult["SECTIONS"] as $key=>$arSection) {
    if ($arSection["DEPTH_LEVEL"] == 1) {
        //$rsElement = CIBlockElement::GetByID($arSection["UF_CLINIC"]);
        $rsElement = CIBlockElement::GetList(array(), array('ID' => $arSection["UF_CLINIC"], 'IBLOCK_ID' => 2), false, false, array('IBLOCK_ID', 'ID', 'CODE', 'NAME', 'PROPERTY_CITY'));
        if ($arElement = $rsElement->GetNext()) {
            $arResult["SECTIONS"][$key]["CLINIC"] = $arElement;
            $arResult["SECTIONS"][$key]["CITY"] = $cities[$arElement['PROPERTY_CITY_VALUE']];
            $arResult["SECTIONS"][$key]["CITY_ID"] = $arElement['PROPERTY_CITY_VALUE'];
            $arResult["SECTIONS"][$key]["SECTION_PAGE_URL"] = str_replace("#CLINIC_CODE#",$arElement["CODE"],$arResult["SECTIONS"][$key]["SECTION_PAGE_URL"]);
        }
    } else {
        $arFilter = array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ACTIVE" => "Y",
            "!PROPERTY_PRICE" => false,
            "SECTION_ID" => $arSection["ID"],
            "INCLUDE_SUBSECTIONS" => "Y");

        $arResult["SECTIONS"][$key]["ELEMENT_CNT"] = CIBlockElement::GetList(array(),$arFilter,array());
        if ($arResult["SECTIONS"][$key]["ELEMENT_CNT"] == 0)
            unset($arResult["SECTIONS"][$key]);
    }
}

$arResult['SECTIONS'] = array_filter($arResult['SECTIONS'], function ($arSection) {
    return $arSection['CITY_ID'] == GetCurrentCity();
});

/*usort($arResult['SECTIONS'], function ($s1, $s2) {
    return $s1['CITY'] > $s2['CITY'];
});*/
?>
