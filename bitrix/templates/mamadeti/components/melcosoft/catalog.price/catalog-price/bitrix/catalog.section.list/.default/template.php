<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php $oldCity = null; ?>
<?php foreach($arResult["SECTIONS"] as $arSection): ?>
    <?php /*if ($oldCity !== null && $oldCity !== $arSection['CITY']): ?>
        <br>
    <?php endif; ?>
    <?php if ($oldCity === null || $oldCity !== $arSection['CITY']): ?>
        <?php $oldCity = $arSection['CITY']; ?>
        <h2 class="name-title"><?php echo $arSection['CITY']; ?></h2>
    <?php endif;*/ ?>

    <div class="b-tree_links__wrap">
        <div class="b-tree_link__item js-clinic-pricelist-wrapper">
            <div class="b-tree_link js-load-clinic-pricelist js-price-click" data-id="<?=$arSection['CLINIC']['ID']?>">
                <div class="b-tree_link__plus"><div class="js-tree-plus">+</div></div>
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["CLINIC"]["NAME"]?></a>
            </div>
            <div class="b-tree_links__sub b-tree_links__sub-1 js-clinic-pricelist" style="display: none;">
            </div>
        </div>
    </div>
<?endforeach;?>
