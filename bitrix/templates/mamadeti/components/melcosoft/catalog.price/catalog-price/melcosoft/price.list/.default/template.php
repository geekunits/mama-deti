<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?/*<form action="<?=POST_FORM_ACTION_URI?>">*/?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?if (count($arResult["ITEMS"])):

/*echo '<pre>';
print_r($arParams);
echo '</pre>';*/

$arCurNav = array();
$CUR_CLINIC = false;

?>
<!--div class="price-list-clinic">
    <div class="name-title"><a href="#">КЛИНИКА «МАТЬ И ДИТЯ» ИРКУТСК</a></div>
    <div class="items">
        <div class="item">
            <div class="sub-title">КОНСУЛЬТАЦИИ СПЕЦИАЛИСТОВ (ВЗРОСЛЫЕ)</div>
            <div class="points">
                <div class="point">
                    <table class="web-table">
                        <tr>
                            <th>Наименивание процедуры</th>
                            <th>Цена</th>
                        </tr>
                        <tr>
                            <td>Прием (осмотр, консультация) акушера-гинеколога категории А первичный</td>
                            <td>1 400 р</td>
                        </tr>
                        <tr>
                            <td>Прием (осмотр, консультация) акушера-гинеколога категории А повторный</td>
                            <td>1 100 р</td>
                        </tr>
                    </table>
                    <div class="points">
                        <div class="point">
                            <div class="sub-title">КОНСУЛЬТАЦИИ СПЕЦИАЛИСТОВ (дети)</div>
                            <table class="web-table">
                                <tr>
                                    <th>Наименивание процедуры</th>
                                    <th>Цена</th>
                                </tr>
                                <tr>
                                    <td>Прием (осмотр, консультация) акушера-гинеколога категории А первичный</td>
                                    <td>1 400 р</td>
                                </tr>
                                <tr>
                                    <td>Прием (осмотр, консультация) акушера-гинеколога категории А повторный</td>
                                    <td>1 100 р</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="sub-title">КОНСУЛЬТАЦИИ СПЕЦИАЛИСТОВ (ВЗРОСЛЫЕ - 2)</div>
            <div class="points">
                <div class="point">
                    <table class="web-table">
                        <tr>
                            <th>Наименивание процедуры</th>
                            <th>Цена</th>
                        </tr>
                        <tr>
                            <td>Прием (осмотр, консультация) акушера-гинеколога категории А первичный</td>
                            <td>1 400 р</td>
                        </tr>
                        <tr>
                            <td>Прием (осмотр, консультация) акушера-гинеколога категории А повторный</td>
                            <td>1 100 р</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div-->

<?php/* if (isset($_GET['testing'])): ?>

<?php endif; ?>

<div class="price-list-clinic">
<?foreach ($arResult["ITEMS"] as $arItem):?>
    <?if ($arItem["CLINIC_SECTION"] != $CUR_CLINIC):?>
        <?=str_repeat('</ul></li>',count($arCurNav));?>
        <?php
            $arCurNav = array();
            $CUR_CLINIC = $arItem["CLINIC_SECTION"];
            $arClinic = $arResult["CLINICS"][$CUR_CLINIC]["CLINIC"];
        ?>
        <div class="name-title">
            <a href="<?=$arParams["SEF_FOLDER"]?><?=$arClinic["CODE"]?>/price/"><?=$arClinic["NAME"]?></a>
        </div>
       <??>
        <ul class="pricelist-list">
    <?endif;?>
    <?php
    $arDiff = array_diff_assoc($arItem["SECTION_NAV"],$arCurNav);
    if (count($arDiff)) {
        $idx = key($arDiff);
        echo str_repeat('</ul></li>',count($arCurNav)-$idx);
        $arCurNav = $arItem["SECTION_NAV"];
        for ($i=$idx;$i<count($arCurNav);$i++) {
            $arSection = $arResult["SECTIONS"][$arCurNav[$i]];
?>
    <li>
        <div class="sub-title l-<?=$arSection["DEPTH_LEVEL"]?>"><?=$arSection["NAME"]?></div>
        <ul>
<?php
        }
    }
    ?>
    <li>
        <span class="priceElementName"><?=$arItem["NAME"]?></span>
        <?if ($arItem["PROPERTY_PRICE_VALUE"]):?>
            <span class="priceElementPrice"><?=number_format($arItem["PROPERTY_PRICE_VALUE"],0,',',' ')?> р</span>
            <span class="priceC"></span>
        <?endif?>
    </li>
<?endforeach?>

<?=str_repeat('</ul></li>',count($arCurNav));?>
    </ul>

</div>
<?else:?>
<b>По данному запросу услуг не найдено</b>
<?endif?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

*/?>

<div class="ajax-price-list">

<div class="button-holder">
    <a href="#" class="print-coupon i-common-pseudo button01 block-button">Распечатать</a>
</div>

<?$cnt = 0;$clinicCount = 0;
foreach ($arResult["ITEMS"] as $i => $arItem) {
    if ($arItem["CLINIC_SECTION"] != $CUR_CLINIC) {
        $arCurNav = array();
        $CUR_CLINIC = $arItem["CLINIC_SECTION"];
        $clinicCount++;
    }
    $arDiff = array_diff_assoc($arItem["SECTION_NAV"], $arCurNav);
    if (count($arDiff)) {
        $idx = key($arDiff);
        $arCurNav = $arItem["SECTION_NAV"];
        for ($i=$idx;$i<count($arCurNav);$i++) {
            $arSection = $arResult["SECTIONS"][$arCurNav[$i]];
            if($arSection['DEPTH_LEVEL'] == 2) $cnt++;
        }
    }
}

$visible = $cnt == 1;

$arCurNav = array();
$CUR_CLINIC = false;

?>

<?foreach ($arResult["ITEMS"] as $i => $arItem):?>
    <?if ($arItem["CLINIC_SECTION"] != $CUR_CLINIC):?>
        <?=str_repeat('</ul></li>',count($arCurNav));?>
        <?php
            $arCurNav = array();
            $CUR_CLINIC = $arItem["CLINIC_SECTION"];
            $arClinic = $arResult["CLINICS"][$CUR_CLINIC]["CLINIC"];
        ?>
        <div class="name-title">
            <a href="<?=$arParams["SEF_FOLDER"]?><?=$arClinic["CODE"]?>/price/"><?=$arClinic["NAME"]?></a>
        </div>
        <?if($i!=0):?>
        </ul>
        <?endif;?>
        <ul class="pricelist-list">
    <?endif?>
    <?php
    $arDiff = array_diff_assoc($arItem["SECTION_NAV"], $arCurNav);
    if (count($arDiff)) {
        $idx = key($arDiff);
        echo str_repeat('</ul></li>',count($arCurNav)-$idx);
        $arCurNav = $arItem["SECTION_NAV"];
        for ($i=$idx;$i<count($arCurNav);$i++) {
            $arSection = $arResult["SECTIONS"][$arCurNav[$i]];
?>
    <li <?=$visible || $clinicCount == 1 ? ' class="open"': '';?>><i></i>
        <p class="priceSectionName<?=$arSection["DEPTH_LEVEL"]?>"><?=$arSection["NAME"]?></p>
        <ul<?=$visible || $clinicCount == 1 ? ' style="display:block;"' : ''?>>
<?php
        }
    }
    ?>
    <li class="dottedBackground">
        <span class="priceElementName"><?=$arItem["NAME"]?></span>
        <?if ($arItem["PROPERTY_PRICE_VALUE"]):?>
            <span class="priceElementPrice"><?=number_format($arItem["PROPERTY_PRICE_VALUE"],0,',',' ')?> р</span>
            <span class="priceC"></span>
        <?endif?>
    </li>
<?endforeach?>
<?=str_repeat('</ul></li>',count($arCurNav));?>
</ul>
</div>
<?else:?>
<b>По данному запросу услуг не найдено</b>
<?endif?>
<?/*if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?=$arResult["NAV_STRING"]?>
<?endif;*/?>
