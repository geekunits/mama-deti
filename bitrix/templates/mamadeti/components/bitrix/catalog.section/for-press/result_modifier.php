<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$pictureWidth = 300;
$pictureHeight = 300;

$section = ForPressSection::factory(array('ID' => $arResult['ID']));
$arResult['CLINIC_NAME'] = $section->getClinic()['NAME'];
$arResult['FILE'] = CFile::GetFileArray($section->getRawField('UF_FILE'));
$arResult['FILE_NAME'] = $section->getRawField('UF_FILE_NAME');

foreach($arResult['ITEMS'] as &$arItem) {
	$pictureId = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE']['ID'] : $arItem['PREVIEW_PICTURE']['ID'];
	if (!empty($pictureId)) {
		$arPicture = CFile::ResizeImageGet($pictureId, 
			array('width'=> $pictureWidth,'height'=> $pictureHeight), 
			BX_RESIZE_IMAGE_EXACT, 
			true);
	} else {
		$arPicture = null;
	}
	$arItem['PICTURE'] = array_change_key_case($arPicture, CASE_UPPER);
	$arItem['PROPERTIES'] = ForPressHelper::getElementProperties($arItem['ID']);
	if (!empty($arItem['PROPERTIES']['PRESS_RELEASE_FILE']['VALUE'])) {
		$arItem['PRESS_RELEASE_FILE'] = CFile::GetFileArray($arItem['PROPERTIES']['PRESS_RELEASE_FILE']['VALUE']);
		$arItem['PRESS_RELEASE_FILE']['DESCRIPTION'] = $arItem['PROPERTIES']['PRESS_RELEASE_FILE']['DESCRIPTION'];
	}
}

?>