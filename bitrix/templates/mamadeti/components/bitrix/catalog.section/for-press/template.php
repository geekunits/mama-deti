<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	$columnCount = 3;
?>
<div class="fopress-column-right">
	<div class="forpress-list">
		<div class="forpress-list__clinic b-header_h1">
			<?php echo $arResult['CLINIC_NAME']; ?>
		</div>
		<div class="forpress-list__desc">
			<div class="forpress-list__desc-txt"><?php echo $arResult['DESCRIPTION']; ?></div>
			<?php if (!empty($arResult['FILE'])): ?>
				<div>
					<a href="<?php echo $arResult['FILE']['SRC']; ?>" target="_blank" class="forpress-list__download">Скачать заявку</a>
				</div>
			<?php endif; ?>
		</div>

		<?php if (count($arResult['ITEMS']) > 0): ?>
			<div class="b-columns_three">
				<?php foreach($arResult['ITEMS'] as $index => $arItem): ?>
					<?php if ($index % $columnCount === 0): ?>
						<div class="forpress-list__item clearfix">
					<?php endif; ?>
					<div class="b-column">
						<div class="forpress-list__pic">
							<?php if (!empty($arItem['PICTURE'])): ?>
								<img 
									src="<?php echo $arItem['PICTURE']['SRC']; ?>" 
									alt="<?php echo $arItem['NAME']; ?>"
									title="<?php echo $arItem['NAME']; ?>">
							<?php endif; ?>
						</div>
						<div class="forpress-list__name">
							<?php echo $arItem['NAME']; ?>
						</div>
						<div class="forpress-list__txt">
							<?php echo $arItem['PREVIEW_TEXT']; ?>
						</div>
						<?php if (!empty($arItem['PROPERTIES']['FILE_LINK']['VALUE'])): ?>
							<div>
								<a href="<?php echo $arItem['PROPERTIES']['FILE_LINK']['VALUE']; ?>" target="_blank" class="forpress-list__download">
									Скачать альбом <?php echo $arItem['PROPERTIES']['FILE_LINK']['DESCRIPTION']; ?>
								</a>
							</div>
						<?php endif; ?>
						<?php if (!empty($arItem['PRESS_RELEASE_FILE'])): ?>
							<div>
								<a href="<?php echo $arItem['PRESS_RELEASE_FILE']['SRC']; ?>" target="_blank" class="forpress-list__download">
									Пресс-релиз
								</a>
							</div>
						<?php endif; ?>
					</div>
					<?php if ($index % $columnCount === $columnCount - 1): ?>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
				<?php if ($index % $columnCount !== $columnCount - 1): ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>