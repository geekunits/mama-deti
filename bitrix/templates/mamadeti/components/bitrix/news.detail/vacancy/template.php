<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="rt-detail">
    <div class="description">
        <?if($arResult["NAME"]){?>
            <div class="name-title"><?=$arResult["NAME"]?></div>
        <?}?>
        <?if($arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]){?>
            <div class="clinic">
                <strong>Клиника:</strong> <?php echo $arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]; ?>
            </div>
        <?}?>
        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
            <div class="preview"><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></div>
        <?endif;?>
        <div class="text">
            <?if(strlen($arResult["DETAIL_TEXT"])>0):?>
                <?echo $arResult["DETAIL_TEXT"];?>
            <?else:?>
                <?echo $arResult["PREVIEW_TEXT"];?>
            <?endif?>
        <a class="back red-color" href="<?php echo preg_replace('/<a href="([^"]+)">.+<\/a>/', '$1', $arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]); ?>vacancies/">вернуться назад</a>
    </div>
</div>
