<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$dateFrom = $arResult['DISPLAY_PROPERTIES']['DATE_FROM']['DISPLAY_VALUE'];
$dateTo = $arResult['DISPLAY_PROPERTIES']['DATE_TO']['DISPLAY_VALUE'];
$nowDt = new DateTime;
$nowDt->setTime(0, 0, 0);

if ($dateFrom || $dateTo) {
    if ($dateTo) {
        $dateToDt = new DateTime('@'.MakeTimeStamp($dateTo));
        $dateToDt->setTime(23, 59, 59);
        $daysLeft = $nowDt->diff($dateToDt)->format('%a');
    } else {
        $daysLeft = null;
    }

    if ($dateFrom) {
        $dateFromDt = new DateTime('@'.MakeTimeStamp($dateFrom));
        $dateFromDt->setTime(0, 0, 0);
    }

    $daysSoon = false;
    if ($dateFromDt) {
        $intervalSoon = ($nowDt < $dateFromDt) ? $dateFromDt->diff($nowDt) : false;
        $daysSoon = $intervalSoon ? strval(intval($intervalSoon->format('%a')) + 1) : false;
    }
}
?>
<div class="rt-detail js-element-wrap" data-id="<?=$arResult["ID"]?>">
    <?if($arResult["DETAIL_PICTURE"]["SRC"]):?>
        <div class="photo">
            <img
                src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                alt="<?=$arResult["NAME"]?>"
                title="<?=$arResult["NAME"]?>"
                >
        </div>
    <?endif?>
    <div class="description">
        <?if ($arResult["NAME"]) {?>
            <div class="name-title"><?=$arResult["NAME"]?></div>
        <?}?>
        <?php if ($arResult['PROPERTIES']['SHOW_COUPON']['VALUE']): ?>
            <a href="#" class="print-coupon button01 block-button" style="width:235px;margin-bottom:10px">Распечатать купон</a>
        <?php endif; ?>
        <?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
            <div class="date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
        <?endif;?>
        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
            <div class="preview"><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></div>
        <?endif;?>
        <?if ($arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]) {?>
            <div class="clinic">
                <strong>Клиника:</strong> <?php echo $arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]; ?>
            </div>
        <?}?>
        <?if ($dateFrom || $dateTo) {?>
            <div class="time-action">
                <?php if ($dateFrom && $dateTo): ?>
                    Акция проводится с <?php echo $dateFrom; ?> по <?php echo $dateTo; ?>.
                <?php elseif ($dateFrom):?>
                    Акция проводится с <?php echo $dateFrom; ?>.
                <?php elseif ($dateTo):?>
                    Акция проводится по <?php echo $dateTo; ?>.
                <?php endif;?>
            </div>
        <?}?>
        <?php if ($daysSoon) { ?>
            <div class="actions-counter in-bl">
                <div class="group">
                    <div class="txt txt-top"><div class="wrap">Через</div></div>
                    <div class="numbers">
                        <?php
                        for ($i = 0; $i < strlen($daysSoon); $i++) {
                            echo '<span class="number">'.$daysSoon[$i].'</span> ';
                        }
                        ?>
                    </div>
                    <div class="txt txt-bottom"><div class="wrap"><?= plural($daysSoon, ['день', 'дня', 'дней']);?></div></div>
                </div>
            </div>
        <?php } elseif ($daysLeft) { ?>
            <div class="actions-counter in-bl">
                <div class="group">
                    <div class="txt txt-top"><div class="wrap">Осталось</div></div>
                    <div class="numbers">
                        <?php
                        for ($i = 0; $i < strlen($daysLeft); $i++) {
                            echo '<span class="number">'.$daysLeft[$i].'</span> ';
                        }
                        ?>
                    </div>
                    <div class="txt txt-bottom"><div class="wrap"><?= plural($daysLeft, ['день', 'дня', 'дней']);?></div></div>
                </div>
            </div>
        <?php } ?>
        <div class="text">
            <?if(strlen($arResult["DETAIL_TEXT"])>0):?>
                <?echo $arResult["DETAIL_TEXT"];?>
            <?else:?>
                <?echo $arResult["PREVIEW_TEXT"];?>
            <?endif?>
            <?if (!empty($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"])):?>
                <br/>
                <strong class="strong-title">Список услуг: </strong>
                <div class="list-cols"><div class="col">
                    <ul class="link-list">
                    <?if (is_array($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"])):?>
                        <?foreach ($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"] as $val):?>
                            <?php if ($val): ?>
                                <li><?=$val?></li>
                            <?php endif; ?>
                        <?endforeach?>
                    <?else:?>
                        <li><?=$arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"]?></li>
                    <?endif?>
                    </ul>
                </div></div>
            <?endif?>
        </div>
        <div class="social-ico">
            <div class="title">Понравился материал? Поделись с друзьями!</div>
            <div class="social-likes">
                <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
                <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
                <div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
                <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
                <div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
            </div>
        </div>
        <br>
        <a class="back red-color" href="<?php echo preg_replace('/<a href="([^"]+)">.+<\/a>/', '$1', $arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]); ?>actions/">вернуться назад</a>
    </div>
</div>
