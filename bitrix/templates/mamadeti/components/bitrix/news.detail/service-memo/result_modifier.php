<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if(strpos($arParams['HTTP_REFERER'], '/services/') !== false){
	$link = $arParams['HTTP_REFERER'];
} elseif(!empty($arResult['DISPLAY_PROPERTIES']['SERVICE']['DISPLAY_VALUE'])){
	$link = is_array($arResult['DISPLAY_PROPERTIES']['SERVICE']['DISPLAY_VALUE']) ? $arResult['DISPLAY_PROPERTIES']['SERVICE']['DISPLAY_VALUE'][0] : $arResult['DISPLAY_PROPERTIES']['SERVICE']['DISPLAY_VALUE'];
	$link = preg_replace('/<a href="([^"]+)">.+<\/a>/', '$1', $link);
} elseif(!empty($arResult['DISPLAY_PROPERTIES']['SERVICE_SECTION']['DISPLAY_VALUE'])){
	$link = is_array($arResult['DISPLAY_PROPERTIES']['SERVICE_SECTION']['DISPLAY_VALUE']) ? $arResult['DISPLAY_PROPERTIES']['SERVICE_SECTION']['DISPLAY_VALUE'][0] : $arResult['DISPLAY_PROPERTIES']['SERVICE_SECTION']['DISPLAY_VALUE'];
	$link = preg_replace('/<a href="([^"]+)">.+<\/a>/', '$1', $link);
} else {
	$link = '/services/';
}

$cp = $this->__component;
$cp->arResult['BACK_URL'] = $link;
?>