<?php 
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	$clinicId = null;
	$clinicIds = $arResult['PROPERTIES']['CLINIC']['VALUE'];
	if (isset($_GET['clinic']) && in_array($_GET['clinic'], $clinicIds)) {
		$clinicId = (int) $_GET['clinic'];
	}
?>
<?php if (!empty($clinicId)): ?>
	<?php
		$formatter = new ClinicResultFormatter($clinicId);
		$clinicMenu = new ClinicMenu($formatter->format(), [
			'useAjax' => false,
			'currentTabName' => 'news',
		]);
		$APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getContent());
	?>
<?php endif; ?>
<div class="news-detail js-element-wrap" data-id="<?=$arResult["ID"]?>">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["PREVIEW_PICTURE"])):?>
		<img class="detail_picture" border="0" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" />
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span><br>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
<?if (!empty($arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"])):?>
	<br/><br/>
<strong class="strong-title">Список клиник: </strong>
<div class="list-cols"><div class="col">
	<ul class="link-list">
	<?if (is_array($arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"])):?>
		<?foreach ($arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"] as $val):?>
			<li><?=$val?></li>
		<?endforeach?>
	<?else:?>
		<li><?=$arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]?></li>
	<?endif?>
	</ul>
</div></div>
<?endif?>

<?if (!empty($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"])):?>
	<br/><br/>
<strong class="strong-title">Список услуг: </strong>
<div class="list-cols"><div class="col">
	<ul class="link-list">
	<?if (is_array($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"])):?>
		<?foreach ($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"] as $val):?>
			<li><?=$val?></li>
		<?endforeach?>
	<?else:?>
		<li><?=$arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"]?></li>
	<?endif?>
	</ul>
</div></div>
<?endif?>

<?if (!empty($arResult["DISPLAY_PROPERTIES"]["SPECIALTY"]["DISPLAY_VALUE"])):?>
	<br/><br/>
<strong class="strong-title">Список специальностей: </strong>
<div class="list-cols"><div class="col">
	<ul class="link-list">
	<?if (is_array($arResult["DISPLAY_PROPERTIES"]["SPECIALTY"]["DISPLAY_VALUE"])):?>
		<?foreach ($arResult["DISPLAY_PROPERTIES"]["SPECIALTY"]["DISPLAY_VALUE"] as $val):?>
			<li><?=strip_tags($val)?></li>
		<?endforeach?>
	<?else:?>
		<li><?=strip_tags($arResult["DISPLAY_PROPERTIES"]["SPECIALTY"]["DISPLAY_VALUE"])?></li>
	<?endif?>
	</ul>
</div></div>
<?endif?>

	<?
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>
<div class="social-ico">
	<div class="title">Понравился материал? Поделись с друзьями!</div>
	<div class="social-likes">
		<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
		<div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
		<div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
		<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
		<div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
	</div>
</div>