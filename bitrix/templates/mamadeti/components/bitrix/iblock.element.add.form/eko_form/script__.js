$(function(){
	var $closeButtonTemplate = $('<span class="btn close"></span>');
	$('.add-more').click(function() {
		var $this = $(this);
		var $container = $this.closest('.row').prev();
		var $containerClone = $container.clone();
		var $input = $containerClone.find('input[name^=PROPERTY]');
		var $img = $containerClone.find('img');
		var name = $input.attr('name');
		var regex = /PROPERTY\[(\d+)\]\[(\d+)\]/
		var matches = name.match(regex);
		var index = parseInt(matches[2]) + 1;
		var replace = 'PROPERTY[' + matches[1] + '][' + index + ']';
		name = name.replace(regex, replace);
		if ($img.size()) {
			var onclick = $img.attr('onclick');
			onclick = onclick.replace(regex, replace);
			$img.attr('onclick', onclick);
		}
		$input.attr('name', name).val('');
		if ($containerClone.find('.close').size() === 0) {
			$containerClone.find('.row-sector').prepend($closeButtonTemplate.clone());
			$containerClone.addClass('p-t');
		}
		
		$containerClone.find('.label-block .middle').html('');
		$container.after($containerClone);
	});
	$('.content-sector').on('click','.row .close',function(e){
		var $container = $(this).closest('.row');
		$container.remove();
	});

	// $('.add-more').click(function(){
	// 	$this = $(this);
	// 	$containerPrev = $this.closest('.row').prev();

	// 	if($containerPrev.find('[name^="PROPERTY"]').length){
	// 		var config = {
	// 			'names' :  ['[name^="PROPERTY[156]"]', /6\]\[(\d*)\]/],
	// 			'type' : 'date'
	// 		};
	// 	} else if($containerPrev.find('[type="file"]').length) {
	// 		var config = {
	// 			'names' : ['[type="file"]', /162_(\d*)/],
	// 			'type' : 'file'
	// 		};
	// 	}
	// 	var index = parseInt($containerPrev.find(config.names[0]).attr('name').match(config.names[1])[1]);
	// 	var section = forms[config.type].replace(/#index#/g,index+1);
	// 	if(config.type == 'date') section = section.replace(/#index1#/g,index+2);
	// 	$containerPrev.next().before(section);
	// 	if(config.type == 'file') $containerPrev.next().find('input').styler();
	// });
	// $('.content-sector').on('click','.row .close',function(e){
	// 	var $container = $(this).closest('.row');
	// 	if($container.find('[type="file"]').length == 0){
	// 		$container.next().remove();
	// 	}
	// 	$container.remove();
	// });
});