<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?php
	$dateProperties = array();
	$multipleDateProperties = array(232, 233, 234, 235, 240, 250, 256, 247, 248, 249, 242);
?>
<?php
	$titles = array(
		'NAME' => 'Контактные данные',
		232 => 'Операции на органах брюшной полости',
		233 => 'Операции гинекологические',
		236 => 'Гинекологические заболевания',
		240 => 'Другие серьёзные оперативные вмешательства',
		241 => 'Травмы, сотрясения головного мозга и т.д.',
		242 => 'Хронические ( негинекологические) заболевания',
		243 => 'Наследственность (рак груди, яичников, толстого кишечника по материнской линии, случаи муковисцидоза, талассемии, др заболеваний, рождение детей с пороками развития и т.д.)',
		245 => 'Внутриматочные  инсеминации в анамнезе:',
		246 => 'Программы ЭКО в анамнезе',
		247 => 'Все беременности',
		254 => 'Просьба отметить, откуда вы узнали о нашей клинике',
	);
?>
<script type="text/html" id="formDate-template">

	<div class="row">
		<div class="row-sector date">
			<div class="text04">
				<input type="text" name="PROPERTY[156][#index1#][VALUE]" size="25" value="">
					<img
						src="/bitrix/js/main/core/images/calendar-icon.gif"
						alt="Выбрать дату в календаре"
						class="calendar-icon"
						onclick="BX.calendar({node:this, field:'PROPERTY[156][#index1#][VALUE]', form: 'iblock_add', bTime: false, currentTime: '1432041754', bHideTime: false});"
						onmouseover="BX.addClass(this, 'calendar-icon-hover');"
						onmouseout="BX.removeClass(this, 'calendar-icon-hover');" border="0">
			</div>
		</div>
	</div>
</script>
<?php if (count($arResult["ERRORS"])): ?>
	<?php echo ShowError(implode('<br>', $arResult["ERRORS"])); ?>
<?php endif; ?>
<?php if (strlen($arResult["MESSAGE"]) > 0): ?>
	<br>
	<div class="form-eko-success"><?php echo ShowNote($arResult["MESSAGE"]); ?></div>
	<br>
<?php else: ?>
	<span class="starrequired">*</span> - поля, обязательные для заполнения
	<br>
	<br>
	<form name="iblock_add" action="<?php echo POST_FORM_ACTION_URI; ?>" method="post" enctype="multipart/form-data">
		<?php echo bitrix_sessid_post(); ?>
		<?php if ($arParams["MAX_FILE_SIZE"] > 0): ?>
			<input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>">
		<?php endif; ?>
		<div class="form-eko">
			<div class="form-rows b-columns_two clearfix">
				<?php if (is_array($arResult["PROPERTY_LIST"])): ?>
					<div class="b-column">

					<?php foreach($arResult['PROPERTY_LIST'] as $propertyID): ?>
						<?php if ($propertyID == 241): ?>
							</div><div class="b-column">
						<?php endif; ?>
						<?php if(in_array($propertyID,array_keys($titles))): ?>
							<div class="title">
								<h3><?=$titles[$propertyID]?></h3>
							</div>
						<?php endif; ?>
						<div class="row">
							<div class="label-block">
								<span class="middle">
									<?php if ($propertyID == 231 || $propertyID == 260): ?>
										<h3>
									<?php endif; ?>
									<?php if (intval($propertyID) > 0):?>
										<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>
									<?php else:?>
										<?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID); ?>
									<?php endif; ?>
									<?php if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?>
										<span class="starrequired">*</span>
									<?php endif; ?>
									<?php if ($propertyID == 231 || $propertyID == 260): ?>
										</h3>
									<?php endif; ?>
								</span>
							</div>
							<div class="row-sector<?php if (in_array($propertyID, array_merge($dateProperties, $multipleDateProperties))): ?> date<?php endif; ?>">
								<?php
									if (intval($propertyID) > 0) {
										if (
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
											&&
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
										) {
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
										} elseif (
											(
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
												||
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
											)
											&&
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
										) {
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
										}
									} elseif (($propertyID == "TAGS") && CModule::IncludeModule('search')) {
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";
									}

									if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
										$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
										$inputNum += $inputNum == 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"] : 0;
									} else {
										$inputNum = 1;
									}

									if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"]) {
										$INPUT_TYPE = "USER_TYPE";
									} else {
										$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];
									}
								?>
								<?php switch ($INPUT_TYPE): ?><?php case 'USER_TYPE': ?>
											<?php for ($i = 0; $i < $inputNum; $i++): ?>
												<?php
													if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
														$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
														$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
													} elseif ($i == 0) {
														$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
														$description = "";
													} else {
														$value = "";
														$description = "";
													}
												?>
												<?php if ($i > 0): ?>
													</div></div>
													<div class="row p-t">
														<div class="label-block">
															<span class="middle"></span>
														</div>
													<div class="row-sector date">
														<span class="btn close"></span>
												<?php endif; ?>
												<?php echo call_user_func_array(
													'GetCustomPublicEditHTML',
													//$arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
													array(
														$arResult["PROPERTY_LIST_FULL"][$propertyID],
														array("VALUE" => $value, "DESCRIPTION" => $description),
														array(
															"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
															"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
															"FORM_NAME"=>"iblock_add",
														),
												)); ?>
											<?php endfor; ?>
											<?php if (in_array($propertyID, $multipleDateProperties)): ?>
												</div></div>
												<div class="row">
													<div class="row-sector">
													<span class="b-btn_white add-more in-bl">
														Добавить
													</span>
											<?php endif; ?>
									<?php break; ?>
									<?php case "TAGS": ?>
										<?php $APPLICATION->IncludeComponent(
											"bitrix:search.tags.input",
											"",
											array(
												"VALUE" => $arResult["ELEMENT"][$propertyID],
												"NAME" => "PROPERTY[".$propertyID."][0]",
												"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
											), null, array("HIDE_ICONS"=>"Y")
										); ?>
									<?php break; ?>
									<?php case "HTML": ?>
										<?php 
										$LHE = new CLightHTMLEditor;
										$LHE->Show(array(
											'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
											'width' => '100%',
											'height' => '200px',
											'inputName' => "PROPERTY[".$propertyID."][0]",
											'content' => $arResult["ELEMENT"][$propertyID],
											'bUseFileDialogs' => false,
											'bFloatingToolbar' => false,
											'bArisingToolbar' => false,
											'toolbarConfig' => array(
												'Bold', 'Italic', 'Underline', 'RemoveFormat',
												'CreateLink', 'DeleteLink', 'Image', 'Video',
												'BackColor', 'ForeColor',
												'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
												'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
												'StyleList', 'HeaderList',
												'FontList', 'FontSizeList',
											),
										)); ?>
										<?php break; ?>
									<?php case "T": ?>
										<?php for ($i = 0; $i < $inputNum; $i++): ?>
											<?php 
												if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
													$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
												} elseif ($i == 0) {
													$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
												} else {
													$value = "";
												}
											?>
											<div class="textarea01">
												<textarea 
													cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" 
													rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" 
													name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
											</div>
										<?php endfor; ?>
										<?php break; ?>
									<?php case "S": ?>
									<?php case "N": ?>
										<?php for ($i = 0; $i < $inputNum; $i++): ?>
											<?php 
												if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
													$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
												} elseif ($i == 0) {
													$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

												} else {
													$value = "";
												}
											?>
											<div class="text04">
												<input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />
											</div>
											<?php if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"): ?>
												<?php
													$APPLICATION->IncludeComponent(
														'bitrix:main.calendar',
														'',
														array(
															'FORM_NAME' => 'iblock_add',
															'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
															'INPUT_VALUE' => $value,
														),
														null,
														array('HIDE_ICONS' => 'Y')
													);
												?>
												<small>
													<?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?>
												</small>
											<?php endif; ?>
										<?php endfor; ?>
										<?php break; ?>
									<?php case "F": ?>
										<?php for ($i = 0; $i < $inputNum; $i++): ?>
											<?php 
												$value = intval($propertyID) > 0 ? 
													$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : 
													$arResult["ELEMENT"][$propertyID];
											?>
											<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
											<input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
											<?php if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])): ?>
												<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
												<?php if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]): ?>
													<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
												<?php else: ?>
													<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
													<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
													[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
												<?php endif; ?>
											<?php endif; ?>
										<?php endfor; ?>
										</div></div>
										<div class="row">
											<div class="row-sector">
											<span class="button01 block-button">
												<input type="button" class="add-more" value="Добавить">
												Добавить
											</span>
										<?php break; ?>
										<?php case "L": ?>
											
											<?php 
												if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C"){
													$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
												} else {
													$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";
												}
											?>
											<?php switch ($type): ?><?php case "checkbox": ?>
											<?php case "radio": ?>
												<?php foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) : ?>
													<?php
														$checked = false;
														if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
															if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))	{
																foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
																	if ($arElEnum["VALUE"] == $key) {
																		$checked = true; 
																		break;
																	}
																}
															}
														} else {
															if ($arEnum["DEF"] == "Y") $checked = true;
														}

													?>
													<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
												<?php endforeach; ?>	
												<?php break; ?>
											<?php case "dropdown": ?>
											<?php case "multiselect": ?>
												<select name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
													<option value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
													<?php 
														if (intval($propertyID) > 0) {
															$sKey = "ELEMENT_PROPERTIES";
														} else { 
															$sKey = "ELEMENT";
														}
													?>
													<?php foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum): ?>
														<?php 
															$checked = false;
															if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
																foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
																	if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
																}
															} else {
																if ($arEnum["DEF"] == "Y") {
																	$checked = true;
																}
															}
														?>
														<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
													<?php endforeach; ?>
												</select>
											<?php break; ?>
										<?php endswitch; ?>
									<?php break; ?>
								<?php endswitch; ?>
							</div>
						</div>
					<?php endforeach; ?>
					<?php if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
						<div class="row">
							<div class="label-block"><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></div>
							<div class="row-sector">
								<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
								<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
							</div>
						</div>
						<div class="row">
							<div class="label-block"><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</div>
							<div class="row-sector"><input type="text" name="captcha_word" maxlength="50" value=""></div>
						</div>
					<?php endif; ?>
				<?php endif; ?>
				<br>
				<span class="button01">
					<input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
					<?=GetMessage("IBLOCK_FORM_SUBMIT")?>
				</span>
				</div>
			</div>
			<br>
			<?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?>
				<span class="button01">
					<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
						<?=GetMessage("IBLOCK_FORM_APPLY")?>
				</span>
			<?endif?>
		</div>
	</form>
<?php endif; ?>