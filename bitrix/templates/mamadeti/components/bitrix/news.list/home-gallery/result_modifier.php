<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

//echo '<pre>'; print_r($arResult); echo '</pre>';

$arParams["RESIZE_CATALOG_METHOD"] = intval($arParams["RESIZE_CATALOG_METHOD"]);

foreach ($arResult["ITEMS"] as &$arItem) {
    if (!is_array($arItem["PREVIEW_PICTURE"]))
	$arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
    $arFileTmp = CFile::ResizeImageGet(
                    $arItem["PREVIEW_PICTURE"], array("width" => $arParams["RESIZE_CATALOG_WIDTH"], "height" => $arParams["RESIZE_CATALOG_HEIGHT"]), $arParams["RESIZE_CATALOG_METHOD"], true
    );
    $arItem["PREVIEW_PICTURE"] = array(
        "SRC" => $arFileTmp["src"],
        "WIDTH" => $arFileTmp["width"],
        "HEIGHT" => $arFileTmp["height"],
    );
    unset($arItem);
}

?>