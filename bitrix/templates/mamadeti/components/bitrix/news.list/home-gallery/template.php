<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php global $currentClinicId; ?>
<div class="col">
    <div class="content-block">
        <div class="block-frame">
            <div class="menu-title">
                <div class="image">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/icon10.png">
                </div>
                <div class="holder">
                    <span class="title">Фото/видео</span>
                </div>
            </div>
            <div class="reviews-gallery">
                <div class="gallery-frame">
                    <ul class="gallery">
                    <?foreach($arResult["ITEMS"] as $i=>$arItem):?>
                        <li class="slide">
                            <ul class="articles-list">
                                <li>
                                <div class="article-block">
                                    <div class="article-visual">
                                    <?if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) {?>
                                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
                                    <?}?>
                                    </div>
                                    <div class="article-text">
                                        <p>
                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["PREVIEW_TEXT"]?></a>
                                        </p>
                                    </div>
                                </div>
                                </li>
                            </ul>
                        </li>
                    <?endforeach;?>
                    </ul>
                </div>
                <div class="bottom-panel">
                    <div class="more-link align-center">
                        <?php if ($currentClinicId): ?>
                            <a href="/gallery/?clinic=<?php echo $currentClinicId; ?>">Перейти в раздел фотогалерея</a>
                        <?php else: ?>
                            <a href="/gallery/">Перейти в раздел фотогалерея</a>
                        <?php endif; ?>
                    </div>
                    <div class="switcher">
                        <a href="#" class="prev">prev</a>
                        <div>
                        </div>
                        <a href="#" class="next">next</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
