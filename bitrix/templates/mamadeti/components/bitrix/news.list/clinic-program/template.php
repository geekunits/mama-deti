<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
global $arrProgramFilter;
$currentDirectionId = null;
if (isset($arrProgramFilter['PROPERTY_DIRECTION.ID'])) {
    $currentDirectionId = $arrProgramFilter['PROPERTY_DIRECTION.ID'];
}
?>
<div class="b-clinic_program__wrap">
    <div class="jq-selectbox__select__red b-clinic_program__select">
        <select name="direction">
            <option value="-1">Все направления</option>
                <?php foreach($arResult['DIRECTIONS'] as $directionId => $directionName): ?>
                    <?php $isSelected =  $currentDirectionId && ($currentDirectionId == $directionId); ?>
                <option value="<?php echo $directionId; ?>" <?php echo $isSelected ? 'selected="selected"' : ''; ?>><?php echo $directionName; ?></option>             
            <?php endforeach; ?>
        </select>
    </div>
    
    <?php foreach($arResult['ITEM_GROUPS'] as $directionGroup): ?>
        <div class="b-clinic_program b-columns_two">
        <?php $index = null; ?>
        <div class="b-header_h2"><?php echo $directionGroup['NAME']; ?></div>
            <?php foreach($directionGroup['ITEMS'] as $index => $arItem): ?>
                <?php if ($index % 2 === 0): ?>
                    <div class="b-clinic_program__item clearfix">
                <?php endif; ?>           
                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="b-clinic_program__link b-column">
                    <?php echo $arItem['NAME']; ?>
                    <?php $price = number_format($arItem["PROPERTIES"]['PRICE']['VALUE'], 0, ',', ' '); ?>
                    <?php if (!empty($price)): ?>
                        <span class="b-clinic_program__price"><?php echo $price; ?> р</span>
                    <?php endif; ?>
                </a>
                <?php if ($index % 2 === 1): ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($index !== null && $index % 2 === 0): ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>

    <?php if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br />
        <?php echo $arResult["NAV_STRING"]?>
    <?endif;?>
</div>

