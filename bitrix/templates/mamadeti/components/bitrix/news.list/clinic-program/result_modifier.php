<?php
global $arrProgramFilter;
$clinicId = $arrProgramFilter['PROPERTY_CLINIC'];

$arResult['ITEM_GROUPS'] = array();

foreach ($arResult['ITEMS'] as $arItem) {
	foreach($arItem['PROPERTIES']['DIRECTION']['VALUE'] as $directionId) {
		if (!isset($arResult['ITEM_GROUPS'][$directionId])) {
			$arResult['ITEM_GROUPS'][$directionId] = array('ITEMS' => array());
		}
		$arResult['ITEM_GROUPS'][$directionId]['ITEMS'][] = $arItem;
	}
}

$arFilter = array(
	'IBLOCK_ID' => 27,
	'PROPERTY_CLINIC.ID' => $clinicId,
);
$arSelect = array(
	'IBLOCK_ID',
	'ID',
	'NAME',
	'PROPERTY_CLINIC',
	'PROPERTY_DIRECTION',
	'PROPERTY_DIRECTION.ID',
	'PROPERTY_DIRECTION.NAME',
);
$dbPrograms = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
$arResult['DIRECTIONS'] = array();
while($arProgram = $dbPrograms->Fetch()) {
	$directionId = $arProgram['PROPERTY_DIRECTION_ID'];
	$directionName = $arProgram['PROPERTY_DIRECTION_NAME'];
	if (isset($arResult['ITEM_GROUPS'][$directionId])) {
		$arResult['ITEM_GROUPS'][$directionId]['NAME'] = $directionName;
	}
	$arResult['DIRECTIONS'][$directionId] = $directionName;
}