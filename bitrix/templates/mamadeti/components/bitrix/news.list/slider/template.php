<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="promo-gallery">
	<div class="gallery-section">
		<a href="#" class="prev">prev</a>
		<a href="#" class="next">next</a>
		<ul class="gallery">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<li>
				<div class="gallery-image">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="737" height="420" alt="<?=$arItem["NAME"]?>">
				</div>
				<div class="gallery-description">
					<?=$arItem["PREVIEW_TEXT"]?>
				</div>
			</li>		
		<?endforeach;?>
		</ul>
		<div class="switcher">
		</div>
	</div>
</div>
