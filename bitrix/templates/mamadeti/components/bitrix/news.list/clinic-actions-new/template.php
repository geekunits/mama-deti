<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
    $dateFormat = 'd.m.Y';
    $timeFormat = 'h:i:s';
    $clinicId = isset($GLOBALS['arrActionsFilter']) && isset($GLOBALS['arrActionsFilter']['PROPERTY_CLINIC']) ?
        $GLOBALS['arrActionsFilter']['PROPERTY_CLINIC'] : null;
?>
<?php if ($arResult['ITEMS']): ?>
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
    <div class="b-clinic_actions__wrap">
        <?php foreach($arResult["ITEMS"] as $index => $arItem):?>
            <?php
                $index += 1;
                $dateTo = $arItem['DISPLAY_PROPERTIES']['DATE_TO']['VALUE'];
                $dateFrom = $arItem['DISPLAY_PROPERTIES']['DATE_FROM']['VALUE'];
                $dtActiveTo = null;
                $dtActiveFrom = null;

                $dateTo = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateTo);
                $dateFrom = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateFrom);
                $dtNow = new \DateTime('now');
                $dtNow->setTime(0, 0, 0);

                if (
                    !empty($dateTo)
                    && ($dtActiveTo = \DateTime::createFromFormat($dateFormat, $dateTo))
                ) {
                    $dtActiveTo->setTime(23, 59, 59);
                }
                if (!empty($dateFrom)) {
                     $dtActiveFrom = \DateTime::createFromFormat($dateFormat, $dateFrom);
                }
                $daysLeft = false;
                $daysSoon = false;
                if ($dtActiveFrom) {
                    $intervalSoon = ($dtNow < $dtActiveFrom) ? $dtActiveFrom->diff($dtNow) : false;
                    $daysSoon = $intervalSoon ? intval($intervalSoon->format('%a')) : false;
                }
                if ($dtActiveTo) {
                    $dtInterval = $dtActiveTo->diff($dtNow);
                    $daysLeft = intval($dtInterval->format('%a')) + 1;
                }
                $imageData = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array('width'=>325,'height'=>183), BX_RESIZE_IMAGE_EXACT, true);
                $dateRange = '';
                if (!empty($dateTo) && !empty($dateFrom)) {
                    if ($dateTo == $dateFrom) {
                        $dateRange = 'Проводится ' . $dateTo;
                    } else {
                        $dateRange = $dateFrom . ' - ' . $dateTo;
                    }
                } elseif (!empty($dateTo)) {
                    $dateRange = 'По ' . $dateTo;
                } elseif (!empty($dateFrom)) {
                    $dateRange = 'С ' . $dateFrom;
                }

                $detailPageUrl = $arItem['DETAIL_PAGE_URL'];
                if (!empty($clinicId)) {
                    $detailPageUrl .= '?clinic=' . $clinicId;
                }
            ?>
            <?php if ($index % 2 === 1): ?>
                <div class="b-columns_two b-news_item clearfix">
            <?php endif; ?>
            <div class="b-column">
                 <a href="<?php echo $detailPageUrl; ?>" class="b-news_link">
                        <span class="b-news_pic">
                            <img src="<?php echo $imageData['src']; ?>" alt="<?php echo $arItem['NAME']; ?>" alt="<?php echo $arItem['TITLE']; ?>">
                            <?php if ($daysSoon): ?>
                                <span class="b-clinic_action__days--soon">
                                    до начала акции <strong><?php echo $daysSoon; ?></strong>
                                    <?php echo plural($daysSoon, ['день', 'дня', 'дней']) ;?>
                                </span>
                            <?php elseif ($daysLeft > 1): ?>
                                <span class="b-clinic_action__days">
                                    до окончания акции <strong><?php echo $daysLeft; ?></strong>
                                    <?php echo plural($daysLeft, ['день', 'дня', 'дней']) ;?>
                                </span>
                            <?php elseif ($daysLeft === 1): ?>
                                <span class="b-clinic_action__days">
                                    <strong>Остался последний день</strong>
                                </span>
                            <?php endif; ?>
                        </span>
                        <?php if (!empty($dateRange)): ?>
                            <span class="b-news_date"><?php echo $dateRange ?></span>
                        <?php endif; ?>
                        <span class="b-news_name"><?php echo $arItem['NAME']; ?></span>
                        <span class="b-news_txt"><?php echo $arItem["PREVIEW_TEXT"]; ?></span>
                    </a>
                </div>
            <?php if ($index % 2 === 0): ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if ($index % 2 === 1): ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?php echo $arResult["NAV_STRING"]?>
    <?php endif;?>
<?php endif; ?>
