<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
    $count=count($arResult["ITEMS"])
?>
<div class="b-bg_white b-widgets_footer__col">
    <div class="b-widgets_footer__padding" style="padding-bottom:0;">
        <div class="align-center b-header_h2">Вопросы-ответы</div>
    </div>
    <?if($count):?>
        <div class="b-widgets_footer__height">
            <div class="b-widgets_footer__padding">
                <div class="b-widgets_footer__faq">
                    <?foreach($arResult["ITEMS"] as $i=>$arItem):?>
                        <?php
                            $question = $arItem["PREVIEW_TEXT"];
                            $question = str_replace('&nbsp;', ' ', $question);
                            $question = preg_replace('/[\n\r\t\s]+/', ' ', $question);
                            $question = trim($question);
                            $question = strip_tags($question);

                            $answer = $arItem["DETAIL_TEXT"];
                            $answer = str_replace('&nbsp;', ' ', $answer);
                            $answer = preg_replace('/[\n\r\t\s]+/', ' ', $answer);
                            $answer = strip_tags($answer);
                            if (strlen($answer) > 100) {
                                $answer = trim(mb_substr($answer, 0, 100)).'...';
                            }
                        ?>
                        <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="b-widgets_footer__faq-item">
                            <span class="b-widgets_footer__faq-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                            <span class="b-widgets_footer__faq-name"><?=$arItem["NAME"]?></span>
                            <span class="b-widgets_footer__faq-question">&ndash; <?=$question?></span>
                            <span class="b-widgets_footer__faq-answer">&ndash; <?=$answer?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="b-widget_footer__shadow"></div>
        </div>
        <div class="b-widgets_footer__padding">
            <div class="align-center b-widgets_footer__more"><a href="/qa/">Все вопросы</a></div>
        </div>
        <?php else: ?>
        <div class="b-widgets_footer__padding">
            <div class="b-widgets_footer__noitems b-widgets_footer__faq  link_ask_direction">
                <div class="b-widgets_footer__noitems-pic"></div>
                <div class="b-widgets_footer__noitems-txt">Задайте свой <br>вопрос врачу!</div>
                <a href="/reviews/<?php echo $urlSuffix ? '?'.$urlSuffix : ''; ?>" class="b-btn_white in-bl">Задать вопрос</a>
            </div>
        </div>
        <?php endif; ?>
</div>
