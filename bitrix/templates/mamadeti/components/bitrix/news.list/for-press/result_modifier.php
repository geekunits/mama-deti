<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
foreach($arResult['ITEMS'] as &$arItem) {
	$pictureId = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
	if (!empty($pictureId)) {
		$arPicture = CFile::ResizeImageGet($pictureId, 
			array('width'=> $pictureWidth,'height'=> $pictureHeight), 
			BX_RESIZE_IMAGE_EXACT, 
			true);
	} else {
		$arPicture = null;
	}
	$arItem['PICTURE'] = array_change_key_case($arPicture, CASE_UPPER);
	$arItem['PROPERTIES'] = ForPressHelper::getElementProperties($arItem['ID']);
	if (!empty($arItem['PROPERTIES']['PRESS_RELEASE_FILE']['VALUE'])) {
		$arItem['PRESS_RELEASE_FILE'] = CFile::GetFileArray($arItem['PROPERTIES']['PRESS_RELEASE_FILE']['VALUE']);
		$arItem['PRESS_RELEASE_FILE']['DESCRIPTION'] = $arItem['PROPERTIES']['PRESS_RELEASE_FILE']['DESCRIPTION'];
	}
}