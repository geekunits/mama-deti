<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
    global $arrNewsFilter;
    $clinicId = null;
    if (!empty($arrNewsFilter) && !empty($arrNewsFilter['PROPERTY_CLINIC'])) {
        $clinicId = $arrNewsFilter['PROPERTY_CLINIC'];
    }
    $dateFormat = 'd.m.Y';
    $timeFormat = 'h:i:s';
?>
<?php if ($arResult['ITEMS']): ?>
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
    <div class="b-clinic_actions__wrap">
        <?php foreach($arResult["ITEMS"] as $index => $arItem):?>
            <?php 
                $index += 1;
                $dtActiveTo = null;
                $dtActiveFrom = null;
                if (!empty($arItem['ACTIVE_TO'])) {
                     $dtActiveTo = \DateTime::createFromFormat($dateFormat, $arItem['ACTIVE_TO']);
                }
                if (!empty($arItem['ACTIVE_FROM'])) {
                     $dtActiveFrom = \DateTime::createFromFormat($dateFormat . ' ' . $timeFormat, $arItem['ACTIVE_FROM']);
                     if (!$dtActiveFrom) {
                        $dtActiveFrom = \DateTime::createFromFormat($dateFormat, $arItem['ACTIVE_FROM']);
                     }
                }
                $imageData = null;
                if (!is_array($arItem['PREVIEW_PICTURE'])) {
                    $arItem['PREVIEW_PICTURE'] = $arItem['DETAIL_PICTURE'];
                }
                if (is_array($arItem['PREVIEW_PICTURE'])) {
                    $imageData = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width'=>325,'height'=>183), BX_RESIZE_IMAGE_EXACT, true);
                }
                $detailPageUrl = $arItem['DETAIL_PAGE_URL'];
                if (!empty($clinicId)) {
                    $detailPageUrl .= '?clinic=' . $clinicId;
                }
            ?>
            <?php if ($index % 2 === 1): ?>
                <div class="b-columns_two b-news_item clearfix">
            <?php endif; ?>
            <div class="b-column">
                 <a href="<?php echo $detailPageUrl; ?>" class="b-news_link">
                     <?php if ($imageData): ?>
                            <span class="b-news_pic">
                                <img src="<?php echo $imageData['src']; ?>" alt="<?php echo $arItem['NAME']; ?>" alt="<?php echo $arItem['TITLE']; ?>">
                            </span>
                        <?php endif; ?>
                        <?php if ($dtActiveFrom): ?>
                            <span class="b-news_date"><?php echo $dtActiveFrom->format($dateFormat); ?></span>
                        <?php endif; ?>
                        <span class="b-news_name"><?php echo $arItem['NAME']; ?></span>
                        <span class="b-news_txt"><?php echo strip_tags($arItem["PREVIEW_TEXT"]); ?></span>
                    </a>
                </div>
            <?php if ($index % 2 === 0): ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if ($index % 2 === 1): ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?php echo $arResult["NAV_STRING"]?>
    <?php endif;?>
<?else:?>
    На данный момент новостей нет
<?endif;?>