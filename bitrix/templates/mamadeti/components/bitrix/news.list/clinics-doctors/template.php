<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

    $arFilter = !empty($arParams['FILTER_NAME']) && isset($GLOBALS[$arParams['FILTER_NAME']]) ? 
        $GLOBALS[$arParams['FILTER_NAME']] : [];
    $clinicId = isset($arFilter['PROPERTY_CLINIC']) ? $arFilter['PROPERTY_CLINIC'] : null;
?>
<?php if (!empty($arResult['ITEMS'])): ?>
    <?php if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br />
    <?php endif; ?>
    <div class="b-clinic_doctors-list">
        <?php foreach ($arResult['ITEMS'] as $arItem): ?>
            <?php
            $detailPageUrl = $arItem['DETAIL_PAGE_URL'] . '?clinic=' . $GLOBALS["arrFilterDoctor"]['PROPERTY_CLINIC'];
            $prevSecTitle = "";
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
            switch ($arParams["ELEMENT_SORT_FIELD"]) {
                case 'PROPERTY_SPECIALTY.NAME': $curSecTitle = $arItem["PROPERTY_SPECIALTY_NAME"];
                    break;
                case 'PROPERTY_CLINIC.NAME': $curSecTitle = $arItem["PROPERTY_CLINIC_NAME"];
                    break;
                default: $curSecTitle = '';
                    break;
            }

            if ($prevSecTitle != $curSecTitle) {
                // echo '<li><span class="name-title red-color">' . $curSecTitle . "</span></li>";
                $prevSecTitle = $curSecTitle;
            }
            $imgSrc = null;
            if (is_array($arItem["PREVIEW_PICTURE"])) {
                $imgSrc = MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z" => 1, "w" => 190, "h" => 210));
            } elseif ($arItem["PROPERTIES"]["SEX"]["VALUE_XML_ID"] === "F") {
                $imgSrc = SITE_TEMPLATE_PATH . '/images/female.gif';
            } else {
                $imgSrc = SITE_TEMPLATE_PATH . '/images/male.gif';
            }
            ?>
            <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="b-clinic_doctors-list__item">
                <div class="b-clinic_doctors__part-left">
                    <div class="b-clinic_doctors__part-left">
                        <div class="b-doctor b-doctor_extra-big">
                            <a class="b-doctor_avatar" href="<?= $detailPageUrl; ?>" target="_blank">
                                <img src="<?= $imgSrc; ?>" alt="<?= $arItem["NAME"] ?>">
                            </a>
                        </div>
                        <div class="b-clinic_doctors__buttons">
                            <a href="#" data-name="<?= $arItem["NAME"] ?>" data-clinic="<?= $GLOBALS["arrFilterDoctor"]['PROPERTY_CLINIC']; ?>" data-doctor="<?= $arItem["ID"]; ?>" class="b-btn_white b-clinic_doctors__btn js-ask_question_doctors_btn">
                                Задать вопрос врачу
                            </a>
                            <?php if (empty($arItem['PROPERTIES']['HIDDEN_BUTTON_BOOK']['VALUE'])): ?>
                                <a href="#" class="b-btn_white b-clinic_doctors__btn js-receiption_doctors_btn" <?= count($arItem['PROPERTY_9']) == 1 ? 'data-clinic="' . $arItem['PROPERTY_9'][0] . '"' : ''; ?> data-doctor="<?= $arItem["ID"]; ?>">
                                    Записаться на прием
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="b-clinic_doctors__part-right">
                    <div class="b-clinic_doctors__description js-clinic_doctors__description">
                        <div class="b-clinic_doctors__description-name">
                            <a href="<?= $detailPageUrl; ?>" target="_blank"><?= $arItem["NAME"] ?></a>
                        </div>
                        <div class="b-clinic_doctors__description-list">
                            <?php if (is_array($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"])): ?>
                                <div class="b-clinic_doctors__description-list___item">
                                    <div class="b-clinic_doctors__description-title">Специальности:</div>
                                    <?php foreach ($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"] as $arValue): ?>
                                        <div class="b-clinic_doctors__description-value">
                                            <?= $arValue["NAME"] ?>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            <?php endif; ?>
                            <?php if (is_array($arItem["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SERVICES"]["VALUE"])): ?>
                                <div class="b-clinic_doctors__description-list___item">
                                    <div class="b-clinic_doctors__description-title">Услуги:</div>
                                    <div class="js-clinic_doctors__service-list">
                                        <?php foreach ($arItem["PROPERTIES"]["SERVICES"]["VALUE"] as $arValue): ?>
                                            <div class="b-clinic_doctors__description-value js-clinic_doctors__service-value">
                                                <a href="<?= $arValue["DETAIL_PAGE_URL"] ?>">- <?= $arValue["NAME"] ?></a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if (is_array($arItem["PROPERTIES"]["SECTION"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SECTION"]["VALUE"])): ?>
                                <div class="b-clinic_doctors__description-list___item">
                                    <div class="b-clinic_doctors__description-title">Отделение:</div>
                                    <div class="b-clinic_doctors__description-value">
                                        <?php foreach ($arItem["PROPERTIES"]["SECTION"]["VALUE"] as $key => $arValue): ?>
                                            <?php if ($key > 0): ?>, <?php endif; ?>
                                            <?= $arValue["NAME"] ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="b-clinic_doctors__description-list___item">
                                <div class="b-clinic_doctors__description-title">Ведет прием в:</div>
                                <?php foreach($arItem['CLINIC'] as $clinicName): ?>
                                    <div class="b-clinic_doctors__description-value">
                                        <?= $clinicName; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?>
    <?php endif; ?>
    </div>

<?php else: ?>
    <span class="name-title red-color">Для вашего города докторов не найдено.</span>
<?php endif; ?>
