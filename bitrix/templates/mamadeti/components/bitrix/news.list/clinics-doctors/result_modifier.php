<?
CModule::IncludeModule("iblock");
foreach($arResult["ITEMS"] as $key=>$arItem){
	$arResult["ITEMS"][$key]["CLINIC"] = '';
	$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "ID"=>$arItem["PROPERTIES"]["CLINIC"]["VALUE"]);
	$res = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
	while($arres = $res->GetNext())
	{
		$arResult["ITEMS"][$key]["CLINIC"][] = '<a class="red-color" href="'.$arres["DETAIL_PAGE_URL"].'">'.$arres["NAME"].'</a>';
	}

	$arResult["ITEMS"][$key]["SECTION"] = '';
	$ressect = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
	if($arressect = $ressect->GetNext()) $arResult["ITEMS"][$key]["SECTION"] = '<a href="'.$arressect["SECTION_PAGE_URL"].'" class="red-color">'.$arressect["NAME"].'</a>';

	$arResult["ITEMS"][$key]["STATISTICS"] = array(
		"REVIEW" => 0,
		"STORY" => 0,
		"QA" => 0,
		"PHOTO" => 0
	);

	$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arItem["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	$arResult["ITEMS"][$key]["STATISTICS"]["REVIEW"] = $res->SelectedRowsCount();

	$arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arItem["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	$arResult["ITEMS"][$key]["STATISTICS"]["STORY"] = $res->SelectedRowsCount();

	$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arItem["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	$arResult["ITEMS"][$key]["STATISTICS"]["QA"] = $res->SelectedRowsCount();

	$arFilter = Array("IBLOCK_ID"=>13, "ACTIVE"=>"Y", "PROPERTY_DOCTORS"=>$arItem["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	$arResult["ITEMS"][$key]["STATISTICS"]["PHOTO"] = $res->SelectedRowsCount();

	$arResult["ITEMS"][$key]["STATISTICS"]["SHOW"] = (
		$arResult["ITEMS"][$key]["STATISTICS"]["REVIEW"]>0 || 
		$arResult["ITEMS"][$key]["STATISTICS"]["STORY"]>0 ||
		$arResult["ITEMS"][$key]["STATISTICS"]["QA"]>0 ||
		$arResult["ITEMS"][$key]["STATISTICS"]["PHOTO"]>0);

	if (is_array($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]))
	{
		$arFilter = array(
			"IBLOCK_ID"=>$arItem["PROPERTIES"]["SPECIALTY"]["LINK_IBLOCK_ID"],
			"ACTIVE"=>"Y",
			"ID" => array_values($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]),
			);
		$arResult["ITEMS"][$key]["PROPERTIES"]["SPECIALTY"]["VALUE"] = array();
		$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL"));
		while ($arElement = $rsElement->GetNext())
		{
			$arResult["ITEMS"][$key]["PROPERTIES"]["SPECIALTY"]["VALUE"][] = $arElement;
		}
	}

	if (is_array($arItem["PROPERTIES"]["SECTION"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SECTION"]["VALUE"]))
	{
		$arFilter = array(
			"IBLOCK_ID"=>$arItem["PROPERTIES"]["SECTION"]["LINK_IBLOCK_ID"],
			"ACTIVE"=>"Y",
			"ID" => array_values($arItem["PROPERTIES"]["SECTION"]["VALUE"]),
			);
		$arResult["ITEMS"][$key]["PROPERTIES"]["SECTION"]["VALUE"] = array();
		$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL"));
		while ($arElement = $rsElement->GetNext())
		{
			$arResult["ITEMS"][$key]["PROPERTIES"]["SECTION"]["VALUE"][] = $arElement;
		}
	}

	if (is_array($arItem["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SERVICES"]["VALUE"]))
	{
		$arFilter = array(
			"IBLOCK_ID"=>$arItem["PROPERTIES"]["SERVICES"]["LINK_IBLOCK_ID"],
			"ACTIVE"=>"Y",
			"ID" => array_values($arItem["PROPERTIES"]["SERVICES"]["VALUE"]),
			);

		$arTable = array(0,0,0);
		$row = 0;

		$arResult["ITEMS"][$key]["PROPERTIES"]["SERVICES"]["VALUE"] = array();
		$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL"));
		while ($arElement = $rsElement->GetNext())
		{
			$arTable[$row++]++;
			if ($row == 3) $row = 0;
			$arResult["ITEMS"][$key]["PROPERTIES"]["SERVICES"]["VALUE"][] = $arElement;
		}

		$arResult["ITEMS"][$key]["PROPERTIES"]["SERVICES_TABLE"] = $arTable;
	}


/*	if (is_array($arItem["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SERVICES"]["VALUE"]))
	{
		$arFilter = array(
			"IBLOCK_ID"=>$arItem["PROPERTIES"]["SERVICES"]["LINK_IBLOCK_ID"],
			"ACTIVE"=>"Y",
			"ID" => array_values($arItem["PROPERTIES"]["SERVICES"]["VALUE"]),
			);
		$arResult["ITEMS"][$key]["PROPERTIES"]["SERVICES"]["VALUE"] = array();
		$rsSection = CIBlockSection::GetList(array("SORT"=>"ASC"),$arFilter,false,array("IBLOCK_ID","ID","NAME","SECTION_PAGE_URL"));
		while ($arSection = $rsSection->GetNext())
		{
			$arResult["ITEMS"][$key]["PROPERTIES"]["SERVICES"]["VALUE"][] = $arSection;
		}
	}*/
}

?>
