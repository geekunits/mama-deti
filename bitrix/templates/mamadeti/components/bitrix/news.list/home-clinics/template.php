<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="main-holder">
	<div class="content-block">
		<div class="content-section">
			<div class="info-block align-center">
				<span class="title-block"><strong>23 клиники</strong><span> в 11 городах</span></span>
				<div class="block-text">
					<p>
						Чтобы выбрать клинику нажмите на кнопку “Выбрать клинику”
					</p>
				</div>
				<div class="button-holder">
					<a href="/clinics/" class="button02">Выбрать клинику</a>
				</div>
			</div>
			<div class="gallery-block">
				<div class="item-gallery">
					<a href="#" class="prev">prev</a>
					<a href="#" class="next">next</a>
					<div class="gallery-frame">
						<ul class="gallery">
						<?foreach($arResult["ITEMS"] as $arItem):?>
							<li class="slide">
								<div class="item-block">
									<div class="item-image">
										<img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z"=>1, "w"=>190, "h"=>230))?>" alt="<?=$arItem["NAME"]?>">
									</div>
									<div class="block-holder">
										<span class="name-title"><?=$arItem["NAME"]?></span>
										<div class="item-holder">
											<div class="left-item">
												<?if(!empty($arItem["PROPERTIES"]["POST_FULL"]["VALUE"])):?>
												<div class="item-text">
													<p><?=$arItem["PROPERTIES"]["POST_FULL"]["VALUE"]?></p>
												</div>
												<?endif?>
												<?if(!empty($arItem["PREVIEW_TEXT"])):?>
													<span class="strong-text"><?=$arItem["PREVIEW_TEXT"]?></span>
												<?endif?>
											</div>
											<?/*<div class="right-item">
												<ul class="item-list">
													<li>
														<a href="/reviews/?doctor=<?=$arItem["ID"]?>"><span class="count"><?=$arItem["STATISTICS"]["REVIEW"]?></span><span class="title">Отзывов</span></a>
													</li>
													<li>
														<a href="/story/?doctor=<?=$arItem["ID"]?>"><span class="count"><?=$arItem["STATISTICS"]["STORY"]?></span><span class="title">Историй со счастливым концом</span></a>
													</li>
													<li>
														<a href="/qa/?doctor=<?=$arItem["ID"]?>"><span class="count"><?=$arItem["STATISTICS"]["QA"]?></span><span class="title">Ответов на вопросы</span></a>
													</li>
												</ul>
											</div>*/?>
										</div>
										<div class="block-holder">
											<div class="left-item">
												<ul class="links-list">
													<li>
														<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="red-color">Подробнее о клинике</a>
													</li>
													<li>
														<a href="/ajax/clinic_location.php?id=<?=$arItem["ID"]?>" class="fancybox-map" data-fancybox-type="iframe">Показать на карте</a>
													</li>
												</ul>
											</div>
											<div class="right-item">
												<a href="#" class="button01 link_reception_clinic" data-clinic="<?=$arItem["ID"]?>">Записаться на прием</a>
											</div>
										</div>
									</div>
								</div>
							</li>	
						<?endforeach;?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
