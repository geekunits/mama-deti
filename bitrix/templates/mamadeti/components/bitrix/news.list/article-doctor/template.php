<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult["ITEMS"])):?>
	<?php global $arrFilterDoctor; ?>
	<span class="name-title">Другие статьи <?php if (count($arrFilterDoctor['PROPERTY_DOCTOR']) > 1): ?>авторов<?php else: ?>автора<?php endif; ?></span>
	<ul class="articles-listing">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<li>
			<div class="block-holder">
				<div class="red-onhover add-able">
					<div class="item-visual">
					<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){?>
						<img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z"=>1,"w"=>140,"h"=>100))?>"  alt="<?=$arItem["NAME"]?>"><span class="image-mask"><a href="#" class="add-link"></a></span>
					<?}?>	
					</div>
					<div class="block-holder">
						<div class="item-link">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
						</div>
						<div class="text-item">
							<p><?=$arItem["PREVIEW_TEXT"]?></p>
						</div>
					</div>
				</div>
			</div>
		</li>	
	<?endforeach;?>
	</ul>
<?endif;?>