<?php

foreach($arResult['ITEMS'] as &$arItem) {
	$arFilter = array(
		'IBLOCK_ID' => 3,
		'ID' => $arItem['PROPERTIES']['DOCTOR']['VALUE'],
	);
	$arSelect = array(
		'IBLOCK_ID',
		'ID',
		'PROPERTY_SEX',
		'NAME',
		'PREVIEW_PICTURE',
		'DETAIL_PICTURE',
		'DETAIL_PAGE_URL',
	);
	$arNavStartParams = array(
		'nTopCount' => 1
	);
	$shortText = strip_tags($arItem['PREVIEW_TEXT']);
	$arItem['SHORT_TEXT'] = strlen($shortText) > 150 ? substr($shortText, 0, 147) . '...' : $shortText;
	$dbDoctor = CIBlockElement::GetList(array(), $arFilter, false, $arNavStartParams, $arSelect);
	$arDoctor = $dbDoctor->GetNext();
	if ($arDoctor) {
        $fileId = !empty($arDoctor["PREVIEW_PICTURE"]) ? $arDoctor["PREVIEW_PICTURE"] : $arDoctor["DETAIL_PICTURE"];
    	if (!empty($fileId)) {
    		$arDoctor['THUMB_PICTURE'] = CFile::ResizeImageGet(intval($fileId), array('width' => 45, 'height' => 45), BX_RESIZE_EXACT, true);
    	} 
    	$parts = explode(' ', trim($arDoctor['NAME']));
    	if (count($parts === 3)) {
    		list($surname, $name, $secondname) = $parts;
    		$arDoctor['SHORT_NAME'] = $surname . ' ' . mb_substr($name, 0, 1) . '. ' . mb_substr($secondname, 0, 1) . '.';
    	} else {
    		$arDoctor['SHORT_NAME'] = $arDoctor['NAME'];
    	}
	}
	$arItem['AUTHOR'] = $arDoctor;
}