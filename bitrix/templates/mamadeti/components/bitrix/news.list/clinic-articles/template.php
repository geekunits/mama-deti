<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?php if (!empty($arResult['ITEMS'])): ?>
    <div class="b-bg_white clinic-articles clinic-articles--main b-clinic_card__padding">    
        <div class="align-center b-header_h2">Статьи</div>
        <div class="b-columns_forth clearfix">
            <?php foreach ($arResult['ITEMS'] as $arItem): ?>

                <?php
                    $detailPageUrl = $arItem['DETAIL_PAGE_URL'];
                    if (!empty($arParams['CLINIC_ID'])) {
                        $detailPageUrl .= '?clinic=' . $arParams['CLINIC_ID'];
                    }
                ?>

                <div class="b-column">
                    <div class="clinic-articles__name"><a href="<?php echo $detailPageUrl; ?>"><?php echo $arItem['NAME']; ?></a></div>
                    <div class="clinic-articles__intro"><?php echo $arItem['SHORT_TEXT']; ?></div>
                    <?php if(!empty($arItem['AUTHOR'])): ?>
                        <div class="b-doctor b-doctor_small">
                            <div class="b-doctor_part-left">
                                <a class="b-doctor_avatar<?php echo $arItem['AUTHOR']['PROPERTY_SEX_VALUE'] === 'Ж' ? ' b-doctor_nophoto__female' : ' b-doctor_nophoto__male'?>" href="<?php echo $arDoctor['DETAIL_PAGE_URL']; ?>">
                                    <?php if (isset($arItem['AUTHOR']['THUMB_PICTURE'])): ?>
                                        <img src="<?php echo $arItem['AUTHOR']['THUMB_PICTURE']['src']; ?>" alt="<?php echo $arItem['AUTHOR']['SHORT_NAME']; ?>">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <?php if (!empty($arItem['AUTHOR'])): ?>
                                <a href="<?php echo $arItem['AUTHOR']['~DETAIL_PAGE_URL']; ?>" class="b-doctor_intro">
                                    <span class="b-doctor_text">Автор:</span>
                                    <span class="b-doctor_name"><?php echo $arItem['AUTHOR']['SHORT_NAME']; ?></span>
                                </a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>  
            <?php endforeach; ?>
        </div>
        <div class="align-center">
            <a class="b-link_more in-bl js-tab" href="<?php echo $arParams['CLINIC_URL']; ?>">Показать еще</a>
        </div>
    </div>
<?php endif; ?>