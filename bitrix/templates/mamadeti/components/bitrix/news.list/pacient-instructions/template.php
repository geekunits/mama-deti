<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php if ($arResult['ITEMS']): ?>
    <div class="instructions">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?php $file = $arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"];?>
            <a href="<?=$file["SRC"]?>" class="instructions__item" target="_blank">
                <span class="instructions__ico"><?=pathinfo($file['FILE_NAME'], PATHINFO_EXTENSION); ?></span>
                <?=$arItem["NAME"]?>
            </a>
        <?endforeach;?>
    </div>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
<?php endif; ?>
