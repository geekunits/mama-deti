<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
	global $currentClinicId;
	global $currentClinicCityId;
	$urlSuffix = array('city' => $currentClinicCityId, 'clinic' => $currentClinicId);
	$urlSuffix = array_filter($urlSuffix);
	$urlSuffix = http_build_query($urlSuffix);
	$count=count($arResult["ITEMS"]);
?>

<div class="b-bg_white b-widgets_footer__col">
    <div class="b-widgets_footer__padding">
        <div class="align-center b-header_h2">Счастливые истории</div>

        <?if($count):?>
	        <div class="b-widgets_footer__height">
	            <div class="b-widgets_footer__history">
	            	<?foreach($arResult["ITEMS"] as $i=>$arItem):?>
		                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="b-widgets_footer__history-item">
		                    <span class="b-widgets_footer__history-name"><?=$arItem["NAME"]?></span>
		                    <?php if ($arItem['DOCTOR']): ?>
			                    <span class="b-widgets_footer__history-doctor">
			                        <span class="in-bl">Врач:</span><!--
			                     --><span class="b-widgets_footer__pic in-bl<?php 
			                     		if (!$arItem['DOCTOR']['THUMB_PICTURE']): ?>
                                            <?if ($arItem['DOCTOR']["PROPERTIES"]["SEX"]["VALUE_XML_ID"] == "F"):?>
                                                b-doctor_nophoto__female
                                            <?else:?>
                                               b-doctor_nophoto__male
                                            <?endif?>
			                     		<?php endif; ?>">
			                     		<?php if ($arItem['DOCTOR']['THUMB_PICTURE']): ?><img src="<?php echo $arItem['DOCTOR']['THUMB_PICTURE']['SRC']; ?>" alt=""><?php endif; ?>
			                     	</span><!--
			                     --><span class="b-widgets_footer__fio in-bl"><?php echo $arItem['DOCTOR']['SHORT_NAME']; ?></span>
			                    </span>
			                <?php endif; ?>
		                    <span class="b-widgets_footer__history-txt"><?=strip_tags($arItem["PREVIEW_TEXT"])?></span>
		                </a>
		            <?php endforeach; ?>
	            </div>
	            <div class="b-widget_footer__shadow"></div>
	        </div>
	        <div class="align-center b-widgets_footer__more"><a href="/story/<?php echo $urlSuffix ? '?'.$urlSuffix : ''; ?>">Все истории (<?php echo $arResult['NAV_RESULT']->NavRecordCount; ?>)</a></div>
	    <?php else: ?>
	    	<div class="b-widgets_footer__noitems b-widgets_footer__history link_review">
	            <div class="b-widgets_footer__noitems-pic"></div>
	            <div class="b-widgets_footer__noitems-txt">Расскажите свою <br>счастливую историю!</div>
	            <a href="/story/<?php echo $urlSuffix ? '?'.$urlSuffix : ''; ?>" class="b-btn_white in-bl">Поделиться</a>
	        </div>
	    <?php endif; ?>
    </div>
</div>