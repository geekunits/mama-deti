<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="b-news_item clearfix">
    <?php if ($arResult['ITEMS']): ?>
        <?php foreach($arResult['ITEMS'] as $arItem): ?>
            <?php
                $imageData = null;
                if (!is_array($arItem['PREVIEW_PICTURE'])) {
                    $arItem['PREVIEW_PICTURE'] = $arItem['DETAIL_PICTURE'];
                }
                if (is_array($arItem['PREVIEW_PICTURE'])) {
                    $imageData = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>325,'height'=>183), BX_RESIZE_IMAGE_EXACT, true);
                }
            ?>
            <a class="b-news_col" href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>">
                <?php if ($imageData): ?>
                    <span class="b-news_pic">
                        <img src="<?=$imageData['src']?>"
                                alt="<?=$arItem["NAME"]?>"
                                title="<?=$arItem["NAME"]?>"
                                >
                    </span>
                <?php endif; ?>
                <span class="b-news_date"><?php echo $arItem['DISPLAY_PROPERTIES']['DATE_FROM']['DISPLAY_VALUE']; ?></span>
                <span class="b-news_name"><?php echo $arItem['NAME']; ?></span>
                <span class="b-news_txt"><?php echo strip_tags($arItem["PREVIEW_TEXT"]);?></span>
            </a>
        <?php endforeach; ?>
    <?else:?>
        <div style="text-align:center">На данный момент новостей нет</div>
    <?endif;?>
</div>
<?php if ($arResult['ITEMS']): ?>
    <div class="align-center">
        <a class="b-link_more in-bl js-tab" href="<?php echo $arParams['CLINIC_URL']; ?>">Показать еще</a>
    </div>
<?endif;?>