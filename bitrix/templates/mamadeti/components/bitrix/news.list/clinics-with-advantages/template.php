<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php
    $maps = [];

    $city = CityManager::getInstance()->getCityById(GetCurrentCity());
    $titles = [
        'hospitals' => 'Госпитали «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
        'clinics' => 'Клиники «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
    ];
    $buttonTitles = [
        'hospitals' => 'Подробнее о госпитале',
        'clinics' => 'Подробнее о клинике',
    ];
    $buttonTitlesPic = [
        'hospitals' => 'Всё о госпитале',
        'clinics' => 'Всё о клинике',
    ];
?>
    <?php $mapId = 'map-common'; ?>

    <?php
        $clinicCollections = ['hospitals' => [], 'clinics' => []];

        foreach ($arResult['ITEMS'] as $arItem) {
            $clinicCollections[$arItem['PROPERTIES']['IS_HOSPITAL']['VALUE'] ? 'hospitals' : 'clinics'][] = $arItem;
        }

        $clinicCollections = array_filter($clinicCollections);
    ?>

    <div class="b-bg_white b-clincs_main__wrap">
        <h1 class="align-center b-header_h1">«Мать и дитя» в Москве</h1>
        <div class="b-clincs_main__map-moscow">
            <div class="b-clincs_main__map-clinics">
                <div class="b-clincs_main__map-clinics__list">
                    <?php foreach ($clinicCollections as $type => $clinics): ?>
                        <?php foreach ($clinics as $index => $clinic): ?>
                            <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" data-placemark="<?php echo $clinic["ID"]; ?>" class="js-main-clinic-map"><?php echo $clinic['NAME']; ?></a>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="b-clincs_main__map l-loader" id="<?php echo $mapId; ?>"></div>
        </div>
        <?php foreach ($clinicCollections as $type => $clinics): ?>

            <div class="b-clincs_main__more">
                <div class="align-center b-header_h1"><?php echo $titles[$type]; ?></div>
                <div class="b-clincs_main__item clearfix">
                    <?php if ($type == 'hospitals'): ?>
                    <div class="b-clincs_main__col b-clincs_main__col-three">
                        <div class="b-clincs_main__adv">
                            <div class="b-clincs_main__adv-txt">Мать и дитя</div>
                            <div class="b-clincs_main__adv-cubics"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-2"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-3"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-4"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-5"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-6"></div>
                        </div>
                        <ul class="b-clincs_main__adv-list">
                            <li>1,2 млн. посещений в год</li>
                            <li>родилось  более 35000  детей</li>
                            <li>№1 в России по числу циклов ЭКО</li>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php foreach ($clinics as $index => $clinic): ?>
                        <?php
                            $image = null;
                            if ($clinic['PREVIEW_PICTURE']) {
                                $image =
                                    CFile::ResizeImageGet(
                                        $clinic['PREVIEW_PICTURE']['ID'],
                                        ['height' => 175, 'width' => 316],
                                        BX_RESIZE_IMAGE_EXACT
                                    );
                                $image = $image['src'];
                            }
                        ?>

                        <div class="b-clincs_main__col b-clincs_main__col-three js-main-clinic-item">
                            <span class="b-clincs_main__pic js-main-clinic-hover">
                                <?php if ($image): ?>
                                    <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                                <?php endif; ?>
                                <span class="b-clincs_main__pic-overlay">
                                    <?php echo $buttonTitles[$type]; ?>
                                </span>
                            </span>
                            <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-clincs_main__link">
                                <span class="b-clincs_main__name"><?php echo $clinic['NAME']; ?></span>
                                <span class="b-clincs_main__address">
                                    <?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                                </span>
                            </a>
                            <div class="b-clinics_main__popup js-main-clinic-popup">
                                <div class="b-clincs_main__item clearfix">
                                    <div class="b-clincs_main__col b-clincs_main__col-three">
                                        <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-clincs_main__link">
                                            <span class="b-clincs_main__pic">
                                                <?php if ($image): ?>
                                                    <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                                                <?php endif; ?>
                                                <span class="b-clincs_main__pic-btn"><?php echo $buttonTitlesPic[$type]; ?></span>
                                            </span>
                                            <span class="b-clincs_main__name"><?php echo $clinic['NAME']; ?></span>
                                            <span class="b-clincs_main__address">
                                                <strong>Адрес:</strong><br>
                                                <?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                                            </span>
                                        </a>
                                        <?php
                                            $phones = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]);
                                            $phoneDescriptions = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]);
                                            $email = trim($clinic["DISPLAY_PROPERTIES"]["CONTACT_EMAIL"]["VALUE"]);
                                        ?>
                                        <?php if (!empty($phones)): ?>
                                            <div class="b-clinics_main__phone">
                                                <?php foreach($phones as $key => $phone): ?>
                                                    <?php
                                                        $isEqual = PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phone);
                                                        if($isEqual){
                                                            $phone = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                                                        }
                                                    ?>
                                                    <div>
                                                        <span class="js-phone" itemprop="telephone"><?php echo $phone; ?></span>
                                                        <?php echo empty($phoneDescriptions[$key]) ? '' : '<span class="b-clinics_main__phone-note">'.$phoneDescriptions[$key].'</span>'; ?>
                                                        <?php if ($isEqual): ?>
                                                            <?php if(CLIENT_FROM_ABROAD): ?>
                                                                <span class="phone-free">звонок из-за рубежа</span>
                                                            <?php else: ?>
                                                                <span class="phone-free">звонок бесплатный</span>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                                <span data-clinic-id="<?php echo $clinic['ID']; ?>" class="b-clinics_main__phone-call js-show-call-me js-goal" data-target="<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">Перезвоните мне</span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!empty($email)): ?>
                                            <div class="b-clinics_main__mail">
                                                <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="b-btn_white in-bl link_reception_clinic js-goal" data-clinic="<?php echo $clinic['ID']; ?>" data-name="<?php echo $clinic['NAME']; ?>" data-target="Appointment_<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">
                                            Записаться на приём
                                        </div>
                                    </div>
                                    <?
                                        if(count($clinic['ACTIONS'])){
                                            ?>
                                                <div class="b-clincs_main__col b-clincs_main__col-three">
                                                    <?
                                                        foreach($clinic['ACTIONS'] AS $arAction){
                                                            ?>
                                                                <div class="b-news_item">
                                                                    <a class="b-news_link" href="<?=$arAction['DETAIL_PAGE_URL'];?>">
                                                                        <span class="b-news_pic">
                                                                            <img src="<?php echo $arAction['DETAIL_PICTURE']['src']; ?>" alt="<?=$arAction['NAME'];?>">
                                                                            <?php if ($arAction['DAYS_SOON']): ?>
                                                                                <span class="b-clinic_action__days--soon">
                                                                                    до начала акции <strong><?php echo $arAction['DAYS_SOON']; ?></strong>
                                                                                    <?php echo plural($arAction['DAYS_SOON'], ['день', 'дня', 'дней']) ;?>
                                                                                </span>
                                                                            <?php elseif ($arAction['DAYS_LEFT'] > 1): ?>
                                                                                <span class="b-clinic_action__days">
                                                                                    до окончания акции <strong><?php echo $arAction['DAYS_LEFT']; ?></strong>
                                                                                    <?php echo plural($arAction['DAYS_LEFT'], ['день', 'дня', 'дней']) ;?>
                                                                                </span>
                                                                            <?php elseif ($arAction['DAYS_LEFT'] === 1): ?>
                                                                                <span class="b-clinic_action__days">
                                                                                    <strong>Остался последний день</strong>
                                                                                </span>
                                                                            <?php endif; ?>
                                                                        </span>
                                                                        <?php if (!empty($arAction['DATE_RANGE'])): ?>
                                                                            <span class="b-news_date"><?php echo $arAction['DATE_RANGE'] ?></span>
                                                                        <?php endif; ?>
                                                                        <span class="b-news_name"><?=$arAction['NAME'];?></span>
                                                                    </a>
                                                                </div>
                                                            <?
                                                        }
                                                    ?>
                                                </div>
                                            <?
                                        }
                                    ?>
                                    <div class="b-clincs_main__col b-clincs_main__col-three">
                                        <?
                                        if($clinic['SERVICES']){
                                            ?>
                                            <div class="b-clinics_main__services-head">
                                                Услуги
                                            </div>
                                            <div class="b-clinics_main__services-list">
                                                <?
                                                foreach($clinic['SERVICES'] AS $arService){
                                                    ?>
                                                    <a href="<?=$arService['LINK'] . 'clinic/' . $clinic['CODE'] . '/';?>"><?=$arService['TEXT'];?></a>
                                                    <?
                                                }
                                                ?>
                                            </div>
                                            <?
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="b-clinics_main__popup-close js-main-clinic-popup-close"></div>
                            </div>
                        </div>

                        <?php if (!(($index + 1) % 3)): ?>
                            </div>
                            <div class="b-clincs_main__item clearfix">
                        <?php endif; ?>

                        <?php

                            $arClinicPhones = is_array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']) ? $clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE'] : array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']);
                            $arClinicDescriptions = is_array($clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']) ? $clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION'] : array($clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']);
                            $clinicPhones = '';
                            foreach($arClinicPhones as $phoneIndex => $phoneValue) {
                                if(PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phoneValue)){
                                    $phoneValue = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                                }

                                $phoneDescription = empty($arClinicDescriptions[$phoneIndex]) ? '' : ' (' . $arClinicDescriptions[$phoneIndex] . ')';
                                $clinicPhones .= '<span>' . $phoneValue . $phoneDescription . '<span><br>';
                            }

                            $maps[$mapId][] = [
                                'coords' => $clinic['PROPERTIES']['COORD']['VALUE'],
                                'name' => $clinic['NAME'],
                                'id' => $clinic['ID'],
                                'content' =>
                                    '<a href="'.$clinic['DETAIL_PAGE_URL'].'">'.$clinic['NAME'].'</a><br/>'
                                    . $clinicPhones
                                    . $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE'],
                            ];
                        ?>
                    <?php endforeach; ?>
                    <?php if (
                                ($type == 'clinics')
                                &&
                                    (
                                    (count($clinicCollections['clinics']) == 1)
                                    ||
                                        (
                                        (count($clinicCollections['clinics'])%2 == 0)
                                        &&
                                        (count($clinicCollections['clinics'])%3 != 0)
                                        )
                                    ||
                                        (count($clinicCollections['clinics'])%3 != 0)
                                    )
                        ): 
                    ?>
                    <div class="b-clincs_main__col b-clincs_main__col-three">
                        <div class="b-clincs_main__adv b-clincs_main__adv--orange">
                            <div class="b-clincs_main__adv-txt">Мать и дитя</div>
                            <div class="b-clincs_main__adv-cubics"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-2"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-3"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-4"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-5"></div>
                            <div class="b-clincs_main__adv-cubics b-clincs_main__adv-cubics-6"></div>
                        </div>
                        <ul class="b-clincs_main__adv-list">
                            <li>в 17 городах России</li>
                            <li>6 000 специалистов</li>
                            <li>График работы 24/7</li>
                        </ul>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>


<?php if ($maps): ?>
    <script type="text/javascript">
        ymaps.ready(function () {
            var map,
                placemarks = {};

            <?php foreach ($maps as $mapId => $placemarks): ?>
                <?php $firstCoords = $placemarks[0]['coords']; ?>
                map = new ymaps.Map (<?php echo json_encode($mapId) ?>, {
                    center: [<?php echo $firstCoords; ?>],
                    zoom: 9,
                    controls: ["smallMapDefaultSet"]
                });

                <?php foreach ($placemarks as $placemark): ?>
                    var placemark = new ymaps.Placemark([<?php echo $placemark['coords']; ?>], {
                            // iconContent: <?php echo json_encode($placemark['name']); ?>,
                            // hintContent: <?php echo json_encode($placemark['name']); ?>, 
                            balloonContent: <?php echo json_encode($placemark['content']); ?>
                            
                        });
                    placemarks[<?php echo $placemark['id'] ;?>] = placemark;
                    placemark.events.add('mouseenter', function(e) {
                        var placemark = e.get('target');
                        placemark.balloon.open();
                    });
                    placemark.events.add('mouseleave', function(e) {
                        var placemark = e.get('target');
                        // placemark.balloon.close();
                    });
                    map.geoObjects.add(placemark);
                <?php endforeach; ?>

                <?php if (count($placemarks) > 1): ?>
                    map.setBounds(map.geoObjects.getBounds());
                    map.setZoom(map.getZoom() - 1);
                <?php else: ?>
                    map.setZoom(13);
                <?php endif; ?>
            <?php endforeach; ?>
            $('#<?php echo $mapId; ?>').removeClass('l-loader');

            $('.js-main-clinic-map').mouseenter(function() {
                placemark = placemarks[$(this).data('placemark')];
                placemark.balloon.open();
            });
        });
    </script>
<?php endif; ?>
