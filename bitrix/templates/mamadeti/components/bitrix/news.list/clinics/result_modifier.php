<?
$arClinicIDs = array();

foreach($arResult['ITEMS'] AS $arItem){
    $arClinicIDs[] = $arItem['ID'];
}

$arActions = array();

//Акции
if(count($arClinicIDs)){
    $resElements = CIBlockElement::GetList(
        array(
            'PROPERTY_SHOW_ON_MAIN' => 'DESC',
            'ACTIVE_FROM' => 'DESC'
        ),
        array(
            'IBLOCK_ID' => 32,
            'PROPERTY_CLINIC' => $arClinicIDs,
            'ACTIVE' => 'Y',
            'ACTIVE_DATE' => 'Y'
        )
    );

    while($rsElement = $resElements->GetNextElement()){
        $arElement = $rsElement->GetFields();
        $arElement['PROPERTIES'] = $rsElement->GetProperties();

        if($arElement['PROPERTIES']['CLINIC']['VALUE']){
            foreach($arElement['PROPERTIES']['CLINIC']['VALUE'] AS $clinicID){
                if(isset($arActions[$clinicID]) && count($arActions[$clinicID]) == 2){
                    break;
                }

                $arElement['CLINIC_ID'] = $clinicID;
                $arActions[$clinicID][] = $arElement;
            }
        }
    }
}

$dateFormat = 'd.m.Y';
$timeFormat = 'h:i:s';

foreach($arResult['ITEMS'] AS &$arItem){
    $arItem['SERVICES'] = array();

    if($arItem['PROPERTIES']['SERVICES']['VALUE']){
        $arItem['SERVICES'] = array_filter(CMamaDetiAPI::GetSelectedServiceTreeMenu($arItem['PROPERTIES']['SERVICES']['VALUE']), function($arService){
            return $arService['DEPTH_LEVEL'] == 1;
        });
    }

    $arItem['ACTIONS'] = array();

    if(isset($arActions[$arItem['ID']])){
        foreach($arActions[$arItem['ID']] AS $arAction){
            $dateTo = $arAction['PROPERTIES']['DATE_TO']['VALUE'];
            $dateFrom = $arAction['PROPERTIES']['DATE_FROM']['VALUE'];

            $dtActiveTo = null;
            $dtActiveFrom = null;

            $dateTo = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateTo);
            $dateFrom = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateFrom);
            $dtNow = new \DateTime('now');
            $dtNow->setTime(0, 0, 0);

            if (
                !empty($dateTo)
                && ($dtActiveTo = \DateTime::createFromFormat($dateFormat, $dateTo))
            ) {
                $dtActiveTo->setTime(23, 59, 59);
            }
            if (!empty($dateFrom)) {
                $dtActiveFrom = \DateTime::createFromFormat($dateFormat, $dateFrom);
            }

            $daysLeft = false;
            $daysSoon = false;

            if ($dtActiveFrom) {
                $intervalSoon = ($dtNow < $dtActiveFrom) ? $dtActiveFrom->diff($dtNow) : false;
                $daysSoon = $intervalSoon ? intval($intervalSoon->format('%a')) : false;
            }

            if ($dtActiveTo) {
                $dtInterval = $dtActiveTo->diff($dtNow);
                $daysLeft = intval($dtInterval->format('%a')) + 1;
            }

            $arAction['DETAIL_PICTURE'] = CFile::ResizeImageGet($arAction['DETAIL_PICTURE'], array('width'=>325,'height'=>155), BX_RESIZE_IMAGE_EXACT, true);
            $dateRange = '';

            if (!empty($dateTo) && !empty($dateFrom)) {
                if ($dateTo == $dateFrom) {
                    $dateRange = 'Проводится ' . $dateTo;
                } else {
                    $dateRange = $dateFrom . ' - ' . $dateTo;
                }
            } elseif (!empty($dateTo)) {
                $dateRange = 'По ' . $dateTo;
            } elseif (!empty($dateFrom)) {
                $dateRange = 'С ' . $dateFrom;
            }

            $detailPageUrl = $arAction['DETAIL_PAGE_URL'];
            if (!empty($arAction['CLINIC_ID'])) {
                $detailPageUrl .= '?clinic=' . $arAction['CLINIC_ID'];
            }

            $arAction['DETAIL_PAGE_URL']    = $detailPageUrl;
            $arAction['DATE_RANGE']         = $dateRange;
            $arAction['DAYS_LEFT']          = $daysLeft;
            $arAction['DAYS_SOON']          = $daysSoon;

            $arItem['ACTIONS'][] = $arAction;
        }
    }
}

unset($arItem);
?>