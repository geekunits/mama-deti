<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php
    $maps = [];

    $city = CityManager::getInstance()->getCityById(GetCurrentCity());
    $titles = [
        'hospitals' => 'Госпитали «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
        'clinics' => 'Клиники «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
    ];
    $buttonTitles = [
        'hospitals' => 'Подробнее о госпитале',
        'clinics' => 'Подробнее о клинике',
    ];
    $buttonTitlesPic = [
        'hospitals' => 'Всё о госпитале',
        'clinics' => 'Всё о клинике',
    ];
?>

<?php if (count($arResult['ITEMS']) > 1): ?>

    <?php
        $clinicCollections = ['hospitals' => [], 'clinics' => []];

        foreach ($arResult['ITEMS'] as $arItem) {
            $clinicCollections[$arItem['PROPERTIES']['IS_HOSPITAL']['VALUE'] ? 'hospitals' : 'clinics'][] = $arItem;
        }

        $clinicCollections = array_filter($clinicCollections);
    ?>

    <div class="b-bg_white b-clincs_main__wrap">
        <?php foreach ($clinicCollections as $type => $clinics): ?>
            <?php $mapId = 'map-'.$type; ?>

            <div class="b-clincs_main__more">
                <div class="align-center b-header_h1"><?php echo $titles[$type]; ?></div>
                <div class="b-clincs_main__item clearfix">
                    <?php foreach ($clinics as $index => $clinic): ?>
                        <?php
                            $image = null;
                            if ($clinic['PREVIEW_PICTURE']) {
                                $image =
                                    CFile::ResizeImageGet(
                                        $clinic['PREVIEW_PICTURE']['ID'],
                                        ['height' => 175, 'width' => 316],
                                        BX_RESIZE_IMAGE_EXACT
                                    );
                                $image = $image['src'];
                            }
                        ?>

                        <div class="b-clincs_main__col b-clincs_main__col-three js-main-clinic-item">
                            <span class="b-clincs_main__pic js-main-clinic-hover">
                                <?php if ($image): ?>
                                    <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                                <?php endif; ?>
                                <span class="b-clincs_main__pic-overlay">
                                    <?php echo $buttonTitles[$type]; ?>
                                </span>
                            </span>
                            <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-clincs_main__link">
                                <span class="b-clincs_main__name"><?php echo $clinic['NAME']; ?></span>
                                <span class="b-clincs_main__address">
                                    <strong>Адрес:</strong><br><?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                                </span>
                                <span class="b-btn_white in-bl"><?php echo $buttonTitles[$type]; ?></span>
                            </a>
                            <div class="b-clinics_main__popup js-main-clinic-popup">
                                <div class="b-clincs_main__item clearfix">
                                    <div class="b-clincs_main__col b-clincs_main__col-three">
                                        <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-clincs_main__link">
                                            <span class="b-clincs_main__pic">
                                                <?php if ($image): ?>
                                                    <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                                                <?php endif; ?>
                                                <span class="b-clincs_main__pic-btn"><?php echo $buttonTitlesPic[$type]; ?></span>
                                            </span>
                                            <span class="b-clincs_main__name"><?php echo $clinic['NAME']; ?></span>
                                            <span class="b-clincs_main__address">
                                                <strong>Адрес:</strong><br>
                                                <?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                                            </span>
                                        </a>
                                        <?php
                                        $phones = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]);
                                        $phoneDescriptions = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]);
                                        $email = trim($clinic["DISPLAY_PROPERTIES"]["CONTACT_EMAIL"]["VALUE"]);
                                        ?>
                                        <?php if (!empty($phones)): ?>
                                            <div class="b-clinics_main__phone">
                                                <?php foreach($phones as $key => $phone): ?>
                                                    <?php
                                                    $isEqual = PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phone);
                                                    if($isEqual){
                                                        $phone = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                                                    }
                                                    ?>
                                                    <div>
                                                        <span class="js-phone" itemprop="telephone"><?php echo $phone; ?></span>
                                                        <?php echo empty($phoneDescriptions[$key]) ? '' : '<span class="b-clinics_main__phone-note">'.$phoneDescriptions[$key].'</span>'; ?>
                                                        <?php if ($isEqual): ?>
                                                            <?php if(CLIENT_FROM_ABROAD): ?>
                                                                <span class="phone-free">звонок из-за рубежа</span>
                                                            <?php else: ?>
                                                                <span class="phone-free">звонок бесплатный</span>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                                <?php if (!$clinic['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']): ?>
                                                    <span data-clinic-id="<?php echo $clinic['ID']; ?>" class="b-clinics_main__phone-call js-show-call-me js-goal" data-target="<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">Перезвоните мне</span>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (!empty($email)): ?>
                                            <div class="b-clinics_main__mail">
                                                <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="b-btn_white in-bl link_reception_clinic js-goal" data-clinic="<?php echo $clinic['ID']; ?>" data-name="<?php echo $clinic['NAME']; ?>" data-target="Appointment_<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">
                                            Записаться на приём
                                        </div>
                                    </div>
                                    <?
                                        if(count($clinic['ACTIONS'])){
                                            ?>
                                            <div class="b-clincs_main__col b-clincs_main__col-three">
                                                <?
                                                foreach($clinic['ACTIONS'] AS $arAction){
                                                    ?>
                                                    <div class="b-news_item">
                                                        <a class="b-news_link" href="<?=$arAction['DETAIL_PAGE_URL'];?>">
                                                            <span class="b-news_pic">
                                                                <img src="<?php echo $arAction['DETAIL_PICTURE']['src']; ?>" alt="<?=$arAction['NAME'];?>">
                                                                <?php if ($arAction['DAYS_SOON']): ?>
                                                                    <span class="b-clinic_action__days--soon">
                                                                        до начала акции <strong><?php echo $arAction['DAYS_SOON']; ?></strong>
                                                                        <?php echo plural($arAction['DAYS_SOON'], ['день', 'дня', 'дней']) ;?>
                                                                    </span>
                                                                <?php elseif ($arAction['DAYS_LEFT'] > 1): ?>
                                                                    <span class="b-clinic_action__days">
                                                                        до окончания акции <strong><?php echo $arAction['DAYS_LEFT']; ?></strong>
                                                                        <?php echo plural($arAction['DAYS_LEFT'], ['день', 'дня', 'дней']) ;?>
                                                                    </span>
                                                                <?php elseif ($arAction['DAYS_LEFT'] === 1): ?>
                                                                    <span class="b-clinic_action__days">
                                                                        <strong>Остался последний день</strong>
                                                                    </span>
                                                                <?php endif; ?>
                                                            </span>
                                                            <?php if (!empty($arAction['DATE_RANGE'])): ?>
                                                                <span class="b-news_date"><?php echo $arAction['DATE_RANGE'] ?></span>
                                                            <?php endif; ?>
                                                            <span class="b-news_name"><?=$arAction['NAME'];?></span>
                                                        </a>
                                                    </div>
                                                    <?
                                                }
                                                ?>
                                            </div>
                                            <?
                                        }
                                    ?>
                                    <div class="b-clincs_main__col b-clincs_main__col-three">
                                        <?
                                        if($clinic['SERVICES']){
                                            ?>
                                            <div class="b-clinics_main__services-head">
                                                Услуги
                                            </div>
                                            <div class="b-clinics_main__services-list">
                                                <?
                                                foreach($clinic['SERVICES'] AS $arService){
                                                    ?>
                                                    <a href="<?=$arService['LINK'] . 'clinic/' . $clinic['CODE'] . '/';?>"><?=$arService['TEXT'];?></a>
                                                    <?
                                                }
                                                ?>
                                            </div>
                                            <?
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="b-clinics_main__popup-close js-main-clinic-popup-close"></div>
                            </div>
                        </div>

                        <?php if (!(($index + 1) % 3)): ?>
                            </div>
                            <div class="b-clincs_main__item clearfix">
                        <?php endif; ?>

                        <?php

                            $arClinicPhones = is_array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']) ? $clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE'] : array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']);
                            $arClinicDescriptions = is_array($clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']) ? $clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION'] : array($clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']);
                            $clinicPhones = '';
                            foreach($arClinicPhones as $phoneIndex => $phoneValue) {
                                if(PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phoneValue)){
                                    $phoneValue = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                                }

                                $phoneDescription = empty($arClinicDescriptions[$phoneIndex]) ? '' : ' (' . $arClinicDescriptions[$phoneIndex] . ')';
                                $clinicPhones .= '<span>' . $phoneValue . $phoneDescription . '<span><br>';
                            }

                            $maps[$mapId][] = [
                                'coords' => $clinic['PROPERTIES']['COORD']['VALUE'],
                                'name' => $clinic['NAME'],
                                'content' =>
                                    '<a href="'.$clinic['DETAIL_PAGE_URL'].'">'.$clinic['NAME'].'</a><br/>'
                                    . $clinicPhones
                                    . $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE'],
                            ];
                        ?>
                    <?php endforeach; ?>
                    <div class="b-clincs_main__col <?php echo !(($index + 1) % 3) ? 'b-clincs_main__col-one' : 'b-clincs_main__col-three'; ?>">
                        <div class="b-clincs_main__map" id="<?php echo $mapId; ?>"></div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

<?php else: ?>

    <?php
        $clinic = reset($arResult['ITEMS']);
        $type = $clinic['PROPERTIES']['IS_HOSPITAL']['VALUE'] ? 'hospitals' : 'clinics';
        $mapId = 'map-'.$type;

        $image = null;
        if ($clinic['PREVIEW_PICTURE']) {
            $image =
                CFile::ResizeImageGet(
                    $clinic['PREVIEW_PICTURE']['ID'],
                    ['height' => 175, 'width' => 316],
                    BX_RESIZE_IMAGE_EXACT
                );
            $image = $image['src'];
        }
    ?>

    <div class="b-bg_white b-clincs_main__wrap">
        <div class="b-clincs_main__one">
            <div class="align-center b-header_h1"><?php echo $clinic['NAME']; ?></div>
            <div class="b-clincs_main__item clearfix">
                <div class="b-clincs_main__col b-clincs_main__col-three js-main-clinic-item">
                    <span class="b-clincs_main__pic js-main-clinic-hover">
                        <?php if ($image): ?>
                            <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                        <?php endif; ?>
                        <span class="b-clincs_main__pic-overlay">
                            <?php echo $buttonTitles[$type]; ?>
                        </span>
                    </span>
                    <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-clincs_main__link">
                        <span class="b-clincs_main__name"><?php echo $clinic['NAME']; ?></span>
                        <span class="b-clincs_main__address">
                            <strong>Адрес:</strong><br><?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                        </span>
                        <span class="b-btn_white in-bl"><?php echo $buttonTitles[$type]; ?></span>
                    </a>
                    <div class="b-clinics_main__popup js-main-clinic-popup">
                        <div class="b-clincs_main__item clearfix">
                            <div class="b-clincs_main__col b-clincs_main__col-three">
                                <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-clincs_main__link">
                                    <span class="b-clincs_main__pic">
                                        <?php if ($image): ?>
                                            <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                                        <?php endif; ?>
                                        <span class="b-clincs_main__pic-btn"><?php echo $buttonTitlesPic[$type]; ?></span>
                                    </span>
                                    <span class="b-clincs_main__name"><?php echo $clinic['NAME']; ?></span>
                                    <span class="b-clincs_main__address">
                                        <strong>Адрес:</strong><br>
                                        <?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                                    </span>
                                </a>
                                <?php
                                $phones = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]);
                                $phoneDescriptions = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]);
                                $email = trim($clinic["DISPLAY_PROPERTIES"]["CONTACT_EMAIL"]["VALUE"]);
                                ?>
                                <?php if (!empty($phones)): ?>
                                    <div class="b-clinics_main__phone">
                                        <?php foreach($phones as $key => $phone): ?>
                                            <?php
                                            if(PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phone)){
                                                $phone = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                                            }
                                            ?>
                                            <div><span class="js-phone" itemprop="telephone"><?php echo $phone; ?></span><?php echo empty($phoneDescriptions[$key]) ? '' : '<span class="b-clinics_main__phone-note">'.$phoneDescriptions[$key].'</span>'; ?></div>
                                        <?php endforeach; ?>
                                        <?php if (!$clinic['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']): ?>
                                            <span data-clinic-id="<?php echo $clinic['ID']; ?>" class="b-clinics_main__phone-call js-show-call-me js-goal" data-target="<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">Перезвоните мне</span>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($email)): ?>
                                    <div class="b-clinics_main__mail">
                                        <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                    </div>
                                <?php endif; ?>
                                <?php if (!$clinic['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']): ?>
                                    <div class="b-btn_white in-bl link_reception_clinic js-goal" data-clinic="<?php echo $clinic['ID']; ?>" data-name="<?php echo $clinic['NAME']; ?>" data-target="Appointment_<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">
                                        Записаться на приём
                                    </div>
                                <?php endif; ?>
                            </div>
                            <?php
                                if(count($clinic['ACTIONS'])){
                                    ?>
                                    <div class="b-clincs_main__col b-clincs_main__col-three">
                                        <?php
                                        foreach($clinic['ACTIONS'] AS $arAction){
                                            ?>
                                            <div class="b-news_item">
                                                <a class="b-news_link" href="<?=$arAction['DETAIL_PAGE_URL'];?>">
                                                                            <span class="b-news_pic">
                                                                                <img src="<?php echo $arAction['DETAIL_PICTURE']['src']; ?>" alt="<?=$arAction['NAME'];?>">
                                                                                <?php if ($arAction['DAYS_SOON']): ?>
                                                                                    <span class="b-clinic_action__days--soon">
                                                                                        до начала акции <strong><?php echo $arAction['DAYS_SOON']; ?></strong>
                                                                                        <?php echo plural($arAction['DAYS_SOON'], ['день', 'дня', 'дней']) ;?>
                                                                                    </span>
                                                                                <?php elseif ($arAction['DAYS_LEFT'] > 1): ?>
                                                                                    <span class="b-clinic_action__days">
                                                                                        до окончания акции <strong><?php echo $arAction['DAYS_LEFT']; ?></strong>
                                                                                        <?php echo plural($arAction['DAYS_LEFT'], ['день', 'дня', 'дней']) ;?>
                                                                                    </span>
                                                                                <?php elseif ($arAction['DAYS_LEFT'] === 1): ?>
                                                                                    <span class="b-clinic_action__days">
                                                                                        <strong>Остался последний день</strong>
                                                                                    </span>
                                                                                <?php endif; ?>
                                                                            </span>
                                                    <?php if (!empty($arAction['DATE_RANGE'])): ?>
                                                        <span class="b-news_date"><?php echo $arAction['DATE_RANGE'] ?></span>
                                                    <?php endif; ?>
                                                    <span class="b-news_name"><?=$arAction['NAME'];?></span>
                                                </a>
                                            </div>
                                            <?
                                        }
                                        ?>
                                    </div>
                                    <?
                                }
                            ?>
                            <div class="b-clincs_main__col b-clincs_main__col-three">
                                <?
                                if($clinic['SERVICES']){
                                    ?>
                                    <div class="b-clinics_main__services-head">
                                        Услуги
                                    </div>
                                    <div class="b-clinics_main__services-list">
                                        <?
                                        foreach($clinic['SERVICES'] AS $arService){
                                            ?>
                                            <a href="<?=$arService['LINK'] . 'clinic/' . $clinic['CODE'] . '/';?>"><?=$arService['TEXT'];?></a>
                                            <?
                                        }
                                        ?>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                        <div class="b-clinics_main__popup-close js-main-clinic-popup-close"></div>
                    </div>
                </div>
                <div class="b-clincs_main__col b-clincs_main__col-two">
                    <div class="b-clincs_main__map" id="<?php echo $mapId; ?>"></div>
                </div>
            </div>
        </div>
    </div>

    <?php
        $arClinicPhones = is_array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']) ? $clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE'] : array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']);
        $arClinicDescriptions = is_array($clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']) ? $clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION'] : array($clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']);
        $clinicPhones = '';
        foreach($arClinicPhones as $phoneIndex => $phoneValue) {
            $phoneDescription = isset($arClinicDescriptions[$phoneIndex]) ? ' (' . $arClinicDescriptions[$phoneIndex] . ')' : '';
            $clinicPhones .= '<span>' . $phoneValue . $phoneDescription . '<span><br>';
        }
        $maps[$mapId][] = [
            'coords' => $clinic['PROPERTIES']['COORD']['VALUE'],
            'name' => $clinic['NAME'],
            'content' =>
                '<a href="'.$clinic['DETAIL_PAGE_URL'].'">'.$clinic['NAME'].'</a><br/>'
                . $clinicPhones
                . $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE'],
        ];
    ?>

<?php endif; ?>

<?php if ($maps): ?>
    <script type="text/javascript">
        ymaps.ready(function () {
            var map;

            <?php foreach ($maps as $mapId => $placemarks): ?>
                <?php $firstCoords = $placemarks[0]['coords']; ?>
                map = new ymaps.Map (<?php echo json_encode($mapId) ?>, {
                    center: [<?php echo $firstCoords; ?>],
                    zoom: 9,
                    controls: ["smallMapDefaultSet"]
                });

                <?php foreach ($placemarks as $placemark): ?>
                    var placemark = new ymaps.Placemark([<?php echo $placemark['coords']; ?>], {
                            // iconContent: <?php echo json_encode($placemark['name']); ?>,
                            // hintContent: <?php echo json_encode($placemark['name']); ?>,
                            balloonContent: <?php echo json_encode($placemark['content']); ?>

                        });
                    placemark.events.add('mouseenter', function(e) {
                        var placemark = e.get('target');
                        placemark.balloon.open();
                    });
                    placemark.events.add('mouseleave', function(e) {
                        var placemark = e.get('target');
                        // placemark.balloon.close();
                    });
                    map.geoObjects.add(placemark);
                <?php endforeach; ?>

                <?php if (count($placemarks) > 1): ?>
                    map.setBounds(map.geoObjects.getBounds());
                    map.setZoom(map.getZoom() - 1);
                <?php else: ?>
                    map.setZoom(13);
                <?php endif; ?>
            <?php endforeach; ?>
        });
    </script>
<?php endif; ?>