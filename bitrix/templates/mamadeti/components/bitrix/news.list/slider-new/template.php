<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php if (!empty($arResult["ITEMS"])) { ?>
<div class="b-banner_main__wrap">
    <div class="b-banner_main l-width">
        <div class="b-banner_main__bg-l"></div>
        <div class="b-banner_main__bg-r"></div>
        <ul class="b-banner_main__container js-banner-slider">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?php $url = $arItem['PROPERTIES']['LINK']['VALUE']; ?>
                <li>
                    <?php if ($url): ?>
                        <a href="<?php echo $url; ?>" class="b-banner_main__link">
                    <?php endif; ?>

                    <div class="b-banner_main__header">
                        <div class="b-banner_main__header-padding">
                            <div class="in-bl b-banner_main__header-empty"></div><div class="in-bl"><span class="b-banner_main__header-big"><?=$arItem["NAME"]?></span> <?=$arItem["CODE"]?></div>
                        </div>
                    </div>
                    <div class="b-banner_main__corners">
                        <div class="b-banner_main__corner-l"></div>
                        <div class="b-banner_main__corner-r"></div>
                    </div>
                    <div class="b-banner_main__pic">
                         <?php $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width"=>660,"height"=>230),BX_RESIZE_IMAGE_EXACT,true); ?>
                        <img src="<?=$file["src"]?>" alt="<?=$arItem["NAME"]?>">
                    </div>
                    <?if(!empty($arItem["PROPERTIES"]["LIST"]["VALUE"])):?>
                        <div class="b-banner_main__txt">
                        <?foreach ($arItem["PROPERTIES"]["LIST"]["VALUE"] as $item) {?>
                            <div class="b-banner_main__txt-item">
                                <div class="b-banner_main__txt-padding">
                                    <span class="b-banner_main__txt-dash">&mdash;</span><?=$item?>
                                </div>
                            </div>
                        <?}?>
                        </div>
                    <?endif;?>

                    <?php if ($url): ?>
                        </a>
                    <?php endif; ?>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</div>
<?php } ?>
