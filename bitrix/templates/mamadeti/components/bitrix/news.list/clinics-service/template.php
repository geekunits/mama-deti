<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="content-col" style="width: 935px;">
	<div class="sector-content">
		<div class="list-holder">
			<ul class="item-listing" style="width: 935px; margin-left: 20px;">
			<?foreach ($arResult['ITEMS'] as $arItem){?>
				<li>
					<div class="content-block add-able">
						<div class="item-frame align-center">
							<div class="item-sector">
								<div class="image-item">
									<?if(!empty($arItem["PREVIEW_PICTURE"])){?>
										<img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z"=>1,"w"=>270,"h"=>210))?>" alt="<?=$arItem["NAME"]?>">
										<span class="image-mask">
											<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="add-link"></a>
										</span>
									<?}?>
								</div>
								<div class="item-link">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
								</div>
							</div>
						</div>
					</div>
				</li>
			<?}?>
			</ul>
		</div>
	</div>
</section>