<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResizeMethod = array(
	BX_RESIZE_IMAGE_EXACT => "BX_RESIZE_IMAGE_EXACT",
	BX_RESIZE_IMAGE_PROPORTIONAL => "BX_RESIZE_IMAGE_PROPORTIONAL",
	BX_RESIZE_IMAGE_PROPORTIONAL_ALT => "BX_RESIZE_IMAGE_PROPORTIONAL_ALT",
	);

$arTemplateParameters = array(
	"RESIZE_CATALOG_METHOD" => array(
		"NAME" => "Метод ресайза",
		"TYPE" => "LIST",
		"VALUES" => $arResizeMethod,
		"DEFAULT" => BX_RESIZE_IMAGE_PROPORTIONAL,
	),
	"RESIZE_CATALOG_WIDTH" => Array(
		"NAME" => "Ширина картинки",
		"TYPE" => "STRING",
		"DEFAULT" => "128",
	),
	"RESIZE_CATALOG_HEIGHT" => Array(
		"NAME" => "Высота картинки",
		"TYPE" => "STRING",
		"DEFAULT" => "204",
	),
);
?>
