<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="b-news_item clearfix">
    <? $i=0; ?>
    <?foreach($arResult["ITEMS"] as $arItem): $i++;?>
    <? if ($i == 4) { $i =1 ; ?></div><div class="b-news_item clearfix">
    <? } ?>
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="b-news_col">
        <span class="b-news_pic">
        <?if (is_array($arItem["PREVIEW_PICTURE"])):?>
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
        <?endif?>
        </span>
        <span class="b-news_date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
        <span class="b-news_name"><?=$arItem["NAME"]?></span>
        <span class="b-news_txt"><?=strip_tags($arItem["PREVIEW_TEXT"])?></span>
        <?/*foreach ($arItem["DISPLAY_PROPERTIES"] as $arProp):?>
            <?if (!empty($arProp["DISPLAY_VALUE"]) && !is_array($arProp["DISPLAY_VALUE"])):?>
                <span class="article-date"><?=$arProp["NAME"]?>: <?=$arProp["DISPLAY_VALUE"]?></span>
            <?endif?>
        <?endforeach*/?>
    </a>
    <?endforeach;?>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?endif;?>