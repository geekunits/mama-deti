<?
CModule::IncludeModule("iblock");
foreach($arResult["ITEMS"] as $key=>$arItem){
	$arResult["ITEMS"][$key]["STATISTICS"] = array(
		"REVIEW" => 0,
		"STORY" => 0,
		"QA" => 0
	);

	$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arItem["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	$arResult["ITEMS"][$key]["STATISTICS"]["REVIEW"] = $res->SelectedRowsCount();

	$arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arItem["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	$arResult["ITEMS"][$key]["STATISTICS"]["STORY"] = $res->SelectedRowsCount();

	$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arItem["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
	$arResult["ITEMS"][$key]["STATISTICS"]["QA"] = $res->SelectedRowsCount();

	$arResult["ITEMS"][$key]["STATISTICS"]["SHOW"] = (
		$arResult["ITEMS"][$key]["STATISTICS"]["REVIEW"]>0 ||
		$arResult["ITEMS"][$key]["STATISTICS"]["STORY"]>0 ||
		$arResult["ITEMS"][$key]["STATISTICS"]["QA"]>0);
	
}

?>
