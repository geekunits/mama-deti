<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult["ITEMS"])):?>
    <div class="main-holder doctors" id="main-doctors-slider">
        <div class="content-block">
            <div class="content-section">
                <div class="info-block align-center">
                    <span class="title-block"><strong>6 000</strong><span>специалистов</span></span>
                    <div class="block-text">
                        <p>
                            Чтобы выбрать врача нажмите на кнопку “Выбрать врача”
                        </p>
                    </div>
                    <div class="button-holder">
                        <a href="/doctors/" class="button02">Выбрать врача</a>
                    </div>
                </div>
                <div class="gallery-block">
                    <div class="item-gallery">
                        <a href="#" class="prev">prev</a>
                        <a href="#" class="next">next</a>
                        <div class="gallery-frame">
                            <ul class="gallery">
                                <?foreach($arResult["ITEMS"] as $arItem):?>
                                    <li class="slide">
                                        <div class="item-block">
                                            <div class="item-image">
                                                <?if (is_array($arItem["PREVIEW_PICTURE"])):?>
                                                    <img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z"=>1, "w"=>190, "h"=>230))?>" alt="<?=$arItem["NAME"]?>">
                                                <?else:?>
                                                    <?if ($arItem["PROPERTIES"]["SEX"]["VALUE_XML_ID"] == "F"):?>
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/female.gif" alt="<?=$arResult["NAME"]?>">
                                                    <?else:?>
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/images/male.gif" alt="<?=$arResult["NAME"]?>">
                                                    <?endif?>
                                                <?endif?>
                                            </div>
                                            <div class="block-holder">
                                                <span class="name-title"><?=$arItem["NAME"]?></span>
                                                <div class="item-holder">
                                                    <div class="left-item">
                                                        <?if(!empty($arItem["PROPERTIES"]["POST_FULL"]["VALUE"])){?>
                                                            <div class="item-text">
                                                                <p><?=$arItem["PROPERTIES"]["POST_FULL"]["VALUE"]?></p>
                                                            </div>
                                                        <?}?>
                                                        <?if(!empty($arItem["PREVIEW_TEXT"])){?>
                                                            <span class="strong-text"><?=$arItem["PREVIEW_TEXT"]?></span>
                                                        <?}?>
                                                    </div>
                                                    <div class="right-item">
                                                        <?if ($arItem["STATISTICS"]["SHOW"]):?>
                                                            <ul class="item-list">
                                                                <?if ($arItem["STATISTICS"]["REVIEW"]>0):?>
                                                                    <li>
                                                                        <a href="/reviews/?doctor=<?=$arItem["ID"]?>"><span class="count"><?=$arItem["STATISTICS"]["REVIEW"]?></span><span class="title">Отзывов</span></a>
                                                                    </li>
                                                                <?endif?>
                                                                <?if ($arItem["STATISTICS"]["STORY"]>0):?>
                                                                    <li>
                                                                        <a href="/story/?doctor=<?=$arItem["ID"]?>"><span class="count"><?=$arItem["STATISTICS"]["STORY"]?></span><span class="title">Историй со счастливым концом</span></a>
                                                                    </li>
                                                                <?endif?>
                                                                <?if ($arItem["STATISTICS"]["QA"]>0):?>
                                                                    <li>
                                                                        <a href="/qa/?doctor=<?=$arItem["ID"]?>"><span class="count"><?=$arItem["STATISTICS"]["QA"]?></span><span class="title">Ответов на вопросы</span></a>
                                                                    </li>
                                                                <?endif?>
                                                            </ul>
                                                        <?endif?>
                                                    </div>
                                                </div>
                                                <div class="block-holder gb-foot">
                                                    <div class="left-item">
                                                        <ul class="links-list">
                                                            <li>
                                                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="red-color">Подробнее о специалисте</a>
                                                            </li>
                                                            <!--<li>
                                                                <a href="#">Как выбрать врача?</a>
                                                            </li>-->
                                                        </ul>
                                                    </div>
                                                    <div class="right-item">
                                                        <a href="#" class="button01 link_reception_doc" data-doctor="<?=$arItem["ID"]?>" data-clinic="<?=$arItem["PROPERTIES"]["CLINIC"]["VALUE"][0]?>">Записаться на прием</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?endif?>