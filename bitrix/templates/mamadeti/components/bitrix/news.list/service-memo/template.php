<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php if ($arResult['ITEMS']): ?>
    <?if($arParams["DISPLAY_TOP_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>

    <div class="rt-actions">
        <div class="items">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <div class="item">
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                        <a class="photo" <?php if ($arItem['DETAIL_TEXT']): ?>href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"<?php endif; ?>>
                            <img
                                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                alt="<?=$arItem["NAME"]?>"
                                title="<?=$arItem["NAME"]?>"
                                >
                        </a>
                    <?endif?>
                    <div class="description">
                        <div class="title name-title">
                            <?php if ($arItem['DETAIL_TEXT']): ?><a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"><?php echo $arItem['NAME']; ?></a><?php else: ?><?php echo $arItem['NAME']; ?><?php endif; ?>
                        </div>
                        <?php
                        $dateFrom = $arItem['DISPLAY_PROPERTIES']['DATE_FROM']['DISPLAY_VALUE'];
                        $dateTo = $arItem['DISPLAY_PROPERTIES']['DATE_TO']['DISPLAY_VALUE'];
                        ?>
                        <?if($dateFrom || $dateTo){?>
                            <div class="time-action">
                                <?php if ($dateFrom && $dateTo): ?>
                                    Акция проводится с <?php echo $dateFrom; ?> по <?php echo $dateTo; ?>.
                                <?php elseif ($dateFrom):?>
                                    Акция проводится с <?php echo $dateFrom; ?>.
                                <?php elseif ($dateTo):?>
                                    Акция проводится по <?php echo $dateTo; ?>.
                                <?php endif;?>
                            </div>
                        <?}?>
                        <div class="text"><?echo $arItem["PREVIEW_TEXT"];?></div>
                        <?php if ($arItem['DETAIL_TEXT']): ?><a class="more red-color" href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>">узнать больше</a><?php endif; ?>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>

    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
<?php endif; ?>