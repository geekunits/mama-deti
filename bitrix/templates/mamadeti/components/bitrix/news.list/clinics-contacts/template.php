<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="workers-list">
<?php foreach ($arResult['ITEMS'] as $arItem) { ?>
    <?php
        $phones = is_array($arItem["PROPERTIES"]["PHONE"]["VALUE"]) ?
            $arItem["PROPERTIES"]["PHONE"]["VALUE"] : array($arItem["PROPERTIES"]["PHONE"]["VALUE"]);
        $phoneDescriptions = is_array($arItem["PROPERTIES"]["PHONE"]["DESCRIPTION"]) ?
            $arItem["PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($arItem["PROPERTIES"]["PHONE"]["DESCRIPTION"]);
    ?>
	<li>
        <div class="block-holder">
            <div class="frame-holder clin-list-holder">
                <div class="frame-image">
                    <?php if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) { ?>
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <?php
                                $arImgClinic = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width' => 190, 'height' => 190), BX_RESIZE_IMAGE_EXACT)
                            ?>
                            <img src="<?php echo $arImgClinic["src"]; ?>" alt="<?php echo $arItem["NAME"]; ?>">
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="block-holder">
		        <h3><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></h3>
                <address class="list-address">
                    <?php if (!empty($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"])): ?><span class="row">
                        <strong>Адрес:</strong><span> <?= htmlspecialchars_decode($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"]) ?></span>
                        </span><?php endif; ?>
                    <?php if (!empty($phones)): ?>
                        <?php foreach($phones as $index => $phone): ?>
                            <?php
                            $isEqual = PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phone);
                            if($isEqual){
                                $phone = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                            }

                            $phoneDescription = empty($phoneDescriptions[$index]) ? 'Телефон' : $phoneDescriptions[$index];
                            ?>
                            <span class="row"><strong><?php echo $phoneDescription; ?>:</strong>
                                <span class="clinics-phone js-phone" data-clinic-id="<?=$arItem['ID'];?>">
                                    <?php echo $phone; ?>
                                </span>
                                <?php if ($isEqual): ?>
                                    <?php if(CLIENT_FROM_ABROAD): ?>
                                        <span class="phone-free">звонок из-за рубежа</span>
                                    <?php else: ?>
                                        <span class="phone-free">звонок бесплатный</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </address>
                <?= $arItem["PREVIEW_TEXT"] ?>
            </div>
        </div>
	</li>
<?php } ?>
</ul>
<?= $arResult["NAV_STRING"] ?>