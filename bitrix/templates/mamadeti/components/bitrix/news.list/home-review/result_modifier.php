<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
foreach ($arResult["ITEMS"] as &$arItem) {
    if (intval($arItem["PROPERTIES"]["DOCTOR"]["VALUE"])>0) {
        $rsElement = CIBlockElement::GetByID(reset($arItem["PROPERTIES"]["DOCTOR"]["VALUE"]));
        if ($obElement = $rsElement->GetNextElement()) {
            $arItem["DOCTOR"] = $obElement->GetFields();
            $arItem["DOCTOR"]["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["DOCTOR"]["PREVIEW_PICTURE"]);
            $arItem["DOCTOR"]["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DOCTOR"]["DETAIL_PICTURE"]);
            $arItem["DOCTOR"]["PROPERTIES"] = $obElement->GetProperties();

            if (intval($arItem["DOCTOR"]["PROPERTIES"]["CLINIC"]["VALUE"])>0) {
                $rsElement = CIBlockElement::GetByID($arItem["DOCTOR"]["PROPERTIES"]["CLINIC"]["VALUE"]);
                if ($obElement = $rsElement->GetNextElement()) {
                    $arItem["CLINIC"] = $obElement->GetFields();
                    $arItem["CLINIC"]["PROPERTIES"] = $obElement->GetProperties();
                }
            }
        }
    } elseif (intval($arItem["PROPERTIES"]["CLINIC"]["VALUE"])>0) {
        $rsElement = CIBlockElement::GetByID($arItem["PROPERTIES"]["CLINIC"]["VALUE"]);
        if ($obElement = $rsElement->GetNextElement()) {
            $arItem["CLINIC"] = $obElement->GetFields();
            $arItem["CLINIC"]["PROPERTIES"] = $obElement->GetProperties();
        }
    }

    if (is_array($arItem["DOCTOR"])) {
        $arItem['DOCTOR']['SHORT_NAME'] = shortenName($arItem['DOCTOR']['NAME']);

        $arFile = is_array($arItem["DOCTOR"]["PREVIEW_PICTURE"]) ? $arItem["DOCTOR"]["PREVIEW_PICTURE"] : $arItem["DOCTOR"]["DETAIL_PICTURE"];
        if (is_array($arFile)) {
            $arFileTmp = CFile::ResizeImageGet(
                        $arFile, array("width" => 45, "height" => 45), BX_RESIZE_IMAGE_EXACT, true
            );
            $arItem["DOCTOR"]["THUMB_PICTURE"] = array_change_key_case($arFileTmp, CASE_UPPER);
        }
    }

    if (is_array($arItem["CLINIC"])) {
        $arItem["CLINIC"]["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["CLINIC"]["PREVIEW_PICTURE"]);
        $arItem["CLINIC"]["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["CLINIC"]["DETAIL_PICTURE"]);

        $arFile = is_array($arItem["CLINIC"]["PREVIEW_PICTURE"]) ? $arItem["CLINIC"]["PREVIEW_PICTURE"] : $arItem["CLINIC"]["DETAIL_PICTURE"];
        if (is_array($arFile)) {
            $arFileTmp = CFile::ResizeImageGet(
                        $arFile, array("width" => 45, "height" => 45), BX_RESIZE_IMAGE_EXACT, true
            );
            $arItem["CLINIC"]["THUMB_PICTURE"] = array_change_key_case($arFileTmp, CASE_UPPER);
        }
    }

    if (isset($arItem)) {
        unset($arItem);
    }
}
?>
