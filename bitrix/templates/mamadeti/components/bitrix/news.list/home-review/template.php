<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
	global $currentClinicId;
	global $currentClinicCityId;
	$urlSuffix = array('city' => $currentClinicCityId, 'clinic' => $currentClinicId);
	$urlSuffix = array_filter($urlSuffix);
	$urlSuffix = http_build_query($urlSuffix);
	$count=count($arResult["ITEMS"])
?>

<div class="b-bg_white b-widgets_footer__col">
    <div class="b-widgets_footer__padding">
        <div class="align-center b-header_h2">Отзывы</div>
        <?if($count):?>
	        <div class="b-widgets_footer__height">
	            <div class="b-widgets_footer__opinion">
	            	<?foreach($arResult["ITEMS"] as $i=>$arItem):?>
	                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="b-widgets_footer__opinion-item">
	                    <span class="b-widgets_footer__opinion-name">
	                        <span class="in-bl"><?php echo $arItem["NAME"]; ?></span><?php 
	                        if ($arItem['DOCTOR']): 
	                        	?><span class="in-bl b-widgets_footer__opinion-arr">&rarr;</span><!--
			                   --><span class="b-widgets_footer__pic in-bl<?php 
			                     		if (!$arItem['DOCTOR']['THUMB_PICTURE']): ?>
                                            <?if ($arItem['DOCTOR']["PROPERTIES"]["SEX"]["VALUE_XML_ID"] == "F"):?>
                                                b-doctor_nophoto__female
                                            <?else:?>
                                               b-doctor_nophoto__male
                                            <?endif?>
			                     		<?php endif; ?>">
	                         		<?php if ($arItem["DOCTOR"]["THUMB_PICTURE"]): ?><img src="<?php echo $arItem["DOCTOR"]["THUMB_PICTURE"]['SRC']; ?>" alt=""><?php endif; ?>
	                         	  </span><!--
	                           --><span class="b-widgets_footer__fio"><?php echo $arItem['DOCTOR']['SHORT_NAME']; ?></span><?php 
	                        elseif ($arItem['CLINIC']): 
	                        	?><span class="in-bl b-widgets_footer__opinion-arr">&rarr;</span><span class="b-widgets_footer__pic in-bl"><?php if ($arItem["CLINIC"]["THUMB_PICTURE"]): ?><img src="<?php echo $arItem["CLINIC"]["THUMB_PICTURE"]['SRC']; ?>" alt=""><?php endif; ?></span><span class="b-widgets_footer__fio"><?php echo $arItem['CLINIC']['NAME']; ?></span>
	                    	<?php endif; ?>
	                    </span>
	                    <span class="b-widgets_footer__opinion-txt"><?=$arItem["PREVIEW_TEXT"]?></span>
	                </a>
	            	<?php endforeach; ?>
	            </div>
	            <div class="b-widget_footer__shadow"></div>
	        </div>
	        <div class="align-center b-widgets_footer__more"><a href="/reviews/<?php echo $urlSuffix ? '?'.$urlSuffix : ''; ?>">Все отзывы (<?php echo $arResult['NAV_RESULT']->NavRecordCount; ?>)</a></div>
	    <?php else: ?>
	        <div class="b-widgets_footer__noitems b-widgets_footer__opinion link_review">
	            <div class="b-widgets_footer__noitems-pic"></div>
	            <div class="b-widgets_footer__noitems-txt">Оставьте свой отзыв, <br>нам это важно!</div>
	            <a href="/reviews/<?php echo $urlSuffix ? '?'.$urlSuffix : ''; ?>" class="b-btn_white in-bl">Оставить отзыв</a>
	        </div>
        <?php endif; ?>
    </div>
</div>