<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php
    $maps = [];

    $city = CityManager::getInstance()->getCityById(GetCurrentCity());
    $titles = [
        'hospitals' => 'Госпитали «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
        'clinics' => 'Клиники «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
    ];
    $buttonTitles = [
        'hospitals' => 'Подробнее о госпитале',
        'clinics' => 'Подробнее о клинике',
    ];
    $buttonTitlesPic = [
        'hospitals' => 'Всё о госпитале',
        'clinics' => 'Всё о клинике',
    ];

?>
<div class="b-bg_white b-clincs_main__wrap">
    <?php foreach ($arResult['ITEMS'] as $clinic): ?>

        <?php
            $type = $clinic['PROPERTIES']['IS_HOSPITAL']['VALUE'] ? 'hospitals' : 'clinics';
            $mapId = 'map-'.$clinic['CODE'];

            $image = null;
            if ($clinic['PREVIEW_PICTURE']) {
                $image =
                    CFile::ResizeImageGet(
                        $clinic['PREVIEW_PICTURE']['ID'],
                        ['height' => 294, 'width' => 294],
                        BX_RESIZE_IMAGE_EXACT
                    );
                $image = $image['src'];
            }
            $phones = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"] : array( $clinic["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]);
            $descriptions = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]['DESCRIPTION']) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]['DESCRIPTION'] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]['DESCRIPTION']);
        ?>

        <div class="b-clincs_main__ufa">
            <div class="align-center b-header_h1"><?php echo $clinic['NAME']; ?></div>
            <div class="b-clincs_main__item clearfix">
                <div class="b-clincs_main__col b-clincs_main__col-half js-main-clinic-item">
                    <div class="b-clincs_main__link">
                        <span class="b-clincs_main__col b-clincs_main__col-three b-clincs_main__pic js-main-clinic-hover">
                            <?php if ($image): ?>
                                <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                            <?php endif; ?>
                            <span class="b-clincs_main__pic-overlay">
                                <?php echo $buttonTitles[$type]; ?>
                            </span>
                        </span>
                        <span class="b-clincs_main__col b-clincs_main__col-three">
                            <span class="b-clincs_main__ufa-txt">
                                <strong>Адрес:</strong><br><?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                            </span>
                            <span class="b-clincs_main__ufa-txt">
                                <?php foreach($phones as $index => $phone): ?>
                                <span class="b-clincs_main__phone">
                                    <?php $description = empty($descriptions[$index]) ? 'Телефон' : $descriptions[$index]; ?>
                                    <strong><?php echo $description; ?>:</strong>
                                    <br>
                                    <span class="js-phone">
                                        <?php echo $phone; ?>
                                    </span>
                                 </span>   
                                <?php endforeach; ?>
                                <span class="b-clincs_main__ufa-call js-show-call-me js-goal" data-clinic-id="<?php echo $clinic['ID']; ?>" data-target="<?php echo $clinic['PROPERTIES']['METRIC_ID']['VALUE']; ?>">Перезвоните мне</span>
                            </span>
                            <span class="b-clincs_main__ufa-txt">
                                <strong>E-mail:</strong><br><?php echo $clinic['DISPLAY_PROPERTIES']['CONTACT_EMAIL']['DISPLAY_VALUE']; ?>
                            </span>
                            <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-btn_white in-bl"><?php echo $buttonTitles[$type]; ?></a>
                        </span>
                        <div class="b-clinics_main__popup js-main-clinic-popup" data-topend="310" data-topstart="290">
                            <div class="b-clincs_main__item clearfix">
                                <div class="b-clincs_main__col b-clincs_main__col-three">
                                    <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="b-clincs_main__link">
                                            <span class="b-clincs_main__pic">
                                                <?php if ($image): ?>
                                                    <img src="<?php echo $image; ?>" alt="<?php echo $clinic['NAME']; ?>">
                                                <?php endif; ?>
                                                <span class="b-clincs_main__pic-btn"><?php echo $buttonTitlesPic[$type]; ?></span>
                                            </span>
                                        <span class="b-clincs_main__name"><?php echo $clinic['NAME']; ?></span>
                                            <span class="b-clincs_main__address">
                                                <strong>Адрес:</strong><br>
                                                <?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
                                            </span>
                                    </a>
                                    <?php
                                    $phones = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]);
                                    $phoneDescriptions = is_array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]) ? $clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($clinic["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]);
                                    $email = trim($clinic["DISPLAY_PROPERTIES"]["CONTACT_EMAIL"]["VALUE"]);
                                    ?>
                                    <?php if (!empty($phones)): ?>
                                        <div class="b-clinics_main__phone">
                                            <?php foreach($phones as $key => $phone): ?>
                                                <div><span class="js-phone" itemprop="telephone"><?php echo $phone; ?></span><?php echo empty($phoneDescriptions[$key]) ? '' : '<span class="b-clinics_main__phone-note">'.$phoneDescriptions[$key].'</span>'; ?></div>
                                            <?php endforeach; ?>
                                            <span data-clinic-id="<?php echo $clinic['ID']; ?>" class="b-clinics_main__phone-call js-show-call-me js-goal" data-target="<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">Перезвоните мне</span>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty($email)): ?>
                                        <div class="b-clinics_main__mail">
                                            <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                        </div>
                                    <?php endif; ?>
                                    <div class="b-btn_white in-bl link_reception_clinic js-goal" data-clinic="<?php echo $clinic['ID']; ?>" data-name="<?php echo $clinic['NAME']; ?>" data-target="Appointment_<?php echo $clinic['DISPLAY_PROPERTIES']['METRIC_ID']['VALUE']; ?>">
                                        Записаться на приём
                                    </div>
                                </div>
                                <?
                                    if(count($clinic['ACTIONS'])){
                                        ?>
                                        <div class="b-clincs_main__col b-clincs_main__col-three">
                                            <?
                                            foreach($clinic['ACTIONS'] AS $arAction){
                                                ?>
                                                <div class="b-news_item">
                                                    <a class="b-news_link" href="<?=$arAction['DETAIL_PAGE_URL'];?>">
                                                        <span class="b-news_pic">
                                                            <img src="<?php echo $arAction['DETAIL_PICTURE']['src']; ?>" alt="<?=$arAction['NAME'];?>">
                                                            <?php if ($arAction['DAYS_SOON']): ?>
                                                                <span class="b-clinic_action__days--soon">
                                                                    до начала акции <strong><?php echo $arAction['DAYS_SOON']; ?></strong>
                                                                    <?php echo plural($arAction['DAYS_SOON'], ['день', 'дня', 'дней']) ;?>
                                                                </span>
                                                            <?php elseif ($arAction['DAYS_LEFT'] > 1): ?>
                                                                <span class="b-clinic_action__days">
                                                                    до окончания акции <strong><?php echo $arAction['DAYS_LEFT']; ?></strong>
                                                                    <?php echo plural($arAction['DAYS_LEFT'], ['день', 'дня', 'дней']) ;?>
                                                                </span>
                                                            <?php elseif ($arAction['DAYS_LEFT'] === 1): ?>
                                                                <span class="b-clinic_action__days">
                                                                    <strong>Остался последний день</strong>
                                                                </span>
                                                            <?php endif; ?>
                                                        </span>
                                                        <?php if (!empty($arAction['DATE_RANGE'])): ?>
                                                            <span class="b-news_date"><?php echo $arAction['DATE_RANGE'] ?></span>
                                                        <?php endif; ?>
                                                        <span class="b-news_name"><?=$arAction['NAME'];?></span>
                                                    </a>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                        <?
                                    }
                                ?>
                                <div class="b-clincs_main__col b-clincs_main__col-three">
                                    <?
                                    if($clinic['SERVICES']){
                                        ?>
                                        <div class="b-clinics_main__services-head">
                                            Услуги
                                        </div>
                                        <div class="b-clinics_main__services-list">
                                            <?
                                            foreach($clinic['SERVICES'] AS $arService){
                                                ?>
                                                <a href="<?=$arService['LINK'] . 'clinic/' . $clinic['CODE'] . '/';?>"><?=$arService['TEXT'];?></a>
                                                <?
                                            }
                                            ?>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="b-clinics_main__popup-close js-main-clinic-popup-close"></div>
                        </div>
                    </div>
                </div>
                <div class="b-clincs_main__col b-clincs_main__col-half">
                    <div class="b-clincs_main__map" id="<?php echo $mapId; ?>"></div>
                </div>
            </div>
        </div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "clinic-services",
            array(
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => "1",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "Y",
                "COUNT_ELEMENTS" => "N",
                "TOP_DEPTH" => 10,
                'SECTION_USER_FIELDS' => ['UF_MENU_DESCRIPTION', 'UF_SHOW_ON_MAIN', 'UF_NOT_SHOW_CLINICS'],
                "CLINIC" => $clinic,
            ),
            $component
        );
        ?>

        <?php
            $phoneValues = '';
            $arPhones = is_array($clinic["DISPLAY_PROPERTIES"]['PHONE']['DISPLAY_VALUE']) ? $clinic["DISPLAY_PROPERTIES"]['PHONE']['DISPLAY_VALUE'] : array($clinic["DISPLAY_PROPERTIES"]['PHONE']['DISPLAY_VALUE']);
            $arPhoneDescriptions = is_array($clinic["DISPLAY_PROPERTIES"]['PHONE']['DESCRIPTION']) ? $clinic["DISPLAY_PROPERTIES"]['PHONE']['DESCRIPTION'] : array($clinic["DISPLAY_PROPERTIES"]['PHONE']['DESCRIPTION']);
            foreach($arPhones as $phoneIndex => $phoneValue) {
                $phoneDescription = empty($arPhoneDescriptions[$phoneIndex]) ? '' : ' (' . $arPhoneDescriptions[$phoneIndex] . ')';
                $phoneValues .= '<span>' . $phoneValue . $phoneDescription . '</span><br>';
            }

            $maps[$mapId] = [];
            $maps[$mapId][] = [
                'coords' => $clinic['PROPERTIES']['COORD']['VALUE'],
                'name' => $clinic['NAME'],
                'content' =>
                    '<a href="'.$clinic['DETAIL_PAGE_URL'].'">'.$clinic['NAME'].'</a><br/>'
                    .$phoneValues
                    .$clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE'],
            ];
        ?>

    <?php endforeach; ?>
</div>

<?php if ($maps): ?>
    <script type="text/javascript">
        ymaps.ready(function () {
            var map;

            <?php foreach ($maps as $mapId => $placemarks): ?>
                <?php $firstCoords = $placemarks[0]['coords']; ?>
                map = new ymaps.Map (<?php echo json_encode($mapId) ?>, {
                    center: [<?php echo $firstCoords; ?>],
                    zoom: 9,
                    controls: ["smallMapDefaultSet"]
                });

                <?php foreach ($placemarks as $placemark): ?>
                    var placemark = new ymaps.Placemark([<?php echo $placemark['coords']; ?>], {
                        content: <?php echo json_encode($placemark['name']); ?>,
                        balloonContent: <?php echo json_encode($placemark['content']); ?>
                    });
                    placemark.events.add('mouseenter', function(e) {
                        var placemark = e.get('target');
                        placemark.balloon.open();
                    });
                    map.geoObjects.add(placemark);
                <?php endforeach; ?>

                <?php if (count($placemarks) > 1): ?>
                    map.setBounds(map.geoObjects.getBounds());
                    map.setZoom(map.getZoom() - 1);
                <?php else: ?>
                    map.setZoom(13);
                <?php endif; ?>
            <?php endforeach; ?>
        });
    </script>
<?php endif; ?>
