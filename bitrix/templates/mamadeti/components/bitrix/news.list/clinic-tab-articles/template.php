<?php if ($arResult['ITEMS']): ?>
<div class="clinic-articles">
    <?php
        $index = 0;

        switch (count($arResult['ITEMS'])) {
            case 1:
                $classPrefix = 'one';
                break;

            case 2:
                $classPrefix = 'two';
                break;

            default:
                $classPrefix = 'two';
                break;
        }
    ?>
    <?php foreach ($arResult['ITEMS'] as $arItem): ?>
        <?php
            $index++;

            $doctors = $arItem['DISPLAY_PROPERTIES']['DOCTOR']['DISPLAY_VALUE'];
            if (!is_array($doctors)) {
                $doctors = [$doctors];
            }

            $doctors = array_map(function ($doctor) {
                $name = strip_tags($doctor);
                $shortenName = shortenName($name);

                return str_replace($name, $shortenName, $doctor);
            }, $doctors);

            $detailPageUrl = $arItem['DETAIL_PAGE_URL'];
            if (!empty($arParams['CLINIC_ID'])) {
                $detailPageUrl .= '?clinic=' . $arParams['CLINIC_ID'];
            }

        ?>
        <?php if (!(($index - 1) % 2)): ?>
            <div class="clinic-articles__item">
                <div class="b-columns_<?php echo $classPrefix; ?> clearfix">
            <?php endif; ?>
                <div class="b-column">
                    <div class="clinic-articles__name"><a href="<?php echo $detailPageUrl; ?>"><?php echo shortenString($arItem['NAME'], 30); ?></a></div>
                    <div class="clinic-articles__intro"><?php echo shortenString(strip_tags($arItem['PREVIEW_TEXT']), 150); ?></div>
                    <?php if(count($arItem['DOCTORS'])): ?>
                        <div class="b-doctor b-doctor_small">
                            <?php foreach($arItem['DOCTORS'] as $arDoctor): ?>
                                <div class="clinic-articles__doctor clearfix">
                                    <?php
                                        $image = $arDoctor['THUMB_PICTURE'];
                                    ?>
                                    <div class="b-doctor_part-left">
                                        <a class="b-doctor_avatar<?php if (!$image): ?> b-doctor_nophoto__female <?php echo $isFemale ? '' : ' b-doctor_nophoto__male'; ?><?php endif; ?>" href="<?php echo $arDoctor['DETAIL_PAGE_URL']; ?>">
                                            <?php if ($image): ?>
                                                <img src="<?php echo $image['SRC']; ?>" alt="<?php echo htmlspecialchars($arDoctor['SHORT_NAME']); ?>">
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <a class="b-doctor_intro" href="<?php echo $arDoctor['DETAIL_PAGE_URL']; ?>">
                                        <span class="b-doctor_text">Автор:</span>
                                        <span class="b-doctor_name"><?php echo $arDoctor['SHORT_NAME']; ?></span>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php if (($index > 1 && !($index % 2)) || $index === count($arResult['ITEMS'])): ?>
                    </div>
                </div>
            <?php endif; ?>
    <?php endforeach; ?>
</div>

<?php if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?php echo $arResult["NAV_STRING"]?>
<?php endif;?>

<?php endif; ?>
