<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="<?=$arResult["FORM_ACTION"]?>" class="footer__subscribe-form">
	<div class="footer__subscribe-checkbox">
		<?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
			<label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
				<input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> style="display:none;"/> Рассылка: &laquo;<?=$itemValue["NAME"]?>&raquo;
			</label>
		<?endforeach;?>
	</div>
	<div class="footer__subscribe-inputs">
		<?if(empty($arResult["EMAIL"])) $arResult["EMAIL"]="Ваш e-mail";?>
		<input type="text" name="sf_EMAIL" size="20"  placeholder="<?=GetMessage("subscr_form_email_title")?>" class="footer__subscribe-input" />
		<div class="b-btn_white"><input type="submit" name="OK" value=""><?=GetMessage("subscr_form_button")?></div>
	</div>
</form>
