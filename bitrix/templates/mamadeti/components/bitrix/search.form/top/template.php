<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form class="b-header_search" action="<?=$arResult["FORM_ACTION"]?>">
    <input type="submit" name="s" value="" class="b-header_search__submit">
    <input type="text" name="q" value="" class="b-header_search__input" placeholder="Поиск по сайту...">
</form>
