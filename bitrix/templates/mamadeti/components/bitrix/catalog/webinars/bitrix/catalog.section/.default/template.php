<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?$prevYear = CIBlockFormatProperties::DateFormat("Y", MakeTimeStamp($arResult['ITEMS'][1]["PROPERTIES"]["DATE"]["VALUE"], CSite::GetDateFormat()));?>
<h2><?=$prevYear?></h2>
<table class="web-table" border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<th>№</th>
		<th>Дата проведения</th>
		<th>Место проведения</th>
		<th width="7%">Время проведения (местное)</th>
		<th>Тема</th>
		<?php /* <th>Анонс</th> */ ?>
		<th>Спикер</th>
	</tr>
		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$currentYear = CIBlockFormatProperties::DateFormat("Y", MakeTimeStamp($arElement["PROPERTIES"]["DATE"]["VALUE"], CSite::GetDateFormat()));
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		if($prevYear != $currentYear):?>
			</table>
			<br><h2><?=$currentYear?></h2>
			<table class="web-table" border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<th>№</th>
				<th>Дата проведения</th>
				<th>Место проведения</th>
				<th width="7%">Время проведения</th>
				<th>Тема</th>
				<?php /* <th>Анонс</th> */ ?>
				<th>Спикер</th>
			</tr>
		<?endif;?>
		<tr id="<?=$this->GetEditAreaId($arElement['ID']);?>">
			<td><?=++$cell?></td>
			<td><?=CIBlockFormatProperties::DateFormat("j F Y года (l)", MakeTimeStamp($arElement["PROPERTIES"]["DATE"]["VALUE"], CSite::GetDateFormat()))?></td>
			<td>
				<?php if (!empty($arElement['PROPERTIES']['PLACE']['DESCRIPTION'])): ?>
					<a href="<?php echo $arElement['PROPERTIES']['PLACE']['DESCRIPTION']; ?>">
				<?php endif; ?>
				<?=$arElement["PROPERTIES"]["PLACE"]["VALUE"]?>
				<?php if (!empty($arElement['PROPERTIES']['PLACE']['DESCRIPTION'])): ?>
					</a>
				<?php endif; ?>
			</td>
			<td><?=CIBlockFormatProperties::DateFormat("H:i", MakeTimeStamp($arElement["PROPERTIES"]["DATE"]["VALUE"], CSite::GetDateFormat()))?></td>
			<td>
			<?if(!empty($arElement['PROPERTIES']['IS_HAVE_VIDEO']['VALUE'])):?>
				<a href="<?=$arElement['DETAIL_PAGE_URL'];?>"><?=$arElement['NAME'];?></a>
			<?else:?>
				<span><?=$arElement['NAME'];?></span>
			<?endif;?>
			<?php if (!empty($arElement['PROPERTIES']['REGISTRATION_LINK']['VALUE'])): ?>
				<br>
				<a href="<?php echo $arElement['PROPERTIES']['REGISTRATION_LINK']['VALUE']; ?>"><strong>Регистрация</strong></a>
			<?php endif; ?>
			<?/*if ($arElement["PROPERTIES"]["VIDEO"]["VALUE"]):?>
				<a class="fancybox-media" href="<?=$arElement["PROPERTIES"]["VIDEO"]["VALUE"]?>"><?=$arElement["NAME"]?></a>
			<?else:?>
				<?=$arElement["NAME"]?>
			<?endif*/?>
			</td>
			<?php /* <td>
				<a class="fancybox-web readmore-web-btn" href="#fancy-text-<?=$arElement["ID"]?>" title="Полное описание">Полное описание</a>
				<div class="popup-full-desc" id="fancy-text-<?=$arElement["ID"]?>">
					<p><?=$arElement["DETAIL_TEXT"]?></p>
				</div>
			</td> */ ?>
			<td>
				<?php if (!empty($arElement['PROPERTIES']['EXECUTOR']['DESCRIPTION'])): ?>
					<a href="<?php echo $arElement['PROPERTIES']['EXECUTOR']['DESCRIPTION']; ?>">
				<?php endif; ?>
				<?=$arElement["PROPERTIES"]["EXECUTOR"]["VALUE"]?>
				<?php if (!empty($arElement['PROPERTIES']['EXECUTOR']['DESCRIPTION'])): ?>
					</a>
				<?php endif; ?>
			</td>
<?//CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ACTIVE_FROM"], CSite::GetDateFormat()));?>
		</tr>
		<?$prevYear = CIBlockFormatProperties::DateFormat("Y", MakeTimeStamp($arElement["PROPERTIES"]["DATE"]["VALUE"], CSite::GetDateFormat()));
		endforeach; // foreach($arResult["ITEMS"] as $arElement):?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</table>
</pre>