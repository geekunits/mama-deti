<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//$APPLICATION->SetPageProperty('PRE_TITLE','Вебинар');
$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_stricker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'ZOOM_DIV' => $strMainID.'_zoom_cont',
	'ZOOM_PICT' => $strMainID.'_zoom_pict'
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);

?><div class="bx_item_detail" id="<?=$arItemIDs['ID']; ?>">
	<div class="bx_item_container">
		<div class="bx_rt">

<?
if (!empty($arResult['DISPLAY_PROPERTIES'])):?>
<div class="item_info_section">
	<?if (!empty($arResult['DISPLAY_PROPERTIES'])):?>
	<dl>
	<?foreach ($arResult['DISPLAY_PROPERTIES'] as &$arProp):?>
		<?if($arProp['CODE'] == "DATE"):?>
			<dt><strong><?=$arProp['NAME'].':'?></strong> <?=ConvertDateTime($arProp['VALUE'], "DD.MM.YYYY", "ru")?></dt>
			<dt><strong>Время проведения:</strong> <?=ConvertDateTime($arProp['VALUE'], "HH:MI", "ru")?></dt>
		<?elseif($arProp['CODE'] == 'VIDEO'):?>
			<a href="<?=$arProp['VALUE']?>"><?=$arProp['NAME']?></a>
		<?else:?>
		<dt><strong><?=$arProp['NAME'];?>:</strong>
		<?=(
				is_array($arProp['DISPLAY_VALUE'])
				? implode(' / ', $arProp['DISPLAY_VALUE'])
				: $arProp['DISPLAY_VALUE']
			);?>
		</dt>
		<?endif;?>
	<?unset($arProp);?>
	<?endforeach;?>
	</dl>
	<?endif;?>
</div>
<?endif?>
			<div class="clb"></div>
		</div>
		<div class="bx_rb">
<div class="item_info_section">
<?
if ('' != $arResult['DETAIL_TEXT'])
{
?>
	<div class="bx_item_description">
		<?/*<div class="bx_item_section_name_gray" style="border-bottom: 1px solid #f2f2f2;"><? echo GetMessage('FULL_DESCRIPTION'); ?></div>*/?>
<?
	if ('html' == $arResult['DETAIL_TEXT_TYPE'])
	{
		echo $arResult['DETAIL_TEXT'];
	}
	else
	{
		?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
	}
?>
	</div>
<?
}
?>
</div>
		</div>
		<div class="bx_lb">
<div class="tac ovh">
<?/*$APPLICATION->IncludeComponent(
	"bitrix:catalog.socnets.buttons",
	"",
	array(
		"URL_TO_LIKE" => $APPLICATION->GetCurPageParam(),
		"TITLE" => "",
		"DESCRIPTION" => "",
		"IMAGE" => "",
		"FB_USE" => "Y",
		"TW_USE" => "Y",
		"GP_USE" => "Y",
		"VK_USE" => "Y",
		"TW_VIA" => "",
		"TW_HASHTAGS" => "",
		"TW_RELATED" => ""
	),
	$component,
	array("HIDE_ICONS" => "Y")
);*/?>
</div>
<div class="tab-section-container">
<?
if ('Y' == $arParams['USE_COMMENTS'])
{
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.comments",
	"",
	array(
		"ELEMENT_ID" => $arResult['ID'],
		"ELEMENT_CODE" => "",
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"URL_TO_COMMENT" => "",
		"WIDTH" => "",
		"COMMENTS_COUNT" => "5",
		"BLOG_USE" => $arParams['BLOG_USE'],
		"FB_USE" => $arParams['FB_USE'],
		"FB_APP_ID" => $arParams['FB_APP_ID'],
		"VK_USE" => $arParams['VK_USE'],
		"VK_API_ID" => $arParams['VK_API_ID'],
		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
		"CACHE_TIME" => $arParams['CACHE_TIME'],
		"BLOG_TITLE" => "",
		"BLOG_URL" => "",
		"PATH_TO_SMILE" => "/bitrix/images/blog/smile/",
		"EMAIL_NOTIFY" => "N",
		"AJAX_POST" => "Y",
		"SHOW_SPAM" => "Y",
		"SHOW_RATING" => "N",
		"FB_TITLE" => "",
		"FB_USER_ADMIN_ID" => "",
		"FB_APP_ID" => $arParams['FB_APP_ID'],
		"FB_COLORSCHEME" => "light",
		"FB_ORDER_BY" => "reverse_time",
		"VK_TITLE" => "",
	),
	$component,
	array("HIDE_ICONS" => "Y")
);?>
<?
}
?>
</div>
		</div>
			<div style="clear: both;"></div>
	</div>
	<div class="clb"></div>
</div>
<div class="social-ico">
	<div class="social-likes">
		<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
		<div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
		<div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
		<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
		<div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
	</div>
</div>
<a href="<?=$arResult['SECTION']['SECTION_PAGE_URL'];?>">Вернуться к списку</a>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>