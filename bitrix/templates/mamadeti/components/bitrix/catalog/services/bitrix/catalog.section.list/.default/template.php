<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h1 class="name-title">Услуги сети клиник “Мать и дитя”</h1>
<div class="list-holder">
	<ul class="item-listing"">
	<?foreach ($arResult['SECTIONS'] as $arSection){?>
		<li>
			<div class="content-block add-able">
				<div class="item-frame align-center">
					<div class="item-sector">
						<div class="image-item">
							<?if(!empty($arSection["PICTURE"])){?>
								<img src="<?=MakeImage($arSection["PICTURE"]["SRC"], array("z"=>1,"w"=>270,"h"=>210))?>" alt="<?=$arSection["NAME"]?>">
								<a href="<?=$arSection["SECTION_PAGE_URL"]?>">
									<span class="image-mask">
										<span class="add-link"></span>
									</span>
								</a>
							<?}?>
						</div>
						<div class="item-link">
							<a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
						</div>
					</div>
				</div>
			</div>
		</li>
	<?}?>
	</ul>
</div>