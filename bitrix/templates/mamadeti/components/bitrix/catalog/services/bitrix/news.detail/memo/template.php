<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="rt-detail">
    <?if($arResult["DETAIL_PICTURE"]["SRC"]):?>
        <div class="photo">
            <img
                src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
                alt="<?=$arResult["NAME"]?>"
                title="<?=$arResult["NAME"]?>"
                >
        </div>
    <?endif?>
    <div class="description">
        <?if($arResult["NAME"]){?>
            <div class="name-title"><?=$arResult["NAME"]?></div>
        <?}?>
        <?php if ($arResult['PROPERTIES']['SHOW_COUPON']['VALUE']): ?>
            <a href="#" class="print-coupon button01 block-button" style="width:235px;margin-bottom:10px">Распечатать купон</a>
        <?php endif; ?>
        <?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
            <div class="date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
        <?endif;?>
        <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
            <div class="preview"><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></div>
        <?endif;?>
        <?if($arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]){?>
            <div class="clinic">
                <strong>Клиника:</strong> <?php echo $arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]; ?>
            </div>
        <?}?>
        <?php
        $dateFrom = $arResult['DISPLAY_PROPERTIES']['DATE_FROM']['DISPLAY_VALUE'];
        $dateTo = $arResult['DISPLAY_PROPERTIES']['DATE_TO']['DISPLAY_VALUE'];
        ?>
        <?if($dateFrom || $dateTo){?>
            <div class="time-action">
                <?php if ($dateFrom && $dateTo): ?>
                    Акция проводится с <?php echo $dateFrom; ?> по <?php echo $dateTo; ?>.
                <?php elseif ($dateFrom):?>
                    Акция проводится с <?php echo $dateFrom; ?>.
                <?php elseif ($dateTo):?>
                    Акция проводится по <?php echo $dateTo; ?>.
                <?php endif;?>
            </div>
        <?}?>
        <div class="text">
            <?if(strlen($arResult["DETAIL_TEXT"])>0):?>
                <?echo $arResult["DETAIL_TEXT"];?>
            <?else:?>
                <?echo $arResult["PREVIEW_TEXT"];?>
            <?endif?>
			<?if (!empty($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"])):?>
				<br/><br/>
				<strong class="strong-title">Список услуг: </strong>
				<div class="list-cols"><div class="col">
					<ul class="link-list">
					<?if (is_array($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"])):?>
						<?foreach ($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"] as $val):?>
                            <?php if ($val): ?>
    							<li><?=$val?></li>
                            <?php endif; ?>
						<?endforeach?>
					<?else:?>
						<li><?=$arResult["DISPLAY_PROPERTIES"]["SERVICES"]["DISPLAY_VALUE"]?></li>
					<?endif?>
					</ul>
				</div></div>
			<?endif?>
        </div>
        <a class="back red-color" href="<?php echo preg_replace('/<a href="([^"]+)">.+<\/a>/', '$1', $arResult["DISPLAY_PROPERTIES"]["CLINIC"]["DISPLAY_VALUE"]); ?>#actions">вернуться назад</a>
    </div>
</div>
