<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$page=explode("/",$APPLICATION->GetCurDir());?>

<aside class="menu-col">
	<nav>
		<ul class="menu">
			<?foreach ($arResult['SECTIONS'] as $arSection){?>
			<li<?if (in_array($page[2], explode("/",$arSection["SECTION_PAGE_URL"]))){?> class="active"<?}?>>
				<a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
				<?if (in_array($page[2], explode("/",$arSection["SECTION_PAGE_URL"]))){
					$arFilter = Array("IBLOCK_ID"=>1, "SECTION_ID"=>$arSection["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("NAME", "DETAIL_PAGE_URL"));
					$count=$res->SelectedRowsCount();
					if ($count>0) echo "<ul>";
					while($arres = $res->GetNext()){ 
				?>
					<li<?if ($APPLICATION->GetCurDir()==$arres["DETAIL_PAGE_URL"]){?> class="active"<?}?>><a href="<?=$arres["DETAIL_PAGE_URL"]?>"><?=$arres["NAME"]?></a></li>
				<?}
					if ($count>0) echo "</ul>";
				}?>
			</li>
			<?}?>
		</ul>
	</nav>
</aside>