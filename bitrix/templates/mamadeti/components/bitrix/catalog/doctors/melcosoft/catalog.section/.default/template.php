<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$prevSecTitle = "";
?>

<?php if(!empty($arResult['ITEMS'])):?>
    <?php if($arParams["DISPLAY_TOP_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?><br />
    <?php endif; ?>
    <div class="b-clinic_doctors-list b-clinic_doctors">
        <?php foreach ($arResult['ITEMS'] as $arItem): ?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
            switch ($arParams["ELEMENT_SORT_FIELD"]) {
                case 'PROPERTY_SPECIALTY.NAME': $curSecTitle = $arItem["PROPERTY_SPECIALTY_NAME"];
                    break;
                case 'PROPERTY_CLINIC.NAME': $curSecTitle = $arItem["PROPERTY_CLINIC_NAME"];
                    break;
                default: $curSecTitle = '';
                    break;
            }

            if ($prevSecTitle != $curSecTitle) {
                echo '<h2>' . $curSecTitle . "</h2>";
                $prevSecTitle = $curSecTitle;
            }
            $imgSrc = null;
            $imgNoClass = null;
            if (is_array($arItem["PREVIEW_PICTURE"])) {
                $imgSrc = MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z" => 1, "w" => 190, "h" => 210));
            } else {
                $imgNoClass = $arItem['PROPERTIES']['SEX']['VALUE_XML_ID'] === 'F' ? 'b-doctor_nophoto__female' : 'b-doctor_nophoto__male';
            }
            ?>
            <div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="b-clinic_doctors-list__item">
                <div class="b-clinic_doctors__part-left">
                    <div class="b-doctor b-doctor_extra-big">
                        <?php if (!empty($imgSrc)): ?>
                            <a class="b-doctor_avatar" href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" target="_blank">
                                <img src="<?= $imgSrc; ?>" alt="<?= $arItem["NAME"] ?>">
                            </a>
                        <?php else: ?>
                            <a class="b-doctor_avatar <?php echo $imgNoClass; ?>" target="_blank" href="<?= $arItem["DETAIL_PAGE_URL"]; ?>"></a>
                        <?php endif; ?>
                    </div>
                    <?php if ($arItem["STATISTICS"]["REVIEW"] || $arItem["STATISTICS"]["QA"] || $arItem["STATISTICS"]["ARTICLE"]): ?>
                    <div class="b-clinic_doctors__activities">
                        <?php if ($arItem["STATISTICS"]["REVIEW"]): ?>
                            <a href="/reviews/?region=<?php echo reset($arItem['PROPERTIES']['CITY']['VALUE']); ?>&clinic=<?php echo reset($arItem['PROPERTIES']['CLINIC']['VALUE']); ?>&doctor=<?php echo $arItem['ID']; ?>" target="_blank"><?php echo $arItem["STATISTICS"]["REVIEW"]; ?> <?php echo plural($arItem["STATISTICS"]["REVIEW"], ['отзыв', 'отзыва', 'отзывов']); ?></a>
                        <?php endif; ?>
                        <?php if ($arItem["STATISTICS"]["QA"]): ?>
                            <a href="/qa/?doctor=<?php echo $arItem['ID']; ?>" target="_blank"><?php echo $arItem["STATISTICS"]["QA"]; ?> <?php echo plural($arItem["STATISTICS"]["QA"], ['ответ', 'ответа', 'ответов']); ?></a>
                        <?php endif; ?>
                        <?php if ($arItem['STATISTICS']['ARTICLE']): ?>
                            <a href="/article/?doctor=<?php echo $arItem['ID']; ?>" target="_blank"><?php echo $arItem["STATISTICS"]["ARTICLE"]; ?> <?php echo plural($arItem["STATISTICS"]["ARTICLE"], ['статья', 'статьи', 'статей']); ?></a>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                    <div class="b-clinic_doctors__buttons">
                        <a href="#" data-name="<?= $arItem["NAME"] ?>" data-clinic="<?= $GLOBALS["arrFilterDoctor"]['PROPERTY_CLINIC']; ?>" data-doctor="<?= $arItem["ID"]; ?>" class="b-btn_white b-clinic_doctors__btn js-ask_question_doctors_btn">
                            Задать вопрос врачу
                        </a>
                        <?php if (empty($arItem['PROPERTIES']['HIDDEN_BUTTON_BOOK']['VALUE'])): ?>
                            <a href="#" class="b-btn_white b-clinic_doctors__btn js-receiption_doctors_btn" <?= count($arItem['PROPERTY_9']) == 1 ? 'data-clinic="' . $arItem['PROPERTY_9'][0] . '"' : ''; ?> data-doctor="<?= $arItem["ID"]; ?>">
                                Записаться на прием
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="b-clinic_doctors__part-right">
                    <div class="b-clinic_doctors__description js-clinic_doctors__description">
                        <div class="b-clinic_doctors__description-name">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" target="_blank"><?= $arItem["NAME"] ?></a>
                        </div>
                        <div class="b-clinic_doctors__description-list">
                            <?if ($arItem["PROPERTIES"]["POST_FULL"]["VALUE"]):?>
                                <div class="b-clinic_doctors__description-list___item">
                                    <div class="b-clinic_doctors__description-title">Занимаемая должность:</div>
                                    <div class="b-clinic_doctors__description-value">
                                        <?=$arItem["PROPERTIES"]["POST_FULL"]["VALUE"]?>
                                    </div>
                                </div>
                            <?endif?>
                            <?php if (is_array($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"])): ?>
                                <div class="b-clinic_doctors__description-list___item">
                                    <div class="b-clinic_doctors__description-title">Специальности:</div>
                                    <?php foreach ($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"] as $arValue): ?>
                                        <div class="b-clinic_doctors__description-value">
                                            <?= $arValue["NAME"] ?>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            <?php endif; ?>
                            <?php if (is_array($arItem["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SERVICES"]["VALUE"])): ?>
                                <div class="b-clinic_doctors__description-list___item js-clinic_doctors__service-list">
                                    <div class="b-clinic_doctors__description-title">Услуги:</div>
                                    <?php foreach ($arItem["PROPERTIES"]["SERVICES"]["VALUE"] as $arValue): ?>
                                        <div class="b-clinic_doctors__description-value js-clinic_doctors__service-value">
                                            <a href="<?= $arValue["DETAIL_PAGE_URL"] ?>">- <?= $arValue["NAME"] ?></a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                            <?php if (is_array($arItem["PROPERTIES"]["SECTION"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SECTION"]["VALUE"])): ?>
                                <div class="b-clinic_doctors__description-list___item">
                                    <div class="b-clinic_doctors__description-title">Отделение:</div>
                                    <div class="b-clinic_doctors__description-value">
                                        <?php foreach ($arItem["PROPERTIES"]["SECTION"]["VALUE"] as $key => $arValue): ?>
                                            <?php if ($key > 0): ?>, <?php endif; ?>
                                            <?= $arValue["NAME"] ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="b-clinic_doctors__description-list___item">
                                <div class="b-clinic_doctors__description-title">Ведет прием в:</div>
                                <?php foreach($arItem['CLINIC'] as $clinicName): ?>
                                    <div class="b-clinic_doctors__description-value">
                                        <?= $clinicName; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?php endif; ?>
<?php else: ?>
    <span class="name-title red-color">Для вашего города докторов не найдено.</span>
<?php endif; ?>
