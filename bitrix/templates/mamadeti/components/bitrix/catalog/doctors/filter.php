<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

$ajax_filter = isset($_REQUEST["ajax_filter"]) && $_REQUEST["ajax_filter"] == "y";
$ajax_filter_control = isset($_REQUEST["ajax_filter_control"]) && $_REQUEST["ajax_filter_control"] == "y";

if ($ajax_filter || $ajax_filter_control)
    $APPLICATION->RestartBuffer();

global ${$arParams["FILTER_NAME"]};

$arrFilter = & ${$arParams["FILTER_NAME"]};
if (!is_array($arrFilter))
    $arrFilter = array();

$CITY_ID = GetCurrentCity();

$arRegionClinic = CMamaDetiAPI::getClinicsID(array("PROPERTY_CITY"=>$CITY_ID));

$arAvailableSort = array(
    "specialty" => Array("PROPERTY_SPECIALTY.NAME", "ASC"),
    "clinic" => Array('PROPERTY_CLINIC.NAME', "ASC"),
);
$bShowSort = false;

$sort_key = array_key_exists("sort", $_REQUEST) && array_key_exists(ToLower($_REQUEST["sort"]), $arAvailableSort) ? ToLower($_REQUEST["sort"]) : "clinic";
$sort_order = array_key_exists("order", $_REQUEST) && in_array(ToLower($_REQUEST["order"]), Array("asc", "desc")) ? ToLower($_REQUEST["order"]) : $arAvailableSort[$sort_key][1];
$sort = $arAvailableSort[$sort_key][0];

$sort2 = "NAME";
$sort2_order = "ASC";

$sort3 = "SORT";
$sort3_order = "ASC";

if ($selClinic > 0 && $selSpecialty > 0) {
    $sort = "SORT";
    $sort_order = "ASC";
    $sort2 = "NAME";
    $sort2_order = "ASC";
    $sort3 = "";
    $sort3_order = "";
} elseif ($selClinic > 0) {
    $sort = "PROPERTY_SPECIALTY.NAME";
    $sort_order = "ASC";
    $sort2 = "SORT";
    $sort2_order = "ASC";
    $sort3 = "NAME";
    $sort3_order = "ASC";
} elseif ($selSpecialty > 0) {
    $sort = "PROPERTY_CLINIC.NAME";
    $sort_order = "ASC";
    $sort2 = "SORT";
    $sort2_order = "ASC";
    $sort3 = "NAME";
    $sort3_order = "ASC";
} else $bShowSort = !$bHideSort;

$arSearchTags = array();
if (!empty($_REQUEST["tags"])) { 
    $arSearchTags = explode(",", $_REQUEST["tags"]);
}
$searchDoctor = trim($_REQUEST["search_doctor"]);
$selClinic = intval($_REQUEST["clinic"]);
$selSpecialty = intval($_REQUEST["specialty"]);

if (!empty($_REQUEST["search_doctor"])) {
    CModule::IncludeModule("search");
    $arSearchParams = array(
        "MODULE_ID" => "iblock",
        "!ITEM_ID" => "S%",
        "PARAM2" => 3, //инфоблок
        "QUERY" => $searchDoctor,
        'PARAMS' => [
            'region' => GetCurrentCity(),
        ]
    );

    $arID = array();
    $obSearch = new CSearch;
    $obSearch->Search($arSearchParams, [], ['STEMMING' => false]);

    while ($arSearch = $obSearch->Fetch()) {
        $arID[] = $arSearch["ITEM_ID"];
    }

    if (count($arID) == 0)
        $arID[] = 0;
    $arrFilter["ID"] = $arID;
}
$selServices = array();
if (is_array($_REQUEST["services"])) {
    foreach ($_REQUEST["services"] as $val)
        if (substr($val, 0, 1) == 'E')
            $selServices[] = intval(substr($val, 1));
}
$selAlphabet = $_REQUEST["alphabet"];

/**********************************/
$arFilter = array("PROPERTY_CLINIC"=>$arRegionClinic);
if ($selSpecialty > 0)
    $arFilter["PROPERTY_SPECIALTY"] = $selSpecialty;
if ($selAlphabet)
    $arFilter["NAME"] = $selAlphabet . "%";
if (!empty($selServices))
    $arFilter["PROPERTY_SERVICES"] = $selServices;

$arClinics = CMamaDetiAPI::getDoctorsClinics($arFilter);

/**********************************/

$arFilter = array("PROPERTY_CLINIC" => $selClinic > 0 ? $selClinic : $arRegionClinic);
if ($selAlphabet)
    $arFilter["NAME"] = $selAlphabet . "%";
if (!empty($selServices))
    $arFilter["PROPERTY_SERVICES"] = $selServices;

$arSpecialty = CMamaDetiAPI::getDoctorsSpecialty($arFilter);

/**********************************/

$arAllServices = CMamaDetiAPI::getDoctorsServices($arFilter);
$arFilter = array("PROPERTY_CLINIC" => $selClinic > 0 ? $selClinic : $arRegionClinic);
if ($selSpecialty > 0)
    $arFilter["PROPERTY_SPECIALTY"] = $selSpecialty;
if ($selAlphabet)
    $arFilter["NAME"] = $selAlphabet . "%";

$arServices = CMamaDetiAPI::getDoctorsServices($arFilter);

/**********************************/

$arFilter = array("PROPERTY_CLINIC" => $selClinic > 0 ? $selClinic : $arRegionClinic);
$arAllAlphabet = CMamaDetiAPI::getDoctorsFirstAlphabet($arFilter);
if ($selSpecialty > 0)
    $arFilter["PROPERTY_SPECIALTY"] = $selSpecialty;
if (!empty($selServices))
    $arFilter["PROPERTY_SERVICES"] = $selServices;

$arAlphabet = CMamaDetiAPI::getDoctorsFirstAlphabet($arFilter);

/**********************************/

$arFilter = array("PROPERTY_CLINIC" => $selClinic > 0 ? $selClinic : $arRegionClinic);
if ($selSpecialty > 0)
    $arFilter["PROPERTY_SPECIALTY"] = $selSpecialty;
if (!empty($selServices))
    $arFilter["PROPERTY_SERVICES"] = $selServices;
if ($selAlphabet)
    $arFilter["NAME"] = $selAlphabet . "%";


$arDoctors = CMamaDetiAPI::getDoctors($arFilter);

/**********************************/

$arTreeAllServices = CMamaDetiAPI::GetSelectedServiceTree(array_keys($arAllServices));
$arTreeServices = CMamaDetiAPI::GetSelectedServiceTree(array_keys($arServices));

$arTreeVisible = array();

foreach ($arTreeServices as $arItem)
    $arTreeVisible[] = $arItem["ID"];

if (!empty($arSearchTags))
    $arrFilter["NAME"] = $arSearchTags;

if ($selClinic > 0)
    $arrFilter["=PROPERTY_CLINIC"] = $selClinic; else
    $arrFilter["=PROPERTY_CLINIC"] = $arRegionClinic;

if ($selSpecialty > 0)
    $arrFilter["PROPERTY_SPECIALTY"] = $selSpecialty;

$arrFilter[] = array("LOGIC" => "AND", array("NAME" => $selAlphabet . "%", "PROPERTY_SERVICES" => $selServices));
?>
<?php if ($ajax_filter): ?>
    <?php

    $jsClinics = array();
    foreach ($arClinics as $id => $name) {
        $jsClinics[] = array("ID" => $id, "NAME" => htmlspecialcharsbx($name));
    }

    $jsSpecialty = array();
    foreach ($arSpecialty as $id => $name) {
        $jsSpecialty[] = array("ID" => $id, "NAME" => htmlspecialcharsbx($name));
    }

    $jsDoctors = array();
    foreach ($arDoctors as $arDoctor) {
        $detailPageUrl = $arDoctor["DETAIL_PAGE_URL"];
        if ($selClinic > 0) {
            $detailPageUrl .= '?clinic=' . $selClinic;
        }
        $jsDoctors[] = array("NAME" => $arDoctor["NAME"], "DETAIL_PAGE_URL" => $detailPageUrl);
    }

    $jsAvailService = array();
    foreach ($arTreeServices as $arItem) {
        $jsAvailService[] = $arItem["ID"];
    }

    ?>
    <div id="ajax_script">
        <script>
            var selAlphabet = <?=CUtil::PhpToJSObject($selAlphabet)?>;
            var arClinics = <?=CUtil::PhpToJSObject($jsClinics)?>;
            var arSpecialty = <?=CUtil::PhpToJSObject($jsSpecialty)?>;
            var arDoctors = <?=CUtil::PhpToJSObject($jsDoctors)?>;
            var arService = <?=CUtil::PhpToJSObject($jsAvailService)?>;
            var arAlphabet = <?=CUtil::PhpToJSObject($arAlphabet)?>;

        document.title = '<?=CUtil::JSEscape($APPLICATION->GetTitle())?>';
        window.history.pushState('page2', '<?=CUtil::JSEscape($APPLICATION->GetTitle())?>', '<?=CUtil::JSEscape($APPLICATION->GetCurPageParam())?>');

            var selClinic = $('select[name="clinic"]'),
                curClinic = parseInt(selClinic.val());

            selClinic.find('option:not(:first)').remove();
            $(arClinics).each(function () {
                if (this.ID == curClinic) {
                    selClinic.append('<option value="' + this.ID + '" selected="selected">' + this.NAME + '</option>'); 
                } else {
                    selClinic.append('<option value="' + this.ID + '">' + this.NAME + '</option>');
                }
            });
            selClinic.trigger('refresh');

            var selSpecialty = $('select[name="specialty"]'),
                curSpecialty = parseInt(selSpecialty.val());

            selSpecialty.find('option:not(:first)').remove();
            $(arSpecialty).each(function () {
                if (this.ID == curSpecialty) {
                    selSpecialty.append('<option value="' + this.ID + '" selected="selected">' + this.NAME + '</option>'); 
                } else {
                    selSpecialty.append('<option value="' + this.ID + '">' + this.NAME + '</option>');
                }
            });
            selSpecialty.trigger('refresh');

            $('.b-dest_alph__letter-wrap .b-dest_alph__letter').removeClass('b-dest_alph__letter-active').filter(function () {
                return $(this).data("id") == selAlphabet;
            }).addClass('b-dest_alph__letter-active');

            var doctorTree = $('.b-dest_alph__list-scroll');
            doctorTree.find('.b-dest_alph__list-doctors___item').remove();

            $(arDoctors).each(function () {
                doctorTree.append(
                    '<div class="b-dest_alph__list-doctors___item">'
                    + '<a href="' + this.DETAIL_PAGE_URL + '">' + this.NAME + '</a>'
                    + '</div>');
                }
            );

            $('.b-tree_links__wrap [id^=st_]').each(function () {
                if ($.inArray($(this).attr("id").substr(3), arService) == -1) {
                    $(this).css('display', 'none'); 
                } else {
                        $(this).css('display', '');
                }
            });

            $('.b-dest_alph__letter-wrap .b-dest_alph__letter').each(function () {
                if ($.inArray($(this).data("id"), arAlphabet) == -1) {
                    $(this).css('display', 'none');
                } else {
                    $(this).css('display', '');
                }
            });

        </script>
    </div>
<?php else: ?>
    <div class="topic-panel">
        <div class="align-left">
            <?php if ($bShowSort): ?>
                <h1 class="name-title">Поиск врача</h1>
            <?php else: ?>
                <div class="name-title">Поиск врача</div>
            <?php endif; ?>
        </div>
        <div class="align-right">
            <div class="place-block">
                <div class="point-block">
                    <div class="align-left">
                        <span class="point-title">Ваше местоположение:</span>
                    </div>
                    <div class="align-left">
                        <div class="point-select active">
                            <a href="#" class="point-link red-color change_city"><?= GetNameCity($CITY_ID) ?></a>

                            <div class="point-drop">
                                <ul class="point-list">
                                    <?php foreach ($GLOBALS["CITY_LIST"] as $id => $arCity) { ?>
                                        <li><a href="<?php echo CityManager::getInstance()->getUrlByCityId($id); ?>" data-id="<?= $id ?>"><?= $arCity["NAME"] ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="point-text">определено автоматически</span>
            </div>
        </div>
    </div>
    <span class="content-title">Введите ФИО врача в строку поиска для установки фильтра</span>
    <form id="search-tree-filter" action="<?= $APPLICATION->GetCurPage(); ?>" class="search-doctor-filter">
        <input type="hidden" name="alphabet" value="<?= htmlspecialcharsbx($selAlphabet) ?>">
        <div class="search-field" id="search-doctors">
            <div class="search-sector active">
                <div class="search-input">
                    <div class="align-left">
                        <ul class="search-list">
                            <?foreach ($arSearchTags as $t) {
                                if (!empty($t)) {
                                    ?>
                                    <li><span class="title"><?= $t ?></span><a class="remove-button"
                                                                               href="#">remove</a></li>
                                <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <input type="button" class="button06 bt_search_doctor" value="">

                    <div class="text03">
                        <input type="text" class="input_search_doctor" name="search_doctor"
                               placeholder="Например Иванов Иван Иванович"
                               value="<?= htmlspecialcharsbx($_REQUEST["search_doctor"]) ?>">
                    </div>
                </div>
                <div id="search_result" class="search-drop" style="display:none;">
                    <div class="drop-frame">
                        <div class="scrollpane">
                            <ul class="search-listing"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-holder">
            <span class="button02 block-button"><input type="submit" value="">найти</span>
        </div>
        <div class="clearfix"></div>
        <span class="content-title">Введите клинику и специальность для установки фильтра</span>
        <div class="b-filters_doctors b-filters_doctors__wrap clearfix">
            <div class="b-filters_doctors__left jq-selectbox__select__red">
                <select name="clinic" style="width:250px;">
                    <option value="0">Все клиники</option>
                    <?php foreach ($arClinics as $id => $name): ?>
                        <option
                            value="<?= $id ?>"<?php if ($id == $selClinic): ?> selected="selected"<?php endif ?>><?= htmlspecialcharsbx($name) ?></option>
                    <?php endforeach ?>
                </select>
                &nbsp;&nbsp;
                <select name="specialty" style="width:250px;">
                    <option value="0">Все специальности</option>
                    <?php foreach ($arSpecialty as $id => $name): ?>
                        <option value="<?= $id; ?>"<?php if ($id == $selSpecialty): ?> selected="selected"<?php endif ?>><?= htmlspecialcharsbx($name); ?></option>
                    <?php endforeach ?>
                </select>

                <div class="b-tree_links__wrap">
                    <?php
                        $previousLevel = 0;
                        $serviceId = null;

                        if (!empty($arTreeAllServices)) {
                            $serviceId = array_pop($arTreeAllServices);
                        }
                    ?>
                    <?php for ($index = 0, $arItems = $arTreeAllServices, $count = count($arItems); $index < $count; $index++): ?>
                        <?php
                            $arItem = $arItems[$index];
                            $arNextItem = $index + 1 < $count ? $arItems[$index + 1] : null;
                            $hasSubItems = $arNextItem && $arNextItem['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'];
                        ?>
                        <?php for ($depth = $previousLevel; $depth > $arItem['DEPTH_LEVEL']; $depth--): ?>
                            </div></div>
                        <?php endfor; ?>
                        <div class="b-tree_link__item" id="st_<?php echo $arItem['ID']; ?>">
                            <div class="b-tree_link">
                                <?php if ($hasSubItems): ?>
                                    <div class="b-tree_link__plus js-tree-click">
                                        <div class="js-tree-plus">+</div>
                                    </div>
                                    <span class="js-tree-select" href="#" data-id="<?= $arItem["ID"]; ?>"><?php echo $arItem['TEXT']; ?></span>
                                <?php else: ?>
                                     <input type="checkbox" 
                                        name="services[]"
                                        value="<?= $arItem["ID"] ?>"
                                        <?php if (in_array(substr($arItem["ID"], 1), $arResult["SEL_SERVICES"])): ?> checked="checked" <?php endif; ?>>&nbsp;<?= $arItem["TEXT"]; ?>
                                <?php endif; ?>
                            </div>
                            <?php if ($hasSubItems): ?>
                                <div class="b-tree_links__sub js-tree_links__sub-<?php echo $arItem['DEPTH_LEVEL']; ?>">
                            <?php else: ?>
                                </div>
                            <?php endif; ?>
                        <?php $previousLevel = $arItem['DEPTH_LEVEL']; ?>
                    <?php endfor; ?>
                    
                    <?php while($previousLevel-- > 1): ?>
                        </div></div>
                    <?php endwhile; ?>        
                </div>
            </div>
            <div class="b-filters_doctors__right">
                <div class="b-dest_alph">
                    <div class="b-dest_alph__letter-wrap">
                        <?php foreach ($arAllAlphabet as $val): ?>
                            <a 
                                class="b-dest_alph__letter <?= $val == $selAlphabet ? 'b-dest_alph__letter-active' : ''; ?>" 
                                href="<?= $APPLICATION->GetCurPageParam('alphabet=' . htmlspecialcharsbx($val), array('alphabet')); ?>"
                                data-id="<?= htmlspecialcharsbx($val); ?>"
                                <?php if (!in_array($val, $arAlphabet)):?> 
                                    style="display:none;"
                                <?php endif; ?>
                            >
                                <span class="b-dest_alph__letter___wrap"><?= $val; ?></span>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <div class="b-dest_alph__list-doctors">
                        <div class="b-dest_alph__list-scroll">
                            <?php foreach ($arDoctors as $arDoctor): ?>
                                <div class="b-dest_alph__list-doctors___item">
                                    <?php
                                        $detailPageUrl = $arDoctor["DETAIL_PAGE_URL"];
                                        if ($selClinic > 0) {
                                            $detailPageUrl .= '?clinic=' . $selClinic;
                                        }
                                    ?>
                                    <a href="<?= $detailPageUrl; ?>" target="_blank"><?= $arDoctor['NAME']; ?></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script>

        var form = $('#search-tree-filter').submit(
            function (e) {

                BX.showWait(BX('search-tree-filter'));

                $.ajax({url: '/doctors/?' + form.serialize(), dataType: 'html', type: 'POST', data: {ajax_filter: "y"}}).done(function (data) {
                    data = '<div>' + data + '</div>';
                    $('#ajax_section').html($(data).find('#ajax_section'));
                    $('.js-sort-panel').html($(data).find('.sort-panel'));

                    eval($(data).find("#ajax_script script").html());

                }).always(function () {
                    BX.closeWait(BX('search-tree-filter'));
                });

                return false;
            }
        );

        form.find("a.b-dest_alph__letter").click(function (e) {
            e.preventDefault();
            var $this = $(this);
            if ($this.hasClass('b-dest_alph__letter-active')) {
                form.find('input[name="alphabet"]').val('');
                $this.removeClass('.b-dest_alph__letter-active');
            } else {
                form.find('input[name="alphabet"]').val($this.data('id'));
            }
            form.submit();
        });

        form.find('input[type=checkbox], select').change(function () {
            form.submit();
        });

    </script>

<?endif ?>
<?if ($ajax_filter_control) die();?>

<?php if ($bShowSort): ?>
    <br>
    <div class="js-sort-panel">
        <div class="sort-panel">
            <div class="align-left">
                <span class="sort-title">Группировать</span>
                <ul class="sort-list">
                    <li<?php if ($sort_key == 'specialty'): ?> class="active"<?php endif ?>>
                        <a href="<?= $APPLICATION->GetCurPageParam("sort=specialty", array("sort")); ?>"
                           class="button07"><span class="icon"><img src="<?= SITE_TEMPLATE_PATH ?>/images/icon13.png"
                                                                    width="15" height="11" alt=""></span><span
                                class="title">по специальности</span></a>
                    </li>
                    <li<?php if ($sort_key == "clinic"): ?> class="active"<?php endif ?>>
                        <a href="<?= $APPLICATION->GetCurPageParam("sort=clinic", array("sort")); ?>"
                           class="button07"><span
                                class="icon"><img src="<?= SITE_TEMPLATE_PATH ?>/images/icon14.png" width="12"
                                                  height="12"
                                                  alt=""></span><span class="title">по клинике</span></a>
                    </li>
                    <?php /*            <li class="last<?if($_REQUEST["sort"]=="sect"):?> active<?endif?>">
                    <a href="<?=$APPLICATION->GetCurPageParam("sort=sect", array("sort"));?>" class="button07"><span class="icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon15.png" width="18" height="13" alt=""></span><span class="title">по отделению</span></a>
                </li>*/
                    ?>
                </ul>
            </div>
            <?php /*
        <div class="block-holder align-right">
            <span class="sort-title">Группировать</span>
            <ul class="sort-list">
                <li<?if ((empty($_REQUEST["order"])) || ($_REQUEST["order"]=="name")) {?> class="active"<?}?>>
                    <a href="<?=$APPLICATION->GetCurPageParam("order=name", array("order"));?>" class="button07"><span class="icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon13.png" width="15" height="11" alt=""></span><span class="title">по алфавиту</span></a>
                </li>
                <li<?if ($_REQUEST["order"]=="clinic") {?> class="active"<?}?>>
                    <a href="<?=$APPLICATION->GetCurPageParam("order=clinic", array("order"));?>" class="button07"><span class="icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon14.png" width="12" height="12" alt=""></span><span class="title">по клинике</span></a>
                </li>
                <li class="last<?if ($_REQUEST["order"]=="sect") {?> active<?}?>">
                    <a href="<?=$APPLICATION->GetCurPageParam("order=sect", array("order"));?>" class="button07"><span class="icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon15.png" width="18" height="13" alt=""></span><span class="title">по отделению</span></a>
                </li>
            </ul>
        </div>
    */
            ?>
        </div>
    </div>
<?php endif ?>
