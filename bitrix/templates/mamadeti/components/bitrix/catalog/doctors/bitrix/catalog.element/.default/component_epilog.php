<?php 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$clinicId = null;
$clinicIds = $arResult['PROPERTIES']['CLINIC']['VALUE'];
if (isset($_GET['clinic']) && is_array($clinicIds) && in_array($_GET['clinic'], $clinicIds)) {
    $clinicId = (int) $_GET['clinic'];
}
if (!empty($clinicId)) {
	$formatter = new ClinicResultFormatter($clinicId);
	$clinicMenu = new ClinicMenu($formatter->format(), [
	    'useAjax' => false,
	    'detectCurrentTab' => false,
	    'currentTabName' => 'doctors',
	]);
	$APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());

}
// $APPLICATION->SetTitle('Врач ' . $arResult['NAME'] . ': отзывы, запись на приём, задать вопрос');
$APPLICATION->SetPageProperty('title', 'Врач ' . $arResult['NAME'] . ': отзывы, запись на приём, задать вопрос');