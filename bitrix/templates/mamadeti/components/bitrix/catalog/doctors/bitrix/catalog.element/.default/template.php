<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
if ($arParams['ADD_SECTIONS_CHAIN'] && !empty($arResult['NAME'])) {
   $arResult['SECTION']['PATH'][] = array(
   'NAME' => $arResult['NAME'],
   'PATH' => ' ');

   $component = $this->__component;
   $component->arResult = $arResult;
}
?>

<?php
$imgSrc = null;
$imgNoClass = null;

if (is_array($arResult["PREVIEW_PICTURE"])) {
    $imgSrc = MakeImage($arResult["PREVIEW_PICTURE"]["SRC"], array("z" => 1, "w" => 190, "h" => 210));
} else {
    $imgNoClass = $arResult['PROPERTIES']['SEX']['VALUE_XML_ID'] === 'F' ? 'b-doctor_nophoto__female' : 'b-doctor_nophoto__male';
}
?>
<h1>
   <?= $arResult["NAME"] ?>
</h1>
<div class="b-clinic_doctors-list b-clinic_doctors">
    <div id="<?= $this->GetEditAreaId($arResult['ID']); ?>" class="b-clinic_doctors-list__item">
        <div class="b-clinic_doctors__part-left">
            <div class="b-doctor b-doctor_extra-big">
                <?php if (!empty($imgSrc)): ?>
                    <div class="b-doctor_avatar">
                        <img src="<?= $imgSrc; ?>" alt="<?= $arResult["NAME"] ?>">
                    </div>
                <?php else: ?>
                    <div class="b-doctor_avatar <?php echo $imgNoClass; ?>"></div>
                <?php endif; ?>
            </div>
            <?php if ($arResult["STATISTICS"]["REVIEW"] || $arResult["STATISTICS"]["QA"] || $arResult['STATISTICS']['ARTICLE']): ?>
            <div class="b-clinic_doctors__activities">
                <?php if ($arResult["STATISTICS"]["REVIEW"]): ?>
                    <a href="/reviews/?region=<?php echo reset($arResult['PROPERTIES']['CITY']['VALUE']); ?>&clinic=<?php echo reset($arResult['PROPERTIES']['CLINIC']['VALUE']); ?>&doctor=<?php echo $arResult['ID']; ?>"><?php echo $arResult["STATISTICS"]["REVIEW"]; ?> <?php echo plural($arResult["STATISTICS"]["REVIEW"], ['отзыв', 'отзыва', 'отзывов']); ?></a>
                <?php endif; ?>
                <?php if ($arResult["STATISTICS"]["QA"]): ?>
                    <a href="/qa/?doctor=<?php echo $arResult['ID']; ?>"><?php echo $arResult["STATISTICS"]["QA"]; ?> <?php echo plural($arResult["STATISTICS"]["QA"], ['ответ', 'ответа', 'ответов']); ?></a>
                <?php endif; ?>
                <?php if ($arResult['STATISTICS']['ARTICLE']): ?>
                    <a href="/article/?doctor=<?php echo $arResult['ID']; ?>"><?php echo $arResult["STATISTICS"]["ARTICLE"]; ?> <?php echo plural($arResult["STATISTICS"]["ARTICLE"], ['статья', 'статьи', 'статей']); ?></a>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <div class="b-clinic_doctors__buttons">
                <a href="#" data-name="<?= $arResult["NAME"] ?>" data-clinic="<?= $GLOBALS["arrFilterDoctor"]['PROPERTY_CLINIC']; ?>" data-doctor="<?= $arResult["ID"]; ?>" class="b-btn_white b-clinic_doctors__btn js-ask_question_doctors_btn">
                    Задать вопрос врачу
                </a>
                <?php if (empty($arResult['PROPERTIES']['HIDDEN_BUTTON_BOOK']['VALUE'])): ?>
                    <a href="#" class="b-btn_white b-clinic_doctors__btn js-receiption_doctors_btn" <?= count($arResult['PROPERTY_9']) == 1 ? 'data-clinic="' . $arResult['PROPERTY_9'][0] . '"' : ''; ?> data-doctor="<?= $arResult["ID"]; ?>">
                        Записаться на прием
                    </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="b-clinic_doctors__part-right">
            <div class="b-clinic_doctors__description js-clinic_doctors__description">
                <div class="b-clinic_doctors__description-list">
                    <?if ($arResult["PROPERTIES"]["SLOGAN"]["VALUE"]):?>
                        <div class="b-clinic_doctors__description-list___item ">
                            <div class="b-clinic_doctors__description-value b-clinic_doctors__description-slogan">
                                <?if ($arResult["PROPERTIES"]["SLOGAN"]["VALUE"]["TYPE"] == "html"):?>
                                    <?=$arResult["PROPERTIES"]["SLOGAN"]["~VALUE"]["TEXT"]?>
                                <?else:?>
                                    <?=$arResult["PROPERTIES"]["SLOGAN"]["VALUE"]["TEXT"]?>
                                <?endif?>
                            </div>
                        </div>
                    <?endif?>
                    <?php if (is_array($arResult["PROPERTIES"]["SPECIALTY"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SPECIALTY"]["VALUE"])): ?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">Специальности:</div>
                            <?php foreach ($arResult["PROPERTIES"]["SPECIALTY"]["VALUE"] as $arValue): ?>
                                <div class="b-clinic_doctors__description-value">
                                    <?= $arValue["NAME"] ?>
                                </div>
                            <?php endforeach ?>
                        </div>
                    <?php endif; ?>
                    <?if ($arResult["PROPERTIES"]["CATEGORY"]["VALUE"]):?>
                        <div>
                            <?=$arResult["PROPERTIES"]["CATEGORY"]["VALUE"]?>
                        </div>
                    <?endif?>
                    <?if ($arResult["PROPERTIES"]["POST"]["VALUE"][0]):?>
                        <div class="b-clinic_doctors__description-list___item">
                        <?foreach ($arResult["PROPERTIES"]["POST"]["VALUE"] as $key => $val) {?>
                            <div class="b-clinic_doctors__description-value">
                                <?=$val?>
                            </div>
                        <?}?>
                        </div>
                    <?endif?>
                    <?if ($arResult["PROPERTIES"]["POST_FULL"]["VALUE"]):?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-value">
                                <?=$arResult["PROPERTIES"]["POST_FULL"]["VALUE"]?>
                            </div>
                        </div>
                    <?endif?>
                    <?if ($arResult["PROPERTIES"]["EDUCATION"]["VALUE"]["TEXT"]):?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">Образование:</div>
                            <div class="b-clinic_doctors__description-value">
                                <?if ($arResult["PROPERTIES"]["EDUCATION"]["VALUE"]["TYPE"] == "html"):?>
                                    <?=$arResult["PROPERTIES"]["EDUCATION"]["~VALUE"]["TEXT"]?>
                                <?else:?>
                                    <?=$arResult["PROPERTIES"]["EDUCATION"]["VALUE"]["TEXT"]?>
                                <?endif?>
                            </div>
                        </div>
                    <?endif?>
                    <?if ($arResult["PROPERTIES"]["CERTIFICATES"]["VALUE"][0]):?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">Сертификаты:</div>
                            <div class="b-clinic_doctors__description-value">
                                <?foreach ($arResult["PROPERTIES"]["CERTIFICATES"]["VALUE"] as $key => $val) {?>
                                    <div class="b-clinic_doctors__description-value">
                                        <?=$val?>
                                    </div>
                                <?}?>
                            </div>
                        </div>
                    <?endif?>

                    <?if ($arResult["PROPERTIES"]["SKILLS"]["VALUE"]):?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">Профессиональные навыки:</div>
                            <div class="b-clinic_doctors__description-value">
                                <?if ($arResult["PROPERTIES"]["SKILLS"]["VALUE"]["TYPE"] == "html"):?>
                                    <?=$arResult["PROPERTIES"]["SKILLS"]["~VALUE"]["TEXT"]?>
                                <?else:?>
                                    <?=$arResult["PROPERTIES"]["SKILLS"]["VALUE"]["TEXT"]?>
                                <?endif?>
                            </div>
                        </div>
                    <?endif?>
                    <?if ($arResult["PROPERTIES"]["MANIPULATIONS"]["VALUE"]):?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">Клиническая практика:</div>
                            <div class="b-clinic_doctors__description-value">
                                <?if ($arResult["PROPERTIES"]["MANIPULATIONS"]["VALUE"]["TYPE"] == "html"):?>
                                    <?=$arResult["PROPERTIES"]["MANIPULATIONS"]["~VALUE"]["TEXT"]?>
                                <?else:?>
                                    <?=$arResult["PROPERTIES"]["MANIPULATIONS"]["VALUE"]["TEXT"]?>
                                <?endif?>
                            </div>
                        </div>
                    <?endif?>
                    <?if ($arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"]):?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">Опыт работы:</div>
                            <div class="b-clinic_doctors__description-value">
                                <?if ($arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"]["TYPE"] == "html"):?>
                                    <?=$arResult["PROPERTIES"]["EXPERIENCE"]["~VALUE"]["TEXT"]?>
                                <?else:?>
                                    <?=$arResult["PROPERTIES"]["EXPERIENCE"]["VALUE"]["TEXT"]?>
                                <?endif?>
                            </div>
                        </div>
                    <?endif?>
                    <?if ($arResult["PROPERTIES"]["MY_HISTORY"]["VALUE"]):?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">История моего становления:</div>
                            <div class="b-clinic_doctors__description-value">
                                <?if ($arResult["PROPERTIES"]["MY_HISTORY"]["VALUE"]["TYPE"] == "html"):?>
                                    <?=$arResult["PROPERTIES"]["MY_HISTORY"]["~VALUE"]["TEXT"]?>
                                <?else:?>
                                    <?=$arResult["PROPERTIES"]["MY_HISTORY"]["VALUE"]["TEXT"]?>
                                <?endif?>
                            </div>
                        </div>
                    <?endif?>
                    <?php if (is_array($arResult["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SERVICES"]["VALUE"])): ?>
                        <div class="b-clinic_doctors__description-list___item js-clinic_doctors__service-list">
                            <div class="b-clinic_doctors__description-title">Услуги:</div>
                            <?php foreach ($arResult["PROPERTIES"]["SERVICES"]["VALUE"] as $arValue): ?>
                                <div class="b-clinic_doctors__description-value js-clinic_doctors__service-value">
                                    <a href="<?= $arValue["DETAIL_PAGE_URL"] ?>">- <?= $arValue["NAME"] ?></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if (is_array($arResult["PROPERTIES"]["SECTION"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SECTION"]["VALUE"])): ?>
                        <div class="b-clinic_doctors__description-list___item">
                            <div class="b-clinic_doctors__description-title">Отделение:</div>
                            <div class="b-clinic_doctors__description-value">
                                <?php foreach ($arResult["PROPERTIES"]["SECTION"]["VALUE"] as $key => $arValue): ?>
                                    <?php if ($key > 0): ?>, <?php endif; ?>
                                    <?= $arValue["NAME"] ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif ?>
                    <div class="b-clinic_doctors__description-list___item">
                        <div class="b-clinic_doctors__description-title">Ведет прием в:</div>
                        <?php foreach($arResult['CLINIC'] as $clinicName): ?>
                            <div class="b-clinic_doctors__description-value">
                                <?= $clinicName; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
