<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule("iblock");
$arResult["CLINIC"] = '';
$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y", "ID"=>$arResult["PROPERTIES"]["CLINIC"]["VALUE"]);
$res = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL", "IBLOCK_ID", "ID", "PROPERTY_CITY"));
$arResult['CLINIC_COUNT'] = 0;
while($arres = $res->GetNext())
{
	$arResult["CLINIC"][] = '<a class="red-color" href="'.$arres["DETAIL_PAGE_URL"].'">'.$arres["NAME"].'</a>';
	$arResult["CLINIC_ID"] = $arres["ID"];
	$arResult["CITY_ID"] = $arres["PROPERTY_CITY_VALUE"];
	$arResult["CLINIC_NAME"] = $arres["NAME"];
	$arResult['CLINIC_COUNT']++;
}

$arResult["SECTION_LINK"] = '';
$ressect = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
if($arressect = $ressect->GetNext()) $arResult["SECTION_LINK"] = '<a href="'.$arressect["SECTION_PAGE_URL"].'" class="red-color">'.$arressect["NAME"].'</a>';


$arResult["STATISTICS"] = array(
	"REVIEW" => 0,
	"STORY" => 0,
	"QA" => 0,
	"PHOTO" => 0,
	'ARTICLE' => 0
);

$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arResult["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
$arResult["STATISTICS"]["REVIEW"] = $res->SelectedRowsCount();

$arFilter = Array("IBLOCK_ID"=>12, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arResult["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
$arResult["STATISTICS"]["STORY"] = $res->SelectedRowsCount();

$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$arResult["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
$arResult["STATISTICS"]["QA"] = $res->SelectedRowsCount();

$arFilter = Array("IBLOCK_ID"=>13, "ACTIVE"=>"Y", "PROPERTY_DOCTORS"=>$arResult["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
$arResult["STATISTICS"]["PHOTO"] = $res->SelectedRowsCount();

$arFilter = ['IBLOCK_ID' => ARTICLE_IBLOCK_ID, 'ACTIVE' => 'Y', 'PROPERTY_DOCTOR' => $arResult['ID']];
$res = CIBlockElement::GetList([], $arFilter, false, false, ['ID']);
$arResult['STATISTICS']['ARTICLE'] = $res->SelectedRowsCount();

$arResult["STATISTICS"]["SHOW"] = (
	$arResult["STATISTICS"]["REVIEW"]>0 ||
	$arResult["STATISTICS"]["STORY"]>0 ||
	$arResult["STATISTICS"]["QA"]>0 ||
	$arResult["STATISTICS"]["PHOTO"]>0 ||
	$arResult['STATISTICS']['ARTICLE']);

if (is_array($arResult["PROPERTIES"]["SPECIALTY"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SPECIALTY"]["VALUE"]))
{
	$arFilter = array(
		"IBLOCK_ID"=>$arResult["PROPERTIES"]["SPECIALTY"]["LINK_IBLOCK_ID"],
		"ACTIVE"=>"Y",
		"ID" => array_values($arResult["PROPERTIES"]["SPECIALTY"]["VALUE"]),
		);
	$arResult["PROPERTIES"]["SPECIALTY"]["VALUE"] = array();
	$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL"));
	while ($arElement = $rsElement->GetNext())
	{
		$arResult["PROPERTIES"]["SPECIALTY"]["VALUE"][] = $arElement;
	}


}

if (is_array($arResult["PROPERTIES"]["SECTION"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SECTION"]["VALUE"]))
{
	$arFilter = array(
		"IBLOCK_ID"=>$arResult["PROPERTIES"]["SECTION"]["LINK_IBLOCK_ID"],
		"ACTIVE"=>"Y",
		"ID" => array_values($arResult["PROPERTIES"]["SECTION"]["VALUE"]),
		);
	$arResult["PROPERTIES"]["SECTION"]["VALUE"] = array();
	$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL"));
	while ($arElement = $rsElement->GetNext())
	{
		$arResult["PROPERTIES"]["SECTION"]["VALUE"][] = $arElement;
	}


}

if (is_array($arResult["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SERVICES"]["VALUE"]))
{
	$reSortServicesId = array_values($arResult["PROPERTIES"]["SERVICES"]["VALUE"]);
	$arFilter = array(
		"IBLOCK_ID"=>$arResult["PROPERTIES"]["SERVICES"]["LINK_IBLOCK_ID"],
		"ACTIVE"=>"Y",
		"ID" => $reSortServicesId,
		);

	$arTable = array(0,0,0);
	$row = 0;
	$arResult["PROPERTIES"]["SERVICES"]["VALUE"] = array();
	$reSortServicesId = array_flip($reSortServicesId);
	foreach ($reSortServicesId as $key => $value) {
		$reSortServicesId[$key] = '';
	}

	$rsElement = CIBlockElement::GetList(array('NAME'=>'ASC'),$arFilter,false,false,array("IBLOCK_ID","ID","NAME","DETAIL_PAGE_URL"));
	while ($arElement = $rsElement->GetNext())
	{
		$arTable[$row++]++;
		if ($row == 3) $row = 0;
		//$arResult["PROPERTIES"]["SERVICES"]["VALUE"][] = $arElement;
		if (isset($reSortServicesId[(int)$arElement["ID"]])) {
			$reSortServicesId[(int)$arElement["ID"]] = $arElement;
		}
	}

	$arResult["PROPERTIES"]["SERVICES"]["VALUE"] = array_filter($reSortServicesId);
	$arResult["PROPERTIES"]["SERVICES_TABLE"] = $arTable;
}

//echo '<pre>'; print_r($arResult); echo '</pre>';
global $currentClinicId;
$currentClinicId = $arResult["CLINIC_ID"];
?>
