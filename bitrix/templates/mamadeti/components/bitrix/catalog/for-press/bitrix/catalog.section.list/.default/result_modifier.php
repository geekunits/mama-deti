<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$pictureWidth = 260;
$pictureHeight = 175;

foreach($arResult['SECTIONS'] as &$arSection) {
	$section = ForPressSection::factory($arSection);
	$arSection['CLINIC_NAME'] = $section->getClinic()['NAME'];
	$arSection['ELEMENT_CNT'] = $section->getItemsCount();
	$arSection['FILE'] = CFile::GetFileArray($section->getRawField('UF_FILE'));
	$arSection['FILE_NAME'] = $section->getRawField('UF_FILE_NAME');
	$arItems = $section->getItems(3);
	foreach($arItems as &$arItem) {
		$pictureId = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
		if (!empty($pictureId)) {
			$arPicture = CFile::ResizeImageGet($pictureId, 
				array('width'=> $pictureWidth,'height'=> $pictureHeight), 
				BX_RESIZE_IMAGE_EXACT, 
				true);
		} else {
			$arPicture = null;
		}
		$arItem['PICTURE'] = array_change_key_case($arPicture, CASE_UPPER);
		$arItem['PROPERTIES'] = ForPressHelper::getElementProperties($arItem['ID']);
				if (!empty($arItem['PROPERTIES']['PRESS_RELEASE_FILE']['VALUE'])) {
			$arItem['PRESS_RELEASE_FILE'] = CFile::GetFileArray($arItem['PROPERTIES']['PRESS_RELEASE_FILE']['VALUE']);
			$arItem['PRESS_RELEASE_FILE']['DESCRIPTION'] = $arItem['PROPERTIES']['PRESS_RELEASE_FILE']['DESCRIPTION'];
		}
	}
	$arSection['ITEMS'] = $arItems;
}


/*
$arViewModeList = array('LINE', 'TEXT', 'TILE');

if (!array_key_exists('VIEW_MODE', $arParams))
	$arParams['VIEW_MODE'] = 'LINE';
if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
	$arParams['VIEW_MODE'] = 'LINE';

if (!array_key_exists('SHOW_PARENT_NAME', $arParams))
	$arParams['SHOW_PARENT_NAME'] = 'Y';
if ('N' != $arParams['SHOW_PARENT_NAME'])
	$arParams['SHOW_PARENT_NAME'] = 'Y';

if (0 < $arResult['SECTIONS_COUNT'])
{
	$boolPicture = false;
	$boolDescr = false;
	$arSelect = array('ID');
	$arMap = array();
	if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE'])
	{
		$arCurrent = current($arResult['SECTIONS']);
		if (!array_key_exists('PICTURE', $arCurrent))
		{
			$boolPicture = true;
			$arSelect[] = 'PICTURE';
		}
		if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent))
		{
			$boolDescr = true;
			$arSelect[] = 'DESCRIPTION';
			$arSelect[] = 'DESCRIPTION_TYPE';
		}
	}
	if ($boolPicture || $boolDescr)
	{
		foreach ($arResult['SECTIONS'] as $key => $arSection)
		{
			$arMap[$arSection['ID']] = $key;
		}

		$rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
		while ($arSection = $rsSections->GetNext())
		{
			$key = $arMap[$arSection['ID']];
			if ($boolPicture)
			{
				$arSection['PICTURE'] = intval($arSection['PICTURE']);
				$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
				$arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
				$arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
			}
			if ($boolDescr)
			{
				$arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
				$arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
			}
		}
	}
}
?>