<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>
<div class="fopress-column-right">
	<div class="forpress-list">
	<?php foreach($arResult['SECTIONS'] as $arSection): ?>
		<?php if  (empty($arSection['ELEMENT_CNT'])) {
			continue;
		} ?>
		<div class="forpress-list__item">
			<div class="forpress-list__clinic b-header_h1"><?php echo $arSection['CLINIC_NAME']; ?></div>
			<div class="forpress-list__desc">
				<div class="forpress-list__desc-txt"><?php echo $arSection['DESCRIPTION']; ?></div>
				<?php if (!empty($arSection['FILE'])): ?>
					<div>
						<a href="<?php echo $arSection['FILE']['SRC']; ?>" target="_blank" class="forpress-list__download">Скачать заявку</a>
					</div>
				<?php endif; ?>
			</div>
			<div class="b-columns_three clearfix">
				<?php foreach($arSection['ITEMS'] as $arItem): ?>
					<div class="b-column">
						<div class="forpress-list__pic">
							<?php if (!empty($arItem['PICTURE'])): ?>
								<img 
									src="<?php echo $arItem['PICTURE']['SRC']; ?>" 
									alt="<?php echo $arItem['NAME']; ?>"
									title="<?php echo $arItem['NAME']; ?>">
							<?php endif; ?>
						</div>
						<div class="forpress-list__name">
							<?php echo $arItem['NAME']; ?>
						</div>
						<div class="forpress-list__txt">
							<?php echo $arItem['PREVIEW_TEXT']; ?>
						</div>
						<?php if (!empty($arItem['PROPERTIES']['FILE_LINK']['VALUE'])): ?>
							<div>
								<a href="<?php echo $arItem['PROPERTIES']['FILE_LINK']['VALUE']; ?>" class="forpress-list__download" target="_blank">
									Скачать альбом <?php echo $arItem['PROPERTIES']['FILE_LINK']['DESCRIPTION']; ?>
								</a>
							</div>
						<?php endif; ?>
						<?php if (!empty($arItem['PRESS_RELEASE_FILE'])): ?>
							<div>
								<a href="<?php echo $arItem['PRESS_RELEASE_FILE']['SRC']; ?>" target="_blank" class="forpress-list__download">
									Пресс-релиз
								</a>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($arSection['ELEMENT_CNT'] > 3): ?>
				<div class="forpress-list__all"><a href="<?php echo $arSection['SECTION_PAGE_URL']?>" class="b-link_more in-bl">Смотреть все альбомы</a></div>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
	</div>
</div>