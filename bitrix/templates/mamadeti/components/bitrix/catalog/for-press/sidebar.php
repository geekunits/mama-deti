<?php 

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (isset($arResult['VARIABLES']) && !empty($arResult['VARIABLES']['SECTION_CODE'])) {
	$currentSectionCode = $arResult['VARIABLES']['SECTION_CODE'];
} else {
	$currentSectionCode = false;
}
$urlTemplate = $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['section'];

$sections = ForPressHelper::getSections();
?>
<div class="fopress-column-left">
	<div class="fopress-column-left__head">Выберите клинику:</div>
	<div class="fopress-menu">
		<a class="fopress-menu__item<?php if ($currentSectionCode === false): ?> fopress-menu__item--active<?php endif; ?>" href="<?php echo $arResult['FOLDER']; ?>">
			Все клиники
		</a>
		<?php foreach($sections as $section): ?>
			<?php if ($section->getItemsCount() == 0) {
				continue; 
			} ?>
			<a class="fopress-menu__item<?php if ($currentSectionCode == $section->getRawField('CODE')): ?> fopress-menu__item--active<?php endif; ?>" href="<?php echo $section->getUrl($urlTemplate); ?>">
				<?php echo $section->getClinic()['NAME']; ?>
			</a>
		<?php endforeach; ?>
	</div>
</div>
