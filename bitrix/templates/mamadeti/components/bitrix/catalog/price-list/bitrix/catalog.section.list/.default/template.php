<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form action="/price-list/">
<div class="search-panel search-price-list">
<fieldset>
	<div class="search-field">
	<select name="clinic">
		<option value="0">Все клиники</option>
	<?foreach ($arResult["SECTIONS"] as $arClinicSection):?>
	<?if ($arClinicSection["DEPTH_LEVEL"]>1) continue;?>
		<option value="<?=$arClinicSection["CLINIC"]["ID"]?>"<?if ($selClinic == $arClinicSection["CLINIC"]["ID"]):?> selected="selected"<?endif?>><?=$arClinicSection["CLINIC"]["NAME"]?></option>
	<?endforeach?>
	</select>
	</div>
	<br>
	<div class="search-field">

                    <div class="search-sector active">
                        <div class="search-input">
                            <div class="align-left">
                                <ul class="search-list">
                                                                    </ul>
                            </div>
                            <?/*<input type="button" class="button06 bt_search_doctor" value="">*/?>

                            <div class="text03">
                                <input type="text" class="input_search_text" name="q" placeholder="Название услуги" value="">
                            </div>
                        </div>
                        <div id="search_result" class="search-drop" style="display: none; margin: 0px 0px 0px -9999px; z-index: auto;">
                            <div class="drop-frame">
                                <div class="scrollpane">
                                    <ul class="search-listing"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

	<div class="button-holder">
		<span class="button02 block-button"><input type="submit">найти</span>
	</div>
</fieldset>
</div>
</form>
<?
$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
$CURRENT_DEPTH = $TOP_DEPTH;

foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
	if($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"])
		echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH),"<ul>";
	elseif($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"])
		echo "</li>";
	else
	{
		while($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"])
		{
			echo "</li>";
			echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
			$CURRENT_DEPTH--;
		}
		echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</li>";
	}

	echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH);
	?>
<?if ($arSection["DEPTH_LEVEL"] == 1):?>
<li class="priceClinicName" id="<?=$this->GetEditAreaId($arSection['ID']);?>"><p class="clinicName"><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["CLINIC"]["NAME"]?><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;(<?=$arSection["ELEMENT_CNT"]?>)<?endif;?></a></p>
<?else:?>
<li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><p class="priceSectionName<?=$arSection["DEPTH_LEVEL"]?>"><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?><?if($arParams["COUNT_ELEMENTS"]):?>&nbsp;(<?=$arSection["ELEMENT_CNT"]?>)<?endif;?></a></p>
<?endif?>
<?

	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
endforeach;

while($CURRENT_DEPTH > $TOP_DEPTH)
{
	echo "</li>";
	echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
	$CURRENT_DEPTH--;
}
?>
