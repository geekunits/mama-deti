<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<span class="sidebar-topic">Выберите направление</span>
<nav>
	<ul class="menu">
<?foreach ($arResult["SECTIONS"] as $arItem):?>
		<li<?if ($arItem["SELECTED"]):?> class="active"<?endif?>>
			<a href="<?=$arItem["SECTION_PAGE_URL"]?>">
				<span class="title"><?=$arItem["NAME"]?></span>
			</a>
		</li>
<?endforeach?>
	</ul>
</nav>
