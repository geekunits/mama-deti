<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$arDirection = array();

$arParams["SELECTED_ELEMENT"] = $arParams["SELECTED_ELEMENT"];
if ($arParams["SELECTED_ELEMENT"])
{
	$rsElement = CIBlockElement::GetList([], ['CODE' => $arParams["SELECTED_ELEMENT"]]);
	if ($obElement = $rsElement->GetNextElement())
	{
		$arProps = $obElement->GetProperties();
		$arDirection = $arProps["DIRECTION"]["VALUE"];
	}
}

$curPage = $APPLICATION->GetCurPageParam();

foreach ($arResult["SECTIONS"] as $key=>$arItem)
{
	if (in_array($arItem["ID"],$arDirection) || $curPage == $arItem["SECTION_PAGE_URL"])
		$arResult["SECTIONS"][$key]["SELECTED"] = true;
}

?>
