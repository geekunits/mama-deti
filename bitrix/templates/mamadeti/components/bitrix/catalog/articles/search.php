<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arFilerArticle = array();
if(!empty($_REQUEST["tag"])){
	$arFilerArticle["TAGS"] = "%".htmlspecialcharsEx($_REQUEST["tag"])."%";
}

global ${$arParams["FILTER_NAME"]};
$arrFilter = &${$arParams["FILTER_NAME"]};

$tabIndex = 0;

if (isset($_REQUEST["direction"]) && intval($_REQUEST["direction"])>0)
{
	$arrFilter["PROPERTY_DIRECTION"] = intval($_REQUEST["direction"]);
	$tabIndex = 1;
}

$TAGS = trim($_REQUEST["tags"]);
$TEXT = trim($_REQUEST["text"]);
$doctorId = (int) ($_REQUEST['doctor']);

$arTags = explode(',',$TAGS);

if (!empty($TEXT))
{
	CModule::IncludeModule("search");

	$arSearchParams = array(
		"MODULE_ID" => "iblock",
		//"ITEM_ID" => $arAllowItems,
		"PARAM2" => 9, //инфоблок
		"QUERY" => $TEXT,
		"TAGS" => $arTags,
	);

	$obSearch = new CSearch;
	$obSearch->Search($arSearchParams);

	$arrFilter["ID"][] = -1;

	if ($obSearch->errorno == 0) 
		while ($arSearch = $obSearch->GetNext())
		{
			$arrFilter["ID"][] = $arSearch["ITEM_ID"];
		}
}
elseif (!empty($TAGS)) {
	$arLogic = array("LOGIC" => "AND");
	foreach ($arTags as $tag)
	{
		$tag = trim($tag);
		if ($tag)
			$arLogic[] = array("TAGS"=>'%'.$tag.'%');
	}
	$arrFilter[] = $arLogic;
} elseif (!empty($doctorId)) {
	$arrFilter['PROPERTY_DOCTOR'] = $doctorId;
}

?> 

<div class="topic-panel">
	<div class="align-left">
		<span class="name-title">Поиск статьи</span>
	</div>
</div>
<span class="content-title">Введите название статьи или ее содержимое</span>
<div class="search-panel" id="search-articles">
	<form action="<?=$APPLICATION->GetCurPage()?>">
		<fieldset>
			<div class="search-field">
				<div class="search-sector active">
					<div class="search-input">
						<div class="align-left">
							<ul class="search-list">
							<?foreach($arTags as $t){
								if (!empty($t)){
								?>
									<li><span class="title"><?=$t?></span><a class="remove-button" href="#">remove</a></li>
								<?}
							}
							?>
							</ul>							
						</div>
						<input type="button" class="button06 bt_search_article" value="">
						<div class="text03">
							<input type="text" class="input_search_article" placeholder="Что такое эко" value="<?=htmlspecialcharsbx($_REQUEST["text"])?>">
						</div>
					</div>
					<div id="search_result" class="search-drop" style="display:none;">
						<div class="drop-frame">
							<div class="scrollpane">
								<ul class="search-listing"></ul>							
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="button-holder">
				<span class="button02 block-button"><input type="submit" value="">найти</span>
			</div>
		</fieldset>
	</form>
</div>
