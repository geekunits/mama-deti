<?
CModule::IncludeModule("iblock");

$arSelTree = array();

$ElementCode = $arParams["SELECTED_ELEMENT"];
if ($ElementCode)
{
	$rsElement = CIBlockElement::GetList(array(),array("CODE"=>$ElementCode),false,false,array("IBLOCK_ID","ID","IBLOCK_SECTION_ID"));
	if ($arElement = $rsElement->Fetch())
	{
		
		$SECTION_ID = $arElement["IBLOCK_SECTION_ID"];
		while (true)
		{
			if ($SECTION_ID<=0)
				break;
			$rsSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ID"=>$SECTION_ID),false,array("IBLOCK_ID","ID","IBLOCK_SECTION_ID"));
			if ($arSection = $rsSection->Fetch())
			{
				$arSelTree[] = $arSection["ID"];
				$SECTION_ID = intval($arSection["IBLOCK_SECTION_ID"]);
			} else $SECTION_ID = 0;
		}

	}
} elseif ($arParams['SELECTED_SECTION_ID']) {
    $arSelTree = [$arParams['SELECTED_SECTION_ID']];
}

$curPage = $APPLICATION->GetCurPage(false);
foreach ($arResult['SECTIONS'] as $key=>$arSection)
{
	if (in_array($arSection["ID"],$arSelTree) || $arSection["~SECTION_PAGE_URL"] == $curPage)
		$arResult['SECTIONS'][$key]["SELECTED"] = true;
}

$arResult["TAGS_NAME"] = array();
$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("TAGS"));
while($arres = $res->GetNext())
{
	$tags = explode(",",$arres["TAGS"]);
	foreach ($tags as $t){
		$t = trim($t);
		if (array_key_exists($t, $arResult["TAGS_NAME"])) $arResult["TAGS_NAME"][$t]++;
		else $arResult["TAGS_NAME"][$t] = 1;
	}
}
ksort($arResult["TAGS_NAME"]);
?>
