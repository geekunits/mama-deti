<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$clinicId = isset($_GET['clinic']) ? (int) $_GET['clinic'] : 0;

if ($clinicId > 0) {
	$formatter = new ClinicResultFormatter($clinicId);
	$clinicMenu = new ClinicMenu($formatter->format(), [
	    'useAjax' => false,
	    'detectCurrentTab' => false,
	    'currentTabName' => 'articles',
	]);
	$APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());
}

if(isset($arResult['TIMESTAMP_X'])){
    $timestamp = MakeTimeStamp($arResult['TIMESTAMP_X'], CSite::GetDateFormat());
    setLastModified($timestamp);
}