<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<div class="sidebar-frame">
		<span class="sidebar-topic">Выберите подрубрику</span>
		<nav>
			<ul class="menu">
			<?foreach ($arResult['SECTIONS'] as $arSection){?>
				<li<?if($arSection["SELECTED"]):?> class="active"<?endif?>>
					<a href="<?=$arSection["SECTION_PAGE_URL"]?>">
						<span class="title"><?=$arSection["NAME"]?></span>
						<?php if (trim($arSection['DESCRIPTION'])): ?>
						    <span class="text"><?=$arSection["DESCRIPTION"]?>;</span>
						<?php endif; ?>
					</a>
				</li>
			<?}?>
			</ul>
		</nav>
	</div>
