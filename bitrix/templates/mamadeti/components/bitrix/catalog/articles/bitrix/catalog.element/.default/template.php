<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if($arParams['ADD_SECTIONS_CHAIN'] && !empty($arResult['NAME']))
{ 
   $arResult['SECTION']['PATH'][] = array(
   'NAME' => $arResult['NAME'], 
   'PATH' => ' '); 

   $component = $this->__component; 
   $component->arResult = $arResult; 
}
?>
<div class="content-block-margin">
	<div class="content-block">
		<div class="content-col">
			<span class="name-title"><?=$arResult["NAME"]?></span>
			<div class="sector-content article">
				<div class="text-block">
					<p>
						<?=$arResult["PREVIEW_TEXT"]?>
					</p>
				</div>
				<?=$arResult["DETAIL_TEXT"]?>

				<?if(is_array($arResult["TAGS_NAME"]) && !empty($arResult["TAGS_NAME"])):?>
				<div class="tags-holder">
					<span class="tag-title">Теги:</span>
					<ul class="tag-list">
					<?
					$count = count($arResult["TAGS_NAME"]);
					foreach($arResult["TAGS_NAME"] as $key=>$tag){?>
						<li><a href="/article/?tag=<?=$tag?>"><?=$tag?><?if($key<$count-1) echo ",";?></a></li>
					<?}?>
					</ul>
				</div>
				<?endif;?>

				<?if(!empty($arResult["DOCTORS"])):?>
					<?php foreach ($arResult['DOCTORS'] as $doctor): ?>
						<div class="block-holder">
							<div class="author-block">
								<div class="author-image">
									<img src="<?=MakeImage($doctor["PREVIEW_PICTURE"], array("z"=>1,"w"=>42,"h"=>42))?>" alt="<?=$doctor["NAME"]?>">
								</div>
								<div class="block-holder">
									<span class="title-author"><?=$doctor["NAME"]?></span>
									<div class="post-row">
										<?php if ($spec = $doctor['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE']): ?>
											<?php echo strip_tags(is_array($spec) ? implode(', ', $spec) : $spec); ?>
											<br/>
										<?php endif; ?>

										<?php if ($doctor["PROPERTIES"]['POST']['VALUE'][0]): ?>
					                        <?foreach ($doctor["PROPERTIES"]["POST"]["VALUE"] as $key => $val) {?>
					                            <span>
					                                <?=$val?>
					                             </span>
												<br/>
					                        <?}?>
										<?php endif; ?>

										<?php if ($clinic = $doctor['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']): ?>
											<span class="red-color">
												<?php echo is_array($clinic) ? implode(', ', $clinic) : $clinic; ?>
											</span>
										<?php endif; ?>
									</div>
								</div>
							</div>
							<div class="block-holder">
								<ul class="button-listing align-right">
									<li>
										<a href="<?=$doctor["DETAIL_PAGE_URL"]?>" class="button02 block-button">Узнать больше о докторе</a>
									</li>
									<li>
										<a href="#" data-doctor="<?=$doctor["ID"]?>" class="button01 block-button link_reception_doc">Записаться на прием</a>
									</li>
								</ul>
							</div>
						</div>
						<br/>
					<?php endforeach; ?>
				<?endif;?>
				<div class="social-ico">
					<div class="title">Понравился материал? Поделись с друзьями!</div>
					<div class="social-likes">
						<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
						<div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
						<div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
						<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
						<div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
					</div>
				</div>
			</div>

			<?if(!empty($arResult["DOCTORS"])):?>
			<?$GLOBALS["arrFilterDoctor"] = array("!ID"=>$arResult["ID"], "PROPERTY_DOCTOR"=>array_keys($arResult["DOCTORS"]));?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"article-doctor",
				Array(
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"AJAX_MODE" => "N",
					"IBLOCK_TYPE" => "contentsite",
					"IBLOCK_ID" => "9",
					"NEWS_COUNT" => "10",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_ORDER1" => "DESC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "arrFilterDoctor",
					"FIELD_CODE" => array(),
					"PROPERTY_CODE" => array(),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N"
				),
			false
			);?>
			<?endif;?>
		</div>
	</div>
</div>