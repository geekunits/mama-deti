<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
CModule::IncludeModule("iblock");

$arDirection = array();
$arOther = array();
$rsSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1),false,array("ID","NAME","UF_ICON"));
while ($arSection = $rsSection->Fetch())
{
	$arSection["UF_ICON"] = CFile::GetFileArray($arSection["UF_ICON"]);
	$arSection["ITEMS"] = array();
	$arDirection[$arSection["ID"]] = $arSection;
}

foreach ($arResult["ITEMS"] as $arItem)
{
	if (is_array($arItem["PROPERTIES"]["DIRECTION"]["VALUE"]) && !empty($arItem["PROPERTIES"]["DIRECTION"]["VALUE"]))
	{
		foreach ($arItem["PROPERTIES"]["DIRECTION"]["VALUE"] as $value)
			$arDirection[$value]["ITEMS"][] = $arItem;
	} else $arOther[] = $arItem;
}

$arResult["DIRECTION"] = $arDirection;
$arResult["OTHER"] = $arOther;
unset($arDirection);
unset($arOther);

?>