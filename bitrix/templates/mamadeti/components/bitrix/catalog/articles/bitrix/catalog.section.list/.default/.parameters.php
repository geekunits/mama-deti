<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"ELEMENT_ID" => array(
		"PARENT" => "VISUAL",
		"NAME" => "Element ID",
		"TYPE" => "STRING",
	),
	"SELECTED_SECTION_ID" => array(
		"PARENT" => "VISUAL",
		"NAME" => "Section ID",
		"TYPE" => "STRING",
	),
);

?>
