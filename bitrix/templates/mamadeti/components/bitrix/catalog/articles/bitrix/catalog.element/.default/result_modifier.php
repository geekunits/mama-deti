<?
$arResult["DOCTOR"] = array();
$arResult['DOCTORS'] = array();
if(!empty($arResult["PROPERTIES"]["DOCTOR"]["VALUE"])){
	$doctorIds = 
		is_array($arResult["PROPERTIES"]["DOCTOR"]["VALUE"]) 
			? $arResult["PROPERTIES"]["DOCTOR"]["VALUE"] 
			: array($arResult["PROPERTIES"]["DOCTOR"]["VALUE"]);

	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_POST", "PROPERTY_CLINIC");
	$arFilter = Array("IBLOCK_ID"=>3, "ID"=>$doctorIds);

	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while ($doctor = $res->GetNextElement())
	{
		$doctor = array_merge(
			$doctor->GetFields(),
			array('PROPERTIES' => $doctor->getProperties())
		);
		foreach ($doctor['PROPERTIES'] as $propName => $prop) {
			$doctor['DISPLAY_PROPERTIES'][$propName] = CIBlockFormatProperties::GetDisplayValue($doctor, $prop, "news_out");
		}
		//$doctor["PROPERTY_CLINIC_DETAIL_PAGE_URL"] = str_replace("//", "/", $doctor["PROPERTY_CLINIC_DETAIL_PAGE_URL"]);
		$arResult["DOCTORS"][$doctor['ID']] = $doctor;
	}


	/*$arResult["DOCTOR"]["ID"] = $arResult["PROPERTIES"]["DOCTOR"]["VALUE"];
	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_POST", "PROPERTY_CLINIC.NAME", "PROPERTY_CLINIC.DETAIL_PAGE_URL");
	$arFilter = Array("IBLOCK_ID"=>3, "ID"=>$arResult["PROPERTIES"]["DOCTOR"]["VALUE"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
	if($arres = $res->GetNext())
	{
		$arResult["DOCTOR"] = $arres;
	}
	$arResult["DOCTOR"]["PROPERTY_CLINIC_DETAIL_PAGE_URL"] = str_replace("//", "/", $arResult["DOCTOR"]["PROPERTY_CLINIC_DETAIL_PAGE_URL"]);*/
}

$arResult["TAGS_NAME"] = array();
if (!empty($arResult["TAGS"]))
{
	$arResult["TAGS_NAME"] = explode(",", $arResult["TAGS"]);
	foreach ($arResult["TAGS_NAME"] as $key=>$tag){
		$arResult["TAGS_NAME"][$key] = trim($tag);
	}
}

$this->__component->SetResultCacheKeys(array(
    "TIMESTAMP_X"
));