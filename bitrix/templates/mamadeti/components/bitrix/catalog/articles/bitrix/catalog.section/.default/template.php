<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="content-block-margin">
	<div class="content-block">
		<div class="content-col">
			<span class="name-title">Статьи</span>
			<div class="list-section">
				<?foreach ($arResult["DIRECTION"] as $arDirection):?>
					<?if (!empty($arDirection["ITEMS"])):?>
					<h2><?=$arDirection["NAME"]?></h2>
							<ul class="articles-listing">
							<?foreach ($arDirection['ITEMS'] as $arItem){?>
							<?
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
							?>
								<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
									<div class="block-holder">
										<div class="red-onhover add-able">
											<div class="item-visual">
											<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){?>
												<img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z"=>1,"w"=>140,"h"=>100))?>" alt="<?=$arItem["NAME"]?>"><span class="image-mask"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="add-link"></a></span>
											<?}?>
											</div>
											<div class="block-holder">
												<div class="item-link">
													<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
												</div>
												<div class="text-item">
													<p>
														<?=$arItem["PREVIEW_TEXT"]?>
													</p>
												</div>
											</div>
										</div>
									</div>
								</li>
							<?}?>
							</ul>
					<?endif?>
				<?endforeach?>

				<?if (!empty($arResult["OTHER"])):?>
				<h2>Другие направления</h2>
						<ul class="articles-listing">
						<?foreach ($arResult["OTHER"] as $arItem){?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
						?>
							<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
								<div class="block-holder">
									<div class="red-onhover add-able">
										<div class="item-visual">
										<?if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])){?>
											<img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z"=>1,"w"=>140,"h"=>100))?>" alt="<?=$arItem["NAME"]?>"><span class="image-mask"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="add-link"></a></span>
										<?}?>
										</div>
										<div class="block-holder">
											<div class="item-link">
												<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
											</div>
											<div class="text-item">
												<p>
													<?=$arItem["PREVIEW_TEXT"]?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</li>
						<?}?>
						</ul>
				<?endif?>
			</div>
			<?if ($arParams["DISPLAY_BOTTOM_PAGER"]) echo $arResult["NAV_STRING"];?>
		</div>
	</div>
</div>