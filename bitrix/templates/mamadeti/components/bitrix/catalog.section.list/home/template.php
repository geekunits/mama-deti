<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="b-bg_white b-services_main__wrap">
	<div class="align-center b-header_h2">Основные направления услуг</div>
	<div class="b-services_main b-slider_content">
		<ul class="b-services_main__slider b-slider_no js-slider-carousel clearfix" data-countrow="4">
			<?foreach ($arResult['SECTIONS'] as $arSection){?>
				<li class="b-services_main__item">
					<a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="b-services_main__link">
						<span class="b-services_main__name"><span class="b-services_main__name-empty in-bl"></span><span class="in-bl"><?=$arSection["NAME"]?></span></span>
						<span class="b-services_main__pic">
							<?if(!empty($arSection["PICTURE"])){?>
				            	<? $file = CFile::ResizeImageGet($arSection["PICTURE"], array("width"=>238,"height"=>165),BX_RESIZE_IMAGE_EXACT,true); ?>
				                <img src="<?=$file["src"]?>" alt="<?=$arSection["NAME"]?>">
							<?}?>
						</span>
					</a>
				</li>
			<?}?>
		</ul>
	</div>
</div>