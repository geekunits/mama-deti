<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "CLINIC" => array(
        "PARENT" => "DATA_SOURCE",
        "NAME" => 'Массив клиники',
        "TYPE" => "STRING",
        "MULTIPLE" => "N",
    ),
);
