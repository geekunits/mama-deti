<?php
$usedServiceSectionIds = CMamaDetiAPI::getClinicsServiceSectionsID(['ID' => $arParams['CLINIC']['ID']]);
$arResult['SECTIONS'] = array_filter($arResult['SECTIONS'], function ($service) use ($usedServiceSectionIds, $arParams) {
    if (!in_array($service['ID'], $usedServiceSectionIds)) {
        return false;
    }
    if ($service['DEPTH_LEVEL'] == 1 || $service['UF_SHOW_ON_MAIN']) {
        if (is_array($service['UF_NOT_SHOW_CLINICS']) && in_array($arParams['CLINIC']['ID'], $service['UF_NOT_SHOW_CLINICS'])) {
            return false;
        }

        return true;
    }

    return false;
});

$sd = ServiceDescription::getInstance();

$arResult['SECTIONS'] = array_map(function ($service) use ($sd, $arParams) {
    $description = $sd->getDescriptionForSection($service['ID'], $arParams['CLINIC']['ID']);
    if ($description) {
        $service['DESCRIPTION'] = $description;
    }

    if (!$description && $service['UF_MENU_DESCRIPTION']) {
        $service['DESCRIPTION'] = strip_tags($service['UF_MENU_DESCRIPTION']);
    } elseif ($service['DESCRIPTION']) {
        $service['DESCRIPTION'] = trim(substr(strip_tags($service['DESCRIPTION']), 0, 150)).'...';
    }

    return $service;
}, $arResult['SECTIONS']);

$arResult['SECTIONS'] = array_values($arResult['SECTIONS']);
