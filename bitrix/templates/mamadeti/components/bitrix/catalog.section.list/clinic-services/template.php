<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php if ($arResult['SECTIONS']): ?>
    <div class="b-services_main__ufa">
        <div class="align-center b-header_h2">Услуги <?php echo $arParams['CLINIC']['DISPLAY_PROPERTIES']['NAME_GENITIVE']['DISPLAY_VALUE'] ?: $clinic['NAME']; ?></div>

        <div class="b-news js-services">
            <?php
                $isHidden = false;
            ?>
            <div class="b-news_item clearfix">
                <?php foreach ($arResult['SECTIONS'] as $index => $arSection): ?>
                    <?php
                        if (!empty($arSection["PICTURE"])) {
                            $image = CFile::ResizeImageGet($arSection["PICTURE"], array("width"=>325,"height"=>155), BX_RESIZE_IMAGE_EXACT);
                            $image = $image['src'];
                        } else {
                            $image = null;
                        }

                        $closeGroup = !(($index + 1) % 3);
                    ?>

                    <a class="b-news_col" href="<?=$arSection["SECTION_PAGE_URL"].'clinic/'.$arParams['CLINIC']['CODE'].'/'?>">
                        <span class="b-news_pic">
                            <?php if ($image): ?>
                                <img alt="<?php echo htmlspecialchars($arSection['NAME']); ?>" src="<?php echo $image; ?>">
                            <?php endif; ?>
                        </span>
                        <span class="b-news_name"><?=$arSection["NAME"]?></span>

                        <span class="b-news_txt"><?php echo $arSection['DESCRIPTION']; ?></span>
                    </a>

                    <?php if ($closeGroup): ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($index === 8 && count($arResult['SECTIONS']) > 9): ?>
                        <?php
                            $isHidden = true;
                        ?>
                        <div class="align-center">
                            <a href="#" class="b-link_more in-bl js-show-services">Показать еще</a>
                        </div>
                        <div class="js-hidden-services" style="display: none;">
                    <?php endif; ?>

                    <?php if ($closeGroup): ?>
                        <div class="b-news_item clearfix">
                    <?php endif; ?>
                <?php endforeach; ?>

                <?php if ($isHidden): ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
