<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '<div class="b-breadcrumbs" data-ajax-zone="breadcrumbs">';

for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> ""&&$index<(count($arResult)-1))
		$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a><span class="b-breadcrumbs_gt">&gt;</span>';
	else
		$strReturn .= '<span class="b-breadcrumbs_current">'.$title.'</span>';
}

$strReturn .= '</div>';
return $strReturn;
?>
