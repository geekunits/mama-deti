<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global ${$arParams["FILTER_NAME"]};
$arrFilter = &${$arParams["FILTER_NAME"]};
if(!is_array($arrFilter))
    $arrFilter = array();
$arrFilter["!DETAIL_TEXT"] = false;

if(!empty($_REQUEST["doctor"])){
    $_REQUEST["doctor"] = (int) $_REQUEST["doctor"];
    $GLOBALS["arrFilter"] = array("PROPERTY_DOCTOR"=>$_REQUEST["doctor"]);
}

$arSelDirection = array();
$selClinic = intval($_REQUEST["clinic"]);
if ($selClinic<=0)
    $selClinic = 0;

$arDirectionID = array();
$arDirection = array();
$arClinic = array();

if (isset($_REQUEST["direction"]) && is_array($_REQUEST["direction"]))
{
    array_walk($_REQUEST["direction"], function($item, $key) use (&$arSelDirection)
    {
        $item = intval($item);
        if ($item>0 && !in_array($item,$arSelDirection))
            $arSelDirection[] = $item;
    });

}

/*Тематики*/
$arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y","!DETAIL_TEXT"=>false,"!PROPERTY_DIRECTION"=>false);
if ($selClinic>0)
    $arFilter["PROPERTY_CLINIC"] = $selClinic;

$rsElement = CIBlockElement::GetList(array(),$arFilter,array("PROPERTY_DIRECTION"));
while ($arElement = $rsElement->Fetch())
{
    $arDirectionID[] = $arElement["PROPERTY_DIRECTION_VALUE"];
}

if (!empty($arDirectionID))
{
    $rsSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ID"=>$arDirectionID),false,array("ID","NAME"));
    while ($arSection = $rsSection->GetNext())
        $arDirection[$arSection["ID"]] = $arSection["NAME"];
}

/*Клиники*/
$arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y","!DETAIL_TEXT"=>false,"!PROPERTY_CLINIC"=>false);
if (!empty($arSelDirection))
    $arFilter["PROPERTY_DIRECTION"] = $arSelDirection;

$rsElement = CIBlockElement::GetList(array("PROPERTY_CLINIC.NAME"=>"ASC"),$arFilter,array("PROPERTY_CLINIC"));
while ($arElement = $rsElement->GetNext())
{
    $arClinic[$arElement["PROPERTY_CLINIC_VALUE"]] = $arElement["PROPERTY_CLINIC_NAME"];
}

if (!empty($arSelDirection))
    $arrFilter["PROPERTY_DIRECTION"] = $arSelDirection;
if ($selClinic>0)
    $arrFilter["PROPERTY_CLINIC"] = $selClinic;
?>


<form action="<?=$APPLICATION->GetCurPage();?>" method="GET" name="QA_FORM" id="QA_FORM" enctype="multipart/form-data">
    <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">

    <div class="btn-wrapp" style="width:200px;float:right;"><a href="#" class="button05 block-button link_ask_direction">Задать вопрос</a></div>
    <div style="clear:both;"></div>

    <div id="qa_form_content" class="faq">
        <? if($_REQUEST["is_ajax_post"] == "Y") $APPLICATION->RestartBuffer();?>
        <div class="faq__margin">
            <div class="faq__filter">
                <div class="in-bl faq__filter-head">Фильтр по параметрам:</div>
                <div class="in-bl">
                    <div class="in-bl faq__filter-label">Выберите клинику</div>
                    <select name="clinic">
                        <option value="-1">Все</option>
                        <?foreach ($arClinic as $key=>$val):?>
                            <option value="<?=$key?>"<?if ($key == $selClinic):?> selected<?endif?>><?=$val?></option>
                        <?endforeach?>
                    </select>
                </div>

                <?if (!empty($arDirection)):?>
                    <div class="in-bl">
                        <div class="in-bl faq__filter-label">Тематики вопросов</div>
                    </div>
                    <div class="button05 faq__filter-switcher js-faq-switch">Выбрать из списка<div class="faq__filter-switcher__arr in-bl"></div></div>
                <?endif?>
            </div>
            <?if (!empty($arDirection)):?>
                <?
                $cellsPerCol = ceil(count($arDirection) / 3);
                $cell=0;
                ?>
                <div class="faq-sections js-faq-sections">
                    <?foreach ($arDirection as $key=>$value):?>
                        <?if ($cell%$cellsPerCol==0):?>
                            <ul class="in-bl">
                        <?endif?>
                        <li><label for=""><input type="checkbox" name="direction[]" value="<?=$key?>"<?if (in_array($key,$arSelDirection)):?> checked<?endif?>>&nbsp;<?=$value?></label></li>
                        <?$cell++?>
                        <?if ($cell%$cellsPerCol==0):?>
                            </ul>
                        <?endif?>
                    <?endforeach?>
                    <?if ($cell%$cellsPerCol!=0):?>
                        </ul>
                    <?endif?>
                </div>
                <script>
                    $(function() {
                        $('body').on('click', '.js-faq-switch', function() {
                            $('.js-faq-sections').slideToggle(300);
                        });
                    });
                </script>
            <?endif?>
        </div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "",
            Array(
                "IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
                "IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
                "NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
                "SORT_BY1"	=>	$arParams["SORT_BY1"],
                "SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
                "SORT_BY2"	=>	$arParams["SORT_BY2"],
                "SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
                "FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
                "PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
                "DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                "SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                "IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                "DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
                "SET_TITLE"	=>	$arParams["SET_TITLE"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                "CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
                "CACHE_TIME"	=>	$arParams["CACHE_TIME"],
                "CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
                "PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
                "PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
                "DISPLAY_NAME"	=>	"Y",
                "DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
                "DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
                "PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
                "ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
                "USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
                "GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
                "FILTER_NAME"	=>	$arParams["FILTER_NAME"],
                "HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                "CHECK_DATES"	=>	$arParams["CHECK_DATES"],
            ),
            $component
        );?>
        <? if($_REQUEST["is_ajax_post"] == "Y") die();?>
    </div>
</form>
