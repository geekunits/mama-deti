function submitForm()
{
	var reviewsForm = BX('QA_FORM');

	BX.ajax.submitComponentForm(reviewsForm, 'qa_form_content', true);
	BX.submit(reviewsForm);

	return true;
}

function refreshForm()
{
	$('form#QA_FORM input[type="checkbox"], form#QA_FORM select').change(function()
	{
		$('div.qa-detail-info').slideUp();
		$('a.qa-detail-info-show-button').show();
		submitForm();
	}
	);
}

BX.addCustomEvent("onAjaxSuccess", refreshForm);

$(function(){
	$('a.qa-detail-info-show-button').click(function(e)
	{
		e.preventDefault();
		$(this).hide();
		$('div.qa-detail-info').slideDown();
	});

	refreshForm();
});
