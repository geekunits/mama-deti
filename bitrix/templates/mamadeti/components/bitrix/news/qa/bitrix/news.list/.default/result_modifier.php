<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arDoctorID = array();
$arDirectionID = array();
foreach($arResult["ITEMS"] as $arItem)
{
	if (intval($arItem["PROPERTIES"]["DOCTOR"]["VALUE"]) && !in_array($arItem["PROPERTIES"]["DOCTOR"]["VALUE"],$arDoctorID))
		$arDoctorID[] = $arItem["PROPERTIES"]["DOCTOR"]["VALUE"];
	if (intval($arItem["PROPERTIES"]["DIRECTION"]["VALUE"]) && !in_array($arItem["PROPERTIES"]["DIRECTION"]["VALUE"],$arDirectionID))
		$arDirectionID[] = $arItem["PROPERTIES"]["DIRECTION"]["VALUE"];
}

$arResult["DOCTORS"] = array();
$arResult["DIRECTION"] = array();

if (count($arDoctorID)>0)
{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ID"=>$arDoctorID,"ACTIVE"=>"Y"),false,false,array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_BUTTON_QUESTION"));
	while ($arElement = $rsElement->GetNext())
	{
		$arResult["DOCTORS"][$arElement["ID"]] = $arElement;
	}
}

if (count($arDirectionID)>0)
{
	$rsSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ID"=>$arDirectionID,"ACTIVE"=>"Y"),false,array("ID","NAME"));
	while ($arSection = $rsSection->GetNext())
	{
		$arResult["DIRECTION"][$arSection["ID"]] = $arSection;
	}
}

//echo '<pre>'; print_r($arResult["DIRECTION"]); echo '</pre>';

?>