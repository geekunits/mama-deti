<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="qa-list">
    <?
    $prevSecTitle="";
    foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

        if (intval($arItem["PROPERTIES"]["DOCTOR"]["VALUE"]>0) && array_key_exists($arItem["PROPERTIES"]["DOCTOR"]["VALUE"], $arResult["DOCTORS"]))
            $arDoctor = $arResult["DOCTORS"][$arItem["PROPERTIES"]["DOCTOR"]["VALUE"]]; else
            $arDoctor = false;
        if (intval($arItem["PROPERTIES"]["DIRECTION"]["VALUE"]>0) && array_key_exists($arItem["PROPERTIES"]["DIRECTION"]["VALUE"], $arResult["DIRECTION"]))
            $arDirection = $arResult["DIRECTION"][$arItem["PROPERTIES"]["DIRECTION"]["VALUE"]]; else
            $arDirection = false;

        switch ($arParams["SORT_BY1"])
        {
            case 'PROPERTY_DIRECTION.NAME': $curSecTitle = $arDirection["NAME"]; break;
            //case 'PROPERTY_DIRECTION.NAME': $curSecTitle = $arItem["PROPERTY_DIRECTION_NAME"]; break;
            default: $curSecTitle=''; break;
        }

        if ($prevSecTitle != $curSecTitle)
        {
            echo '<li><span class="name-title red-color">'.$curSecTitle."</span></li>";
            $prevSecTitle = $curSecTitle;
        }
        ?>
        <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="qa-title">
				<span class="qa-date"><?echo reset(explode(' ', $arItem["DATE_CREATE"]))?></span>
				<?
				$arTitle = array();
				$arTitle[] = '<span class="qa-bold">' . $arItem["NAME"] . '</span>';
				$age = intval($arItem["PROPERTIES"]["AGE"]["VALUE"]);
				?><?
				if ($age>0)
				{
					$str = $age." ";
					switch ($age%10)
					{
						case 1:
							$str.="год"; break;
						case 2:
						case 3:
						case 4:
							$str.="года"; break;
						default: $str.="лет";
					}
					$arTitle[] = $str;
				}
				if (!empty($arItem["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]))
					$arTitle[] = strip_tags($arItem["DISPLAY_PROPERTIES"]["CITY"]["DISPLAY_VALUE"]);
				?>
				<?='<span class="qa-date">'.implode(', ',$arTitle).'</span>'?>
            </div>
            <?if ($arItem["PROPERTIES"]["THEME"]["VALUE"]):?>
                Тема: <?=$arItem["PROPERTIES"]["THEME"]["VALUE"]?><br/>
            <?endif?>
            <?/*if ($arDirection):?>
Направление: <?=$arDirection["NAME"]?><br/>
<?endif**/?>
            <div class="qa-intro"><?echo $arItem["PREVIEW_TEXT"];?></div>
            <?if (is_array($arDoctor)):?>
                <div class="qa-doctor"><span>На ваш вопрос ответил врач:</span> <a href="<?=$arDoctor["DETAIL_PAGE_URL"]?>"><?=$arDoctor["NAME"]?></a></div>
                <?/*if ($arDoctor["PROPERTY_BUTTON_QUESTION_VALUE"]>0):?>
	<div style="width:200px;float:right;"><a href="#" class="button05 block-button link_ask" data-doctor="<?=$arItem["PROPERTIES"]["DOCTOR"]["VALUE"]?>">Задать вопрос врачу</a></div>
	<?endif*/?>
            <?endif?>
            <a class="qa-view" href="<?echo $arItem["DETAIL_PAGE_URL"]?>">Посмотреть ответ</a>
        </li>
    <?endforeach;?>
</ul>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
   <?=$arResult["NAV_STRING"]?>
<?endif;?>