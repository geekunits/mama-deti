<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$arResult["DOCTOR"] = false;
$arResult["DIRECTION"] = false;

if (intval($arResult["PROPERTIES"]["DOCTOR"]["VALUE"])>0)
{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ID"=>$arResult["PROPERTIES"]["DOCTOR"]["VALUE"],"ACTIVE"=>"Y"),false,false,array("ID","NAME","DETAIL_PAGE_URL","PROPERTY_BUTTON_QUESTION"));
	if ($arElement = $rsElement->GetNext())
		$arResult["DOCTOR"] = $arElement;
}

if (intval($arResult["PROPERTIES"]["DIRECTION"]["VALUE"])>0)
{
	$rsSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ID"=>$arResult["PROPERTIES"]["DIRECTION"]["VALUE"],"ACTIVE"=>"Y"),false,array("ID","NAME","SECTION_PAGE_URL"));
	if ($arSection = $rsSection->GetNext())
		$arResult["DIRECTION"] = $arSection;
}
//echo '<pre>'; print_r($arResult); echo '</pre>';
?>