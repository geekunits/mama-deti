<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="qa-detail">
    <div class="qa-title">
        <span class="qa-date"><?=reset(explode(' ', $arResult["DATE_CREATE"]))?></span>
        <span class="qa-name">
            <?
            $arTitle[] = '<span class="qa-bold">' . $arResult["NAME"] . '</span>';
            $age = intval($arResult["PROPERTIES"]["AGE"]["VALUE"]);
            ?><?
            if ($age>0)
            {
                $str = $age." ";
                switch ($age%10)
                {
                    case 1:
                        $str.="год"; break;
                    case 2:
                    case 3:
                    case 4:
                        $str.="года"; break;
                    default: $str.="лет";
                }
                $arTitle[] = $str;
            }
            if (!empty($arResult["DISPLAY_PROPERTIES"]["CITY"]["VALUE"]))
                $arTitle[] = strip_tags($arResult["DISPLAY_PROPERTIES"]["CITY"]["DISPLAY_VALUE"]);
            ?>
            <?=implode(', ',$arTitle)?>
        </span>
    </div>
    <?if ($arResult["DIRECTION"]):?>
	<div class="qa-theme">Тематика вопроса: <a href="<?php echo $arResult['DIRECTION']['SECTION_PAGE_URL']; ?>"><?=$arResult["DIRECTION"]["NAME"]?></a></div>
    <?endif?>
    <div class="qa-intro"><?=$arResult["PREVIEW_TEXT"]?></div>
    <div class="qa-answer"><?=$arResult["DETAIL_TEXT"]?></div>

    <?if (is_array($arResult["DOCTOR"])):?>
        <div class="qa-doctor">На ваш вопрос ответил врач: <a href="<?php echo $arResult['DOCTOR']['DETAIL_PAGE_URL']; ?>"><?=$arResult["DOCTOR"]["NAME"]?></a></div>
        <?/*if ($arResult["DOCTOR"]["PROPERTY_BUTTON_QUESTION_VALUE"]>0):?>
		<div style="width:200px;float:right;"><a href="#" class="button05 block-button link_ask" data-doctor="<?=$arResult["PROPERTIES"]["DOCTOR"]["VALUE"]?>">Задать вопрос врачу</a></div>
		<?endif*/?>
    <?endif?>

</div>
