<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

$urlParams = array(
	'region' => (int) $_REQUEST['region'],
	'clinic' => (int) $_REQUEST['clinic'],
	'doctor' => (int) $_REQUEST['doctor'],
);
$urlParams = array_filter($urlParams, function($param) { return $param > 0; });
$urlSuffix = $urlParams ? '?'.http_build_query($urlParams) : '';

$arDoctors = array();
$arClinics = array();
$doctorIds = $arResult['PROPERTIES']['DOCTOR']['VALUE'];
$clinicIds = $arResult['PROPERTIES']['CLINIC']['VALUE'];

if (!empty($doctorIds)) {
	$arDoctors = CMamaDetiAPI::GetDoctors(array('ID' => $doctorIds));
	foreach($arDoctors as &$arDoctor) {
		$pictureId = empty($arDoctor['PREVIEW_PICTURE']) ? $arDoctor['DETAIL_PICTURE'] : $arDoctor['PREVIEW_PICTURE'];
		if (!empty($pictureId)) {
			$arPicture = CFile::GetFileArray($pictureId);
			$arDoctor['PICTURE'] = $arPicture;
			$arThumbPicture = CFile::ResizeImageGet(
				$arPicture, 
				array('width' => 110, 'height' => 110), 
				BX_RESIZE_IMAGE_EXACT, 
				true
		    );
		    $arDoctor['THUMB_PICTURE'] = array_change_key_case($arThumbPicture, CASE_UPPER);
		}
	    $arDoctor['SEX'] = CIBlockElement::GetProperty(
    		DOCTOR_IBLOCK_ID, 
    		$arDoctor['ID'], 
    		array('SORT' => 'ASC'), 
    		array('CODE' => 'SEX')
		)->GetNext();
	}

} elseif (!empty($clinicIds)) {
	$arClinics = array();
	$arFilter = array('IBLOCK_ID' => CLINIC_IBLOCK_ID, 'ID' => $clinicIds, 'ACTIVE' => 'Y');
	$dbResult = CIBlockElement::GetList(array('SORT' => 'ASC'), $arFilter);
	while ($arClinic = $dbResult->GetNext()) {
		$arClinics[] = $arClinic;
	}
	foreach($arClinics as &$arClinic) {
		$pictureId = empty($arClinic['PREVIEW_PICTURE']) ? $arClinic['DETAIL_PICTURE'] : $arClinic['PREVIEW_PICTURE'];
		if (empty($pictureId)) {
			continue;
		}
		$arPicture = CFile::GetFileArray($pictureId);
		$arClinic['PICTURE'] = $arPicture;
		$arThumbPicture = CFile::ResizeImageGet(
			$arPicture, 
			array('width' => 110, 'height' => 110), 
			BX_RESIZE_IMAGE_EXACT, 
			true
	    );

	    $arClinic['THUMB_PICTURE'] = array_change_key_case($arThumbPicture, CASE_UPPER);		
	}
}

if (!empty($arDoctors)) {
	$arResult['DOCTORS'] = $arDoctors;
} elseif (!empty($arClinics)) {
	$arResult['CLINICS'] = $arClinics;
}
$date = explode(' ', $arResult['TIMESTAMP_X']);
$arResult['DATE'] = $date[0];
$arResult['BACK_URL'] = '/reviews/' . $urlSuffix;

?>