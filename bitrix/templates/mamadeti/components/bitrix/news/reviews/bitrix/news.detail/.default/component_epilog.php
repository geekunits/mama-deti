<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$APPLICATION->AddChainItem("Отзыв №".$arResult["ID"]);

$resultClinicId = intval($arResult['PROPERTIES']['CLINIC']['VALUE']);
$clinicId = $resultClinicId > 0 && intval($_GET['clinic']) === $resultClinicId ? $resultClinicId : null;

if ($clinicId) {
	$formatter = new ClinicResultFormatter($clinicId);
	$clinicMenu = new ClinicMenu($formatter->format(), [
		'useAjax' => false,
		'currentTabName' => 'reviews',
		'detectCurrentTab' => false,
	]);
	$APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());
}

?>