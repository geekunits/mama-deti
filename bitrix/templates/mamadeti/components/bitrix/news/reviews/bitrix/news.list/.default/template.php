<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php

    if ($_GET['clinic']) {
        $requestClinicId = $_GET['clinic'];
    }
?>
<div class="b-columns_two b-review_reveiws__wrap">
    <?php if (count($arResult['ITEMS']) > 0): ?>
        <?php foreach($arResult['ITEMS'] as $index => $arItem): ?>
            <?php if ($index % 2 === 0): ?>
                <div class="b-review_item clearfix">
            <?php endif; ?>
                <?php
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="b-column">
                    <?php if (!empty($arItem['DOCTORS'])): ?>
                        <div class="b-review_doctor b-doctor_small<?php if ($arItem['DOCTORS_COUNT'] > 1): ?> b-review_doctor--more<?php endif; ?>">
                            <?php if ($arItem['DOCTORS_COUNT'] > 1): ?>
                                <span class="b-review_doctor--count">...<a href="<?php echo $arItem['URL']; ?>">и ещё <?php echo ($arItem['DOCTORS_COUNT'] - 1) . ' ' . plural($arItem['DOCTORS_COUNT'] - 1, array('врач', 'врача', 'врачей')); ?></a></span>
                            <?php endif; ?>
                            <span class="in-bl b-review_head__author"><?php echo $arItem['NAME']; ?></span>
                            <span class="in-bl b-review_head__arr">&rarr;</span>
                            <?php $arDoctor = $arItem['DOCTORS'][0]; ?>
                            <?php //foreach($arItem['DOCTORS'] as $arDoctor): ?>
                                <?php
                                    $detailPageUrl = $arDoctor['DETAIL_PAGE_URL'];
                                    if (!empty($requestClinicId) && isset($arDoctor['CLINICS'][$requestClinicId])) {
                                        $detailPageUrl .= '?clinic=' . intval($requestClinicId);
                                    }
                                ?>
                                <a href="<?php echo $detailPageUrl; ?>" class="b-review_doctor__fio in-bl">
                                    <?php if (!empty($arDoctor['THUMB_PICTURE'])): ?>
                                        <span class="b-doctor_avatar in-bl">
                                            <img src="<?php echo $arDoctor['THUMB_PICTURE']['SRC']; ?>" alt="<?php echo $arDoctor['NAME']; ?>">
                                        </span>
                                    <?php else: ?>
                                        <span class="b-doctor_avatar in-bl <?php echo $arDoctor['SEX']['VALUE_XML_ID'] === 'F' ? 'b-doctor_nophoto__female' : 'b-doctor_nophoto__male'; ?>">
                                        </span>
                                    <?php endif; ?>
                                    <?php echo $arDoctor['NAME']; ?>
                                </a>
                            <?php //endforeach; ?>
                        </div>
                    <?php elseif (!empty($arItem['CLINICS'])): ?>
                        <div class="b-review_doctor b-doctor_small<?php if ($arItem['CLINICS_COUNT'] > 1): ?> b-review_doctor--more<?php endif; ?>">
                            <?php if ($arItem['CLINICS_COUNT'] > 1): ?>
                                <div class="b-review_doctor--count">...<a href="<?php echo $arItem['URL']; ?>">и ещё <?php echo ($arItem['CLINICS_COUNT'] - 1) . ' ' . plural($arItem['CLINICS_COUNT'] - 1, array('клиника', 'клиники', 'клиник')); ?></div>
                            <?php endif; ?>
                            <span class="in-bl b-review_head__author"><?php echo $arItem['NAME']; ?></span>
                            <span class="in-bl b-review_head__arr">&rarr;</span>
                            <?php foreach($arItem['CLINICS'] as $arClinic): ?>
                                <a href="<?php echo $arClinic['DETAIL_PAGE_URL']; ?>" class="b-review_doctor__fio in-bl">
                                    <?php if (!empty($arClinic['THUMB_PICTURE'])): ?>
                                        <span class="b-doctor_avatar in-bl">
                                            <img src="<?php echo $arClinic['THUMB_PICTURE']['SRC']; ?>" alt="<?php echo $arClinic['NAME']; ?>">
                                        </span>
                                    <?php endif; ?>
                                    <?php echo $arClinic['NAME']; ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($arItem['SHOW_READ_MORE']): ?>
                        <a class="b-review_txt" href="<?php echo $arItem['URL']; ?>">
                    <?php else: ?>
                        <div class="b-review_txt">
                    <?php endif; ?>
                        <?php
                            $dt =  DateTime::createFromFormat ('Y.m.d', $arItem['CREATED_DATE']);
                        ?>
                        <span class="b-review_date"><?php echo ($arItem['ACTIVE_FROM'] ? FormatDate('d.m.Y', MakeTimeStamp($arItem['ACTIVE_FROM'])) : $dt->format('d.m.Y')); ?></span>
                        <?php echo $arItem['SHORT_TEXT']; ?>
                        <?php if ($arItem['SHOW_READ_MORE']): ?>
                            <span class="b-review_more">Читать отзыв полностью</span>
                        <?php endif; ?>
                    <?php if ($arItem['SHOW_READ_MORE']): ?>
                        </a>
                    <?php else: ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php if ($index % 2 === 1): ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if ($index % 2 === 0): ?>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <div>Извините, отзывы отсутствуют.</div>
    <?php endif; ?>
</div>
<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <br /><?=$arResult["NAV_STRING"]?>
<?php endif; ?>

<?php /*
<span class="name-title red-color" style="display:inline-block;">Отзывы</span>
<?if(empty($arResult['ITEMS'])):?>
    <p style="text-align:center;"><img  class="link_review" style="cursor:pointer;"src="<?=SITE_TEMPLATE_PATH.'/images/review_placeholder.jpg';?>"/></p>
<?else:?>
<div class="list-holder">
    <ul class="review-listing">
    <?foreach($arResult["ITEMS"] as $arItem):?>
    <?php
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="review-block">
<?if (is_array($arItem["DOCTOR"]["THUMB_PICTURE"])):?>
        <div class="review-image">
            <img src="<?=$arItem["DOCTOR"]["THUMB_PICTURE"]["SRC"]?>" alt="<?=$arItem["DOCTOR"]["NAME"]?>" title="<?=$arItem["DOCTOR"]["NAME"]?>">
        </div>
<?elseif (is_array($arItem["CLINIC"]["THUMB_PICTURE"])):?>
        <div class="review-image">
            <img src="<?=$arItem["CLINIC"]["THUMB_PICTURE"]["SRC"]?>" alt="<?=$arItem["CLINIC"]["NAME"]?>" title="<?=$arItem["CLINIC"]["NAME"]?>">
        </div>
<?else:?>
        <div class="review-image">
        </div>
<?endif?>
            <div class="block-holder">
                <span class="author">
                    <?if ($arItem['DISPLAY_PROPERTIES']["DOCTOR"]['DISPLAY_VALUE']):?>
                        <span><?php echo is_array($arItem['DISPLAY_PROPERTIES']["DOCTOR"]['DISPLAY_VALUE']) ? implode(', ', $arItem['DISPLAY_PROPERTIES']["DOCTOR"]['DISPLAY_VALUE']) : $arItem['DISPLAY_PROPERTIES']["DOCTOR"]['DISPLAY_VALUE']; ?></span>
                    <?elseif (is_array($arItem["CLINIC"])):?>
                                <span><a href="<?=$arItem["CLINIC"]["DETAIL_PAGE_URL"]?>"><?=$arItem["CLINIC"]["NAME"]?></a></span>
                    <?endif?>
                </span>
                <?$date = split(' ',$arItem['TIMESTAMP_X']);?>
                <span class="article-date"><?=$date[0];?></span>
                <div class="article-text">
                    <p>
                        <?=$arItem["PREVIEW_TEXT"]?>
                        <?if(strlen($arItem["PREVIEW_TEXT"]) > 300):?>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"].$urlSuffix?>" class="red-color">Читать отзыв полностью</a>
                        <?endif;?>

                    </p>
                </div>
                <div class="info-doctor">
                    <span>Автор отзыва: <?php echo $arItem['NAME']; ?></span>
                </div>
            </div>
        </div>
    </li>
    <?endforeach;?>
    </ul>
</div>
<?endif;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
