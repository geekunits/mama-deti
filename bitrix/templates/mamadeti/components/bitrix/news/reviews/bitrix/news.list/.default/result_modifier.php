<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php

    $textMaxLength = 300;
    $textSuffix = '...';

    $urlParams = array(
        'region' => (int) $_REQUEST['region'],
        'clinic' => (int) $_REQUEST['clinic'],
        'doctor' => (int) $_REQUEST['doctor'],
    );
    $urlParams = array_filter($urlParams, function ($param) { return $param > 0; });
    $urlSuffix = $urlParams ? '?'.http_build_query($urlParams) : '';

    foreach ($arResult["ITEMS"] as $index => $arItem) {

        $arDoctors = array();
        $arClinics = array();
        $doctorIds = $arItem['PROPERTIES']['DOCTOR']['VALUE'];
        $clinicIds = $arItem['PROPERTIES']['CLINIC']['VALUE'];
        if (!empty($doctorIds)) {
            $arDoctors = CMamaDetiAPI::GetDoctors(array('ID' => $doctorIds));
            foreach ($arDoctors as &$arDoctor) {
                $pictureId = empty($arDoctor['PREVIEW_PICTURE']) ? $arDoctor['DETAIL_PICTURE'] : $arDoctor['PREVIEW_PICTURE'];
                if (!empty($pictureId)) {
                    $arPicture = CFile::GetFileArray($pictureId);
                    $arDoctor['PICTURE'] = $arPicture;
                    $arThumbPicture = CFile::ResizeImageGet(
                        $arPicture,
                        array('width' => 110, 'height' => 110),
                        BX_RESIZE_IMAGE_EXACT,
                        true
                    );
                    $arDoctor['THUMB_PICTURE'] = array_change_key_case($arThumbPicture, CASE_UPPER);
                }

                $arDoctor['SEX'] = CIBlockElement::GetProperty(
                    DOCTOR_IBLOCK_ID,
                    $arDoctor['ID'],
                    array('SORT' => 'ASC'),
                    array('CODE' => 'SEX')
                )->GetNext();
                $arDoctor['CLINICS'] = CMamaDetiAPI::getDoctorsClinics(array('ID' => $arDoctor['ID']));
            }

        } elseif (!empty($clinicIds)) {
            $arClinics = array();
            $arFilter = array('IBLOCK_ID' => CLINIC_IBLOCK_ID, 'ID' => $clinicIds, 'ACTIVE' => 'Y');
            $dbResult = CIBlockElement::GetList(array('SORT' => 'ASC'), $arFilter);
            while ($arClinic = $dbResult->GetNext()) {
                $arClinics[] = $arClinic;
            }
            foreach ($arClinics as $clinicIndex => $arClinic) {
                $pictureId = empty($arClinic['PREVIEW_PICTURE']) ? $arClinic['DETAIL_PICTURE'] : $arClinic['PREVIEW_PICTURE'];
                if (empty($pictureId)) {
                    continue;
                }
                $arPicture = CFile::GetFileArray($pictureId);
                $arClinic['PICTURE'] = $arPicture;
                $arThumbPicture = CFile::ResizeImageGet(
                    $arPicture,
                    array('width' => 110, 'height' => 110),
                    BX_RESIZE_IMAGE_EXACT,
                    true
                );

                $arClinic['THUMB_PICTURE'] = array_change_key_case($arThumbPicture, CASE_UPPER);

                $arClinics[$clinicIndex] = $arClinic;
            }
        }

        if (!empty($arDoctors)) {
            $arItem['DOCTORS'] = $arDoctors;
            $arItem['DOCTORS_COUNT'] = count($arDoctors);
        } elseif (!empty($arClinics)) {
            $arItem['CLINICS'] = $arClinics;
            $arItem['CLINICS_COUNT'] = count($arClinics);
        }
        $shortText = trim(strip_tags($arItem['PREVIEW_TEXT']));
        if (mb_strlen($shortText) > $textMaxLength) {
            $shortText = mb_substr($shortText, 0, $textMaxLength - mb_strlen($textSuffix)) . $textSuffix;
            $arItem['SHOW_READ_MORE'] = true;
        } else {
            $arItem['SHOW_READ_MORE'] = false;
        }
        $arItem['SHORT_TEXT'] = $shortText;
        $date = split(' ',$arItem['TIMESTAMP_X']);
        $arItem['DATE'] = $date[0];
        $arItem['URL'] = $arItem['DETAIL_PAGE_URL'] . $urlSuffix;

        $arResult['ITEMS'][$index] = $arItem;
    }
?>
