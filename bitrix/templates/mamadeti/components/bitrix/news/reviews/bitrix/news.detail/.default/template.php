<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<span class="b-review_doctor b-doctor_small">
    <span class="in-bl b-review_head__author"><?php echo $arResult['NAME']; ?></span>
    <?php if (!empty($arResult['DOCTORS'])): ?>
        <span class="in-bl b-review_head__arr">&rarr;</span>
        <?php foreach($arResult['DOCTORS'] as $arDoctor): ?>
            <a class="b-review_doctor__fio in-bl" href="<?php echo $arDoctor['DETAIL_PAGE_URL']; ?>">
                <?php if (!empty($arDoctor['THUMB_PICTURE'])): ?>
                    <span class="b-doctor_avatar in-bl">
                        <img src="<?php echo $arDoctor['THUMB_PICTURE']['SRC']; ?>" alt="<?php echo $arDoctor['NAME']; ?>">
                    </span>
                <?php else: ?>
                    <span class="b-doctor_avatar in-bl <?php echo $arDoctor['SEX']['VALUE_XML_ID'] === 'F' ? 'b-doctor_nophoto__female' : 'b-doctor_nophoto__male'; ?>">
                    </span>
                <?php endif; ?>
                <?php echo $arDoctor['NAME']; ?>
            </a>
        <?php endforeach; ?>
    <?php elseif (!empty($arResult['CLINICS'])): ?>
        <span class="in-bl b-review_head__arr">&rarr;</span>
        <?php foreach($arResult['CLINICS'] as $arClinic): ?>
            <a class="b-review_doctor__fio in-bl" href="<?php echo $arClinic['DETAIL_PAGE_URL']; ?>">
                <?php if (!empty($arClinic['THUMB_PICTURE'])): ?>
                    <span class="b-doctor_avatar in-bl">
                        <img src="<?php echo $arClinic['THUMB_PICTURE']['SRC']; ?>" alt="<?php echo $arClinic['NAME']; ?>">
                    </span>
                <?php endif; ?>
                <?php echo $arClinic['NAME']; ?>
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</span>
<div class="b-review_txt">
    <?php if (!empty($arResult['DATE'])): ?>
        <span class="b-review_date"><?php echo ($arResult['ACTIVE_FROM'] ? FormatDate('d.m.Y', MakeTimeStamp($arResult['ACTIVE_FROM'])) : $arResult['DATE']); ?></span>
    <?php endif; ?>
    <?php echo $arResult['PREVIEW_TEXT']; ?>
</div>
<a href="<?php echo $arResult['BACK_URL']; ?>" class="b-review_more">Вернуться назад</a>