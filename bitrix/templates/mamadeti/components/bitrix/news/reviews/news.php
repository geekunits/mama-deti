<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

CModule::IncludeModule("iblock");

$selRegion = isset($_REQUEST["region"]) ? intval($_REQUEST["region"]) : $GLOBALS["CURRENT_CITY"]["ID"];
$selClinic = isset($_REQUEST["clinic"]) ? intval($_REQUEST["clinic"]) : -1;
$selDoctor  = isset($_REQUEST["doctor"]) ? intval($_REQUEST["doctor"]) : -1;

$arRegion = array();
$arClinic = array();
$arDoctor = array();

$rsElement = CIBlockElement::GetList(array("NAME"=>"ASC"),array("IBLOCK_ID"=>CITY_IBLOCK_ID,"ACTIVE"=>"Y"));
while ($arElement = $rsElement->Fetch()) {
	$arRegion[$arElement["ID"]] = $arElement['NAME'];
}


$arAllDoctor = CMamaDetiAPI::getReviewDoctorFilter();


$rsElement = CIBlockElement::GetList(
	array(
		"NAME"=>"ASC",
	),
	array(
		"IBLOCK_ID"=>CLINIC_IBLOCK_ID,
		"ACTIVE"=>"Y",
		"PROPERTY_CITY"=>$selRegion
	)
);
while ($arElement = $rsElement->Fetch()) {
	$arClinic[$arElement["ID"]] = $arElement["NAME"];
}

if (!array_key_exists($selClinic, $arClinic))
	$selClinic = -1;

if ($selClinic>0)
{
	$arDoctor = $arAllDoctor[$selClinic];
	if (!isset($arDoctor[$selDoctor])) { $selDoctor = -1; }

}
else {
	$selDoctor = -1;
}

$GLOBALS["arrFilter"] = [
	['LOGIC' => 'OR', ['PROPERTY_DOCTOR.ACTIVE' => 'Y'], ['PROPERTY_DOCTOR' => false]],
];

$clinicId = $selClinic > 0 && intval($_GET['clinic']) === $selClinic ? $selClinic : null;
?>

<div style="width:200px;float:right;display:inline-block;"><a href="#" class="button05 block-button link_review">Оставить отзыв</a></div>

<form action="<?=$APPLICATION->GetCurPage();?>" method="GET" name="REVIEWS_FORM" id="REVIEWS_FORM" enctype="multipart/form-data">
	<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
	<div id="reviews_form_content">

	<? if($_REQUEST["is_ajax_post"] == "Y") $APPLICATION->RestartBuffer();?>


		<div class="reviews_filter new-reviews">
			Выберите регион:
			<select name="region">
			<?foreach ($arRegion as $id=>$name):?>
				<option value="<?=$id?>"<?if ($id==$selRegion):?> selected<?endif?>><?=$name?></option>
			<?endforeach?>
			</select>
			Выберите клинику:
				<select name="clinic">
					<option value="-1">Все</option>
				<?foreach ($arClinic as $id=>$name):?>
					<option value="<?=$id?>"<?if ($id==$selClinic):?> selected<?endif?>><?=$name?></option>
				<?endforeach?>
				</select>
			<?if ($selClinic>0):?>
				<br/><br/>Выберите врача:
				<select name="doctor">
					<option value="-1">Все</option>
				<?foreach ($arDoctor as $id=>$name):?>
					<option value="<?=$id?>"<?if ($id==$selDoctor):?> selected<?endif?>><?=$name?></option>
				<?endforeach?>
				</select>
			<?endif?>
		</div>
		<br><br>
		<?php if ($clinicId): ?>
			<?php
				$formatter = new ClinicResultFormatter($clinicId);
				$clinicMenu = new ClinicMenu($formatter->format(), [
					'useAjax' => false,
					'currentTabName' => 'reviews',
					'detectCurrentTab' => false,
				]);
				$APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());
			?>
		<?php endif; ?>
		<?
		if($selDoctor>0) {
			$GLOBALS["arrFilter"]["PROPERTY_DOCTOR"] = $selDoctor;
			//$GLOBALS["arrFilter"]["PROPERTY_CLINIC"] = $selClinic;
		} elseif ($selClinic>0)
			$GLOBALS["arrFilter"]["PROPERTY_CLINIC"] = $selClinic;
		else
			$GLOBALS["arrFilter"]["PROPERTY_CLINIC"] = $arClinic ? array_keys($arClinic) : -1;
		?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"",
			Array(
				"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
				"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
				"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
				"SORT_BY1"	=>	$arParams["SORT_BY1"],
				"SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
				"SORT_BY2"	=>	$arParams["SORT_BY2"],
				"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
				"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
				"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
				"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
				"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
				"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
				"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
				"SET_TITLE"	=>	$arParams["SET_TITLE"],
				"SET_STATUS_404" => $arParams["SET_STATUS_404"],
				"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
				"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
				"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
				"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
				"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
				"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
				"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
				"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
				"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
				"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
				"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
				"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
				"DISPLAY_NAME"	=>	"Y",
				"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
				"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
				"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
				"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
				"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
				"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
				"FILTER_NAME"	=>	$arParams["FILTER_NAME"],
				"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
				"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
			),
			$component
		);?>
		<? if($_REQUEST["is_ajax_post"] == "Y") die();?>
	</div>
</form>
