<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

$arParams["RESIZE_DETAIL_METHOD"] = intval($arParams["RESIZE_DETAIL_METHOD"]);

if (is_array($arResult["DETAIL_PICTURE"]))
{
    $arFileTmp = CFile::ResizeImageGet(
                    $arResult["DETAIL_PICTURE"], array("width" => $arParams["RESIZE_DETAIL_WIDTH"], "height" => $arParams["RESIZE_DETAIL_HEIGHT"]), $arParams["RESIZE_DETAIL_METHOD"], true
    );
    $arResult["DETAIL_PICTURE"] = array(
        "SRC" => $arFileTmp["src"],
        "WIDTH" => $arFileTmp["width"],
        "HEIGHT" => $arFileTmp["height"],
    );
}
?>