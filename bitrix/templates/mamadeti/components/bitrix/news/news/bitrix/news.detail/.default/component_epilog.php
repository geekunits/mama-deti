<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
$APPLICATION->AddChainItem($arResult["NAME"], "");

$ogp = OgpManager::getInstance();
$ogp->add('title', $arResult['NAME']);

$arElement = CIBlockElement::GetByID($arResult['ID'])->GetNext();
$ogp->add('url', 'http://'.$_SERVER['HTTP_HOST'].$arElement['DETAIL_PAGE_URL']);
if ($arElement['PREVIEW_TEXT']) {
    $ogp->add('description', $arElement['PREVIEW_TEXT']);
}
if ($arElement['DETAIL_PICTURE']) {
    $picture = CFile::GetPath($arElement['DETAIL_PICTURE']);
    $ogp->add('image', 'http://'.$_SERVER['HTTP_HOST'].$picture);
}
?>
