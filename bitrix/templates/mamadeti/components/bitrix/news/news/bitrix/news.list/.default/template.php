<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>

<div class="articles-gallery">
	<ul class="gallery" style="width:inherit;">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="add-able">
			<div class="article-holder">
				<div class="article-block">
					<div class="article-visual">
						<img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z"=>1,"w"=>242,"h"=>150))?>" alt="<?=$arItem["NAME"]?>">
						<span class="image-mask">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="add-link"></a>
						</span>
					</div><span class="author"><?=$arItem["NAME"]?></span>
<?foreach ($arItem["DISPLAY_PROPERTIES"] as $arProp):?>
	<?if (!empty($arProp["DISPLAY_VALUE"])):?>
		<?if (is_array($arProp["DISPLAY_VALUE"])):/*?>
		<span class="article-date"><?=$arProp["NAME"]?>: <?=implode(', ',$arProp["DISPLAY_VALUE"])?></span>
		<?*/else:?>
		<span class="article-date"><?=$arProp["NAME"]?>: <?=$arProp["DISPLAY_VALUE"]?></span>
		<?endif?>
	<?endif?>
<?endforeach?>
					<span class="article-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
					<div class="article-text">
						<p>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=mb_substr($arItem["PREVIEW_TEXT"],0,180,"UTF-8")?><?if(strlen($arItem["PREVIEW_TEXT"])>180)echo "...";?></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</li>
<?endforeach;?>
</ul>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>