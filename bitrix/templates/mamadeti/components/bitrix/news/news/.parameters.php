<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResizeMethod = array(
	BX_RESIZE_IMAGE_EXACT => "BX_RESIZE_IMAGE_EXACT",
	BX_RESIZE_IMAGE_PROPORTIONAL => "BX_RESIZE_IMAGE_PROPORTIONAL",
	BX_RESIZE_IMAGE_PROPORTIONAL_ALT => "BX_RESIZE_IMAGE_PROPORTIONAL_ALT",
	);

$arTemplateParameters = array(
	"RESIZE_NEWS_METHOD" => array(
		"NAME" => "Метод ресайза для анонса",
		"TYPE" => "LIST",
		"VALUES" => $arResizeMethod,
		"DEFAULT" => BX_RESIZE_IMAGE_PROPORTIONAL,
	),
	"RESIZE_NEWS_WIDTH" => Array(
		"NAME" => "Ширина картинки анонса",
		"TYPE" => "STRING",
		"DEFAULT" => "100",
	),
	"RESIZE_NEWS_HEIGHT" => Array(
		"NAME" => "Высота картинки анонса",
		"TYPE" => "STRING",
		"DEFAULT" => "100",
	),
	"RESIZE_DETAIL_METHOD" => array(
		"NAME" => "Метод ресайза для детальной",
		"TYPE" => "LIST",
		"VALUES" => $arResizeMethod,
		"DEFAULT" => BX_RESIZE_IMAGE_PROPORTIONAL,
	),
	"RESIZE_DETAIL_WIDTH" => Array(
		"NAME" => "Ширина картинки детальной",
		"TYPE" => "STRING",
		"DEFAULT" => "100",
	),
	"RESIZE_DETAIL_HEIGHT" => Array(
		"NAME" => "Высота картинки детальной",
		"TYPE" => "STRING",
		"DEFAULT" => "100",
	),
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"USE_SHARE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_USE_SHARE"),
		"TYPE" => "CHECKBOX",
		"MULTIPLE" => "N",
		"VALUE" => "Y",
		"DEFAULT" =>"N",
		"REFRESH"=> "Y",
	),
);

if ($arCurrentValues["USE_SHARE"] == "Y")
{
	$arTemplateParameters["SHARE_HIDE"] = array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_SHARE_HIDE"),
		"TYPE" => "CHECKBOX",
		"VALUE" => "Y",
		"DEFAULT" => "N",
	);

	$arTemplateParameters["SHARE_TEMPLATE"] = array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_SHARE_TEMPLATE"),
		"DEFAULT" => "",
		"TYPE" => "STRING",
		"MULTIPLE" => "N",
		"COLS" => 25,
		"REFRESH"=> "Y",
	);
	
	if (strlen(trim($arCurrentValues["SHARE_TEMPLATE"])) <= 0)
		$shareComponentTemlate = false;
	else
		$shareComponentTemlate = trim($arCurrentValues["SHARE_TEMPLATE"]);

	include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/components/bitrix/main.share/util.php");

	$arHandlers = __bx_share_get_handlers($shareComponentTemlate);

	$arTemplateParameters["SHARE_HANDLERS"] = array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_SHARE_SYSTEM"),
		"TYPE" => "LIST",
		"MULTIPLE" => "Y",
		"VALUES" => $arHandlers["HANDLERS"],
		"DEFAULT" => $arHandlers["HANDLERS_DEFAULT"],
	);

	$arTemplateParameters["SHARE_SHORTEN_URL_LOGIN"] = array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_LOGIN"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	);
	
	$arTemplateParameters["SHARE_SHORTEN_URL_KEY"] = array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_KEY"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	);
}

?>