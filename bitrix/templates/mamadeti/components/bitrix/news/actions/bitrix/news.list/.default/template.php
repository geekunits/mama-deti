<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
    $dateFormat = 'd.m.Y';
    $timeFormat = 'h:i:s';
    $displayClinic = false;
    if (!empty($arParams['FILTER_NAME'])) {
        $filterName = $arParams['FILTER_NAME'];
        $arFilter = $GLOBALS[$filterName];
        if (count($arFilter['PROPERTY_CLINIC']) > 1) {
            $displayClinic = true;
        }
    }
?>
<?php if ($arResult['ITEMS']): ?>
    <div class="b-actions_list__wrap">
        <?php foreach($arResult["ITEMS"] as $index => $arItem):?>
            <?php
                $index += 1;
                $dateTo = $arItem['DISPLAY_PROPERTIES']['DATE_TO']['VALUE'];
                $dateFrom = $arItem['DISPLAY_PROPERTIES']['DATE_FROM']['VALUE'];
                $dtActiveTo = null;
                $dtActiveFrom = null;

                $dateTo = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateTo);
                $dateFrom = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateFrom);
                $dtNow = new \DateTime('now');
                $dtNow->setTime(0, 0, 0);

                if (
                    !empty($dateTo)
                    && ($dtActiveTo = \DateTime::createFromFormat($dateFormat, $dateTo))
                ) {
                    $dtActiveTo->setTime(23, 59, 59);
                }
                if (!empty($dateFrom)) {
                     $dtActiveFrom = \DateTime::createFromFormat($dateFormat, $dateFrom);
                }
                $daysLeft = false;
                $daysSoon = false;
                if ($dtActiveFrom) {
                    $intervalSoon = ($dtNow < $dtActiveFrom) ? $dtActiveFrom->diff($dtNow) : false;
                    $daysSoon = $intervalSoon ? intval($intervalSoon->format('%a')) : false;
                }
                if ($dtActiveTo) {
                    $dtInterval = $dtActiveTo->diff($dtNow);
                    $daysLeft = intval($dtInterval->format('%a')) + 1;
                }
                $imageData = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array('width'=>325,'height'=>183), BX_RESIZE_IMAGE_EXACT, true);
                if ($displayClinic) {
                    $arRawClinics = CMamaDetiAPI::GetClinics(array('ID' => $arItem['PROPERTIES']['CLINIC']['VALUE']));
                    $arClinics = array();
                    foreach ($arRawClinics as $arRawClinic) {
                        $rawId = $arRawClinic['ID'];
                        $arClinics[$rawId] = $arRawClinic;
                    }
                    // $arClinic = empty($arClinics) ? null : $arClinics[0];
                } else {
                    $arClinics = array();
                    $arClinic = null;
                }
                $dateRange = '';
                if (!empty($dateTo) && !empty($dateFrom)) {
                    if ($dateTo == $dateFrom) {
                        $dateRange = 'Проводится ' . $dateTo;
                    } else {
                        $dateRange = $dateFrom . ' - ' . $dateTo;
                    }
                } elseif (!empty($dateTo)) {
                    $dateRange = 'По ' . $dateTo;
                } elseif (!empty($dateFrom)) {
                    $dateRange = 'С ' . $dateFrom;
                }
            ?>
            <?php if ($index % 2 === 1): ?>
                <div class="b-columns_two b-news_item clearfix">
            <?php endif; ?>
            <div class="b-column">
                 <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="b-news_link">
                        <span class="b-news_pic">
                            <img src="<?php echo $imageData['src']; ?>" alt="<?php echo $arItem['NAME']; ?>" alt="<?php echo $arItem['TITLE']; ?>">
                            <?php if ($daysSoon): ?>
                                <span class="b-clinic_action__days--soon">
                                    до начала акции <strong><?php echo $daysSoon; ?></strong>
                                    <?php echo plural($daysSoon, ['день', 'дня', 'дней']) ;?>
                                </span>
                            <?php elseif ($daysLeft > 1): ?>
                                <span class="b-clinic_action__days">
                                    до окончания акции <strong><?php echo $daysLeft; ?></strong>
                                    <?php echo plural($daysLeft, ['день', 'дня', 'дней']) ;?>
                                </span>
                            <?php elseif ($daysLeft === 1): ?>
                                <span class="b-clinic_action__days">
                                    <strong>Остался последний день</strong>
                                </span>
                            <?php endif; ?>
                        </span>
                        <?php if (!empty($arClinics)): ?>
                            <?php foreach($arClinics as $arClinic): ?>
                                <div class="b-actions_list__clinic"><?php echo trim($arClinic['NAME']); ?></div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if (!empty($dateRange)): ?>
                            <span class="b-news_date"><?php echo $dateRange ?></span>
                        <?php endif; ?>
                        <span class="b-news_name"><?php echo $arItem['NAME']; ?></span>
                        <span class="b-news_txt"><?php echo strip_tags($arItem["PREVIEW_TEXT"]); ?></span>
                    </a>
                </div>
            <?php if ($index % 2 === 0): ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php if ($index % 2 === 1): ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?php echo $arResult["NAV_STRING"]?>
    <?php endif;?>
<?php endif; ?>
