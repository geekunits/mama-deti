<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php

$APPLICATION->AddChainItem($arResult['IBLOCK']['NAME'], $arResult['IBLOCK']['LIST_PAGE_URL']);
$APPLICATION->AddChainItem($arResult['NAME'],'');

$ogp = OgpManager::getInstance();
$ogp->add('title', $arResult['NAME']);



$arElement = CIBlockElement::GetByID($arResult['ID'])->GetNext();
$ogp->add('url', 'http://'.$_SERVER['HTTP_HOST'].$arElement['DETAIL_PAGE_URL']);
if ($arElement['PREVIEW_TEXT']) {
    $ogp->add('description', $arElement['PREVIEW_TEXT']);
}
if ($arElement['DETAIL_PICTURE']) {
    $picture = CFile::GetPath($arElement['DETAIL_PICTURE']);
    $ogp->add('image', 'http://'.$_SERVER['HTTP_HOST'].$picture);
}

$clinicIds = $arResult['PROPERTIES']['CLINIC']['VALUE'];
$clinicId = null;
if (isset($_GET['clinic']) && is_array($clinicIds) && in_array($_GET['clinic'], $clinicIds)) {
    $clinicId = (int) $_GET['clinic'];
}
if (!empty($clinicId)) {
    $formatter = new ClinicResultFormatter($clinicId);
    $clinicMenu = new ClinicMenu($formatter->format(), [
        'useAjax' => false,
        'detectCurrentTab' => false,
        'currentTabName' => 'actions',
    ]);
    $APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());
}
$cm = CityManager::getInstance();
$city = $cm->getCurrentCity();

$APPLICATION->SetPageProperty('title', $arElement['NAME'].' &ndash; акция в Мать и дитя '.$city['NAME']);
