<?php 

$APPLICATION->AddChainItem($arResult["NAME"], "");

$clinicId = (int) (is_array($arResult['PROPERTIES']['CLINIC']['VALUE']) ? reset($arResult['PROPERTIES']['CLINIC']['VALUE']) : $arResult['PROPERTIES']['CLINIC']['VALUE']);

$formatter = new ClinicResultFormatter($clinicId);
$clinic = $formatter->format();

if (!empty($clinic)) {
	$clinicMenu = new ClinicMenu($clinic, [
		'useAjax' => false,
		'currentTabName' => 'photos',
		'detectCurrentTab' => false,
	]);
	$APPLICATION->SetPageProperty('CLINIC_MENU', $clinicMenu->getTitleContent() . $clinicMenu->getContent());
	$GLOBALS['CLINIC'] = $clinic;
	$APPLICATION->SetDirProperty('SHOW_H1','N');
	echo '<br/><br/>';
}

?>