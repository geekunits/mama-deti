<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h3>Фотогалерея: <?=$arResult['NAME']?></h3>
<?if (!empty($arResult["PROPERTIES"]["PHOTO"]["VALUE"])) {?>
<div class="visual-gallery gallery-clinic-detail">
    <a href="#" class="prev">prev</a>
    <a href="#" class="next">next</a>
    <div class="gallery-frame">
        <ul class="gallery">
            <?foreach ($arResult['RES_MOD_PHOTO'] as $key=>$photo) {?>
            <?
                 $arPicture = CFile::ResizeImageGet($photo, array(
                    'width'  => 273,
                    'height' => 230
                ), BX_RESIZE_IMAGE_EXACT, true);
            ?>
            <li>
            	<?
                $pieces = explode("#", $arResult['RES_MOD_DESCR'][$key]);

                if((count($pieces) >= 3)&&(intval($pieces[1]) > 0 )){
                    $desc = $pieces[2];
                }else{
                    $desc = $arResult['RES_MOD_DESCR'][$key];
                }
                ?>

                <a class="fancybox" rel="gallery-<?=$arResult["ID"]?>" href="<?=CFile::GetPath($photo)?>" title="<?=$desc?>">
                    <img src="<?=$arPicture['src']?>" alt="<?=$desc?>">
                </a>
                <?
                    if ($desc) {
                    	echo '<div class="gallery-desc">'.$desc.'</div>';
                    }
                ?>
            </li>
            <?}?>
        </ul>
    </div>
</div>
<?}?>

<?php if ($arResult['DETAIL_TEXT']): ?>
    <?php echo $arResult['DETAIL_TEXT']; ?>
    <br/>
<?php endif; ?>
<div class="social-ico">
    <div class="title">Понравился материал? Поделись с друзьями!</div>
    <div class="social-likes">
        <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
        <div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
        <div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
        <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
        <div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
    </div>
</div>
<br/>