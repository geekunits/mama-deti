<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arParams["DISPLAY_TOP_PAGER"])
    $arResult["NAV_STRING"];

$currentClinicId = !empty($_REQUEST['clinic']) ? $_REQUEST['clinic'] : null;
$cntClinics = array();//array_fill_keys(array_values(array_unique($arParams['CLINIC_IDS'])),array());
foreach ($arResult['ITEMS'] as $index => $arItem) {
    if(is_array($arItem['PROPERTIES']['CLINIC']['VALUE'])){
        foreach ($arItem['PROPERTIES']['CLINIC']['VALUE'] as $c) {
            if($currentClinicId){
                if($c == $currentClinicId){
                    $cntClinics[$c][] = $index;
                }
            } else {
                $cntClinics[$c][] = $index;
            }
        }
    } else {
        if($currentClinicId){
            if($arItem['PROPERTIES']['CLINIC']['VALUE'] == $currentClinicId){
                $cntClinics[$arItem['PROPERTIES']['CLINIC']['VALUE']][] = $index;
            }
        } else {
            $cntClinics[$arItem['PROPERTIES']['CLINIC']['VALUE']][] = $index;
        }
    }
}?>
<div class="list-holder">
    <?foreach ($cntClinics as $clinicId => $galleryIds):?>
        <?if(!empty($galleryIds)):?>
        <?$clinic = CIBlockElement::GetByID($clinicId)->GetNext();?>
        <?php if (!$arParams['_CLINIC']): ?>
            <h2 class="name-title red-color"><a href="<?=$clinic['DETAIL_PAGE_URL'];?>"><?=$clinic['NAME']?></a></h2>
        <?php endif; ?>
        <div class="gallery-sector gallery-clinics">
            <div class="articles-gallery">
                <a href="#" class="prev">prev</a>
                <a href="#" class="next">next</a>
                <div class="gallery-frame">
                    <ul class="photo-list gallery">
                    <?foreach ($galleryIds as $gId): $arItem = $arResult['ITEMS'][$gId];?>
                        <?php 
                            $detailPageUrl = $arItem['DETAIL_PAGE_URL'];
                            if (!empty($arParams['_CLINIC'])) {
                                $detailPageUrl .= '?from=clinic';
                            }
                        ?>
                        <li>
                            <div class="content-block add-able">
                                <div class="item-frame">
                                    <div class="item-section">
                                        <div class="image-item">
                                        <?if(!empty($arItem["PREVIEW_PICTURE"])): $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width"=>370,"height"=>210),BX_RESIZE_IMAGE_PROPORTIONAL,true);?>
                                            <img src="<?=$file['src'];?>" alt="<?=$arItem["NAME"]?>" style="margin-left: <?= 230 / 2 - $file['width'] / 2;?>px">
                                            <span class="image-mask">

                                                <a href="<?=$detailPageUrl;?>" class="add-link"></a>
                                            </span>
                                        <?endif;?>
                                        </div>
                                        <div class="holder-item">
                                            <span class="article-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                                            <div class="item-link">
                                                <a href="<?=$detailPageUrl;?>"><?=$arItem["NAME"]?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
        <?endif;?>
    <?endforeach;?>

    <?/*foreach($arResult["ITEMS"] as $index => $arItem):?>
        <?if($currentClinicId != $arItem['PROPERTIES']['CLINIC']['VALUE']): $clinic = CIBlockElement::GetByID($arItem['PROPERTIES']['CLINIC']['VALUE'])->GetNext();?>
        <?php if ($index): ?></ul></div></div></div><?php endif; ?>
        <h2 class="name-title red-color"><a href="<?=$clinic['DETAIL_PAGE_URL'];?>"><?=$clinic['NAME']?></a></h2>
        <div class="gallery-sector gallery-clinics">
            <div class="articles-gallery">
                <a href="#" class="prev">prev</a>
                <a href="#" class="next">next</a>
                <div class="gallery-frame">
                    <ul class="photo-list gallery">

        <?$currentClinicId = $arItem['PROPERTIES']['CLINIC']['VALUE']; endif;?>
        <li>
            <div class="content-block add-able">
                <div class="item-frame">
                    <div class="item-section">
                        <div class="image-item">
                        <?if(!empty($arItem["PREVIEW_PICTURE"])): $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width"=>370,"height"=>210),BX_RESIZE_IMAGE_PROPORTIONAL,true);?>
                            <img src="<?=$file['src'];?>" alt="<?=$arItem["NAME"]?>" style="margin-left: <?= 230 / 2 - $file['width'] / 2;?>px">
                            <span class="image-mask">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="add-link"></a>
                            </span>
                        <?endif;?>
                        </div>
                        <div class="holder-item">
                            <span class="article-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                            <div class="item-link">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    <?endforeach;?>
</ul></div></div></div><?*/?>
</div>
<?php /* if ($arParams['_CLINIC']): ?>
    <?php
        $cnt = CIBlockElement::GetList(array(),array(
            'IBLOCK_ID' => $arParams['_CLINIC']['IBLOCK_ID'],
            'SECTION_ID' => $arParams['_CLINIC']['IBLOCK_SECTION_ID'],
        ),array(),false, array('ID', 'NAME'));
    ?>
    <?if($cnt > 1): ?>
        <span class="name-title red-color align-center"><a href="/gallery/">Все клиники</a></span>
    <?php endif; ?>
<?php endif; */ ?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?endif;?>
