<?php
$imageWidth = 45;
$imageHeight = 45;

$urlParams = array(
	'region' => (int) $_REQUEST['region'],
	'clinic' => (int) $_REQUEST['clinic'],
	'doctor' => (int) $_REQUEST['doctor'],
);

$urlParams = array_filter($urlParams, function($param) { return $param > 0; });
$urlSuffix = $urlParams ? '?'.http_build_query($urlParams) : '';

$arDoctor = CIBlockElement::GetById($arResult['PROPERTIES']['DOCTOR']['VALUE'])->GetNext();
$arResult['PICTURE_SRC'] = null;
if ($arDoctor) {
	$imageId = empty($arDoctor['PREVIEW_PICTURE']) ? $arDoctor['DETAIL_PICTURE'] : $arDoctor['PREVIEW_PICTURE'];
	$arSex = CIBlockElement::GetProperty($arDoctor['IBLOCK_ID'], $arDoctor['ID'], array(), array('CODE' => 'SEX'))->GetNext();
	if ($imageId) {
		$arPicture = CFile::ResizeImageGet($imageId, array('width' => $imageWidth, 'height' => $imageHeight), BX_RESIZE_IMAGE_EXACT, true);
		if ($arPicture) {
			$arResult['DOCTOR_PICTURE_SRC'] = $arPicture['src'];
		}
	}
	$arDoctor['SEX'] = !empty($arSex) ? $arSex['VALUE_XML_ID'] : 'M';
}
$arResult['DOCTOR'] = $arDoctor;

$arResult['BACK_URL'] = '/story/' . $urlSuffix;
