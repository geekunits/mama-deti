<a href="<?php echo $arResult['DOCTOR']['DETAIL_PAGE_URL']; ?>" class="b-review_doctor b-doctor_small">
	<span class="in-bl">Врач:</span>
	<?php if ($arResult['DOCTOR_PICTURE_SRC']): ?>
		<span class="b-doctor_avatar in-bl">
			<img alt="<?php echo $arResult['DOCTOR']['NAME']; ?>" src="<?php echo $arResult['DOCTOR_PICTURE_SRC']; ?>">
		</span>
	<?php else: ?>
		<span class="b-doctor_avatar in-bl <?php echo $arResult['DOCTOR']['SEX'] === 'F' ? 'b-doctor_nophoto__female' : 'b-doctor_nophoto__male'; ?>"></span>
	<?php endif; ?>
	<span class="b-review_doctor__fio in-bl">
		<?php echo $arResult['DOCTOR']['NAME']; ?>
	</span>
</a>
<?php echo empty($arResult['DETAIL_TEXT']) ? $arResult['PREVIEW_TEXT'] : $arResult['DETAIL_TEXT']; ?>
<a href="<?php echo $arResult['BACK_URL']; ?>" class="b-review_more">
	Вернуться назад
</a>
<?php /*


<div class="review-frame">
	<div class="review-block">
		<div class="review-image">
       		<?php if ($arResult['DOCTOR_PICTURE_SRC']): ?>
       			<img alt="<?php echo $arResult['DOCTOR']['NAME']; ?>" src="<?php echo $arResult['DOCTOR_PICTURE_SRC']; ?>">
   			<?php endif; ?>
		</div>
		<div class="block-holder">
			<span class="author"><?=$arResult["NAME"]?></span>
			<span class="article-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
			<div class="review-text">
				<p><?=$arResult["PREVIEW_TEXT"]?></p>
				<?=$arResult["DETAIL_TEXT"]?>
			</div>
			<div class="review-panel">
				<div class="align-left">
					<a href="/story/" class="red-color">Вернуться назад</a>
				</div>
				<div class="info-doctor02 align-left">
					<?if(!empty($arResult["DISPLAY_PROPERTIES"]["DOCTOR"])):?>
						<span>Врач:</span>
						<?=$arResult["DISPLAY_PROPERTIES"]["DOCTOR"]["DISPLAY_VALUE"]?>
					<?endif;?>
				</div>
			</div>
		</div>
	</div>
</div>