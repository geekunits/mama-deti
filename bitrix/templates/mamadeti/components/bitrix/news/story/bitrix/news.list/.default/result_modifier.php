<?php

$imageWidth = 45;
$imageHeight = 45;
$maxLength = 150;
$textSuffix = '...';

$urlParams = array(
	'region' => (int) $_REQUEST['region'],
	'clinic' => (int) $_REQUEST['clinic'],
	'doctor' => (int) $_REQUEST['doctor'],
);
$urlParams = array_filter($urlParams, function($param) { return $param > 0; });
$urlSuffix = $urlParams ? '?'.http_build_query($urlParams) : '';
foreach($arResult['ITEMS'] as &$arItem) {
	$arItem['URL'] = $arItem['DETAIL_PAGE_URL'] . $urlSuffix;
	if (!empty($arItem['PROPERTIES']['DOCTOR']['VALUE'])) {
		$arDoctor = CIBlockElement::GetById($arItem['PROPERTIES']['DOCTOR']['VALUE'])->GetNext();
		$arItem['PICTURE_SRC'] = null;
		if ($arDoctor) {
			$imageId = empty($arDoctor['PREVIEW_PICTURE']) ? $arDoctor['DETAIL_PICTURE'] : $arDoctor['PREVIEW_PICTURE'];
			$arSex = CIBlockElement::GetProperty($arDoctor['IBLOCK_ID'], $arDoctor['ID'], array(), array('CODE' => 'SEX'))->GetNext();
			if ($imageId) {
				$arPicture = CFile::ResizeImageGet($imageId, array('width' => $imageWidth, 'height' => $imageHeight), BX_RESIZE_IMAGE_EXACT, true);
				if ($arPicture) {
					$arItem['DOCTOR_PICTURE_SRC'] = $arPicture['src'];
				}
			}
			$arDoctor['SEX'] = !empty($arSex) ? $arSex['VALUE_XML_ID'] : 'M';
		}
		$arItem['DOCTOR'] = $arDoctor;
	}
	$itemText = empty($arItem['~PREVIEW_TEXT']) ? $arItem['~DETAIL_TEXT'] : $arItem['~PREVIEW_TEXT'];
	$itemText = trim(strip_tags($itemText));
	if (mb_strlen($itemText) > $maxLength) {
		$itemText = mb_substr($itemText, 0, $maxLength - mb_strlen($textSuffix)) . $textSuffix;
	}
	$arItem['SHORT_TEXT'] = $itemText;
}