<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<div class="b-columns_two">
	<?php if (count($arResult['ITEMS']) > 0): ?>
		<?php foreach($arResult['ITEMS'] as $index => $arItem): ?>
			<?php if ($index % 2 === 0): ?>
				<div class="b-review_item clearfix">
			<?php endif; ?>
				<div class="b-column">
			        <a href="<?php echo $arItem['URL']; ?>" class="b-review_name"><?php echo $arItem['NAME']; ?></a>
			       	<a href="<?php echo $arItem['DOCTOR']['DETAIL_PAGE_URL']; ?>" class="b-review_doctor b-doctor_small">
			       		<span class="in-bl">Врач:</span>
			       		<?php if ($arItem['DOCTOR_PICTURE_SRC']): ?>
				       		<span class="b-doctor_avatar in-bl">
				       			<img alt="<?php echo $arItem['DOCTOR']['NAME']; ?>" src="<?php echo $arItem['DOCTOR_PICTURE_SRC']; ?>">
			       			</span>
		       			<?php else: ?>
		       				<span class="b-doctor_avatar in-bl <?php echo $arItem['DOCTOR']['SEX'] === 'F' ? 'b-doctor_nophoto__female' : 'b-doctor_nophoto__male'; ?>"></span>
		       			<?php endif; ?>
			       		<span class="b-review_doctor__fio in-bl"><?php echo $arItem['DOCTOR']['NAME']; ?></span></a>
		       			<a href="<?php echo $arItem['URL']; ?>" class="b-review_txt">
				        	<?php echo $arItem['SHORT_TEXT']; ?>
				        	<span class="b-review_more">Читать историю</span>
				        </a>
				</div>
			<?php if ($index % 2 === 1): ?>
				</div>
			<?php endif; ?>
		<?php if ($index % 2 === 0): ?>
		<?php endif; ?>
		<?php endforeach; ?>
	<?php else: ?>
		<div>Извините, счастливые истории отсутствуют.</div>
	<?php endif; ?>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>