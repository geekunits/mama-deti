<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["USE_RSS"]=="Y"):?>
    <?php
    if(method_exists($APPLICATION, 'addheadstring'))
        $APPLICATION->AddHeadString('<link rel="alternate" type="application/rss+xml" title="'.$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"].'" href="'.$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"].'" />');
    ?>
    <a href="<?=$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["rss"]?>" title="rss" target="_self"><img alt="RSS" src="<?=$templateFolder?>/images/gif-light/feed-icon-16x16.gif" border="0" align="right" /></a>
<?endif?>

<?if($arParams["USE_SEARCH"]=="Y"):?>
<?=GetMessage("SEARCH_LABEL")?><?$APPLICATION->IncludeComponent(
    "bitrix:search.form",
    "flat",
    Array(
        "PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"]
    ),
    $component
);?>
<?endif?>

<?if($arParams["USE_FILTER"]=="Y"):?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.filter",
    "",
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "FILTER_NAME" => $arParams["FILTER_NAME"],
        "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
        "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    ),
    $component
);
?>
<?endif?>

<?/*if (!empty($_REQUEST["doctor"])) {
    $_REQUEST["doctor"] = (int) $_REQUEST["doctor"];
    $GLOBALS["arrFilter"] = array("PROPERTY_DOCTOR"=>$_REQUEST["doctor"]);
}*/?>

<?php
$selRegion = isset($_REQUEST["region"]) ? intval($_REQUEST["region"]) : $GLOBALS["CURRENT_CITY"]["ID"];
$selClinic = isset($_REQUEST["clinic"]) ? intval($_REQUEST["clinic"]) : -1;
$selDoctor = isset($_REQUEST["doctor"]) ? intval($_REQUEST["doctor"]) : -1;

$arRegion = array();
$arClinic = array();
$arDoctor = array();

$rsElement = CIBlockElement::GetList(array("NAME"=>"ASC"),array("IBLOCK_ID"=>7,"ACTIVE"=>"Y"));
while ($arElement = $rsElement->Fetch())
    $arRegion[$arElement["ID"]] = $arElement['NAME'];

$rsElement = CIBlockElement::GetList(
    array(),
    array(
        "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
        "ACTIVE"=>"Y",
        "!PROPERTY_DOCTOR"=>false
    ),
    array(
        "PROPERTY_DOCTOR",
        "PROPERTY_DOCTOR.NAME",
        "PROPERTY_DOCTOR.PROPERTY_CLINIC"
    )
);

$arAllDoctor = array();
while ($arElement = $rsElement->Fetch()) {
    $clinicId = $arElement['PROPERTY_DOCTOR_PROPERTY_CLINIC_VALUE'];
    if (!isset($arAllDoctor[$clinicId])) {
        $arAllDoctor[$clinicId] = array();
    }

    $arAllDoctor[$clinicId][$arElement['PROPERTY_DOCTOR_VALUE']] = $arElement['PROPERTY_DOCTOR_NAME'];
}

$rsElement = CIBlockElement::GetList(
    array(
        "NAME"=>"ASC",
    ),
    array(
        "IBLOCK_ID"=>2,
        "ACTIVE"=>"Y",
        "PROPERTY_CITY"=>$selRegion
    )
);
while ($arElement = $rsElement->Fetch()) {
    $arClinic[$arElement["ID"]] = $arElement["NAME"];
}

if (!array_key_exists($selClinic, $arClinic))
    $selClinic = -1;

if ($selClinic>0) {
    $arDoctor = $arAllDoctor[$selClinic];
    if (!isset($arDoctor[$selDoctor])) { $selDoctor = -1; }

} else {
    $selDoctor = -1;
}
?>

<div style="width:200px;float:right;display:inline-block;"><a href="#" class="button05 block-button link_review">Оставить отзыв</a></div>

<form action="<?=$APPLICATION->GetCurPage();?>" method="GET" name="REVIEWS_FORM" id="REVIEWS_FORM" enctype="multipart/form-data">
<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
<div id="reviews_form_content">

<?php if($_REQUEST["is_ajax_post"] == "Y") $APPLICATION->RestartBuffer();?>

<div class="reviews_filter new-reviews">
    Выберите регион:
    <select name="region">
    <?foreach ($arRegion as $id=>$name):?>
        <option value="<?=$id?>"<?if ($id==$selRegion):?> selected<?endif?>><?=$name?></option>
    <?endforeach?>
    </select>
Выберите клинику:
    <select name="clinic">
        <option value="-1">Все</option>
    <?foreach ($arClinic as $id=>$name):?>
        <option value="<?=$id?>"<?if ($id==$selClinic):?> selected<?endif?>><?=$name?></option>
    <?endforeach?>
    </select>
<?if ($selClinic>0):?>
    <br/><br/>Выберите врача:
    <select name="doctor">
        <option value="-1">Все</option>
    <?foreach ($arDoctor as $id=>$name):?>
        <option value="<?=$id?>"<?if ($id==$selDoctor):?> selected<?endif?>><?=$name?></option>
    <?endforeach?>
    </select>
<?endif?>
</div>

<br/>
<br/>

<?php
 $GLOBALS["arrFilter"]["PROPERTY_DOCTOR"] = -1;

if ($selDoctor>0) {
    $GLOBALS["arrFilter"]["PROPERTY_DOCTOR"] = $selDoctor;
    //$GLOBALS["arrFilter"]["PROPERTY_CLINIC"] = $selClinic;
} elseif ($arAllDoctor[$selClinic]) {
    $GLOBALS["arrFilter"]["PROPERTY_DOCTOR"] = array_keys($arAllDoctor[$selClinic]);
} else {
    $doctorIds = array_map(function ($clinicId) use ($arAllDoctor) {
        return array_keys($arAllDoctor[$clinicId]) ?: array();
    }, array_keys($arClinic));
    $doctorIds = call_user_func_array('array_merge', $doctorIds);

    if ($doctorIds) {
        $GLOBALS["arrFilter"]["PROPERTY_DOCTOR"] = $doctorIds;
    }
}

?>
<?if(!empty($GLOBALS['arrFilter'])):?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "",
    Array(
        "_REQUEST" => $_REQUEST,
        "IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
        "IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
        "NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
        "SORT_BY1"	=>	$arParams["SORT_BY1"],
        "SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
        "SORT_BY2"	=>	$arParams["SORT_BY2"],
        "SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
        "FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
        "PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
        "DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
        "DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
        "SET_TITLE"	=>	$arParams["SET_TITLE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
        "CACHE_TIME"	=>	$arParams["CACHE_TIME"],
        "CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
        "PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
        "PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
        "DISPLAY_NAME"	=>	"Y",
        "DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
        "PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
        "ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
        "USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
        "FILTER_NAME"	=>	$arParams["FILTER_NAME"],
        "HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
        "CHECK_DATES"	=>	$arParams["CHECK_DATES"],
    ),
    $component
);?>
<?else:?>
<p>Извините, счастливые истории отсутствуют.</p>
<?endif;?>
<?php if($_REQUEST["is_ajax_post"] == "Y") die();?>

</div>
</form>
