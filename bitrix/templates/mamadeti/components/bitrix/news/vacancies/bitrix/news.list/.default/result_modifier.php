<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
foreach ($arResult["ITEMS"] as &$arItem) {
    if (intval($arItem["PROPERTIES"]["CLINIC"]["VALUE"])>0) {
        $rsElement = CIBlockElement::GetByID($arItem["PROPERTIES"]["CLINIC"]["VALUE"]);
        if ($obElement = $rsElement->GetNextElement()) {
            $arItem["CLINIC"] = $obElement->GetFields();
            $arItem["CLINIC"]["PROPERTIES"] = $obElement->GetProperties();
        }
    }
}

if (isset($arItem))
    unset($arItem);
?>
