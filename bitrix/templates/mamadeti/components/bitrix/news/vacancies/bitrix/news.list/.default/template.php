<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?php
    $urlParams = array(
        'clinic' => (int) $_REQUEST['clinic'],
    );
    $urlParams = array_filter($urlParams, function ($param) { return $param > 0; });
    $urlSuffix = $urlParams ? '?'.http_build_query($urlParams) : '';
?>

<div class="list-holder">
    <?php if ($arResult['ITEMS']): ?>
        <?
            if(GetCurrentCity() == 27){
                foreach($arResult["ITEMS"] as $arItem){
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                        <p><?php echo $arItem['NAME']; ?></p>
                    <?
                }
                ?>
                <?
            }else{
                foreach($arResult["ITEMS"] as $arItem){
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <p><a class="red-color" href="<?php echo $arItem['DETAIL_PAGE_URL'] . $urlSuffix; ?>"><?php echo $arItem['NAME']; ?></a></p>
                    <?
                }
            }
        ?>
    <?php endif; ?>
</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?endif;?>
