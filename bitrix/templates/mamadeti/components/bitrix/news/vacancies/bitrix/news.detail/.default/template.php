<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?php
    $urlParams = array(
        'clinic' => (int) $_REQUEST['clinic'],
    );
    $urlParams = array_filter($urlParams, function ($param) { return $param > 0; });
    $urlSuffix = $urlParams ? '?'.http_build_query($urlParams) : '';
?>
<span class="name-title red-color"><?php echo $arResult['NAME']; ?></span>

<?if (is_array($arResult["CLINIC"])):?>
    <a href="<?=$arResult["CLINIC"]["DETAIL_PAGE_URL"]?>"><?=$arResult["CLINIC"]["NAME"]?></a>
<?endif?>

<p><?=$arResult["DETAIL_TEXT"]?></p>

<a href="/about/vacancies-about-us/<?php echo $urlSuffix; ?>" class="red-color">Вернуться назад</a>
