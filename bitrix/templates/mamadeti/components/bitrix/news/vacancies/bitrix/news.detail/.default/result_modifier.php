<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?php

if (intval($arResult["PROPERTIES"]["DOCTOR"]["VALUE"])>0) {
    $rsElement = CIBlockElement::GetByID($arResult["PROPERTIES"]["DOCTOR"]["VALUE"]);
    if ($obElement = $rsElement->GetNextElement()) {
        $arResult["DOCTOR"] = $obElement->GetFields();
        $arResult["DOCTOR"]["PREVIEW_PICTURE"] = CFile::GetFileArray($arResult["DOCTOR"]["PREVIEW_PICTURE"]);
        $arResult["DOCTOR"]["DETAIL_PICTURE"] = CFile::GetFileArray($arResult["DOCTOR"]["DETAIL_PICTURE"]);
        $arResult["DOCTOR"]["PROPERTIES"] = $obElement->GetProperties();

        if (intval($arResult["DOCTOR"]["PROPERTIES"]["CLINIC"]["VALUE"])>0) {
            $rsElement = CIBlockElement::GetByID($arResult["DOCTOR"]["PROPERTIES"]["CLINIC"]["VALUE"]);
            if ($obElement = $rsElement->GetNextElement()) {
                $arResult["CLINIC"] = $obElement->GetFields();
                $arResult["CLINIC"]["PROPERTIES"] = $obElement->GetProperties();
            }
        }
    }
} elseif (intval($arResult["PROPERTIES"]["CLINIC"]["VALUE"])>0) {
    $rsElement = CIBlockElement::GetByID($arResult["PROPERTIES"]["CLINIC"]["VALUE"]);
    if ($obElement = $rsElement->GetNextElement()) {
        $arResult["CLINIC"] = $obElement->GetFields();
        $arResult["CLINIC"]["PROPERTIES"] = $obElement->GetProperties();
    }
}

if (is_array($arResult["DOCTOR"])) {
    $arFile = is_array($arResult["DOCTOR"]["PREVIEW_PICTURE"]) ? $arResult["DOCTOR"]["PREVIEW_PICTURE"] : $arResult["DOCTOR"]["DETAIL_PICTURE"];
    if (is_array($arFile)) {
        $arFileTmp = CFile::ResizeImageGet(
                    $arFile, array("width" => 110, "height" => 110), BX_RESIZE_IMAGE_EXACT, true
        );
        $arResult["DOCTOR"]["THUMB_PICTURE"] = array_change_key_case($arFileTmp, CASE_UPPER);
    }
}

if (is_array($arResult["CLINIC"])) {
    $arResult["CLINIC"]["PREVIEW_PICTURE"] = CFile::GetFileArray($arResult["CLINIC"]["PREVIEW_PICTURE"]);
    $arResult["CLINIC"]["DETAIL_PICTURE"] = CFile::GetFileArray($arResult["CLINIC"]["DETAIL_PICTURE"]);

    $arFile = is_array($arResult["CLINIC"]["PREVIEW_PICTURE"]) ? $arResult["CLINIC"]["PREVIEW_PICTURE"] : $arResult["CLINIC"]["DETAIL_PICTURE"];
    if (is_array($arFile)) {
        $arFileTmp = CFile::ResizeImageGet(
                    $arFile, array("width" => 110, "height" => 110), BX_RESIZE_IMAGE_EXACT, true
        );
        $arResult["CLINIC"]["THUMB_PICTURE"] = array_change_key_case($arFileTmp, CASE_UPPER);
    }
}

//echo '<pre>'; print_r($arResult); echo '</pre>';

?>
