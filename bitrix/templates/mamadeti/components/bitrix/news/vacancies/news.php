<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php

CModule::IncludeModule("iblock");
$selClinic = isset($_REQUEST["clinic"]) ? intval($_REQUEST["clinic"]) : -1;

$arClinic = array();

$rsElement = CIBlockElement::GetList(
    array(
        "NAME" => "ASC",
    ),
    array(
        "IBLOCK_ID" => 2,
        "ACTIVE" => "Y",
        "PROPERTY_CITY" => $GLOBALS['CURRENT_CITY']['ID'],
    ),
    false,
    false,
    array(
        'ID',
        'IBLOCK_ID',
        'NAME',
        'PROPERTY_CITY',
    )
);
while ($arElement = $rsElement->Fetch()) {
    $arClinic[$arElement["ID"]] = $arElement["NAME"];
}

if (!array_key_exists($selClinic, $arClinic)) {
    $selClinic = -1;
}

if ($selClinic>0) {
    $GLOBALS["arrFilter"]["PROPERTY_CLINIC"] = $selClinic;
} else {
    $GLOBALS["arrFilter"]["PROPERTY_CLINIC"] = $arClinic ? array_keys($arClinic) : -1;
}

$rsVacancies =
    CIBlockElement::GetList(
        array(),
        array(
            "IBLOCK_ID" => $arParams['IBLOCK_ID'],
            "ACTIVE" => "Y",
            "PROPERTY_CLINIC" => array_keys($arClinic),
        ),
        false,
        false,
        array(
            'ID',
            'IBLOCK_ID',
            'PROPERTY_CLINIC',
        )
    );

$cm = CityManager::getInstance();
?>
 <?php if ($cm->getCurrentCityId() == 27): ?>
<div>Хотите стать одним из нас? Пришлите свое резюме на электронный адрес:</div>
<strong>Исключительно для вопросов по открытым вакансиям и работе в Москве</strong><br>
 <a href="mailto:e.soloveva@mcclinics.ru" >e.soloveva@mcclinics.ru</a> или позвоните по телефону +7 (495) 230-05-45 доб.126. 

<div> 
  <br />
 </div>
<?php endif; ?>

<?php if ($cm->getCurrentCityId() != 27 || ($cm->getCurrentCityId() == 27 && $rsVacancies->GetNext())): ?>
    <h3>
    Вакансии
    <?php if ($selClinic > 0): ?>
        в <?php echo str_replace('Клиника', 'Клинике', $arClinic[$selClinic]); ?>
    <?php elseif (count($arClinic) === 1): ?>
        в <?php echo str_replace('Клиника', 'Клинике', current($arClinic)); ?>
    <?php else: ?>
        <?php echo $GLOBALS['CURRENT_CITY']['NAME']; ?>
    <?php endif; ?>
    </h3>
<?php endif; ?>

<form action="<?=$APPLICATION->GetCurPage();?>" method="GET" name="REVIEWS_FORM" id="REVIEWS_FORM" enctype="multipart/form-data">
<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
<div id="reviews_form_content">

<?php if ($_REQUEST["is_ajax_post"] == "Y") {
    $APPLICATION->RestartBuffer();
}?>

<?php if ((count($arClinic) > 1 && $cm->getCurrentCityId() != 27) || (count($arClinic) > 1 && $rsVacancies->GetNext())): ?>
    <div class="reviews_filter new-reviews">
        Выберите клинику:
        <select name="clinic">
            <option value="-1">Все</option>
        <?foreach ($arClinic as $id=>$name):?>
            <option value="<?=$id?>"<?if ($id==$selClinic):?> selected<?endif?>><?=$name?></option>
        <?endforeach?>
        </select>
    </div>
<?php endif; ?>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "",
    Array(
        "_REQUEST" => $_REQUEST,
        "IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
        "IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
        "NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
        "SORT_BY1"	=>	$arParams["SORT_BY1"],
        "SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
        "SORT_BY2"	=>	$arParams["SORT_BY2"],
        "SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
        "FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
        "PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
        "DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
        "DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
        "SET_TITLE"	=>	$arParams["SET_TITLE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
        "CACHE_TIME"	=>	$arParams["CACHE_TIME"],
        "CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
        "PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
        "PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
        "DISPLAY_NAME"	=>	"Y",
        "DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
        "PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
        "ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
        "USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
        "FILTER_NAME"	=>	$arParams["FILTER_NAME"],
        "HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
        "CHECK_DATES"	=>	$arParams["CHECK_DATES"],
    ),
    $component
);?>

<?php if (!$rsVacancies->GetNext() && $cm->getCurrentCityId() != 27): ?>
    <p>
        <?php if ($selClinic > 0 || count($arClinic) === 1): ?>
            На сегодняшний день вакантных должностей в клинике нет.
        <?php else: ?>
            На сегодняшний день вакантных должностей в регионе нет.
        <?php endif; ?>
    </p>
<?php endif; ?>

<?php if ($_REQUEST["is_ajax_post"] == "Y") {
    die();
}?>
</div>
<?
    if($cm->getCurrentCityId() == 27):
?>
    <div class="vacancies-hh">
        <script class="hh-script" src="https://api.hh.ru/widgets/vacancies/employer?title=%D0%92%D0%B0%D0%BA%D0%B0%D0%BD%D1%81%D0%B8%D0%B8+%C2%AB%D0%9C%D0%B0%D1%82%D1%8C+%D0%B8+%D0%B4%D0%B8%D1%82%D1%8F%C2%BB+%D0%B2+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B5+%D0%BD%D0%B0+hh.ru&employer_id=1104505&locale=RU&links_color=931368&border_color=ffffff&area=1&area=2019"></script>
    </div>
<?php endif; ?>
</form>
