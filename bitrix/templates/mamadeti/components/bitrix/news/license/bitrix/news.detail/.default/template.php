<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="license-detail">
<h1><?=$arResult["NAME"]?></h1>
<ul>
<?foreach ($arResult["MORE_PHOTO"] as $arPhoto):?>
	<li>
		<a class="fancybox" data-fancybox-group="license" href="<?=$arPhoto["~SRC"]?>" title="<?=$arPhoto["DESCRIPTION"]?>">
			<span class="image"><img src="<?=$arPhoto["SRC"]?>" alt="<?=$arPhoto["DESCRIPTION"]?>"></span>
			<span class="name"><?=$arPhoto["DESCRIPTION"]?></span>
		</a>
	</li>
<?endforeach?>
</ul>
</div>