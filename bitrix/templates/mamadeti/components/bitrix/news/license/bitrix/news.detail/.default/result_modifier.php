<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if ($arParams["RESIZE_MORE_PHOTO"] == "Y") {
    $arMorePhoto = array();
    foreach ($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $fileid) {
	$arFile = CFile::GetFileArray($fileid);
        $arFileTmp = CFile::ResizeImageGet(
                        $arFile, array("width" => $arParams["RESIZE_MORE_PHOTO_WIDTH"], "height" => $arParams["RESIZE_MORE_PHOTO_HEIGHT"]), intval($arParams["RESIZE_MORE_PHOTO_METHOD"]), true
        );
        $arMorePhoto[] = array(
            "SRC" => $arFileTmp["src"],
	    "~SRC" => $arFile["SRC"],
	    "DESCRIPTION" => $arFile["DESCRIPTION"],
            "WIDTH" => $arFileTmp["width"],
            "HEIGHT" => $arFileTmp["height"],
        );
    }

    $arResult["MORE_PHOTO"] = $arMorePhoto;
}
?>