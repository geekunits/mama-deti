<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="b-menu_left__wrap">
	<ul class="b-menu_left">
		<?
		$previousLevel = 0;
		foreach($arResult as $arItem):
		?>
			<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
				<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
			<?endif?>
			<?if ($arItem["IS_PARENT"]):?>
				<li class="b-menu_item b-menu_item__<?=$arItem["DEPTH_LEVEL"];?><?if ($arItem["SELECTED"]):?> b-menu_item__active<?endif?>">
					<span class="b-menu_item__plusminus<?if ($arItem["SELECTED"]):?> b-menu_item__plusminus-active<?endif?> js-menu-click"><?if ($arItem["SELECTED"]):?>&ndash;<?else:?>+<?endif?></span>
					<a class="b-menu_item__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
					<ul class="b-menu_left__sub js-menu-toggle">
			<?else:?>
				<?if ($arItem["PERMISSION"] > "D"):?>
					<li class="b-menu_item b-menu_item__<?=$arItem["DEPTH_LEVEL"];?><?if ($arItem["SELECTED"]):?> b-menu_item__active<?endif?>">
						<a class="b-menu_item__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
					</li>
				<?endif?>
			<?endif?>
			<?$previousLevel = $arItem["DEPTH_LEVEL"];?>
		<?endforeach?>

		<?if ($previousLevel > 1)://close last item tags?>
			<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
		<?endif?>
	</ul>
</div>
<?endif?>
