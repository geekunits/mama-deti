<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$depth = 1;
$nextDepth = false;
$arNewMenu = array();
foreach($arResult as $arMenu)
{
	$arMenu["IS_PARENT"] = false;
	if ($arMenu["SELECTED"])
	{
		$depth = $arMenu["DEPTH_LEVEL"];
		$nextDepth = true;
	} elseif ($arMenu["DEPTH_LEVEL"]<=$depth)
	{
		$depth = $arMenu["DEPTH_LEVEL"];
		$nextDepth = false;
	}

	if ($arMenu["DEPTH_LEVEL"] == $depth)
		$arNewMenu[] = $arMenu;
	elseif ($nextDepth && $arMenu["DEPTH_LEVEL"] == $depth+1)
		$arNewMenu[] = $arMenu;
}

$menuIndex = 0;
$previousDepthLevel = 1;
foreach($arNewMenu as $arMenu)
{
	if ($menuIndex > 0)
		$arNewMenu[$menuIndex - 1]["IS_PARENT"] = $arMenu["DEPTH_LEVEL"] > $previousDepthLevel;
	$previousDepthLevel = $arMenu["DEPTH_LEVEL"];
	$menuIndex++;
}

$arResult = $arNewMenu;
unset($arNewMenu);


//determine if child selected


$bWasSelected = false;
$arParents = array();
$depth = 1;
foreach($arResult as $i=>$arMenu)
{
	$depth = $arMenu['DEPTH_LEVEL'];

	if($arMenu['IS_PARENT'] == true)
	{
		$arParents[$arMenu['DEPTH_LEVEL']-1] = $i;
	}
	elseif($arMenu['SELECTED'] == true)
	{
		$bWasSelected = true;
		break;
	}
}

if($bWasSelected)
{
	for($i=0; $i<$depth-1; $i++)
		$arResult[$arParents[$i]]['CHILD_SELECTED'] = true;
}
?>
