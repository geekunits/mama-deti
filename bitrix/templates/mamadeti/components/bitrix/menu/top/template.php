<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
CModule::IncludeModule("iblock");
?>
<?php
    $previousLevel = 0;
    $arStack = array();

    $isExtended = false;
    $isHighlighted = false;
    $childCount = 0;
?>
<?php foreach($arResult as $arItem):?>
    <?while ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
        <?$previousLevel--?>
        <?=array_pop($arStack)?>
    <?endwhile?>
    <?if ($arItem["DEPTH_LEVEL"] == 1):?>
        <?php
            $childCount = 0;
            $disableExtended = $arItem['TOTAL_CHILDREN_COUNT'] < 9 || !$arParams['HAS_HIGHLIGHTED'];
            $isExtended = (bool) $arItem['PARAMS']['EXTENDED'];
            $isHighlighted = !$disableExtended && $isExtended;
        ?>
        <div class="b-menu_top__box in-bl<?if ($arItem["SELECTED"]):?> b-menu_top__active<?endif?>">
            <a href="<?=$arItem["LINK"]?>" class="b-menu_top__link" <?php if ($arItem['PARAMS']['target']): ?>target="<?= $arItem['PARAMS']['target']; ?>"<?php endif; ?>
            <?php if ($arItem['PARAMS']['rel']): ?>rel="<?php echo $arItem['PARAMS']['rel']; ?>"<?php endif; ?>>
                <?=$arItem["TEXT"]?>
            </a>
        <?php if ($arItem['IS_PARENT']): ?>
            <?php if ($isExtended): ?>
                 <div class="b-menu_sub__wrap">
                    <div class="b-menu_sub__services <?php if ($disableExtended): ?>b-menu_sub__services-col2<?php else: ?>l-width<?php endif; ?>">
                        <div class="b-menu_service">
                <?php if (!$disableExtended): ?>
                            <div class="b-menu_service__left">
                                <div class="b-menu_service__detail">
                <?php else: ?>
                            <div class="b-menu_service__right clearfix">
                <?php endif; ?>
                <?php
                    $arStack[] = ($arItem['CHILDREN_COUNT'] ? '</div>' : '').'</div></div></div></div></div>';
                    if (($disableExtended && $arItem['TOTAL_CHILDREN_COUNT']) || $arItem['CHILDREN_COUNT']) {
                        $childPerColumn = ceil($disableExtended ? $arItem['TOTAL_CHILDREN_COUNT'] / 2 : $arItem['CHILDREN_COUNT'] / 3);
                    }
                ?>
            <?php else:?>
                <div class="b-menu_sub__wrap">
                    <div class="b-menu_sub">
                <?php
                    $arStack[] = '</div></div></div>';
                ?>
            <?php endif; ?>
        <?php else: ?>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <?php if ($isExtended): ?>
            <?php if ($isHighlighted && !$arItem['PARAMS']['HIGHLIGHT']): ?>
                <?php $isHighlighted = false; ?>
                    </div>
                </div>
                <div class="b-menu_service__right clearfix">
            <?php endif; ?>
            <?php if ($isHighlighted): ?>
                <a class="<?php echo $arItem['PARAMS']['CLASS']; ?>" href="<?=$arItem["LINK"]?>">
                    <?=$arItem["TEXT"]?>
                    <span class="b-menu_service__detail-txt"><?php echo $arItem['PARAMS']['DESCRIPTION']; ?></span>
                </a>
            <?php else: ?>
                <?php
                    $childCount++;
                ?>
                <?php if ($childCount === 1 || !(($childCount - 1) % $childPerColumn)): ?>
                    <?php if ($childCount > 1): ?>
                        </div>
                    <?php endif; ?>
                    <div class="b-menu_service__col">
                <?php endif; ?>
                <a class="<?php echo $arItem['PARAMS']['CLASS']; ?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            <?php endif; ?>
        <?php else: ?>
            <a class="<?php echo $arItem['PARAMS']['CLASS']; ?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
        <?php endif; ?>
    <?endif?>
    <?$previousLevel = $arItem["DEPTH_LEVEL"];?>
<?endforeach?>
<?while (!empty($arStack)):?>
    <?=array_pop($arStack)?>
<?endwhile?>
<?endif?>