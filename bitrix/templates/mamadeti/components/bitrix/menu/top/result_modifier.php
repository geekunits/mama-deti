<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

//determine if child selected

$bWasSelected = false;
$arParents = array();
$depth = 1;
$currentParentIndex = null;

$arResult = array_filter($arResult, function ($arItem) {
    return $arItem['DEPTH_LEVEL'] < 3;
});

$hasHighlighted = false;

foreach ($arResult as $i => $arMenu) {
    if ($arMenu['PARAMS']['HIGHLIGHT']) {
        $hasHighlighted = true;
    }

    if ($arMenu['IS_PARENT'] == true && $arMenu['DEPTH_LEVEL'] == 1) {
        $currentParentIndex = $i;
        $arResult[$currentParentIndex]['CHILDREN_COUNT'] = 0;
        $arResult[$currentParentIndex]['TOTAL_CHILDREN_COUNT'] = 0;
    } elseif ($arMenu['DEPTH_LEVEL'] > 1) {
        if (!$arMenu['PARAMS']['HIGHLIGHT']) {
            $arResult[$currentParentIndex]['CHILDREN_COUNT']++;
        }
        $arResult[$currentParentIndex]['TOTAL_CHILDREN_COUNT']++;
    }

    if (!$bWasSelected) {
        $depth = $arMenu['DEPTH_LEVEL'];

        if ($arMenu['IS_PARENT'] == true) {
            $arParents[$arMenu['DEPTH_LEVEL']-1] = $i;
        } elseif ($arMenu['SELECTED'] == true) {
            $bWasSelected = true;
        }
    }
}

if ($bWasSelected) {
    for ($i = 0; $i<$depth-1; $i++) {
        $arResult[$arParents[$i]]['CHILD_SELECTED'] = true;
    }
}

$arParams['HAS_HIGHLIGHTED'] = $hasHighlighted;
