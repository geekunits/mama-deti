$(document).ready(function(){

    touchStartEnd = $("html").hasClass("bx-touch")?"touchstart touchend":"click";
    touchStart = $("html").hasClass("bx-touch")?"touchstart":"click";
    touchEnd = $("html").hasClass("bx-touch")?"touchend":"click";

    var $actionsForm = $('.js-actions_form');

    $actionsForm.find('[name=clinic_id]').change(function(e) {
        $actionsForm.submit();
    });

    $('.js-popups input, .js-popups select, .js-wrap input, .js-wrap select').styler();
    BX.addCustomEvent('onAjaxSuccess', function(){
        $('.js-popups input, .js-popups select, .js-wrap input, .js-wrap select').styler();
    });

    $(document).on('click', '.js-city-selector-link', function(e) {
        var link = $(this).data('link');
        if (typeof link !== 'undefined') {
            e.preventDefault();
            window.location.assign(link);
        }
    });

    $(".change_city").click(function(){
        $('.point-drop').show();
        return false;
    });

    $("body").click(function(){
        $(".point-drop").hide();
    });

    $("body").on("click", ".js-show-call-me, .link_phone", function(){
        var toTop = $(window).scrollTop(), link = $(this);
        $.ajax({
            url: "/ajax/callback.php",
            data: {services: link.data('service-ids'), CLINIC: link.data('clinic-id')},
            context: $('#callback-popup')
        }).done(handleAjaxPopup).done(function() {
            if (!link.data('clinic-id')) {
                return;
            }

            $('#callback-popup select[name="MS_P_CLINIC"]').val(link.data('clinic-id'));
        });
        return false;
    });

    $("body").on("click", ".js-show-appointment, .link_zapis_priem", function(){
        $.ajax({
            url: "/ajax/zapis_na_priem.php",
            data: {CLINIC:$(this).data('clinic')},
            context: $('#zapisnapriem-popup')
        }).done(handleAjaxPopup);
        return false;
    });

    $("body").on("click", ".js-show-open-day", function(e) {
        e.preventDefault();

        window.reachGoal('AppointmentDL');

        $.ajax({
            url: "/ajax/open_day.php",
            context: $('#open-day-popup')
        }).done(handleAjaxPopup);

    });

    $("body").on("click", ".link_ask", function(){
        $.ajax({
            url: "/ajax/doctor_question.php",
            data: {CLINIC: $(this).data('clinic'), DOCTOR: $(this).data('doctor'), SERVICE: $(this).data('service')},
            context: $('#ask-popup')
        }).done(handleAjaxPopup);
        return false;
    });

    $("body").on("click", ".link_ask_direction, .js-ask_question_doctors_btn", function(){
        $.ajax({
            url: "/ajax/question.php",
            data: {CLINIC: $(this).data('clinic'), DOCTOR: $(this).data('doctor'), SERVICE: $(this).data('service')},
            context: $('#ask-popup')
        }).done(handleAjaxPopup);
        return false;
    });

    $(document).on('click', '.link_review', function(){
        $that = $(this);
        $.ajax({
            url: "/ajax/review.php",
            data: {},
            context: $('#ask-popup')
        }).done(function(data){
            $this = $(this);
            $this.html(data);
            $this.find('[name="review_type"][value="'+$that.data('type')+'"]')
                .prop('checked', true)
                .trigger('refresh');
            $this.find('select[name="MS_P_CLINIC"]')
                .val($that.data('clinic'))
                .trigger('refresh');
            /**DO NOT DELETE**/
            $this.find('[name="review_type"][value="'+$that.data('type')+'"]').trigger('change');
            if($that.data('type') == 'doctor'){
                $this.find('select[name="MS_P_DOCTOR"]').data('default',$that.data('doctor'));
            }
            $this.find('[name="MS_NAME"]').focus();
            $this.show();
            centrateDiv($this);
        });
        return false;
    });

    $("body").on('click', ".popup-holder .bg, .popup-holder .close", function(){
        closePopup();
        return false;
    });

    $('.show-all-clinics').click(function(e)
    {
        e.preventDefault();
        var c = $('.ajax-show-all-clinics');
        BX.showWait(c[0]);

        $.ajax({url: window.location.pathname+'?show_all=y&ajax_catalog=y', context: c}).done(function(data)
        {
            $(this).html(data);
            BX.closeWait(this);
            $('.show-all-clinics').hide();
            $('html, body').animate({
                scrollTop: $(this).offset().top
            }, 2000);
        });
    });

    $.fn.clinicMapPrint = function(locname, coord)
    {
        if (locname == undefined)
        {
            locname = $(this).data('locname');
            coord = $(this).data('coord');
        }
        window.open("http://maps.yandex.ru/print/?text="+locname+"&ll="+coord+"&z=15&l=map", "_blank", "menubar=no,location=no,resizable=no,scrollbars=yes,status=no");
    }

    $(document).on("click", ".ya-map-print", function(e)
    {
        e.preventDefault();
        $(this).clinicMapPrint();
    });


    $(".tab").hide();
    $(".tab").first().show();
    $(".tabcontrols_li > a").click(function(){
        var _this = $(this).parent();
        var index = _this.index();
        _this.addClass("active").trigger('tabchange').siblings().removeClass("active").trigger('tabchange');
        $(".tab").hide();
        $(".tab").eq(index).show();
        return false;
    });

    $(".tabcontrols_li.active > a").click();

    if (location.hash) {
        $(location.hash).click();
    }

    $(".check-list input").click(function(){
        if ($(this).is(":checked")) {
            $(this).closest("li").addClass("active");
            $(this).closest("li").find("input").prop("checked", true);
        }
        else {
            $(this).closest("li").removeClass("active");
            $(this).closest("li").find("input").prop("checked", false);
        }
    });
    $(".check-list label").click(function(){
        if ($(this).prev().is(":checked")) {
            $(this).closest("li").addClass("active");
            $(this).closest("li").find("input").prop("checked", true);
        }
        else {
            $(this).closest("li").removeClass("active");
            $(this).closest("li").find("input").prop("checked", false);
        }
    });

    $("body").on("click", ".search-input .remove-button", function(){
        $(this).closest("li").remove();
        return false;
    });

    var searchClinic = function()
    {
        var tags = "";
        var $input_search = $("#search-clinics .search-input input:text");
        var txt = $input_search.val();
        var txt_tmp = $input_search.data("val");
        if ((txt.length>2) && (txt!=txt_tmp)) {
            $("#search-clinics .search-list span").each(function(){
                tags = tags + $(this).html() + ",";
            });
            tags = tags.substr(0,tags.length-1);
            $.post("/ajax/searchClinics.php", {txt:txt, tags:tags, city: $input_search.data('city')}, function(data){
                if (data.count>0){
                    if (data.count*40>198) $("#search-clinics .scrollpane").css("height", "198px");
                    else $("#search-clinics .scrollpane").css("height", data.count*40+"px");
                    $("#search-clinics .search-listing").html(data.str);
                    $("#search-clinics .search-drop").show();
                    $(".search-drop").css({"margin":"0", "z-index":"100"});
                    return true;
                }
                else $("#search-clinics .search-drop").hide();
            }, "json");
        }
        else $("#search-clinics .search-drop").hide();
        return false;
    }

    $(".bt_search_clinic").click(function(){
        var $input_search = $("#search-clinics .search-input input:text");
        var txt = $input_search.val();

        if (txt.length>2)
        {
            if (!searchClinic())
                alert("Ничего не найдено");
        } else
            alert("Введите минимум 3 символа");
        return false;
    });
    $(".input_search_clinic").keyup(function(){
        searchClinic();
        return false;
    });

    $("body").on("click", "#search-clinics #search_result a", function(){
        var txt = $(this).find("span").html();
        var str = '<li><span class="title">' + txt + '</span><a href="#" class="remove-button">remove</a></li>';
        $("#search-clinics .search-list").append(str);
        $("#search-clinics .search-input input:text").val("");
        $("#search-clinics .search-drop").hide();
        $("#search-clinics .search-listing").html("");
        return false;
    });

    $("#search-clinics input:submit").click(function(){
        var tags = ""; var service = "";
        $("#search-clinics .search-list span").each(function(){
            tags = tags + $(this).html() + ",";
        });
        tags = tags.substr(0,tags.length-1);

        $("#search-clinics .service-tree-menu-search input:checked").each(function(){
            service = service + $(this).val() + ",";
        });
        service = service.substr(0,service.length-1);
        window.location.href = '?tags=' + tags + '&service=' + service;
        return false;
    });

    $(".search-price-list input[name='q']").keyup(function() {
        var txt = $(this).val();
        var form = $(this).closest('form');
        if (txt.length>2)
        {
            var ar = form.serializeArray();
            ar[ar.length] = {name: "ajax", value: "y"};
            $.ajax({url: form.attr("action"), data: ar, dataType: "json", context: form}).done(function(data)
            {
                if (data.ITEMS.length>0)
                {
                    $(this).find(".scrollpane").css("height", data.ITEMS.count*40+"px");
                    var str = "";
                    $.each(data.ITEMS, function(i,v) {
                        str+="<li><a href=\"#\" class=\"title\"><span class=\"black-color\">"+v+"</span></a></li>";
                    });
                    $(this).find(".search-listing").html(str);
                    $(this).find(".search-drop").show();
                    $(".search-drop").css({"margin":"0", "z-index":"100"});
                }
            })
        }
    });

    $("body").on("click", ".search-price-list .search-listing a", function(){
        var txt = $(this).find("span").html();
        $(".search-price-list input[name='q']").val(txt);
        $(".search-price-list .search-drop").hide();
        $(".search-price-list .search-listing").html("");
        return false;
    });


    $(".bt_search_doctor").click(function(){
        var tags = "";
        var $input_search = $("#search-doctors .search-input input:text");
        var txt = $input_search.val();
        var txt_tmp = $input_search.data("val");
        if ((txt.length>2) && (txt!=txt_tmp)) {
            $("#search-doctors .search-list span").each(function(){
                tags = tags + $(this).html() + ",";
            });
            tags = tags.substr(0,tags.length-1);
            $.post("/ajax/searchDoctors.php", {txt:txt, tags:tags}, function(data){
                if (data.count>0){
                    if (data.count*40>198) $("#search-doctors .scrollpane").css("height", "198px");
                    else $("#search-doctors .scrollpane").css("height", data.count*40+"px");
                    $("#search-doctors .search-listing").html(data.str);
                    $("#search-doctors .search-drop").show();
                    $(".search-drop").css({"margin":"0", "z-index":"110"});
                }
                else alert("Ничего не найдено");
            }, "json");
        }
        else alert("Введите минимум 3 символа");
        return false;
    });
    $(".input_search_doctor").keyup(function(){
        var tags = "";
        var $input_search = $(this);
        var txt = $input_search.val();
        var txt_tmp = $input_search.data("val");
        if ((txt.length>2) && (txt!=txt_tmp)) {
            $("#search-doctors .search-list span").each(function(){
                tags = tags + $(this).html() + ",";
            });
            tags = tags.substr(0,tags.length-1);
            $.post("/ajax/searchDoctors.php", {txt:txt, tags:tags}, function(data){
                if (data.count>0){
                    if (data.count*40>198) $("#search-doctors .scrollpane").css("height", "198px");
                    else $("#search-doctors .scrollpane").css("height", data.count*40+"px");
                    $("#search-doctors .search-listing").html(data.str);
                    $("#search-doctors .search-drop").show();
                    $(".search-drop").css({"margin":"0", "z-index":"110"});
                }
                else $("#search-doctors .search-drop").hide();
            }, "json");
        }
        else $("#search-doctors .search-drop").hide();
        return false;
    });

    $("body").on("click", "#search-doctors #search_result a", function(){
        var txt = $(this).find("span").html();
        $("#search-doctors .search-input input:text").val(txt);
        /*var str = '<li><span class="title">' + txt + '</span><a href="#" class="remove-button">remove</a></li>';
        $("#search-doctors .search-list").append(str);
        $("#search-doctors .search-input input:text").val("");*/
        $("#search-doctors .search-drop").hide();
        $("#search-doctors .search-listing").html("");
        return false;
    });

    $("#search-doctors form").submit(function(){
        var tags = ""; var service = "";
        $("#search-doctors .search-list span").each(function(){
            tags = tags + $(this).html() + ",";
        });
        tags = tags.substr(0,tags.length-1);

        window.location.href = '?tags=' + tags + '&' + $.param($(this).serializeArray());
        return false;
    });


    $(".popup-frame .button09").click(function(){
        $(".search-drop").css({"margin":"0 0 0 -9999px", "z-index":"auto"});
        $(this).closest(".search-sector").find(".search-drop").css({"margin":"0", "z-index":"100"});
        return false;
    });

    $("body").on("click", ".popup-frame .search-listing a", function(){
        $(this).closest(".search-sector").find(".search-input input:text").val($(this).find("span").html());
        $(this).closest(".search-sector").find(".search-input input:text").change();
        $(this).closest(".search-sector").find(".search-input input:text").addClass("text-active");
        $(this).closest(".search-drop").css({"margin":"0 0 0 -9999px", "z-index":"auto"});
        return false;
    });

    $(".popup-frame").click(function(){
        $(".search-drop").css({"margin":"0 0 0 -9999px", "z-index":"auto"});
    });
    $("body").click(function(){
        $(".search-drop").css({"margin":"0 0 0 -9999px", "z-index":"auto"});
    });

    $("body").on("click", ".link_reception_doc, .js-receiption_doctors_btn, .link_reception_clinic", function(){
        $.ajax({
            url: "/ajax/zapis_na_priem.php",
            data: {CLINIC: $(this).data('clinic'), DOCTOR: $(this).data('doctor')},
            context: $('#zapisnapriem-popup')
        }).done(handleAjaxPopup);
        return false;
    });

    $("body").on("change", ".clinic_appoint", function(){
        var name = $(this).val();
        $(".service_appoint").closest(".search-sector").find("input:text").val("Любое направление");
        $.post("/ajax/getServiceClinic.php", {name:name}, function(data){
            if (data.count*40>198) $(".service_appoint").css("height", "198px");
            else $(".service_appoint").css("height", data.count*40+"px");
            $(".service_appoint").html(data.str);
        }, "json");
    });


    $(".bt_search_article").click(function(){
        var tags = "";
        var $input_search = $("#search-articles .search-input input:text");
        var txt = $input_search.val();
        var txt_tmp = $input_search.data("val");
        if ((txt.length>2) && (txt!=txt_tmp)) {
            $("#search-articles .search-list span").each(function(){
                tags = tags + $(this).html() + ",";
            });
            tags = tags.substr(0,tags.length-1);
            $.post("/ajax/searchArticles.php", {txt:txt, tags:tags}, function(data){
                if (data.count>0){
                    if (data.count*40>198) $("#search-articles .scrollpane").css("height", "198px");
                    else $("#search-articles .scrollpane").css("height", data.count*40+"px");
                    $("#search-articles .search-listing").html(data.str);
                    $("#search-articles .search-drop").show();
                    $(".search-drop").css({"margin":"0", "z-index":"100"});
                }
                else alert("Ничего не найдено");
            }, "json");
        }
        else alert("Введите минимум 3 символа");
        return false;
    });
    $(".input_search_article").keyup(function(){
        var tags = "";
        var $input_search = $(this);
        var txt = $input_search.val();
        var txt_tmp = $input_search.data("val");
        if ((txt.length>2) && (txt!=txt_tmp)) {
            $("#search-articles .search-list span").each(function(){
                tags = tags + $(this).html() + ",";
            });
            tags = tags.substr(0,tags.length-1);
            $.post("/ajax/searchArticles.php", {txt:txt, tags:tags}, function(data){
                if (data.count>0){
                    if (data.count*40>198) $("#search-articles .scrollpane").css("height", "198px");
                    else $("#search-articles .scrollpane").css("height", data.count*40+"px");
                    $("#search-articles .search-listing").html(data.str);
                    $("#search-articles .search-drop").show();
                    $(".search-drop").css({"margin":"0", "z-index":"100"});
                }
                else $("#search-articles .search-drop").hide();
            }, "json");
        }
        else $("#search-articles .search-drop").hide();
        return false;
    });

    $("body").on("click", "#search-articles #search_result a", function(){
        var txt = $(this).find("span").html();
        var str = '<li><span class="title">' + txt + '</span><a href="#" class="remove-button">remove</a></li>';
        $("#search-articles .search-list").append(str);
        $("#search-articles .search-input input:text").val("");
        $("#search-articles .search-drop").hide();
        $("#search-articles .search-listing").html("");
        return false;
    });

    $("#search-articles input:submit").click(function(){
        var tags = ""; var service = "";
        $("#search-articles .search-list span").each(function(){
            tags = tags + $(this).html() + ",";
        });
        tags = tags.substr(0,tags.length-1);

        window.location.href = '?tags=' + tags + '&text='+$('.input_search_article').val();
        return false;
    });


    $('.fancybox').fancybox({
        helpers     : {
            title   : { type : 'over' }
        }
    });

    $('.fancybox-web').fancybox({
        helpers: {
            title: null
        }
    });

    $('.fancybox-service-detail').fancybox({
        cyclic: true,
        showNavArrows: true,
        helpers: {
            title: null
        }
    });

    $(".fancybox-map").click(function(e)
    {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            data: {}
        }).done(function(data) {
            $.fancybox(data);
        });

        return false;
    });

    var popupHolderWindow = $(window).height();

    /*console.log(popupHolderWindow);*/

    var menuWidth = $('.tabcontrols-holder.fixed-holder ul.tabcontrols').width();
    var menuSize = $('.tabcontrols-holder.fixed-holder ul.tabcontrols>li').size();

    $('.tabcontrols-holder.fixed-holder .tabcontrols > li > a').css({'width':menuWidth/menuSize});

    $(document).on('click', '.service-tree-menu i', function()
    {
        $(this).parent().toggleClass('collapse');
        $(this).parent().find('ul:first').slideToggle();
    });

    if( $('#header .header_more_phone').length>0 ){
        $('#header').addClass('more-phone-head');
    }

    jQuery('.search-block input[placeholder], .subscribe-block input[placeholder], .form-rows input[placeholder], .subs-edit-form input[placeholder]').placeholder();

    if (document.cookie.indexOf(CITY_COOKIE_NAME) < 0) {
        $('.change-link').click();
    }

    $('body').on('click', '.ya-event', function() {
        var name = $(this).data('ya-event');
        if (!name) return;

        if (typeof window.reachGoal !== 'function') {
            return;
        }

        window.reachGoal(name);
    });

    var phoneReplaces = {};

    function tokenizeNumber(number) {
        return number.replace(/(^\s*\+\d|\-+|\s+|\(|\))/g, '');
    }
/*
    function parsePhone(phone) {
        var parts = [], token = $(phone.id).first().text();

        if (/^\d{4}/.test(phone.text)) {
            phone.text = phone.text.replace(/^(\d{1})(\d{3})(.+)/, function(s, country, city, number) {
                return '+' + country + ' (' + city + ') ' + number;
            });
        }

        phone.text = phone.text.replace(/\-+/g, ' ');

        if (!token && phoneReplacements.hasOwnProperty(phone.id)) {
            token = tokenizeNumber(phoneReplacements[phone.id]);
        }

        if (token) {
            token = tokenizeNumber(token);
            phoneReplaces[token] = phone.text;
        }

        parts = phone.text.split(')');

        if (parts.length === 2) {
            phone.text = '<span>' + parts[0] + ')</span> <span class="highlight">'+parts[1]+'</span>';
        }

        return phone;
    }
*/
    window.__cs_onReplacePhones = function(phones) {
        var originalPhones = {};

        $('.js-clinic-phone').each(function(){
            var clinicId = $(this).data('clinic-id');

            if(clinicId){
                originalPhones[clinicId] = $(this).text().trim().replace(/\s+/g, ' ');
            }
        });

        var comagicPhones = {};

        for(var i=0;i<phones.length;i++){
            if(phones[i].text){
                var clinicId = $(phones[i].id).first().closest('.js-clinic-phone').data('clinic-id');

                if(clinicId){
                    comagicPhones[clinicId] = $("<div/>").html(phones[i].text).text().trim().replace(/\s+/g, ' ');
                }
            }
        }

        $('.js-phone').each(function() {
            var clinicId = $(this).data('clinic-id') || window["CURRENT_CLINIC"];

            var originalPhone = $(this).text().trim().replace(/\s+/g, ' ');

            if(clinicId && tokenizeNumber(originalPhones[clinicId]) == tokenizeNumber(originalPhone) && comagicPhones[clinicId]){
                /*
                если для клиники есть номер замена, и номер, который мы хотим заменить равен тому,
                что заменили в шапке, то заменяем на новый comagic номер
                P.s на странице клиники может быть несколько номеров, поэтому меняем только нужные
                */
                $(this).html(comagicPhones[clinicId]);
            }
        });

        return true;
    };

    if (window.comagicId) {
        setTimeout(function() {
            window.__cs = [];
            __cs.push(["setAccount", window.comagicId]);
            __cs.push(["setHost", "//server.comagic.ru/comagic"]);

                var s;
            s = document.createElement('script');
            s.async=1;
            s.src='http://app.comagic.ru/static/cs.min.js';
            $('head').append(s);
        }, 1);
    }

    $('body').on('click', '.print-coupon, .js-print-page', function(e) {
        e.preventDefault();
        window.print();
    });

    if ($('.b-service_doctors-item').length) {
        var maxDoctorHeight = 0;

        $('.b-service_doctors-item').each(function() {
            var $this = $(this);
            if ($this.height() > maxDoctorHeight) {
                maxDoctorHeight = $this.innerHeight();
            }
        }).height(maxDoctorHeight);
    }

    if($.fn.bxSlider) {
        if($('li','.js-banner-slider').length > 1) {
            var blSliderMain = $('.js-banner-slider').bxSlider({
                mode: 'fade',
                controls: true,
                auto: true,
                autoHover: true,
                onSlideAfter: function(){
                    blSliderMain.startAuto();
                }
            });
        }

        $('.js-slider-carousel').each(function() {
            if (typeof $(this).data('countrow') != 'undefined' && $(this).data('countrow') < $(this).find('li').length) {
                $(this)
                    .removeClass('b-slider_no')
                    .bxSlider({
                        pager: false,
                        slideWidth: $(this).find('li'),
                        maxSlides: $(this).data('countrow') ? $(this).data('countrow') : 4
                });
            }
        });
    }


    if($.fn.customTabs) {
        var settingTabs = {
            triggers: '.trigger',
            tabs: '.section',
            mode: 'none'
        };
        var currentTab = $('body').find('.trigger.active');

        $('.tabs').customTabs(settingTabs);

        $('.social-tabs').customTabs($.extend('target',settingTabs,{triggers: '.switch',tabs: '.sect'}));

        currentTab.click();
        if ($('.tabs').length) {
            $('.tabs .triggers .row').each(function() {
                $el = $(this).find('.trigger');
                $el.width(100/$el.length+'%');
            });
        }
    };


    var ClinicLoader = function() {
        var self = this;
        var cache = {};
        self.hasCache = function(key) {
            return typeof cache[key] !== 'undefined';
        };
        self.getCache = function(key) {
            return cache[key];
        };
        self.setCache = function(key, data) {
            cache[key] = data;
            return self;
        };
        self.addCache = function(key, data) {
            if (!self.hasCache(key)) {
                cache[key] = data;
            }
            return self;
        };
        self.load = function(url, callback, data) {
            var promise, cacheKey;
            var $contentBlock = $('.js-clinic-content'),
                $clinicCard = $('.js-clinic-card');
            data = data || {};
            $contentBlock.css({opacity: 0.35});
            $clinicCard.addClass('l-loader');
            cacheKey = url + JSON.stringify(data);
            data.CLEAR_CACHE = 'Y';
            if (self.hasCache(cacheKey)) {
                promise = $.Deferred();
                promise.resolve(self.getCache(cacheKey));
            } else {
                promise = $.ajax({
                    type: 'GET',
                    url: url,
                    data: data,
                });
            }

            promise.done(function(response) {
                $contentBlock.css({opacity: 1});
                $clinicCard.removeClass('l-loader');

                self.updateState(url);
                self.addCache(cacheKey, response);
                callback(response);
            });

        };
        self.updateState = function(url) {
            if (history.pushState) {
                history.pushState({url: url}, document.title, url);
            }
        };

        return self;
    };

    var ClinicPage = function(loader) {
        var self = this;
        self.afterUpdate = function() {
            self.initSocialTabs();
           try {
                FB.XFBML.parse();
            } catch(ex) {
                console.log(ex);
            }
            self.initMenu();
            self.initFancy();
            self.initClinicText();
            self.initPriceList();

            handleDoctorsTab();
            $('.js-popups input, .js-popups select, .js-wrap input, .js-wrap select').styler();
            self.initProgramList();
            self.initCarousel();
        };

        self.getCurrentTabUrl = function() {
            var $currentTab = $('.js-tab.active');
            if (!$currentTab.size()) {
                return false;
            }
            return $currentTab.attr('href');
        }

        self.initCarousel = function() {
            $('.js-slider-carousel').each(function() {
                if (typeof $(this).data('countrow') != 'undefined' && $(this).data('countrow') < $(this).find('li').length) {
                    $(this)
                        .removeClass('b-slider_no')
                        .bxSlider({
                            pager: false,
                            slideWidth: $(this).find('li'),
                            maxSlides: $(this).data('countrow') ? $(this).data('countrow') : 4
                    });
                }
            });
        };

        self.initPriceList = function() {
        };

        self.initProgramList = function() {
            $('.b-clinic_program__select').change(function() {
                var directionId = $(this).find(':selected').val();
                loader.load(self.getCurrentTabUrl(), self.update, {
                    DIRECTION: directionId
                }, false)
            });
        };

        self.initFancy = function() {
            $('.fancybox-service-detail').fancybox({
                cyclic: true,
                showNavArrows: true,
                helpers: {
                    title: null
                }
            });
        };

        self.initClinicText = function() {

            var $cutLessButton = $('.js-clinic-text-cut-less'),
                $cutMoreButton = $('.js-clinic-text-cut-more'),
                $cutServiceText = $('.js-clinic-text-cut');
            
            $cutLessButton.hide();

            if ($cutServiceText.height() > 240) {
                $cutServiceText.addClass('b-clinic_card__text-cut');
                $cutMoreButton.show();
            }

            $cutMoreButton.click(function() {
                $(this).hide();
                $cutServiceText.removeClass('b-clinic_card__text-cut');
                $cutLessButton.show();
            });

            $cutLessButton.click(function() {
                $(this).hide();
                $cutServiceText.addClass('b-clinic_card__text-cut');
                $cutMoreButton.show();
            });

            /*var $cutLessButton = $('.js-clinic-text-cut-less'),
                $cutMoreButton = $('.js-clinic-text-cut-more'),
                initDotDotDot = function() {
                    $cutLessButton.hide();
                    $cutMoreButton.show();
                    $(".js-clinic-text-cut").dotdotdot({
                        after: ".js-clinic-text-cut-more",
                        height: 240,
                        callback: function (isTruncated) {
                            if (isTruncated) {
                                $('.js-clinic-text-cut-more').click(function (e) {
                                    e.preventDefault();
                                    $('.js-clinic-text-cut').trigger('destroy.dot');
                                    $cutLessButton.show();
                                    $cutMoreButton.hide();
                                });
                            } else {
                                $('.js-clinic-text-cut-more').hide();
                            }
                        }
                    });
                    }

            $cutLessButton.click(function() {
                initDotDotDot();
            });

            initDotDotDot();

            $(window).load(function() {
               $(".js-clinic-text-cut").trigger( "update.dot" );
            });*/

        };
        self.initSocialTabs = function() {

            $('.b-clinic_social__tab').click(function() {
                if ($(this).hasClass('b-clinic_social__tab-active')) return;

                var $this = $(this);

                if ($this.data('url')) {
                    window.open($this.data('url'), '_blank');
                    return;
                }

                $parent = $this.parents('.b-clinic_social__wrap');

                $parent.find('.b-clinic_social__tab-active').removeClass('b-clinic_social__tab-active');
                $this.addClass('b-clinic_social__tab-active');

                $parent.find('.b-clinic_social__item-active').removeClass('b-clinic_social__item-active');
                $parent.find('.b-clinic_social__item[data-id='+$this.data('id')+']').addClass('b-clinic_social__item-active');
            });
        };
        self.initMenu = function() {
            $('.js-tab').each(function() {
                var $this = $(this);
                if ($this.data('initialized')) {
                    return;
                }
                $this.click(function(e) {
                    e.preventDefault();
                    var $this = $(this);
                    if ($this.hasClass('active')) {
                        return;
                    }
                    var url = $this.prop('href');

                    $('body, html').animate({scrollTop: $('.js-clinic-content').offset().top - 130}, 300);
                    loader.load(url, self.update);
                });
                $this.data('initalized', true);
            });
        };

        self.update = function(data) {
            var $data = $('<div></div>');
            $data.html(data);
            var $title = $data.find('title');


            $data.find('[data-ajax-zone]').each(function() {
                var $ajaxZone = $(this),
                    ajaxZoneName = $ajaxZone.data('ajaxZone'),
                    ajaxZoneKey = $ajaxZone.data('ajaxKey');
                var $content = $('[data-ajax-zone=' + ajaxZoneName + ']');
                $content.each(function() {
                    var $this = $(this),
                        contentZoneKey = $this.data('ajaxKey');
                    if (!ajaxZoneKey || !contentZoneKey || ajaxZoneKey !== contentZoneKey) {
                        $this.html($ajaxZone.html());
                        $this.data('ajaxKey', ajaxZoneKey);
                    }
                });
            });

            if ($title.length) {
                $(document).find('head > title').text($title.text());
            }


            self.afterUpdate();
        };
        self.init = function() {
            self.initMenu();
            self.initSocialTabs();
            self.initClinicText();
            self.initProgramList();
        };
        self.init();
        return self;
    };
    if ($('.js-clinic-card').length) {
        var clinicPage = new ClinicPage(new ClinicLoader);
    }

    var urlCache = {};
    loadClinicUrl = function(url, noHistory) {
        var $currentTab = $('.clinics-tabs .section:visible'),

        callback = function(data) {
            var content;

            urlCache[url] = data;

            if (url.indexOf('ajax_filter') >= 0) {
                if (url.indexOf('PAGEN_2') < 0) {
                    data = '<div>' + data + '</div>';
                }
                content = $(data).find('.ajax_section').html();

                $currentTab.find('.ajax_section').html(content);
            } else {
                content = $(data).find('.clinics-tabs .section').html();

                $currentTab.html(content);

                if ($(data).find('.clinics-tabs .triggers').length) {
                    $('body').find('.clinics-tabs .triggers').html($(data).find('.clinics-tabs .triggers'));
                }

                //document.title = $(data).find('.clinics-tabs').data('title');
                if (!noHistory && history.pushState) {
                    history.pushState({url: url}, document.title, url);
                }
            }

            $currentTab
                .css({opacity: 1})
                .find('input, select')
                    .styler();

            handleDoctorsTab();

            $('body, html').animate({scrollTop: $('.content-block').position().top}, 300);
        };

        $currentTab.css({opacity: 0.3});

        var promise = null;
        if (typeof urlCache[url] !== 'undefined') {
            promise = $.Deferred();
            promise.resolve(urlCache[url]);
        } else {
            promise = $.ajax({
                type: 'GET',
                url: url
            });
        }

        promise.done(callback);
    };

    $('.clinics-tabs').on('click', '.paging-block a', function(e) {
        e.preventDefault();

        loadClinicUrl($(this).attr('href'));
    });

    if (history.pushState && location.href.indexOf('/clinics/') >= 0) {
        window.addEventListener('popstate', function(e) {
            loadClinicUrl(location.href, true);
        }, false);
    }

    $('.mobile-panel-phone, .mobile-panel-phone-select a').on(touchStart, function() {
        $(this).addClass('hover-effect');
    });
    $('.mobile-panel-phone, .mobile-panel-phone-select a').on(touchEnd, function() {
        $(this).removeClass('hover-effect');
    });
    $('.mobile-panel-click').on('click', function() {
        $('.mobile-panel-phone-select, .mobile-panel-overlay').show();
    });
    $('.mobile-panel-phone-select a').click(function() {
        $('.mobile-panel-phone-select, .mobile-panel-overlay').hide();
        $('.mobile-panel-phone-select a.hover-effect').removeClass('hover-effect');
    });

    $('.mobile-panel-overlay').on('click',function() {
        $('.mobile-panel-phone-select, .mobile-panel-overlay').hide();
    });

    $('.js-search-tab').on('click',function() {
        if ($(this).hasClass('js-search-tab-active')) {
            return;
        }
        $('.js-search-tab').removeClass('js-search-tab-active');
        $(this).addClass('js-search-tab-active');
        $('#where_search_field').val($(this).attr('data-id'));
        $('#search_form_md').submit();
    });

    var timeoutMenuTop = -1;
    $('.b-menu_top__box').hover(
        function() {
            $this = $(this);
            $subMenu = $this.find('.b-menu_sub__wrap');
            widthCont = $('.l-width:first').width();
            widthDocument = $('.l-wrapper').width();

            clearTimeout(timeoutMenuTop);

            if ($('.js-menu-top-active').length) {
                $('.js-menu-top-active .b-menu_sub__wrap').hide();
                $('.js-menu-top-active').removeClass('js-menu-top-active b-menu_top__hover');
            }

            $this.addClass('js-menu-top-active b-menu_top__hover');

            leftMargin = $this.position().left;

            $subMenu.show();

            if (($subMenu.width() + leftMargin) > widthCont) {
                $subMenu.css({left: '-' + (($subMenu.width() + leftMargin) - widthCont) + 'px'});
            }
        },
        function() {
            timeoutMenuTop = setTimeout(function() {
                $subMenu = $this.find('.b-menu_sub__wrap');
                $this.removeClass('js-menu-top-active b-menu_top__hover');
                $subMenu.hide();
            } , 500);
        }
    );

    $('.js-news-switch').click(function() {
        $this = $(this);
        idx = $this.data('id');

        $this.hide();
        $('.js-news-switch[data-id!='+idx+']').show();

        $('.js-news').hide();
        $('.js-news[data-id!='+idx+']').show();
    });

    $('.js-show-services').click(function(e) {
        var $this = $(this);

        $this.closest('.js-services').find('.js-hidden-services').show();
        $this.hide();

        e.preventDefault();
    });

    (function() {
        function submitForm()
        {
            var reviewsForm = BX('REVIEWS_FORM');

            BX.ajax.submitComponentForm(reviewsForm, 'reviews_form_content', true);
            BX.submit(reviewsForm);

            return true;
        }

        function refreshForm()
        {
            $('form#REVIEWS_FORM select').change(function()
            {
                submitForm();
            }
            );
        }

        BX.addCustomEvent("onAjaxSuccess", refreshForm);

        refreshForm();
    })();

    if ($(".js-service-text-cut").length) {
        var $cutLessButton = $('.js-service-text-cut-less'),
            $cutMoreButton = $('.js-service-text-cut-more'),
            $cutServiceText = $(".js-service-text-cut"),
            $skypeConsult = $('.js-skype-consult'),
            $bannerEKO = $('.js-banner-eko'),
            $serviceSidebar = $('.js-service-sidebar'),
            heightService = 0;

        if ($cutServiceText.height() > 300 && !$skypeConsult.length && !$bannerEKO.length) {
            heightService = 300;
            $cutServiceText.css('height', heightService);
        }

        if ($cutServiceText.height() < 300) {
            heightService = 1;
        }

        if (heightService == 0) {
            heightService = $serviceSidebar.height()+ 40;
            $cutServiceText.css('height', heightService);
        }

        if (heightService > 1) {
            $cutMoreButton.show();
            $cutServiceText.addClass('b-service_about__text--cut');
        }

        $cutMoreButton.click(function() {
            $(this).hide();
            $cutServiceText.removeClass('b-service_about__text--cut').css('height', 'auto');
            $cutLessButton.show();
        });

        $cutLessButton.click(function() {
            $(this).hide();
            $cutServiceText.addClass('b-service_about__text--cut').css('height', heightService+'px');
            $cutMoreButton.show();
        });
    }

    $('.js-price-show-all').click(function() {
        $(this).slideUp(300);
        $('.js-price-hidden').slideDown(300);
    });

    $('.js-program-show-all').click(function() {
        var $this = $(this), $title = $this.find('.b-link_more'), oldTitle= $title.html();

        $title
            .html($this.data('toggle-title'))

        $this
            .data('toggle-title', oldTitle);

        $('.js-program-hidden').slideToggle(300);
    });

    $('.js-article-show-all').click(function() {
        var $this = $(this), $title = $this.find('.b-link_more'), oldTitle= $title.html();

        $title
            .html($this.data('toggle-title'))

        $this
            .data('toggle-title', oldTitle);

        $('.js-article-hidden').slideToggle(300);
    });

    $('.js-show-oms-clinics').mouseover(function() {
        $(this).hide();
        $('.js-list-oms-clinics').show();
        if ($('.js-banner-eko').length) {
            $('.js-service-text-cut-more').click();
        }
    });

    $("body").on("click", ".js-tree-click", function(){
        $this = $(this);
        $parent = $this.parents('.b-tree_link__item:first');

        $this.toggleClass('b-tree_link__plus-active');

        $parent.find('.b-tree_links__sub:first').stop(true,true).slideToggle(300);

        if ($this.hasClass('b-tree_link__plus-active')) {
            $this.html('&ndash;');
        } else {
            $this.text('+');
        }
    });

    $("body").on("click", ".js-price-click", function(){
        $this = $(this);
        $parent = $this.parents('.b-tree_link__item:first');

        $this.toggleClass('b-tree_link__plus-active');

        $parent.find('.b-tree_links__sub:first').stop(true,true).slideToggle(300);

        if ($this.hasClass('b-tree_link__plus-active')) {
            $this.find('.js-tree-plus').html('&ndash;');
        } else {
            $this.find('.js-tree-plus').text('+');
        }
    });

    $("body").on("click", ".js-menu-click", function(){
        $this = $(this);
        $parent = $this.parent();

        $this.toggleClass('b-menu_item__plusminus-active');

        $parent.find('.js-menu-toggle:first').stop(true,true).slideToggle(300);

        if ($this.hasClass('b-menu_item__plusminus-active')) {
            $this.html('&ndash;');
        } else {
            $this.text('+');
        }
    });

    $('.js-clinic_doctors__description').hideDoctorServices();
    $(document).ajaxComplete(function() {
        $('.js-clinic_doctors__description').hideDoctorServices();
    });

    $('body').on('click', '.js-goal', function() {
        if (typeof window.reachGoal !== 'function') {
            return;
        }

        var $this = $(this);
        if (!$this.data('target')) {
            return;
        }

        window.reachGoal($this.data('target'));
    });

    $('body').on('click', '.js-gasend', function() {
        if (typeof window.ga !== 'function') {
            return;
        }

        var $this = $(this);	  
        if (!$this.data('targetga')) {
            return;
        }
	  var params = $this.data('targetga').split('/');
	  
	  if(params.length) window.ga('send',(!!params[0] ? params[0] : ''),(!!params[1] ? params[1] : ''),(!!params[2] ? params[2] : ''),(!!params[3] ? params[4] : ''));
    });
    
    $('.js-tooltip').tooltipster();

    $('body').on('click', '.js-show-popup-14may-event', function(e) {
        e.preventDefault();
        loadPopupForm('/ajax/fourteenMayEvent.php', 'fourteen-may-event-popup');
    });

    $('body').on('click', '.js-show-popup-prorody-event', function(e) {
        e.preventDefault();
        $that = $(this);
        params = {newsId: $that.parents('.js-element-wrap').data('id')};
        if ($(this).hasClass('mail-event-repalce')) {
            params = $.extend(params, {eventReplace: $that.attr('class').replace(/\D/g,'')});
        }
        loadPopupForm('/ajax/prorody.php', 'fourteen-may-event-popup', params);
    });

    $('body').on('click', '.js-show-popup-14may-contest', function(e) {
        e.preventDefault();
        loadPopupForm('/ajax/fourteenMayCompetition.php', 'fourteen-may-competition-popup');
    });

    $('body').on('click', '.js-show-popup-form-eko', function(e) {
        e.preventDefault();
        var url = '/ajax/formEko.php';
        if (typeof CURRENT_CLINIC !== 'undefined') {
            url += '?clinic=' + CURRENT_CLINIC;
        }
        loadPopupForm(url, 'form-eko-popup');
    });

    $('body').on('click', '.js-show-popup-form-eko-26nov', function(e) {
        e.preventDefault();
        var url = '/ajax/formEkoAnketa.php';
        
        loadPopupForm(url, 'form-eko-popup');
    });

    $('body').on('click', '.js-show-popup-form-primapur', function(e) {
        e.preventDefault();
        var url = '/ajax/formPrimapur.php';

        loadPopupForm(url, 'form-eko-popup');
    });

    $('body').on('click', '.js-show-popup-form-pregnant', function(e) {
        e.preventDefault();
        var url = '/ajax/formPregnant.php';

        loadPopupForm(url, 'form-eko-popup');
    });

    $('body').on('click', '.js-show-popup-form-cikl', function(e) {
        e.preventDefault();
        var url = '/ajax/formCikl.php';
        /*
        if (typeof CURRENT_CLINIC !== 'undefined') {
            url += '?clinic=' + CURRENT_CLINIC;
        }
        */
        loadPopupForm(url, 'form-cikl-popup');
    });

    $('body').on('click', '.js-show-popup-custom-request', function(e) {
        e.preventDefault();
        loadPopupForm('/ajax/customRequest.php', 'custom-request-popup');
    });

    $('body').on('click', '.js-show-popup-open-day-spb', function(e) {
        e.preventDefault();
        loadPopupForm('/ajax/openDaySpb.php', 'open-day-spb');
    });

    $('body').on('click', '.js-show-form-register-conf', function(e) {
        e.preventDefault();
        loadPopupForm('/ajax/registerConf.php', 'form-register-conf');
    });

    $('.js-callback-fixed').click(function() {
        $('.header .js-show-call-me').click();
    });

    $('.js-scroll-top').click(function() {
        $('body, html').animate({scrollTop: 0}, 300);
    });

    flagOpenPopupClinic = false;

    $('.js-main-clinic-hover, .js-main-clinic-popup').hover(
        function() {
            flagOpenPopupClinic = true;
        },
        function() {
            flagOpenPopupClinic = false;
        }
    );

    $('.js-main-clinic-hover').click(function(){
        $this = $(this);
        if ($this.hasClass('b-clincs_main__pic-open')) return;

        var $popup = $this.parents('.js-main-clinic-item').find('.js-main-clinic-popup');
        $('.b-clincs_main__pic-open').removeClass('b-clincs_main__pic-open');
        $('.js-main-clinic-popup:visible').animate({top: ($(this).data('topstart') || 210), opacity: 0}, 100, function() {$(this).hide()});

        $('html, body').animate({scrollTop: $this.offset().top - 20}, 300);

        $this.addClass('b-clincs_main__pic-open');

        $popup.show().animate({top: ($popup.data('topend') || 190), opacity: 1}, 200);
    });

    $(document).click(function() {
        if (flagOpenPopupClinic) return;
        $('.b-clincs_main__pic-open').removeClass('b-clincs_main__pic-open');
        $('.js-main-clinic-popup').animate({top: ($('.js-main-clinic-popup').data('topstart') || 210), opacity: 0}, 100, function() {$(this).hide()});
    });

    $('.js-main-clinic-popup-close').click(function() {
        $popup = $(this).parents('.js-main-clinic-popup');
        $('.b-clincs_main__pic-open').removeClass('b-clincs_main__pic-open');
        $popup.animate({top: ($popup.data('topstart') || 210), opacity: 0}, 100, function() {$(this).hide()});
    });

});

function handleAjaxPopup(data){
    $this = $(this);
    $this.html(data);
    $this.show().find('.popup').show();
    centrateDiv($this);
}

function closePopup(data){
    $('.popup-holder').hide();
}

function loadPopupForm(url, idx, params) {
    var
        toTop = $(window).scrollTop(),
        contextSelector = '#' + idx;

    $.ajax({
        url: url,
        context: $(contextSelector),
        data: params
    }).done(handleAjaxPopup).done(function() {
        var $this = $(this);

        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false
        };

        $.datepicker.setDefaults($.datepicker.regional['ru']);

        $this.find('.js-phone-input').mask('8(999) 999-99-99');
        $this.find('.js-calendar-click').click(function () {
            $(this).find('.js-calendar-input').focus();
        });

        $this.find('.js-time-input').each(function () {
            $(this).mask('99:99');
        });

        $this.find('.js-calendar-input').each(function () {
            var $this = $(this);
            $this.datepicker({
                minDate: 2
            });
        });

        $this.find('input, select').styler();

    });
}


function centrateDiv($this) {
    var popupW = $this.find('.popup').width();
    var popupH = $this.find('.popup-sector').height();
    var positionTop = popupH > $(window).height()  ? $(document).scrollTop() : ($(window).height() - popupH)/2 + $(document).scrollTop();
    $this.css('top', $this.attr('id') == 'city-popup' ? 50 : positionTop);
    $this.find('.popup').css('margin-left','-'+popupW/2+'px');
}



function refreshCaptcha(el){
    $.getJSON('/ajax/refreshCaptcha.php', function(data) {
        var $form = $(el).closest('form');
        $form.find('.js-captcha_sid').val(data);
        $form.find('.js-captcha_img').attr('src','/bitrix/tools/captcha.php?captcha_sid=' + data);
    });
}

(function($) {
    function hideDoctorServices($parent, $children, options) {
        if ($parent.data('hide-doctor-services')) {
            return;
        }
        $parent.data('hide-doctor-services', true);

        var childrenSize = $children.size();
        if (childrenSize > options.countVisible) {

            var $showButton = $('<div class="b-clinic_doctors__more">').text('...показать еще ' + (childrenSize - options.countVisible));
            $children.filter(':gt(' + (options.countVisible - 1) + ')').hide();
            $showButton.click(function() {
                $children.show();
                $showButton.remove();
            });
            $parent.append($showButton);
        }
    }

    $.fn.hideDoctorServices = function(options) {
        var defaultOptions = {
            countVisible: 5,
            parentSelector: '.js-clinic_doctors__service-list',
            childSelector: '.js-clinic_doctors__service-value'
        };
        options = $.extend({}, defaultOptions, options);
        this.each(function() {
            var $parent = $(this).find(options.parentSelector);
            var $children = $parent.find(options.childSelector);
            hideDoctorServices($parent, $children, options);
        });
        return this;
    };
})(jQuery);

(function($) {

    function getAjaxFormContainerResponseHandler($container) {
        return function(response) {
            var $oldForm = $container.find('form');
            $oldForm.off();
            $container.html(response);
            var $form = $container.find('form');
            if ($form.size() !== 1) {
                return;
            }

            $container.find('input, select').styler();

            $form.submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method') || 'POST',
                    data: $form.serialize()
                }).done(getAjaxFormContainerResponseHandler($container));
            });
        };
    }

    $(function() {
        $('.js-ajax-form-container').each(function() {
            var $container = $(this),
                source = $container.data('ajax-source'),
                $form;
            if (!source) {
                return;
            }
            $.ajax({
                url: source,
                type: 'GET'
            }).done(getAjaxFormContainerResponseHandler($container));
        });
    });
})(jQuery);