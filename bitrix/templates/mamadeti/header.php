<?php
    $CITY_ID = GetCurrentCity();

    foreach($_GET AS $key => $value){
        if(strpos($key, 'PAGEN_') !== false){
            setCanonicalPage($APPLICATION->GetCurPage(false));
            break;
        }
    }
?><!DOCTYPE html>
<html class="nojs">
<head>

    <title><?=$APPLICATION->ShowTitle()?></title>
    <?=$APPLICATION->ShowHead()?>
    <meta name='yandex-verification' content='5db75fec1fece87f' />
    <link rel="shortcut icon" href="/favicon.ico?2" type="image/x-icon">

    <?=$APPLICATION->SetAdditionalCSS("http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic")?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/social-likes_flat.css")?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.fancybox.css")?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.formstyler.css")?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/tooltipster.css")?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/all.css")?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/new.css")?>

    <script>
        document.documentElement.className = document.documentElement.className.replace("nojs","js");
    </script>

    <script src="http://api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU"></script>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-1.9.0.min.js'); ?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-ui.min.js'); ?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js'); ?>
    <script>
        (function (i,s,o,g,r,a,m) {i['GoogleAnalyticsObject']=r;i[r]=i[r]||function () {
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-56244691-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
        var phoneReplacements = <?php echo getReplacementPhones($CITY_ID); ?>;
        var comagicId = <?php echo getComagicId($CITY_ID); ?>;
    </script>
</head>

<body<?php if (IS_MOBILE && !IS_TABLET) { ?> class="mobile-version"<?php } ?>>
<?php if (CityManager::getInstance()->getCurrentCity()['CODE'] === 'ufa'): ?>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KVKZD9"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KVKZD9');</script>
    <!-- End Google Tag Manager -->    
<?php endif; ?>
<div id="change-region-url" data-url="<?=$APPLICATION->ShowProperty('CHANGE-REGION-URL',$APPLICATION->GetCurPageParam())?>"></div>
<?=$APPLICATION->ShowPanel()?>
<div class="l-wrapper js-wrap">
    <div class="header">
        <div class="l-width">
            <div class="header__content">
                <div class="header__vertical header__vertical--empty in-bl"></div>
                <div class="header__vertical in-bl">
                    <?php $APPLICATION->AddBufferContent('getCurrentCityPhone'); ?>
                </div>
                <strong class="header__logo"><a href="/" title="Группа компаний Мать и Дитя" alt="Группа компаний Мать и Дитя">Мать и дитя</a></strong>
                <div class="header__city">
                    <div class="header__city-current">
                        <?=GetNameCity($CITY_ID)?><div class="header__city-arr in-bl"></div>
                    </div>
                    <div class="header__city-popup">
                        <div class="header__city-content">
                            <?$APPLICATION->IncludeComponent(
                                "pdv:form.city",
                                "",
                                Array(),
                                false
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-top">
        <div class="l-width">
            <div class="menu-top__content">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:search.form",
                    "top",
                    Array(
                        "USE_SUGGEST" => "N",
                        "PAGE" => "#SITE_DIR#search/index.php"
                    ),
                false
                );?>
                <div class="menu-top__items">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "2",
                        "CHILD_MENU_TYPE" => "top_child",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => "",
                        ),
                        false
                    );?>
                </div>
            </div>
        </div>
    </div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "sect",
            "AREA_FILE_SUFFIX" => "banner_top_inc",
            "AREA_FILE_RECURSIVE" => "Y",
            "EDIT_TEMPLATE" => ""
        ),
        false
    );?>
    <div class="l-width">
        <div class="l-container">
            <div class="main-holder b-printable-can">
                <?php
                if ($APPLICATION->GetCurPage(false) != SITE_DIR):?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "bread",
                        Array(
                            "START_FROM" => "0",
                            "PATH" => "",
                            "SITE_ID" => "s1"
                        ),
                        false
                    );
                    ?>
                    <?php

                    if (!function_exists('__headerBlock')) {
                        function __headerBlock()
                        {
                            global $APPLICATION;

                            ob_start();

                            if ($APPLICATION->GetProperty('SHOW_LEFT_MENU','N') == 'Y' && ERROR_404 != 'Y') {
                                $APPLICATION->IncludeComponent("bitrix:menu", "left", Array(
                                    "ROOT_MENU_TYPE" => $APPLICATION->GetProperty('TYPE_LEFT_MENU','left'), // Тип меню для первого уровня
                                    "MENU_CACHE_TYPE" => "N",   // Тип кеширования
                                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                                    "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                                    "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                                    "MAX_LEVEL" => "2", // Уровень вложенности меню
                                    "CHILD_MENU_TYPE" => $APPLICATION->GetProperty('TYPE_LEFT_MENU','left'),    // Тип меню для остальных уровней
                                    "USE_EXT" => "Y",   // Подключать файлы с именами вида .тип_меню.menu_ext.php
                                    "DELAY" => "N", // Откладывать выполнение шаблона меню
                                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                                    ),
                                    false,
                                    array("HIDE_ICONS" => "Y")
                                );
                                echo '<div class="content-block-margin">';
                            }

                            if ($APPLICATION->GetProperty('CLINIC_MENU')) {
                                echo $APPLICATION->GetProperty('CLINIC_MENU');
                            }

                            if ($APPLICATION->GetProperty('CONTENT_BLOCK','Y') == 'Y' || ERROR_404 == 'Y') {
                                echo '<div class="content-block">';
                                echo '<div class="'.$APPLICATION->GetProperty('CONTENT_BLOCK_CLASS','content-sector').'">';
                            }

                            if ($APPLICATION->GetProperty('SHOW_LEFT_MENU','N') == 'Y' && ERROR_404 != 'Y') {

                                echo '<div class="content-col">';
                            }

                            if ($APPLICATION->GetProperty('SHOW_H1','Y') == 'Y' && $APPLICATION->GetTitle() || ERROR_404 == 'Y') {
                                $title = $APPLICATION->GetTitle();
                                echo '<h1>'.$title.'</h1>';
                            }

                            $html = ob_get_contents();
                            ob_end_clean();

                            return $html;
                        }
                    }

                    $APPLICATION->AddBufferContent('__headerBlock');
                    ?>
                <?endif?>
