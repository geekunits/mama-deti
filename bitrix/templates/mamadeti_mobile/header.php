<?php 
    $cm = CityManager::getInstance();
    $currentCityId = $cm->getCurrentCityId();
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <link href="/favicon.ico?3" rel="icon" type="image/x-icon" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="canonical" rel="http://<?php echo CityManager::getInstance()->getDomainByCityId(CityManager::getInstance()->getCurrentCityId(), false).$_SERVER['REQUEST_URI']; ?>">

    <?=$APPLICATION->SetAdditionalCSS('//fonts.googleapis.com/css?family=Roboto:300,700,400&amp;subset=latin,cyrillic,cyrillic-ext'); ?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/font-awesome.min.css'); ?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/normalize.css'); ?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/photoswipe/default-skin.css'); ?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/photoswipe/photoswipe.css'); ?>
    <?=$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/mobile.css'); ?>

    <?$APPLICATION->AddHeadScript('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'); ?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-migrate-1.2.1.min.js'); ?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/modernizr-2.6.2-respond-1.1.0.min.js'); ?>

    <title><?=$APPLICATION->ShowTitle()?></title>
    <?=$APPLICATION->ShowHead()?>
    
    <script>
        var phoneReplacements = <?php echo getReplacementPhones($currentCityId); ?>;
        var comagicId = <?php echo getComagicId($currentCityId); ?>;
    </script>
    <script>
        (function (i,s,o,g,r,a,m) {i['GoogleAnalyticsObject']=r;i[r]=i[r]||function () {
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-56244691-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    <?php if (CityManager::getInstance()->getCurrentCity()['CODE'] === 'ufa'): ?>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KVKZD9"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KVKZD9');</script>
        <!-- End Google Tag Manager -->    
    <?php endif; ?>
    <?=$APPLICATION->ShowPanel()?>
    <div class="wrapper">
        <div class="container">
            <div class="header js-header noselect">
                <div class="header__icon hover js-sidebar-icon js-hover">
                    <span class="header__icon-trigger">
                        <span class="header__icon-trigger__top"></span>
                        <span class="header__icon-trigger__middle"></span>
                        <span class="header__icon-trigger__bottom"></span>
                    </span>
                </div>
                
                <div class="header__city form-select js-hover js-select-wrap">
                    <div class="form-select__txt"><i class="fa fa-chevron-down"></i><span class="js-select-current"><?php echo $cm->getCurrentCity()['NAME']; ?></span></div>
                    <select class="js-select js-city-selector">
                        <?php foreach ($cm->getCities() as $city): ?>
                            <option value="http://<?php echo $cm->getDomainByCityId($city['ID']); ?>" <?php echo $city['ID'] == $cm->getCurrentCityId() ? 'selected' : ''; ?>><?php echo $city['NAME']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <a href="/" class="header__logo"></a>
            </div>

            <?php if ($APPLICATION->GetCurDir() !== '/' && $APPLICATION->GetCurDir() !== '/clinics/'): ?>
                <!-- СКРЫВАЕМ ДЛЯ ГЛАВНОЙ -->
                <h1 class="js-main-header">
                    <?php $APPLICATION->AddBufferContent(function() {
                        global $APPLICATION;
                        return $APPLICATION->GetPageProperty('h1') ?: $APPLICATION->GetTitle();
                    }); ?>
                </h1>
                <div class="content js-main-content">
                <!-- СКРЫВАЕМ ДЛЯ ГЛАВНОЙ -->
            <?php endif; ?>

            <!-- END HEADER -->
