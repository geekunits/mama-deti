
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;"> 
  <tbody> 
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Перинатальный Медицинский Центр &laquo;Мать и дитя&raquo;</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">Севастопольский проспект 24, корпус 1.</span></p>
           
            <p><strong>Эл. почта: </strong>info@mospmc.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(495) 331 - 85 - 10</span> - Информационный центр</li>
             
              <li><span itemprop="telephone">(495) 331 - 85 - 10</span> - Взрослая регистратура</li>
             
              <li><span itemprop="telephone">(495) 718 - 67 - 67</span> - Реанимация новорожденных</li>
             
              <li><span itemprop="telephone">(495) 331 - 16 - 01</span> - Приемное отделение</li>
             
              <li><span itemprop="telephone">(495) 331 - 45 - 27</span> - Отделение лечения бесплодия и ЭКО</li>
             
              <li><span itemprop="telephone">(495) 331 - 85 - 10</span> - Детская регистратура</li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/moscow/perinatal-medical-centre-pmc/" ><img src="/upload/iblock/a1b/a1b3ce346c42a737bafc084a1a23e40d_thumb_ea697b56323adb01fddb018b87f00df5.JPG"  /></a> <a style="color:#d73612;" href="/clinics/moscow/perinatal-medical-centre-pmc/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клинический Госпиталь Лапино «Мать и дитя»</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Московская область</span>, <span itemprop="streetAddress">Одинцовский район, 1-е Успенское шоссе, Лапино, 111</span></p>
           
            <p><strong>Эл. почта: </strong>lpn.info@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(495) 526 60 60</span> - Регистратура</li>
             
              <li><span itemprop="telephone">(499) 372 24 95</span> - Запись на прием, многоканальный</li>
             
              <li><span itemprop="telephone">(495) 526 60 50</span> - Приемное отделение, скорая помощь, травмпункт, круглосуточно</li>
             
              <li><span itemprop="telephone">(495) 526 60 79</span> - Аптека </li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/moscow/clinical-hospital-lapino/" ><img src="/upload/iblock/b8a/b8a0d4b987467e571a6daa093038c106_thumb_b157b7b6ed6c01fb2f408f463a141872.jpg"  /></a> <a style="color:#d73612;" href="/clinics/moscow/clinical-hospital-lapino/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Савеловская</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">ул. Бутырская, 46, строение 2</span></p>
           
            <p><strong>Эл. почта: </strong>savelovskaya.customer@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(499) 753 - 16 - 53</span> - Многоканальный</li>
             
              <li><span itemprop="telephone">(495) 602 - 50 - 25</span> - Женский центр, 2 этаж</li>
             
              <li><span itemprop="telephone">(495) 610 - 47 - 22</span> - Отделение ЭКО, Медико-генетический центр, 3 этаж</li>
             
              <li><span itemprop="telephone">(495) 610 - 58 - 69</span> - Детский центр, 1 этаж</li>
             
              <li><span itemprop="telephone">(495) 610 - 26 - 37</span> - Отделение стоматологии, 1 этаж</li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/moscow/the-clinic-health/" ><img src="/upload/iblock/594/5949a5bce1ad2093ae1e53a18c82eff9_thumb_393404a8c0c05231b9a43be0edb68f81.jpg"  /></a> <a style="color:#d73612;" href="/clinics/moscow/the-clinic-health/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Кунцево</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">Можайское шоссе, 2</span></p>
           
            <p><strong>Эл. почта: </strong>info.kuntsevo@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(495) 925-21-12 - Отделение неотложной помощи помощи для детей (круглосуточно)</span></li>
             
              <li><span itemprop="telephone">(495) 925-22-00 - Женский центр</span></li>
             
              <li><span itemprop="telephone">(495) 925-21-27 - Отделение ЭКО</span></li>
             
              <li><span itemprop="telephone">(495) 925-21-12 - Детский центр</span></li>
             
              <li><span itemprop="telephone">(495) 925-21-12 - Стоматологическое отделение</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/moscow/the-clinic-mother-and-child-kuntsevo/" ><img src="/upload/iblock/3a2/3a2e3ce452eca581374ca85ccc492f29_thumb_c95011b7c6f6f7f7a022db21b319827d.jpg"  /></a> <a style="color:#d73612;" href="/clinics/moscow/the-clinic-mother-and-child-kuntsevo/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Новогиреево</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">Союзный проспект, 22</span></p>
           
            <p><strong>Эл. почта: </strong>novogireevo.customer@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(495) 660 - 86 - 32</span> - Многоканальный</li>
             
              <li><span itemprop="telephone">(499) 748 - 41 - 82</span>, <span itemprop="telephone">(499) 748 - 41 - 83</span> - Женский центр</li>
             
              <li><span itemprop="telephone">(499) 748 - 41 - 84</span>, <span itemprop="telephone">(499) 748 - 41 - 85</span> - Детский центр</li>
             
              <li><span itemprop="telephone">(495) 301 - 55 - 61</span> - Стоматология</li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/moscow/the-clinic-mother-and-child-novogireevo/" ><img src="/upload/iblock/637/6377da8f962373caf3486a29f58649ff_thumb_95daae03289584d566d9f9725718ad39.jpg"  /></a> <a style="color:#d73612;" href="/clinics/moscow/the-clinic-mother-and-child-novogireevo/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Сокол</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш Адрес:</strong> <span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">Волоколамское шоссе, 6</span></p>
           
            <p><strong>Эл. почта: </strong>sokol.customer@mcclinics.ru </p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(499) 158 - 21 - 11</span>; <span itemprop="telephone">(495) 926 - 18 - 88</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/moscow/the-clinic-mother-and-child-sokol/" ><img src="/upload/iblock/39c/39c5c64b348d7b05523cca977b66b4b4_thumb_4d581a9f1884f39e4954aeb283d775a5.JPG"  /></a> <a style="color:#d73612;" href="/clinics/moscow/the-clinic-mother-and-child-sokol/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Юго-Запад</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Москва</span>, <span itemprop="streetAddress">ул. Островитянова, 4</span></p>
           
            <p><strong>Эл. почта: </strong>uz.customer@mcclinics.ru </p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(495) 735 - 18 - 18</span>, <span itemprop="telephone">(495) 438 - 02 - 23</span> - Женский центр</li>
             
              <li><span itemprop="telephone">(495) 438 - 17 - 03</span>, <span itemprop="telephone">(499) 737 - 35 - 29</span> - Детский центр</li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/moscow/the-clinic-mother-and-child-south-west/" ><img src="/upload/iblock/f38/f38cad2bdbe499d28e8cec4558ad3914_thumb_242941cd4a13f39629941c3e3f3aaf6e.JPG"  /></a> <a style="color:#d73612;" href="/clinics/moscow/the-clinic-mother-and-child-south-west/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Санкт-Петербург</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Санкт-Петербург</span>, <span itemprop="streetAddress">Средний проспект В.О., 88</span></p>
           
            <p><strong>Эл. почта: </strong>spb.customer@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(812) 676 - 30 - 60</span>, <span itemprop="telephone">(812) 980 - 37 - 37</span>, <span itemprop="telephone">(812) 988 - 37 - 73</span> - Клиника</li>
             
              <li><span itemprop="telephone">(812) 676 - 30 - 50</span> - Офис</li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/saint-petersburg/the-clinic-mother-and-child-st-petersburg/" > <img src="/upload/iblock/dda/dda1cfbd15fcab7dfa9daf911801a846_thumb_79b9acd39efb50356c1559cd4931a9f7.jpg"  /> </a> <a style="color:#d73612;" href="/clinics/saint-petersburg/the-clinic-mother-and-child-st-petersburg/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Госпиталь «Мать и дитя» Уфа</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Уфа</span>, <span itemprop="streetAddress">Лесной проезд, д.4</span></p>
           
            <p><strong>Эл. почта: </strong>ufa.hospital@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(347) 216 - 03 - 03</span>, <span itemprop="telephone">(347) 276 - 03 - 03</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/ufa/hospital-mother-and-child-ufa-/" > <img width="190" height="210" src="/upload/iblock/b7e/b7edd7182b52b39992adbcc82ec6a0ea_thumb_a2645521c4870fc6fbd5b35983e1ff1d.jpg"  /> </a> <a style="color:#d73612;" href="/clinics/ufa/hospital-mother-and-child-ufa-/" >Подробнее о госпитале &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Уфа</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Уфа</span>, <span itemprop="streetAddress">ул. Королева, 24</span></p>
           
            <p><strong>Эл. почта: </strong>ufa.clinic@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(347) 293 - 03 - 03</span>, <span itemprop="telephone">(919) 613 - 26 - 76</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/ufa/the-clinic-mother-and-child-ufa/" > <img width="190" height="210" src="/upload/iblock/ec6/ec6b2882574ee00dcf22396f5d76650a_thumb_d92a0f12531e1a934ba8d780bc09eea5.jpg"  /> </a> <a style="color:#d73612;" href="/clinics/ufa/the-clinic-mother-and-child-ufa/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Ярославль</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Ярославль</span>, <span itemprop="streetAddress">ул. 5-я Яковлевская,  17</span></p>
           
            <p><strong>Эл. почта: </strong>yaroslavl.customer@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(4852) 741 - 515</span>, <span itemprop="telephone">(4852) 680 - 868</span>, <span itemprop="telephone">(4852) 700 - 330</span>, <span itemprop="telephone">(4852) 743 - 544</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/yaroslavl/hospital-pirogov-group-of-companies-mother-and-child-yaroslavl/" ><img src="/upload/iblock/577/577414678ad1f192668d1cd5edeaac64_thumb_c3374d7dca46cad66ade3f06e16ea560.jpg"  /></a> <a style="color:#d73612;" href="/clinics/yaroslavl/hospital-pirogov-group-of-companies-mother-and-child-yaroslavl/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Пермь</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> Пермский край, <span itemprop="addressLocality">Пермь</span>, <span itemprop="streetAddress">ул. Екатерининская, 64</span></p>
           
            <p><strong>Эл. почта: </strong>perm.customer@mcclinics.ru</p>
           
            <p><strong>Телефон:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(342) 2 - 101 - 101</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"><img src="/upload/medialibrary/7bb/7bb506f024c38256d753b030959b9cfa.jpg" title="пермь_новая_домик.jpg" border="0" alt="пермь_новая_домик.jpg" width="190" height="210"  /><a style="color:#d73612;" href="/clinics/perm/the-clinic-mother-and-child-perm/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Иркутск</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Иркутск</span>, <span itemprop="streetAddress">ул. Пушкина, 8А</span></p>

            <p><strong>Эл. почта: </strong>mamadeti-irk@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(3952) 63 - 03 - 87</span></li>
             
              <li><span itemprop="telephone">(3952) 63 - 03 - 79</span> - Регистратура</li>
             
              <li><span itemprop="telephone">(952) 61 - 38 - 389</span> - Администратор</li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/irkutsk/the-clinic-mother-and-child-irkutsk/" ><img src="/upload/iblock/ce0/ce03aa69ff69b8c1fedcbd5c66bdbad1_thumb_67e053676f7577e80f1af4d918d54c0e.jpg"  /></a> <a style="color:#d73612;" href="/clinics/irkutsk/the-clinic-mother-and-child-irkutsk/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Самара</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Самара</span>, <span itemprop="streetAddress">ул. Ново-Садовая, 139</span></p>
           
            <p><strong>Эл. почта: </strong>info@mc-idk.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(800) 250 - 24 - 24</span>, <span itemprop="telephone">(846) 933 - 82 - 88</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/samara/clinic-idk-the-group-of-companies-mother-and-child-samara/" ><img src="/upload/iblock/5d1/5d12150f0887a0bc4e70f1bda9805c0c_thumb_b50d100efb2830876154373b16b5284c.jpg"  /></a> <a style="color:#d73612;" href="/clinics/samara/clinic-idk-the-group-of-companies-mother-and-child-samara/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Госпитальный центр «Мать и дитя» Самара</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Самара</span>, <span itemprop="streetAddress">ул. Энтузиастов, 29</span></p>
           
            <p><strong>Эл. почта: </strong>info@mc-idk.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(800) 250 - 24 - 24</span>, <span itemprop="telephone">(846) 269 - 65 - 65</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/samara/clinic-idk-mother-and-child-samara-entuziastov-29/" ><img src="/upload/iblock/e25/e255a31956a18634ca352ae236b93eb8_thumb_9fa393110a33f1d937035d11b0037e96.JPG"  /></a> <a style="color:#d73612;" href="/clinics/samara/clinic-idk-mother-and-child-samara-entuziastov-29/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Детская клиника «Мать и дитя» Самара</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Самара</span>, <span itemprop="streetAddress">ул. Гагарина/Митирева, 30/16</span></p>
           
            <p><strong>Эл. почта: </strong>info@mc-idk.ru</p>
           
            <p><strong>Телефон:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(800) 250 - 24 - 24</span> - (звонок по России бесплатный)</li>
             </ul>
           		</div>
         	</div>
       </td> 	<td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> 	<a href="/clinics/samara/children-s-clinic-mother-and-child-idk/" > 		<img src="/upload/iblock/68b/68b28e4f8f81994043e265acab22eb3f_thumb_334a84534968433dc13ab7cd1d977e93.JPG"  /> 	</a> 	<a style="color:#d73612;" href="/clinics/samara/children-s-clinic-mother-and-child-idk/" >Подробнее о клинике &gt;&gt;</a> 	</td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Новокуйбышевск</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Новокуйбышевск</span>, <span itemprop="streetAddress">ул. Репина, 11</span></p>
           
            <p><strong>Эл. почта: </strong>info@mc-idk.ru</p>
           
            <p><strong>Телефон:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(800) 250 - 24 - 24</span></li>
             </ul>
           		</div>
         	</div>
       </td> 	<td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> 		<a href="/clinics/novokuibyshevsk/clinic-idk-the-group-of-companies-mother-and-child-novokuibyshevsk/" > 			<img src="/upload/iblock/f45/f459025dc0525e2ad8e4964f55e893f7_thumb_c25bc48625b2e0b54e86f0ecc4b10cbe.JPG"  /> 		</a> 		<a style="color:#d73612;" href="/clinics/novokuibyshevsk/clinic-idk-the-group-of-companies-mother-and-child-novokuibyshevsk/" >Подробнее о клинике &gt;&gt;</a> 	</td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Тольятти</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">Тольятти</span>, <span itemprop="streetAddress">ул. Ворошилова, 73</span></p>
           
            <p><strong>Эл. почта: </strong>info@mc-idk.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">(800) 250 - 24 - 24</span> - (звонок по России бесплатный)</li>
             
              <li><span itemprop="telephone">(8482) 75 - 82 - 75</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/togliatti/clinic-idk-the-group-of-companies-mother-and-child-togliatti/" > <img src="/upload/iblock/ff2/ff24f561cd66fc097cdadc1cf9b5730c_thumb_c74baf1d7958fcbefe509931497355ce.jpg"  /> </a> <a style="color:#d73612;" href="/clinics/togliatti/clinic-idk-the-group-of-companies-mother-and-child-togliatti/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника &quot;Мать и дитя&quot; &quot;Авиценна&quot; Красный проспект Новосибирск</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> г. <span itemprop="addressLocality">Новосибирск</span>, <span itemprop="streetAddress">Красный проспект, д. 35</span></p>
           
            <p><strong>Эл. почта: </strong>clientservice@avicenna-nsk.ru </p>
           
            <p><strong>Телефон:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">8 (383) 363-30-03</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/novosibirsk/clinic-mother-and-child-avicenna-red-prospect/" > <img src="/upload/iblock/eb1/eb1f3b299eb04ba19f42dd17f510c243_thumb_042c1f7077cb73960e531cab2ab6276a.jpg"  /> </a> <a style="color:#d73612;" href="/clinics/novosibirsk/clinic-mother-and-child-avicenna-red-prospect/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника &quot;Мать и дитя&quot; &quot;Авиценна&quot; проспект Димитрова Новосибирск</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong>г. <span itemprop="addressLocality">Новосибирск</span>, <span itemprop="streetAddress">проспект Димитрова, д. 7</span></p>
           
            <p><strong>Эл. почта: </strong>clientservice@avicenna-nsk.ru </p>
           
            <p><strong>Телефон:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">8 (383) 363-30-03</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/novosibirsk/clinic-mother-and-child-avicenna-prospekt-dimitrova/" > <img src="/upload/iblock/eb1/eb1f3b299eb04ba19f42dd17f510c243_thumb_042c1f7077cb73960e531cab2ab6276a.jpg"  /> </a> <a style="color:#d73612;" href="/clinics/novosibirsk/clinic-mother-and-child-avicenna-prospekt-dimitrova/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клинический Госпиталь &quot;Мать и дитя&quot; &quot;Авиценна&quot; Новосибирск</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> г. Новосибирск, улица Урицкого, д. 2 <span itemprop="streetAddress"></span></p>
           
            <p><strong>Эл. почта: </strong>clientservice@avicenna-nsk.ru </p>
           
            <p><strong>Телефон:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">8 (383) 363-30-03</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/novosibirsk/clinical-hospital-mother-and-child-avicenna-novosibirsk/" > <img src="/upload/iblock/b91/b915111ac5d8f6a7d596ee5c55c21eec_thumb_5c29aee135fb5f90bcfa3ba51ba5219a.jpg"  /> </a> <a style="color:#d73612;" href="/clinics/novosibirsk/clinical-hospital-mother-and-child-avicenna-novosibirsk/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника &quot;Мать и дитя&quot; &quot;Сердечко&quot; Новосибирск</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> г. <span itemprop="addressLocality">Новосибирск</span>, <span itemprop="streetAddress">Красный проспект, д. 6</span></p>
           
            <p><strong>Эл. почта: </strong>clientservice@avicenna-nsk.ru </p>
           
            <p><strong>Телефон:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">8(383) 255-44-55</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"> <a href="/clinics/novosibirsk/clinic-mother-and-child-heart/" ><img src="/upload/iblock/b91/b915111ac5d8f6a7d596ee5c55c21eec_thumb_5c29aee135fb5f90bcfa3ba51ba5219a.jpg"  /> </a><a style="color: rgb(215, 54, 18);" href="/clinics/novosibirsk/clinic-mother-and-child-heart/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   
    <tr> <td style="border-bottom-style: solid; border-bottom-width: 1px;"> 	 
        <div itemtype="http://schema.org/Organization" itemscope=""> 
          <p><strong><span itemprop="name" style="color: rgb(215, 54, 18); font-size: 18px;">Клиника «Мать и дитя» Рязань</span></strong></p>
         		 
          <div itemtype="http://schema.org/PostalAddress" itemscope="" itemprop="address"> 
            <p><strong>Наш адрес:</strong> <span itemprop="addressLocality">г. Рязань</span>, <span itemprop="streetAddress">ул. Шереметьевская, дом 16А</span></p>
           
            <p><strong>Эл. почта: </strong>mdrzn@mcclinics.ru</p>
           
            <p><strong>Телефоны:</strong></p>
           
            <ul> 
              <li><span itemprop="telephone">+7 (4912) 77 02 10</span></li>
             
              <li><span itemprop="telephone">+7 (961) 009 49 25</span></li>
             </ul>
           		</div>
         	</div>
       </td> <td style="width: 190px; padding: 20px 0px 20px 20px; text-align: center; border-bottom-style: solid; border-bottom-width: 1px;"><a href="/clinics/ryazan/clinic-mother-and-child-ryazan/" ><img src="/upload/medialibrary/4db/4db251df77fcfebb42fab22e15cb7144.jpg" border="0" width="190" height="210"  /></a><a style="color:#d73612;" href="/clinics/ryazan/clinic-mother-and-child-ryazan/" >Подробнее о клинике &gt;&gt;</a> </td> </tr>
   </tbody>
 </table>
