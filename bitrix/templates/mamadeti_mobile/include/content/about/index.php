
<p>Добро пожаловать в группу компаний &laquo;Мать и дитя&raquo;! 
  <br />
 Мы открыли для пациентов двери нашей первой клиники в январе 2006 года. «Мать и дитя» сегодня - это 4 ультрасовременных высокотехнологичных госпиталя, 19 клиник, обладающих, мощными диагностическими и терапевтическими ресурсами, в 11 городах России, более 6000 квалифицированных специалистов, знающих и любящих свою работу, уникальный спектр медицинских услуг для всей семьи. </p>
 
<p>Наша компания &ndash; лидер в области акушерства, гинекологии и педиатрии. Забота о женщине на каждом этапе ее жизни, о детях – с самого рождения и практически до совершеннолетия – в нашей компетенции.</p>
 
<p> </p>
 
<p>Мы - № 1 в России по числу циклов ЭКО, успешно применяем все возможности современной репродуктологии и добиваемся главного – рождения долгожданного ребенка в каждой семье.</p>
 
<p> </p>
 
<p>Роды в «Мать и дитя» - безопасность и абсолютный комфорт, сочетание традиций классического акушерства и высоких медицинских технологий, заботы о будущей матери и индивидуального подхода к ведению беременности и родов. </p>
 
<p> </p>
 
<p>Сохранение и восстановление детского здоровья – также приоритетное направление нашей деятельности. Мы – эксперт № 1 в стране в области реанимации, интенсивной терапии и выхаживания недоношенных малышей. Динамическое наблюдение, плановая и экстренная, амбулаторная и стационарная квалифицированная помощь каждому ребенку - наша задача.</p>
 
<p> </p>
 
<p>Мы уже предложили пациентам большинство методов диагностики, консервативного и оперативного лечения, реабилитации, применяемых в международной медицинской практике. Мы не останавливаемся на достигнутом, совершенствуем свои навыки и уровень предоставляемого сервиса, чтобы вновь и вновь подтверждать оказанное нам доверие пациентов. Нас рекомендуют своим близким, что является для нас высшей оценкой нашего труда.</p>
 
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: justify;"> </p>
 
<p> 
  <br />
 Добро пожаловать в «Мать и дитя» - здесь каждый пациент обретает то, что необходимо именно ему. Мы считаем, что нет ничего невозможного. На невозможное просто требуется больше времени, наших сил и профессионализма – и все получается.</p>
