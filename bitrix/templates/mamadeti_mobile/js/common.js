var pressEventEnd = $('html').hasClass('touch') ? 'touchend' : 'click',
    pressEventStart = $('html').hasClass('touch') ? 'touchstart' : 'click',
    pressEventStartEnd = $('html').hasClass('touch') ? 'touchstart touchend' : 'click',
    scrollTop = false,
    hideHeader = true;

function sendMetrikaEvent(metrikaId) {
    if (window.yaCounter26861655) {
        window.yaCounter26861655.reachGoal(metrikaId);
    }
}

function openSidebar() {
    if (!$('body').hasClass('open-sidebar')) {
        $('body').addClass('open-sidebar');
    } else {
        $('body').removeClass('open-sidebar');
    }
}

function hideContent(flag, scroll) {
    if (flag) {
        $('.js-main-content').hide(); 
    } else {
        $('.js-main-content').show();
        if (scroll) {
            $(document).scrollTop(scroll);
        }
    }
}

$(function () {

    $(document).on('change', '.js-select', function () {
        text = typeof $(this).find('option:selected').data('value') != 'undefined' && $(this).find('option:selected').data('value').length 
                ? $(this).find('option:selected').data('value') 
                : $(this).find('option:selected').text();
                
        $(this).parents('.js-select-wrap').find('.js-select-current').text(text);
    });

    $(document).on(pressEventStart, '.js-hover', function (e) {
        $(this).addClass('hover-effect');
    });

    $(document).on(pressEventEnd, '.js-hover', function (e) {
        $(this).removeClass('hover-effect');
    });

    $('.js-sidebar-icon').on(pressEventEnd, function (e) {
        e.preventDefault();
        openSidebar();
    });

    $('.js-overlay-page').on('click', function (e) {
        e.preventDefault();
        openSidebar();
    });

    $('.js-map-show').on('click', function (e) {
        var $this = $(this);

        e.preventDefault();
        scrollTop = $(document).scrollTop();
        hideContent(true, scrollTop);
        $(document).scrollTop(0);
        $('.js-map').show();

        if (!$this.data('map-ready')) {
            $this.data('map-ready', true);
            maps.ready(function() {
                maps.init('clinic');
                maps.resetMarkers('clinic');
                maps.addMarkers('clinic', {"clinic":{"name":"","body":'',"coords":$this.data('coords').split(',')}});
                maps.refreshZoom('clinic');
                $('.js-clinic-address').html($this.data('address'));
                $('.map__container').removeClass('loading');
            });
        }
    });

    $('.js-map-close').on('click', function (e) {
        e.preventDefault();
        hideContent(false, scrollTop);
        $('.js-map').hide();
    });

    $.mask.definitions['~'] = '[+-]';

    $('.js-clinic-menu-more').on('click', function (e) {
        $('.js-clinic-menu-hidden').show();
        $(this).hide();
    });

    $('.js-doctor-reviews-show').click(function() {
        $('.js-doctor-info').hide();
        $('.js-doctor-reviews').show();
        location.hash = '#reviews';
    });

    $('.js-doctor-info-show').click(function() {
        $('.js-doctor-info').show();
        $('.js-doctor-reviews').hide();
        location.hash = '';
    });

    $('.js-tree-head').click(function() {
        $parent = $(this).parents('.js-tree-item');
        if ($parent.find('.js-tree-content:visible').length) {
            $parent.find('.js-tree-content').slideUp(300);
            $parent.find('.js-tree-switch').html('+');
        } else {
            $parent.find('.js-tree-content').slideDown(300);
            $parent.find('.js-tree-switch').html('&ndash;');
        }
    });

    if ($('.js-txt-cut').length) {
        $('.js-txt-cut').each(function() {
            height = $(this).data('height') || 80;
            txtMore = $(this).data('more') || 'еще...';
            if (height < $(this).height()) {
                $(this).height(height).addClass('text-cutter').after('<div class="text-catter-more js-txt-cut-open">'+txtMore+'</div>');
            }
         });
    }

    if ($('.js-tree-content').length) {
        $('.js-tree-content:not(.js-tree-active)').addClass('hidden');
    }

    $('.js-txt-cut-open').click(function() {
        $(this).siblings('.js-txt-cut').height('auto').removeClass('text-cutter');
        $(this).remove();
    });

    if ($('.js-doctor-reviews').length) {
        $('.js-doctor-reviews').addClass('hidden');
                                                                                                            
        if (location.hash === '#reviews') {
            $('.js-doctor-reviews-show').trigger('click');
        }
    }

    $(document)
        .on('change', '.js-city-selector, .js-clinic-selector', function(e) {
            var $this = $(this);
            if (!$this.val()) {
                return;
            }

            location.href = $this.val();
        }).on('change', '.js-action-filter select, .js-doctor-filter select, .js-program-filter select, .js-review-filter select', function(e) {
            $(this).closest('form').submit();
        }).on('click', '.js-book-show', function (e) {
            var $this = $(this),

            handleAjaxResponse = function(response) {
                var $book = $('.js-book'),
                    $metrika;

                $book.html($('<div>' + response + '</div>').find('.js-book').html());
                $book.find('input[data-phone="yes"]').mask('8 999 999-99-99');
                $book.find('input[data-date="yes"]').mask("99.99.9999",{placeholder:"дд/мм/гггг"});
                $book.find('input[data-time="yes"]').mask("99:99",{placeholder:"чч:мм"});

                $book.find('form').submit(function(e) {
                    e.preventDefault();

                    var $form = $(this);

                    $form.find('input[type="submit"]').closest('.button').addClass('loading');

                    $.ajax({
                        type: $form.attr('method'),
                        url: $form.attr('action'),
                        data: $form.serialize()
                    }).done(handleAjaxResponse);
                });

                $metrika = $book.find('.js-metrika-success');
                if ($metrika.size() && typeof $metrika.data('metrikaId') !== 'undefined') {
                    sendMetrikaEvent($metrika.data('metrikaId'));
                }
            };

            if ($this.data('busy')) {
                return;
            }
            $this
                .data({busy: true})
                .addClass('loading');

            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: '/book.php',
                data: {CLINIC: $this.data('clinic'), DOCTOR: $this.data('doctor')}
            }).done(function(response) {
                $this
                    .data({busy: false})
                    .removeClass('loading');

                handleAjaxResponse(response);

                scrollTop = $(document).scrollTop();
                hideContent(true, scrollTop);
                $(document).scrollTop(0);
                $('.js-main-header').hide();
                $('.js-book').show();
            });
        });
        
    $('.js-book').on('click', '.js-book-close', function (e) {
        hideContent(false, scrollTop);
        $('.js-main-header').show(); 
        $('.js-book').hide();
    });

    $(document).on('change', 'select[name=filter_clinic], select[name=filter_section]', function(e) {
        var $form = $(this).closest('form'),
            data = $form.serializeArray(),
            url = $form.attr('action') + '?', 
            attributes = [];
        $.each(data, function(index) {
            var attribute = data[index];
            attributes.push(attribute.name + '=' + attribute.value);
        });
        url += attributes.join('&');
        location.href = url;
    });

    $(document).on('click', '.js-metrika-target', function() {
        var metrikaId = $(this).data('metrikaId');
        if (typeof metrikaId !== 'undefined') {
            sendMetrikaEvent(metrikaId);
        }
       
    });

    var phoneReplaces = {};

    function tokenizeNumber(number) {
        return number.replace(/(^\s*\+\d|\-+|\s+|\(|\))/g, '');
    }

    function parsePhone(phone) {
        var parts = [], token = $(phone.id).first().text();

        if (/^\d{4}/.test(phone.text)) {
            phone.text = phone.text.replace(/^(\d{1})(\d{3})(.+)/, function(s, country, city, number) {
                return '+' + country + ' (' + city + ') ' + number;
            });
        }

        phone.text = phone.text.replace(/\-+/g, ' ');

        if (!token && phoneReplacements.hasOwnProperty(phone.id)) {
            token = tokenizeNumber(phoneReplacements[phone.id]);
        }

        if (token) {
            token = tokenizeNumber(token);
            phoneReplaces[token] = phone.text;
        } 

        parts = phone.text.split(')');

        if (parts.length === 2) {
            phone.text = '<span>' + parts[0] + ')</span> <span class="highlight">'+parts[1]+'</span>';
        }

        return phone;
    }

    window.__cs_onReplacePhones = function(phones) {
        
        for (var i = 0; i < phones.length; i++) {
            phones[i] = parsePhone(phones[i]);
        }

        $('.js-phone').each(function() {
            var $this = $(this), token = tokenizeNumber($this.text());
            if (typeof phoneReplaces[token] === 'undefined') return;

            $this.html(phoneReplaces[token]);

        });

        return true;
    };

    if (window.comagicId) {
        setTimeout(function() {
            window.__cs = [];
            __cs.push(["setAccount", window.comagicId]);
            __cs.push(["setHost", "//server.comagic.ru/comagic"]);

                var s;
            s = document.createElement('script');
            s.async=1;
            s.src='http://app.comagic.ru/static/cs.min.js';
            $('head').append(s);
        }, 1);
    }

    $(document).on('click', '.js-button-mobile-confirm', function(e) {
         e.preventDefault();
         var url;
        if (confirm('Форма доступна в полной версии сайта. Перейти на полную версию?')) {
            url = window.location.href.replace(/^(http:\/\/)m\.(.+)/, '$1$2');
            window.location.assign(url);
        }
    });

    
    $('body').on('click', '.js-gasend', function() {
        if (typeof window.ga !== 'function') {
            return;
        }

        var $this = $(this);	  
        if (!$this.data('targetga')) {
            return;
        }
	  var params = $this.data('targetga').split('/');
	  
	  if(params.length) window.ga('send',(!!params[0] ? params[0] : ''),(!!params[1] ? params[1] : ''),(!!params[2] ? params[2] : ''),(!!params[3] ? params[4] : ''));
    });
});
