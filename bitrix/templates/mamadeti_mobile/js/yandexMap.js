(function() {
	var defaultSettings = {
        center: [55.753676,37.619899], 
        zoom: 13,
        controls: [],
        behaviors: ['default', 'drag', 'scrollZoom']
    },

    defaultMarkerSettings = {
    	iconLayout: 'default#image',
        //iconImageClipRect: [[0,0], [36, 34]],
        iconImageHref: '/bitrix/templates/mamadeti_mobile/i/i-marker_map.png?2',
        iconImageSize: [37, 33],
        iconImageOffset: [-12, -33],
        zIndex: 1
    },

    hoverMarkerSettings = $.extend({}, defaultMarkerSettings, {
        iconImageClipRect: [[36,0], [72, 34]],
        zIndex: 10
    }),

    instance = null;

    function updatePlacemark(placemark, options) {
    	for (var option in options) {
    		placemark.options.set(option, options[option]);
    	}
    }

    function createInstance() {
        var self = this,

        maps = {},
        placemarks = {},

        events = {
            balloonopen: [],
            balloonclose: [],
            mouseleave: [],
            mouseenter: []
        };

        self.isReady = false;

        self.init = function(mapId, settings) {
            settings = $.extend({}, defaultSettings, settings || {});
            if (typeof maps[mapId] !== 'undefined') {
                return;
            }

            maps[mapId] = new ymaps.Map('map-' + mapId, settings, {
                suppressMapOpenBlock: true
            });

            return self;
        };

        self.initDeferred = function(mapId) {
            $(document).trigger('initmap.' + mapId);
        };

        self.handleEvent = function(event, callback) {
            events[event].push(callback);

            return self;
        };

        self.refreshZoom = function(mapId) {
            var map = maps[mapId], 
            geoObjects = maps[mapId].geoObjects;

            map.setBounds(geoObjects.getBounds());
            map.setZoom(map.getZoom()-1);
            
            if (geoObjects.getLength() < 2) {
                map.setZoom(14);
            }

            return self;
        };

        self.refreshViewport = function(mapId) {
            maps[mapId].container.fitToViewport();

            return self;
        };

        self.addMarker = function(mapId, markerId, marker) {
            var placemark = new ymaps.Placemark(
                marker.coords, 
                {
                    balloonContentHeader: marker.name, 
                    balloonContentBody: marker.body
                },
                {
                    balloonOffset: [0, -35],
                    hideIconOnBalloonOpen: false,
                    preset: 'custom#default',
                    balloonPanelMaxMapArea: false
                }
            );

            for (var e in events) {
                placemark.events.add(e, (function(e) {
                    return function() {
                        $.each(events[e], function() {
                            this(mapId, markerId);
                        });    
                    };
                })(e));
            }

            maps[mapId].geoObjects.add(placemark);

            if (typeof placemarks[mapId] == 'undefined') {
                placemarks[mapId] = {};
            }
            placemarks[mapId][markerId] = placemark;

            return self;
        };

        self.addMarkers = function(mapId, markers) {
            for (var markerId in markers) {
                self.addMarker(mapId, markerId, markers[markerId]);
            }

            return self;
        };

        self.resetMarkers = function(mapId) {
            if (typeof placemarks[mapId] === 'undefined') {
                return;
            }

            $.each(placemarks[mapId], function() {
                maps[mapId].geoObjects.remove(this);
            })
            placemarks[mapId] = {};
        }

        self.getMarker = function(mapId, markerId) {
            return placemarks[mapId][markerId];
        };

        self.getMarkers = function(mapId) {
            return placemarks[mapId];
        };

        self.hoverMarker = function(mapId, markerId) {
            updatePlacemark(
                self.getMarker(mapId, markerId), 
                {preset: 'custom#hover'}
            );

            return self;
        };

        self.leaveMarker = function(mapId, markerId) {
            updatePlacemark(
                self.getMarker(mapId, markerId), 
                {preset: 'custom#default'}
            );

            return self;
        };

        self.openBalloon = function(mapId, markerId, immediate) {
            var marker = self.getMarker(mapId, markerId);
            if (immediate) {
                marker.options.set('balloonAutoPanDuration', 0);
            }
            marker.balloon.open();

            return self;
        };

        self.isBalloonOpened = function(mapId, markerId) {
            return self.getMarker(mapId, markerId).balloon.isOpen();
        };

        self.ready = function(callback) {
            ymaps.ready(callback);

            return self;
        };

        return self;
    }

    instance = createInstance();

    ymaps.ready(function() {
        ymaps.option.presetStorage.add('custom#default', defaultMarkerSettings);
        ymaps.option.presetStorage.add('custom#hover', hoverMarkerSettings);

        instance.isReady = true;
    });

    window.maps = instance;
})();