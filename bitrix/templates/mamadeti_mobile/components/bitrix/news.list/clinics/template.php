<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php
    $city = CityManager::getInstance()->getCityById(GetCurrentCity());

    $titles = [
        'hospitals' => 'Госпитали «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
        'clinics' => 'Клиники «Мать и дитя» в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'],
    ];

    $clinicCollections = ['hospitals' => [], 'clinics' => []];

    foreach ($arResult['ITEMS'] as $arItem) {
        $clinicCollections[$arItem['PROPERTIES']['IS_HOSPITAL']['VALUE'] ? 'hospitals' : 'clinics'][] = $arItem;
    }

    $clinicCollections = array_filter($clinicCollections);
?>

<div class="clinics-list">
    <?php foreach ($clinicCollections as $type => $clinics): ?>
        <div class="header1"><?php echo $titles[$type]; ?></div>
        <div class="clinics-list__items">
            <?php foreach ($clinics as $index => $clinic): ?>
                <?php
                    $image = null;
                    if ($clinic['PREVIEW_PICTURE']) {
                        $image =
                            CFile::ResizeImageGet(
                                $clinic['PREVIEW_PICTURE']['ID'],
                                ['width' => 640, 'height' => 400],
                                BX_RESIZE_IMAGE_EXACT
                            );
                        $image = $image['src'];
                    }

                    $arClinicPhones = is_array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']) ? $clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE'] : array($clinic['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']);
                    $arClinicDescriptions = $clinic['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION'];
                    $clinicPhones = '';
                    foreach ($arClinicPhones as $phoneIndex => $phoneValue) {
                        $isEqual = PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phoneValue);
                        if($isEqual){
                            $phoneValue = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                        }

                        $phoneDescription = array();

                        if(!empty($arClinicDescriptions[$phoneIndex])){
                            $phoneDescription[] = $arClinicDescriptions[$phoneIndex];
                        }

                        $clinicPhones.= '<span class="clinics-list__phone-item">';
                        $clinicPhones.= PhoneFormatter::format($phoneValue);

                        if($isEqual) {
                            $phoneDescription[] = CLIENT_FROM_ABROAD ? 'звонок из-за рубежа' : 'звонок бесплатный';
                        }

                        if(!empty($phoneDescription)){
                            $clinicPhones.= ' <span class="phone-note">' . implode(', ', $phoneDescription) . '</span>';
                        }

                        $clinicPhones.= '</span>';
                    }
                ?>

                <div class="clinics-list__item">
                    <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="clinics-list__pic" style="background-image:url('<?php echo $image; ?>')"><img src="<?php echo $image; ?>" alt=""></a>
                    <a href="<?php echo $clinic['DETAIL_PAGE_URL']; ?>" class="clinics-list__name"><?php echo $clinic['NAME']; ?></a>
                    <div class="clinics-list__phone"><?php echo $clinicPhones; ?></div>
                    <div class="clinics-list__address"><?php echo $clinic['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?></div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>
