<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<?php if ($arResult['ITEMS']): ?>
    <div class="doctors-list">
        <?php foreach ($arResult['ITEMS'] as $index => $arItem):?>
            <?php
                $imageData = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array('width' => 120, 'height' => 120), BX_RESIZE_IMAGE_EXACT, true);
            ?>
            <div class="doctors-list__item">
                <div class="doctor-card clearfix">
                    <div class="doctor-card__left">
                        <?php if ($imageData): ?>
                            <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="doctor-card__pic js-hover">
                                <img src="<?php echo $imageData['src']; ?>" alt="">
                            </a>
                        <?php else: ?>
                            <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="doctor-card__pic js-hover nophoto--doctor_<?php echo $arItem['DISPLAY_PROPERTIES']['SEX']['DISPLAY_VALUE'] === 'Ж' ? 'female' : 'male'; ?>"></a>
                        <?php endif; ?>
                    </div>
                    <div class="doctor-card__right">
                        <?php if ($arItem['STATISTICS']['REVIEW']): ?>
                            <div class="doctors-list__reviews">
                                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>#reviews" class="doctor-card__reviews"><?php echo $arItem['STATISTICS']['REVIEW']; ?></a>
                            </div>
                        <?php endif; ?>
                        <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="doctor-card__name"><?php echo $arItem['NAME']; ?></a>
                        <div class="doctor-card__spec">
                            <?php
                                echo strip_tags(
                                    is_array($arItem['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE'])
                                        ? implode(', ', $arItem['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE'])
                                        : $arItem['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE']
                                );
                            ?>
                        </div>
                        <div class="doctor-card__clinics">
                            <?php if ($arItem['DISPLAY_PROPERTIES']['SECTION']['DISPLAY_VALUE']): ?>
                                <p><?php echo strip_tags($arItem['DISPLAY_PROPERTIES']['SECTION']['DISPLAY_VALUE']); ?></p>
                            <?php endif; ?>
                            
                            Ведёт приём в:<br/>
                            <?php echo is_array($arItem['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']) ? implode('<br/>', $arItem['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']) : $arItem['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if ($arParams['DISPLAY_BOTTOM_PAGER']): ?>
        <?php echo $arResult['NAV_STRING']?>
    <?php endif;?>
<?php endif; ?>
