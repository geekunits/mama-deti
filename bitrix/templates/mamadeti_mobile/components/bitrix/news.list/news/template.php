<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<?php if ($arResult['ITEMS']): ?>
    <div class="news-list">
        <?php foreach ($arResult['ITEMS'] as $arItem):?>
            <?php
                $imageData = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'] ?: $arItem['PREVIEW_PICTURE']['ID'], array('width' => 640, 'height' => 300), BX_RESIZE_IMAGE_EXACT, true);

                $dateFrom = $arItem['DATE_ACTIVE_FROM'];
                $dateFrom = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateFrom);
            ?>
            <div class="news-list__item">
                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="news-list__pic" style="background-image: url(<?php echo $imageData['src']; ?>)">
                    <img src="<?php echo $imageData['src']; ?>" alt="">
                </a>
                <div class="news-detail__date"><?php echo $dateFrom; ?></div>
                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="news-list__name"><?php echo $arItem['NAME']; ?></a>
                <div class="news-list__txt"><?php echo strip_tags($arItem['PREVIEW_TEXT']); ?></div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if ($arParams['DISPLAY_BOTTOM_PAGER']): ?>
        <?php echo $arResult['NAV_STRING']?>
    <?php endif;?>
<?php endif; ?>
