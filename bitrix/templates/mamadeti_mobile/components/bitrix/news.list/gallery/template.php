<div class="gallery">
	<?php foreach ($arResult['ITEMS'] as $arItem): ?>
		<?php
            $image = null;
            if ($arItem['PREVIEW_PICTURE']) {
                $image =
                    CFile::ResizeImageGet(
                        $arItem['PREVIEW_PICTURE']['ID'],
                        ['height' => 640, 'width' => 640],
                        BX_RESIZE_IMAGE_EXACT
                    );
                $image = $image['src'];
            }
        ?>
		<a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="gallery__item js-hover">
			<span class="gallery__pic" style="background-image: url('<?php echo $image; ?>')"><img src="<?php echo $image; ?>" alt=""></span>
			<span class="gallery__name"><?php echo $arItem['NAME']; ?></span>
		</a>
	<?php endforeach; ?>
</div>
