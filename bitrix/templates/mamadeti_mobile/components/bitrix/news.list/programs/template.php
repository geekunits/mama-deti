<?php
$prevDirectionName = null;
$prevAgeName = null;
$labelProgram = 0;
?>
<?php foreach ($arResult['ITEMS'] as $arItem): ?>
	<?php
		$direction = $arItem['DISPLAY_PROPERTIES']['DIRECTION']['DISPLAY_VALUE'];
		if (is_array($direction)) {
			$direction = $direction[0];
		}
		$directionName = strip_tags($direction);
		$ageName = $arItem['DISPLAY_PROPERTIES']['AGE']['DISPLAY_VALUE'];
	?>
	<?php if ($prevDirectionName != $directionName): ?>
		<?php if ($prevDirectionName): ?>
			</div></div>
		<?php endif; ?>

		<div class="clinic__prorgam-item">
			<div class="clinic__prorgam-title"><?php echo $directionName; ?></div>
			<?php if (!empty($ageName)): ?>
				<div class="clinic__prorgam-subtitle"><?php echo $ageName; ?></div>
			<?php endif; ?>
			<div class="clinic__prorgam-list">

	<?php elseif ($ageName !== $prevAgeName): ?>
		<?php if (!empty($prevAgeName) || !empty($prevDirectionName)): ?>
			</div>
		<?php endif; ?>
		<div class="clinic__prorgam-subtitle"><?php echo $ageName; ?></div>
		<div class="clinic__prorgam-list">
	<?php endif; ?>

	<a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="clinic__prorgam-link js-hover">
		<span class="clinic__prorgam-cost"><?php echo $arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE'] ? formatCurrencyAmount($arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE'], true) : ''; ?> </span>
		<span class="clinic__prorgam-name">
			<?php
				echo $arItem['NAME'];
				if ($arItem["DISPLAY_PROPERTIES"]["TYPE"]){
					$labelProgram++;

					$xmlId = $arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE_XML_ID"];

					if(isset(ProgramHelper::$types[$xmlId])){
						$type = ProgramHelper::$types[$xmlId];
						?>
							<span class="programm-label programm-label--<? echo $xmlId; ?> in-bl"><? echo $type['label']; ?></span>
						<?php
					}
				}
            ?>
		</span>
	</a>
	<?php // var_dump($arItem['DISPLAY_PROPERTIES']['AGE']['DISPLAY_VALUE']); ?>
	<?php 
		$prevDirectionName = $directionName;
		$prevAgeName = $ageName;
	?>
<?php endforeach; ?>
</div></div>
<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
	<?php echo $arResult["NAV_STRING"]?>
<?php endif;?>

<?php if ($labelProgram > 0): ?>
	<div class="program-label-note">
		<?php
			foreach(ProgramHelper::$types AS $typeCode => $type){
				?>
					<div class="programm-label programm-label--<?php echo $typeCode;?> in-bl"><?php echo $type['label'];?></div>
					<div class="program-label-note__txt"><?php echo $type['description'];?></div>
				<?php
			}
		?>
	</div>
<?php endif; ?>