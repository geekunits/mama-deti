<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
    $dateFormat = 'd.m.Y';
    $timeFormat = 'h:i:s';
    $displayClinic = false;
    if (!empty($arParams['FILTER_NAME'])) {
        $filterName = $arParams['FILTER_NAME'];
        $arFilter = $GLOBALS[$filterName];
        if (count($arFilter['PROPERTY_CLINIC']) > 1) {
            $displayClinic = true;
        }
    }
?>
<?php if ($arResult['ITEMS']): ?>
    <div class="news-list">
        <?php foreach($arResult["ITEMS"] as $index => $arItem):?>
            <?php
                $index += 1;
                $dateTo = $arItem['DISPLAY_PROPERTIES']['DATE_TO']['VALUE'];
                $dateFrom = $arItem['DISPLAY_PROPERTIES']['DATE_FROM']['VALUE'];
                $dtActiveTo = null;
                $dtActiveFrom = null;

                $dateTo = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateTo);
                $dateFrom = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateFrom);
                $dtNow = new \DateTime('now');
                $dtNow->setTime(0, 0, 0);

                $dateRange = '';
                if (!empty($dateTo) && !empty($dateFrom)) {
                    if ($dateTo == $dateFrom) {
                        $dateRange = $dateTo;
                    } else {
                        $dateRange = 'c '.$dateFrom.' по '.$dateTo;
                    }
                } elseif (!empty($dateTo)) {
                    $dateRange = 'по '.$dateTo;
                } elseif (!empty($dateFrom)) {
                    $dateRange = 'с '.$dateFrom;
                }

                if (
                    !empty($dateTo)
                    && ($dtActiveTo = \DateTime::createFromFormat($dateFormat, $dateTo))
                ) {
                    $dtActiveTo->setTime(23, 59, 59);
                }
                if (!empty($dateFrom)) {
                     $dtActiveFrom = \DateTime::createFromFormat($dateFormat, $dateFrom);
                }

                $daysLeft = false;
                $daysSoon = false;
                if ($dtActiveFrom) {
                    $intervalSoon = ($dtNow < $dtActiveFrom) ? $dtActiveFrom->diff($dtNow) : false;
                    $daysSoon = $intervalSoon ? intval($intervalSoon->format('%a')) : false;
                }
                if ($dtActiveTo) {
                    $dtInterval = $dtActiveTo->diff($dtNow);
                    $daysLeft = intval($dtInterval->format('%a')) + 1;
                }
                $imageData = CFile::ResizeImageGet($arItem['DETAIL_PICTURE']['ID'], array('width'=>640, 'height'=>300), BX_RESIZE_IMAGE_EXACT, true);
                if ($displayClinic) {
                    $arRawClinics = CMamaDetiAPI::GetClinics(array('ID' => $arItem['PROPERTIES']['CLINIC']['VALUE']));
                    $arClinics = array();
                    foreach ($arRawClinics as $arRawClinic) {
                        $rawId = $arRawClinic['ID'];
                        $arClinics[$rawId] = $arRawClinic;
                    }
                    // $arClinic = empty($arClinics) ? null : $arClinics[0];
                } else {
                    $arClinics = array();
                    $arClinic = null;
                }

            ?>
            <div class="news-list__item">
                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="news-list__pic" style="background-image: url(<?php echo $imageData['src']; ?>)">
                    <img src="<?php echo $imageData['src']; ?>" alt="">
                    <?php if ($daysLeft || $daysSoon): ?>
                        <span class="news-list__end<?php echo $daysSoon ? '--soon' : '';?>">
                            <?php if ($daysSoon): ?>
                                до начала акции <?php echo $daysSoon.' '.plural($daysSoon, ['день', 'дня', 'дней']); ?>
                            <?php elseif ($daysLeft > 1): ?>
                                до окончания акции <?php echo $daysLeft.' '.plural($daysLeft, ['день', 'дня', 'дней']); ?>
                            <?php elseif ($daysLeft === 1): ?>
                                остался последний день
                            <?php endif; ?>
                        </span>
                    <?php endif; ?>
                </a>
                <div class="news-detail__date"><?php echo $dateRange; ?></div>
                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="news-list__name"><?php echo $arItem['NAME']; ?></a>
                <div class="news-list__txt"><?php echo strip_tags($arItem["PREVIEW_TEXT"]); ?></div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?php echo $arResult["NAV_STRING"]?>
    <?php endif;?>
<?php endif; ?>
