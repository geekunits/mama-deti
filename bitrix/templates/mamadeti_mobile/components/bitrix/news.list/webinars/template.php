<div class="clinic__webinars content-scroll">             
    <table width="100%" style="border-collapse: collapse;" class="web-table web-table-left">
      <tbody> 
        <tr>
            <th style="text-align: center;" colspan="1"></th>
            <th style="text-align: center;" colspan="1"><b>Дата проведения вебинара</b></th>
            <th style="text-align: center;" colspan="1"><b>Название вебинара</b></th>
            <th style="text-align: center;" colspan="1"><b>Спикер</b></th>
        </tr>
        <?php foreach ($arResult['ITEMS'] as $index => $arItem): ?>
            <tr>
                <td><?php echo $index + 1; ?></td>
                <td><?php echo $arItem['DISPLAY_PROPERTIES']['DATE']['DISPLAY_VALUE']; ?></td>
                <td>
                    <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"><?php echo $arItem['NAME']; ?></a>
                    <?php if (!empty($arItem['DISPLAY_PROPERTIES']['REGISTRATION_LINK']['DISPLAY_VALUE'])): ?>
                        <br>
                        <a href="$arItem['DISPLAY_PROPERTIES']['REGISTRATION_LINK']['DISPLAY_VALUE']">Записаться</a>
                    <?php endif; ?>
                </td>
                <td><?php echo $arItem['DISPLAY_PROPERTIES']['EXECUTOR']['DISPLAY_VALUE']; ?></td>
            </tr>
        <?php endforeach; ?>
       </tbody>
     </table>
</div>