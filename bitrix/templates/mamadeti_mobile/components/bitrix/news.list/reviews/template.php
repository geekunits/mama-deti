<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php if ($arResult['ITEMS']): ?>
    <?php foreach ($arResult['ITEMS'] as $arItem): ?>
        <div class="doctor-reviews__item">
            <div class="doctor-reviews__date"><?php echo $arItem['NAME']; ?></div>
            <div class="js-txt-cut" data-more="читать полностью"><?php echo $arItem['PREVIEW_TEXT']; ?></div>
            <div class="doctor-reviews__author"><?php echo $arItem['DATE']; ?></div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <p>Нет отзывов.</p>
<?php endif; ?>

<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?php echo $arResult["NAV_STRING"]?>
<?php endif;?>