<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php if ($arResult['ITEMS']): ?>
    <?php foreach ($arResult['ITEMS'] as $arItem): ?>
        <div class="reviews__item">
            <?php if ($arItem['DOCTOR_IDS']): ?>
                <?php if (count($arItem['DOCTOR_IDS']) > 1): ?>
                    <span class="reviews__doctor-date"><?php echo $arItem['DATE']; ?> оставлен отзыв о</span>
                <?php endif; ?>
                <?php foreach ($arItem['DOCTOR_IDS'] as $index => $id): ?>
                    <?php $arDoctor = $arResult['DOCTORS'][$id]; ?>
                    <a 
                        href="<?php echo $arDoctor['DETAIL_PAGE_URL'] ?>?clinic=<?php echo ClinicManager::getInstance()->getCurrentClinic()['ID']; ?>" 
                        class="reviews__doctor <?php echo count($arItem['DOCTOR_IDS']) > 1 ? 'reviews__doctor--size_small' : ''; ?> clearfix"
                    >
                        <?php if ($arDoctor['DETAIL_PICTURE']): ?>
                            <span class="reviews__doctor-pic"><img src="<?php echo $arDoctor['DETAIL_PICTURE']['src']; ?>"></span>
                        <?php else: ?>
                            <span class="reviews__doctor-pic nophoto--doctor_<?php echo $arItem['PROPERTY_SEX_VALUE'] === 'Ж' ? 'female' : 'male'; ?>"></span>
                        <?php endif; ?>
                        <span class="reviews__doctor-info">
                            <?php if (count($arItem['DOCTOR_IDS']) === 1): ?>
                                <?php
                                    $dt =  DateTime::createFromFormat ('Y.m.d', $arItem['CREATED_DATE']);
                                ?>
                                <span class="reviews__doctor-date"><?php echo ($arItem['ACTIVE_FROM'] ? FormatDate('d.m.Y', MakeTimeStamp($arItem['ACTIVE_FROM'])) : $dt->format('d.m.Y')); ?> оставлен отзыв о</span>
                            <?php endif; ?>
                            <span class="reviews__doctor-name"><?php echo $arDoctor['NAME']; ?></span>
                        </span>
                    </a>
                <?php endforeach; ?>
            <?php elseif ($arItem['CLINIC_IDS']): ?>
                <?php foreach ($arItem['CLINIC_IDS'] as $index => $id): ?>
                    <?php $arClinic = $arResult['CLINICS'][$id]; ?>
                    <a href="<?php echo $arDoctor['DETAIL_PAGE_URL'] ?>" class="reviews__doctor clearfix">
                        <?php if ($arClinic['DETAIL_PICTURE']): ?>
                            <span class="reviews__doctor-pic"><img src="<?php echo $arClinic['DETAIL_PICTURE']['src']; ?>"></span>
                        <?php endif; ?>
                        <span class="reviews__doctor-info">
                            <?php if ($index === 0): ?>
                                <span class="reviews__doctor-date"><?php echo $arItem['DATE']; ?> оставлен отзыв о</span>
                            <?php endif; ?>
                            <span class="reviews__doctor-name"><?php echo $arClinic['NAME']; ?></span>
                        </span>
                    </a>
                <?php endforeach; ?>
            <?php else: ?>
                <span class="reviews__doctor-info">
                    <span class="reviews__doctor-date"><?php echo $arItem['DATE']; ?> оставлен отзыв</span>
                </span>
            <?php endif; ?>
            <div class="reviews__txt js-txt-cut" data-more="читать полностью"><?php echo $arItem['PREVIEW_TEXT']; ?></div>
            <div class="reviews__author"><?php echo $arItem['NAME']; ?></div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <p>Нет отзывов.</p>
<?php endif; ?>

<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?php echo $arResult["NAV_STRING"]?>
<?php endif;?>