<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
    $doctorIds = [];
    $clinicIds = [];

    foreach($arResult["ITEMS"] as &$arItem) {
        $date = split(' ',$arItem['TIMESTAMP_X']);
        $arItem['DATE'] = $date[0];

        if ($arItem['PROPERTIES']['DOCTOR']['VALUE']) {
            $reviewDoctorIds = $arItem['PROPERTIES']['DOCTOR']['VALUE'];
            $reviewDoctorIds = is_array($reviewDoctorIds) ? $reviewDoctorIds : [$reviewDoctorIds];
            $doctorIds = array_merge($doctorIds, $reviewDoctorIds);
            $arItem['DOCTOR_IDS'] = array_values($reviewDoctorIds);
        } elseif ($arItem['PROPERTIES']['CLINIC']['VALUE']) {
            $reviewClinicIds = $arItem['PROPERTIES']['CLINIC']['VALUE'];
            $reviewClinicIds = is_array($reviewClinicIds) ? $reviewClinicIds : [$reviewClinicIds];
            $clinicIds = array_merge($clinicIds, $reviewClinicIds);
            $arItem['CLINIC_IDS'] = array_values($reviewClinicIds);
        }
    }

    $rsDoctors = CIBlockElement::GetList(
        [], 
        ['IBLOCK_ID' => DOCTOR_IBLOCK_ID, 'ID' => $doctorIds, 'ACTIVE' => 'Y',], 
        false, 
        false, 
        ['NAME', 'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'PROPERTY_SEX']
    );
    $arResult['DOCTORS'] = [];
    while ($arDoctor = $rsDoctors->GetNext()) {
        $arDoctor['DETAIL_PICTURE'] = CFile::ResizeImageGet(
            $arDoctor['DETAIL_PICTURE'], 
            array('width'=>54,'height'=>54), 
            BX_RESIZE_IMAGE_EXACT
        );
        $arResult['DOCTORS'][$arDoctor['ID']] = $arDoctor;
    }
    unset($rsDoctors);
    
    foreach($arResult["ITEMS"] as &$arItem) {
        $arItem["DOCTOR_IDS"] = array_intersect($arItem["DOCTOR_IDS"], array_keys($arResult["DOCTORS"]));
    }

    $rsClinics = CIBlockElement::GetList(
        [], 
        ['IBLOCK_ID' => CLINIC_IBLOCK_ID,], 
        false, 
        false, 
        ['NAME', 'DETAIL_PAGE_URL', 'DETAIL_PICTURE']
    );
    $arResult['CLINICS'] = [];
    while ($arClinic = $rsClinics->GetNext()) {
        $arClinic['DETAIL_PICTURE'] = CFile::ResizeImageGet(
            $arClinic['DETAIL_PICTURE'], 
            array('width'=>54,'height'=>54), 
            BX_RESIZE_IMAGE_EXACT
        );
        $arResult['CLINICS'][$arClinic['ID']] = $arClinic;
    }
    unset($rsClinics);
?>
