<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>
<?

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>
<div class="pager">
<?
if($arResult["bDescPageNumbering"] === true):
	$bFirst = true;
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
		if($arResult["bSavePage"]):
?>
			<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">Предыдущие</a></div>
<?
		else:
			if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):
?>
			<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">Предыдущие</a></div>
<?
			else:
?>
			<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">Предыдущие</a></div>
<?
			endif;
		endif;
		
	endif;
	
	if ($arResult["NavPageNomer"] > 1):
?>
		<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">Следующие</a></div>
<?
	else:
?>
<?
	endif; 

else:
	$bFirst = true;

	if ($arResult["NavPageNomer"] > 1):
		if($arResult["bSavePage"]):
?>
			<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">Предыдущие</a></div>
<?
		else:
			if ($arResult["NavPageNomer"] > 2):
?>
			<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">Предыдущие</a></div>
<?
			else:
?>
			<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">Предыдущие</a></div>
<?
			endif;
		
		endif;
	else:
?>
		<div class="pager__item"></div>
<?	
	endif;
	
	if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
?>
		<div class="pager__item"><a class="form-input form-input--border_red js-hover" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">Следующие</a></div>
<?
	endif;
endif;
?>
</div>