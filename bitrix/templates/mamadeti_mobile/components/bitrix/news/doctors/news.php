<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<?php
    $cityId = CityManager::getInstance()->getCurrentCityId();
    $clinicIds = CMamaDetiAPI::GetClinicsID(['PROPERTY_CITY' => $cityId]);
    $requestLetter = isset($_REQUEST['alphabet']) ? htmlspecialchars($_REQUEST['alphabet']) : null;
    $requestClinicId = isset($_REQUEST['clinic']) ? intval($_REQUEST['clinic']) : null;
    $requestSpecialityId = isset($_REQUEST['specialty']) ? intval($_REQUEST['specialty']) : null;

    $arFilter = [
        'ACTIVE' => 'Y',
        'PROPERTY_CLINIC' => $clinicIds,
    ];

    if ($requestClinicId) {
        $arFilter['PROPERTY_CLINIC'] = $requestClinicId;
    }

    if ($requestSpecialityId) {
        $arFilter['PROPERTY_SPECIALTY'] = $requestSpecialityId;
    }

    if ($requestLetter) {
        $arFilter['NAME'] = $requestLetter.'%';
    }

    $arClinics = CMamaDetiAPI::getDoctorsClinics(array_merge($arFilter, ['PROPERTY_CLINIC' => $clinicIds]));

    $arSpecialty = CMamaDetiAPI::getDoctorsSpecialty(array_merge($arFilter, ['PROPERTY_SPECIALTY' => null]));

    $arAlphabet = CMamaDetiAPI::getDoctorsFirstAlphabet(array_merge($arFilter, ['NAME' => null]));

    global $arDoctorFilter;
    $arDoctorFilter = $arFilter;
?>
<form class="filter js-doctor-filter" method="GET">
	<div class="filter__item">
		<div class="form-input form-input--border_red form-select js-hover js-select-wrap">
			<i class="form-select__arr fa fa-chevron-down"></i>
			<div class="form-select__txt js-select-current"><?php echo isset($arClinics[$requestClinicId]) ? $arClinics[$requestClinicId] : 'Врачи из любой клиники'; ?></div>
			<select name="clinic" class="js-select">
				<option value="" <?php echo !$requestClinicId ? 'selected' : ''; ?>>Любая клиника</option>
				<?php foreach ($arClinics as $clinicId => $arClinic): ?>
					<option value="<?php echo $clinicId; ?>" <?php echo $clinicId == $requestClinicId ? ' selected' : ''; ?>>
						<?php echo $arClinic; ?>
					</option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
    <div class="filter__item">
        <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
            <i class="form-select__arr fa fa-chevron-down"></i>
            <div class="form-select__txt js-select-current"><?php echo isset($arSpecialty[$requestSpecialityId]) ? $arSpecialty[$requestSpecialityId] : 'Любой специальности'; ?></div>
            <select name="specialty" class="js-select">
                <option value="" <?php echo !$requestClinicId ? 'selected' : ''; ?>>Любой специальности</option>
                <?php foreach ($arSpecialty as $specialityId => $specialityName): ?>
                    <option value="<?php echo $specialityId; ?>" <?php echo $specialityId == $requestSpecialityId ? ' selected' : ''; ?>>
                        <?php echo $specialityName; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="filter__item">
        <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
            <i class="form-select__arr fa fa-chevron-down"></i>
            <div class="form-select__txt"><?php echo $requestLetter ? 'Фамилия на букву «<strong class="js-select-current">'.$requestLetter.'</strong>»' : 'Фамилия на любую букву'; ?></div>
            <select name="alphabet" class="js-select">
                <option value="" <?php echo !$requestLetter ? 'selected' : ''; ?>>все буквы</option>
                <?php foreach ($arAlphabet as $letter): ?>
                    <option value="<?php echo $letter; ?>" <?php echo $letter == $requestLetter ? ' selected' : ''; ?>><?php echo $letter; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</form>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doctors",
	Array(
		"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
		"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
		"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
		"SORT_BY1"	=>	$arParams["SORT_BY1"],
		"SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
		"SORT_BY2"	=>	$arParams["SORT_BY2"],
		"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
		"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
		"SET_TITLE"	=>	$arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
		"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
		"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
		"DISPLAY_NAME"	=>	$arParams["DISPLAY_NAME"],
		"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME"	=>	'arDoctorFilter',
		"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
	),
	$component
);?>
