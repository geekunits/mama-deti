<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters["CLINIC_ID"] = array(
    "NAME" => 'Clinic id',
    "DEFAULT" => "",
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
);

?>