<?php

$service = Service::getInstance()->findService('section', $arResult['VARIABLES']['SECTION_CODE']);

$parentServiceIds = CMamaDetiAPI::getSectionParentIds($service['ID']);
if ($parentServiceIds) {
    $parentService = Service::getInstance()->findService('section', end($parentServiceIds));
}

$APPLICATION->SetPageProperty('h1', $service['NAME']);

require_once __DIR__.'/_service.php';