<?php

$service = Service::getInstance()->findService('section', $arResult['VARIABLES']['SECTION_CODE'], $arResult['CLINIC']['ID']);
$clinicServiceSectionId = CMamaDetiAPI::getClinicsServiceSectionsID(['ID' => $arResult['CLINIC']['ID']]);
if (!in_array($service['ID'], $clinicServiceSectionId)) {
    LocalRedirect($arResult['CLINIC']['DETAIL_PAGE_URL'].'services/');
}

$parentServiceIds = CMamaDetiAPI::getSectionParentIds($service['ID']);
if ($parentServiceIds) {
	$parentService = Service::getInstance()->findService('section', end($parentServiceIds), $arResult['CLINIC']['ID']);
}
require_once __DIR__.'/_service.php';