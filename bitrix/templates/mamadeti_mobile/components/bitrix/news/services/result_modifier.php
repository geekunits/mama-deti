<?php

if ($_REQUEST['CLINIC_CODE']) {
    $arClinic =
        CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => CLINIC_IBLOCK_ID, 'CODE' => $_REQUEST['CLINIC_CODE']],
            false,
            false,
            ['IBLOCK_ID', 'ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PROPERTY_NAME_GENITIVE', 'PROPERTY_NAME_PREPOSITIONAL', 'PROPERTY_PHONE', 'PROPERTY_METRIC_ID', 'PROPERTY_CITY']
        )->GetNext();
    $arClinic['DOCTOR_IDS'] = CMamaDetiAPI::getDoctorsID(['PROPERTY_CLINIC' => $arClinic['ID']]);

    if ($arClinic['ID']) {
        $arClinic['SERVICE_IDS'] = array();
        $dbProperty = CIBlockElement::GetProperty(CLINIC_IBLOCK_ID, $arClinic['ID'], array('SORT' => 'ASC'), array('CODE' => 'SERVICES'));
        while ($arService = $dbProperty->GetNext()) {
            $arClinic['SERVICE_IDS'][] = $arService['VALUE'];
        }

    }
    $arResult['CLINIC'] = $arClinic;

    ClinicManager::getInstance()->setCurrentClinicId($arClinic['ID']);
}