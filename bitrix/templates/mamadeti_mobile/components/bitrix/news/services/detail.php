<?php

$service = Service::getInstance()->findService('element', $arResult['VARIABLES']['ELEMENT_CODE'], $arResult['CLINIC']['ID']);
$clinicServiceId = CMamaDetiAPI::getClinicsServicesID(['ID' => $arResult['CLINIC']['ID']]);
if (!in_array($service['ID'], $clinicServiceId)) {
    LocalRedirect($arResult['CLINIC']['DETAIL_PAGE_URL'].'services/');
}

$parentServiceIds = CMamaDetiAPI::getElementParentIds($service['ID']);
if ($parentServiceIds) {
	$parentService = Service::getInstance()->findService('section', end($parentServiceIds), $arResult['CLINIC']['ID']);
}

require_once __DIR__.'/_service.php';