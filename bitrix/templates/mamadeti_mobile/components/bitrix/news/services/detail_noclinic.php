<?php

$service = Service::getInstance()->findService('element', $arResult['VARIABLES']['ELEMENT_CODE']);

$parentServiceIds = CMamaDetiAPI::getElementParentIds($service['ID']);
if ($parentServiceIds) {
    $parentService = Service::getInstance()->findService('section', end($parentServiceIds));
}

$APPLICATION->SetPageProperty('h1', $service['NAME']);

require_once __DIR__.'/_service.php';