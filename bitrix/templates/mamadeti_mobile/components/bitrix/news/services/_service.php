<div class="clinic">
	<?php if ($arResult['CLINIC']): ?>
		<?php
			$urls = [];
			// $urls[] = [
			//     'NAME' => 'Все услуги клиники',
			//     'URL' => ClinicManager::getInstance()->getCurrentClinic()['DETAIL_PAGE_URL'].'services/',
			// ];
			if ($parentService) {
				$urls[] = [
					'NAME' => $parentService['NAME'], 
					'URL' => $parentService['URL'].'clinic/'.ClinicManager::getInstance()->getCurrentClinic()['CODE'].'/',
				];
			}
			$serviceIds = CMamaDetiAPI::GetClinicsServicesID(['ID' => $arResult['CLINIC']['ID']]);
			$arResult['SERVICES'] = CMamaDetiAPI::GetSelectedServiceTreeMenu($serviceIds, true, 2);
			$serviceManager = Service::getInstance();
			$previousLevel = 0;
		?>

		<?php $APPLICATION->IncludeFile(
			'/includes/clinic_header.php', 
			[
				'showBook' => true,
				'showBack' => true,
				'subUrls' => $urls,
			]
		); ?>

		<?php
			$childServices = 
				($service['SERVICE_TYPE'] === 'section')
					? $serviceManager->findServiceSectionChildren($service['ID'], $arResult['CLINIC']['ID'])
					: [];
			$APPLICATION->SetPageProperty('h1', $service['NAME']);
		?>

		<div class="clinic__subneader">
			<?php echo $service['NAME']; ?> 
		</div>
		<div class="services">
			<div class="services__item">
				<div class="services__content">
					<div class="services__content-txt">
						<div class="js-txt-cut" data-height="100">
							<?php echo UrlReplacer::toMobile($service['DESCRIPTION']); ?>
						</div>
					</div>
					<?php if ($childServices): ?>
						<div class="services__content-links">
							<?php foreach ($childServices as $childService): ?>
								<a href="<?php echo $childService['URL'].'clinic/'.$arResult['CLINIC']['CODE'].'/'; ?>" class="js-hover"><i class="fa fa-chevron-right"></i><?php echo $childService['NAME']; ?></a>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="clinic__subneader">
			Все услуги клиники
		</div>

		<div class="services">
			<?php for ($index = 0, $arItems = $arResult['SERVICES'], $count = count($arResult['SERVICES']); $index < $count; $index++): ?>
		        <?php
		            $arItem = $arItems[$index];
		            if ($arItem['DEPTH_LEVEL'] > 2) {
		                continue;
		            }

		            $arNextItem = $index + 1 < $count ? $arItems[$index + 1] : null;
		            $hasSubItems = $arNextItem && $arNextItem['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'];
		            
		        ?>
		        <?php for ($depth = $previousLevel; $depth > $arItem['DEPTH_LEVEL']; $depth--): ?>
		            </div></div></div>
		        <?php endfor; ?>

		        <?php if ($arItem['DEPTH_LEVEL'] == 1): ?>
		            <div class="services__item js-tree-item">
		                <div class="services__head js-tree-head js-hover">
		                    <div class="price__head-switch js-tree-switch">+</div>
		                    <?php echo $arItem['TEXT']; ?>
		                </div>
		                <div class="services__content js-tree-content">
		                    <div class="services__content-txt">
		                        <div class="<?php if ($hasSubItems): ?>js-txt-cut<?php endif; ?>" data-height="100"><?php echo UrlReplacer::toMobile($serviceManager->findService('section', $arItem['SECTION_ID'], $arResult['CLINIC']['ID'])['DESCRIPTION']); ?></div>
		                    </div>
		                    <?php if ($hasSubItems): ?>
		                        <div class="services__content-links">
		                    <?php else: ?>
		                        </div></div>
		                    <?php endif; ?>
		        <?php else: ?>
		            <a href="<?php echo $arItem['LINK'].'clinic/'.$arResult['CLINIC']['CODE'].'/'; ?>" class="js-hover"><i class="fa fa-chevron-right"></i><?php echo $arItem['TEXT']; ?></a>
		        <?php endif; ?>

		        <?php $previousLevel = $arItem['DEPTH_LEVEL']; ?>
		    <?php endfor; ?>
		    <?php for ($depth = $previousLevel; $depth > 1; $depth--): ?>
		        </div></div></div>
		    <?php endfor; ?>

		</div>
	<?php else: ?>
		<div class="services">
			<div class="services__item">
				<div class="services__content">
					<div class="services__content-txt">
						<div class="js-txt-cut" data-height="100">
							<?php echo UrlReplacer::toMobile($service['DESCRIPTION']); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php if ($service['SERVICE_IDS']): ?>
			<div class="select-clinic__note"><strong>Услуга оказывается в следующих клиниках:</strong></div>
		    <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
		        <i class="form-select__arr fa fa-chevron-down"></i>
		        <div class="form-select__txt js-select-current">Выберите одну из клиник</div>
		        <?php
		        	$rawClinics = CMamaDetiAPI::GetClinics([
        				'PROPERTY_CITY' => CityManager::getInstance()->getCurrentCity()['ID'], 
        				'PROPERTY_SERVICES' => $service['SERVICE_IDS']
        			]);
        			if ($_REQUEST['doctor']) {
        			    $doctorClinicIds = array_keys(CMamaDetiAPI::getDoctorsClinics(['ID' => $_REQUEST['doctor']]));
        			    $rawClinics = array_filter($rawClinics, function($clinic) use ($doctorClinicIds) {
        			        return in_array($clinic['ID'], $doctorClinicIds);
        			    });
        			}
        			
		        	$clinics = [];
		        	foreach ($rawClinics as $clinic) {
		        		$clinics[$clinic['ID']] =  $clinic;
		        	}
		        	$clinics = array_values($clinics);
		        ?>
		        <select id="select-clinic" name="" class="js-select js-clinic-selector">
		                <option value="">Выберите клинику</option>
		            	<?php foreach ($clinics as $clinic): ?>
		                    <option value="<?php echo $service['URL'].'clinic/'.$clinic['CODE'].'/'; ?>"><?php echo $clinic['NAME']; ?></option>
		            	<?php endforeach; ?>
		        </select>
		    </div>
		<?php endif; ?>
	<?php endif; ?>
</div>
