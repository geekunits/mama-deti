<?php

	$filter = [
		'IBLOCK_ID' => $iBlockId,
		'UF_CLINIC' => $clinicId,
		'DEPTH_LEVEL' => 1,
		'ACTIVE' => 'Y',
	];
	$select = ['ID', 'UF_CLINIC', 'NAME', 'IBLOCK_ID', 'LEFT_MARGIN', 'RIGHT_MARGIN'];
	$query = CIBlockSection::GetList([], $filter, false, $select);
	$parentSection = $query->GetNext();
	$sections = [];

	if ($parentSection) {
		$sectionFilter = [
			'IBLOCK_ID' => $iBlockId,
			'ACTIVE' => 'Y',
			'SECTION_ID' => $parentSection['ID'],
			'DEPTH_LEVEL' => 2,
		];

		$elementFilter = [
			'IBLOCK_ID' => $iBlockId,
			'ACTIVE' => 'Y',
			'!PROPERTY_PRICE' => false,
			'INCLUDE_SUBSECTIONS' => 'Y',
		];

		$query = CIBlockSection::GetList(['NAME' => 'ASC'], $sectionFilter, false, ['ID', 'SECTION_ID', 'IBLOCK_ID', 'NAME', 'DEPTH_LEVEL']);
		while(($s = $query->GetNext()) !== false) {
			$elementFilter['SECTION_ID'] = $s['ID'];
			$count = CIBlockElement::GetList([], $elementFilter, []);
			if ($count) {
				$sections[] = $s;
			}
		}
	}
	$clinicText = $clinic['NAME'];
	$sectionText = !empty($section) ? $section['NAME'] : 'Выберите раздел';

?>
<form method="GET" class="filter" action="<?php echo $sefFolder; ?>">
	<div class="filter__item">
		<div class="select-price-clinic form-input form-input--border_red form-select js-hover js-select-wrap">
			<i class="form-select__arr fa fa-chevron-down"></i>
			<div class="form-select__txt js-select-current"><?php echo $clinicText; ?></div>
	        <select name="filter_clinic" class="js-select">
	            <?php foreach ($clinics as $clinic): ?>
	                <option value="<?php echo $clinic['ID']; ?>"<?php if ($clinicId == $clinic['ID']): ?> selected="selected"<?php endif; ?>><?php echo $clinic['NAME']; ?></option>
	            <?php endforeach; ?>
	        </select>
	    </div>
	</div>
	<div class="filter__item">
		<div class="select-price-section  form-input form-input--border_red form-select js-hover js-select-wrap">
	        <i class="form-select__arr fa fa-chevron-down"></i>
	        <div class="form-select__txt js-select-current"><?php echo $sectionText; ?></div>
	        <select name="filter_section" class="js-select">
	            <option value="">Выберите раздел</option>
	            <?php foreach ($sections as $section): ?>
	                <option value="<?php echo $section['ID']; ?>"<?php if ($sectionId == $section['ID']): ?> selected="selected"<?php endif; ?>><?php echo $section['NAME']; ?></option>
	            <?php endforeach; ?>
	        </select>
		</div>
	</div>
</form>