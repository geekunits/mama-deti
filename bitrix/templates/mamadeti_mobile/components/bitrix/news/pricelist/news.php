<?php
$clinic = null;
$clinicId = isset($_GET['filter_clinic']) ? (int) $_GET['filter_clinic'] : null;
$allClinics = ClinicManager::getInstance()->getClinics();
$clinicIds = [];
foreach($allClinics as $c) {
	$clinicIds[] = $c['ID'];
}
$filteredClinicIds = [];
$sectionFilter = [
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'UF_CLINIC' => $clinicIds,
	'ACTIVE' => 'Y',
	'DEPTH_LEVEL' => 1,
];
$query = CIBlockSection::GetList(['SORT' => 'ASC'], $sectionFilter, false, ['ID', 'IBLOCK_ID', 'UF_CLINIC']);
while($s = $query->GetNext()) {
	$filteredClinicIds[$s['UF_CLINIC']] = $s['UF_CLINIC'];
}
$filteredClinics = [];
foreach($allClinics as $c) {
	if (isset($filteredClinicIds[$c['ID']])) {
		$filteredClinics[$c['ID']] = $c;
	}
}
if (!empty($clinicId)) {
	$clinic = isset($filteredClinics[$clinicId]) ? $filteredClinics[$clinicId] : null;
}
?>
<?php if (empty($clinic)): ?>
	<?php 
		$APPLICATION->IncludeFile(
		    '/includes/clinic_select.php', [
		    	'urlCallback' => function($clinic) use ($arParams) {
		    		return $arParams['SEF_FOLDER'] . '?filter_clinic=' . $clinic['ID'];
		    	},
		    	'clinics' => $filteredClinics,
		    ]
		); 
	?>
<?php else: ?>
	<?php
	    global $priceFilter;
	    $showExpanded = false;
	    $priceFilter = [
	        "!PROPERTY_PRICE" => false,
	    ];
		$sectionId = isset($_GET['filter_section']) ? (int) $_GET['filter_section'] : null;
		$section = null;
		if (!empty($sectionId)) {

			$filter = [
				'ID' => $sectionId, 
				'IBLOCK_ID' => $arParams['IBLOCK_ID'], 
				'ACTIVE' => 'Y',
			];
			$select = ['ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'UF_CLINIC', 'NAME', 'DEPTH_LEVEL'];
			$query = CIBlockSection::GetList(['SORT' => 'ASC'], $filter, false, $select);
			$section = $query->GetNext();

			if (!empty($section) && !empty($section['IBLOCK_SECTION_ID'])) {
				$filter['ID'] = $section['IBLOCK_SECTION_ID'];
				$filter['UF_CLINIC'] = $clinicId;
				$query = CIBlockSection::GetList(['SORT' => 'ASC'], $filter, false, $select);
				$topSection = $query->GetNext();
				if (!empty($topSection)) {
					$priceFilter['SECTION_ID'] = $sectionId;
					$priceFilter['INCLUDE_SUBSECTIONS'] = 'Y';
					$showExpanded = true;
				} else {
					$section = null;
				}
			}
		}

		if ($_GET['q']) {
			$priceFilter['?NAME'] = $_GET['q'];
			$showExpanded = true;
		}
	?>
	<?php $APPLICATION->IncludeFile($this->GetFolder() . '/_filter.php', [
		'clinics' => $filteredClinics,
		'clinicId' => $clinicId,
		'sectionId' => $sectionId,
		'clinic' => $clinic,
		'section' => $section,
		'iBlockId' => $arParams['IBLOCK_ID'],
		'sefFolder' => $arParams['SEF_FOLDER'],
	]); ?>
	<?$APPLICATION->IncludeComponent("melcosoft:price.list", "clinic", array(
	    "IBLOCK_TYPE" => "contentsite",
	    "IBLOCK_ID" => $arParams['IBLOCK_ID'],
	    "FILTER_NAME" => "priceFilter",
	    "CLINIC_ID" => $clinicId,
	    "PAGE_ELEMENT_COUNT" => "50000",
	    "AJAX_MODE" => "N",
	    "AJAX_OPTION_JUMP" => "N",
	    "AJAX_OPTION_STYLE" => "Y",
	    "AJAX_OPTION_HISTORY" => "N",
	    "CACHE_TYPE" => "A",
	    "CACHE_TIME" => "36000000",
	    "CACHE_GROUPS" => "Y",
	    "CACHE_FILTER" => "Y",
	    "PAGER_TEMPLATE" => "mamadeti",
	    "DISPLAY_TOP_PAGER" => "N",
	    "DISPLAY_BOTTOM_PAGER" => "Y",
	    "PAGER_TITLE" => "",
	    "PAGER_SHOW_ALWAYS" => "N",
	    "PAGER_DESC_NUMBERING" => "N",
	    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	    "PAGER_SHOW_ALL" => "N",
	    "AJAX_OPTION_ADDITIONAL" => "",
	    "DISABLE_PRICE_LIST_AJAX" => "Y",
	    "_REQUEST" => $_REQUEST,
	    'SHOW_MORE_BUTTON' => 'Y',
	    'SHOW_EXPANDED' => $showExpanded ? 'Y' : 'N',
	    ),
	    false
	);?>
<?php endif; ?>