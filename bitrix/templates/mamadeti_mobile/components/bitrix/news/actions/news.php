<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<?php
    $cityId = CityManager::getInstance()->getCurrentCityId();
    $requestClinicId = isset($_REQUEST['clinic_id']) ? intval($_REQUEST['clinic_id']) : null;
    $requestServiceId = isset($_REQUEST['service_id']) ? intval($_REQUEST['service_id']) : null;

    $arClinicFilter = array(
        'IBLOCK_ID' => 2,
        'ACTIVE' => 'Y',
        'PROPERTY_CITY' => $cityId,
    );
    $arClinicSelect = array(
        'PROPERTY_SERVICES',
    );
    $arClinicsRaw = CMamaDetiAPI::GetClinics($arClinicFilter, $arClinicSelect);
    $arClinics = array();
    $arAllServiceIds = array();
    $arAllClinicIds = array();
    foreach ($arClinicsRaw as $arClinicRaw) {
        $rawId = $arClinicRaw['ID'];
        $serviceId = $arClinicRaw['PROPERTY_SERVICES_VALUE'];
        $arAllClinicIds[$rawId] = $rawId;
        $arAllServiceIds[$serviceId] = $serviceId;
        if (empty($arClinics[$rawId])) {
            $arClinics[$rawId] = $arClinicRaw;
            $arClinics[$rawId]['SERVICES'] = array();
            $arClinics[$rawId]['ACTIONS'] = array();
        }
        $arClinics[$rawId]['SERVICES'][$serviceId] = $serviceId;
    }

    if (!empty($requestClinicId) && !in_array($requestClinicId, $arAllClinicIds)) {
        $requestClinicId = null;
    }

    $arServiceTree = CMamaDetiAPI::GetSelectedServiceTree($arAllServiceIds);

    $arActionFilter = array(
        'IBLOCK_ID' => 32,
        'ACTIVE' => 'Y',
        'PROPERTY_CLINIC' => $arAllClinicIds,
        'ACTIVE_DATE' => 'Y',
        // 'PROPERTY_SERVICES' => $arAllServiceIds,
    );

    $arActionSelect = array(
        'IBLOCK_ID',
        'ID',
        'NAME',
        'PROPERTY_CLINIC',
        'PROPERTY_SERVICES',
    );

    $dbAction = CIBlockElement::GetList(array('SORT' => 'ASC'), $arActionFilter, false, false, $arActionSelect);

    $arActions = array();
    $arServiceIds = array();
    $arClinicIds = array();
    $arActiveClinicIds = array();
    $arActiveActionIds = array();
    $arActiveServiceIds = array();
    $arClinicServiceIds = array();
    $arActionClinicIds = array();
    $arServiceClinicIds = array();
    // Для каждой акции выбираем доступные клиники и доступные сервисы
    while ($arRawAction = $dbAction->GetNext()) {
        $rawId = $arRawAction['ID'];
        $clinicId = $arRawAction['PROPERTY_CLINIC_VALUE'];
        $serviceId = $arRawAction['PROPERTY_SERVICES_VALUE'];
        if (empty($arActions[$rawId])) {
            $arActions[$rawId] = $arRawAction;
            $arActions[$rawId]['SERVICES'] = array();
            $arActions[$rawId]['CLINIC'] = array();
        }
        $arAction[$rawId]['CLINIC'][$clinicId] = $arClinics[$clinicId];
        $arActions[$rawId]['SERVICES'][$serviceId] = $serviceId;
        $arActionClinicIds[$clinicId] = $clinicId;
        $arSerivceClinicIds[$serviceId] = array();
        if (!empty($serviceId) && !empty($clinicId)) {
            if (empty($arSerivceClinicIds[$serviceId])) {
                $arServiceClinicIds[$serviceId][$clinicId] = $clinicId;
            }
        }
        if (empty($requestClinicId) || $requestClinicId == $clinicId) {
            $arActiveClinicIds[$clinicId] = $clinicId;
            $arClinicServiceIds[$serviceId] = $serviceId;
        }
    }
    // Фильтруем дерево сервисов и оставляем только те, где есть акции
    $arTopServices = array();
    $arCurrentTopService = null;
    $arActiveServiceIds = array();
    $arActiveServiceClinicIds = array();
    $showAllServices = false;
    foreach ($arServiceTree as $arService) {
        $serviceId = substr($arService['ID'], 1);
        if ($arService['IS_PARENT']) {
            if ($arService['DEPTH_LEVEL'] == 1) {
                if (!empty($arCurrentTopService) && !empty($arCurrentTopService['SUB_SERVICE_IDS'])) {
                    $topServiceId = substr($arCurrentTopService['ID'], 1);
                    $arTopServices[$topServiceId] = $arCurrentTopService;
                    if (!empty($requestServiceId) && $topServiceId == $requestServiceId) {
                        $showAllServices = true;
                    }
                }
                $arCurrentTopService = $arService;
                $arCurrentTopService['SUB_SERVICE_IDS'] = array();
                $arCurrentTopService['CLINIC_IDS'] = array();
            }
        } else {
            if (!empty($requestServiceId) && 'S'.$requestServiceId == $arCurrentTopService['ID'] && isset($arServiceClinicIds[$serviceId])) {
                foreach ($arServiceClinicIds[$serviceId] as $clinicId) {
                    $arActiveServiceClinicIds[$clinicId] = $clinicId;
                }
            }

            if (isset($arClinicServiceIds[$serviceId])) {
                $arCurrentTopService['SUB_SERVICE_IDS'][$serviceId] = $serviceId;
                if (empty($requestServiceId) || 'S'.$requestServiceId == $arCurrentTopService['ID']) {
                    $arActiveServiceIds[$serviceId] = $serviceId;
                }
            }
        }
    }

    if (!empty($requestServiceId) && !isset($arTopServices[$requestServiceId])) {
        $requestServiceId = null;
    }

    $arActiveClinics = array();
    $arFilterClinicIds = empty($arActiveServiceClinicIds) ? $arActionClinicIds : $arActiveServiceClinicIds;
    foreach ($arClinics as $clinicId => $arClinic) {
        if (isset($arFilterClinicIds[$clinicId])) {
            $arActiveClinics[$clinicId] = $arClinic;
        }
    }

    global $arActionsFilter;
    global $APPLICATION;
    $arActionsFilter = array(
        'PROPERTY_CLINIC' => $arActiveClinicIds,
        'ACTIVE' => 'Y',
        'ACTIVE_DATE' => 'Y',
    );
    if (!empty($requestServiceId)) {
        $arActionsFilter['PROPERTY_SERVICES'] = $arActiveServiceIds;
    }
    $servicePageParams = array();
    if (!empty($requestClinicId)) {
        $servicePageParams['clinic_id'] = $requestClinicId;
    }
    $clinicPageParams = array();
    if (!empty($requestServiceId)) {
        $clinicPageParams['service_id'] = $requestServiceId;
    }
    $pageUri = $APPLICATION->GetCurPage();
?>
<form class="filter js-action-filter" method="GET">
	<div class="filter__item">
		<div class="form-input form-input--border_red form-select js-hover js-select-wrap">
			<i class="form-select__arr fa fa-chevron-down"></i>
			<div class="form-select__txt js-select-current"><?php echo isset($arActiveClinics[$requestClinicId]) ? $arActiveClinics[$requestClinicId]['NAME'] : 'Акции из всех клиник'; ?></div>
			<select name="clinic_id" class="js-select">
				<option value="" <?php echo !$requestClinicId ? 'selected' : ''; ?>>Все клиники</option>
				<?php foreach ($arActiveClinics as $clinicId => $arClinic): ?>
					<option value="<?php echo $clinicId; ?>" <?php echo $clinicId == $requestClinicId ? ' selected' : ''; ?>>
						<?php echo $arClinic['NAME']; ?>
					</option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<?php if ($arTopServices): ?>
	    <div class="filter__item">
		    <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
			    <i class="form-select__arr fa fa-chevron-down"></i>
			    <div class="form-select__txt js-select-current"><?php echo isset($arTopServices[$requestServiceId]) ? $arTopServices[$requestServiceId]['TEXT'] : 'По всем услугам'; ?></div>
			    <select name="service_id" class="js-select">
				    <option value="" <?php echo !$requestServiceId ? 'selected' : ''; ?>>Все услуги</option>
				    <?php foreach ($arTopServices as $serviceId => $arService): ?>
					    <option value="<?php echo $serviceId; ?>" <?php echo $serviceId == $requestServiceId ? ' selected' : ''; ?>>
						    <?php echo $arService['TEXT']; ?>
					    </option>
			    <?php endforeach; ?>
			    </select>
		    </div>
	    </div>
	<?php endif; ?>
</form>

<?php if (count($arActions) > 0): ?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"actions",
		Array(
			"IBLOCK_TYPE"	=>	$arParams["IBLOCK_TYPE"],
			"IBLOCK_ID"	=>	$arParams["IBLOCK_ID"],
			"NEWS_COUNT"	=>	$arParams["NEWS_COUNT"],
			"SORT_BY1"	=>	$arParams["SORT_BY1"],
			"SORT_ORDER1"	=>	$arParams["SORT_ORDER1"],
			"SORT_BY2"	=>	$arParams["SORT_BY2"],
			"SORT_ORDER2"	=>	$arParams["SORT_ORDER2"],
			"FIELD_CODE"	=>	$arParams["LIST_FIELD_CODE"],
			"PROPERTY_CODE"	=>	$arParams["LIST_PROPERTY_CODE"],
			"DETAIL_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
			"SECTION_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"IBLOCK_URL"	=>	$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
			"DISPLAY_PANEL"	=>	$arParams["DISPLAY_PANEL"],
			"SET_TITLE"	=>	$arParams["SET_TITLE"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"INCLUDE_IBLOCK_INTO_CHAIN"	=>	$arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
			"CACHE_TYPE"	=>	$arParams["CACHE_TYPE"],
			"CACHE_TIME"	=>	$arParams["CACHE_TIME"],
			"CACHE_FILTER"	=>	$arParams["CACHE_FILTER"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"DISPLAY_TOP_PAGER"	=>	$arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER"	=>	$arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE"	=>	$arParams["PAGER_TITLE"],
			"PAGER_TEMPLATE"	=>	$arParams["PAGER_TEMPLATE"],
			"PAGER_SHOW_ALWAYS"	=>	$arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_DESC_NUMBERING"	=>	$arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME"	=>	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
			"DISPLAY_DATE"	=>	$arParams["DISPLAY_DATE"],
			"DISPLAY_NAME"	=>	$arParams["DISPLAY_NAME"],
			"DISPLAY_PICTURE"	=>	$arParams["DISPLAY_PICTURE"],
			"DISPLAY_PREVIEW_TEXT"	=>	$arParams["DISPLAY_PREVIEW_TEXT"],
			"PREVIEW_TRUNCATE_LEN"	=>	$arParams["PREVIEW_TRUNCATE_LEN"],
			"ACTIVE_DATE_FORMAT"	=>	$arParams["LIST_ACTIVE_DATE_FORMAT"],
			"USE_PERMISSIONS"	=>	$arParams["USE_PERMISSIONS"],
			"GROUP_PERMISSIONS"	=>	$arParams["GROUP_PERMISSIONS"],
			"FILTER_NAME"	=>	'arActionsFilter',
			"HIDE_LINK_WHEN_NO_DETAIL"	=>	$arParams["HIDE_LINK_WHEN_NO_DETAIL"],
			"CHECK_DATES"	=>	$arParams["CHECK_DATES"],
		),
		$component
	);?>
<?php else: ?>
	На данный момент акций нет.
<?php endif; ?>
