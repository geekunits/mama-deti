<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
$dateFrom = $arResult['DATE_ACTIVE_FROM'];
$dateFrom = preg_replace('/(\d+)\.(\d+)\.(\d+) \d+:\d+:\d+/', '$1.$2.$3', $dateFrom);
?>
<div class="news-detail">
    <div class="news-detail__pic">
        <img src="<?=$arResult['DETAIL_PICTURE']['SRC'] ?: $arResult['PREVIEW_PICTURE']['SRC']?>" alt="">
    </div>
    <div class="news-detail__date"><?php echo $dateFrom; ?></div>
    <div class="news-detail__name"><?php echo $arResult['NAME']; ?></div>
    <div class="news-detail__txt content-scroll"><?=UrlReplacer::toMobile($arResult['DETAIL_TEXT']);?></div>
</div>
