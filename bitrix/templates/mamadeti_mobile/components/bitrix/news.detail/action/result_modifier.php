<?php

$arResult['DOCTOR'] = [];
if (!empty($arResult['DISPLAY_PROPERTIES']['DOCTOR']) && !empty($arResult['DISPLAY_PROPERTIES']['DOCTOR']['DISPLAY_VALUE'])) {
	$arResult['DOCTOR'] = is_array($arResult['DISPLAY_PROPERTIES']['DOCTOR']['DISPLAY_VALUE']) ? 
		$arResult['DISPLAY_PROPERTIES']['DOCTOR']['DISPLAY_VALUE'] : [$arResult['DISPLAY_PROPERTIES']['DOCTOR']['DISPLAY_VALUE']];
}