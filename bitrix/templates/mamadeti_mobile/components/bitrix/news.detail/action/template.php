<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
$dateFrom = $arResult['DISPLAY_PROPERTIES']['DATE_FROM']['DISPLAY_VALUE'];
$dateTo = $arResult['DISPLAY_PROPERTIES']['DATE_TO']['DISPLAY_VALUE'];
$nowDt = new DateTime();
$nowDt->setTime(0, 0, 0);

if ($dateFrom || $dateTo) {
    if ($dateTo) {
        $dateToDt = new DateTime('@'.MakeTimeStamp($dateTo));
        $dateToDt->setTime(23, 59, 59);
        $daysLeft = strval(intval($nowDt->diff($dateToDt)->format('%a')) + 1);
    } else {
        $daysLeft = null;
    }

    if ($dateFrom) {
        $dateFromDt = new DateTime('@'.MakeTimeStamp($dateFrom));
        $dateFromDt->setTime(0, 0, 0);
    }

    $daysSoon = false;
    if ($dateFromDt) {
        $intervalSoon = ($nowDt < $dateFromDt) ? $dateFromDt->diff($nowDt) : false;
        $daysSoon = $intervalSoon ? intval($intervalSoon->format('%a')) + 1 : false;
    }

    $dateRange = '';
    if (!empty($dateTo) && !empty($dateFrom)) {
        if ($dateTo == $dateFrom) {
            $dateRange = $dateTo;
        } else {
            $dateRange = 'c '.$dateFrom.' по '.$dateTo;
        }
    } elseif (!empty($dateTo)) {
        $dateRange = 'по '.$dateTo;
    } elseif (!empty($dateFrom)) {
        $dateRange = 'с '.$dateFrom;
    }
}
?>
<div class="news-detail">
    <div class="news-detail__pic">
        <img src="<?=$arResult['DETAIL_PICTURE']['SRC'] ?: $arResult['PREVIEW_PICTURE']['SRC']?>" alt="">
        <?php if ($daysLeft || $daysSoon): ?>
            <span class="news-list__end<?php echo $daysSoon ? '--soon' : '';?>">
                <?php if ($daysSoon): ?>
                    до начала акции <?php echo $daysSoon.' '.plural($daysSoon, ['день', 'дня', 'дней']); ?>
                <?php elseif ($daysLeft > 1): ?>
                    до окончания акции <?php echo $daysLeft.' '.plural($daysLeft, ['день', 'дня', 'дней']); ?>
                <?php elseif ($daysLeft === 1): ?>
                    остался последний день
                <?php endif; ?>
            </span>
        <?php endif; ?>
    </div>
    <?php if ($dateRange): ?><div class="news-detail__date"><?php echo $dateRange; ?></div><?php endif; ?>
    <div class="news-detail__name"><?php echo $arResult['NAME']; ?></div>
    <div class="news-detail__txt content-scroll"><?=UrlReplacer::toMobile($arResult['DETAIL_TEXT']);?></div>
    <div class="news-detail__clinics">
        <?php if (is_array($arResult['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE'])): ?>
            <p><strong>Акция проводится в клиниках</strong></p>
            <?php foreach ($arResult['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE'] as $clinicName): ?>
                <?php echo $clinicName; ?>
            <?php endforeach; ?>
        <?php else: ?>
                <p><strong>Акция проводится в клинике</strong></p>
                <?php echo $arResult['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']; ?>
        <?php endif; ?>
    </div>
    <?php if (!empty($arResult['DOCTOR'])): ?>
        <div class="news-detail__doctors">
            <p><strong>Врачи, участвующие в акции</strong></p>
            <?php foreach($arResult['DOCTOR'] as $doctor): ?>
                <?php echo $doctor; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
