<div class="clinic__subneader">
    <?php echo $arResult['NAME']; ?>
</div>

<div class="clinic__webinars clinic__static content-scroll">
    <dl>
        <?php $date = explode('&nbsp;', $arResult['DISPLAY_PROPERTIES']['DATE']['DISPLAY_VALUE']); ?>
        <dt><strong>Дата проведения:</strong> <?php echo $date[0]; ?></dt>
        <?php if ($date[1]): ?>
            <dt><strong>Время проведения:</strong> <?php echo $date[1]; ?></dt>
        <?php endif; ?>
        <dt><strong>Место проведения:</strong> <?php echo $arResult['DISPLAY_PROPERTIES']['PLACE']['DISPLAY_VALUE']; ?></dt>
        <dt><strong>Спикер:</strong> <?php echo $arResult['DISPLAY_PROPERTIES']['EXECUTOR']['DISPLAY_VALUE']; ?></dt>
    </dl>
    <div class="bx_rb">
        <div class="item_info_section">
            <div class="bx_item_description">
                <?php echo $arResult['DETAIL_TEXT']; ?>
            </div>
        </div>
    </div>
</div>