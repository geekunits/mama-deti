<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters["TAB_CODE"] = array(
    "NAME" => 'Tab code',
    "DEFAULT" => "",
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
);

$arTemplateParameters["GET_PARAMS"] = array(
    "NAME" => 'Get params',
    "DEFAULT" => "",
    "TYPE" => "STRING",
    "MULTIPLE" => "N",
);

?>