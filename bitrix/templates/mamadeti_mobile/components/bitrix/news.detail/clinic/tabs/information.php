<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Персональные данные
</div>

<div class="clinic__static content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['INFORMATION']['DISPLAY_VALUE']); ?>
</div>