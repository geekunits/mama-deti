<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php',
        [
            'showBook' => empty($arResult['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']),
            'showBack' => false,
        ]
    );

    $hideTabsAfter = 4;
?>
<div class="clinic__menu">
    <?php foreach (array_values($arResult['TABS']) as $index => $tab): ?>
        <?php if ($index === $hideTabsAfter): ?>
            <div class="js-clinic-menu-hidden hidden">
        <?php endif; ?>
        <a href="<?php echo $tab['URL']; ?>" class="clinic__menu-item <?php echo $tab['CLASS']; ?> js-hover"><i class="fa fa-chevron-right"></i><?php echo $tab['NAME']; ?></a>
    <?php endforeach; ?>
    <?php if (count($arResult['TABS']) > $hideTabsAfter): ?>
        </div>
        <div class="clinic__menu-more js-clinic-menu-more js-hover"><i class="fa fa-ellipsis-h"></i>Показать еще разделы</div>
    <?php endif; ?>
</div>

<?php
$servicesList = array('FB' => 'Facebook','VK' => 'Vkontakte','OK' => 'Одноклассники', 'INST' => 'Instagram', 'YOUTUBE' => 'Youtube','TW' => 'Twitter');
$arServices = array();
foreach ($servicesList as $n => $name) {
    if (!empty($arResult['PROPERTIES'][$n . '_URL']['VALUE'])) {
        $url = null;

        ob_start();
        switch ($n) {
		case 'TW':
			?>
			<a target="_blank" href="<?=$arResult['PROPERTIES'][$n . '_URL']['VALUE']?>" class="social__item social__item--tw js-hover"><i class="fa fa-twitter"></i></a>
			<?
		break;
            case 'FB':
                ?>
                <a target="_blank" href="<?=$arResult['PROPERTIES']['FB_URL']['VALUE']?>" class="social__item js-hover"><i class="fa fa-facebook"></i></a>
                <?break;

            case 'VK':
                ?>
                <a target="_blank" href="<?=$arResult['PROPERTIES']['VK_URL']['VALUE']?>" class="social__item social__item--vk js-hover"><i class="fa fa-vk"></i></a>
                <?break;

            case 'OK':
                ?>
                <a target="_blank" href="<?=$arResult['PROPERTIES']['OK_URL']['VALUE']?>" class="social__item social__item--ok js-hover"><i class="fa fa-odnoklassniki"></i></a>
                <?break;

            case 'INST':
                ?>
                <a target="_blank" href="<?=$arResult['PROPERTIES']['INST_URL']['VALUE']?>" class="social__item social__item--instagram js-hover"><i class="fa fa-instagram"></i></a>
                <?break;
            case 'YOUTUBE':
                 ?>
                <a target="_blank" href="<?=$arResult['PROPERTIES']['YOUTUBE_URL']['VALUE']?>" class="social__item social__item--youtube js-hover"><i class="fa fa-youtube"></i></a>
                <?break;  
        }
        $arServices[] = ob_get_contents();
        ob_end_clean();
    }
}
?>

<?php if (!empty($arServices)): ?>
    <div class="clinic__social">
        <strong>Присоединяйтесь</strong>
        <span class="clinic__social-icons">
            <span class="social">
                <?php echo implode("\n", $arServices); ?>
            </span>
        </span>
    </div>
<?php endif; ?>

<?php
    $fileId = $arResult['PROPERTIES']['SCHEDULE_FILE']['VALUE'];
    
    $values = $arResult["PROPERTIES"]["SCHEDULE"]["VALUE"];
    for($i = 1; $i < $equalsCount; $i++) {
        if ($values[$i] != $values[$i - 1]) {
            $checkEquals = false;
            break;
        }
    }
    $arScheduleFiles = array();
    $arScheduleFileProperty = $arResult['PROPERTIES']['SCHEDULE_FILE'];
    if (is_array($arScheduleFileProperty)) {
        foreach($arScheduleFileProperty['VALUE']  as $index => $fileId) {
            $fileDescription = empty($arScheduleFileProperty['DESCRIPTION'][$index]) ? 'Скачать расписание' : $arScheduleFileProperty['DESCRIPTION'][$index];
            $filePath = CFile::GetPath($fileId);
            $arScheduleFiles[] = array(
                'path' => $filePath, 
                'description' => $fileDescription,
            );
        }
    }
    $scheduleText = trim($arResult['PROPERTIES']['SCHEDULE_TEXT']['~VALUE']['TEXT']);
    if (!empty($scheduleText)) {
        $arDayOfWeek = array(
            0 => "Пн - Пт", 
            5 => "Сб", 
            6 => "Вс"
        );
    } else {
        $arDayOfWeek = array(
            "Пн","Вт","Ср","Чт","Пт", "Сб", "Вс",
        );
    }

    $parsedValues = [];
    if (!empty($scheduleText)) {
        foreach ($values as $day => $value) {
            if (!isset($arDayOfWeek[$day])) {
                continue;
            }

            $parsedValues[$arDayOfWeek[$day]] = $value;
        }    
    } elseif ($values && is_array($values)) {
        reset($values);
        do {
            $startDay = key($values);
            $endDay = $startDay;
            $time = current($values);
            while($time === next($values)) {
                $endDay = key($values);
            }
            prev($values);

            if (!isset($arDayOfWeek[$startDay]) || !isset($arDayOfWeek[$endDay])) {
                continue;
            }

            if ($startDay === $endDay) {
                $parsedValues[$arDayOfWeek[$startDay]] = $time;
            } else {
                $parsedValues[$arDayOfWeek[$startDay].' - '.$arDayOfWeek[$endDay]] = $time;
            }
        } while (next($values));
    }
?>
<?php if (!empty($values) || !empty($scheduleText)): ?>
    <div class="clinic__schedule">
        <?php if (!empty($scheduleText)): ?>
            <div class="clinic__schedule-header">
                Расписание
            </div>
            <table class="clinic__schedule-table">
                <tr>
                    <td width="35%">Пн-Вс</td>
                    <td>Круглосуточно</td>
                </tr>
                <tr>
                    <td colspan="2" class="clinic__schedule-table__desc">
                        <?php echo $scheduleText; ?>
                    </td>
                </tr>
            </table>
        <?php endif; ?>
        <?php if (is_array($values) && !empty($values)):?>
            <div class="clinic__schedule-header">
                Поликлиника
            </div>
            <table class="clinic__schedule-table">
                <?php foreach ($parsedValues as $day => $value):?>
                    <tr>
                        <td width="35%"><?php echo $day; ?></td>
                        <td><?php echo $value; ?></td>
                    </tr>
                <?endforeach?>
            </table>
        <?endif?>
        <?php foreach($arScheduleFiles as $arFile): ?>
            <a href="<?php echo $arFile['path']; ?>" class="clinic__schedule-link"><i class="fa fa-download"></i><?php echo $arFile['description']; ?></a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<div class="clinic__about">
    <div class="clinic__about-header">Общая информация</div>
    <?php echo UrlReplacer::toMobile($arResult['PREVIEW_TEXT'].' '.$arResult['DETAIL_TEXT']); ?>
</div>