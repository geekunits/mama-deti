<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>
<?php
    $requestLetter = isset($_REQUEST['alphabet']) ? htmlspecialchars($_REQUEST['alphabet']) : null;
    $requestSpecialityId = isset($_REQUEST['specialty']) ? intval($_REQUEST['specialty']) : null;

    $arFilter = [
        'ACTIVE' => 'Y',
        'PROPERTY_CLINIC' => $arResult['ID'],
    ];

    if ($requestSpecialityId) {
        $arFilter['PROPERTY_SPECIALTY'] = $requestSpecialityId;
    }
    if ($requestLetter) {
        $arFilter['NAME'] = $requestLetter.'%';
    }
    
    $arSpecialty = CMamaDetiAPI::getDoctorsSpecialty(array_merge($arFilter, ['PROPERTY_SPECIALTY' => null]));
    $arAlphabet = CMamaDetiAPI::getDoctorsFirstAlphabet(array_merge($arFilter, ['NAME' => null]));

    global $arDoctorFilter;
    $arDoctorFilter = $arFilter;
?>

<div class="clinic__subneader">
    Врачи клиники
</div>

<form class="filter js-doctor-filter" method="GET">
    <div class="filter__item">
        <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
            <i class="form-select__arr fa fa-chevron-down"></i>
            <div class="form-select__txt js-select-current"><?php echo isset($arSpecialty[$requestSpecialityId]) ? $arSpecialty[$requestSpecialityId] : 'Любой специальности'; ?></div>
            <select name="specialty" class="js-select">
                <option value="" <?php echo !$requestClinicId ? 'selected' : ''; ?>>Любой специальности</option>
                <?php foreach ($arSpecialty as $specialityId => $specialityName): ?>
                    <option value="<?php echo $specialityId; ?>" <?php echo $specialityId == $requestSpecialityId ? ' selected' : ''; ?>>
                        <?php echo $specialityName; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="filter__item">
        <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
            <i class="form-select__arr fa fa-chevron-down"></i>
            <div class="form-select__txt"><?php echo $requestLetter ? 'Фамилия на букву «<strong class="js-select-current">'.$requestLetter.'</strong>»' : 'Фамилия на любую букву'; ?></div>
            <select name="alphabet" class="js-select">
                <option value="" <?php echo !$requestLetter ? 'selected' : ''; ?>>все буквы</option>
                <?php foreach ($arAlphabet as $letter): ?>
                    <option value="<?php echo $letter; ?>" <?php echo $letter == $requestLetter ? ' selected' : ''; ?>><?php echo $letter; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</form>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "doctors",
    Array(
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => DOCTOR_IBLOCK_ID,
        "NEWS_COUNT" => "10",
        "SORT_BY1" => "NAME",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arDoctorFilter",
        "FIELD_CODE" => array(
            'DETAIL_PICTURE',
            'DETAIL_PAGE_URL',
        ),
        "PROPERTY_CODE" => array(
            "CLINIC",
            "SPECIALTY",
            "SEX",
            'SECTION',
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "/doctors/#ELEMENT_CODE#/?clinic=".$arResult['ID'],
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "60",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "180",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "PAGER_TEMPLATE" => "mamadeti",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "RESIZE_CATALOG_METHOD" => "2",
        "RESIZE_CATALOG_WIDTH" => "325",
        "RESIZE_CATALOG_HEIGHT" => "155",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
    $component
);?>
