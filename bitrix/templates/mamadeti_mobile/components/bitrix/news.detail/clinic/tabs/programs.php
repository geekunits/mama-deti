<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>
<?php
    $requestDirectionId = isset($_REQUEST['direction']) ? intval($_REQUEST['direction']) : null;

    $arFilter = [
        'ACTIVE' => 'Y',
        'PROPERTY_CLINIC' => $arResult['ID'],
    ];
    $arDirections = CMamaDetiAPI::getProgramDirections($arFilter);
    if ($requestDirectionId) {
        $arFilter['PROPERTY_DIRECTION'] = $requestDirectionId;
    }

    global $arProgramFilter;
    $arProgramFilter = $arFilter;
?>

<div class="clinic__subneader">
    Программы
</div>

<div class="clinic__program">

    <form class="filter js-program-filter" method="GET">
        <div class="filter__item">
            <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
                <i class="form-select__arr fa fa-chevron-down"></i>
                <div class="form-select__txt js-select-current"><?php echo isset($arDirections[$requestDirectionId]) ? $arDirections[$requestDirectionId] : 'Любое направление'; ?></div>
                <select name="direction" class="js-select">
                    <option value="" <?php echo !$requestDirectionId ? 'selected' : ''; ?>>Любое направление</option>
                    <?php foreach ($arDirections as $directionId => $directionName): ?>
                        <option value="<?php echo $directionId; ?>" <?php echo $directionId == $requestDirectionId ? ' selected' : ''; ?>>
                            <?php echo $directionName; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </form>

    <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "programs",
        Array(
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => PROGRAM_IBLOCK_ID,
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "PROPERTY_DIRECTION.SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "PROPERTYSORT_AGE",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arProgramFilter",
            "FIELD_CODE" => array(
                'DETAIL_PICTURE',
                'DETAIL_PAGE_URL',
            ),
            "PROPERTY_CODE" => array(
                "CLINIC",
                "DIRECTION",
                "PRICE",
                "AGE",
                "TYPE",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "/program/#ELEMENT_CODE#/?clinic=".$arResult['ID'],
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "180",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "PAGER_TEMPLATE" => "mamadeti",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "RESIZE_CATALOG_METHOD" => "2",
            "RESIZE_CATALOG_WIDTH" => "325",
            "RESIZE_CATALOG_HEIGHT" => "155",
            "AJAX_OPTION_ADDITIONAL" => ""
        ),
        $component
    );?>

</div>
