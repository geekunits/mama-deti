<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}?>
<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php',
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>
<?php
    $requestServiceId = isset($_REQUEST['service_id']) ? intval($_REQUEST['service_id']) : null;

    $arClinicFilter = array(
        'IBLOCK_ID' => 2,
        'ACTIVE' => 'Y',
        'ID' => $arResult['ID'],
    );
    $arClinicSelect = array(
        'PROPERTY_SERVICES',
    );
    $arClinicsRaw = CMamaDetiAPI::GetClinics($arClinicFilter, $arClinicSelect);
    $arClinics = array();
    $arAllServiceIds = array();
    $arAllClinicIds = array();
    foreach ($arClinicsRaw as $arClinicRaw) {
        $rawId = $arClinicRaw['ID'];
        $serviceId = $arClinicRaw['PROPERTY_SERVICES_VALUE'];
        $arAllClinicIds[$rawId] = $rawId;
        $arAllServiceIds[$serviceId] = $serviceId;
        if (empty($arClinics[$rawId])) {
            $arClinics[$rawId] = $arClinicRaw;
            $arClinics[$rawId]['SERVICES'] = array();
            $arClinics[$rawId]['ACTIONS'] = array();
        }
        $arClinics[$rawId]['SERVICES'][$serviceId] = $serviceId;
    }

    if (!empty($requestClinicId) && !in_array($requestClinicId, $arAllClinicIds)) {
        $requestClinicId = null;
    }

    $arServiceTree = CMamaDetiAPI::GetSelectedServiceTree($arAllServiceIds);

    $arActionFilter = array(
        'IBLOCK_ID' => 32,
        'ACTIVE' => 'Y',
        'PROPERTY_CLINIC' => $arAllClinicIds,
        'ACTIVE_DATE' => 'Y',
        // 'PROPERTY_SERVICES' => $arAllServiceIds,
    );

    $arActionSelect = array(
        'IBLOCK_ID',
        'ID',
        'NAME',
        'PROPERTY_CLINIC',
        'PROPERTY_SERVICES',
    );

    $dbAction = CIBlockElement::GetList(array('SORT' => 'ASC'), $arActionFilter, false, false, $arActionSelect);

    $arActions = array();
    $arServiceIds = array();
    $arActiveClinicIds = array();
    $arActiveActionIds = array();
    $arActiveServiceIds = array();
    $arClinicServiceIds = array();
    $arActionClinicIds = array();
    $arServiceClinicIds = array();
    // Для каждой акции выбираем доступные клиники и доступные сервисы
    while ($arRawAction = $dbAction->GetNext()) {
        $rawId = $arRawAction['ID'];
        $clinicId = $arRawAction['PROPERTY_CLINIC_VALUE'];
        $serviceId = $arRawAction['PROPERTY_SERVICES_VALUE'];
        if (empty($arActions[$rawId])) {
            $arActions[$rawId] = $arRawAction;
            $arActions[$rawId]['SERVICES'] = array();
            $arActions[$rawId]['CLINIC'] = array();
        }
        $arAction[$rawId]['CLINIC'][$clinicId] = $arClinics[$clinicId];
        $arActions[$rawId]['SERVICES'][$serviceId] = $serviceId;
        $arActionClinicIds[$clinicId] = $clinicId;
        $arSerivceClinicIds[$serviceId] = array();
        if (!empty($serviceId) && !empty($clinicId)) {
            if (empty($arSerivceClinicIds[$serviceId])) {
                $arServiceClinicIds[$serviceId][$clinicId] = $clinicId;
            }
        }
        if (empty($requestClinicId) || $requestClinicId == $clinicId) {
            $arActiveClinicIds[$clinicId] = $clinicId;
            $arClinicServiceIds[$serviceId] = $serviceId;
        }
    }
    // Фильтруем дерево сервисов и оставляем только те, где есть акции
    $arTopServices = array();
    $arCurrentTopService = null;
    $arActiveServiceIds = array();
    $arActiveServiceClinicIds = array();
    $showAllServices = false;
    foreach ($arServiceTree as $arService) {
        $serviceId = substr($arService['ID'], 1);
        if ($arService['IS_PARENT']) {
            if ($arService['DEPTH_LEVEL'] == 1) {
                if (!empty($arCurrentTopService) && !empty($arCurrentTopService['SUB_SERVICE_IDS'])) {
                    $topServiceId = substr($arCurrentTopService['ID'], 1);
                    $arTopServices[$topServiceId] = $arCurrentTopService;
                    if (!empty($requestServiceId) && $topServiceId == $requestServiceId) {
                        $showAllServices = true;
                    }
                }
                $arCurrentTopService = $arService;
                $arCurrentTopService['SUB_SERVICE_IDS'] = array();
                $arCurrentTopService['CLINIC_IDS'] = array();
            }
        } else {
            if (!empty($requestServiceId) && 'S'.$requestServiceId == $arCurrentTopService['ID'] && isset($arServiceClinicIds[$serviceId])) {
                foreach ($arServiceClinicIds[$serviceId] as $clinicId) {
                    $arActiveServiceClinicIds[$clinicId] = $clinicId;
                }
            }

            if (isset($arClinicServiceIds[$serviceId])) {
                $arCurrentTopService['SUB_SERVICE_IDS'][$serviceId] = $serviceId;
                if (empty($requestServiceId) || 'S'.$requestServiceId == $arCurrentTopService['ID']) {
                    $arActiveServiceIds[$serviceId] = $serviceId;
                }
            }
        }
    }

    if (!empty($requestServiceId) && !isset($arTopServices[$requestServiceId])) {
        $requestServiceId = null;
    }

    global $arActionsFilter;
    global $APPLICATION;
    $arActionsFilter = array(
        'PROPERTY_CLINIC' => $arResult['ID'],
        'ACTIVE' => 'Y',
        'ACTIVE_DATE' => 'Y',
    );
    if (!empty($requestServiceId)) {
        $arActionsFilter['PROPERTY_SERVICES'] = $arActiveServiceIds;
    }
    $servicePageParams = array();
    if (!empty($requestClinicId)) {
        $servicePageParams['clinic_id'] = $requestClinicId;
    }
    $clinicPageParams = array();
    if (!empty($requestServiceId)) {
        $clinicPageParams['service_id'] = $requestServiceId;
    }
    $pageUri = $APPLICATION->GetCurPage();
?>
<div class="clinic__subneader">
    Акции
</div>

<form class="filter js-action-filter" method="GET">
    <?php if ($arTopServices): ?>
	    <div class="filter__item">
		    <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
			    <i class="form-select__arr fa fa-chevron-down"></i>
			    <div class="form-select__txt js-select-current"><?php echo isset($arTopServices[$requestServiceId]) ? $arTopServices[$requestServiceId]['TEXT'] : 'По всем услугам'; ?></div>
			    <select name="service_id" class="js-select">
				    <option value="" <?php echo !$requestServiceId ? 'selected' : ''; ?>>Все услуги</option>
				    <?php foreach ($arTopServices as $serviceId => $arService): ?>
					    <option value="<?php echo $serviceId; ?>" <?php echo $serviceId == $requestServiceId ? ' selected' : ''; ?>>
						    <?php echo $arService['TEXT']; ?>
					    </option>
			    <?php endforeach; ?>
			    </select>
		    </div>
	    </div>
	<?php endif; ?>
</form>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "actions",
    Array(
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => ACTION_IBLOCK_ID,
        "NEWS_COUNT" => "10",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arActionsFilter",
        "FIELD_CODE" => array(
            0 => "ID",
            1 => "CODE",
            2 => "XML_ID",
            3 => "NAME",
            4 => "TAGS",
            5 => "SORT",
            6 => "PREVIEW_TEXT",
            7 => "PREVIEW_PICTURE",
            8 => "DETAIL_TEXT",
            9 => "DETAIL_PICTURE",
            10 => "DATE_ACTIVE_FROM",
            11 => "ACTIVE_FROM",
            12 => "DATE_ACTIVE_TO",
            13 => "ACTIVE_TO",
            14 => "SHOW_COUNTER",
            15 => "SHOW_COUNTER_START",
            16 => "IBLOCK_TYPE_ID",
            17 => "IBLOCK_ID",
            18 => "IBLOCK_CODE",
            19 => "IBLOCK_NAME",
            20 => "IBLOCK_EXTERNAL_ID",
            21 => "DATE_CREATE",
            22 => "CREATED_BY",
            23 => "CREATED_USER_NAME",
            24 => "TIMESTAMP_X",
            25 => "MODIFIED_BY",
            26 => "USER_NAME",
        27 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "SHOW_COUPON",
            1 => "CLINIC",
            2 => "DATE_TO",
            3 => "DATE_FROM",
            4 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "/actions/#ELEMENT_CODE#/?clinic=".$arResult['ID'],
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "60",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "180",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "PAGER_TEMPLATE" => "mamadeti",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "RESIZE_CATALOG_METHOD" => "2",
        "RESIZE_CATALOG_WIDTH" => "325",
        "RESIZE_CATALOG_HEIGHT" => "155",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
    $component
);?>
