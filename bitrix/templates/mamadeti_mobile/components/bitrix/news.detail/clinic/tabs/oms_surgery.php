<?php
$APPLICATION->IncludeFile(
    '/includes/clinic_header.php',
    [
        'showBook' => true,
        'showBack' => true,
    ]
);
?>

<div class="clinic__subneader">
    <?php echo !empty($arResult["DISPLAY_PROPERTIES"]["OMS_SURGERY"]["DESCRIPTION"]) ? $arResult["DISPLAY_PROPERTIES"]["OMS_SURGERY"]["DESCRIPTION"] : 'Хирургия по ОМС'; ?>
</div>

<div class="clinic__static content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['OMS_SURGERY']['DISPLAY_VALUE']); ?>
</div>