<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    ЭКО по ОМС
</div>

<div class="clinic__static content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['OMS']['DISPLAY_VALUE']); ?>
</div>