<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>
<?php
    $filterId = isset($_REQUEST['filter']) ? htmlspecialchars($_REQUEST['filter']) : null;

    $arDoctors = CMamaDetiAPI::getDoctors(['PROPERTY_CLINIC' => $arResult['ID']]);
    $arDoctorIds = [];
    $arFilters = [
        '' => 'Все отзывы',
        'clinic' => 'Отзывы по клинике',
        'doctors' => 'Отзывы по врачам',
    ];
    foreach ($arDoctors as $doctor) {
        $arFilters[$doctor['ID']] =  $doctor['NAME'];
        $arDoctorIds[] = $doctor['ID'];
    }
    
    switch ($filterId) {
        case '':
            $arFilter = [
                'PROPERTY_CLINIC' => $arResult['ID'],
                [
                    'LOGIC' => 'OR', 
                    ['PROPERTY_DOCTOR' => false], 
                    ['PROPERTY_DOCTOR.ACTIVE' => 'Y'],
                ]
            ];
            break;

        case 'clinic':
            $arFilter = ['PROPERTY_CLINIC' => $arResult['ID'], 'PROPERTY_DOCTOR' => false];
            break;

        case 'doctors':
            $arFilter = ['PROPERTY_CLINIC' => $arResult['ID'], 'PROPERTY_DOCTOR' => $arDoctorIds];
            break;

        default:
            if (is_numeric($filterId)) {
                $arFilter = ['PROPERTY_CLINIC' => $arResult['ID'], 'PROPERTY_DOCTOR' => $filterId];
            }
            break;
    }

    $arFilter['ACTIVE'] = 'Y';

    global $arReviewFilter;
    $arReviewFilter = $arFilter;
?>

<div class="clinic__subneader">
    Отзывы
</div>

<form class="filter js-review-filter" method="GET">
    <div class="filter__item">
        <div class="form-input form-input--border_red form-select js-hover js-select-wrap">
            <i class="form-select__arr fa fa-chevron-down"></i>
            <div class="form-select__txt js-select-current"><?php echo $arFilters[$filterId]; ?></div>
            <select name="filter" class="js-select">
                <?php foreach ($arFilters as $id => $name): ?>
                    <option value="<?php echo $id; ?>" <?php echo $id == $filterId ? ' selected' : ''; ?>>
                        <?php echo $name; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</form>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "reviews.clinic",
    Array(
        "IBLOCK_TYPE" => "contentsite",
        "IBLOCK_ID" => REVIEW_IBLOCK_ID,
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "ACTIVE_FROM",
	    "SORT_ORDER1" => "DESC",
	    "SORT_BY2" => "CREATED_DATE",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "arReviewFilter",
        "FIELD_CODE" => array(
            'TIMESTAMP_X'
        ),
        "PROPERTY_CODE" => array(
            "CLINIC",
            'DOCTOR',
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "/reviews/#ELEMENT_CODE#/?clinic=".$arResult['ID'],
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "60",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "N",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "PAGER_TEMPLATE" => "mamadeti",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "RESIZE_CATALOG_METHOD" => "2",
        "RESIZE_CATALOG_WIDTH" => "325",
        "RESIZE_CATALOG_HEIGHT" => "155",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
    $component
);?>
