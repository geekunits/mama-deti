<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );

    global $arGalleryFilter;
    $arGalleryFilter = ['PROPERTY_CLINIC' => $arResult['ID'], 'INCLUDE_SUBSECTIONS' => 'Y'];
?>

<div class="clinic__subneader">
    Фотогалерея
</div>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "gallery",
    Array(
        "IBLOCK_TYPE" => "contentsite",
        "IBLOCK_ID" => GALLERY_IBLOCK_ID,
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "ID",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arGalleryFilter",
        "FIELD_CODE" => array(
            'DETAIL_PICTURE',
            'DETAIL_PAGE_URL',
        ),
        "PROPERTY_CODE" => array(
            "CLINIC",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "/gallery/#ELEMENT_CODE#/?clinic=".$arResult['ID'],
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "60",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "180",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "PAGER_TEMPLATE" => "mamadeti",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "RESIZE_CATALOG_METHOD" => "2",
        "RESIZE_CATALOG_WIDTH" => "325",
        "RESIZE_CATALOG_HEIGHT" => "155",
        "AJAX_OPTION_ADDITIONAL" => ""
    ),
    $component
);?>
