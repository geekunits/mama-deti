<?php
$APPLICATION->IncludeFile(
    '/includes/clinic_header.php',
    [
        'showBook' => true,
        'showBack' => true,
    ]
);
?>

<div class="clinic__subneader">
    Независимая оценка качества
</div>

<div class="clinic__static content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['QUALITY_CONTROL']['DISPLAY_VALUE']); ?>
</div>