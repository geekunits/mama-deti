<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<?php
    $arVideos = array_reverse($arResult['PROPERTIES']['VIDEO']['VALUE']);
    $arDescriptions = array_reverse($arResult['PROPERTIES']['VIDEO']['DESCRIPTION']);
?>

<div class="clinic__subneader">
    Видео
</div>

<div class="clinic__static content-scroll">
   <?php if (!empty($arVideos)): ?>
        <div>
        <?php foreach($arVideos as $index => $link): ?>
            <div>
                <?php if (!empty($arDescriptions[$index])): ?>
                    <h3><?php echo $arDescriptions[$index]; ?></h3>
                <?php endif; ?>
                <iframe src="<?php echo $link; ?>" width="420" height="315" frameborder="0" scrolling="no" allowfullscreen></iframe>
            </div>
        <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>