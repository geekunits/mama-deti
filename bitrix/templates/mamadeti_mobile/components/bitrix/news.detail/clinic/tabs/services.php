<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
    $previousLevel = 0;

    $serviceManager = Service::getInstance();

    if (is_array($arResult["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SERVICES"]["VALUE"])) {
        $arResult["SERVICES"] = CMamaDetiAPI::GetSelectedServiceTreeMenu($arResult["PROPERTIES"]["SERVICES"]["VALUE"], true, 2);
    }
?>

<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="services">
    <div class="clinic__subneader">
        Услуги
    </div>

    <?php for ($index = 0, $arItems = $arResult['SERVICES'], $count = count($arResult['SERVICES']); $index < $count; $index++): ?>
        <?php
            $arItem = $arItems[$index];
            if ($arItem['DEPTH_LEVEL'] > 2) {
                continue;
            }

            $arNextItem = $index + 1 < $count ? $arItems[$index + 1] : null;
            $hasSubItems = $arNextItem && $arNextItem['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'];
            
        ?>
        <?php for ($depth = $previousLevel; $depth > $arItem['DEPTH_LEVEL']; $depth--): ?>
            </div></div></div>
        <?php endfor; ?>

        <?php if ($arItem['DEPTH_LEVEL'] == 1): ?>
            <div class="services__item js-tree-item">
                <div class="services__head js-tree-head js-hover">
                    <div class="price__head-switch js-tree-switch">+</div>
                    <?php echo $arItem['TEXT']; ?>
                </div>
                <div class="services__content js-tree-content">
                    <div class="services__content-txt">
                        <div class="<?php if ($hasSubItems): ?>js-txt-cut<?php endif; ?>" data-height="100"><?php echo UrlReplacer::toMobile($serviceManager->findService('section', $arItem['SECTION_ID'], $arResult['ID'])['DESCRIPTION']); ?></div>
                    </div>
                    <?php if ($hasSubItems): ?>
                        <div class="services__content-links">
                    <?php else: ?>
                        </div></div>
                    <?php endif; ?>
        <?php else: ?>
            <a href="<?php echo $arItem['LINK'].'clinic/'.$arResult['CODE'].'/'; ?>" class="js-hover"><i class="fa fa-chevron-right"></i><?php echo $arItem['TEXT']; ?></a>
        <?php endif; ?>

        <?php $previousLevel = $arItem['DEPTH_LEVEL']; ?>
    <?php endfor; ?>
    <?php for ($depth = $previousLevel; $depth > 1; $depth--): ?>
        </div></div></div>
    <?php endfor; ?>
</div>

