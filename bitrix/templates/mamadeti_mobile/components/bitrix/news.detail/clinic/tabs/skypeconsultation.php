<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Консультации по Skype
</div>

<div class="clinic__static clinic__skype content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['SKYPE_CONSULTATION']['DISPLAY_VALUE']); ?>
</div>