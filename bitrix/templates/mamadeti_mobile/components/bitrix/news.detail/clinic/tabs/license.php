<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Лицензии
</div>

<div class="gallery-detail js-gallery-swipe">
	<?php foreach ($arResult['PROPERTIES']['LICENSE']['VALUE'] as $index => $photo): ?>
		<?php
            $image =
                CFile::ResizeImageGet(
                    $photo,
                    ['height' => 1024, 'width' => 1024],
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true
                );
            $thumb =
                CFile::ResizeImageGet(
                    $photo,
                    ['height' => 640, 'width' => 640],
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true
                );
		?>
		<figure itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">
			<a data-size="<?php echo $image['width'].'x'.$image['height']; ?>" itemprop="contentUrl" class="gallery-detail__item" href="<?php echo $image['src']; ?>">
                <i class="fa fa-search-plus"></i>
				<img alt="<?php echo htmlspecialchars($arResult['PROPERTIES']['LICENSE']['DESCRIPTION'][$index]); ?>" itemprop="thumbnail" src="<?php echo $thumb['src']; ?>">
			</a>
		</figure>
	<?php endforeach; ?>
</div>
