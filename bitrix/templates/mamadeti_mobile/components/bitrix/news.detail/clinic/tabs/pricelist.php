<?php
    global $priceFilter;
    $priceFilter = array(
        "!PROPERTY_PRICE" => false,
    );
?>

<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Цены
</div>

<?$APPLICATION->IncludeComponent("melcosoft:price.list", "clinic", array(
    "IBLOCK_TYPE" => "contentsite",
    "IBLOCK_ID" => "31",
    "FILTER_NAME" => "priceFilter",
    "CLINIC_ID" => $arResult["ID"],
    "PAGE_ELEMENT_COUNT" => "50000",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_GROUPS" => "Y",
    "CACHE_FILTER" => "Y",
    "PAGER_TEMPLATE" => "mamadeti",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "DISABLE_PRICE_LIST_AJAX" => "Y",
    "_REQUEST" => $_REQUEST,
    'SHOW_MORE_BUTTON' => 'Y',
    ),
    false
);?>