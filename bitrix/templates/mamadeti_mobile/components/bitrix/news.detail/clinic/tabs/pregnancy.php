<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Беременность
</div>

<div class="clinic__static clinic__pregancy content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['PREGNANCY']['DISPLAY_VALUE']); ?>
</div>