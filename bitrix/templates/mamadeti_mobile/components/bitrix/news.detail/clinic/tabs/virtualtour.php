<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Виртуальный тур
</div>

<div class="clinic__static clinic__pregancy content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['VIRTUAL_TOUR']['DISPLAY_VALUE']); ?>
</div>