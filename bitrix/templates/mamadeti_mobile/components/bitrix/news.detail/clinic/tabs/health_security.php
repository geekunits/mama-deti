<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Контакты органов исполнительной власти в сфере охраны здоровья граждан
</div>

<div class="clinic__static clinic__staticimg content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['HEALTH_PROTECTION_CONTACTS']['DISPLAY_VALUE']); ?>
</div>