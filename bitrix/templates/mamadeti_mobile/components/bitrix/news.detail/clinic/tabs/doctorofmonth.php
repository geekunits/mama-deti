<?php
    $APPLICATION->IncludeFile(
        '/includes/clinic_header.php', 
        [
            'showBook' => true,
            'showBack' => true,
        ]
    );
?>

<div class="clinic__subneader">
    Врачи месяца
</div>

<div class="clinic__static clinic__doctorsbest content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['DOCTOR_OF_MONTH']['DISPLAY_VALUE']); ?>
</div>