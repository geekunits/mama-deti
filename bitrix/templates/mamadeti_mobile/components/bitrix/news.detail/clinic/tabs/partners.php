<?php
$APPLICATION->IncludeFile(
    '/includes/clinic_header.php',
    [
        'showBook' => true,
        'showBack' => true,
    ]
);
?>

<div class="clinic__subneader">
    Партнеры
</div>

<div class="clinic__static content-scroll">
    <?php echo UrlReplacer::toMobile($arResult['DISPLAY_PROPERTIES']['PARTNERS']['DISPLAY_VALUE']); ?>
</div>