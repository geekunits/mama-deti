<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
/**DOCTORS**/
$rsDoctors = CIBlockElement::GetList(
    array(),
    array("IBLOCK_ID"=>DOCTOR_IBLOCK_ID,"ACTIVE"=>"Y","PROPERTY_CLINIC"=>$arResult["ID"]),
    false,
    array('nTopCount'=>1)
);
$arResult["HAS_DOCTOR"] = $rsDoctors->SelectedRowsCount();

/**REVIEWS**/
$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "PROPERTY_CLINIC"=>$arResult["ID"],'ACTIVE'=>'Y',);
$rsReview = CIBlockElement::GetList(
    Array(), 
    $arFilter,
    false,
    array('nTopCount'=>1)
);
$arResult["HAS_REVIEW"] = $rsReview->SelectedRowsCount();

/**PROGRAMS**/
$rsProgram = CIBlockElement::GetList(
    array(),
    array("IBLOCK_ID"=>PROGRAM_IBLOCK_ID,"PROPERTY_CLINIC" =>$arResult["ID"],'ACTIVE'=>'Y',),
    false,
    array('nTopCount'=>1)
);
$arResult["HAS_PROGRAM"] = $rsProgram->SelectedRowsCount();

/**ACTIONS**/
$rsAction = CIBlockElement::GetList(
    array(),
    array("IBLOCK_ID"=>ACTION_IBLOCK_ID,"PROPERTY_CLINIC" =>$arResult["ID"],'ACTIVE'=>'Y',),
    false,
    array('nTopCount'=>1)
);
$arResult["HAS_ACTION"] = $rsAction->SelectedRowsCount();

/**NEWS**/
$rsAction = CIBlockElement::GetList(
    array(),
    array("IBLOCK_ID"=>NEWS_IBLOCK_ID,"PROPERTY_CLINIC" =>$arResult["ID"],'ACTIVE'=>'Y',),
    false,
    array('nTopCount'=>1)
);
$arResult["HAS_NEWS"] = $rsAction->SelectedRowsCount();

/**PRICE**/
$rsPrice = CIBlockSection::GetList(
    array(),
    array("IBLOCK_ID"=>PRICELIST_IBLOCK_ID,"UF_CLINIC" =>$arResult["ID"],),
    false,
    array('ID','IBLOCK_ID','UF_CLINIC'),
    array('nTopCount'=>1)
);
$arResult["HAS_PRICE"] = $rsPrice->SelectedRowsCount();

/**GALLERY**/
$rsGallery = CIBlockElement::GetList(
    array(),
    array("IBLOCK_ID"=>GALLERY_IBLOCK_ID,"PROPERTY_CLINIC" =>$arResult["ID"],'ACTIVE'=>'Y','INCLUDE_SUBSECTIONS'=>'Y',),
    false,
    array('nTopCount'=>1)
);
$arResult["HAS_GALLERY"] = $rsGallery->SelectedRowsCount();

$clinicManager = ClinicManager::getInstance();
$clinicManager->setCurrentClinicId($arResult['ID']);

$tabs = [
    'services' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'services/',
        'NAME' => 'Услуги в клинике',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['PROPERTIES']['SERVICES']['VALUE'];
        },
    ],

    'birth' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'birth/',
        'NAME' => 'Роды',
        'CLASS' => 'clinic__menu-item--select_color',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['BIRTH']['DISPLAY_VALUE']) > 0;
        },
    ],

    'pregnancy' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'pregnancy/',
        'NAME' => 'Беременность',
        'CLASS' => 'clinic__menu-item--select_color',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['PREGNANCY']['DISPLAY_VALUE']) > 0;
        },
    ],

    'doctors' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'doctors/',
        'NAME' => 'Врачи клиники',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['HAS_DOCTOR'];
        },
    ],

    'actions' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'actions/',
        'NAME' => 'Акции клиники',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['HAS_ACTION'];
        },
    ],

    'pricelist' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'pricelist/',
        'NAME' => 'Цены',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['HAS_PRICE'];
        },
    ],

    'videos' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'videos/',
        'NAME' => 'Видео',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['DISPLAY_PROPERTIES']['VIDEO']['VALUE'];
        },
    ],

    'programs' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'programs/',
        'NAME' => 'Программы',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['HAS_PROGRAM'];
        },
    ],

    'reviews' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'reviews/',
        'NAME' => 'Отзывы',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['HAS_REVIEW'];
        },
    ],

    'skypeconsultation' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'skypeconsultation/',
        'NAME' => 'Консультации по Skype',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['SKYPE_CONSULTATION']['DISPLAY_VALUE']) > 0;
        },
    ],

    'news' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'news/',
        'NAME' => 'Новости',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['HAS_NEWS'];
        },
    ],

    'meeting' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'meeting/',
        'NAME' => 'Популярная педиатрия',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['MEETING']['DISPLAY_VALUE']) > 0;
        },
    ],

    'webinars' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'webinars/',
        'NAME' => 'Вебинары',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['WEBINARS']['DISPLAY_VALUE']) > 0;
        },
    ],

    'tour' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'tour/',
        'NAME' => 'Экскурсия',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['TOUR']['DISPLAY_VALUE']) > 0;
        },
    ],

    'virtualtour' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'virtualtour/',
        'NAME' => 'Виртуальный тур',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['VIRTUAL_TOUR']['DISPLAY_VALUE']) > 0;
        },
    ],

    'accompanying' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'accompanying/',
        'NAME' => 'Размещение сопровождающих',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['ACCOMPANYING']['DISPLAY_VALUE']) > 0;
        },
    ],

    'rules' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'rules/',
        'NAME' => 'Памятки пациенту',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['RULES']['DISPLAY_VALUE']) > 0;
        },
    ],

    'contacts' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'contacts/',
        'NAME' => 'Контакты',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['CONTACTS']['DISPLAY_VALUE']) > 0;
        },
    ],

    'license' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'license/',
        'NAME' => 'Лицензии',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return count($arResult['DISPLAY_PROPERTIES']['LICENSE']['DISPLAY_VALUE']) > 0;
        },
    ],

    'information' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'information/',
        'NAME' => 'Персональные данные',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['INFORMATION']['DISPLAY_VALUE']) > 0;
        },
    ],

    'law' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'law/',
        'NAME' => 'Законодательство',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['LAW']['DISPLAY_VALUE']) > 0;
        },
    ],

    'doctorofmonth' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'doctorofmonth/',
        'NAME' => 'Врачи месяца',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['DOCTOR_OF_MONTH']['DISPLAY_VALUE']) > 0;
        },
    ],

    'oms' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'oms/',
        'NAME' => 'ЭКО по ОМС',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['OMS']['DISPLAY_VALUE']) > 0;
        },
    ],

    'oms_mrt' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'oms_mrt/',
        'NAME' => 'МРТ по ОМС',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['OMS_MRT']['DISPLAY_VALUE']) > 0;
        },
    ],

    'oms_obstetrics' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'oms_obstetrics/',
        'NAME' => !empty($arResult["DISPLAY_PROPERTIES"]["OMS_OBSTETRICS"]["DESCRIPTION"]) ? $arResult["DISPLAY_PROPERTIES"]["OMS_OBSTETRICS"]["DESCRIPTION"] : 'Акушерство и гинекология по ОМС',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['OMS_OBSTETRICS']['DISPLAY_VALUE']) > 0;
        },
    ],

    'oms_surgery' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'oms_surgery/',
        'NAME' => !empty($arResult["DISPLAY_PROPERTIES"]["OMS_SURGERY"]["DESCRIPTION"]) ? $arResult["DISPLAY_PROPERTIES"]["OMS_SURGERY"]["DESCRIPTION"] : 'Хирургия по ОМС',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['OMS_SURGERY']['DISPLAY_VALUE']) > 0;
        },
    ],

    'leading' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'leading/',
        'NAME' => 'Руководство',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['LEADING']['DISPLAY_VALUE']) > 0;
        },
    ],

    'corporate' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'corporate/',
        'NAME' => 'Для корпоративных клиентов',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['CORPORATE']['DISPLAY_VALUE']) > 0;
        },
    ],

    'gallery' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'gallery/',
        'NAME' => 'Фотогалерея',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return $arResult['HAS_GALLERY'] > 0;
        },
    ],

    'prices' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'prices/',
        'NAME' => 'Цены',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['PRICES']['DISPLAY_VALUE']) > 0;
        },
    ],

    'health_security' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'health_security/',
        'NAME' => 'Контакты органов исполнительной власти в сфере охраны здоровья граждан',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['HEALTH_PROTECTION_CONTACTS']['DISPLAY_VALUE']) > 0;
        },
    ],
    'partners' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'partners/',
        'NAME' => 'Партнеры',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['PARTNERS']['DISPLAY_VALUE']) > 0;
        },
    ],
    'quality_control' => [
        'URL' => $arResult['DETAIL_PAGE_URL'].'quality_control/',
        'NAME' => 'Независимая оценка качества',
        'CLASS' => '',
        'CALLBACK' => function() use ($arResult) {
            return strlen($arResult['DISPLAY_PROPERTIES']['QUALITY_CONTROL']['DISPLAY_VALUE']) > 0;
        },
    ],
];

$activeTabs = [];
foreach ($tabs as $tabCode => $tab) {
    if ($tab['CALLBACK']()) {
        $activeTabs[$tabCode] = $tab;
    }
}

if (!$arParams['TAB_CODE']) {
    $tabCode = 'description';
} else {
    if (!in_array($arParams['TAB_CODE'], array_keys($activeTabs))) {
        LocalRedirect($arResult['DETAIL_PAGE_URL']);
    } else {
        $tabCode = $arParams['TAB_CODE'];
    }
}

$arResult['TABS'] = $activeTabs;
$arResult['TAB_CODE'] = $tabCode;
