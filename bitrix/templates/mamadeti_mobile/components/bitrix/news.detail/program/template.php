<?php
$labelProgram = 0;
?>
<div class="clinic__prorgam">
    <div class="clinic__prorgam-title">
		<?php
			echo $arResult['NAME'];
			if ($arResult["PROPERTIES"]["TYPE"]){
				$labelProgram++;

				$xmlId = $arResult["PROPERTIES"]["TYPE"]["VALUE_XML_ID"];

				if(isset(ProgramHelper::$types[$xmlId])){
					$type = ProgramHelper::$types[$xmlId];
					?>
						<span class="programm-label programm-label--<? echo $xmlId; ?> in-bl"><? echo $type['label']; ?></span>
					<?php
				}
			}
        ?>
    </div>
    <div class="content-scroll clinic__prorgam-content">
        <?php if ($arResult['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE']): ?>
            <p><strong>Стоимость:</strong> <?php echo formatCurrencyAmount($arResult['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE']); ?></p>
        <?php endif; ?>
        <?php echo $arResult['DETAIL_TEXT']; ?>
    </div>
</div>
<?php if ($labelProgram > 0): ?>
	<div class="program-label-note">
		<?php
			foreach(ProgramHelper::$types AS $typeCode => $type){
				?>
					<div class="programm-label programm-label--<?php echo $typeCode;?> in-bl"><?php echo $type['label'];?></div>
					<div class="program-label-note__txt"><?php echo $type['description'];?></div>
				<?php
			}
		?>
	</div>
<?php endif; ?>