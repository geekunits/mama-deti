<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
    $imageData = CFile::ResizeImageGet($arResult['DETAIL_PICTURE']['ID'], array('width' => 120, 'height' => 120), BX_RESIZE_IMAGE_EXACT, true);

    global $arReviewFilter;
    $arReviewFilter = ['PROPERTY_DOCTOR' => $arResult['ID'], 'ACTIVE' => 'Y'];
?>
<div class="clinic__doctor">
    <script>
    $('.js-book-clinic').hide();
    </script>
    <div class="doctor-card clearfix">
        <div class="doctor-card__left">
            <?php if ($imageData): ?>
                <a href="<?php echo $arResult['DETAIL_PAGE_URL']; ?>" class="doctor-card__pic">
                    <img src="<?php echo $imageData['src']; ?>" alt="">
                </a>
            <?php else: ?>
                <a href="<?php echo $arResult['DETAIL_PAGE_URL']; ?>" class="doctor-card__pic nophoto--doctor_<?php echo $arResult['DISPLAY_PROPERTIES']['SEX']['DISPLAY_VALUE'] === 'Ж' ? 'female' : 'male'; ?>"></a>
            <?php endif; ?>
        </div>
        <div class="doctor-card__right">
            <?php if (isset($arParams['CLINIC_ID'])): ?>
                <div class="doctor-card__name"><?php echo $arResult['NAME']; ?></div>
            <?php endif; ?>
            <div class="doctor-card__spec"><?php echo strip_tags(is_array($arResult['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE']) ? implode(', ', $arResult['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE']) : $arResult['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE']); ?></div>
            <?php if ($arResult['DISPLAY_PROPERTIES']['SECTION']['DISPLAY_VALUE']): ?>
                <div class="doctor-card__clinics">
                    <strong>Отделение:</strong><br/>
                    <?php echo strip_tags(is_array($arResult['DISPLAY_PROPERTIES']['SECTION']['DISPLAY_VALUE']) ? implode(', ', $arResult['DISPLAY_PROPERTIES']['SECTION']['DISPLAY_VALUE']) : $arResult['DISPLAY_PROPERTIES']['SECTION']['DISPLAY_VALUE']); ?>
                </div>
            <?php endif; ?>
            <?php if (empty($arResult['DISPLAY_PROPERTIES']['HIDDEN_BUTTON_BOOK']['VALUE'])): ?>
                <div class="button js-book-show" data-doctor="<?php echo $arResult['ID']; ?>" data-clinic="<?php echo ClinicManager::getInstance()->getCurrentClinic()['ID']; ?>">Записаться на приём</div>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>
        <div class="doctor-info js-doctor-info">
            <?php if ($arResult['STATISTICS']['REVIEW']): ?>
                <div class="doctor-card__tab js-doctor-reviews-show"><span class="doctor-card__reviews"><?php echo $arResult['STATISTICS']['REVIEW']; ?></span>Отзывы</div>
            <?php endif; ?>
            <div class="doctor-reviews__header">Резюме врача</div>
                <p>
                    <?php
                        $features = [
                            //$arResult['DISPLAY_PROPERTIES']['POST']['DISPLAY_VALUE'],
                            $arResult['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE'],
                        ];
                        $features = array_filter($features);

                        echo implode('<br/>',  $features);
                    ?>
                </p>
                
                <?if ($arResult["PROPERTIES"]["POST"]["VALUE"][0]):?>
                    <?foreach ($arResult["PROPERTIES"]["POST"]["VALUE"] as $key => $val) {?>
                        <p>
                            <?=$val?>
                        </p>
                    <?}?>
                <?endif?>
                <p><?php echo $arResult['DISPLAY_PROPERTIES']['POST_FULL']['DISPLAY_VALUE']; ?></p>
            
            <?php if ($arResult['DISPLAY_PROPERTIES']['EDUCATION']['DISPLAY_VALUE']): ?>
                <div class="doctor-info__item">
                    <strong>Образование:</strong><br>
                    <?php echo $arResult['DISPLAY_PROPERTIES']['EDUCATION']['DISPLAY_VALUE']; ?>
                </div>
            <?php endif; ?>
            <?php if ($arResult['PROPERTIES']['CERTIFICATES']['VALUE'][0]): ?>
                <div class="doctor-info__item">
                    <strong>Сертификаты:</strong><br>
                        <?foreach ($arResult['PROPERTIES']['CERTIFICATES']['VALUE'] as $key => $val) {?>
                            <div class="b-clinic_doctors__description-value">
                                <?=$val?>
                            </div>
                        <?}?>
                </div>
            <?php endif; ?>
            <?php if ($arResult['DISPLAY_PROPERTIES']['SKILLS']['DISPLAY_VALUE']): ?>
            <div class="doctor-info__item">
                <strong>Профессиональные навыки:</strong><br>
                <?php echo $arResult['DISPLAY_PROPERTIES']['SKILLS']['DISPLAY_VALUE']; ?>
            </div>
            <?php endif; ?>
            <?php if ($arResult['DISPLAY_PROPERTIES']['MANIPULATIONS']['DISPLAY_VALUE']): ?>
            <div class="doctor-info__item">
                <strong>Клиническая практика:</strong><br>
                <?php echo $arResult['DISPLAY_PROPERTIES']['MANIPULATIONS']['DISPLAY_VALUE']; ?>
            </div>
            <?php endif; ?>
            <?php if ($arResult['DISPLAY_PROPERTIES']['EXPERIENCE']['DISPLAY_VALUE']): ?>
                <strong>Опыт работы:</strong><br>
                <div class="doctor-info__item">
                    <?php echo $arResult['DISPLAY_PROPERTIES']['EXPERIENCE']['DISPLAY_VALUE']; ?>
                </div>
            <?php endif; ?>
            <?php if ($arResult['DISPLAY_PROPERTIES']['MY_HISTORY']['DISPLAY_VALUE']): ?>
                <strong>История моего становления:</strong><br>
                <div class="doctor-info__item">
                    <?php echo $arResult['DISPLAY_PROPERTIES']['MY_HISTORY']['DISPLAY_VALUE']; ?>
                </div>
            <?php endif; ?>
            <?php if ($arResult['DISPLAY_PROPERTIES']['SERVICES']['DISPLAY_VALUE']): ?>
                <?php
                    $doctorServiceIds = $arResult['PROPERTIES']['SERVICES']['VALUE'];
                    $doctorServiceIds = is_array($doctorServiceIds) ? $doctorServiceIds : [$doctorServiceIds];

                    if ($clinic = ClinicManager::getInstance()->getCurrentClinic()) {
                        $clinicServiceIds = $clinic['PROPERTIES']['SERVICES']['VALUE'];
                        $clinicServiceIds = is_array($clinicServiceIds) ? $clinicServiceIds : [$clinicServiceIds];
                    
                        $serviceIds = array_intersect($doctorServiceIds, $clinicServiceIds);
                    } else {
                        $serviceIds = $doctorServiceIds;
                    }
                    
                    $services = [];
                    foreach ($serviceIds as $serviceId) {
                        $service = Service::getInstance()->findService('element', $serviceId);
                        if ($service['ACTIVE'] === 'N') {
                            continue;
                        }
                        $services[] = '<a href="'.$service['DETAIL_PAGE_URL'].($clinic ? 'clinic/'.$clinic['CODE'].'/' : '?doctor='.$arResult['ID']).'">'.$service['NAME'].'</a>';
                    }
                    
                    if ($services) {
                        echo '<div class="doctor-info__item"><strong>Услуги:</strong><br/>'.implode('<br/>', $services).'</div>';
                    }
                ?>
            <?php endif; ?>
            <?php if ($arResult['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']): ?>
                <div class="doctor-info__item">
                    <strong>Ведет приём в:</strong><br/>
                    <?php echo is_array($arResult['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']) ? implode('<br/>', $arResult['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']) : $arResult['DISPLAY_PROPERTIES']['CLINIC']['DISPLAY_VALUE']; ?>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($arResult['STATISTICS']['REVIEW']): ?>
            <div class="doctor-reviews js-doctor-reviews">
                <div class="doctor-card__tab js-doctor-info-show">Резюме врача</div>
                <div class="doctor-reviews__header">Отзывы о враче <div class="doctor-card__reviews"><?php echo $arResult['STATISTICS']['REVIEW']; ?></div></div>
                <?$APPLICATION->IncludeComponent("bitrix:news.list", "reviews", array(
                    "IBLOCK_TYPE" => "contentsite",
                    "IBLOCK_ID" => REVIEW_IBLOCK_ID,
                    "NEWS_COUNT" => "999",
                    "SORT_BY1" => "TIMESTAMP_X",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arReviewFilter",
                    "FIELD_CODE" => array(
                    ),
                    "PROPERTY_CODE" => array(
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "60",
                    "CACHE_FILTER" => "Y",

                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "N",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "RESIZE_CATALOG_METHOD" => "2",
                    "RESIZE_CATALOG_WIDTH" => "325",
                    "RESIZE_CATALOG_HEIGHT" => "155",
                    "AJAX_OPTION_ADDITIONAL" => ""
                    ),
                    $component
                );?>
            </div>
        <?php endif; ?>
    </div>

</div>
