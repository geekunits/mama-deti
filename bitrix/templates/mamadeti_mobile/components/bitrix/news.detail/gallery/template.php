<div class="clinic__subneader">
	<?php echo $arResult['NAME']; ?>
</div>
<div class="gallery-detail js-gallery-swipe">
	<?php foreach ($arResult['RES_MOD_PHOTO'] as $index => $photo): ?>
		<?php
			$image =
                CFile::ResizeImageGet(
                    $photo,
                    ['height' => 1024, 'width' => 1024],
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true
                );

            $thumb =
                CFile::ResizeImageGet(
                    $photo,
                    ['height' => 640, 'width' => 640],
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true
                );

            $pieces = explode("#", $arResult['RES_MOD_DESCR'][$index]);
            if((count($pieces) >= 3)&&(intval($pieces[1]) > 0 )){
                $desc = $pieces[2];
            }else{
                $desc = $arResult['RES_MOD_DESCR'][$index];
            }
		?>
		<figure itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">
			<a 
                data-size="<?php echo $image['width'].'x'.$image['height']; ?>" 
                itemprop="contentUrl" 
                class="gallery-detail__item" 
                href="<?php echo $image['src']; ?>"
            >
				<img 
                    alt="<?php echo htmlspecialchars($desc); ?>"
                    itemprop="thumbnail" 
                    src="<?php echo $thumb['src']; ?>"
                >
                <figure><?php echo htmlspecialchars($desc); ?></figure>
                <i class="fa fa-search-plus"></i>
			</a>
		</figure>
	<?php endforeach; ?>
</div>
