<?
// сортировка фотогалереи по хэшу в начале описания фото

/* ищем признаки сортировки */
$sort = array();
foreach ($arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"] as $key=>$descr) {
    $pieces = explode("#", $descr);

    if((count($pieces) >= 3)&&(intval($pieces[1]) > 0 )){
        $sort[$key] = intval($pieces[1]);
    }
}

$photo_origin = $arResult["PROPERTIES"]["PHOTO"]["VALUE"];
$desc_origin = $arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"];

/* собираем элементы с явно указанными позициями сортировки */
$photo_arr = array();
$desc_arr = array();
foreach ($photo_origin as $key0 => $val0) {
    foreach ($sort as $key=>$val) {
    	if($key0 == $val - 1){
    		$photo_arr[$key0] = $photo_origin[$key];
    		$desc_arr[$key0] = $desc_origin[$key];
    		unset($photo_origin[$key]);
    		unset($desc_origin[$key]);
    	}
    }
}

$photo_origin = array_values($photo_origin); //убираем пропуски индексов в исходном массиве
$desc_origin = array_values($desc_origin); 

/* заполняем 'дырки' в полученном массиве если они есть */
$j = 0;
$keys = array_keys($photo_arr);
$last = end($keys);

for ($i=0; $i < $last; $i++) {
	if (!$photo_arr[$i]) {
		$photo_arr[$i] = $photo_origin[$j];
		$desc_arr[$i] = $desc_origin[$j];
		unset($photo_origin[$j]);
		unset($desc_origin[$j]);
		$j++;
	}
}	        

ksort($photo_arr);	//после затыкания дырок новые элементы с порядковыми ключами всеравно добавляются в конец
ksort($desc_arr);

$arResult['RES_MOD_PHOTO'] = array_merge($photo_arr, $photo_origin);
$arResult['RES_MOD_DESCR'] = array_merge($desc_arr, $desc_origin);
?>