<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?php
$arTime = array();
$h = 9;
$m = 0;
while (true) {
    $arTime[] = $h.':'.($m < 10 ? '0'.$m : $m);
    $m += 30;
    if ($m == 60) {
        ++$h;
        $m = 0;
    }
    if ($h == 19 && $m == 30) {
        break;
    }
}
$arTime2 = array();
$t = intval(date("H"));
($t >= 20 || $t < 9) ? $h = 9 : $h = $t + 1 ;
$m = 0;
while (true) {
    $arTime2[] = $h . ":" . ($m < 10 ? '0' . $m : $m);
    $m += 30;
    if ($m == 60) {
        $h++;
        $m = 0;
    }
    if ($h == 20 && $m == 30)
        break;
}

$clinicId = isset($arResult["FORM"]["P_CLINIC"]) ? $arResult["FORM"]["P_CLINIC"] : (int) $_REQUEST['CLINIC'] ;

$clinic = null;

if ($clinicId) {
    $clinics = CMamaDetiAPI::GetClinics(['ID' => $clinicId]);
    if (!empty($clinics)) {
        $clinic = $clinics[0];
    }
}else{
    $clinic = $arResult['CLINICS'][0];
}
?>

<div class="js-book">
    <div class="header1">Запись на прием</div>
    <div class="content">
        <?php if ($arResult['EVENT_SEND_ID'] > 0): ?>
            <br>
            <p><strong>Заявка отправлена</strong></p>
            <p>Ваше обращение получено и будет обработано!</p>
            <p>Данная форма является ЗАЯВКОЙ на запись на прием. Для подтверждения факта записи на прием с Вами свяжутся наши сотрудники в часы работы клиники. </p>
            <div class="book__success-button"><div class="button js-book-close js-hover">Закрыть</div></div>
            <span
                class="js-metrika-success"
                <?php if ($arResult['METRIC_ID']): ?>
                    data-metrika-id="SEND_<?php echo $arResult['METRIC_ID']; ?>_m"
                <?php endif; ?>
                style="display:none"
            ></span>
        <?php else: ?>
            <div class="book__content">
                <?= $arResult['FORM_HEADER'] ?>

                    <div class="book__close fa fa-times js-book-close js-hover"></div>

                    <div class="book__note">Данная форма является ЗАЯВКОЙ на запись на прием. Для подтверждения факта записи на прием с Вами свяжутся наши сотрудники.</div>

                    <br>

                    <input type="hidden" value="<?=intval($_REQUEST['CLINIC'])?>" name="MS_P_CLINIC">
                    <input type="hidden" value="<?=intval($_REQUEST['DOCTOR'])?>" name="MS_P_DOCTOR">

                    <?if(!empty($_REQUEST['DOCTOR'])):?>
                        <?$doc = CMamaDetiAPI::getDoctors(array('ID' => $_REQUEST['DOCTOR']));?>
                        <div class="book__to">
                            Запись к доктору:
                            <div class="book__to-name">
                                <?=$doc[0]['NAME'];?>
                                <?php if (count($arResult['CLINICS']) === 1): ?>
                                    <br/><?php echo reset($arResult['CLINICS'])['NAME']; ?>
                                <?php elseif ($_REQUEST['CLINIC']): ?>
                                    <br/><?php echo $arResult['CLINICS'][$_REQUEST['CLINIC']]['NAME']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?elseif(!empty($_REQUEST['CLINIC'])):?>
                        <div class="book__to">
                            Запись в клинику:
                            <div class="book__to-name"><?=$clinic['NAME'];?></div>
                        </div>
                    <?endif;?>
                    <div class="book__input">
                        <input name="MS_NAME"  type="text" class="form-input <?php echo in_array('NAME', $arResult['FORM_ERROR_FIELDS']) ? 'form-input--error' : ''; ?>" placeholder="Ваше имя" value="<?= $arResult['FORM']['NAME'] ?>">
                    </div>
                    <div class="book__input">
                        <input name="MS_P_PHONE" type="tel" id="id102" placeholder="Телефон для связи" value="<?= $arResult['FORM']['P_PHONE'] ?>" data-phone="yes" class="form-input <?php echo in_array('P_PHONE', $arResult['FORM_ERROR_FIELDS']) ? 'form-input--error' : ''; ?>" placeholder="Ваш телефон с кодом">
                    </div>
                    <div class="book__input">
                        <input name="MS_P_DATE" id="id104" value="<?= $arResult['FORM']['P_DATE'] ?>" type="tel" class="form-input <?php echo in_array('P_DATE', $arResult['FORM_ERROR_FIELDS']) ? 'form-input--error' : ''; ?>" data-date="yes" placeholder="Желаемая дата приема">
                    </div>


                    <?if (empty($_REQUEST["CLINIC"])):?>
                        <?php if ($_REQUEST['DOCTOR'] && count($arResult['CLINICS']) === 1): ?>
                            <input type="hidden" value="<?php echo reset($arResult['CLINICS'])['ID']; ?>" name="MS_P_CLINIC">
                        <?php else: ?>
                            <div class="book__input">
                                <div class="form-input form-input--font_grey form-select js-select-wrap">
                                    <i class="form-select__arr fa fa-chevron-down"></i>
                                    <div class="form-select__txt js-select-current"><?php echo reset($arResult['CLINICS'])['NAME']; ?></div>
                                    <select name="MS_P_CLINIC" class="js-select">
                                        <?foreach ($arResult['CLINICS'] as $arClinic):?>
                                            <option value="<?=$arClinic['ID']?>"<?if ($arResult["FORM"]["P_CLINIC"] == $arClinic["ID"]):?> selected="selected"<?endif?>><?=$arClinic['NAME']?></option>
                                        <?endforeach?>
                                    </select>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?endif?>

                    <div class="book__input">
                        <div class="form-input form-input--font_grey form-select js-select-wrap">
                            <i class="form-select__arr fa fa-chevron-down"></i>
                            <div class="form-select__txt js-select-current"><?php echo in_array($arResult['FORM']['P_TIME'], $arTime) ? htmlspecialchars($arResult['FORM']['P_TIME']) : reset($arTime); ?></div>
                            <select name="MS_P_TIME" class="js-select">
                                <?php foreach ($arTime as $value): ?>
                                    <option value="<?=$value?>"<?if ($arResult["FORM"]["P_TIME"] == $value):?> selected="selected"<?endif?>><?=$value?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                    <div class="book__input">
                        <?php /* <input
                            name="MS_P_CALLBACK_TIME"
                            type="tel"
                            id="id103"
                            value="<?= $arResult['FORM']['P_CALLBACK_TIME'] ?>"
                            data-time="yes"
                            class="form-input <?php echo in_array('P_CALLBACK_TIME', $arResult['FORM_ERROR_FIELDS']) ? 'form-input--error' : ''; ?>"
                            placeholder="Удобное время для звонка"> */ ?>
                        <div class="form-input form-input--font_grey form-select js-select-wrap">
                            <i class="form-select__arr fa fa-chevron-down"></i>
                            <div class="form-select__txt js-select-current">Удобное время для звонка</div>
                            <select name="MS_P_CALLBACK_TIME" class="js-select">
                                <option value="" selected="selected">Удобное время для звонка</option>
                                <?php foreach ($arTime2 as $value): ?>
                                    <option value="<?=$value?>"><?=$value?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>

                    <div class="book__input">
                        <textarea rows="5" placeholder="Какие услуги клиники вас интересуют"
                             class="form-input <?php echo in_array('PREVIEW_TEXT', $arResult['FORM_ERROR_FIELDS']) ? 'form-input--error' : ''; ?>"
                             name="MS_PREVIEW_TEXT"><?= $arResult["FORM"]["PREVIEW_TEXT"] ?></textarea>
                    </div>
                    <?php
                        $propEnumValue = null;

                        $rsProperty = CIBlockProperty::GetList(
                            array(),
                            array(
                                'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                'CODE' => 'SHOW_CONFIDENTIAL_CONFIRM'
                            )
                        );

                        if($arProperty = $rsProperty->Fetch()){
                            $rsPropEnums = CIBlockProperty::GetPropertyEnum($arProperty['ID']);

                            if($arEnum = $rsPropEnums->Fetch()){
                                $propEnumValue = $arEnum['ID'];
                            }
                        }
                    ?>
                    <div class="book__input js-confidential-confirm" style="<?=($arResult['CONFIDENTIAL_CLINICS'][$clinic['ID']] ? '' : 'display:none;');?>">
                        <div class="checkbox">
                            <input type="checkbox" name="MS_P_SHOW_CONFIDENTIAL_CONFIRM" id="MS_P_SHOW_CONFIDENTIAL_CONFIRM" value="<?=$propEnumValue;?>"<?php echo (isset($_POST['MS_P_SHOW_CONFIDENTIAL_CONFIRM']) && $_POST['MS_P_SHOW_CONFIDENTIAL_CONFIRM'] == $propEnumValue ? ' checked="checked"' : '');?> class="checkbox__input" />
                            <label for="MS_P_SHOW_CONFIDENTIAL_CONFIRM" class="checkbox__label js-confidential-text"><?=$clinic['PROPERTIES']['CONFIDENTIAL_CONFIRM_TEXT']['~VALUE'];?></label>
                            <?php if(in_array("P_SHOW_CONFIDENTIAL_CONFIRM", $arResult["FORM_ERROR_FIELDS"])): ?>
                                <div style="color: #ce156d; font-size: 14px;">Вы должны принять условия на обработку персональных данных</div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if (isset($arResult['CAPTCHA_IMAGE'])): ?>
                        <div class="book__captcha">
                            <input name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>">
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult['CAPTCHACode'])?>" alt="">
                            <div class="book__captcha-input">
                                <input type="text" name="CAPTCHA" class="form-input <?php echo in_array('CAPTCHA', $arResult['FORM_ERROR_FIELDS']) ? 'form-input--error' : ''; ?>" value="" placeholder="Код с картинки"/>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($arResult['FORM_ERROR']): ?>
                        <div class="book__error"><?= $arResult['FORM_ERROR_MSG'] ?></div>
                    <?php endif ?>

                    <div class="book__button">
                        <div class="button js-hover js-gasend" data-targetga="event/zapis1/zapis2"><input type="submit" name="form" value="Записаться на прием" >Записаться на прием</div>
                    </div>

                <?= $arResult['FORM_FOOTER'] ?>
            </div>
        <?php endif; ?>
    </div>
    <script>
        var confidentialClinics = <?=json_encode($arResult['CONFIDENTIAL_CLINICS']);?>;
        
        $("#feedback select[name='MS_P_CLINIC']").on('change', function(){
            var clinicId = $(this).val();

            if(confidentialClinics[clinicId]){
                $('.js-confidential-text').html(confidentialClinics[clinicId]);
                $('.js-confidential-confirm').show();
            }else{
                $('.js-confidential-confirm').hide();
            }

            $('.js-confidential-confirm')
                .find("input[name='MS_P_SHOW_CONFIDENTIAL_CONFIRM']")
                .prop('checked', false)
                .trigger('change');
        });
    </script>
</div>
