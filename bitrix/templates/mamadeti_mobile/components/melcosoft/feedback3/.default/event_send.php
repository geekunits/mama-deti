<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

CModule::IncludeModule("iblock");

$arSendFields["CITY"] = $GLOBALS["CURRENT_CITY"]["~NAME"];
$arSendFields["CITY_EMAIL"] = $GLOBALS["CURRENT_CITY"]["~EMAIL"];

if (intval($arResult["FORM"]["P_CLINIC"])>0)
{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$arResult["FORM"]["P_CLINIC"]),false,false,array("IBLOCK_ID","ID","PROPERTY_ZAPIS_EMAIL"));
	if ($arElement = $rsElement->GetNext())
	{
		$arSendFields["CLINIC_EMAIL"] = $arElement["~PROPERTY_ZAPIS_EMAIL_VALUE"];
	}
}

?>