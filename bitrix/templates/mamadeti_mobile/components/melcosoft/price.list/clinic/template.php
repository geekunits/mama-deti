<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php 
    $currentClinicName = null; 
    $oldClinic = null;
    global $APPLICATION;
    
?>
<?php if (count($arResult["ITEMS"])): ?>
    <?php
        $arResult['ITEMS'] = CMamaDetiAPI::resortPriceList($arResult['ITEMS'], $arResult['SECTIONS']);

        $prevTopSectionId = null;
        $prevSubSectionId = null;

        $showExpanded = $arParams['SHOW_EXPANDED'] == 'Y';
    ?>
    <div class="price">
        <?php foreach($arResult['ITEMS'] as $arItem):  ?>
            <?php
                $arSectionNav = $arItem['SECTION_NAV'];
                if (count($arSectionNav) === 2) {
                    list($topSectionId, $subSectionId) = $arSectionNav;
                    $arSubSection = $arResult['SECTIONS'][$subSectionId];
                } else {
                    $topSectionId = array_pop($arSectionNav);
                    $subSectionId = null;
                }
                $arTopSection = $arResult['SECTIONS'][$topSectionId];
            ?>

            <?php if (
                ($prevTopSectionId !== $topSectionId && $prevTopSectionId)
                || ($prevSubSectionId !== $subSectionId)
            ): ?>
                </div>
            <?php endif; ?>

            <?php if ($prevTopSectionId !== $topSectionId): ?>
                <?php if ($prevTopSectionId !== null): ?>
                    </div></div>
                <?php endif; ?>
                <div class="price__item js-tree-item">
                    <div class="price__head js-tree-head js-hover">
                        <div class="price__head-switch js-tree-switch"><?php if ($showExpanded): ?>&ndash;<?php else: ?>+<?php endif; ?></div>
                        <?php echo upperCaseFirst(mb_strtolower($arTopSection['NAME'])); ?>
                    </div>
                    <div class="price__list js-tree-content<?php if ($showExpanded): ?> js-tree-active<?php else: ?> hidden<?php endif; ?>">
            <?php endif; ?>

            <?php if ($subSectionId && $prevSubSectionId !== $subSectionId): ?>
                <div class="price__list-head">
                    <?php echo $arSubSection['NAME']; ?>
                </div>
            <?php endif; ?>

            <?php if ($prevTopSectionId !== $topSectionId || $prevSubSectionId !== $subSectionId): ?>
                <div class="price__list-items">
            <?php endif; ?>

            <div class="price__list-item">
                <div class="price__list-cost"><?php echo number_format($arItem['PROPERTY_PRICE_VALUE'], 0, ',', ' ')?> р</div>
                <div class="price__list-name"><?php echo $arItem['NAME']; ?></div>
            </div>
            <?php
                $prevTopSectionId = $topSectionId;
                $prevSubSectionId = $subSectionId;
            ?>
        <?php endforeach; ?>
        </div></div></div>
    </div>
<?php else: ?>
    <b>По данному запросу услуг не найдено</b>
<?php endif; ?>
