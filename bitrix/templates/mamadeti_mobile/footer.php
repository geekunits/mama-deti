<?php
$cm = CityManager::getInstance();
?>
            <!-- FOOTER -->
            <?php if ($APPLICATION->GetCurDir() !== '/' && $APPLICATION->GetCurDir() !== '/clinics/'): ?>
                <!-- СКРЫВАЕМ ДЛЯ ГЛАВНОЙ -->
                </div>
                <!-- СКРЫВАЕМ ДЛЯ ГЛАВНОЙ -->
            <?php endif; ?>

            <div class="book js-book"></div>

            <div class="map js-map hidden">
                <div class="content">
                    <div class="map__content">
                        <div class="map__close fa fa-times js-map-close js-hover"></div>
                        <p class="map__adrres js-clinic-address"></p>
                        <div class="map__container loading" id="map-clinic"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="overlay js-overlay-page"></div>
    <div class="sidebar-menu js-sidebar">
        <div class="sidebar-menu__list">
            <a href="/doctors/" class="sidebar-menu__item js-hover"><i class="fa fa-chevron-right"></i>Врачи</a>
            <a href="/clinics/" class="sidebar-menu__item js-hover"><i class="fa fa-chevron-right"></i>Клиники</a>
            <a href="/services/" class="sidebar-menu__item js-hover"><i class="fa fa-chevron-right"></i>Услуги</a>
            <a href="/actions/" class="sidebar-menu__item js-hover"><i class="fa fa-chevron-right"></i>Акции</a>
            <a href="/about/general/" class="sidebar-menu__item js-hover"><i class="fa fa-chevron-right"></i>О «Мать и дитя»</a>
            <a href="http://www.mcclinics.ru/" target="_blank" class="sidebar-menu__item js-hover"><i class="fa fa-chevron-right"></i>Инвесторам</a>
        </div>
    </div>

    <div class="footer js-footer">
        <div class="footer__social">
            <div class="footer__social-header">Группа компаний «Мать и дитя»  в соцсетях:</div>
            <span class="social">
                <a href="http://facebook.com/gc.motherandchild" class="social__item js-hover"><i class="fa fa-facebook"></i></a>
                <a href="http://vk.com/gc.motherandchild" class="social__item social__item--vk js-hover"><i class="fa fa-vk"></i></a>
                <?/*<a href="http://www.youtube.com/channel/UCGz14xsIW0mx2IpmWznRV6w" class="social__item social__item--youtube js-hover"><i class="fa fa-youtube"></i></a>*/?>
                <a href="https://instagram.com/mamadeti_club/" class="social__item social__item--instagram js-hover"><i class="fa fa-instagram"></i></a>
            </span>
        </div>
        <a href="http://<?php echo $cm->getDomainByCityId($cm->getCurrentCityId(), false) . $APPLICATION->GetCurPage(); ?>">Перейти на полную версию сайта</a>
        <div class="footer__copy">Мать и Дитя &copy; <?=date('Y');?>. Все права защищены. ООО «ХАВЕН» </div>
    </div>

    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div> 
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>

    <noindex>
        <?php if (EnvManager::getInstance()->isProd()): ?>
            <?php
                $metrikaDirectory = $_SERVER['DOCUMENT_ROOT'] . '/share/includes/metrika/';
                include($metrikaDirectory . 'default_metrika.php');
                $metrikaFile = CityManager::getInstance()->getCurrentCity()['CODE'] . '_metrika.php';
                if (file_exists($metrikaDirectory . $metrikaFile)) {
                    include($metrikaDirectory . $metrikaFile);
                }
                
            ?>
        <?php endif; ?>
    </noindex>

    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/yandexMap.js?v=1441880764"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/console.js"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/jquery.maskedinput.min.js"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/photoswipe/photoswipe.min.js"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/photoswipe/photoswipe-ui-default.min.js"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/gallery.js?<?php echo ASSET_VERSION; ?>"></script>
    <script src="<?php echo SITE_TEMPLATE_PATH; ?>/js/common.js?<?php echo ASSET_VERSION; ?>"></script>
</body>
</html>
