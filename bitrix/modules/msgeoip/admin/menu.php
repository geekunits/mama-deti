<?
IncludeModuleLangFile(__FILE__);

if('D' < $APPLICATION->GetGroupRight("msgeoip"))
{
        $arItems = array(
            array(
                "text" => GetMessage('MSGEOIP_IMPORT_TEXT'),
                "url" => "ms_geoip_import.php",
                "more_url" => array(),
                "page_icon" => "default_page_icon",
            ),
            array(
                "text" => GetMessage('MSGEOIP_IP_INFO'),
                "url" => "ms_geoip_info.php",
                "more_url" => array(),
                "page_icon" => "default_page_icon",
            ),
        );
        $aMenu[] = array(
            "parent_menu" => "global_menu_services",
            "sort" => "900",
            "text" => GetMessage('MSGEOIP_MENU_TEXT'),
            "title" => GetMessage('MSGEOIP_MENU_TITLE'),
            "icon" => "default_menu_icon",
            "page_icon" => "default_page_icon",
            "items_id" => "menu_msgeoip",
            "items" => $arItems,
        );
	return $aMenu;
}
return false;
?>
