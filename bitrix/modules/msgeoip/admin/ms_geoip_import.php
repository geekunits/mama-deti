<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

// получим права доступа текущего пользователя на модуль
$MODULE_RIGHT = $APPLICATION->GetGroupRight("msgeoip");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($MODULE_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

CModule::IncludeModule("msgeoip");

$bAllLinesLoaded = True;
$CUR_FILE_POS = isset($_REQUEST["CUR_FILE_POS"]) ? intval($_REQUEST["CUR_FILE_POS"]) : 0;
$ADD_CNT = intval($ADD_CNT);
$io = CBXVirtualIo::GetInstance();
$strError = "";
$max_execution_time = IntVal($max_execution_time);
if ($max_execution_time <= 0)
    $max_execution_time = 10;

$STEP = intval($STEP);
if ($STEP <= 0)
    $STEP = 1;

if (isset($_REQUEST["CUR_LOAD_SESS_ID"]) && strlen($_REQUEST["CUR_LOAD_SESS_ID"]) > 0)
    $CUR_LOAD_SESS_ID = $_REQUEST["CUR_LOAD_SESS_ID"];
else
    $CUR_LOAD_SESS_ID = "CL" . time();


if (!$strError) {
    if ($STEP == 2) {
        $csvFile = new CCSVData();
        $csvFile->LoadFile($io->GetPhysicalName($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/msgeoip/geodb/GeoLite2-City-Locations.csv'));
        $csvFile->SetDelimiter(',');

        $arHeader = $csvFile->Fetch();

        if ($CUR_FILE_POS <= 0) {
            CMSGEOIPCityLocations::DeleteAll();
            CMSGEOIPCityBlocks::DeleteAll();
        } else {
            $csvFile->SetPos($CUR_FILE_POS);
        }

        $geoCL = new CMSGEOIPCityLocations;

        while ($arRes = $csvFile->Fetch()) {
            $arLoadData = array(
                "GEONAME_ID" => $arRes[0],
                "CONTINENT_CODE" => $arRes[1],
                "CONTINENT_NAME" => $arRes[2],
                "COUNTRY_ISO_CODE" => $arRes[3],
                "COUNTRY_NAME" => $arRes[4],
                "SUBDIVISION_ISO_CODE" => $arRes[5],
                "SUBDIVISION_NAME" => $arRes[6],
                "CITY_NAME" => $arRes[7],
                "METRO_CODE" => $arRes[8],
                "TIME_ZONE" => $arRes[9],
            );

            if ($geoCL->Add($arLoadData) > 0)
                $ADD_CNT++;

            if (intval($max_execution_time) > 0 && (getmicrotime() - START_EXEC_TIME) > intval($max_execution_time)) {
                $bAllLinesLoaded = False;
                break;
            }
        }

        $debug_html = ob_get_contents();
        ob_end_clean();
    }

    if ($STEP == 3) {
        $csvFile = new CCSVData();
        $csvFile->LoadFile($io->GetPhysicalName($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/msgeoip/geodb/GeoLite2-City-Blocks.csv'));
        $csvFile->SetDelimiter(',');

        $arHeader = $csvFile->Fetch();

        if ($CUR_FILE_POS > 0) {
            $csvFile->SetPos($CUR_FILE_POS);
        }

        $arNetworkLen = array();

        $num = 1;
        for ($i = 0; $i < 128; $i++) {
            $arNetworkLen[128 - $i] = $i == 0 ? $num : $num - 1;
            $num *= 2;
        }

        $geoCB = new CMSGEOIPCityBlocks;

        while ($arRes = $csvFile->Fetch()) {

            if (($i = strrpos($arRes[0],':')) !== false) {

                $START_IP = sprintf("%u",ip2long(substr($arRes[0],$i+1)));

                if  (!empty($START_IP)) //ip v4
                {

                $arLoadData = array(
                    "START_IP" => $START_IP,
                    "END_IP" => $START_IP + $arNetworkLen[intval($arRes[1])],
                    "GEONAME_ID" => $arRes[2],
                    "REGISTERED_COUNTRY_GEONAME_ID" => $arRes[3],
                    "REPRESENTED_COUNTRY_GEONAME_ID" => $arRes[4],
                    "POSTAL_CODE" => $arRes[5],
                    "LATITUDE" => $arRes[6],
                    "LONGITUDE" => $arRes[7],
                    "IS_ANONYMOUS_PROXY" => $arRes[8],
                    "IS_SATELLITE_PROVIDER" => $arRes[9],
                );

                if ($geoCB->Add($arLoadData))
                    $ADD_CNT++;
                } //else ip v6
            }

            if (intval($max_execution_time) > 0 && (getmicrotime() - START_EXEC_TIME) > intval($max_execution_time)) {
                $bAllLinesLoaded = False;
                break;
            }
        }

        $debug_html = ob_get_contents();
        ob_end_clean();
    }

    ?>

    <?
    $APPLICATION->SetTitle("Импорт MS GEO IP");

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

    $_SESSION[$CUR_LOAD_SESS_ID]["ERRORS"] .= $strError;
    CAdminMessage::ShowMessage($_SESSION[$CUR_LOAD_SESS_ID]["ERRORS"]);

    if ($bAllLinesLoaded && $STEP > 1) {

        if ($STEP == 2) {
            $bAllLinesLoaded = false;
            $ADD_CNT = 0;
            $CUR_FILE_POS = 0;
            $STEP++;
        } else {
            if (is_set($_SESSION, $CUR_LOAD_SESS_ID))
                unset($_SESSION[$CUR_LOAD_SESS_ID]);
        }
    } elseif ($STEP > 1)
        $CUR_FILE_POS = $csvFile->GetPos();
    if (!$bAllLinesLoaded && $STEP) {

        $_SESSION[$CUR_LOAD_SESS_ID]["LOAD_SCHEME"] = $paramsStr;

        $strParams = bitrix_sessid_get() . "&CUR_FILE_POS=" . $CUR_FILE_POS . "&CUR_LOAD_SESS_ID=" . urlencode($CUR_LOAD_SESS_ID) . "&STEP=" . $STEP . "&max_execution_time=" . IntVal($max_execution_time);
        $strParams .= "&ADD_CNT=" . $ADD_CNT;
        ?>
        Происходит импорт...
        <a href="<? echo $APPLICATION->GetCurPage(); ?>?lang=<? echo LANG ?>&<? echo $strParams ?>">Происходит
            импорт...</a><br>

        <script language="JavaScript" type="text/javascript">
            <!--
            function DoNext() {
                window.location = "<?echo $APPLICATION->GetCurPage(); ?>?lang=<?echo LANG ?>&<?echo $strParams ?>";
            }
            setTimeout('DoNext()', 2000);
            //-->
        </script>
    <?
    }
    ?>

    <form method="POST" action="<? echo $sDocPath ?>?lang=<? echo LANG ?>" ENCTYPE="multipart/form-data" name="dataload"
          id="dataload">
        <?$aTabs = array(
            array(
                "DIV" => "edit1",
                "TAB" => 'Начало импорта',
                "ICON" => "iblock",
                "TITLE" => 'Начала импорта',
            ),
            array(
                "DIV" => "edit2",
                "TAB" => 'Импорт локаций',
                "ICON" => "iblock",
                "TITLE" => 'Импорт локаций',
            ),
            array(
                "DIV" => "edit3",
                "TAB" => 'Импорт IP блоков',
                "ICON" => "iblock",
                "TITLE" => 'Импорт IP блоков',
            ),
        );
        $tabControl = new CAdminTabControl("tabControl", $aTabs, false, true);
        $tabControl->Begin();?>
        <?$tabControl->BeginNextTab();
        if ($STEP == 1) {
            ?>
        <?
        }
        $tabControl->EndTab();
        ?>

        <?$tabControl->BeginNextTab();
        if ($STEP == 2) {
            ?>
            <? if ($debug_html): ?>
                <tr>
                    <td>
                        <?= $debug_html ?>
                    </td>
                </tr>
            <? endif ?>
            <tr>
                <td>
                    <?echo CAdminMessage::ShowMessage(array(
                        "TYPE" => "PROGRESS",
                        "MESSAGE" => !$bAllLinesLoaded ? 'Импорт локаций...' : 'Импорт завершен',
                        "DETAILS" =>
                            'Добавлено: ' . $ADD_CNT . '<br>',
                        "HTML" => true,
                    ))?>
                </td>
            </tr>
        <?
        }
        $tabControl->EndTab();
        ?>

        <?$tabControl->BeginNextTab();
        if ($STEP == 3) {
            ?>
            <? if ($debug_html): ?>
                <tr>
                    <td>
                        <?= $debug_html ?>
                    </td>
                </tr>
            <? endif ?>
            <tr>
                <td>
                    <?echo CAdminMessage::ShowMessage(array(
                        "TYPE" => "PROGRESS",
                        "MESSAGE" => !$bAllLinesLoaded ? 'Импорт блоков...' : 'Импорт завершен',
                        "DETAILS" =>
                            'Добавлено: ' . $ADD_CNT . '<br>',
                        "HTML" => true,
                    ))?>
                </td>
            </tr>
        <?
        }
        $tabControl->EndTab();
        ?>

        <? if ($STEP == 1): ?>
            <?$tabControl->Buttons();
            ?>

            <?
            if ($STEP < 4): ?>
                <input type="hidden" name="STEP" value="<? echo $STEP + 1; ?>">
                <? echo bitrix_sessid_post(); ?>
                <?
                if ($STEP > 1): ?>
                    <input type="hidden" name="URL_DATA_FILE" value="<? echo htmlspecialcharsbx($DATA_FILE_NAME); ?>">
                    <input type="hidden" name="IBLOCK_ID" value="<? echo $IBLOCK_ID ?>">
                <? endif; ?>
                <input type="hidden" name="max_execution_time"
                       value="<? echo htmlspecialcharsbx($max_execution_time); ?>">
            <? endif ?>

            <input type="submit" value="Далее &gt;&gt;" name="submit_btn" class="adm-btn-save">
        <? endif ?>

        <? $tabControl->End(); ?>
    </form>
    <script language="JavaScript">
        <!--
        <?if ($STEP < 2): ?>
        tabControl.SelectTab("edit1");
        tabControl.DisableTab("edit2");
        tabControl.DisableTab("edit3");
        tabControl.DisableTab("edit4");
        <?elseif ($STEP == 2): ?>
        tabControl.SelectTab("edit2");
        tabControl.DisableTab("edit1");
        tabControl.DisableTab("edit3");
        tabControl.DisableTab("edit4");
        <?elseif ($STEP == 3): ?>
        tabControl.SelectTab("edit3");
        tabControl.DisableTab("edit1");
        tabControl.DisableTab("edit2");
        tabControl.DisableTab("edit4");
        <?elseif ($STEP > 3): ?>
        tabControl.SelectTab("edit4");
        tabControl.DisableTab("edit1");
        tabControl.DisableTab("edit2");
        tabControl.DisableTab("edit3");
        <?endif; ?>
        //-->
    </script>

<?
}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>