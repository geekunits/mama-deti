<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

ini_set("display_errors", 1);

// получим права доступа текущего пользователя на модуль
$MODULE_RIGHT = $APPLICATION->GetGroupRight("msgeoip");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($MODULE_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

CModule::IncludeModule("msgeoip");

$APPLICATION->SetTitle("Информация об IP");

if (!$IPADDRESS)
	$IPADDRESS = $_SERVER["REMOTE_ADDR"];

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

    <form method="POST" action="<? echo $sDocPath ?>?lang=<? echo LANG ?>" ENCTYPE="multipart/form-data" name="dataload"
          id="dataload">
        <?$aTabs = array(
            array(
                "DIV" => "edit1",
                "TAB" => 'Информация об IP',
                "ICON" => "iblock",
                "TITLE" => 'Информация об IP',
            ),
        );
        $tabControl = new CAdminTabControl("tabControl", $aTabs, false, true);
        $tabControl->Begin();?>

        <? $tabControl->BeginNextTab(); ?>
        <? if ($IPADDRESS): ?>
            <?
            $rsCityBlock = CMSGEOIPCityBlocks::GetByIP($IPADDRESS);
            if ($arCityBlock = $rsCityBlock->Fetch()) {

                $message = "";

                if ($arCityBlock["LATITUDE"])
                    $message .= "LATITUDE: " . $arCityBlock["LATITUDE"] . "<br>";

                if ($arCityBlock["LONGITUDE"])
                    $message .= "LONGITUDE: " . $arCityBlock["LONGITUDE"] . "<br>";

                $rsCityLocation = CMSGEOIPCityLocations::GetByGeonameID($arCityBlock["GEONAME_ID"]);
                if ($arCityLocation = $rsCityLocation->GetNext()) {
                    if ($arCityLocation["CONTINENT_CODE"])
                        $message .= "CONTINENT CODE: " . $arCityLocation["CONTINENT_CODE"] . "<br>";
                    if ($arCityLocation["CONTINENT_NAME"])
                        $message .= "CONTINENT NAME: " . $arCityLocation["CONTINENT_NAME"] . "<br>";
                    if ($arCityLocation["COUNTRY_ISO_CODE"])
                        $message .= "COUNTRY ISO CODE: " . $arCityLocation["COUNTRY_ISO_CODE"] . "<br>";
                    if ($arCityLocation["COUNTRY_NAME"])
                        $message .= "COUNTRY NAME: " . $arCityLocation["COUNTRY_NAME"] . "<br>";
                    if ($arCityLocation["SUBDIVISION_ISO_CODE"])
                        $message .= "SUBDIVISION ISO CODE: " . $arCityLocation["SUBDIVISION_ISO_CODE"] . "<br>";
                    if ($arCityLocation["SUBDIVISION_NAME"])
                        $message .= "SUBDIVISION NAME: " . $arCityLocation["SUBDIVISION_NAME"] . "<br>";
                    if ($arCityLocation["CITY_NAME"])
                        $message .= "CITY NAME: " . $arCityLocation["CITY_NAME"] . "<br>";
                    if ($arCityLocation["METRO_CODE"])
                        $message .= "METRO CODE: " . $arCityLocation["METRO_CODE"] . "<br>";
                    if ($arCityLocation["TIME_ZONE"])
                        $message .= "TIME ZONE: " . $arCityLocation["TIME_ZONE"] . "<br>";
                }

                ob_start();
                $APPLICATION->IncludeComponent(
                    "bitrix:map.yandex.view",
                    "",
                    Array(
                        "INIT_MAP_TYPE" => "MAP",
                        "MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:".$arCityBlock["LATITUDE"].";s:10:\"yandex_lon\";d:".$arCityBlock["LONGITUDE"].";s:12:\"yandex_scale\";i:10;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:".$arCityBlock["LONGITUDE"].";s:3:\"LAT\";d:".$arCityBlock["LATITUDE"].";s:4:\"TEXT\";s:4:\"info\";}}}",
                        "MAP_WIDTH" => "600",
                        "MAP_HEIGHT" => "500",
                        "CONTROLS" => array("ZOOM", "MINIMAP", "TYPECONTROL", "SCALELINE"),
                        "OPTIONS" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING"),
                        "MAP_ID" => ""
                    ),
                    false
                );
                $htmlMAP = ob_get_contents();
                ob_end_clean();


                echo CAdminMessage::ShowMessage(array(
                    "TYPE" => "OK",
                    "MESSAGE" => 'Информация об IP',
                    "DETAILS" =>
                        $message,
                    "HTML" => true,
                ));

                echo $htmlMAP;
            } else {
                CAdminMessage::ShowMessage('Информация об IP ненайдена');
            }
            ?>
        <? endif ?>
        <tr>
            <td>
                IP: <input type="text" name="IPADDRESS" value="<?= htmlspecialcharsbx($IPADDRESS) ?>">
            </td>
        </tr>
        <? $tabControl->EndTab(); ?>

        <? $tabControl->Buttons(); ?>
        <input type="submit" value="Далее &gt;&gt;" name="submit_btn" class="adm-btn-save">
        <? $tabControl->End(); ?>
    </form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>