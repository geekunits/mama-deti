<?
IncludeModuleLangFile(__FILE__);

if(class_exists("msgeoip")) return;
Class msgeoip extends CModule
{
	var $MODULE_ID = "msgeoip";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function msgeoip()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = MSGEOIP_VERSION;
			$this->MODULE_VERSION_DATE = MSGEOIP_VERSION_DATE;
		}

		$this->MODULE_NAME = GetMessage("MSGEOIP_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("MSGEOIP_MODULE_DESCRIPTION");
		$this->MODULE_CSS = "/bitrix/modules/msgeoip/msgeoip.css";
	}

	function InstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;

		// Database tables creation
		$bDBInstall = !$DB->Query("SELECT 'x' FROM b_msgeoip_blocks_meta WHERE 1=0", true);
		if($bDBInstall)
		{
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/db/".$DBType."/install.sql");
		}

		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}
		else
		{
			RegisterModule("msgeoip");
			CModule::IncludeModule("msgeoip");

			if($bDBInstall)
			{
			}

			//RegisterModuleDependences("main", "OnPanelCreate", "workflow", "CWorkflow", "OnPanelCreate", "200");


			return true;
		}
	}

	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;

		if(!array_key_exists("savedata", $arParams) || ($arParams["savedata"] != "Y"))
		{
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/db/".$DBType."/uninstall.sql");
		}

		//UnRegisterModuleDependences("main", "OnPanelCreate", "workflow", "CWorkflow", "OnPanelCreate");

		UnRegisterModule("msgeoip");

		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}

		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		return true;
	}

	function DoInstall()
	{
		global $DB, $DOCUMENT_ROOT, $APPLICATION, $step;
		$MSGEOIP_RIGHT = $APPLICATION->GetGroupRight("msgeoip");
		if($MSGEOIP_RIGHT == "W")
		{
			$step = IntVal($step);
			if($step < 2)
			{
				$APPLICATION->IncludeAdminFile(GetMessage("MSGEOIP_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/step1.php");
			}
			elseif($step == 2)
			{
				if($this->InstallDB())
				{
					$this->InstallEvents();
					$this->InstallFiles();
				}
				$GLOBALS["errors"] = $this->errors;
				$APPLICATION->IncludeAdminFile(GetMessage("MSGEOIP_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/step2.php");
			}
		}
	}

	function DoUninstall()
	{
		global $DB, $DOCUMENT_ROOT, $APPLICATION, $step;
		$MSGEOIP_RIGHT = $APPLICATION->GetGroupRight("msgeoip");
		if($MSGEOIP_RIGHT == "W")
		{
			$step = IntVal($step);
			if($step < 2)
			{
				$APPLICATION->IncludeAdminFile(GetMessage("MSGEOIP_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/unstep1.php");
			}
			elseif($step == 2)
			{
				$this->UnInstallDB(array(
					"savedata" => $_REQUEST["savedata"],
				));
				$this->UnInstallFiles();
				$GLOBALS["errors"] = $this->errors;
				$APPLICATION->IncludeAdminFile(GetMessage("MSGEOIP_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/msgeoip/install/unstep2.php");
			}
		}
	}

	function GetModuleRightList()
	{
		$arr = array(
			"reference_id" => array("D","R","W"),
			"reference" => array(
				"[D] ".GetMessage("MSGEOIP_DENIED"),
				"[R] ".GetMessage("MSGEOIP_READ"),
				"[W] ".GetMessage("MSGEOIP_WRITE"))
			);
		return $arr;
	}
}
?>