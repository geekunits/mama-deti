<?

class CMSGEOIPCityLocations extends CMSAllGEOIP
{
    function Add($arFields)
    {
        global $DB;

        if (!CMSGEOIPCityBlocks::CheckFields("ADD", $arFields))
            return false;

        $arInsert = $DB->PrepareInsert("b_msgeo_city_locations", $arFields);

        $strSql =
            "INSERT INTO b_msgeo_city_locations(" . $arInsert[0] . ") " .
            "VALUES(" . $arInsert[1] . ")";
        $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

        $ID = IntVal($DB->LastID());

        return $ID;
    }

    function Update($ID, $arFields)
    {
        global $DB;

        $ID = IntVal($ID);

        if (!CMSGEOIPCityBlocks::CheckFields("UPDATE", $arFields))
            return false;

        $strUpdate = $DB->PrepareUpdate("b_msgeo_city_locations", $arFields);

        $strSql =
            "UPDATE b_msgeo_city_locations SET " .
            "	" . $strUpdate . " ";

        $strSql .= "WHERE ID = " . $ID . " ";
        $res = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

        return $ID;
    }

    function Delete($ID)
    {
        global $DB;

        $ID = IntVal($ID);

        return $DB->Query("DELETE FROM b_msgeo_city_locations WHERE ID = " . $ID . "", true);
    }

    function DeleteAll()
    {
        global $DB;

        return $DB->Query("DELETE FROM b_msgeo_city_locations", true);
    }

    function GetByID($ID)
    {
        global $DB;

        $ID = IntVal($ID);

        $strSql = "SELECT ID, GEONAME_ID, CONTINENT_CODE, CONTINENT_NAME, COUNTRY_ISO_CODE, COUNTRY_NAME, SUBDIVISION_ISO_CODE, SUBDIVISION_NAME, CITY_NAME, METRO_CODE, TIME_ZONE " .
            "FROM b_msgeo_city_locations " .
            "WHERE ID = " . $ID;

        return $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
    }

    function GetByGeonameID($ID)
    {
        global $DB;

        $ID = IntVal($ID);

        $strSql = "SELECT ID, GEONAME_ID, CONTINENT_CODE, CONTINENT_NAME, COUNTRY_ISO_CODE, COUNTRY_NAME, SUBDIVISION_ISO_CODE, SUBDIVISION_NAME, CITY_NAME, METRO_CODE, TIME_ZONE " .
            "FROM b_msgeo_city_locations " .
            "WHERE GEONAME_ID = " . $ID;

        return $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
    }

    function GetList($arOrder = Array("ID" => "DESC"), $arFilter = Array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
    {
        global $DB;

        if (count($arSelectFields) <= 0 || in_array("*", $arSelectFields))
            $arSelectFields = array("ID", "GEONAME_ID", "CONTINENT_CODE", "CONTINENT_NAME", "COUNTRY_ISO_CODE", "COUNTRY_NAME", "SUBDIVISION_ISO_CODE", "SUBDIVISION_NAME", "CITY_NAME", "METRO_CODE", "TIME_ZONE");

        $arFields = array(
            "ID" => array("FIELD" => "O.ID", "TYPE" => "int"),
            "GEONAME_ID" => array("FIELD" => "O.GEONAME_ID", "TYPE" => "int"),
            "CONTINENT_CODE" => array("FIELD" => "O.CONTINENT_CODE", "TYPE" => "string"),
            "CONTINENT_NAME" => array("FIELD" => "O.CONTINENT_NAME", "TYPE" => "string"),
            "COUNTRY_ISO_CODE" => array("FIELD" => "O.COUNTRY_ISO_CODE", "TYPE" => "string"),
            "COUNTRY_NAME" => array("FIELD" => "O.COUNTRY_NAME", "TYPE" => "string"),
            "SUBDIVISION_ISO_CODE" => array("FIELD" => "O.SUBDIVISION_ISO_CODE", "TYPE" => "string"),
            "SUBDIVISION_NAME" => array("FIELD" => "O.SUBDIVISION_NAME", "TYPE" => "string"),
            "CITY_NAME" => array("FIELD" => "O.CITY_NAME", "TYPE" => "string"),
            "METRO_CODE" => array("FIELD" => "O.METRO_CODE", "TYPE" => "string"),
            "TIME_ZONE" => array("FIELD" => "O.TIME_ZONE", "TYPE" => "string"),
        );

        $arSqls = CMSGEOIPCityBlocks::PrepareSql($arFields, $arOrder, $arFilter, $arGroupBy, $arSelectFields);

        $arSqls["SELECT"] = str_replace("%%_DISTINCT_%%", "", $arSqls["SELECT"]);

        if (is_array($arGroupBy) && count($arGroupBy) == 0) {
            $strSql =
                "SELECT " . $arSqls["SELECT"] . " " .
                "FROM b_msgeo_city_locations O " .
                "	" . $arSqls["FROM"] . " ";
            if (strlen($arSqls["WHERE"]) > 0)
                $strSql .= "WHERE " . $arSqls["WHERE"] . " ";
            if (strlen($arSqls["GROUPBY"]) > 0)
                $strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";

            $dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
            if ($arRes = $dbRes->Fetch())
                return $arRes["CNT"];
            else
                return False;
        }

        $strSql =
            "SELECT " . $arSqls["SELECT"] . " " .
            "FROM b_msgeo_city_locations O " .
            "	" . $arSqls["FROM"] . " ";
        if (strlen($arSqls["WHERE"]) > 0)
            $strSql .= "WHERE " . $arSqls["WHERE"] . " ";
        if (strlen($arSqls["GROUPBY"]) > 0)
            $strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
        if (strlen($arSqls["ORDERBY"]) > 0)
            $strSql .= "ORDER BY " . $arSqls["ORDERBY"] . " ";

        if (is_array($arNavStartParams) && IntVal($arNavStartParams["nTopCount"]) <= 0) {
            $strSql_tmp =
                "SELECT COUNT('x') as CNT " .
                "FROM b_msgeo_city_locations O " .
                "	" . $arSqls["FROM"] . " ";
            if (strlen($arSqls["WHERE"]) > 0)
                $strSql_tmp .= "WHERE " . $arSqls["WHERE"] . " ";
            if (strlen($arSqls["GROUPBY"]) > 0)
                $strSql_tmp .= "GROUP BY " . $arSqls["GROUPBY"] . " ";

            $dbRes = $DB->Query($strSql_tmp, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
            $cnt = 0;
            if (strlen($arSqls["GROUPBY"]) <= 0) {
                if ($arRes = $dbRes->Fetch())
                    $cnt = $arRes["CNT"];
            } else {
                $cnt = $dbRes->SelectedRowsCount();
            }

            $dbRes = new CDBResult();

            $dbRes->NavQuery($strSql, $cnt, $arNavStartParams);
        } else {
            if (is_array($arNavStartParams) && IntVal($arNavStartParams["nTopCount"]) > 0)
                $strSql .= "LIMIT " . IntVal($arNavStartParams["nTopCount"]);

            $dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        }

        return $dbRes;
    }

    function CheckFields($ACTION, &$arFields, $ID = 0)
    {
        return true;
    }
}

?>