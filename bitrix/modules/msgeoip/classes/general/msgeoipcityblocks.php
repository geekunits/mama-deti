<?

class CMSGEOIPCityBlocks extends CMSAllGEOIP
{
    function Add($arFields)
    {
        global $DB;

        if (!CMSGEOIPCityBlocks::CheckFields("ADD", $arFields))
            return false;

        $arInsert = $DB->PrepareInsert("b_msgeo_city_blocks", $arFields);

        $strSql =
            "INSERT INTO b_msgeo_city_blocks(" . $arInsert[0] . ") " .
            "VALUES(" . $arInsert[1] . ")";
        $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

        $ID = IntVal($DB->LastID());

        return $ID;
    }

    function Update($ID, $arFields)
    {
        global $DB;

        $ID = IntVal($ID);

        if (!CMSGEOIPCityBlocks::CheckFields("UPDATE", $arFields))
            return false;

        $strUpdate = $DB->PrepareUpdate("b_msgeo_city_blocks", $arFields);

        $strSql =
            "UPDATE b_msgeo_city_blocks SET " .
            "	" . $strUpdate . " ";

        $strSql .= "WHERE ID = " . $ID . " ";
        $res = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

        return $ID;
    }

    function Delete($ID)
    {
        global $DB;

        $ID = IntVal($ID);

        return $DB->Query("DELETE FROM b_msgeo_city_blocks WHERE ID = " . $ID . "", true);
    }

    function DeleteAll()
    {
        global $DB;

        return $DB->Query("DELETE FROM b_msgeo_city_blocks", true);
    }

    function GetByID($ID)
    {
        global $DB;

        $ID = IntVal($ID);

        $strSql = "SELECT ID, START_IP, END_IP, GEONAME_ID, REGISTERED_COUNTRY_GEONAME_ID, REPRESENTED_COUNTRY_GEONAME_ID, POSTAL_CODE, LATITUDE, LONGITUDE, IS_ANONYMOUS_PROXY, IS_SATELLITE_PROVIDER " .
            "FROM b_msgeo_city_blocks " .
            "WHERE ID = " . $ID;

        return $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
    }

    function GetByIP($IP)
    {
        global $DB;

        $uIP = sprintf("%u",ip2long($IP));

        $strSql = "SELECT ID, START_IP, END_IP, GEONAME_ID, REGISTERED_COUNTRY_GEONAME_ID, REPRESENTED_COUNTRY_GEONAME_ID, POSTAL_CODE, LATITUDE, LONGITUDE, IS_ANONYMOUS_PROXY, IS_SATELLITE_PROVIDER " .
            "FROM b_msgeo_city_blocks " .
            "WHERE ".$uIP.">=START_IP AND ".$uIP."<=END_IP";

        return $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
    }

    function GetList($arOrder = Array("ID" => "DESC"), $arFilter = Array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
    {
        global $DB;

        if (count($arSelectFields) <= 0 || in_array("*", $arSelectFields))
            $arSelectFields = array("ID", "START_IP", "END_IP", "GEONAME_ID", "REGISTERED_COUNTRY_GEONAME_ID", "REPRESENTED_COUNTRY_GEONAME_ID", "POSTAL_CODE", "LATITUDE", "LONGITUDE", "IS_ANONYMOUS_PROXY", "IS_SATELLITE_PROVIDER");

        $arFields = array(
            "ID" => array("FIELD" => "O.ID", "TYPE" => "int"),
            "START_IP" => array("FIELD" => "O.START_IP", "TYPE" => "int"),
            "END_IP" => array("FIELD" => "O.END_IP", "TYPE" => "int"),
            "GEONAME_ID" => array("FIELD" => "O.GEONAME_ID", "TYPE" => "int"),
            "REGISTERED_COUNTRY_GEONAME_ID" => array("FIELD" => "O.REGISTERED_COUNTRY_GEONAME_ID", "TYPE" => "int"),
            "REPRESENTED_COUNTRY_GEONAME_ID" => array("FIELD" => "O.REPRESENTED_COUNTRY_GEONAME_ID", "TYPE" => "int"),
            "POSTAL_CODE" => array("FIELD" => "O.POSTAL_CODE", "TYPE" => "string"),
            "LATITUDE" => array("FIELD" => "O.LATITUDE", "TYPE" => "double"),
            "LONGITUDE" => array("FIELD" => "O.LONGITUDE", "TYPE" => "double"),
            "IS_ANONYMOUS_PROXY" => array("FIELD" => "O.IS_ANONYMOUS_PROXY", "TYPE" => "int"),
            "IS_SATELLITE_PROVIDER" => array("FIELD" => "O.IS_SATELLITE_PROVIDER", "TYPE" => "int"),
        );

        $arSqls = CMSGEOIPCityBlocks::PrepareSql($arFields, $arOrder, $arFilter, $arGroupBy, $arSelectFields);

        $arSqls["SELECT"] = str_replace("%%_DISTINCT_%%", "", $arSqls["SELECT"]);

        if (is_array($arGroupBy) && count($arGroupBy) == 0) {
            $strSql =
                "SELECT " . $arSqls["SELECT"] . " " .
                "FROM b_msgeo_city_blocks O " .
                "	" . $arSqls["FROM"] . " ";
            if (strlen($arSqls["WHERE"]) > 0)
                $strSql .= "WHERE " . $arSqls["WHERE"] . " ";
            if (strlen($arSqls["GROUPBY"]) > 0)
                $strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";

            $dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
            if ($arRes = $dbRes->Fetch())
                return $arRes["CNT"];
            else
                return False;
        }

        $strSql =
            "SELECT " . $arSqls["SELECT"] . " " .
            "FROM b_msgeo_city_blocks O " .
            "	" . $arSqls["FROM"] . " ";
        if (strlen($arSqls["WHERE"]) > 0)
            $strSql .= "WHERE " . $arSqls["WHERE"] . " ";
        if (strlen($arSqls["GROUPBY"]) > 0)
            $strSql .= "GROUP BY " . $arSqls["GROUPBY"] . " ";
        if (strlen($arSqls["ORDERBY"]) > 0)
            $strSql .= "ORDER BY " . $arSqls["ORDERBY"] . " ";

        if (is_array($arNavStartParams) && IntVal($arNavStartParams["nTopCount"]) <= 0) {
            $strSql_tmp =
                "SELECT COUNT('x') as CNT " .
                "FROM b_msgeo_city_blocks O " .
                "	" . $arSqls["FROM"] . " ";
            if (strlen($arSqls["WHERE"]) > 0)
                $strSql_tmp .= "WHERE " . $arSqls["WHERE"] . " ";
            if (strlen($arSqls["GROUPBY"]) > 0)
                $strSql_tmp .= "GROUP BY " . $arSqls["GROUPBY"] . " ";

            $dbRes = $DB->Query($strSql_tmp, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
            $cnt = 0;
            if (strlen($arSqls["GROUPBY"]) <= 0) {
                if ($arRes = $dbRes->Fetch())
                    $cnt = $arRes["CNT"];
            } else {
                $cnt = $dbRes->SelectedRowsCount();
            }

            $dbRes = new CDBResult();

            $dbRes->NavQuery($strSql, $cnt, $arNavStartParams);
        } else {
            if (is_array($arNavStartParams) && IntVal($arNavStartParams["nTopCount"]) > 0)
                $strSql .= "LIMIT " . IntVal($arNavStartParams["nTopCount"]);

            $dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        }

        return $dbRes;
    }

    function CheckFields($ACTION, &$arFields, $ID = 0)
    {
        return true;
    }
}

?>