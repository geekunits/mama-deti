<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);

//todo
if($APPLICATION->GetGroupRight("main") < "R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

CModule::IncludeModule("sale");
$arYMSettings = array();
$bSaved = false;

if($REQUEST_METHOD=="POST" && check_bitrix_sessid())
{
	if(isset($_POST["YMSETTINGS"]) && is_array($_POST["YMSETTINGS"]) &&!empty($_POST["YMSETTINGS"]))
	{
		$arYMSettings = $_POST["YMSETTINGS"];
		CSaleYMHandler::saveSettings($arYMSettings);
		$bSaved = true;
	}
}

if(empty($arYMSettings))
	$arYMSettings = CSaleYMHandler::getSettings();

$siteList = array();
$rsSites = CSite::GetList($by = "sort", $order = "asc", Array());

while($arRes = $rsSites->Fetch())
	$siteList[$arRes['ID']] = $arRes['NAME'];

$arTabs = array();

foreach ($siteList as $siteId => $arRes['NAME'])
{
	$arTabs[] = array(
		"DIV" => "sale_ym_edit_".$siteId,
		"TAB" => $arRes['NAME']." (".$siteId.")",
		"TITLE" => $arRes['NAME']." (".$siteId.")",
		"SITE_ID" => $siteId
	);
}

$tabControl = new CAdminTabControl("tabControl", $arTabs);

$APPLICATION->SetTitle(GetMessage("SALE_YM_TITLE"));

require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/prolog_admin_after.php");

if($bSaved)
	CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("SALE_YM_SETTINGS_SAVED"), "TYPE"=>"OK"));

?>
<form method="post" action="<?=$APPLICATION->GetCurPage()?>?lang=<?=LANGUAGE_ID?>" name="ymform">
<?
$tabControl->Begin();

foreach($arTabs as $arTab)
{
	$tabControl->BeginNextTab();
	$siteSetts = $arYMSettings[$arTab["SITE_ID"]];
	$arDeliveryFilter = array(
		"LID" => $arTab["SITE_ID"],
		"ACTIVE" => "Y"
	);

	$dbDeliveryList = CSaleDelivery::GetList(
		array("NAME" => "ASC"),
		$arDeliveryFilter,
		false,
		false,
		array("ID", "NAME")
	);

	$arDeliveryList=array();
	while ($arDelivery = $dbDeliveryList->Fetch())
		$arDeliveryList[$arDelivery["ID"]] = $arDelivery["NAME"];

	$dbResultList = CSalePersonType::GetList(
		"NAME",
		"ASC",
		array(
			"LID" => $arTab["SITE_ID"],
			"ACTIVE" => "Y"
		)
	);

	$arPersonTypes = array();
	while ($arPT = $dbResultList->Fetch())
		$arPersonTypes[$arPT['ID']] = $arPT['NAME'];

	?>
		<tr>
			<td><?=GetMessage("SALE_YM_CAMPAIGN_ID")?>:</td>
			<td><input type="text" name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][CAMPAIGN_ID]" size="45" maxlength="255" value="<?=isset($siteSetts["CAMPAIGN_ID"]) ? htmlspecialcharsbx($siteSetts["CAMPAIGN_ID"]) : ""?>"></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_YANDEX_URL")?>:</td>
			<td><input type="text" name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][YANDEX_URL]" size="45" maxlength="255" value="<?=isset($siteSetts["YANDEX_URL"]) ? htmlspecialcharsbx($siteSetts["YANDEX_URL"]) : "https://api.partner.market.yandex.ru/v2/"?>"></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_YANDEX_TOKEN")?>:</td>
			<td><input type="text" name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][YANDEX_TOKEN]" size="45" maxlength="255" value="<?=isset($siteSetts["YANDEX_TOKEN"]) ? htmlspecialcharsbx($siteSetts["YANDEX_TOKEN"]) : ""?>"></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_OAUTH_TOKEN")?>:</td>
			<td><input type="text" name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][OAUTH_TOKEN]" size="45" maxlength="255" value="<?=isset($siteSetts["OAUTH_TOKEN"]) ? htmlspecialcharsbx($siteSetts["OAUTH_TOKEN"]) : ""?>"></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_OAUTH_CLIENT_ID")?>:</td>
			<td><input type="text" name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][OAUTH_CLIENT_ID]" size="45" maxlength="255" value="<?=isset($siteSetts["OAUTH_CLIENT_ID"]) ? htmlspecialcharsbx($siteSetts["OAUTH_CLIENT_ID"]) : ""?>"></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_OAUTH_LOGIN")?>:</td>
			<td><input type="text" name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][OAUTH_LOGIN]" size="45" maxlength="255" value="<?=isset($siteSetts["OAUTH_LOGIN"]) ? htmlspecialcharsbx($siteSetts["OAUTH_LOGIN"]) : ""?>"></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_PAYER_TYPE")?>:</td>
			<td>
				<select name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][PERSON_TYPE]">
					<?foreach ($arPersonTypes as $ptId => $ptName):?>
						<option value="<?=$ptId?>"<?=isset($siteSetts["PERSON_TYPE"]) && $siteSetts["PERSON_TYPE"] == $ptId ? "selected" : ""?>><?=htmlspecialcharsbx($ptName)?> </option>
					<?endforeach;?>
				</select>
			</td>
		</tr>
		<tr class="heading"><td colspan="2"><?=GetMessage("SALE_YM_DELIVERY")?></td></tr>
		<?foreach ($arDeliveryList as $deliveryId => $deliveryName):
			$selected = isset($siteSetts["DELIVERIES"][$deliveryId]) ? $siteSetts["DELIVERIES"][$deliveryId] : '';
		?>
			<tr>
				<td><?=htmlspecialcharsbx($deliveryName)?></td>
				<td>
					<select name="YMSETTINGS[<?=htmlspecialcharsbx($arTab["SITE_ID"])?>][DELIVERIES][<?=$deliveryId?>]">
						<option value=""><?=GetMessage("SALE_YM_NOT_USE")?></option>
						<option value="DELIVERY"<?=$selected == "DELIVERY" ? "selected" : ""?>><?=GetMessage("SALE_YM_DELIVERY_DELIVERY")?></option>
						<option value="PICKUP"<?=$selected == "PICKUP" ? "selected" : ""?>><?=GetMessage("SALE_YM_DELIVERY_PICKUP")?></option>
						<!--<option value="POST"><?=GetMessage("SALE_YM_DELIVERY_POST")?></option>-->
					</select>
				</td>
			</tr>
		<?endforeach;?>
		<tr class="heading"><td colspan="2"><?=GetMessage("SALE_YM_PAYSYSTEMS")?></td></tr>
		<tr>
			<td><?=GetMessage("SALE_YM_SHOP_PREPAID")?>:</td>
			<td><?=makeSelectorFromPaySystems("YMSETTINGS[".htmlspecialcharsbx($arTab["SITE_ID"])."][PAY_SYSTEMS][SHOP_PREPAID]", $siteSetts["PAY_SYSTEMS"]["SHOP_PREPAID"], $arTab["SITE_ID"])?></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_CASH_ON_DELIVERY")?>:</td>
			<td><?=makeSelectorFromPaySystems("YMSETTINGS[".htmlspecialcharsbx($arTab["SITE_ID"])."][PAY_SYSTEMS][CASH_ON_DELIVERY]", $siteSetts["PAY_SYSTEMS"]["CASH_ON_DELIVERY"], $arTab["SITE_ID"])?></td>
		</tr>
		<tr>
			<td><?=GetMessage("SALE_YM_CARD_ON_DELIVERY")?>:</td>
			<td><?=makeSelectorFromPaySystems("YMSETTINGS[".htmlspecialcharsbx($arTab["SITE_ID"])."][PAY_SYSTEMS][CARD_ON_DELIVERY]", $siteSetts["PAY_SYSTEMS"]["CARD_ON_DELIVERY"], $arTab["SITE_ID"])?></td>
		</tr>
	<?
}
$tabControl->Buttons(array(
	"btnSave" => true,
	"btnApply" => false
));
?>
<?=bitrix_sessid_post();?>
<?$tabControl->End();?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");

function makeSelectorFromPaySystems($psTypeYandex, $psIdValue, $siteId)
{
	static $arPaySystems = array();

	if(empty($arPaySystems))
	{
		$dbResultList = CSalePaySystem::GetList(
			array("NAME" => "ASC"),
			array(
				"LID" => $siteId,
				"ACTIVE" => "Y"
			),
			false,
			false,
			array("ID", "NAME")
		);

		while($arPS = $dbResultList->Fetch())
			$arPaySystems[$arPS['ID']] = $arPS['NAME'];
	}

	$result = '<select name="'.$psTypeYandex.'">'.
		'<option value="">'.GetMessage("SALE_YM_NOT_USE").'</option>';

	foreach ($arPaySystems as $psId => $psName)
	{
		$result.= '<option value="'.
			$psId.'"'.
			($psIdValue == $psId ? ' selected ': '').'>'.
			htmlspecialcharsbx($psName).
			'</option>';
	}

	$result .= '</select>';

	return $result;
}
?>

