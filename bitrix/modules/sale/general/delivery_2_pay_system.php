<?
class CSaleDelivery2PaySystem
{
	public static $arFirstDS = array();
	public static $arFirstPS = array();

	private static function makeSqlSearch($arFilter)
	{
		$arSqlSearch = array();

		if (is_array($arFilter))
		{
			foreach ($arFilter as $key => $val)
			{
				if (strlen($val) <= 0)
					continue;

				$key = strtoupper($key);

				switch($key)
				{
					case "PAYSYSTEM_ID":
						$arSqlSearch[] = $key."=".intval($val);
						break;

					case "DELIVERY_ID":
					case "DELIVERY_PROFILE_ID":
						$arSqlSearch[] = GetFilterQuery($key, $val);
						break;
				}
			}
		}

		return GetFilterSqlSearch($arSqlSearch);
	}

	public static function GetList($arFilter = array(), $arGroupBy = false, $arSelectFields = array())
	{
		global $DB;

		$strSqlSearch = self::makeSqlSearch($arFilter);

		$arFieldsToSelect = array();

		if (count($arSelectFields) > 0)
		{
			$arAllFields = array("DELIVERY_ID", "DELIVERY_PROFILE_ID", "PAYSYSTEM_ID");

			foreach ($arSelectFields as $value)
				if(in_array($value, $arAllFields))
					$arFieldsToSelect[] = $value;
		}

		if(!empty($arFieldsToSelect))
			$strFieldsToSelect = implode(", ", $arFieldsToSelect);
		else
			$strFieldsToSelect = "*";

		$strSql = "
			SELECT ".
				$strFieldsToSelect.
			" FROM
				b_sale_delivery2paysystem
			WHERE
			".$strSqlSearch;

		if($arGroupBy !== false && is_array($arGroupBy) && !empty($arGroupBy))
		{
			$strGroupBy = implode(", ", $arGroupBy);
			$strSql .=" GROUP BY ".$strGroupBy;
		}

		$res = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);

		return $res;
	}

	public static function isPaySystemApplicable($paySystemId, $deliveryId)
	{
		if(strlen($deliveryId) <= 0)
			return true;
		$result = false;
		$arDelivery = CSaleDeliveryHelper::getDeliverySIDAndProfile($deliveryId);

		$psInList = $dInList = $together = false;

		$dbPSRec = self::GetList();

		while($arPSRec = $dbPSRec->Fetch())
		{
			$psInRecord = $dInRecord = false;

			if($arPSRec["PAYSYSTEM_ID"] == $paySystemId)
				$psInList = $psInRecord = true;

			if($arPSRec["DELIVERY_ID"] == $arDelivery["SID"]
				&&
				(
					is_null($arPSRec["DELIVERY_PROFILE_ID"])
					||
					$arPSRec["DELIVERY_PROFILE_ID"] == $arDelivery["PROFILE"]
				)
			)
			{
				$dInList = $dInRecord = true;
			}

			if($dInRecord && $psInRecord)
			{
				$together = true;
				break;
			}
		}

		if($together)
			$result = true;
		elseif (!$psInList && !$dInList)
			$result = true;

		return $result;
	}


	public static function UpdateDelivery($ID, $arFields)
	{
		if(!is_array($arFields) || strlen($ID) <= 0)
			return false;

		$arAddedRecords = array();

		$arFilterFields["DELIVERY_ID"] = $ID;

		if(isset($arFields["DELIVERY_PROFILE_ID"]))
			$arFilterFields["DELIVERY_PROFILE_ID"] = $arFields["DELIVERY_PROFILE_ID"];

		self::Delete($arFilterFields);

		if(!is_array($arFields["PAYSYSTEM_ID"]))
			$arFields["PAYSYSTEM_ID"] = array("PAYSYSTEM_ID" => $arFields["PAYSYSTEM_ID"]);

		foreach ($arFields["PAYSYSTEM_ID"] as $psId)
		{
			$arFilterFields["PAYSYSTEM_ID"] = $psId;
			self::Add($arFilterFields);

			$arAddedRecords[] = $arFilterFields;
		}

		foreach($arAddedRecords as $arRecord)
			self::synchroPS($arRecord);

		return true;
	}

	/**
	 * UpdatePaySystem
	 *
	 * @param int $ID Pay system id.
	 * @param array $arFields delivery idenificators.
	 *
	 * @return mixed Pay_system_id if success or false otherwise.
	 */
	public static function UpdatePaySystem($ID, $arFields)
	{
		$ID = trim($ID);
		$arUpdateFields = array("PAYSYSTEM_ID" => $ID);

		if (strlen($ID) <= 0 || !is_array($arFields) || empty($arFields))
			return false;

		if ($arFields[0] == "")
			unset($arFields[0]);

		self::Delete($arUpdateFields);

		$arRecords = array();

		foreach ($arFields as $deliveryId)
		{

			$delivery = CSaleDeliveryHelper::getDeliverySIDAndProfile($deliveryId);

			if(!isset($delivery["SID"]))
				continue;

			$arUpdateFields["DELIVERY_ID"] = $delivery["SID"];

			if(isset($delivery["PROFILE"]))
				$arUpdateFields["DELIVERY_PROFILE_ID"] = $delivery["PROFILE"];
			else
				$arUpdateFields["DELIVERY_PROFILE_ID"] = null;

			self::Add($arUpdateFields);
			$arRecords[] = $arUpdateFields;
		}

		foreach ($arRecords as $arRecord)
			CSaleDelivery2PaySystem::synchroDS($arRecord);

		return true;
	}

	public static function Delete($arFilter)
	{
		global $DB;

		$strSqlSearch = self::makeSqlSearch($arFilter);

		return $DB->Query("DELETE FROM b_sale_delivery2paysystem WHERE ".$strSqlSearch);
	}

	public static function Add($arFields)
	{
		global $DB;

		if(!isset($arFields["DELIVERY_ID"])
			||
			strlen(trim($arFields["DELIVERY_ID"])) <=0
			||
			!isset($arFields["PAYSYSTEM_ID"])
			||
			intval($arFields["PAYSYSTEM_ID"]) <=0
		)
		{
			//echo "<pre>".print_r(debug_backtrace(),true)."</pre>";
			return false;
		}

		$arFieldsFiltered = array();

		$arFieldsFiltered["DELIVERY_ID"] = $DB->ForSql($arFields["DELIVERY_ID"]);
		$arFieldsFiltered["PAYSYSTEM_ID"] = $DB->ForSql($arFields["PAYSYSTEM_ID"]);

		if(isset($arFields["DELIVERY_PROFILE_ID"]))
			$arFieldsFiltered["DELIVERY_PROFILE_ID"] = $DB->ForSql($arFields["DELIVERY_PROFILE_ID"]);

		$arInsert = $DB->PrepareInsert("b_sale_delivery2paysystem", $arFieldsFiltered);

		$strSql = "INSERT INTO b_sale_delivery2paysystem (".$arInsert[0].") VALUES(".$arInsert[1].")";

		return $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
	}

	public static function getSyncData()
	{
		$arD2PS = array();
		$dbD2PS = self::GetList();
		while($arTmpD2PS = $dbD2PS->Fetch())
			$arD2PS[] = $arTmpD2PS;

		static $arPS = null;
		if(is_null($arPS))
		{
			$arPS = array();
			$dbPS = CSalePaySystem::GetList();
			while($arTmpPS = $dbPS->Fetch())
				$arPS[] = $arTmpPS;
		}

		static $arDS = null;
		if(is_null($arDS))
		{
			$arDS = array();
			$dbDS = CSaleDeliveryHandler::GetList();
			while($arTmpDS = $dbDS->Fetch())
				$arDS[] = $arTmpDS;

			$dbDS = CSaleDelivery::GetList();
			while($arTmpDS = $dbDS->Fetch())
			{
				if(!isset($arTmpDS["SID"]))
					$arTmpDS["SID"] = $arTmpDS["ID"];

				$arDS[] = $arTmpDS;
			}
		}

		return array(
			"D2PS" => $arD2PS,
			"DS" => $arDS,
			"PS" => $arPS
			);
	}

	public static function isPSPresented($arFields)
	{
		$result = false;
		$arSynData = static::getSyncData();

		foreach ($arSynData["D2PS"] as $arD2PS)
		{
			if($arFields["PAYSYSTEM_ID"] == $arD2PS["PAYSYSTEM_ID"])
			{
				if(	$arD2PS["DELIVERY_ID"] == $arFields["DELIVERY_ID"]
					&&
					$arD2PS["DELIVERY_PROFILE_ID"] == $arFields["DELIVERY_PROFILE_ID"]
				)
				{
					continue;
				}

				$result = true;
				break;
			}
		}

		return $result;
	}

	public static function isDSPresented($arFields)
	{
		$result = false;
		$arSynData = static::getSyncData();

		foreach ($arSynData["D2PS"] as $arD2PS)
		{
			if ($arFields["DELIVERY_ID"] == $arD2PS["DELIVERY_ID"]
				&&
				(
					$arFields["DELIVERY_PROFILE_ID"] == $arD2PS["DELIVERY_PROFILE_ID"]
					||
					$arFields["DELIVERY_PROFILE_ID"] == null
				)
			)
			{
				if($arD2PS["PAYSYSTEM_ID"] == $arFields["PAYSYSTEM_ID"])
				{
					continue;
				}

				$result = true;
				break;
			}
		}

		return $result;
	}

	public static function isRecordExist($arFields)
	{
		$result = false;

		$arSynData = static::getSyncData();

		foreach ($arSynData["D2PS"] as $arD2PS)
		{
			if(	$arD2PS["DELIVERY_ID"] == $arFields["DELIVERY_ID"]
				&&
				$arD2PS["PAYSYSTEM_ID"] == $arFields["PAYSYSTEM_ID"]
				&&
				$arD2PS["DELIVERY_PROFILE_ID"] == $arFields["DELIVERY_PROFILE_ID"]
			)
			{
				$result = true;
				break;

			}
		}

		return $result;
	}



	public static function synchroPS($arFields, $bFirst = true)
	{
		if($bFirst)
		{
			static::$arFirstDS["DELIVERY_ID"] = $arFields["DELIVERY_ID"];
			static::$arFirstDS["DELIVERY_PROFILE_ID"] = $arFields["DELIVERY_PROFILE_ID"];
		}

		$arSynData = static::getSyncData();
		$arSyncRecords = array();

		foreach ($arSynData["DS"] as $arDS)
		{
			if(isset($arDS["PROFILES"]) && is_array($arDS["PROFILES"]))
			{


				foreach ($arDS["PROFILES"] as $profileId => $arProfile)
				{
						$arRecord = array(
							"DELIVERY_ID" => $arDS["SID"],
							"PAYSYSTEM_ID" => $arFields["PAYSYSTEM_ID"],
							"DELIVERY_PROFILE_ID" => $profileId,
							);

						if(!static::isRecordExist($arRecord)
							&&
							(
								$arRecord["DELIVERY_ID"] != static::$arFirstDS["DELIVERY_ID"]
								||
								$arRecord["DELIVERY_PROFILE_ID"] != static::$arFirstDS["DELIVERY_PROFILE_ID"]
							)
						)
							$arSyncRecords[] = $arRecord;

				}
			}
			else
			{
					$arRecord = array(	"DELIVERY_ID" => $arDS["SID"],
										"PAYSYSTEM_ID" => $arFields["PAYSYSTEM_ID"],
												"DELIVERY_PROFILE_ID" => null);
					if(!static::isRecordExist($arRecord)
						&&
						(
							$arRecord["DELIVERY_ID"] != static::$arFirstDS["DELIVERY_ID"]
							||
							$arRecord["DELIVERY_PROFILE_ID"] != static::$arFirstDS["DELIVERY_PROFILE_ID"]
						)
					)
						$arSyncRecords[] = $arRecord;

			}
		}

		if(!empty($arSyncRecords))
		{
			foreach ($arSyncRecords as $synRecord)
				static::Add($synRecord);

			foreach ($arSyncRecords as $synRecord)
				static::synchroDS($synRecord, false);
		}

		return $arSyncRecords;
	}

	public static function synchroDS($arFields, $bFirst = true)
	{
		if($bFirst)
			static::$arFirstPS["PAYSYSTEM_ID"] = $arFields["PAYSYSTEM_ID"];

		if(static::isPSPresented($arFields))
		{
			return array();
		}

		$arSynData = static::getSyncData();

		$arSyncRecords = array();

		foreach ($arSynData["PS"] as $arPS)
		{
				$arRecord = array(
									"DELIVERY_ID" => $arFields["DELIVERY_ID"],
									"PAYSYSTEM_ID" => $arPS["ID"],
									"DELIVERY_PROFILE_ID" => $arFields["DELIVERY_PROFILE_ID"]
								);
				if(!static::isRecordExist($arRecord) && $arRecord["PAYSYSTEM_ID"] != static::$arFirstDS["PAYSYSTEM_ID"])
					$arSyncRecords[] = $arRecord;
		}

		if(!empty($arSyncRecords))
		{
			foreach ($arSyncRecords as $synRecord)
				static::Add($synRecord);


			foreach ($arSyncRecords as $synRecord)
				static::synchroPS($synRecord, false);
		}

		return $arSyncRecords;
	}
}
?>