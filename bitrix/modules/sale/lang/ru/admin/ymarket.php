<?
$MESS["SALE_YM_TITLE"] = "Настройка \"Покупка на маркете\"";
$MESS["SALE_YM_OAUTH_TOKEN"] = "Авторизационный токен (для запросов магазина к Яндексу)";
$MESS["SALE_YM_OAUTH_CLIENT_ID"] = "Идентификатор приложения";
$MESS["SALE_YM_OAUTH_LOGIN"] = "Логин пользователя";
$MESS["SALE_YM_YANDEX_TOKEN"] = "Токен для запросов Яндекса к магазину";
$MESS["SALE_YM_SETTINGS_SAVED"] = "Настройки сохранены";
$MESS["SALE_YM_DELIVERY"] = "Службы доставки";
$MESS["SALE_YM_DELIVERY_DELIVERY"] = "Курьерская доставка";
$MESS["SALE_YM_DELIVERY_PICKUP"] = "Самовывоз";
$MESS["SALE_YM_DELIVERY_POST"] = "Почта";
$MESS["SALE_YM_NOT_USE"] = "Не использовать";
$MESS["SALE_YM_PAYSYSTEMS"] = "Способы оплаты";
$MESS["SALE_YM_SHOP_PREPAID"] = "Предоплата напрямую магазину";
$MESS["SALE_YM_CASH_ON_DELIVERY"] = "Наличный расчет при получении заказа";
$MESS["SALE_YM_CARD_ON_DELIVERY"] = "Оплата банковской картой при получении заказа";
$MESS["SALE_YM_PAYER_TYPE"] = "Тип плательщика для заказов";
$MESS["SALE_YM_CAMPAIGN_ID"] = "Идентификатор компании";
$MESS["SALE_YM_YANDEX_URL"] = "URL ресурса партнерского API Яндекс.Маркета.";
?>