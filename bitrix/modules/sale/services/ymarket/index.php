<?
define("NO_AGENT_CHECK", true);
define("NO_AGENT_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("DisableEventsCheck", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule('sale'))
{
	CHTTP::SetStatus("500 Internal Server Error");
	die('{"error":"Module \"sale\" not installed"}');
}

$requestObject = isset($_REQUEST["REQUEST_OBJECT"]) ? $_REQUEST["REQUEST_OBJECT"] : '';
$method = isset($_REQUEST["METHOD"]) ? $_REQUEST["METHOD"] : '';
$postData = '';

if($_SERVER['REQUEST_METHOD'] == 'POST' && count($_POST) <= 0)
	$postData = file_get_contents("php://input");

$YMHandler = new CSaleYMHandler;
$result = $YMHandler->processRequest($requestObject, $method, $postData);
$APPLICATION->RestartBuffer();
echo $result;

require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_after.php");
?>