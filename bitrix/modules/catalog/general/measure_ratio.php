<?
IncludeModuleLangFile(__FILE__);

class CCatalogMeasureRatioAll
{
	protected static function checkFields($action, &$arFields)
	{
		if(is_set($arFields, "RATIO"))
		{
			$arFields["RATIO"] = str_replace(',', '.', $arFields["RATIO"]);
			$arFields["RATIO"] = doubleval($arFields["RATIO"]);
		}
	/*	if((($action == 'ADD') || is_set($arFields, "BARCODE")) && (strlen($arFields["BARCODE"]) <= 0))
		{
			$GLOBALS["APPLICATION"]->ThrowException(GetMessage("CP_EMPTY_BARCODE"));
			return false;
		}*/

		return true;
	}

	public static function update($id, $arFields)
	{
		$id = intval($id);
		if($id < 0 || !self::checkFields('UPDATE', $arFields))
			return false;
		global $DB;
		$strUpdate = $DB->PrepareUpdate("b_catalog_measure_ratio", $arFields);
		$strSql = "UPDATE b_catalog_measure_ratio SET ".$strUpdate." WHERE ID = ".$id;
		if(!$DB->Query($strSql, true, "File: ".__FILE__."<br>Line: ".__LINE__))
			return false;
		return $id;
	}

	public static function delete($id)
	{
		global $DB;
		$id = intval($id);
		if($id > 0)
		{
			if($DB->Query("DELETE FROM b_catalog_measure_ratio WHERE ID = ".$id." ", true))
				return true;
		}
		return false;
	}
}