<?php

class CityManager
{
    protected static $instance = null;

    protected $cities = null;

    protected $socialIdToNameMap = array(
        'URL_VK' => 'vk',
        'URL_FB' => 'fb',
        'URL_IG' => 'inst',
        'URL_OK' => 'ok',
        'URL_YT' => 'youtube',
    );

    protected $externals = array(
        'samara' => 'http://samara.mamadeti.ru/',
        'tolyatti' => 'http://samara.mamadeti.ru/',
        'novokuybyshevsk' => 'http://samara.mamadeti.ru/',
    );

    protected $rewrites = array(
        '#\/program.*#' => '/program/',
        '#\/price-list2.*#' => '/price-list2/',
    );

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        CModule::IncludeModule('iblock');
        $this->rewrites['#\/clinics\/.*#'] = function ($matches, $cityId, $cityCode) {
            return '/clinics/';
        };
    }

    public function getCities()
    {
        if ($this->cities === null) {
            $cache_dir = '/';

            $obCache = new CPHPCache();
            if ($obCache->InitCache(60 * 60 * 24, "cities", $cache_dir)) {
                $vars = $obCache->GetVars();
                $cities = $vars[0];
            } else {
                $cities = [];
                $rsCities =
                    CIBlockElement::GetList(
                        array('NAME' => 'ASC'),
                        array("IBLOCK_ID" => CITY_IBLOCK_ID, 'ACTIVE' => 'Y'),
                        false,
                        false,
                        array(
                            'IBLOCK_ID',
                            'ID',
                            'NAME',
                            'CODE',
                            'PROPERTY_DEFAULT',
                            'NAME_PREPOSITIONAL',
                            'PROPERTY_PHONE',
                            'PROPERTY_PHONE_CODE_LENGTH',
                            /*'PROPERTY_PHONE',
                            'PROPERTY_children_born',
                            'PROPERTY_childbirth_count',*/
                        )
                    );
                while ($city = $rsCities->GetNextElement()) {
                    $city = array_merge($city->GetFields(), ['PROPERTIES' => $city->GetProperties()]);
                    $cities[$city['ID']] = $city;
                }
                
                if ($obCache->StartDataCache()) {
                    if (defined("BX_COMP_MANAGED_CACHE")) {
                        global $CACHE_MANAGER;
                        $CACHE_MANAGER->StartTagCache($cache_dir);
                        $CACHE_MANAGER->RegisterTag("iblock_id_" . CITY_IBLOCK_ID);
                        $CACHE_MANAGER->EndTagCache();
                    }
                    $obCache->EndDataCache(array($cities));
                }
            }
            
            $this->cities = $cities;
        }

        return $this->cities;
    }

    protected function getCityByProp($propName, $propValue)
    {
        $cities = $this->getCities();

        foreach ($cities as $city) {
            if ($city[$propName] == $propValue) {
                return $city;
            }
        }

        return null;
    }

    public function getCityById($cityId)
    {
        $cities = $this->getCities();

        return isset($cities[$cityId]) ? $cities[$cityId] : null;
    }

    public function getCityByName($cityName)
    {
        return $this->getCityByProp('NAME', $cityName);
    }

    public function getCityByCode($cityCode)
    {
        return $this->getCityByProp('CODE', $cityCode);
    }

    public function isCurrentCityDefault()
    {
        return $this->getDefaultCity() === $this->getCurrentCity();
    }

    public function getDefaultCity()
    {
        return $this->getCityByProp('PROPERTY_DEFAULT_VALUE', 1);
    }

    public function getCodeByCityId($cityId)
    {
        $city = $this->getCityById($cityId);
        if (!$city) {
            return null;
        }

        return $city['CODE'];
    }

    public function getDomainByCityId($cityId, $mobile = null)
    {
        $city = $this->getCityById($cityId);
        if (!$city) {
            return null;
        }
        
        if (isset($this->externals[$city['CODE']])) {
            $domain = parse_url($this->externals[$city['CODE']], PHP_URL_HOST);
            
            return $domain;
        }

        $isDefault = $city['PROPERTY_DEFAULT_VALUE'] == 1;

        if ($mobile === null) {
            $isMobile = IS_MOBILE_SITE;
        } else {
            $isMobile = $mobile;
        }
        $mobilePrefix = $isMobile ? 'm.' : '';

        if (EnvManager::getInstance()->isDev()) {
            return $mobilePrefix.($isDefault ? '' : $city['CODE'].'.').'dev.mamadeti.ru';
        } else {
            return $mobilePrefix.($isDefault ? '' : $city['CODE'].'.').'mamadeti.ru';
        }
    }

    public function getCityIdByDomain($domain = null)
    {
        if ($domain === null) {
            $domain = $_SERVER['HTTP_HOST'];
        }

        $domain = preg_replace('/^m\./', '', $domain);

        if (EnvManager::getInstance()->isDev()) {
            $domain = preg_replace('/\.*dev\./', '.', $domain);
        } else {
            $domain = preg_replace('/^www\./', '', $domain);
        }

        $matches = null;
        preg_match('/([^\.]+)\.mamadeti\.ru/', $domain, $matches);

        if (!$matches) {
            $city = $this->getDefaultCity();
        } else {
            $city = $this->getCityByCode($matches[1]);
        }

        return $city['ID'];
    }

    public function saveCityId($cityId)
    {
        global $APPLICATION;

        $APPLICATION->set_cookie(CITY_COOKIE_NAME, $cityId, time()+60*60*24*30*12, '/', COOKIE_DOMAIN);
    }

    public function getSavedCityId()
    {
        return GetCurrentCity();

        /*global $APPLICATION;

        $cityId = $APPLICATION->get_cookie(CITY_COOKIE_NAME);

        if (!$this->getCityById($cityId)) {
            return null;
        }

        return $cityId;*/
    }

    public function getCurrentCityId()
    {
        return $this->getSavedCityId();
    }

    public function getCurrentCity()
    {
        $cityId = GetCurrentCity();

        return $this->getCityById($cityId);
    }

    public function getUrlByCityId($cityId)
    {
        global $APPLICATION;
        $city = $this->getCityById($cityId);
        if (isset($this->externals[$city['CODE']])) {
            return $this->externals[$city['CODE']];
        }
        $currentDirectory = $APPLICATION->GetCurDir();
        $cityCode = $this->getCodeByCityId($cityId);
        foreach ($this->rewrites as $pattern => $rewrite) {
            if (preg_match($pattern, $currentDirectory)) {
                if (is_callable($rewrite)) {
                    $currentDirectory = preg_replace_callback($pattern, function ($matches) use ($rewrite, $cityId, $cityCode) {
                        return call_user_func_array($rewrite, array($matches, $cityId, $cityCode));
                    }, $currentDirectory);
                } else {
                    $currentDirectory = preg_replace($pattern, $rewrite, $currentDirectory);
                }
                break;
            }
        }
        $currentCity = $this->getCurrentCity();
        $url = '';
        if ($currentCity['ID'] != $city['ID']) {
            $url .= $_SERVER['HTTPS'] ? 'https://' : 'http://';
            $url .= $this->getDomainByCityId($cityId);
        }
        $url .= $currentDirectory;

        return $url;
    }
    /**
     *
     * Returns array stdClasses with fields:
     *  * name,
     *  * url.
     *
     * @return \stdClass[]
     */
    public function getCitySocialUrlsByCityId($cityId)
    {
        $city = $this->getCityById($cityId);
        if (empty($city)) {
            return array();
        }

        $socials = array();
        foreach ($this->socialIdToNameMap as $socialId => $socialName) {
            if (!empty($city['PROPERTIES'][$socialId]['VALUE'])) {
                $social = new \stdClass;
                $social->name = $socialName;
                $social->url = $city['PROPERTIES'][$socialId]['VALUE'];
                $socials[] = $social;
            }
        }

        return $socials;
    }
}
