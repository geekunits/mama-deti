<?php

class PhoneFormatter
{
    public static $defaultCountryCode = 7;

    protected static $cachedPhones = [];

    public static function formatPhoneForDisplay($phone)
    {
        if(!isset(static::$cachedPhones[$phone])){
            $digits = static::getOnlyPhoneDigits($phone);

            if(mb_strlen($digits) == 11){
                $formattedPhone = '+' . static::$defaultCountryCode;
                $formattedPhone.= ' (' . mb_substr($digits, 1, 3) . ')';
                $formattedPhone.= ' ' . mb_substr($digits, 4, 3);
                $formattedPhone.= ' ' . mb_substr($digits, 7, 2);
                $formattedPhone.= ' ' . mb_substr($digits, 9, 2);
            }else{
                $formattedPhone = $phone;
            }

            static::$cachedPhones[$phone] = $formattedPhone;
        }

        return static::$cachedPhones[$phone];
    }

    public static function formatPhoneForLink($phone)
    {
        $phone = preg_replace('/[^\d]+/', '', $phone);
        $phone = '+'.$phone;

        return $phone;
    }

    public static function formatPhone($phone)
    {
        return '<a class="phone-nowrap" href="tel:'.static::formatPhoneForLink($phone).'">'.static::formatPhoneForDisplay($phone).'</a>';
    }

    public static function formatPhones($phones)
    {
        $phones = strip_tags($phones);
        $phones = preg_split('/,\s*/', $phones);
        $phones = array_map(function($phone) {
            return static::formatPhone($phone);
        }, $phones);

        return implode(', ', $phones);
    }

    public static function format($phones)
    {
        return static::formatPhones($phones);
    }

    public static function comparePhones($phone, $comparePhone)
    {
        $phones = array($phone, $comparePhone);

        foreach($phones AS &$phone){
            $phone = static::getOnlyPhoneDigits($phone);

            if(mb_substr($phone, 0, 1) != static::$defaultCountryCode){
                $phone = static::$defaultCountryCode . mb_substr($phone, 1);
            }
        }

        return $phones[0] == $phones[1];
    }

    public static function getOnlyPhoneDigits($phone)
    {
        $phone = strip_tags($phone);

        return preg_replace('/[^\d]+/', '', $phone);
    }

    /*
     * Если пользователь из-за границы, и номер совпал с горячей линией,
     * то нам нам нужно получить номер из строки и подменить его на номер горячей линии для заграницы
     * */
    public static function prepare($phone)
    {
        if(CLIENT_FROM_ABROAD && PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phone)){
            return preg_replace('/(.*?\>)([\+\d\s\(\)]+)(\<\/.*?)/siu', '$1' . static::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER) . '$3', $phone);
        }

        return $phone;
    }
}
