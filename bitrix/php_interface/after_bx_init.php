<?php
// Файл подключается сразу после инициализации ядра bitrix в init.php.
// Он откатывает изменения, сделанные в before_bx_init.php.
if (isset($_SERVER['OLD_HTTP_HOST'])) {
    $_SERVER['HTTP_HOST'] = $_SERVER['OLD_HTTP_HOST'];
    unset($_SERVER['OLD_HTTP_HOST']);
}
