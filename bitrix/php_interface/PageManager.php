<?php 

class PageManager {

	private $_vars = [];
	private static $_instance;

	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	private function __construct() {

	}


	public function addVar($name, $value) {
		$this->_vars[$name] = $value;
		return $this;
	}

	public function renderVariables() {
		if (!empty($this->_vars)) {
			echo '<script>';
			foreach($this->_vars as $name => $value) {
				echo 'window["' . $name . '"] = ' . json_encode($value) . ';' . PHP_EOL;
			}
			echo '</script>';
		}
	}
}