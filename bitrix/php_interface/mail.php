<?php

function custom_mail($to, $subject, $message, $additionalHeaders = '')
{
   $smtpServerHost         = '127.0.0.1';
   $smtpServerHostPort      = 25;

   $eol = CAllEvent::GetMailEOL();
   $from = 'info@mamadeti.ru';
   $additionalHeaders = preg_replace('/From: (.+)\n/i', "From: $from\n", $additionalHeaders);
   $additionalHeaders .= $eol . 'To: ' . $to . $eol . 'Subject: ' . $subject;

   $recepients = is_array($to) ? $to : explode(',', $to);

   foreach ($recepients as $recepient) {
       if (!($smtp = new Net_SMTP($smtpServerHost, $smtpServerHostPort))) {
           return false;
       }

       //$smtp->setDebug(true);

       if (PEAR::isError($e = $smtp->connect())) {
           return false;
       }

       $smtp->mailFrom($from);
       $smtp->rcptTo(trim($recepient));

       $smtp->data($additionalHeaders . "\r\n\r\n" . $message);

       $smtp->disconnect();
   }

   return true;
}
