<?php

require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/after_bx_init.php';

include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/makeimage.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/mamadeti_api.php';

require_once __DIR__.'/mail.php';

require_once __DIR__.'/EnvManager.php';
require_once __DIR__.'/PageManager.php';
require_once __DIR__.'/OgpManager.php';
require_once __DIR__.'/CityManager.php';
require_once __DIR__.'/ClinicManager.php';
require_once __DIR__.'/RegionSearchIndexer.php';
require_once __DIR__.'/ProcedureManager.php';
require_once __DIR__.'/UrlReplacer.php';
require_once __DIR__.'/PhoneFormatter.php';

require_once __DIR__.'/include/program/ProgramHelper.php';
require_once __DIR__.'/include/program/ProgramEvents.php';
require_once __DIR__.'/include/program/ProgramDirectionEvents.php';

require_once __DIR__.'/include/service/Service.php';
require_once __DIR__.'/include/service/ServiceMirror.php';
require_once __DIR__.'/include/service/ServiceDescription.php';
require_once __DIR__.'/include/service/ServiceEvents.php';

require_once __DIR__.'/include/pricelist/PricelistEvents.php';

require_once __DIR__.'/include/clinicMenu/ClinicMenuTab.php';
require_once __DIR__.'/include/clinicMenu/GroupClinicMenuTab.php';
require_once __DIR__.'/include/clinicMenu/ServiceClinicMenuTab.php';
require_once __DIR__.'/include/clinicMenu/ClinicMenuRenderer.php';
require_once __DIR__.'/include/clinicMenu/PropertyClinicMenuRenderer.php';
require_once __DIR__.'/include/clinicMenu/FileClinicMenuRenderer.php';
require_once __DIR__.'/include/clinicMenu/ClinicMenu.php';
require_once __DIR__.'/include/clinicMenu/ClinicResultFormatter.php';

require_once __DIR__.'/include/sitemap/Sitemap.php';
require_once __DIR__.'/include/sitemap/IndexSitemap.php';
require_once __DIR__.'/include/sitemap/UrlSitemap.php';
require_once __DIR__.'/include/sitemap/SitemapGenerator.php';
require_once __DIR__.'/include/sitemap/SitemapSection.php';
require_once __DIR__.'/include/sitemap/url/SitemapSectionManager.php';
require_once __DIR__.'/include/sitemap/url/IBlockSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/ServiceSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/ClinicSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/DoctorSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/NewsSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/ProgramSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/ActionSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/GallerySitemapSection.php';
require_once __DIR__.'/include/sitemap/url/PricelistSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/ArticleSitemapSection.php';
require_once __DIR__.'/include/sitemap/url/MiscSitemapSection.php';


require_once __DIR__.'/include/melcosoft.php';
require_once __DIR__.'/include/logHandler.php';
require_once __DIR__.'/include/faqHandler.php';
require_once __DIR__.'/include/functions.php';
/**Перезагрузка классов для модуля SEO **/
require_once __DIR__.'/sitemapfile.php';
require_once __DIR__.'/sitemapindex.php';

require_once __DIR__.'/include/Schedule.php';
require_once __DIR__.'/include/skypeConsultation/SkypeConsultation.php';
require_once __DIR__.'/include/skypeConsultation/SkypeConsultationFinder.php';

require_once __DIR__.'/include/forPress/ForPressSection.php';
require_once __DIR__.'/include/forPress/ForPressElement.php';
require_once __DIR__.'/include/forPress/ForPressHelper.php';

//require_once __DIR__.'/include/instagram/InstagramAPI.php';

$detect = new Mobile_Detect();
define('IS_MOBILE', $detect->isMobile());
define('IS_TABLET', $detect->isTablet());
define('IS_MOBILE_SITE', SITE_ID === 'mo');

define('COOKIE_DOMAIN', 'mamadeti.ru');
define('CITY_COOKIE_NAME', 'SAVED_CITY_ID');
define('BX_CRONTAB_SUPPORT', true);

define('ASSET_VERSION', 85);

define('SERVICE_IBLOCK_ID', 1);
define('CLINIC_IBLOCK_ID', 2);
define('DOCTOR_IBLOCK_ID', 3);
define('SECTION_BANNER_IBLOCK_ID', 5);
define('CITY_IBLOCK_ID', 7);
define('NEWS_IBLOCK_ID', 8);
define('QA_IBLOCK_ID', 10);
define('REVIEW_IBLOCK_ID', 11);
define('STORY_IBLOCK_ID', 12);
define('SPECIALTY_IBLOCK_ID', 19);
define('ACTION_IBLOCK_ID', 32);
define('PROGRAM_IBLOCK_ID', 27);
define('PROGRAM_DIRECTION_IBLOCK_ID', 30);
define('ARTICLE_IBLOCK_ID', 9);
define('SERVICE_DESCRIPTION_IBLOCK_ID', 39);
define('MEMO_IBLOCK_ID', 33);
define('PRICELIST_IBLOCK_ID', 31);
define('PROCEDURES_IBLOCK_ID', 38);
define('VACANCIES_ELEMENT_ID', 2371);
define('GALLERY_IBLOCK_ID', 13);
define('EVENTFORM_IBLOCK_ID', 40);
define('SKYPE_CONSULTATION_IBLOCK_ID', 41);
define('ECO_REQUEST_IBLOCK_ID', 42);
define('FOR_PRESS_IBLOCK_ID', 43);
define('PACIENT_INSTRUCTIONS', 56);

define('INSTAGRAM_LOGIN', 'mamadeti.app');
define('INSTAGRAM_ACCESS_TOKEN', '3043925600.a7f6f15.7d214d5981984296bf695e92ea2033e9');

define('SOAP_SERVICE_USER', 'EMEA\ruCSA_MCcbksSvc');
define('SOAP_SERVICE_PASS', 'A4JHc_iC2Xrt5iE-Nd5P');
define('SOAP_SERVICE_WSDL', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/MCService.wsdl');

$clientFromAbroad = false;
// '5.254.65.111'
if(($geo = getGeoDataByIP()) !== null && $geo->country->isoCode !== 'RU') {
    $clientFromAbroad = true;
}

define('CLIENT_FROM_ABROAD', $clientFromAbroad);

define('HOTLINE_PHONE_NUMBER', '78007007001');
define('HOTLINE_PHONE_NUMBER_MSK', '74956607001');
define('GLOBAL_PHONE_NUMBER', CLIENT_FROM_ABROAD ? HOTLINE_PHONE_NUMBER_MSK : HOTLINE_PHONE_NUMBER);

function onEcoRequestAfterIBlockAdd(&$arFields)
{
    $iBlockId = $arFields['IBLOCK_ID'];
    if ($iBlockId != ECO_REQUEST_IBLOCK_ID) {
        return;
    }
    $eventType = 'ECO_REQUEST_ADD';
    $propertyCodes = array(
        'PHONE',
        'EMAIL',
        'AGE_WOMAN',
        'AGE_MAN',
        'AGE_NOCHILD',
        'OPERATIONS_ABDOMEN',
        'HYSTEROSCOPY',
        'LAPAROSCOPY',
        'LAPAROTOMY_SURGERY',
        'HYSTEROMYOMA',
        'ENDOMETRIOSIS',
        'INFLAMMATORY_DISEASES',
        'OTHER',
        'OTHER_SURGERY',
        'INJURIES',
        'INJURIES_OTHER',
        'CHRONIC_DISEASES',
        'HEREDITY_WOMAN',
        'HEREDITY_MAN',
        'SPERMATOGENESIS',
        'INSEMINATIONS',
        'INSEMINATIONS_EKO',
        'ABORTIONS',
        'RESOURCE',
        'RESOURCE_MORE',
    );
    $properties = array();
    $mailFields = array();
    $dbProperties = CIBlockElement::GetProperty($iBlockId, $arFields['ID']);
    while ($property = $dbProperties->GetNext()) {
        $propertyCode = $property['CODE'];
        if (in_array($propertyCode, $propertyCodes)) {
            if (!empty($property['VALUE_ENUM'])) {
                $propertyValue = $property['VALUE_ENUM'];
            } else {
                $propertyValue = is_array($property['VALUE']) ? implode(',', $property['VALUE']) : $property['VALUE'];
            }
            $mailFields[$propertyCode] = $propertyValue;
        }
        $properties[] = $property;
    }
    $mailFields['NAME'] = $arFields['NAME'];
    $mailFields['ID'] = $arFields['ID'];
    CEvent::Send($eventType, SITE_ID, $mailFields);
}

$regionSearchIndexer = new RegionSearchIndexer();
$regionSearchIndexer->
    // Клиники
    addRule(2, new RegionByCityLookup('CITY'))->
    // Врачи
    addRule(3, new RegionByCityLookup('CITY'))->
    // Лицензии
    addRule(16, new RegionByClinicLookup('CLINIC'))->
    // Новости
    addRule(8, new RegionByClinicLookup('CLINIC'))->
    // Акции
    addRule(32, new RegionByClinicLookup('CLINIC'))->
    // Отзывы
    addRule(11, new RegionByClinicLookup('CLINIC'))->
    // Вопрос-Ответ
    addRule(10, new RegionByCityLookup('CITY'))->
    // Программы
    addRule(27, new RegionByClinicLookup('CLINIC'))->
    // Фотогалерея
    addRule(13, new RegionByClinicLookup('CLINIC'))->
    // Памятки
    addRule(33, new RegionByClinicLookup('CLINICS'))->
    // Фотогалерея
    addRule(37, new RegionByClinicLookup('CLINIC'));

AddEventHandler('search', 'BeforeIndex', array($regionSearchIndexer, 'beforeIndex'));

AddEventHandler('main', 'OnAdminListDisplay', array('CMelcosoft', 'OnAdminListDisplayHandler'));
AddEventHandler('subscribe', 'BeforePostingSendMail', array('CMelcosoft', 'BeforePostingSendMailHandler'));
AddEventHandler("main", "OnEpilog", Array("CMelcosoft", "Redirect404"));

AddEventHandler('iblock', 'OnAfterIBlockSectionAdd', array('ServiceEvents', 'onAfterIBlockSectionAdd'));
AddEventHandler('iblock', 'OnAfterIBlockSectionUpdate', array('ServiceEvents', 'onAfterIBlockSectionUpdate'));
AddEventHandler('iblock', 'OnBeforeIBlockSectionDelete', array('ServiceEvents', 'onBeforeIBlockSectionDelete'));
AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array('ServiceEvents', 'onAfterIBlockElementAdd'));
AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('ServiceEvents', 'onAfterIBlockElementUpdate'));
AddEventHandler('iblock', 'OnBeforeIBlockElementDelete', array('ServiceEvents', 'onBeforeIBlockElementDelete'));

AddEventHandler('iblock', 'OnAfterIBlockSectionUpdate', array('PricelistEvents', 'onAfterIBlockSectionUpdate'));

AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array('ProgramEvents', 'OnAfterIBlockElementAdd'));
AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('ProgramEvents', 'OnAfterIBlockElementUpdate'));

AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('ProgramDirectionEvents', 'onAfterIBlockElementUpdate'));
AddEventHandler('iblock', 'OnBeforeIBlockElementDelete', array('ProgramDirectionEvents', 'onBeforeIBlockElementDelete'));

AddEventHandler('iblock', 'OnAfterIBlockElementAdd', 'onEcoRequestAfterIBlockAdd');

LogHandler::attachEvents();

FaqHandler::attachEvents();

/*
AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array('Logging', 'onAfterIBlockElementAddLog'));
AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array('Logging', 'onAfterIBlockElementUpdateLog'));
AddEventHandler('iblock', 'OnAfterIBlockElementDelete', array("Logging", "onAfterIBlockElementDeleteLog"));
AddEventHandler('iblock', 'OnAfterIBlockSectionAdd', array('Logging', 'onAfterIBlockSectionAddLog'));
AddEventHandler('iblock', 'OnAfterIBlockSectionUpdate', array('Logging', 'onAfterIBlockSectionUpdateLog'));
AddEventHandler('iblock', 'OnAfterIBlockSectionDelete', array("Logging", "onAfterIBlockSectionDeleteLog"));
*/
CityDetection();

RedirectToMobile();
