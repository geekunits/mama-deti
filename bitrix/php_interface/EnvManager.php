<?php

class EnvManager
{
    protected static $instance = null;

    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    protected function __construct()
    {
    }

    public function isDev()
    {
        return defined('ENV') && ENV === 'dev';
    }

    public function isProd()
    {
        return !defined('ENV') || ENV === 'prod';
    }
}
