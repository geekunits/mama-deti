<?php

class SitemapGenerator
{
    protected $cityCode = null;
    protected $domain = null;
    protected $path = null;

    protected $urls = [];

    public function __construct($cityCode, $isMobile = false)
    {
        $cm = CityManager::getInstance();

        $city = $cm->getCityByCode($cityCode);

        $this->cityCode = $this->cityCode;
        $this->domain = $cm->getDomainByCityId($city['ID'], $isMobile);

        $this->path = $_SERVER['DOCUMENT_ROOT'].'/sitemaps/'.$city['CODE'];
        if (!file_exists($this->path)) {
            mkdir($this->path);
        }
    }

    /**
     * Физический путь к файлу указанной группы ссылок.
     *
     * @param  string $section
     * @return string
     */
    protected function getSectionFileName($section)
    {
        return $this->path.'/sitemap-'.$section.'.xml';
    }

    /**
     * Урл к файлу указанной группы ссылок.
     *
     * @param  string $section
     * @return string
     */
    protected function getSectionUrl($section)
    {
        return 'http://'.$this->domain.'/sitemap-'.$section.'.xml';
    }

    /**
     * Физический путь к индексному файлу карты сайта.
     *
     * @return string
     */
    protected function getIndexFileName()
    {
        return $this->path.'/sitemap.xml';
    }

    protected function closeUrlFiles()
    {
        foreach ($this->urls as $section => $sitemap) {
            $sitemap->closeFile();
        }
    }

    protected function addSitemap($indexSitemap, $section)
    {
        $indexSitemap->addSitemap(
            $this->getSectionUrl($section),
            date('c', filemtime($this->getSectionFileName($section)))
        );
    }

    protected function saveIndex()
    {
        $indexSitemap = new IndexSitemap();
        $indexSitemap->openFile($this->getIndexFileName());
        foreach ($this->urls as $section => $sitemap) {
            if ($sitemap->getFileCount() === 1) {
                $this->addSitemap($indexSitemap, $section);
            } else {
                for ($index = 1; $index <= $sitemap->getFileCount(); $index++) {
                    $this->addSitemap($indexSitemap, $section.'-'.$index);
                }
            }
        }
        $indexSitemap->closeFile();
    }

    /**
     * Добавить URL в карту сайта.
     *
     * Название группы ссылок будет использоваться в качестве суффикса файла карты
     * сайта.
     *
     * @param string $section    название группы ссылок
     * @param string $loc        ссылка
     * @param string $lastmod
     * @param string $priority
     * @param string $changefreq
     */
    public function addUrl($section, $loc, $lastmod, $priority = '1.0', $changefreq = 'monthly')
    {
        if (!isset($this->urls[$section])) {
            $sitemap = new UrlSitemap();
            $sitemap->openFile($this->getSectionFileName($section));

            $this->urls[$section] = $sitemap;
        }

        $loc = 'http://' . $this->domain . $loc;

        $this->urls[$section]->addUrl($loc, $lastmod, $priority, $changefreq);
    }

    /**
     * Сохранить индексный файл.
     */
    public function save()
    {
        $this->closeUrlFiles();
        $this->saveIndex();
    }
}
