<?php

abstract class SitemapSection implements Iterator {

	protected $cityId;
	private $_records;
	private $_current = 0;

	public function __construct($cityId, $config) {
		$this->cityId = $cityId;
		foreach($config as $property => $value) {
			if (!property_exists($this, $property)) {
				throw new InvalidArgumentException('Unknown property ' . $property . ' in class ' . get_class($this) . '.');
			}
			$this->$property = $value;
		}
	}

	abstract protected function fetchRecords();

	protected function buildPriority($depth) {
		$depth = (int) $depth;
		switch($depth) {
			case 0: return '1.0';
			case 1: return '0.9';
			case 2: return '0.9';
			case 3: return '0.8';
			default: return '0.7';
		}
	}

	protected function buildLastmod($timestamp) {
		return $this->buildLastmodFromTimestamp(strtotime($timestamp));
	}

	protected function buildLastmodFromTimestamp($time = null) {
		if (empty($time)) {
			$time = time();
		}
		return date('Y-m-d', $time);
	}

	protected function getClinicIds() {
		return CMamaDetiAPI::getClinicsID([
			'PROPERTY_CITY' => $this->getCityId(),
		]);
	}

	protected function getCityId() {
		return $this->cityId;
	}

	public function getRecords() {
		if (is_null($this->_records)) {
			$this->_records = array_values($this->fetchRecords());
		}
		return $this->_records;
	}

	public function getRecord() {
		if (!$this->valid()) {
			return false;
		}
		$record = $this->current();
		$this->next();
		return $record;
	}

	protected function generatePaginationUrls($baseUrl, $pagesCount, $paginationParam = 'PAGEN_1') {
		$urls = [];
		for($index = 2; $index <= $pagesCount; $index++) {
			$urls[] = $baseUrl . '?' . http_build_query([
				$paginationParam => $index,
			]);
		}
		return $urls;
	}

	public function current() {
		$records = $this->getRecords();
		return $this->valid() ? $records[$this->_current] : null;
	}

	public function key() {
		return $this->_current;
	}

	public function next() {
		++ $this->_current;
	}

	public function rewind() {
		$this->_current = 0;
	}

	public function valid() {
		return $this->_current < count($this->getRecords());
	}


}