<?php

class UrlSitemap extends Sitemap
{
    protected function getHeaderXml()
    {
        return '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    }

    protected function getFooterXml()
    {
        return '</urlset>';
    }

    public function addUrl($loc, $lastmod, $priority, $changefreq)
    {
        $this->addRecord(
            '<url>'
            .'<loc>'.$this->escapeValue($loc).'</loc>'
            .'<lastmod>'.$this->escapeValue($lastmod).'</lastmod>'
            .'<priority>'.$this->escapeValue($priority).'</priority>'
            .'<changefreq>'.$this->escapeValue($changefreq).'</changefreq>'
            .'</url>'
        );

        return $this;
    }
}
