<?php

class IndexSitemap extends Sitemap
{
    protected function getHeaderXml()
    {
        return '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    }

    protected function getFooterXml()
    {
        return '</sitemapindex>';
    }

    public function addSitemap($loc, $lastmod)
    {
        $this->addRecord(
            '<sitemap>'
            .'<loc>'.$this->escapeValue($loc).'</loc>'
            .'<lastmod>'.$this->escapeValue($lastmod).'</lastmod>'
            .'</sitemap>'
        );

        return $this;
    }
}
