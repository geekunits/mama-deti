<?php

class ClinicSitemapSection extends IBlockSitemapSection {

	public $folder = '/clinics/';

	protected function fetchRecords() {
		$records = parent::fetchRecords();
		$elements = $this->getElements();
		$priority = $this->buildPriority(3);
		foreach($elements as $element) {
			$formatter = new ClinicResultFormatter($element['ID']);
			$menu = new ClinicMenu($formatter->format(), ['detectCurrentTab' => false]);
			$tabs = $menu->getActiveTabs();

            foreach($tabs as $tab) {
                $loc = $tab->getUrl();

				if ($tab->getName() == 'description' || strpos($loc, '/services') === 0) {
					continue;
				}

				$records[$loc] = [$loc, $this->buildLastmod($element['TIMESTAMP_X']), $priority];
			}
		}
		return $records;
	}

	protected function getFilter() {
		return [
			'IBLOCK_ID' => CLINIC_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'PROPERTY_CITY' => $this->getCityId(),
		];
	}

}