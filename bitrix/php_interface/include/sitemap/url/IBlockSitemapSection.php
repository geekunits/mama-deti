<?php

abstract class IBlockSitemapSection extends SitemapSection {

	public $folder;

	protected function fetchRecords() {
		$records = [];

		$elements = $this->getElements();
		$priority = $this->buildPriority(2);

		$maxTimestamp = 0;

		foreach($elements as $element) {
			$loc = $element['DETAIL_PAGE_URL'];
			$timestamp = strtotime($element['TIMESTAMP_X']);
			$maxTimestamp = max([$maxTimestamp, $timestamp]);

			$records[$loc] = [$loc, $this->buildLastmodFromTimestamp($timestamp), $priority];
		}

		if (!empty($this->folder)) {
			$loc = $this->folder;

			if (empty($maxTimestamp)) {
				$maxTimestamp = time();
			}

			$records[$loc] = [$loc, $this->buildLastmodFromTimestamp($maxTimestamp), $this->buildPriority(1)];
		}

		return $records;
	}

	protected function getElements() {
		$sort = ['SORT' => 'ASC'];

		$filter = $this->getFilter();

		$select = [
			'ID',
			'IBLOCK_ID',
			'DETAIL_PAGE_URL',
			'TIMESTAMP_X',
		];

		$elements = [];
		$result = CIBlockElement::GetList($sort, $filter, false, false, $select);
		if (!empty($result)) {
			while($element = $result->GetNext()) {
				$elements[$element['ID']] = $element;
			}
		}
		return $elements;
	}

	abstract protected function getFilter();

}