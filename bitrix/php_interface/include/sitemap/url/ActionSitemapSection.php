<?php

class ActionSitemapSection extends IBlockSitemapSection {

	public $folder = '/actions/';

	protected function fetchRecords() {
		$records = parent::fetchRecords();
		$perPage = 20;
		$elementsCount = count($records) - 1;
		$pagesCount = (int) ceil($elementsCount / $perPage);

		$paginationUrls = $this->generatePaginationUrls($this->folder, $pagesCount);
		
		list($loc, $lastmod, $priority) = end($records);

		$priority = $this->buildPriority(2);
		foreach($paginationUrls as $loc) {
			$records[$loc] = [$loc, $lastmod, $priority];
		}

		return $records;
	}

	protected function getFilter() {
		$clinicIds = $this->getClinicIds();
		return [
			'IBLOCK_ID' => ACTION_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'PROPERTY_CLINIC' => $clinicIds,			
		];
	}

}