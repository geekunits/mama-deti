<?php

class MiscSitemapSection extends SitemapSection {

	public $pages = [
		['/', 0],
		['/about/general/', 2],
		['/about/contacts/', 2],
		['/about/rukovodstvo/', 2],
		['/about/hot_line_quality/', 2],
		['/about/vacancies-about-us/', 2],
		['/about/feedback/', 2],
	];

	public $dynamicPages = [
		'qa',
		'kolonkaYurista',
	];

	protected function pageQa() {
		$records = [];

		$folder = '/qa/';
		$priority = $this->buildPriority(1);
		$filter = [
			'ACTIVE' => 'Y',
			'IBLOCK_ID' => QA_IBLOCK_ID,
			'!DETAIL_TEXT' => false,
			// '!PROPERTY_CLINIC' => false,

		];
		$select = ['ID', 'IBLOCK_ID', 'TIMESTAMP_X'];
		$sort = ['TIMESTAMP_X' => 'DESC'];
		$nav = ['nTopCount' => 1];

		$perPage = 20;
		$elementsCount = (int) CIBlockElement::GetList($sort, $filter, [], false, $select);
		$result = CIBlockElement::GetList($sort, $filter, false, $nav, $select);
		if (empty($result) || !$element = $result->GetNext()) {
			return [];
		}
		
		$loc = $folder;
		$lastmod = $this->buildLastmod($element['TIMESTAMP_X']);
		$priority = $this->buildPriority(1);
		$records[$loc] = [$loc, $lastmod, $priority];

		$pagesCount = (int) ceil($elementsCount / $perPage);

		$paginationUrls = $this->generatePaginationUrls($folder, $pagesCount);

		$priority = $this->buildPriority(2);
		foreach($paginationUrls as $loc) {
			$records[$loc] = [$loc, $lastmod, $priority];
		}
		return $records;

	}

	protected function pageKolonkaYurista() {
		$loc = '/info/kolonka-yurista/';
		$priority = $this->buildPriority(2);
		$filter = [
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'IBLOCK_ID' => 24,
		];
		$select = ['ID', 'IBLOCK_ID', 'TIMESTAMP_X'];
		$sort = ['TIMESTAMP_X' => 'DESC'];
		$nav = ['nTopCount' => 1];
		$result = CIBlockElement::GetList($sort, $filter, false, $nav, $select);
		if (empty($result) || !$element = $result->GetNext()) {
			return [];
		}
		
		return [$loc => [$loc, $this->buildLastmod($element['TIMESTAMP_X']), $priority]];
	}

	protected function buildDynamicRecords() {
		$records = [];
		foreach($this->dynamicPages as $name) {
			$method = 'page' . ucfirst($name);
			if (method_exists($this, $method)) {
				$records = array_merge($records, $this->$method());
			}
		}
		return $records;
	}

	protected function buildStaticRecords() {
		$records = [];

		foreach($this->pages as $page) {
			list($loc, $depth) = $page;
			$path = rtrim($_SERVER['DOCUMENT_ROOT'], DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR .ltrim($loc, DIRECTORY_SEPARATOR);
			if (file_exists($path)) {

				if (is_dir($path)) {
					$path = $path . 'index.php';
				}
			}
			if (!is_file($path)) {
				continue;
			}
			$timestamp = filemtime($path);
			if (empty($timestamp)) {
				$timestamp = time();
			}
			$records[$loc] = [
				$loc, 
				$this->buildLastmodFromTimestamp($timestamp), 
				$this->buildPriority($depth)
			];
		}

		return $records;
	}

	public function fetchRecords() {
		$records = array_merge([], $this->buildDynamicRecords(), $this->buildStaticRecords());
		return $records;
	}

}