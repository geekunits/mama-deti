<?php

class ServiceSitemapSection extends SitemapSection {

	protected function fetchRecords() {
		$records = [];
		list($serviceMap, $sectionIds) = $this->getServiceMap();
		$serviceIds = array_keys($serviceMap);
		
		$services = $this->getServices($serviceIds);
		$sections = $this->getServiceSections($sectionIds);

		$maxTimestamp = 0;

		foreach($serviceMap as $serviceId => $serviceRecord) {
			$service = $services[$serviceId];
			if (empty($service)) {
				continue;
			}

			$timestamp = strtotime($service['TIMESTAMP_X']);
			$maxTimestamp = max([$timestamp, $maxTimestamp]);

			$depth = count($serviceRecord['sections']) + 2;

			list($loc, $lastmod, $priority) = $this->buildServiceRecord($service, $depth);
			$records[$loc] = [$loc, $lastmod, $priority];

			foreach($serviceRecord['clinics'] as $clinic) {
				list($loc, $lastmod, $priority) = $this->buildServiceRecord($service, $depth + 1, $clinic);
				$records[$loc] = [$loc, $lastmod, $priority];

				foreach($serviceRecord['sections'] as $parentSectionId) {
					$parentSection = $sections[$parentSectionId];
					if (empty($parentSection)) {
						continue;
					}

					list($loc, $lastmod, $priority) = $this->buildSectionRecord($parentSection);
					$records[$loc] = [$loc, $lastmod, $priority];

					list($loc, $lastmod, $priority) = $this->buildSectionRecord($parentSection, $clinic);
					$records[$loc] = [$loc, $lastmod, $priority];
				}

			}
			
		}

		$loc = '/services/';
		$records[$loc] = [$loc, $this->buildLastmodFromTimestamp($maxTimestamp), $this->buildPriority(1)];

		return $records;
	}

	protected function buildServiceRecord($service, $depth, $clinic = null) {
		$loc = $service['DETAIL_PAGE_URL'];
		if (!empty($clinic)) {
			$loc .= 'clinic/' . $clinic['CODE'] . '/';
		}
		$priority = $this->buildPriority($depth);
		$lastmod = $this->buildLastmod($service['TIMESTAMP_X']);
		return [$loc, $lastmod, $priority];
	}

	protected function buildSectionRecord($section, $clinic = null) {
		$depth = $parentSection['DEPTH_LEVEL'] + 1;
		$loc = $section['SECTION_PAGE_URL'];
		if (!empty($clinic)) {
			$loc .= 'clinic/' . $clinic['CODE'] . '/';
			$depth ++;
		}
		$priority = $this->buildPriority($depth);
		$lastmod = $this->buildLastmod($service['TIMESTAMP_X']);
		return [$loc, $lastmod, $priority];

	}

	protected function getServiceMap() {
		$clinics = CMamaDetiAPI::getClinics([
			'PROPERTY_CITY' => $this->getCityId(),
		]);
		$serviceMap = [];
		$sectionIds = [];
		foreach($clinics as $clinic) {
			$clinicServiceIds = $clinic['PROPERTIES']['SERVICES']['VALUE'];
			if (!is_array($clinicServiceIds)) {
				if (is_numeric($clinicServiceIds)) {
					$clinicServiceIds = [$clinicServiceIds];
				} else {
					continue;
				}
			}
			foreach($clinicServiceIds as $serviceId) {
				if (!isset($serviceMap[$serviceId])) {
					$parentIds = CMamaDetiAPI::getElementParentIds($serviceId, true);
					if ($parentIds === false) {
						continue;
					}
					$serviceMap[$serviceId] = [
						'sections' => $parentIds,
						'clinics' => [],
					];
					$sectionIds = array_merge($sectionIds, $parentIds);
				}
				$serviceMap[$serviceId]['clinics'][$clinic['ID']] = $clinic;
			}
		}
		$sectionIds = array_unique($sectionIds);
		return [$serviceMap, $sectionIds];
	}

	protected function getServiceSections(array $sectionIds) {
		$filter = [
			'IBLOCK_ID' => SERVICE_IBLOCK_ID,
			'ID' => $sectionIds,
			'ACTIVE' => 'Y',
			'SECTION_GLOBAL_ACTIVE' => 'Y',
		];
		$select = [
			'IBLOCK_ID',
			'ID',
			'DEPTH_LEVEL',
			'TIMESTAMP_X',
			'SECTION_PAGE_URL',
		];
		$sort = ['SORT' => 'ASC'];
		$sections = [];
		$result = CIBlockSection::GetList($sort, $filter, false, $select);
		if (!empty($result)) {
			while($section = $result->GetNext()) {
				$sections[$section['ID']] = $section;
			}
		}
		return $sections;

	}

	protected function getServices(array $serviceIds) {
		$filter = [
			'IBLOCK_ID' => SERVICE_IBLOCK_ID,
			'ID' => $serviceIds,
			'ACTIVE' => 'Y',
		];
		$select = [
			'IBLOCK_ID',
			'ID',
			'TIMESTAMP_X',
			'DETAIL_PAGE_URL',
		];
		$sort = ['SORT' => 'ASC'];
		$services = [];
		$result = CIBlockElement::GetList($sort, $filter, false, false, $select);
		if (!empty($result)) {
			while($service = $result->GetNext()) {
				$services[$service['ID']] = $service;
			}
		}
		return $services;
	}



}