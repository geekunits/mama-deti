<?php

class DoctorSitemapSection extends IBlockSitemapSection {

	public $folder = '/doctors/';

	protected function fetchRecords() {
		$records = parent::fetchRecords();
		$filter = $this->getFilter();

		$sort = ['SORT' => 'ASC'];
		$select = ['ID', 'PROPERTY_CLINIC'];

		$elementsCount = 0;
		$result = CIBlockElement::GetList($sort, $filter, false, false, $select);
		while($element = $result->GetNext()) {
			$elementsCount += count($element['PROPERTY_CLINIC_VALUE']);
		}

		$perPage = 10;

		$pagesCount = (int) ceil($elementsCount / $perPage);
		
		$paginationUrls = $this->generatePaginationUrls($this->folder, $pagesCount);
		list($loc, $lastmod, $priority) = end($records);
		reset($records);
		foreach($paginationUrls as $loc) {
			$records[$loc] = [$loc, $lastmod, $this->buildPriority(2)];
		}
		return $records;
	}

	protected function getFilter() {
		$clinicIds = $this->getClinicIds();

		return [
			'ACTIVE' => 'Y',
			'IBLOCK_ID' => DOCTOR_IBLOCK_ID,
			'PROPERTY_CLINIC' => $clinicIds,
		];

	}

}