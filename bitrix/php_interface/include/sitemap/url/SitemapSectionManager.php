<?php

class SitemapSectionManager {

	private $_cityId;
	private $_sections;

	public function __construct($cityId) {
		$this->_cityId = $cityId;
	}

	public function getSectionsConfig() {
		return [
			'service' => [
				'class' => 'ServiceSitemapSection',
			],
			'clinic' => [
				'class' => 'ClinicSitemapSection',
			],
			'doctor' => [
				'class' => 'DoctorSitemapSection',
			],
			// 'news' => [
			// 	'class' => 'NewsSitemapSection',
			// ],
			'program' => [
				'class' => 'ProgramSitemapSection',
			],
			'action' => [
				'class' => 'ActionSitemapSection',
			],
			// 'gallery' => [
			// 	'class' => 'GallerySitemapSection',
			// ],
			'pricelist' => [
				'class' => 'PricelistSitemapSection',
			],
			'article' => [
				'class' => 'ArticleSitemapSection',
			],
			'misc' => [
				'class' => 'MiscSitemapSection',
			],
		];
	}

	public function getSections() {
		if (is_null($this->_sections)) {
			$this->_sections = $this->buildSections();
		}
		return $this->_sections;
	}

	protected function buildSection($config) {

	}

	protected function buildSections() {
		$sections = [];
		$cityId = $this->getCityId();
		foreach($this->getSectionsConfig() as $name => $config) {
			$class = $config['class'];
			unset($config['class']);
			$sections[$name] = new $class($cityId, $config);
		}
		return $sections;
	}

	protected function getCityId() {
		return $this->_cityId;
	}
}