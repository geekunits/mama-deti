<?php

class ArticleSitemapSection extends SitemapSection {

	public function fetchRecords() {
		$records = [];

		$sections = $this->getSections();

		$maxTimestamp = 0;

		$priority = $this->buildPriority(2);
		foreach($sections as $section) {
			$loc = $section['SECTION_PAGE_URL'];
			$timestamp = strtotime($section['TIMESTAMP_X']);
			$maxTimestamp = max([$timestamp, $maxTimestamp]);
			$records[$loc] = [$loc, $this->buildLastmodFromTimestamp($timestamp), $priority];
		}

		$elements = $this->getElements();

		$priority = $this->buildPriority(3);
		foreach($elements as $element) {
			$loc = $element['DETAIL_PAGE_URL'];
			$timestamp = strtotime($section['TIMESTAMP_X']);
			$maxTimestamp = max([$timestamp, $maxTimestamp]);
			$records[$loc] = [$loc, $this->buildLastmodFromTimestamp($timestamp), $priority];

		}

		$loc = '/article/';
		$records[$loc] = [$loc, $this->buildLastmodFromTimestamp($maxTimestamp), $this->buildPriority(1)];

		return $records;
	}

	public function getSections() {
		$sort = ['SORT' => 'ASC'];

		$filter = [
			'IBLOCK_ID' => ARTICLE_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
		];

		$select = [
			'ID',
			'IBLOCK_ID',
			'SECTION_PAGE_URL',
			'TIMESTAMP_X',
			'NAME',
		];

		$sections = [];
		$result = CIBlockSection::GetList($sort, $filter, false, $select);
		if (!empty($result)) {
			while($section = $result->GetNext()) {
				$sections[$section['ID']] = $section;
			}
		}
		return $sections;
	}

	public function getElements() {
		$sort = ['SORT' => 'ASC'];

		$filter = [
			'IBLOCK_ID' => ARTICLE_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'SECTION_ACTIVE' => 'Y',
		];

		$select = [
			'ID',
			'IBLOCK_ID',
			'DETAIL_PAGE_URL',
			'TIMESTAMP_X',
			'NAME',
		];

		$elements = [];
		$result = CIBlockElement::GetList($sort, $filter, false, false, $select);
		if (!empty($result)) {
			while($element = $result->GetNext()) {
				$elements[$element['ID']] = $element;
			}
		}
		return $elements;
	}

}