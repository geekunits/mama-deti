<?php

class ProgramSitemapSection extends IBlockSitemapSection {

	public $folder = '/program/';

	protected function getFilter() {

		$clinicIds = $this->getClinicIds();

		return [
			'IBLOCK_ID' => PROGRAM_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'PROPERTY_CLINIC' => $clinicIds,			
		];
	}

}