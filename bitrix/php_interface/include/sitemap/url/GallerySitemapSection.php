<?php

class GallerySitemapSection extends IBlockSitemapSection {

	public $folder = '/gallery/';

	protected function getFilter() {
		$clinicIds = $this->getClinicIds();
		$doctorIds = CMamaDetiAPI::getDoctorsID(['PROPERTY_CLINIC' => $clinicIds]);

		return [
			'IBLOCK_ID' => GALLERY_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			[
				'LOGIC' => 'OR',
				['PROPERTY_CLINIC' => $clinicIds],
				['PROPERTY_DOCTORS' => $doctorIds],
				['PROPERTY_GLOBAL' => 1],
			],		
		];

	}

}