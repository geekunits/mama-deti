<?php

class NewsSitemapSection extends IBlockSitemapSection {

	public $folder = '/news/';

	protected function fetchRecords() {
		$records = parent::fetchRecords();

		$perPage = 12;

		$filter = $this->getFilter();
		$filter['PROPERTY_TYPE'] = 4;
		$sort = ['SORT' => 'ASC'];
		$select = ['ID', 'PROPERTY_TYPE']; 

		$elementsCount = (int) CIBlockElement::GetList($sort, $filter, [], false, $select);
		$pagesCount = (int) ceil($elementsCount / $perPage);

		$paginationUrls = $this->generatePaginationUrls($this->folder, $pagesCount);

		list($loc, $lastmod, $priority) = end($records);

		reset($records);

		foreach($paginationUrls as $loc) {
			$records[$loc] = [$loc, $lastmod, $this->buildPriority(2)];
		}
		return $records;
	}

	protected function getFilter() {

		$clinicIds = $this->getClinicIds();
		$serviceIds = CMamaDetiAPI::getClinicsServicesID(['PROPERTY_CITY' => $this->getCityId()]);

		return [
			'IBLOCK_ID' => NEWS_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			[
				'LOGIC' => 'OR',
				['PROPERTY_CLINIC' => $clinicIds],
				['PROPERTY_CLINIC' => false, 'PROPERTY_SERVICES' => $serviceIds],
				['PROPERTY_GLOBAL' => 1],
			],
			
		];
	}

}