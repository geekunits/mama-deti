<?php

class PricelistSitemapSection extends SitemapSection {

	protected $folder = '/price-list2';
	protected $urlTemplate = '/#CLINIC_CODE#/price/';

	public function fetchRecords() {
		$records = [];

		$loc = rtrim($this->folder, '/') . '/';
		$records[$loc] = [$loc, date('Y-m-d'), $this->buildPriority(1)];

		$clinicIds = $this->getClinicIds();
		
		$sections = $this->getSections();

		$priority = $this->buildPriority(2);
		foreach($sections as $section) {
			$loc = $section['URL'];
			$records[$loc] = [$loc, $this->buildLastmod($section['TIMESTAMP_X']), $priority];
		}
		return $records;
	}

	public function getSections() {
		$sort = ['SORT' => 'ASC'];

		$clinicIds = $this->getClinicIds();
		$allClinics = $clinics = CMamaDetiAPI::GetClinics(['ID' => $clinicIds]);
		$clinics = [];
		foreach($allClinics as $clinic) {
			$clinics[$clinic['ID']] = $clinic;
		}

		$filter = [
			'IBLOCK_ID' => PRICELIST_IBLOCK_ID,
			'ACTIVE' => 'Y',
			'ACTIVE_DATE' => 'Y',
			'UF_CLINIC' => $clinicIds,			
		];

		$select = [
			'ID',
			'IBLOCK_ID',
			'DETAIL_PAGE_URL',
			'TIMESTAMP_X',
			'NAME',
			'UF_CLINIC',
		];

		$sections = [];
		$result = CIBlockSection::GetList($sort, $filter, false, $select);
		if (!empty($result)) {
			while($section = $result->GetNext()) {
				if (!empty($clinics[$section['UF_CLINIC']])) {
					$clinic = $clinics[$section['UF_CLINIC']];
					$section['URL'] = $this->folder . str_replace('#CLINIC_CODE#', $clinic['CODE'], $this->urlTemplate);
					$sections[$section['ID']] = $section;
				}

			}
		}
		return $sections;
	}

}