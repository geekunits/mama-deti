<?php

abstract class Sitemap
{
    public $maxRecordCount = 50000;
    public $maxFileSize = 10485760; // 10MB

    protected $baseFileName = null;
    protected $count = 1;

    protected $recordCount = 0;
    protected $fileSize = 0;

    protected $fh;

    abstract protected function getHeaderXml();

    abstract protected function getFooterXml();

    protected function write($string)
    {
        fputs($this->fh, $string);
    }

    protected function escapeValue($value)
    {
        return htmlspecialchars($value, ENT_XML1);
    }

    protected function strlen($string)
    {
        return mb_strlen($string, '8bit');
    }

    public function getFileCount()
    {
        return $this->count;
    }

    public function openFile($file)
    {
        $this->fh = fopen($file, 'w+');
        $this->write($this->getHeaderXml());

        if (!$this->baseFileName) {
            $this->baseFileName = $file;
        }

        $this->recordCount = 0;
        $this->fileSize = $this->strlen($this->getHeaderXml()) + $this->strlen($this->getFooterXml());
    }

    public function closeFile()
    {
        $this->write($this->getFooterXml());
        fclose($this->fh);
        $this->fh = null;
    }

    protected function createFileName($index)
    {
        $pathinfo = pathinfo($this->baseFileName);

        return $pathinfo['dirname'].DIRECTORY_SEPARATOR.$pathinfo['filename'].'-'.$index.'.'.$pathinfo['extension'];
    }

    protected function createNewFile()
    {
        $this->closeFile();

        if ($this->count === 1) {
            $firstFile = $this->createFileName($this->count);
            rename($this->baseFileName, $firstFile);
        }

        $this->count++;

        $newFile = $this->createFileName($this->count);
        $this->openFile($newFile);
    }

    protected function addRecord($string)
    {
        if (
            $this->recordCount >= $this->maxRecordCount
            || ($this->strlen($string) + $this->fileSize) >= $this->maxFileSize
        ) {
            $this->createNewFile();
        }

        $this->recordCount++;
        $this->fileSize += $this->strlen($string);

        $this->write($string);
    }
}
