<?
//<title>Clinic Price</title>
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/catalog/import_setup_templ.php');

global $APPLICATION, $USER;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/catalog_import/excel_reader2.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/catalog_import/price_utils.php");

$arSetupErrors = array();

//********************  ACTIONS  **************************************//
if (($ACTION == 'IMPORT_EDIT' || $ACTION == 'IMPORT_COPY') && $STEP == 1) {
    if (array_key_exists('IBLOCK_ID', $arOldSetupVars))
        $IBLOCK_ID = $arOldSetupVars['IBLOCK_ID'];
    if (array_key_exists('URL_DATA_FILE', $arOldSetupVars))
        $URL_DATA_FILE = $arOldSetupVars['URL_DATA_FILE'];
    if (array_key_exists('DATA_FILE_NAME', $arOldSetupVars))
        $DATA_FILE_NAME = $arOldSetupVars['DATA_FILE_NAME'];
}

if ($STEP > 1) {
    if (strlen($URL_DATA_FILE) > 0 && file_exists($_SERVER["DOCUMENT_ROOT"] . $URL_DATA_FILE) && is_file($_SERVER["DOCUMENT_ROOT"] . $URL_DATA_FILE) && $APPLICATION->GetFileAccessPermission($URL_DATA_FILE) >= "R")
        $DATA_FILE_NAME = $URL_DATA_FILE;

    if (strlen($DATA_FILE_NAME) <= 0)
        $arSetupErrors[] = GetMessage("CATI_NO_DATA_FILE");

    if (empty($arSetupErrors)) {
        $IBLOCK_ID = IntVal($IBLOCK_ID);
        $arIBlock = array();
        if ($IBLOCK_ID <= 0) {
            $arSetupErrors[] = GetMessage("CATI_NO_IBLOCK");
        } else {
            $arIBlock = CIBlock::GetArrayByID($IBLOCK_ID);
            if (false === $arIBlock) {
                $arSetupErrors[] = GetMessage("CATI_NO_IBLOCK");
            }
        }
    }

    if (empty($arSetupErrors)) {
        if (!CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, 'iblock_admin_display'))
            $arSetupErrors[] = GetMessage("CATI_NO_IBLOCK_RIGHTS");
    }

    if (empty($arSetupErrors)) {
        $bIBlockIsCatalog = False;
        if (CCatalog::GetByID($IBLOCK_ID))
            $bIBlockIsCatalog = True;

        $io = CBXVirtualIo::GetInstance();

        $reader = new ImportExcelReader($io->GetPhysicalName($_SERVER["DOCUMENT_ROOT"] . $DATA_FILE_NAME), false);
        $reader->setEncoding('utf-8');
        $reader->IBLOCK_ID = $IBLOCK_ID;

        $arClinicName = array();
        $arFields = $reader->getFields(1,0);
        foreach ($arFields as $key=>$val)
        {
            if ($key<3 || empty($val))
                continue;
            $arClinicName[$key] = $val;
        }

        /*$reader->filterSections();

        $arAvailSections = array();
        $curSectionCode = false;

        for ($i = 2; $i <= $reader->rowcount(); $i++) {
            $arFields = $reader->getFields($i, 0);
            if (!$reader->IsEmpty($arFields)) {
                if ($reader->IsSection($arFields)) {
                    $curSectionCode = $arFields[1];
                    $arAvailSections[$curSectionCode] = array("NAME" => $arFields[2], "CNT" => 0);
                } elseif ($curSectionCode) {
                    //$arAvailSections[$curSectionCode]["CNT"]++;
                    $arSection = explode('.', $curSectionCode);
                    $strCode = "";
                    foreach ($arSection as $val) {
                        $strCode .= $val;
                        if (array_key_exists($strCode, $arAvailSections))
                            $arAvailSections[$strCode]["CNT"]++;
                        $strCode .= ".";
                    }
                }
            }
        }

        echo '<pre>';
        echo "TOTAL : ".count($arAvailSections)."\r\n";
        print_r($arAvailSections);
        echo '</pre>';*/
    }

    if (!empty($arSetupErrors)) {
        $STEP = 1;
    }
}

if (($ACTION == 'IMPORT_EDIT' || $ACTION == 'IMPORT_COPY') && $STEP == 2)
{
    if (array_key_exists('max_execution_time', $arOldSetupVars))
        $max_execution_time = $arOldSetupVars['max_execution_time'];
    if (array_key_exists('skip_clinics', $arOldSetupVars))
        $skip_clinics = $arOldSetupVars['skip_clinics'];
    if (array_key_exists('SETUP_PROFILE_NAME', $arOldSetupVars))
        $SETUP_PROFILE_NAME = $arOldSetupVars['SETUP_PROFILE_NAME'];
}


//********************  END ACTIONS  **********************************//

$aMenu = array(
    array(
        "TEXT" => GetMessage("CATI_ADM_RETURN_TO_LIST"),
        "TITLE" => GetMessage("CATI_ADM_RETURN_TO_LIST_TITLE"),
        "LINK" => "/bitrix/admin/cat_import_setup.php?lang=" . LANGUAGE_ID,
        "ICON" => "btn_list",
    )
);

$context = new CAdminContextMenu($aMenu);

$context->Show();

if (!empty($arSetupErrors))
    echo ShowError(implode('<br />', $arSetupErrors));
?>
<form method="POST" action="<? echo $APPLICATION->GetCurPage(); ?>" ENCTYPE="multipart/form-data" name="dataload">
    <?
    $aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("CAT_ADM_CSV_IMP_TAB1"), "ICON" => "store", "TITLE" => GetMessage("CAT_ADM_CSV_IMP_TAB1_TITLE")),
        array("DIV" => "edit2", "TAB" => GetMessage("CAT_ADM_CSV_IMP_TAB2"), "ICON" => "store", "TITLE" => GetMessage("CAT_ADM_CSV_IMP_TAB2_TITLE")),
        array("DIV" => "edit3", "TAB" => GetMessage("CAT_ADM_CSV_IMP_TAB4"), "ICON" => "store", "TITLE" => GetMessage("CAT_ADM_CSV_IMP_TAB4_TITLE")),
    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs, false, true);
    $tabControl->Begin();

    $tabControl->BeginNextTab();

    if ($STEP == 1) {
        ?>
        <tr class="heading">
        <td colspan="2"><? echo GetMessage("CATI_DATA_LOADING"); ?></td>
        </tr>
        <tr>
            <td valign="top" width="40%"><? echo GetMessage("CATI_DATA_FILE_SITE"); ?>:</td>
            <td valign="top" width="60%">
                <input type="text" name="URL_DATA_FILE" size="40"
                       value="<? echo htmlspecialcharsbx($URL_DATA_FILE); ?>">
                <input type="button" value="<? echo GetMessage("CATI_BUTTON_CHOOSE") ?>"
                       onclick="cmlBtnSelectClick();"><?
                CAdminFileDialog::ShowScript(
                    array(
                        "event" => "cmlBtnSelectClick",
                        "arResultDest" => array("FORM_NAME" => "dataload", "FORM_ELEMENT_NAME" => "URL_DATA_FILE"),
                        "arPath" => array("PATH" => "/upload/catalog", "SITE" => SITE_ID),
                        "select" => 'F',// F - file only, D - folder only, DF - files & dirs
                        "operation" => 'O',// O - open, S - save
                        "showUploadTab" => true,
                        "showAddToMenuTab" => false,
                        "fileFilter" => 'xls',
                        "allowAllFiles" => true,
                        "SaveConfig" => true
                    )
                );
                ?></td>
        </tr>
        <tr>
            <td valign="top" width="40%"><? echo GetMessage("CATI_INFOBLOCK"); ?>:</td>
            <td valign="top" width="60%"><?
                if (!isset($IBLOCK_ID))
                    $IBLOCK_ID = 0;
                echo GetIBlockDropDownListEx(
                    $IBLOCK_ID,
                    'IBLOCK_TYPE_ID',
                    'IBLOCK_ID',
                    array('CHECK_PERMISSIONS' => 'Y', 'MIN_PERMISSION' => 'W'),
                    "",
                    "",
                    'class="adm-detail-iblock-types"',
                    'class="adm-detail-iblock-list"'
                );
                ?></td>
        </tr>
    <?
    }

    $tabControl->EndTab();

    $tabControl->BeginNextTab();

    if ($STEP == 2) {
        ?>
        <tr>
            <td valign="top" width="40%"><? echo GetMessage("CATI_AUTO_STEP_TIME"); ?>:</td>
            <td valign="top" width="60%">
                <input type="text" name="max_execution_time" size="40"
                       value="<? echo intval($max_execution_time); ?>"><br>
                <small><? echo GetMessage("CATI_AUTO_STEP_TIME_NOTE"); ?></small>
            </td>
        </tr>
        <tr>
            <td valign="top" width="40%">Укажите какие клиники не импортировать:</td>
            <td valign="top" width="60%">
                <input type="hidden" name="skip_clinics[]" value="-1">
                <ul>
                <?foreach ($arClinicName as $id=>$name):?>
                    <li><input type="checkbox" name="skip_clinics[]" value="<?=$id?>"<?if (in_array($id,$skip_clinics)):?> checked="checked" <?endif?>>&nbsp;<?=htmlspecialcharsEx($name)?></li>
                <?endforeach?>
                </ul>
            </td>
        </tr>
        <?
        if ($ACTION == "IMPORT_SETUP" || $ACTION == 'IMPORT_EDIT' || $ACTION == 'IMPORT_COPY') {
            ?>
            <tr class="heading">
            <td colspan="2"><? echo GetMessage("CATI_IMPORT_SCHEME_NAME"); ?></td>
            </tr>
            <tr>
            <td valign="top" width="40%"><? echo GetMessage("CATI_IMPORT_SCHEME_NAME"); ?></td>
            <td valign="top" width="60%">
                <input type="text" name="SETUP_PROFILE_NAME" size="40"
                       value="<? echo htmlspecialcharsbx($SETUP_PROFILE_NAME); ?>">
            </td>
            </tr><?
        }
    }

    $tabControl->EndTab();

    $tabControl->BeginNextTab();

    if ($STEP == 3) {
        $FINITE = true;
    }

    $tabControl->EndTab();

    $tabControl->Buttons();

    ?><? echo bitrix_sessid_post();?><?

    if ($ACTION == 'IMPORT_EDIT' || $ACTION == 'IMPORT_COPY') {
        ?><input type="hidden" name="PROFILE_ID" value="<? echo intval($PROFILE_ID); ?>"><?
    }

    if ($STEP < 3) {
        ?><input type="hidden" name="STEP" value="<? echo intval($STEP) + 1; ?>">
        <input type="hidden" name="lang" value="<? echo LANGUAGE_ID; ?>">
        <input type="hidden" name="ACT_FILE" value="<? echo htmlspecialcharsbx($_REQUEST["ACT_FILE"]); ?>">
        <input type="hidden" name="ACTION" value="<? echo htmlspecialcharsbx($ACTION); ?>">
        <?
        if ($STEP > 1) {
            ?><input type="hidden" name="IBLOCK_ID" value="<? echo intval($IBLOCK_ID); ?>">
            <input type="hidden" name="URL_DATA_FILE" value="<? echo htmlspecialcharsbx($DATA_FILE_NAME); ?>"><?

            $arfieldsString = array(
                'IBLOCK_ID',
                'URL_DATA_FILE',
                'max_execution_time',
                'skip_clinics'
            );
            ?><input type="hidden" name="SETUP_FIELDS_LIST" value="<? echo implode(',', $arfieldsString); ?>"><?
        }
        if ($STEP > 1) {
            ?><input type="submit" name="backButton" value="&lt;&lt; <? echo GetMessage("CATI_BACK") ?>"><?
        }
        ?><input type="submit"
                 value="<? echo ($STEP == 2) ? (($ACTION == "IMPORT") ? GetMessage("CATI_NEXT_STEP_F") : GetMessage("CICML_SAVE")) : GetMessage("CATI_NEXT_STEP") . " &gt;&gt;" ?>"
                 name="submit_btn"><?
    }

    $tabControl->End();

    if (2 == $STEP) {
        echo BeginNote();
        ?><span class="required">*</span> <? echo GetMessage("CATI_OF_CAN_BUY_DESCR"); ?><?
        echo EndNote();
    }

    ?></form>
<script type="text/javascript">
    <?if ($STEP < 2):?>
    tabControl.SelectTab("edit1");
    tabControl.DisableTab("edit2");
    tabControl.DisableTab("edit3");
    <?elseif ($STEP == 2):?>
    tabControl.SelectTab("edit2");
    tabControl.DisableTab("edit1");
    tabControl.DisableTab("edit3");
    <?elseif ($STEP == 3):?>
    tabControl.SelectTab("edit3");
    tabControl.DisableTab("edit1");
    tabControl.DisableTab("edit2");
    <?endif;?>
</script>