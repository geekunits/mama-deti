<?

class ImportExcelReader extends Spreadsheet_Excel_Reader
{
    var $encoding = '';
    var $clinics = array();
    var $IBLOCK_ID = false;
    var $notSectionCode = array();
    var $LAST_ERROR = false;
    var $tmpid = false;

    function setEncoding($val)
    {
        $this->encoding = $val;
    }

    function val($row, $col, $sheet = 0)
    {
        global $APPLICATION;

        if ($this->encoding) {
            return $APPLICATION->ConvertCharset(parent::val($row, $col, $sheet), $this->encoding, SITE_CHARSET);
        } else
            return parent::val($row, $col, $sheet);
    }

    function getFields($row, $sheet, $colcount = false)
    {
        $arRes = array();

        if (!$colcount)
            $colcount = $this->colcount($sheet);

        for ($i = 1; $i <= $colcount; $i++) {

            if ($row > 1 && $i == 1) {
                $arRes[$i] = preg_replace('/\s+/', '', $this->val($row, $i, $sheet));
                if (substr($arRes[$i], -1) == '.')
                    $arRes[$i] = substr($arRes[$i], 0, -1);
            } else
                $arRes[$i] = trim(preg_replace('/\s+/', ' ', $this->val($row, $i, $sheet)));
        }

        return $arRes;
    }

    function IsSection($arFields)
    {
        if (in_array($arFields[1], $this->notSectionCode))
            return false;

        $arSection = explode(".", $arFields[1]);
        if (count($arSection) > 3)
            return false;
        foreach ($arFields as $key => $val) {
            if ($key < 3)
                continue;
            if (!empty($val))
                return false;
        }
        return true;
    }

    function IsEmpty($arFields)
    {
        if (!preg_match("#^(\d+\.?)+$#", $arFields[1]))
            return true;

        foreach ($arFields as $key => $val) {
            if (!empty($val))
                return false;
        }
        return true;
    }

    function createClinicSection($CODE, $NAME)
    {
        $arID = explode(".", $CODE);

        $bs = new CIBlockSection;

        foreach ($this->clinics as &$arClinic) {
            $strCode = '';
            $PARENT_SECTION_ID = $arClinic["ID"];
            foreach ($arID as $lvl => $ID) {

                if ($lvl>0)
                    $strCode.=".";
                $strCode .= $ID;

                if (!array_key_exists($strCode, $arClinic["SECTIONS"])) {

                    $arFilter = array(
                        "IBLOCK_ID" => $this->IBLOCK_ID,
                        "SECTION_ID" => $PARENT_SECTION_ID,
                        "=XML_ID" => $strCode,
                    );

                    $dbRes = $bs->GetList(array(), $arFilter);
                    if (!$arRes = $dbRes->Fetch()) {

                        if ($strCode !== $CODE)
                            continue;

                        $arFields = array(
                            "IBLOCK_ID" => $this->IBLOCK_ID,
                            "XML_ID" => $strCode,
                            "NAME" => $NAME,
                            "SORT" => intval($ID) * 100,
                            "TMP_ID"=>$this->tmpid,
                        );

                        $arFields["IBLOCK_SECTION_ID"] = $PARENT_SECTION_ID;

                        /*echo '<pre>';
                        print_r($arFields);
                        echo '</pre>';*/

                        if (!$ID = $bs->Add($arFields)) {
                            $this->LAST_ERROR = $bs->LAST_ERROR;
                            return false;
                        }

                        $dbRes = $bs->GetByID($ID);
                        $arRes = $dbRes->Fetch();
                    } else
                    {
                        $bs->Update($arRes["ID"],array("TMP_ID"=>$this->tmpid,"ACTIVE"=>"Y"));
                    }
                    $arClinic["SECTIONS"][$strCode] = $arRes;
                }

                $PARENT_SECTION_ID = $arClinic["SECTIONS"][$strCode]["ID"];
            }
        }

        if (isset($arClinic))
            unset ($arClinic);

        return true;
    }

    function filterSections()
    {
        $arAvailSections = array();
        $arStack = array();
        $curSectionCode = false;

        for ($i = 2; $i <= $this->rowcount(); $i++) {
            $arFields = $this->getFields($i, 0);
            if (!$this->IsEmpty($arFields)) {
                if ($this->IsSection($arFields)) {
                    $curSectionCode = $arFields[1];
                    $arAvailSections[$curSectionCode] = array("NAME" => $arFields[2], "CNT" => 0);
                } elseif ($curSectionCode) {

                    if (strpos($arFields[1], $curSectionCode) === false) {
                        end($arAvailSections);

                        $arInvalidSection = array();

                        do {
                            $key = key($arAvailSections);
                            $bFound = strpos($arFields[1], $key) === 0;
                            if ($bFound)
                                break;
                            $arInvalidSection[] = $key;
                        } while (prev($arAvailSections));
                        reset($arAvailSections);

                        if ($bFound) {
                            $curSectionCode = $key;
                            $this->notSectionCode = array_merge($arInvalidSection, $this->notSectionCode);
                            foreach ($arInvalidSection as $key)
                                unset($arAvailSections[$key]);
                        }
                    }

                    $arSection = explode('.', $curSectionCode);
                    $strCode = "";
                    foreach ($arSection as $val) {
                        $strCode .= $val;
                        if (array_key_exists($strCode, $arAvailSections))
                            $arAvailSections[$strCode]["CNT"]++;
                        $strCode .= ".";
                    }
                }
            }
        }

        foreach ($arAvailSections as $code => $arSection) {
            if ($arSection["CNT"] == 0)
                $this->notSectionCode[] = $code;
        }

        /*$arAvailSections = array();
        $curSectionCode = false;

        for ($i = 2; $i <= $reader->rowcount(); $i++) {
            $arFields = $reader->getFields($i, 0);
            if (!$reader->IsEmpty($arFields)) {
                if ($reader->IsSection($arFields)) {
                    $curSectionCode = $arFields[1];
                    $arAvailSections[$curSectionCode] = array("NAME" => $arFields[2], "CNT" => 0);
                } elseif ($curSectionCode) {
                    //$arAvailSections[$curSectionCode]["CNT"]++;
                    $arSection = explode('.', $curSectionCode);
                    $strCode = "";
                    foreach ($arSection as $val) {
                        $strCode .= $val;
                        if (array_key_exists($strCode, $arAvailSections))
                            $arAvailSections[$strCode]["CNT"]++;
                        $strCode .= ".";
                    }
                }
            }
        }*/
    }
}

?>