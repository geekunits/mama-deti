<?php
//<title>Clinic Price</title>
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/catalog/import_setup_templ.php');

function p($arg, $exit = true){
    echo '<pre>' . print_r($arg, true) . '</pre>';

    if($exit){
        die();
    }
}

if(!$CUR_FILE_POS){ //выполняем 1 раз
    COption::SetOptionString("catalog", "import.date", date('Y-m-d H:i:s')); //пишем текущую дату начала импорта
}


$startImportExecTime = getmicrotime();

global $USER;
global $APPLICATION;
global $DB;

$bTmpUserCreated = false;
if (!CCatalog::IsUserExists()) {
    $bTmpUserCreated = true;
    if (isset($USER)) {
        $USER_TMP = $USER;
        unset($USER);
    }

    $USER = new CUser();
}

$strImportErrorMessage = "";
$strImportOKMessage = "";

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/catalog_import/excel_reader2.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/catalog_import/price_utils.php");

$max_execution_time = intval($max_execution_time);
if ($max_execution_time <= 0)
    $max_execution_time = 0;
if (defined('BX_CAT_CRON') && true == BX_CAT_CRON)
    $max_execution_time = 0;

if (defined("CATALOG_LOAD_NO_STEP") && CATALOG_LOAD_NO_STEP)
    $max_execution_time = 0;

$bAllLinesLoaded = true;

$io = CBXVirtualIo::GetInstance();

if (!function_exists('CSVCheckTimeout')) {
    function CSVCheckTimeout($max_execution_time)
    {
        return ($max_execution_time <= 0) || (getmicrotime() - START_EXEC_TIME <= $max_execution_time);
    }
}

$DATA_FILE_NAME = "";

if (strlen($URL_DATA_FILE) > 0) {
    $URL_DATA_FILE = Rel2Abs("/", $URL_DATA_FILE);
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . $URL_DATA_FILE) && is_file($_SERVER["DOCUMENT_ROOT"] . $URL_DATA_FILE))
        $DATA_FILE_NAME = $URL_DATA_FILE;
}

if (strlen($DATA_FILE_NAME) <= 0)
    $strImportErrorMessage .= GetMessage("CATI_NO_DATA_FILE") . "<br>";

$IBLOCK_ID = intval($IBLOCK_ID);
if ($IBLOCK_ID <= 0) {
    $strImportErrorMessage .= GetMessage("CATI_NO_IBLOCK") . "<br>";
} else {
    $arIBlock = CIBlock::GetArrayByID($IBLOCK_ID);
    if (false === $arIBlock) {
        $strImportErrorMessage .= GetMessage("CATI_NO_IBLOCK") . "<br>";
    }
}

if ('' == $strImportErrorMessage) {
    $bWorkflow = CModule::IncludeModule("workflow") && ($arIBlock["WORKFLOW"] != "N");

    $bIBlockIsCatalog = false;
    $rsCatalogs = CCatalog::GetList(
        array(),
        array('IBLOCK_ID' => $IBLOCK_ID),
        false,
        false,
        array('IBLOCK_ID')
    );
    if ($arCatalog = $rsCatalogs->Fetch()) {
        $bIBlockIsCatalog = true;
    }

    $reader = new ImportExcelReader($io->GetPhysicalName($_SERVER["DOCUMENT_ROOT"] . $DATA_FILE_NAME), false);
    $reader->setEncoding('utf-8');
    $reader->IBLOCK_ID = $IBLOCK_ID;
}

$CUR_SHEET = 0;

if ('' == $strImportErrorMessage) {

    $bs = new CIBlockSection();
    $el = new CIBlockElement();

    if ($CUR_FILE_POS < 2) {
        $tmpid = md5(uniqid(""));
        $CUR_FILE_POS = 2;
        $reader->filterSections();
    }

    if (!isset($notSectionCode) || !is_array($notSectionCode)) {
        $reader->filterSections();
        $notSectionCode = $reader->notSectionCode;
    } else
        $reader->notSectionCode = $notSectionCode;

    $reader->tmpid = $tmpid;

    $arFields = $reader->getFields(1, $CUR_SHEET);
    foreach ($arFields as $key => $val) {
        if ($key < 3 || empty($val) || in_array($key,$skip_clinics))
            continue;

        $dbRes = $bs->GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID, "NAME" => $val, "DEPTH_LEVEL" => 1));
        if (!$arRes = $dbRes->Fetch()) {
            if (!$ID = $bs->Add(array("IBLOCK_ID" => $IBLOCK_ID, "NAME" => $val, "TMP_ID"=>$tmpid, "ACTIVE" => "N"))) {
                $strErrorR .= "Ошибка добавления клиники: ".$bs->LAST_ERROR . "<br>";
            }
            $dbRes = $bs->GetByID($ID);
            $arRes = $dbRes->Fetch();
        } else {
            if ($bs->Update($arRes["ID"],array("TMP_ID"=>$tmpid/*,"ACTIVE"=>"Y"*/)))
                $strErrorR .= "Ошибка обновления клиники: ".$bs->LAST_ERROR . "<br>";
        }
        $reader->clinics[$key] = $arRes;
    }

    /*echo '<pre>';
    print_r($reader->getFields(1,$CUR_SHEET));
    print_r($reader->getFields(2,$CUR_SHEET));
    print_r($reader->clinics);
    echo '</pre>';
    die();*/

    if (isset($curSectionCode) && isset($curSectionName)) {
        if (!$reader->createClinicSection($curSectionCode, $curSectionName)) {
            $strImportErrorMessage.="Ошибка создания разделов в клиниках ".$reader->LAST_ERROR."<br>";
        }
    }

    $procedureManager = ProcedureManager::getInstance();

    while ($CUR_FILE_POS <= $reader->rowcount($CUR_SHEET)) {

        $arFields = $reader->getFields($CUR_FILE_POS, $CUR_SHEET);

        if (!empty($arFields[2]) && !$reader->IsEmpty($arFields)) { //Название
            $codeERU = trim($arFields[1], " \t\n\r\0\x0B");

            if (!$reader->IsSection($arFields)) {
                $curElementSort+=100;

                $procedureItem = $procedureManager->addItem($codeERU, $arFields[2]);

                foreach ($arFields as $key=>$val) {
                    if ($key<3)
                        continue;

                    $val = preg_replace('/[^\d+]/','',$val);

                    if (array_key_exists($key, $reader->clinics) && array_key_exists($curSectionCode,$reader->clinics[$key]["SECTIONS"])) {
                        $SECTION_ID = $reader->clinics[$key]["SECTIONS"][$curSectionCode]["ID"];
                        $rsElement = $el->GetList(array(),array("IBLOCK_ID"=>$IBLOCK_ID,"XML_ID"=>$codeERU,"SECTION_ID"=>$SECTION_ID),false,false,array("ID"));
                        if ($arElement = $rsElement->Fetch()) {
                            $arUpdateProduct = array(
                                "SORT"=>$curElementSort,
                                "TMP_ID"=>$tmpid,
                                "NAME"=>$arFields[2],
                                "ACTIVE"=>"Y",
                                "PROPERTY_VALUES" => array("PRICE"=>$val,'PROCEDURE'=>$procedureItem['ID'])
                            );
                            if (!$el->Update($arElement["ID"],$arUpdateProduct))
                                $strImportErrorMessage.="Ошибка обновления элемента: ".$el->LAST_ERROR."<br>";
                            $el->SetPropertyValuesEx($arElement["ID"],$IBLOCK_ID,array("PRICE"=>$val));
                        } else {
                            $arLoadProduct = array(
                                "IBLOCK_ID"=>$IBLOCK_ID,
                                "SORT"=>$curElementSort,
                                "TMP_ID"=>$tmpid,
                                "NAME"=>$arFields[2],
                                "IBLOCK_SECTION_ID" => $SECTION_ID,
                                "XML_ID" => $codeERU,
                                "PROPERTY_VALUES" => array("PRICE"=>$val,'PROCEDURE'=>$procedureItem['ID'])
                            );
                            if (!$el->Add($arLoadProduct))
                                $strImportErrorMessage.="Ошибка добавления элемента: ".$el->LAST_ERROR."<br>";
                        }
                    }
                }

            } else {
                $curSectionCode = $codeERU;
                $curSectionName = $arFields[2];
                $curElementSort = 0;

                if (!$reader->createClinicSection($curSectionCode, $curSectionName)) {
                    $strImportErrorMessage.="Ошибка добавления разделов в клиниках: ".$reader->LAST_ERROR."<br>";
                }

                $procedureManager->addGroup($curSectionCode, $curSectionName);
            }
        }

        $CUR_FILE_POS++;
        if (!($bAllLinesLoaded = CSVCheckTimeout($max_execution_time))) break;
    }

    if (!$bAllLinesLoaded) {
        $bAllDataLoaded = false;

        $INTERNAL_VARS_LIST = "tmpid,curSectionCode,curSectionName,curElementSort,notSectionCode,line_num,correct_lines,error_lines,killed_lines,arIBlockProperty,bThereIsGroups,arProductGroups,arIBlockPropertyValue,bDeactivationStarted,bUpdatePrice";
        $SETUP_VARS_LIST = "IBLOCK_ID,URL_DATA_FILE,max_execution_time,skip_clinics";
    }

    if($bAllDataLoaded){
        if(($importDate = COption::GetOptionString("catalog", "import.date")) !== false){
            /*
             * Нужно удалить все разделы и элементы у которых дата обновления раньше, чем запустили импорт
             * и только те разделы и элементы, клиники, которых не отмечены (те которые нужно загрузить)
             * */
            $arClinicIDs = array();

            foreach($reader->clinics AS $clinic){
                $arClinicIDs[] = $clinic['ID'];
            }

            $arClinicSubSections = array();

            if(count($arClinicIDs)){
                $dbRes = CIBlockSection::GetList(
                    array(),
                    array(
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "ID" => $arClinicIDs,
                        "DEPTH_LEVEL" => 1
                    )
                );

                //получим список всех ID разделов, которые надо удалить, если их не загружали
                while($arClinic = $dbRes->Fetch()) {
                    $resSections = $DB->Query('SELECT ID 
                                               FROM b_iblock_section 
                                               WHERE LEFT_MARGIN>' . $arClinic['LEFT_MARGIN'] . ' 
                                               AND RIGHT_MARGIN<' . $arClinic['RIGHT_MARGIN'] . ' 
                                               AND IBLOCK_ID=' . $IBLOCK_ID);

                    while($arCheckSection = $resSections->Fetch()){
                        $arClinicSubSections[] = $arCheckSection['ID'];
                    }
                }
            }

            if(count($arClinicSubSections)){
                //удаляем свойства элементов
                $deletePropertiesSql = 'DELETE FROM b_iblock_element_property 
                                    WHERE IBLOCK_ELEMENT_ID IN(SELECT ID 
                                                               FROM b_iblock_element 
                                                               WHERE IBLOCK_ID=' . $IBLOCK_ID . ' 
                                                               AND TIMESTAMP_X<"' . $importDate . '"
                                                               AND IBLOCK_SECTION_ID IN(' . implode(', ', $arClinicSubSections) . ')
                                                               )';

                $DB->Query($deletePropertiesSql);

                //удаляем привязки элементов к разделам
                $deleteSectionsSql = 'DELETE FROM b_iblock_section_element
                                  WHERE IBLOCK_ELEMENT_ID IN(SELECT ID FROM b_iblock_element
                                                             WHERE IBLOCK_ID=' . $IBLOCK_ID . ' 
                                                             AND IBLOCK_SECTION_ID IN(' . implode(', ', $arClinicSubSections) . ') 
                                                             AND TIMESTAMP_X<"' . $importDate . '")';

                $DB->Query($deleteSectionsSql);

                //удаляем элементы
                $deleteElementsSql = 'DELETE FROM b_iblock_element 
                                  WHERE IBLOCK_ID=' . $IBLOCK_ID . ' 
                                  AND TIMESTAMP_X<"' . $importDate . '"
                                  AND IBLOCK_SECTION_ID IN(' . implode(', ', $arClinicSubSections) . ')';

                $DB->Query($deleteElementsSql);

                //удаляем разделы
                $deleteSectionsSql = 'DELETE FROM b_iblock_section
                                  WHERE IBLOCK_ID=' . $IBLOCK_ID . ' 
                                  AND ID IN(' . implode(', ', $arClinicSubSections) . ') 
                                  AND TIMESTAMP_X<"' . $importDate . '"';

                $DB->Query($deleteSectionsSql);
            }
        }
    }
}

if ($bTmpUserCreated) {
    unset($USER);
    if (isset($USER_TMP)) {
        $USER = $USER_TMP;
        unset($USER_TMP);
    }
}
