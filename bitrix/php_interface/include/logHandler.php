<?php
/* логирование изменений, добавлений, удалений в инфоблоках */
class LogHandler
{
    const LOGGING_IBLOCK_ID = 50;
    protected $arElementData = array();
    protected $arSectionData = array();

    public static function attachEvents() {
        $logging = new static;

        AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array($logging, 'onAfterIBlockElementAddLog'));
        AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array($logging, 'onAfterIBlockElementUpdateLog'));
        AddEventHandler('iblock', 'OnAfterIBlockElementDelete', array($logging, 'onAfterIBlockElementDeleteLog'));
        AddEventHandler('iblock', 'OnBeforeIBlockElementDelete', array($logging, 'onBeforeIBlockElementDeleteLog'));

        AddEventHandler('iblock', 'OnAfterIBlockSectionAdd', array($logging, 'onAfterIBlockSectionAddLog'));
        AddEventHandler('iblock', 'OnAfterIBlockSectionUpdate', array($logging, 'onAfterIBlockSectionUpdateLog'));
        AddEventHandler('iblock', 'OnAfterIBlockSectionDelete', array($logging, 'onAfterIBlockSectionDeleteLog'));
        AddEventHandler('iblock', 'OnBeforeIBlockSectionDelete', array($logging, 'onBeforeIBlockSectionDeleteLog'));
    }

    /* логируются не все инфоблоки, только указанные - константы объявлены в файле init.php */
    protected function checkIblock($id){
        return in_array($id, array(SERVICE_IBLOCK_ID, DOCTOR_IBLOCK_ID, SERVICE_DESCRIPTION_IBLOCK_ID, PROGRAM_IBLOCK_ID));
    }

    protected function getElementData($ID){
        $resElement = CIBlockElement::GetList(
            array(),
            array(
                "IBLOCK_ID" => (int)CIBlockElement::GetIBlockByID($ID),
                "ID"        => $ID
            )
        );

        if($obElement = $resElement->GetNextElement()) {
            $arElement = $obElement->GetFields();
            $arElement['PROPERTIES'] = $obElement->GetProperties();

            return $arElement;
        }

        return false;
    }

    /* логирование элемента инфоблока */
    public function elementLog($arFields, $type) {
        if(self::checkIblock($arFields["IBLOCK_ID"])) {
            $property_string = "<b>Название</b>:<br/>\r\n" . $arFields['NAME'] . "<br/><br/>\r\n\r\n";
            $property_string.= "****************************************************************************<br/>\r\n";
            $property_string.= "<b>Активность</b>:<br/>\r\n" . $arFields['ACTIVE'] . "<br/><br/>\r\n\r\n";

            if ($arFields['PREVIEW_TEXT'] != '') {
                $property_string.= "****************************************************************************<br/>\r\n";
                $property_string.= "<b>Описание для анонса</b>:<br/>\r\n" . $arFields['PREVIEW_TEXT'] . "<br/><br/>\r\n\r\n";
            }

            if ($arFields['DETAIL_TEXT'] != '') {
                $property_string.= "****************************************************************************<br/>\r\n";
                $property_string.= "<b>Детальное описание</b>:<br/>\r\n" . $arFields['DETAIL_TEXT'] . "<br/><br/>\r\n\r\n";
            }

            foreach ($arFields['PROPERTIES'] as $arProperty) {
                $str = '';

                switch ($arProperty['PROPERTY_TYPE']) {
                    case "S":
                        $value = $arProperty['VALUE'];

                        switch ($arProperty['USER_TYPE']) {
                            case "HTML":
                                $value = $arProperty['VALUE']['TEXT'];
                                break;
                        }

                        if($arProperty['MULTIPLE'] != 'Y') {
                            $value = array($value);
                        }

                        foreach ($value as $v) {
                            if ($v) {
                                $str.= "<br/>\r\n" . $v;
                            }
                        }

                        break;

                    case "L":
                        $value = $arProperty['VALUE_ENUM'];

                        if($arProperty['MULTIPLE'] != 'Y'){
                            $value = array($value);
                        }

                        foreach ($value as $v) {
                            if ($v) {
                                $str.= "<br/>\r\n" . $v;
                            }
                        }

                        break;

                    case "E":
                        $value = $arProperty['VALUE'];

                        if($arProperty['MULTIPLE'] != 'Y'){
                            $value = array($value);
                        }

                        foreach ($value as $linkElementID) {
                            if ($linkElementID) {
                                $res = CIBlockElement::GetList(
                                    array(),
                                    array(
                                        "IBLOCK_ID" => $arProperty['LINK_IBLOCK_ID'],
                                        "ID"        => $linkElementID
                                    ),
                                    false,
                                    false,
                                    array("NAME")
                                );

                                while($arElement = $res->GetNext()){
                                    $str.= "<br/>\r\n" . $arElement['NAME'];
                                }
                            }
                        }

                        break;

                    case "G":
                        $value = $arProperty['VALUE'];

                        if($arProperty['MULTIPLE'] != 'Y'){
                            $value = array($value);
                        }

                        foreach ($value as $linkSectionID) {
                            if ($linkSectionID) {
                                $res = CIBlockSection::GetList(
                                    array(),
                                    array(
                                        "IBLOCK_ID" => $arProperty['LINK_IBLOCK_ID'],
                                        "ID"        => $linkSectionID
                                    ),
                                    false,
                                    array("NAME"),
                                    false
                                );

                                while($arSection = $res->GetNext()) {
                                    $str.= "<br/>\r\n" . $arSection['NAME'];
                                }
                            }
                        }

                        break;

                    case "N":
                        $value = $arProperty['VALUE'];

                        if($arProperty['MULTIPLE'] != 'Y'){
                            $value = array($value);
                        }

                        foreach ($value as $v) {
                            switch ($arProperty['USER_TYPE']) {
                                case "Melcosoft_IntCheckBox":
                                    $v = $v == 1 ? 'да' : 'нет';

                                    break;
                            }

                            $str.= "<br/>\r\n" . $v;
                        }

                        break;
                }

                if ($str) {
                    $property_string.= "****************************************************************************<br/>\r\n";
                    $property_string.= "<b>" . $arProperty['NAME'] . "</b>:" . $str . "<br/><br/>\r\n\r\n";
                }
            }

            $PROP = array();
            $PROP['DATE_CHANGED'] = date("d.m.Y H:i:s");
            $PROP['AUTHOR_CHANGED'] = $arFields['MODIFIED_BY'];
            $PROP['IB_CHANGED'] = $arFields['IBLOCK_ID'];
            $PROP['TYPE_CHANGED'] = $type;
            $PROP['ID_CHANGED'] = $arFields['ID'];
            $PROP['ID_SECTION_CHANGED'] = $arFields['IBLOCK_SECTION_ID'];

            $property_string = str_replace('  ', '', $property_string);

            $arLoadProductArray = Array(
                "MODIFIED_BY"       => $arFields['MODIFIED_BY'],
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "IBLOCK_ID"         => self::LOGGING_IBLOCK_ID,
                "PROPERTY_VALUES"   => $PROP,
                "NAME"              => $arFields['~NAME'],
                "ACTIVE"            => "Y",
                "DETAIL_TEXT"       => $property_string,
            );

            $el = new CIBlockElement;
            $el->Add($arLoadProductArray);
        }
    }

    public function onAfterIBlockElementAddLog($arFields) {
        if($arElementData = $this->getElementData($arFields['ID'])){
            $this->elementLog($arElementData, 'добавление элемента');
        }
    }

    public function onAfterIBlockElementUpdateLog($arFields) {
        if($arElementData = $this->getElementData($arFields['ID'])) {
            $this->elementLog($arElementData, 'изменение элемента');
        }
    }

    public function onBeforeIBlockElementDeleteLog($ID) {
        $this->arElementData = $this->getElementData($ID);
    }

    public function onAfterIBlockElementDeleteLog() {
        if($this->arElementData){
            global $USER;
            $this->arElementData['MODIFIED_BY'] = $USER->GetID();
            $this->elementLog($this->arElementData, 'удаление элемента');
        }
    }






    protected function getSectionData($ID){

        $rsSections = CIBlockSection::GetList(array(), array("ID"=>(int)$ID), true, array("ID", "IBLOCK_ID"));
        $arSction = $rsSections->GetNext();

        if(self::checkIblock($arSction['IBLOCK_ID'])) {

            $arFilter = array("IBLOCK_ID"=>$arSction['IBLOCK_ID'], "ID"=>$ID);
            $arSelect = array(
                "ID",
                "ACTIVE",
                "MODIFIED_BY",
                "IBLOCK_ID",
                "NAME",
                "DESCRIPTION",
                "UF_*",
            );

            /* тут двойной гетлист т.к. чтобы вытянуть пользовательсткие св-ва UF_* нужно обязательно указывать инфоблок */
            $rsSections = CIBlockSection::GetList(array(), $arFilter, true, $arSelect);
            if($arFields = $rsSections->GetNext()) {
                return $arFields;
            }
        }

        return false;
    }

    /* логирование раздела(секции) инфоблока */
    public function sectionLog($arFields, $type) {

        if(self::checkIblock($arFields["IBLOCK_ID"])) {

            $property_string = "<b>Название</b>:<br/>\r\n" . $arFields['NAME'] . "<br/><br/>\r\n\r\n";
            $property_string.= "****************************************************************************<br/>\r\n";
            $property_string.= "<b>Активность</b>:<br/>\r\n" . $arFields['ACTIVE'] . "<br/><br/>\r\n\r\n";

            if ($arFields['DESCRIPTION'] != '') {
                $property_string.= "****************************************************************************<br/>\r\n";
                $property_string.= "<b>Описание</b>:<br/>\r\n" . $arFields['DESCRIPTION'] . "<br/><br/>\r\n\r\n";
            }

            foreach ($arFields as $property_id => $property_val) {
                if (strpos($property_id, "UF_") === 0){
                    $arFilter = array('FIELD_NAME' => $property_id);
                    $rsData = CUserTypeEntity::GetList(array(), $arFilter);
                    $arRes = $rsData->Fetch();

                    $ar_res = CUserTypeEntity::GetByID( $arRes['ID'] );

                    $str = '';

                    switch ($ar_res['USER_TYPE_ID']) {
                        case "usercheckbox":
                        case "boolean":                        
                            $value = $property_val == '1' ? 'да' : 'нет';
                            $str.= "<br/>\r\n" . $value;
                            break;

                        case "ms_user_type_eautocomplete":
                        case "iblock_element":
                            $value = $property_val;

                            if($ar_res['MULTIPLE'] != 'Y'){
                                $value = array($value);
                            }

                            foreach ($value as $linkElementID) {
                                if ($linkElementID != '') {
                                    $res = CIBlockElement::GetList(
                                        array(), 
                                        array(
                                            'IBLOCK_ID' => $ar_res['SETTINGS']['IBLOCK_ID'],
                                            'ID'        => $linkElementID
                                        ),
                                        false,
                                        false,
                                        array('NAME')
                                    );
                                    
                                    while($arElement = $res->GetNext()) {
                                        $str.= "<br/>\r\n" . $arElement['NAME'];
                                    }
                                }
                            }
                            break;

                        case "iblock_section":

                            $value = $property_val;

                            if($ar_res['MULTIPLE'] != 'Y'){
                                $value = array($value);
                            }

                            foreach ($value as $linkSectionID) {
                                if ($linkSectionID) {
                                    $res = CIBlockSection::GetList(
                                        array(),
                                        array(
                                            "IBLOCK_ID" => $ar_res['SETTINGS']['IBLOCK_ID'],
                                            "ID" => $linkSectionID
                                        ),
                                        false,
                                        array("NAME"),
                                        false
                                    );

                                    while($arSection = $res->GetNext()) {
                                        $str.= "<br/>\r\n" . $arSection['NAME'];
                                    }
                                }
                            }
                            break;

                        case "string":
                            $value = $property_val;

                            if($ar_res['MULTIPLE'] != 'Y') {
                                $value = array($value);
                            }

                            foreach ($value as $v) {
                                if ($v) {
                                    $str.= "<br/>\r\n" . $v;
                                }
                            }
                            break;

                        default:
                    }

                    if ($str) {
                        $property_string.= "****************************************************************************<br/>\r\n";
                        $property_string.= "<b>" . $ar_res['EDIT_FORM_LABEL']['ru'] . "</b>:" . $str . "<br/><br/>\r\n\r\n";
                    }
                }
            }

            $PROP = array();
            $PROP['DATE_CHANGED'] = date("d.m.Y H:i:s");
            $PROP['AUTHOR_CHANGED'] = $arFields['MODIFIED_BY'];
            $PROP['IB_CHANGED'] = $arFields['IBLOCK_ID'];
            $PROP['TYPE_CHANGED'] = $type;
            $PROP['ID_SECTION_CHANGED'] = $arFields['ID'];

            $property_string = str_replace('  ', '', $property_string);
            $arLoadProductArray = Array(
                "MODIFIED_BY"       => $arFields['MODIFIED_BY'],
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "IBLOCK_ID"         => self::LOGGING_IBLOCK_ID,
                "PROPERTY_VALUES"   => $PROP,
                "NAME"              => $arFields['~NAME'],
                "ACTIVE"            => "Y",
                "DETAIL_TEXT"       => $property_string,
            );

            $el = new CIBlockElement;
            $el->Add($arLoadProductArray);
        }
    }

    public function onAfterIBlockSectionAddLog($arFields) {
        if($arSectionData = $this->getSectionData($arFields['ID'])) {
            $this->sectionLog($arSectionData, 'добавление секции');
        }        
    }

    public function onAfterIBlockSectionUpdateLog($arFields) {
        if($arSectionData = $this->getSectionData($arFields['ID'])) {
            $this->sectionLog($arSectionData, 'изменение секции');
        }
    }

    public function onBeforeIBlockSectionDeleteLog($ID) {
        $this->arSectionData[$ID] = $this->getSectionData($ID);
    }

    public function onAfterIBlockSectionDeleteLog($arFieldsSect) {
        if($this->arSectionData){
            global $USER;
            $this->arSectionData['MODIFIED_BY'] = $USER->GetID();
            $this->sectionLog($this->arSectionData[$arFieldsSect["ID"]], 'удаление секции');
        }        
    }
}