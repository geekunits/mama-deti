<?php

class SkypeConsultation {

	protected $raw;
	protected $schedule;
	protected $doctorRaw;

	public function __construct(array $raw) {
		$this->raw = $raw;
		$this->schedule = new Schedule($raw['PROPERTY_SCHEDULE_VALUE']);
	}

	public function isValidNow() {
		$days = $this->getSchedule()->getDays();
		$dtNow = new DateTime('NOW');
		$nowIndex = intval($dtNow->format('N')) - 1;
		$nowDay = $days[$nowIndex];
		if (!$nowDay->hasSchedule) {
			return false;
		}
		$dtBeginTime = $nowDay->beginTime;
		$dtEndTime = $nowDay->endTime;
		if ($dtBeginTime->getTimestamp() > $dtNow->getTimestamp()) {
			return false;
		}
		if ($dtEndTime->getTimestamp() < $dtNow->getTimestamp()) {
			return false;
		}
		return true;
	}

	public function getSchedule() {
		return $this->schedule;
	}

	public function getRaw() {
		return $this->raw;
	}

	public function getDoctorRaw() {
		if (is_null($this->doctorRaw) && !empty($this->raw['PROPERTY_DOCTOR_VALUE'])) {
			$dbResult = CIBlockElement::GetById($this->raw['PROPERTY_DOCTOR_VALUE']);
			$dbElement = $dbResult->GetNextElement();
			if (!empty($dbElement)) {
				$this->doctorRaw = $dbElement->GetFields();
				$this->doctorRaw['PROPERTIES'] = $dbElement->GetProperties();
			} 
		}
		return $this->doctorRaw;
	}

	public function getNumber() {
		return trim($this->raw['PROPERTY_SKYPE_VALUE']);
	}

	public function getText() {
		return trim($this->raw['DETAIL_TEXT']);
	}

}