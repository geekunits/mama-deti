<?php

class SkypeConsultationFinder {

	public static function find(array $arFilter = array()) {
		$arFilter['IBLOCK_ID'] = SKYPE_CONSULTATION_IBLOCK_ID;
		$arFilter['ACTIVE'] = 'Y';
		$arSelect = [
			'ID',
			'IBLOCK_ID',
			'NAME',
			'PREVIEW_TEXT',
			'DETAIL_TEXT',
			'PROPERTY_DOCTOR',
			'PROPERTY_SERVICE_SECTIONS',
			'PROPERTY_SHOW_CHILD_SERVICES',
			'PROPERTY_SERVICES',
			'PROPERTY_SCHEDULE',
			'PROPERTY_SKYPE',
			'PROPERTY_DOCTOR.PROPERTY_CITY',
			'PROPERTY_DOCTOR.PROPERTY_CLINIC',
		];

		$dbResult = CIBlockElement::GetList(['ID' => 'ASC'], $arFilter, false, false, $arSelect);
		$raws = [];
		while($arItem = $dbResult->GetNext()) {
			$itemId = $arItem['ID'];
			if (!isset($raws[$itemId])) {
				$arItem['SECTION_IDS'] = [];
				$arItem['SERVICE_IDS'] = [];
				$raws[$itemId] = $arItem;
			}
			$raws[$itemId]['SECTION_IDS'][$arItem['PROPERTY_SERVICE_SECTIONS_VALUE']] = $arItem['PROPERTY_SERVICE_SECTIONS_VALUE'];
			$raws[$itemId]['SERVICE_IDS'][$arItem['PROPERTY_SERVICES_VALUE']] = $arItem['PROPERTY_SERVICES_VALUE'];
		}
		$skypeConsultations = [];
		foreach($raws as $raw) {
			$skypeConsultations[] = new SkypeConsultation($raw);
		}
		return $skypeConsultations;
	}

	public static function findByClinicId($cityId, $clinicId) {
		$doctorIds = self::getDoctorIds($cityId, $clinicId);
		$arFilter = [
			'PROPERTY_DOCTOR' => $doctorIds,
		];
		return self::find($arFilter);
	}

	public static function findByServiceId($cityId, $serviceId, $clinicId = null) {
		$doctorIds = self::getDoctorIds($cityId);
		$arFilter = [
			'PROPERTY_DOCTOR' => $doctorIds,
			'PROPERTY_SERVICES' => $serviceId,
		];
		if (empty($clinicId)) {
			$clinicId = false;
		}
		$arFilter['PROPERTY_CLINICS'] = $clinicId;

		return self::find($arFilter);

	}

	public static function findByServiceSectionId($cityId, $sectionId, $clinicId = null) {
		$doctorIds = self::getDoctorIds($cityId);
		$arFilter = [
			'PROPERTY_DOCTOR' => $doctorIds,
			'PROPERTY_SERVICE_SECTIONS' => $sectionId,
		];
		if (empty($clinicId)) {
			$clinicId = false;
		}
		$arFilter['PROPERTY_CLINICS'] = $clinicId;
			
		return self::find($arFilter);
	}


	protected static function getDoctorIds($cityId) {
		$arFilter = [
			'PROPERTY_CITY' => $cityId,
		];
		return CMamaDetiAPI::getDoctorsID($arFilter);
	}

}