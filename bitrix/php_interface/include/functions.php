<?php

use \GeoIp2\Exception\AddressNotFoundException;
use \GeoIp2\Database\Reader as GeoIpReader;

function changeCaseFirst($string, $case)
{
    if (empty($string)) {
        return $string;
    }

    return mb_convert_case(mb_substr($string, 0, 1), $case).mb_substr($string, 1);
}

function upperCaseFirst($string)
{
    return changeCaseFirst($string, MB_CASE_UPPER);
}

function lowerCaseFirst($string)
{
    return changeCaseFirst($string, MB_CASE_LOWER);
}

function plural($number, array $forms)
{
    $number = abs($number % 100);
    if ($number > 10 && $number < 20) {
        return $forms[2];
    }

    $number = $number % 10;

    return $forms[(int) ($number !== 1) + (int) ($number >= 5 || !$number)];
}

function formatCurrencyAmount($amount, $short = false)
{
    return number_format($amount, 0, '.', ' ').($short ? ' р.' : ' руб.');
}

function sortByCity($item1, $item2)
{
    return strcmp($item1['CITY'], $item2['CITY']);
}

function detectIP() {
    $ip = null;
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        list($ip) = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function getGeoDataByIP($ip = null)
{//46.147.40.43
    //5.254.65.111
    if($ip == null){
        $ip = detectIP();
    }
    if (is_null($ip)) {
        return null;
    }
    // Этот адрес не найден в базе.
    // $ip = '10.207.99.22';

    $reader = new GeoIpReader(__DIR__ . '/geomaxmind/GeoLite2-Country.mmdb');
    try {
        $country =  $reader->country($ip);
    } catch (AddressNotFoundException $e) {
        $country = null;
    }
    return $country;
}

function detectCityIdByIP()
{
    $detectedCityId = null;
    if (CModule::IncludeModule('msgeoip')) {
        if ($FAKE_IP) {
            $REMOTE_IP = $FAKE_IP;
        } else {
            $REMOTE_IP =
                !empty($_SERVER['HTTP_X_FORWARDED_FOR'])
                    ? reset(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']))
                    : $_SERVER['REMOTE_ADDR'];
        }
        //$REMOTE_IP = '2.95.34.0';

        $rsCityBlock = CMSGEOIPCityBlocks::GetByIP($REMOTE_IP);
        if ($arCityBlock = $rsCityBlock->Fetch()) {
            if ($arCityBlock['LATITUDE'] && $arCityBlock['LONGITUDE']) {
                $COORD = $arCityBlock['LATITUDE'].','.$arCityBlock['LONGITUDE'];
                $BEST_DIST = false;
                $BEST_CITY = false;
                foreach ($arCityList as $arCity) {
                    $DIST = GetDistanceForCoord($arCity['PROPERTIES']['COORD']['VALUE'], $COORD);
        //AddMessage2Log(print_r($arCity,true));
                    if ($BEST_DIST === false || $BEST_DIST > $DIST) {
                        $BEST_DIST = $DIST;
                        $BEST_CITY = $arCity['ID'];
                    }
                }

                $detectedCityId = $BEST_CITY;
            }
        }
    }

    return $detectedCityId;
}

function CityDetectionSuperNew()
{
    global $APPLICATION;

    $cm = CityManager::getInstance();
    $cityId = $cm->getCityIdByDomain();

    $GLOBALS['CITY_LIST'] = $cm->getCities();
    $GLOBALS['CURRENT_CITY'] = $cm->getCityById($cityId);
}

function CityDetectionNew()
{
    global $APPLICATION;

    $cm = CityManager::getInstance();

    $savedCityId = $cm->getSavedCityId();
    $domainCityId = $cm->getCityIdByDomain();

    $cityId = null;

    if (!$savedCityId) {
        $detectedCityId = detectCityIdByIP();

        if (!$detectedCityId) {
            $detectedCityId = $cm->getDefaultCity()['ID'];
        }

        if ($domainCityId != $detectedCityId) {
            LocalRedirect('http://'.$cm->getDomainByCityId($detectedCityId).$APPLICATION->GetCurUri());

            return;
        }

        $cityId = $domainCityId;
    } else {
        if ($savedCityId != $domainCityId) {
            LocalRedirect('http://'.$cm->getDomainByCityId($savedCityId).$APPLICATION->GetCurUri());

            return;
        }

        $cityId = $savedCityId;
    }

    $GLOBALS['CITY_LIST'] = $cm->getCities();
    $GLOBALS['CURRENT_CITY'] = $cm->getCityById($cityId);
}

function CityDetectionOld()
{
    global $APPLICATION;

    $cache_dir = '/';

    $obCache = new CPHPCache();
    if ($obCache->InitCache(60 * 60 * 24, 'med_city', $cache_dir)) {
        $vars = $obCache->GetVars();
        $arCityList = $vars['CITY_LIST'];
    } else {
        $obCache->StartDataCache();

        CModule::IncludeModule('iblock');

        $IBLOCK_ID = 7;

        $arCityList = array();

        $rsElement = CIBlockElement::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y'), false, false, array('IBLOCK_ID', 'ID', 'NAME', 'PROPERTY_*'));
        while ($obElement = $rsElement->GetNextElement()) {
            $arFields = $obElement->GetFields();
            $arProps = $obElement->GetProperties();

            $arCity = array(
                'ID' => $arFields['ID'],
                'NAME' => $arFields['NAME'],
                '~NAME' => $arFields['~NAME'],
            );

            $arCity['PROPERTIES'] = $arProps;

            $arCityList[$arCity['ID']] = $arCity;
        }

        if (defined('BX_COMP_MANAGED_CACHE')) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache($cache_dir);
            $CACHE_MANAGER->RegisterTag('iblock_id_'.$IBLOCK_ID);
            $CACHE_MANAGER->EndTagCache();
        }
        $obCache->EndDataCache(array('CITY_LIST' => $arCityList));
    }

    $GLOBALS['CITY_LIST'] = $arCityList;

    $FAKE_IP = $APPLICATION->get_cookie('FAKE_IP');
    $CITY_ID = $APPLICATION->get_cookie(CITY_COOKIE_NAME);

    if (!array_key_exists($CITY_ID, $arCityList) || strlen($FAKE_IP) > 0) {
        $CITY_ID = false;

        if (CModule::IncludeModule('msgeoip')) {
            if ($FAKE_IP) {
                $REMOTE_IP = $FAKE_IP;
            } else {
                $REMOTE_IP =
                    !empty($_SERVER['HTTP_X_FORWARDED_FOR'])
                        ? reset(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']))
                        : $_SERVER['REMOTE_ADDR'];
            }
            //$REMOTE_IP = '2.95.34.0';

            $rsCityBlock = CMSGEOIPCityBlocks::GetByIP($REMOTE_IP);
            if ($arCityBlock = $rsCityBlock->Fetch()) {
                if ($arCityBlock['LATITUDE'] && $arCityBlock['LONGITUDE']) {
                    $COORD = $arCityBlock['LATITUDE'].','.$arCityBlock['LONGITUDE'];
                    $BEST_DIST = false;
                    $BEST_CITY = false;
                    foreach ($arCityList as $arCity) {
                        $DIST = GetDistanceForCoord($arCity['PROPERTIES']['COORD']['VALUE'], $COORD);
            //AddMessage2Log(print_r($arCity,true));
                        if ($BEST_DIST === false || $BEST_DIST > $DIST) {
                            $BEST_DIST = $DIST;
                            $BEST_CITY = $arCity['ID'];
                        }
                    }

                    $CITY_ID = $BEST_CITY;
                }
            }
        }

        if (!$CITY_ID) {
            foreach ($arCityList as $arCity) {
                if (!$CITY_ID) {
                    $CITY_ID = $arCity['ID'];
                } //first city by SORT field
                if ($arCity['PROPERTIES']['DEFAULT']['VALUE']) {
                    $CITY_ID = $arCity['ID']; //set city by DEFAULT property
                    break;
                }
            }
        }

        //$APPLICATION->set_cookie("CITY_ID", $CITY_ID, time() + 60 * 60 * 24 * 30 * 12);
    }

    $GLOBALS['CURRENT_CITY'] = $arCityList[$CITY_ID];

    if (in_array($CITY_ID, [402, 404, 35])) {
        LocalRedirect('http://samara.mamadeti.ru/');
    }
}

function CityDetection()
{
    // if (EnvManager::getInstance()->isDev()) {
    //     return CityDetectionNew();
    // } else {
    //     return CityDetectionOld();
    // }
    CityDetectionSuperNew();
}

//Определяем текущий город
function GetCurrentCity()
{
    global $APPLICATION;

    $CITY_ID = $GLOBALS['CURRENT_CITY']['ID'];
    //$CITY_ID = $APPLICATION->get_cookie("CITY_ID");
    if (empty($CITY_ID)) {
        return false;
    }

    return $CITY_ID;
}

//Определяем название текущего города
function GetNameCity($CITY_ID)
{
    return $GLOBALS['CITY_LIST'][$CITY_ID]['NAME'];
}

//Определяем номер телефона для текущего города
function GetCityPhone($CITY_ID)
{
    $arPhone = $GLOBALS['CITY_LIST'][$CITY_ID]['PROPERTIES']['PHONE'];
    $cnt = 0;

    $strphone = '<div class="b-header_phone__col in-bl">';

    if (count($arPhone['VALUE']) >= 4) {
        $phonePerColumn = ceil(count($arPhone['VALUE']) / 2);
    } else {
        $phonePerColumn = 0;
    }

    foreach ($arPhone['VALUE'] as $key => $phonetmp) {
        $taggedPhone = html_entity_decode($phonetmp);
        if (!empty($taggedPhone)) {
            CModule::IncludeModule('iblock');
            $el = CIBlockElement::GetList(
                array(),
                array(
                    'IBLOCK_ID' => 2,
                    '?NAME' => $arPhone['DESCRIPTION'][$key],
                ), false, false,
                array(
                    'ID',
                    'IBLOCK_ID',
                    'ACTIVE',
                    'DETAIL_PAGE_URL', )
                )->GetNext();
            $strphone .=
                '<div class="b-header_phone__item">'
                .'<div class="b-header_phone__num b-header_phone__bg">'
                .$taggedPhone
                .'</div>';
            if ($el['ACTIVE'] === 'Y') {
                $strphone .= '<a href="'.$el['DETAIL_PAGE_URL'].'" class="b-header_phone__bg">'
                    .$arPhone['DESCRIPTION'][$key]
                    .'</a>';
            } else {
                $strphone .= '<span class="b-header_phone__bg">'.$arPhone['DESCRIPTION'][$key].'</span>';
            }
            $strphone .= '</div>';
            ++$cnt;

            if ($phonePerColumn && $cnt == $phonePerColumn) {
                $strphone .= '</div><div class="b-header_phone__col in-bl">';
            }
        }
    }

    $strphone .= '</div>';

    if ($phonePerColumn && $cnt > $phonePerColumn) {
        return '<div class="b-header_phone__wrap">'.$strphone.'</div>';
    } else {
        return '<div class="b-header_phone__wrap b-header_phone__onecol">'.$strphone.'</div>';
    }
}

//Получаем количество счетчиков для главной
function GetCityBirth($CITY_ID)
{
    $data = null;
    if (!empty($GLOBALS['CITY_LIST'][$CITY_ID]['PROPERTIES']['children_born']['VALUE']) && !empty($GLOBALS['CITY_LIST'][$CITY_ID]['PROPERTIES']['childbirth_count']['VALUE'])) {
        $data = array(
            'children_born' => str_split($GLOBALS['CITY_LIST'][$CITY_ID]['PROPERTIES']['children_born']['VALUE'], 1),
            'childbirth_count' => str_split($GLOBALS['CITY_LIST'][$CITY_ID]['PROPERTIES']['childbirth_count']['VALUE'], 1),
        );
    }

    return $data;
}

//Расстояние между 2 точками
function GetDistanceForCoord($coord1, $coord2)
{
    $dist = '';
    if ((!empty($coord1)) && (!empty($coord2))) {
        $coord = explode(',', $coord1);
        $lat1 = deg2rad(trim($coord[0]));
        $lng1 = deg2rad(trim($coord[1]));

        $coord = explode(',', $coord2);
        $lat2 = deg2rad(trim($coord[0]));
        $lng2 = deg2rad(trim($coord[1]));

        if ((!empty($lat1)) && (!empty($lng1)) && (!empty($lat1)) && (!empty($lat2))) {
            $delta_lat = $lat2 - $lat1;
            $delta_lng = $lng2 - $lng1;

            $dist = round(6378137 * acos(cos($lat1) * cos($lat2) * cos($lng1 - $lng2) + sin($lat1) * sin($lat2)) / 1000, 2);
        }
    }

    return $dist;
}

function setServiceClinics()
{
    CModule::IncludeModule('iblock');
    $resel = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 2, 'ACTIVE' => 'Y'), false, false, array('ID'));
    while ($arresel = $resel->GetNext()) {
        $service = array();
        $arFilter = array('IBLOCK_ID' => 1, 'ACTIVE' => 'Y', 'PROPERTY_CLINICS' => $arresel['ID']);
        $resserv = CIBlockElement::GetList(array('SORT' => 'asc', 'NAME' => 'asc'), $arFilter, false, false, array('IBLOCK_SECTION_ID'));
        while ($arresserv = $resserv->GetNext()) {
            $ressect = CIBlockSection::GetByID($arresserv['IBLOCK_SECTION_ID']);
            if ($arressect = $ressect->GetNext()) {
                if ($arressect['IBLOCK_SECTION_ID'] > 0) {
                    $service[] = $arressect['IBLOCK_SECTION_ID'];
                }
            }
            $service[] = $arresserv['IBLOCK_SECTION_ID'];
        }
        $service = array_unique($service);
        CIBlockElement::SetPropertyValues($arresel['ID'], 2, $service, 'SERVICE');
    }

    return 'setServiceClinics();';
}

function setSectionDoctors()
{
    CModule::IncludeModule('iblock');
    $resel = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 3), false, false, array('ID', 'IBLOCK_SECTION_ID'));
    while ($arresel = $resel->GetNext()) {
        $ressect = CIBlockSection::GetByID($arresel['IBLOCK_SECTION_ID']);
        if ($arressect = $ressect->GetNext()) {
            CIBlockElement::SetPropertyValues($arresel['ID'], 3, $arressect['NAME'], 'SECTION');
        }
    }

    return 'setSectionDoctors;';
}

function GetCustomPublicEditHTML($arProperty, $value, $strHTMLControlName)
{
    /* @var CMain */
    global $APPLICATION;

    $s = '<div class="text04"><input type="text" name="'.htmlspecialcharsbx($strHTMLControlName['VALUE']).'" size="25" value="'.htmlspecialcharsbx($value['VALUE']).'" />';
    ob_start();
    $APPLICATION->IncludeComponent(
        'bitrix:main.calendar',
        '',
        array(
            'FORM_NAME' => $strHTMLControlName['FORM_NAME'],
            'INPUT_NAME' => $strHTMLControlName['VALUE'],
            'INPUT_VALUE' => $value['VALUE'],
            'SHOW_TIME' => 'N',
        ),
        null,
        array('HIDE_ICONS' => 'Y')
    );
    $s .= ob_get_contents().'</div>';
    ob_end_clean();

    return  $s;
}

function shortenName($name)
{
    $nameParts = explode(' ', $name);
    if (count($nameParts) !== 3) {
        return $name;
    }

    return $nameParts[0].' '.substr($nameParts[1], 0, 1).'. '.substr($nameParts[2], 0, 1).'.';
}

function shortenString($s, $len)
{
    if (strlen($s) <= $len) {
        return $s;
    }

    $s = substr($s, 0, $len);
    $s = trim($s);
    $s .= '...';

    return $s;
}

function redirectCityId($cityId)
{
    global $APPLICATION;
    $cm = CityManager::getInstance();
    if ($GLOBALS['CITY_ID'] != $cityId) {
        $redirectDomain = $cm->getDomainByCityId($cityId, false);
        if (!empty($redirectDomain)) {
            $redirectUrl = 'http://'.$redirectDomain.$APPLICATION->GetCurUri();
            LocalRedirect($redirectUrl);
            die();
        }
    }
}

function RedirectToMobile()
{
    global $APPLICATION;

    if (!IS_MOBILE_SITE) {
        if (isset($_SERVER['HTTP_REFERER']) && preg_match('/^http:\/\/m\..*mamadeti\.ru/', $_SERVER['HTTP_REFERER'])) {
            session_start();
            $_SESSION['force_desktop'] = true;
        }
    }
    if ((IS_MOBILE && !IS_TABLET) && !IS_MOBILE_SITE && ((session_id() || session_start()) && !$_SESSION['force_desktop'])) {
        $cm = CityManager::getInstance();
        $redirectUrl = 'http://'.$cm->getDomainByCityId($cm->getCurrentCityId(), true).$APPLICATION->GetCurUri();
        LocalRedirect($redirectUrl, true);
        die();
    }
}

function getReplacementPhonesRaw($cityId) {
    $replacementPhones = [];
    $city = CityManager::getInstance()->getCityById($cityId);
    if (!empty($city) && !empty($city['PROPERTIES']['PHONE_REPLACEMENT'])) {
        foreach($city['PROPERTIES']['PHONE_REPLACEMENT']['VALUE'] as $index => $phone) {
            $id = $city['PROPERTIES']['PHONE_REPLACEMENT']['DESCRIPTION'][$index];
            $replacementPhones[$id] = $phone;
        }
    }
    return $replacementPhones;
}

function getReplacementPhones($cityId) {
    return json_encode(getReplacementPhonesRaw($cityId));
}

function getComagicIdRaw($cityId) {
    $comagicId = null;
    $city = CityManager::getInstance()->getCityById($cityId);
    if (!empty($city) && !empty($city['PROPERTIES']['COMAGIC_ID']['VALUE'])) {
        $comagicId = $city['PROPERTIES']['COMAGIC_ID']['VALUE'];
    }
    return $comagicId;
}

function getComagicId($cityId) {
    return json_encode(getComagicIdRaw($cityId));
}

function fetchCityPhones($cityId) {

    static $cachedPhones = [];

    if (isset($cachedPhones[$cityId])) {
 //       return $cachedPhones[$cityId];
    }

    $cache_dir = '/';

    $obCache = new CPHPCache();
    if (1==2 && $obCache->InitCache(60 * 60 * 24, "phones_".$cityId, $cache_dir)) {
        $vars = $obCache->GetVars();
        $phones = $vars[0];
    } else {
        $phones = [];
        $city = CityManager::getInstance()->getCityById($cityId);
        $cityPhones = $city['PROPERTIES']['PHONE'];

        $sort = ['SORT' => 'ASC'];
        $select = ['ID', 'NAME', 'ACTIVE', 'DETAIL_PAGE_URL'];

        $nav = ['nTopCount' => 1];
        foreach($cityPhones['VALUE'] as $phoneKey => $phoneValue) {
            $filter = [
                'IBLOCK_ID' => CLINIC_IBLOCK_ID,
            ];

            $phone = html_entity_decode($phoneValue);
            $description = $cityPhones['DESCRIPTION'][$phoneKey];
            if (empty($phone)) {
                continue;
            }
            if (!empty($description)) {
                if(preg_match('/^\[(\d+)\]/', $description, $match)){
                    $description = mb_substr($description, mb_strlen($match[0]));

                    $filter['ID'] = $match[1];
                }else{
                    $filter['?NAME'] = $description;
                }

                $result = CIBlockElement::GetList($sort, $filter, false, $nav, $select);
                $clinic = empty($result) ? null : $result->GetNext();
            } else {
                $clinic = null;
            }
            $phones[] = [
                'phone' => $phone,
                'description' => $description,
                'clinic' => $clinic,
            ];
        }

        if ($obCache->StartDataCache()) {
            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . CLINIC_IBLOCK_ID);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($phones));
        }
    }

    $cachedPhones[$cityId] = $phones;

    return $phones;
}

function getCurrentCityPhone() {
    global $APPLICATION;
    $cityId = CityManager::getInstance()->getCurrentCityId();
    $clinic = $APPLICATION->GetPageProperty('CLINIC');
    $cityPhones = fetchCityPhones($cityId);

    ob_start();
    $APPLICATION->IncludeFile('/includes/header/cityPhones.php', compact('clinic', 'cityPhones'));
    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}

function callToSoapService($soapAction, $params) {

    require_once('nusoap/nusoap.php');

    $soapClient = new nusoap_client(SOAP_SERVICE_WSDL, true);
    $soapClient->setCredentials(SOAP_SERVICE_USER, SOAP_SERVICE_PASS, 'basic');
    $soapClient->soap_defencoding = 'UTF-8'; //Fix encode erro, if you need

    // SetAppointment  SetClientCallback
    if ($soapAction == "CALLBACK") {
        $soapAction = 'SetClientCallback';
    } elseif ($soapAction == "ZAPIS_NA_PRIEM") {
        $soapAction = 'SetAppointment';
    } else {
        return false;
    }

    //AddMessage2Log(print_r($params,true));

    $return = $soapClient->call($soapAction, $params);

    //AddMessage2Log(print_r($return,true));
    // ----------
    //array(1) {
    //  ["SetClientCallbackResult"]=>
    //  array(2) {
    //    ["ErrorCode"]=>
    //    string(1) "0"
    //    ["ErrorMsg"]=>
    //    string(0) ""
    //  }
    //}

    if ($soapClient->fault) {
        return $return;
    } else {
        $err = $soapClient->getError();
        if ($err) {
            return 'NUSOAP_ERR: ' .$err;
        } else {
            // Result';
            $returnText = $return[$soapAction.'Result']['ErrorCode'];
            if ($returnText < 0) {
                $returnText .= ' '.$return[$soapAction.'Result']['ErrorMsg'];
            }
            return $returnText;
        }
    }
}

function getClinicNameById($clinicId) {
    if (intval($clinicId)>0) {
        $rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$clinicId),false,false,array("IBLOCK_ID","ID","NAME"));
        if ($arElement = $rsElement->GetNext())
        {
            return $arElement["NAME"];
        }
    }
    return $clinicId;
}

function error404(){
    global $APPLICATION;
    
    $APPLICATION->RestartBuffer();
    require($_SERVER["DOCUMENT_ROOT"]."/404.php");
    die();
}

function setCanonicalPage($page)
{
    global $APPLICATION;

    static $canonicalPage = '';

    if(!$canonicalPage){
        $CITY_ID = GetCurrentCity();
        $cm = CityManager::getInstance();
        $domain = $cm->getDomainByCityId($CITY_ID);

        $canonicalPage = 'http://' . $domain . $page;

        $APPLICATION->AddHeadString('<link rel="canonical" href="' . $canonicalPage . '" />', true);
    }
}

function setLastModified($timestamp)
{
    if(headers_sent()){
        return;
    }

    $ifModifiedSince = false;

    if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])){
        $ifModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
    }else if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
        $ifModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
    }

    $lastModifiedUnix = strtotime(date("D, d M Y H:i:s", $timestamp));

    if ($ifModifiedSince && $ifModifiedSince >= $lastModifiedUnix) {
        header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
        exit;
    }

    header('Last-Modified: '. gmdate("D, d M Y H:i:s \G\M\T", $timestamp));
}
