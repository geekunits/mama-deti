<?php

class Service
{
    protected static $instance = null;

    protected $cachedServices = [];

    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {
        CModule::IncludeModule('iblock');
    }

    protected function findServiceSection($field, $sectionId)
    {
        $obSection = new CIBlockSection();

        $arSection =
            $obSection->GetList(
                [],
                ['IBLOCK_ID' => SERVICE_IBLOCK_ID, $field => $sectionId],
                false,
                ['IBLOCK_ID', 'ID', 'ACTIVE', 'NAME', 'CODE', 'DESCRIPTION', 'SECTION_PAGE_URL', 'DEPTH_LEVEL', 'UF_PROCEDURES', 'UF_LINKED_SERVICES', 'UF_LINKED_SERVICE_EL', 'UF_SHOW_PREGN_COUNT', 'TIMESTAMP_X']
            )->
            GetNext();

        if ($arSection) {
            $arSection['SERVICE_TYPE'] = 'section';
            $arSection['SERVICE_IDS'] = CMamaDetiAPI::GetServicesID(['SECTION_ID' => $arSection['ID'], 'INCLUDE_SUBSECTIONS' => 'Y']);
            $arSection['URL'] = $arSection['SECTION_PAGE_URL'];
            $arSection['PROCEDURE_IDS'] = $arSection['UF_PROCEDURES'];
            $arSection['LINKED_SERVICE_SECTION_IDS'] = $arSection['UF_LINKED_SERVICES'] ?: [];
            $arSection['LINKED_SERVICE_ELEMENT_IDS'] = $arSection['UF_LINKED_SERVICE_EL'] ?: [];
            $seoPropertyValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arSection["IBLOCK_ID"], $arSection["ID"]);
            $arSection["SEO"] = $seoPropertyValues->getValues();
            $arSection['META_TITLE'] = $arSection['SEO']['SECTION_META_TITLE'];
            $arSection['META_KEYWORDS'] = $arSection['SEO']['SECTION_META_KEYWORDS'];
            $arSection['META_DESCRIPTION'] = $arSection['SEO']['SECTION_META_DESCRIPTION'];

        }

        return $arSection;
    }

    protected function findServiceElement($field, $elementId)
    {
        $obElement = new CIBlockElement();

        $arElement =
            $obElement->GetList(
                [],
                ['IBLOCK_ID' => SERVICE_IBLOCK_ID, $field => $elementId],
                false,
                false,
                ['IBLOCK_ID', 'ID', 'ACTIVE', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'PROPERTY_PROCEDURES', 'PROPERTY_LINKED_SERVICES', 'PROPERTY_LINKED_SERVICE_SECTIONS', 'PROPERTY_SHOW_PREGN_COUNT', 'TIMESTAMP_X']
            )->
            GetNextElement();

        if ($arElement) {
            $arElement = array_merge($arElement->GetFields(), ['PROPERTIES' => $arElement->GetProperties()]);
            $arElement['SERVICE_TYPE'] = 'element';
            $arElement['DESCRIPTION'] = $arElement['PREVIEW_TEXT']."\n".$arElement['DETAIL_TEXT'];
            $arElement['URL'] = $arElement['DETAIL_PAGE_URL'];
            $arElement['SERVICE_IDS'] = [$arElement['ID']];
            $arElement['PROCEDURE_IDS'] = $arElement['PROPERTIES']['PROCEDURES']['VALUE'];
            $arElement['LINKED_SERVICE_SECTION_IDS'] = $arElement['PROPERTIES']['LINKED_SERVICE_SECTIONS']['VALUE'] ?: [];
            $arElement['LINKED_SERVICE_ELEMENT_IDS'] = $arElement['PROPERTIES']['LINKED_SERVICES']['VALUE'] ?: [];
            $seoPropertyValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arElement["IBLOCK_ID"], $arElement["ID"]);
            $arElement["SEO"] = $seoPropertyValues->getValues();
            $arElement['META_TITLE'] = $arElement['SEO']['ELEMENT_META_TITLE'];
            $arElement['META_KEYWORDS'] = $arElement['SEO']['ELEMENT_META_KEYWORDS'];
            $arElement['META_DESCRIPTION'] = $arElement['SEO']['ELEMENT_META_DESCRIPTION'];
        }

        return $arElement;
    }

    protected function filterLinkedServiceElements($clinicId, $linkedElementIds)
    {
        $clinicServiceIds = CMamaDetiAPI::getClinicsServicesID($clinicId);
        if (!$clinicServiceIds) {
            return [];
        }

        return array_intersect($clinicServiceIds, $linkedElementIds);
    }

    protected function filterLinkedServiceSections($clinicId, $linkedSectionIds)
    {
        $clinicServiceSectionIds = CMamaDetiAPI::getClinicsServiceSectionsID(['ID' => $clinicId]);
        if (!$clinicServiceSectionIds) {
            return [];
        }

        return array_intersect($clinicServiceSectionIds, $linkedSectionIds);
    }

    public function getLinkedServiceIds($service, $clinicId = null)
    {
        if ($clinicId) {
            if ($service['LINKED_SERVICE_ELEMENT_IDS']) {
                $service['LINKED_SERVICE_ELEMENT_IDS'] =
                    $this->filterLinkedServiceElements(
                        $clinicId,
                        $service['LINKED_SERVICE_ELEMENT_IDS']
                    );
            }

            if ($service['LINKED_SERVICE_SECTION_IDS']) {
                $service['LINKED_SERVICE_SECTION_IDS'] =
                    $this->filterLinkedServiceSections(
                        $clinicId,
                        $service['LINKED_SERVICE_SECTION_IDS']
                    );
            }
        }

        return ['SECTION_IDS' => $service['LINKED_SERVICE_SECTION_IDS'], 'ELEMENT_IDS' => $service['LINKED_SERVICE_ELEMENT_IDS']];

    }
    
    public function getEkoClinicIds($service)
    {
        $cacheKey = 'eko.'.$service['SERVICE_TYPE'].'.'.$service['ID'];
        if (!isset($this->cachedServices[$cacheKey])) {
            $filter = [
                'IBLOCK_ID' => CLINIC_IBLOCK_ID,
                'ACTIVE' => 'Y',
            ];
            switch ($service['SERVICE_TYPE']) {
                case 'section':
                    $filter['PROPERTY_EKO_SECTIONS'] = $service['ID'];
                    break;
                    
               case 'element':
                    $filter['PROPERTY_EKO_ELEMENTS'] = $service['ID'];
                    break;
            }
            
            $ids = [];
            $rsClinics = CIBlockElement::GetList([], $filter);
            while ($arClinic = $rsClinics->GetNext()) {
                $ids[] = $arClinic['ID'];
            }
            
            $this->cachedServices[$cacheKey] = $ids;
        }
        
        return $this->cachedServices[$cacheKey];
    }

    public function findService($type, $serviceId, $clinicId = null)
    {
        $cacheKey = $type.$serviceId;

        if (!isset($this->cachedServices[$cacheKey])) {
            $field = is_numeric($serviceId) ? 'ID' : 'CODE';

            switch ($type) {
                case 'section':
                    $service = $this->findServiceSection($field, $serviceId);
                    break;

                case 'element':
                    $service = $this->findServiceElement($field, $serviceId);
                    break;
            }
            if ($service) {
                $description = ServiceDescription::getInstance()->getDescription($type, $service['ID'], $clinicId);
                if ($description) {
                    $service['DESCRIPTION'] = $description;
                }

                $this->cachedServices[$type.$service['CODE']] = $service;
                $this->cachedServices[$type.$service['ID']] = $service;
            } else {
                $this->cachedServices[$cacheKey] = null;
            }
        }

        return $this->cachedServices[$cacheKey];
    }

    public function findServiceSectionChildren($sectionId, $clinicId = null)
    {
        $cacheKey = 'children.'.$sectionId.'.'.$clinicId;

        if (!isset($this->cachedServices[$cacheKey])) {
            $parentSection = $this->findService('section', $sectionId);
            if ($clinicId) {
                $clinicServiceElementIds = CMamaDetiAPI::getClinicsServicesID(['ID' => $clinicId]);
                $clinicServiceSectionIds = CMamaDetiAPI::getClinicsServiceSectionsID(['ID' => $clinicId]);
            }

            $children = [];

            $obElement = new CIBlockElement;
            $elementFilter = [
                'IBLOCK_ID' => SERVICE_IBLOCK_ID,
                'ACTIVE' => 'Y',
                'SECTION_ID' => $parentSection['ID'],
                'INCLUDE_SUBSECTIONS' => 'N',
            ];
            if ($clinicId) {
                $elementFilter['ID'] = $clinicServiceElementIds ?: -1;
            }
            $rsElements = 
                $obElement->GetList(
                    ["SORT" => "ASC", 'ID' => 'ASC'],
                    $elementFilter
                );
            while ($arElement = $rsElements->GetNext()) {
                $arElement['SERVICE_TYPE'] = 'element';
                $arElement['URL'] = $arElement['DETAIL_PAGE_URL'];

                $children[] = $arElement;
            }
            unset($rsElements);

            $obSection = new CIBlockSection;
            $sectionFilter = [
                'IBLOCK_ID' => SERVICE_IBLOCK_ID, 
                'ACTIVE' => 'Y', 
                'DEPTH_LEVEL' => $parentSection['DEPTH_LEVEL'] + 1,
                'SECTION_ID' => $parentSection['ID'],
            ];
            if ($clinicId) {
                $sectionFilter['ID'] = $clinicServiceSectionIds ?: -1;
            }
            $rsSections = 
                $obSection->GetList(
                    ["left_margin" => "asc"], 
                    $sectionFilter
                );
                
            while ($arSection = $rsSections->GetNext()) {
                $arSection['SERVICE_TYPE'] = 'section';
                $arSection['URL'] = $arSection['SECTION_PAGE_URL'];

                $children[] = $arSection;
            }
            unset($rsSections);

            $this->cachedServices[$cacheKey] = $children;
        }

        return $this->cachedServices[$cacheKey];
    }

    public function getRedirectUrl($type, $serviceId) {

        $cityId = CityManager::getInstance()->getSavedCityId();
        $clinicIds = CMamaDetiAPI::getClinicsID(['PROPERTY_CITY' =>$cityId]);
        if ($type === 'section') {
            $parentIds = CMamaDetiAPI::getSectionParentIds($serviceId);
            $redirectSectionIds = $this->getSectionRedirectSectionIds($serviceId, $clinicIds);
        } else {
            $parentIds = CMamaDetiAPI::getElementParentIds($serviceId);
            $redirectSectionIds = $this->getElementRedirectSectionIds($serviceId, $clinicIds);
        }
        if (empty($redirectSectionIds)) {
            return false;
        }
        foreach($parentIds as $parentId) {
            if (in_array($parentId, $redirectSectionIds)) {
                $redirectSection = $this->findServiceSection('ID', $parentId);
                if (!empty($redirectSection)) {
                    return $redirectSection['SECTION_PAGE_URL'];
                }
            }
        }
        return '/services/';
    }

    protected function getSectionRedirectSectionIds($serviceId, $clinicIds) {
        $serviceIds = CMamaDetiAPI::getClinicsServiceSectionsID(['ID' => $clinicIds]);
        if (in_array($serviceId, $serviceIds)) {
            return false;
        }
        return $serviceIds;

    }

    protected function getElementRedirectSectionIds($serviceId, $clinicIds) {
        $serviceIds = CMamaDetiAPI::getClinicsServicesID(['ID' => $clinicIds]);
        if (in_array($serviceId, $serviceIds)) {
            return false;
        }
        return CMamaDetiAPI::getClinicsServiceSectionsID(['ID' => $clinicIds]);
    }
}
