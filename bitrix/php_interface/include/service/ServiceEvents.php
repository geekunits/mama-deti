<?php

/**
 * Класс с обработчиками событий для инфоблока услуг.
 *
 * Обрабатывает создание, изменение и удаление разделов и элементов в услугах,
 * делегируя ServiceMirror управление зеркалами изменившихся объектов.
 */
class ServiceEvents
{
    public function onAfterIBlockSectionAdd(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] != SERVICE_IBLOCK_ID || !$arFields['RESULT']) {
            return;
        }

        ServiceMirror::getInstance()->makeMirrorForServiceSection($arFields['RESULT']);
    }

    public function onAfterIBlockSectionUpdate(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] != SERVICE_IBLOCK_ID || !$arFields['RESULT']) {
            return;
        }

        ServiceMirror::getInstance()->makeMirrorForServiceSection($arFields['ID'], true);
    }

    public function onBeforeIBlockSectionDelete($ID)
    {
        $arSection = CIBlockSection::GetByID($ID)->GetNext();
        if (!$arSection || $arSection['IBLOCK_ID'] != SERVICE_IBLOCK_ID) {
            return true;
        }

        ServiceMirror::getInstance()->deleteMirrorForServiceSection($ID);
    }

    public function onAfterIBlockElementAdd(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] != SERVICE_IBLOCK_ID || !$arFields['RESULT']) {
            return;
        }

        ServiceMirror::getInstance()->makeMirrorForServiceElement($arFields['RESULT']);
    }

    public function onAfterIBlockElementUpdate(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] != SERVICE_IBLOCK_ID || !$arFields['RESULT']) {
            return;
        }

        ServiceMirror::getInstance()->makeMirrorForServiceElement($arFields['ID'], true);
    }

    public function onBeforeIBlockElementDelete($ID)
    {
        $arElement = CIBlockElement::GetByID($ID)->GetNext();
        if (!$arElement || $arElement['IBLOCK_ID'] != SERVICE_IBLOCK_ID) {
            return true;
        }

        ServiceMirror::getInstance()->deleteMirrorForServiceElement($ID);
    }
}
