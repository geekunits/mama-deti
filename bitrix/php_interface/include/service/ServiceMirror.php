<?php

/**
 * Класс для генерации зеркальных разделов услуг.
 *
 * Зеркальные разделы услуг - это копии разделов и элементов дерева услуг в
 * отдельном инфоблоке. Они нужны для того, чтобы добавлять региональные и привязанные
 * к клиникам описания услуг.
 */
class ServiceMirror
{
    protected static $instance = null;

    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    protected function __construct()
    {
        CModule::IncludeModule('iblock');
    }

    /**
     * Получить идентификатор родительского раздела для указанного элемента.
     *
     * @param  integer      $elementId
     * @return integer|null
     */
    protected function getElementParentId($elementId)
    {
        $obElement = new CIBlockElement;

        $rsSections = $obElement->GetElementGroups($elementId, true);
        $arSection = $rsSections->GetNext();

        return $arSection['ID'];
    }

    /**
     * Получить массив идентификаторов родителей указанного раздела.
     *
     * Массив отсортирован по убыванию глубины вложенности.
     *
     * @param  integer   $sectionId
     * @return integer[]
     */
    protected function getSectionParentIds($sectionId)
    {
        $obSection = new CIBlockSection;

        $rsNav = $obSection->GetNavChain(false, $sectionId);
        $ids = [];

        while ($arSection = $rsNav->GetNext()) {
            if ($arSection['ID'] == $sectionId) {
                continue;
            }

            $ids[] = $arSection['ID'];
        }

        return $ids;
    }

    /**
     * Получить ближайшего родителя указанного рзадела.
     *
     * @param  integer $sectionId
     * @return integer
     */
    protected function getSectionParentId($sectionId)
    {
        $ids = $this->getSectionParentIds($sectionId);
        if (!$ids) {
            return null;
        } else {
            return end($ids);
        }
    }

    /**
     * Найти раздел с указанным идентификатором в услугах.
     *
     * @param  integer $id
     * @return array
     */
    protected function getServiceSection($id)
    {
        $obSection = new CIBlockSection;

        $rsSections = $obSection->GetList([], ['IBLOCK_ID' => SERVICE_IBLOCK_ID, 'ID' => $id]);

        $arSection = $rsSections->GetNext();

        return $arSection;
    }

    /**
     * Найти элемент с указанным идентификатором в услугах.
     *
     * @param  integer $id
     * @return array
     */
    protected function getServiceElement($id)
    {
        $obElement = new CIBlockElement;

        $rsElements = $obElement->GetList([], ['IBLOCK_ID' => SERVICE_IBLOCK_ID, 'ID' => $id]);

        $arElement = $rsElements->GetNext();

        return $arElement;
    }

    /**
     * Найти зеркальный раздел по фильтру.
     *
     * @param  array      $filter
     * @return array|null
     */
    protected function findMirrorSection(array $filter)
    {
        $obSection = new CIBlockSection;
        $filter =
            array_merge(
                ['IBLOCK_ID' => SERVICE_DESCRIPTION_IBLOCK_ID, 'ACTIVE' => 'Y'],
                $filter
            );

        $rsSections =
            $obSection->GetList(
                [],
                $filter,
                false,
                ['CODE', 'NAME', 'IBLOCK_ID', 'ID', 'IBLOCK_SECTION_ID', 'UF_*']
            );

        $arSection = $rsSections->GetNext();

        return $arSection;
    }

    /**
     * Добавить зеркальный раздел с указанными значениями полей.
     *
     * В случае успеха метод возвращает идентификатор нового раздела.
     *
     * @param  array        $fields
     * @return integer|null
     */
    protected function addMirrorSection(array $fields)
    {
        $obSection = new CIBlockSection;

        $fields =
            array_merge(
                ['IBLOCK_ID' => SERVICE_DESCRIPTION_IBLOCK_ID],
                $fields
            );

        $id = $obSection->Add($fields);

        return $id;
    }

    /**
     * Обновить зеркальный раздел в соответствии с массивом полей услуги.
     *
     * Фактически обновление происходит только в том случае, когда значения полей
     * услуги отличаются от значений полей зеркального раздела.
     *
     * @param  array $mirrorSection массив полей зеркального раздела
     * @param  array $service       массив полей раздела или элемента услуги
     * @return bool
     */
    protected function updatedMirrorSection(array $mirrorSection, array $service)
    {
        if ($service['IBLOCK_SECTION_ID']) {
            $newMirrorParentId = $this->makeMirrorForServiceSection($service['IBLOCK_SECTION_ID']);
        } else {
            $newMirrorParentId = 0;
        }

        $fields = [];
        if ($newMirrorParentId != $mirrorSection['IBLOCK_SECTION_ID']) {
            $fields['IBLOCK_SECTION_ID'] = $newMirrorParentId;
        }

        $fieldNames = ['NAME', 'CODE'];
        foreach ($fieldNames as $fieldName) {
            if ($service[$fieldName] != $mirrorSection[$fieldName]) {
                $fields[$fieldName] = $service[$fieldName];
            }
        }

        if (!$fields) {
            return true;
        }

        $obSection = new CIBlockSection;

        return $obSection->Update($mirrorSection['ID'], $fields);
    }

    /**
     * Общий метод для создания зеркального раздела для раздела или элемента услуг.
     *
     * Метод возвращает идентификатор раздела.
     *
     * @param  string       $type      section или element
     * @param  string       $serviceId идентификатор раздела или элемента услуги.
     * @param  boolean|null $update    выполнять ли принудительное обновление раздела, если он был создан ранее
     * @return integer|null
     */
    public function makeMirror($type, $serviceId, $update = false)
    {
        $getParentId = 'get'.$type.'parentId';
        $getService = 'getService'.$type;
        $mirrorBindProps = ['UF_SERVICE_'.strtoupper($type) => $serviceId];
        $parentId = $this->{$getParentId}($serviceId);
        if ($parentId) {
            $mirrorParentId = $this->makeMirrorForServiceSection($parentId);
        } else {
            $mirrorParentId = 0;
        }
        if ($mirrorSection = $this->findMirrorSection($mirrorBindProps)) {
            if ($update) {
                $service = $this->{$getService}($serviceId);

                $this->updatedMirrorSection($mirrorSection, $service);
            }

            return $mirrorSection['ID'];
        } else {
            $service = $this->{$getService}($serviceId);
            return $this->addMirrorSection(array_merge([
                'NAME' => $service['NAME'],
                'CODE' => $service['CODE'],
                'IBLOCK_SECTION_ID' => $mirrorParentId,
            ], $mirrorBindProps));
        }
    }

    protected function deleteMirror($type, $serviceId)
    {
        $mirrorBindProps = ['UF_SERVICE_'.strtoupper($type) => $serviceId];
        if ($mirrorSection = $this->findMirrorSection($mirrorBindProps)) {
            $obSection = new CIBlockSection;

            return $obSection->Delete($mirrorSection['ID']);
        } else {
            return false;
        }
    }

    /**
     * Создать зеркальный раздел для раздела услуг.
     *
     * @param  integer      $sectionId
     * @param  boolean      $update
     * @return integer|null
     */
    public function makeMirrorForServiceSection($sectionId, $update = false)
    {
        return $this->makeMirror('section', $sectionId, $update);
    }

    /**
     * Создать зеркальный раздел для элемента услуг.
     *
     * @param  integer      $elementId
     * @param  boolean      $update
     * @return integer|null
     */
    public function makeMirrorForServiceElement($elementId, $update = false)
    {
        return $this->makeMirror('element', $elementId, $update);
    }

    public function deleteMirrorForServiceSection($sectionId)
    {
        return $this->deleteMirror('section', $sectionId);
    }

    public function deleteMirrorForServiceElement($elementId)
    {
        return $this->deleteMirror('element', $elementId);
    }
}
