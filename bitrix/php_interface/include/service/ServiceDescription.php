<?php

class ServiceDescription
{
    protected static $instance = null;

    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    protected function __construct()
    {
        CModule::IncludeModule('iblock');
    }

    public function getDescription($type, $serviceId, $clinicId = null)
    {
        $mirrorSectionId = ServiceMirror::getInstance()->makeMirror($type, $serviceId);
        $cityId = CityManager::getInstance()->getSavedCityId();

        $additionalFilter = [
            //'PROPERTY_CLINIC' => false,
        ];
        if ($clinicId) {
            $additionalFilter['PROPERTY_CLINIC'] = $clinicId;
        } else {
            $additionalFilter['PROPERTY_REGION'] = $cityId;
        }

        $filter =
            array_merge([
                'IBLOCK_ID' => SERVICE_DESCRIPTION_IBLOCK_ID,
                'SECTION_ID' => $mirrorSectionId,
                'ACTIVE' => 'Y',
                'INCLUDE_SUBSECTIONS' => 'N',
            ], $additionalFilter);

        $obElement = new CIBlockElement;

        $arElement =
            $obElement->GetList(
                [],
                $filter,
                false,
                false,
                [
                    'IBLOCK_ID',
                    'ID',
                    'PREVIEW_TEXT',
                    'PROPERTY_REGION',
                    'PROPERTY_CLINIC',
                ]
            )->
            GetNext();
        if ($arElement) {
            return $arElement['PREVIEW_TEXT'];
        } else {
            return null;
        }
    }

    public function getDescriptionForSection($sectionId, $clinicId = null)
    {
        return $this->getDescription('section', $sectionId, $clinicId);
    }

    public function getDescriptionForElement($elementId, $clinicId = null)
    {
        return $this->getDescription('element', $elementId, $clinicId);
    }
}
