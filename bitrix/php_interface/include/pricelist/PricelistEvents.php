<?php

/**
 * Класс с обработчиками событий для инфоблока прайс листов.
 *
 */
class PricelistEvents
{

	public function onAfterIBlockSectionUpdate(&$arFields)
	{
		if ($arFields['IBLOCK_ID'] != PRICELIST_IBLOCK_ID || !$arFields['RESULT']) {
			return;
		}
		if (!isset($arFields['UF_UPDATEPROGRAM'])) {
			return;
		}
		if (!$arFields['UF_UPDATEPROGRAM']) {
			return;
		}
		if (!$arFields["UF_CLINIC"]) {
			return;
		}

		//AddMessage2Log("JSON: ".json_encode($arFields));

		// Убираем галочку обновления программ
		$bs = new CIBlockSection;
		$options = Array(
			"IBLOCK_ID" => $arFields['IBLOCK_ID'],
			"UF_UPDATEPROGRAM" => '0'
		);
		$bs->Update($arFields['ID'], $options);

		// Берем все прайслисты из раздела клиники ($arFields["UF_CLINIC"])
		$pricelistItems = [];
		$selectArray = Array("ID", "NAME", "CODE", "XML_ID", "PROPERTY_PRICE");
		$filterArray = Array(
			"IBLOCK_ID"          => $arFields['IBLOCK_ID'],
			"SECTION_ID"         => $arFields['ID'],
			"INCLUDE_SUBSECTIONS"=> "Y",
			"ACTIVE"             => "Y"
		);
		$res = CIBlockElement::GetList(
			Array(),
			$filterArray,
			false,
			false,
			$selectArray
		);
		while($ob = $res->GetNextElement())
		{
			$elem = $ob->GetFields();
			$xml_id = trim($elem["XML_ID"], " \t\n\r\0\x0B");
			$pricelistItems[$xml_id] = $elem;
		}

		// Берем все програмы у данной клиники
		$programItems = [];
		$selectArray = Array(
			"ID",
			"NAME",
			//"PROPERTY_CLINIC", // множественное, берём по другому
			//"PROPERTY_DIRECTION", // множественное, берём по другому
			//"PROPERTY_SERVICES", // множественное, берём по другому
			"PROPERTY_PRICE",
			//"PROPERTY_DIRECTION_SORT",
			"PROPERTY_CODE_ERU",
			//"PROPERTY_AGE",
		);
		$filterArray = Array(
			"IBLOCK_ID"          => PROGRAM_IBLOCK_ID,
			//"!PROPERTY_CODE_ERU" => null,
			"PROPERTY_CLINIC"    => $arFields["UF_CLINIC"],
			"ACTIVE"             => "Y"
		);
		$res = CIBlockElement::GetList(
			Array(),
			$filterArray,
			false,
			false,
			$selectArray
		);
		while($ob = $res->GetNextElement())
		{
			$elem = $ob->GetFields();
			$programItems[$elem['ID']] = $elem;
		}

		// При совпадении ЕРУ в прайсе и программе - обновляем название программы и ее стоимость
		$el = new CIBlockElement;
		foreach ($programItems as $pItem) {

			$codeERU = trim($pItem["PROPERTY_CODE_ERU_VALUE"], " \t\n\r\0\x0B");
			if (!$codeERU) {
				continue;
			}

			if (isset($pricelistItems[$codeERU])) {
				//// Получаем множественное поле CLINIC
				//$multiCLINIC = array();
				//$res = CIBlockElement::GetProperty(
				//	PROGRAM_IBLOCK_ID,
				//	$pItem['ID'],
				//	array("sort" => "asc"),
				//	array("CODE" => "CLINIC")
				//);
				//while ($ob = $res->GetNext())
				//{
				//	$multiCLINIC[] = $ob['VALUE'];
				//}
				//
				//// Получаем множественное поле DIRECTION
				//$multiDIRECTION = array();
				//$res = CIBlockElement::GetProperty(
				//	PROGRAM_IBLOCK_ID,
				//	$pItem['ID'],
				//	array("sort" => "asc"),
				//	array("CODE" => "DIRECTION")
				//);
				//while ($ob = $res->GetNext())
				//{
				//	$multiDIRECTION[] = $ob['VALUE'];
				//}
				//
				//// Получаем множественное поле SERVICES
				//$multiSERVICES = array();
				//$res = CIBlockElement::GetProperty(
				//	PROGRAM_IBLOCK_ID,
				//	$pItem['ID'],
				//	array("sort" => "asc"),
				//	array("CODE" => "SERVICES")
				//);
				//while ($ob = $res->GetNext())
				//{
				//	$multiSERVICES[] = $ob['VALUE'];
				//}

				// не апдейтим пустые и нулевые ценники
				$itemPrice =
					$pricelistItems[$codeERU]["PROPERTY_PRICE_VALUE"]
					?
					: $pItem["PROPERTY_PRICE_VALUE"];

				// формируем массив свойств инфоблока
				$itemProp = [
					//'CLINIC' => $multiCLINIC, // множественное
					//'DIRECTION' => $multiDIRECTION, // множественное
					//'SERVICES' => $multiSERVICES, // множественное
					'PRICE' => $itemPrice,
					//'DIRECTION_SORT' => $pItem["PROPERTY_DIRECTION_SORT_VALUE"],
					//'CODE_ERU' => $codeERU,
					//'AGE' => $pItem["PROPERTY_AGE_VALUE"], // ТИП Список так обновлять нельзя!
				];
				$itemName = htmlspecialchars_decode(
					$pricelistItems[$codeERU]["NAME"],
					ENT_QUOTES
				);
				$options = Array(
					"IBLOCK_ID" => PROGRAM_IBLOCK_ID,
					"NAME" => $itemName,
					//"PROPERTY_VALUES"=> $itemProp,
				);

				// Обновляем текущую подходящую по ЕРУ Программу
				$el->Update($pItem['ID'], $options);

				// Обновляем ценник
				CIBlockElement::SetPropertyValuesEx($pItem['ID'], PROGRAM_IBLOCK_ID, array('PRICE' => $itemPrice));

				//AddMessage2Log("program #".$pItem['ID']." UPDATE: ".json_encode($options));
			}
		}

	}

}
