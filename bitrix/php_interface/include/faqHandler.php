<?php
class FaqHandler
{
    public $element;

    const EMAIL_EVENT_ID = "TL_QUESTION_ANSWER";

    public static function attachEvents()
    {
        $ob = new static;

        AddEventHandler('iblock', 'OnBeforeIBlockElementAdd', array($ob, 'onBeforeIBlockElementSave'));
        AddEventHandler('iblock', 'OnBeforeIBlockElementUpdate', array($ob, 'onBeforeIBlockElementSave'));

        AddEventHandler('iblock', 'OnAfterIBlockElementAdd', array($ob, 'onAfterIBlockElementSave'));
        AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', array($ob, 'onAfterIBlockElementSave'));
    }

    public function onBeforeIBlockElementSave(&$arFields)
    {
        if(!$this->checkIblock($arFields['IBLOCK_ID'])){
            return true;
        }

        $this->element = $arFields['ID'] ? $this->GetElementData($arFields['ID']) : false ;
    }

    public function onAfterIBlockElementSave($arFields)
    {
        if(!$this->checkIblock($arFields['IBLOCK_ID'])){
            return;
        }

        $element = $this->GetElementData($arFields['ID']);

        $answerDate     = $element['PROPERTIES']['ANSWER_DATE']['VALUE'];
        $oldAnswerDate  = $this->element ? $this->element['PROPERTIES']['ANSWER_DATE']['VALUE'] : false ;
        $active         = $element['ACTIVE'];
        $oldActive      = $this->element ? $this->element['ACTIVE'] : 'N' ;

        if(!$answerDate && ($active == 'Y' && !empty($element['~DETAIL_TEXT']))){
            $answerDate = ConvertTimeStamp(time(), "FULL");

            CIBlockElement::SetPropertyValuesEx($element['ID'], $element['IBLOCK_ID'], array(
                'ANSWER_DATE' => $answerDate
            ));
        }

        $needSend = false;

        if(!empty($element['~DETAIL_TEXT']) && $active == 'Y' && ($active != $oldActive || $answerDate != $oldAnswerDate)){ //если сменилась активность или дата ответа
            $needSend = true;
        }

        if(!$needSend) {
            return;
        }

        $email = $element['PROPERTIES']["EMAIL"]["~VALUE"];

        if (empty($email) || !check_email($email)){
            return;
        }

        $rsSite = CSite::GetList($by = "sort", $order = "desc", Array(
            'ACTIVE' => 'Y',
            'DEFAULT' => 'Y'
        ));

        if (!$arSite = $rsSite->GetNext()){
            return;
        }

        CEvent::Send(self::EMAIL_EVENT_ID, $arSite["LID"], array(
            "NAME" => $element["~NAME"],
            "QUESTION" => strip_tags($element["~PREVIEW_TEXT"]),
            "ANSWER" => strip_tags($element["~DETAIL_TEXT"]),
            "USER_EMAIL" => $email,
            "ELEMENT_DETAIL_PAGE_URL" => "http://" . $arSite["SERVER_NAME"] . $element["DETAIL_PAGE_URL"],
        ));
    }

    protected function checkIblock($id)
    {
        return $id == QA_IBLOCK_ID;
    }

    protected function getElementData($ID){
        $resElement = CIBlockElement::GetList(
            array(),
            array(
                "IBLOCK_ID" => (int)CIBlockElement::GetIBlockByID($ID),
                "ID"        => $ID
            )
        );

        if($obElement = $resElement->GetNextElement()) {
            $arElement = $obElement->GetFields();
            $arElement['PROPERTIES'] = $obElement->GetProperties();

            return $arElement;
        }

        return false;
    }
}