<?php

class Schedule {

	protected $raw;
	protected $days;
	protected $timeIntervals;
	protected $names = [
		'Понедельник',
		'Вторник',
		'Среда',
		'Четверг',
		'Пятница',
		'Суббота',
		'Воскресение',
	];

	protected $shortNames = [
		'Пн',
		'Вт',
		'Ср',
		'Чт',
		'Пт',
		'Сб',
		'Вс',
	];

	public function __construct($raw) {
		$this->raw = $raw;
		$$timeIntervals = [];
		foreach($raw as $day => $timeInterval) {
			$timeIntervals[$day] = $this->parseTimeInterval($timeInterval);
		}
		$this->timeIntervals = $timeIntervals;
	}

	protected function parseTimeInterval($timeInterval) {
		$times = array_map('trim', explode('-', $timeInterval));
		if (count($times) !== 2) {
			return false;
		}
		$dtTimes = [];
		$timeFormats = ['H:i', 'H.i'];
		foreach($times as $time) {
			foreach($timeFormats as $timeFormat) {
				$dtTime = DateTime::createFromFormat($timeFormat, $time);
				if ($dtTime !== false) {
					break;
				}
			}

			if ($dtTime === false) {
				return false;
			}
			$dtTimes[] = $dtTime;
		}
		return $dtTimes;
	}

	public function getDays() {
		if (is_null($this->days)) {
			$timeIntervals = $this->timeIntervals;
			$days = [];
			foreach($timeIntervals as $index => $dtTimes) {
				$day = new \stdClass;
				$day->name = $this->names[$index];
				$day->shortName = $this->shortNames[$index];
				if ($dtTimes !== false) {
					$day->hasSchedule = true;
					$day->beginTime = $dtTimes[0];
					$day->endTime = $dtTimes[1];
				} else {
					$day->hasSchedule = false;
					$day->beginTime = false;
					$day->endTime = false;
				}
				$day->raw = $this->raw[$index];
				$days[] = $day;
			}
			$this->days = $days;
		}
		return $this->days;
	}
}