<?php


trait ConfigurableTrait {

	public function configure(array $config = []) {
		foreach($config as $property => $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
		}
		return $this;
	}

}

class HttpRequest {

	use ConfigurableTrait;

	const HTTP_GET = 'GET';
	const HTTP_POST = 'POST';

	public $method = self::HTTP_GET;
	public $url;
	public $timeout = 10;
	public $headers = [];
	public $curlOptions = [];

	public function __construct(array $config = []) {
		$this->configure($config);
	}

	public function url($url) {
		$this->url = $url;
		return $this;
	}

	public function method($method) {
		$this->method = $method;
		return $this;
	}

	public function addHeader($header, $value) {
		$this->$headers[$header] = $value;
		return $this;
	}

	public function addHeaders(array $headers = []) {
		foreach($headers as $header => $value) {
			$this->addHeader($header, $value);
		}
		return $this;
	}

	public function send() {
		$ch = $this->prepareCurl();
		$result = curl_exec($ch);
		
		$response = $this->buildResponse($ch, $result);

		curl_close($ch);

		return $response;
	}

	protected function curlSetHeaders($ch) {
		$headers = [];
		foreach($this->headers as $header => $value) {
			$headers[] = "{$header}: {$value}";
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		return $ch;
	}

	protected function curlSetOptions($ch) {
		foreach($this->curlOptions as $option => $value) {
			curl_setopt($ch, $option, $value);
		}
		return $ch;
	}

	protected function prepareCurl() {
		$ch = curl_init($this->url);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->method);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$ch = $this->curlSetHeaders($ch);
		$ch = $this->curlSetOptions($ch);
		
		return $ch;
	}

	protected function buildResponse($ch, $result) {
        if ($result === false) {
            if ($curlErrorNumber = curl_errno($ch)) {
                $curlErrorString = curl_error($ch);
                throw new Exception('Unable to connect to "' . $this->url . '": ' . $curlErrorNumber . ' ' . $curlErrorString);
            }
            throw new Exception('Unable to connect to "' . $this->url . '".');
        }
        $info = curl_getinfo($ch);
        $response = explode("\r\n\r\n", $result, 2 + $info['redirect_count']);
        $bodyRaw = array_pop($response);
        $headersRaw = array_pop($response);
        $request = clone $this;
        return new HttpResponse($info, $request, $headersRaw, $bodyRaw);
	}

}

class HttpResponse {
	public $info;
	public $request;
	public $headersRaw;
	public $bodyRaw;
	
	public $code;
	public $headers;

	public $parseCallback;

	public function __construct($info, $request, $headersRaw, $bodyRaw) {
		$this->info = $info;
		$this->request = $request;
		$this->headersRaw = $headersRaw;
		$this->bodyRaw = $bodyRaw;
		$this->code = $this->parseCode($headersRaw);
		$this->headers = $this->parseHeaders($headersRaw);
	}

	protected function parseHeaders($headersRaw) {
        $lines = preg_split("/(\r|\n)+/", $string, -1, PREG_SPLIT_NO_EMPTY);
        $headers = [];
        foreach ($lines as $line) {
            list($name, $value) = explode(':', $line, 2);
            $headers[strtolower(trim($name))] = trim($value);
        }
        return $headers;
	}

	protected function parseCode($headersRaw) {
        $end = strpos($headersRaw, "\r\n");
        if ($end === false) {
        	$end = strlen($headersRaw);
        }
        $parts = explode(' ', substr($headersRaw, 0, $end));

        if (count($parts) < 2 || !is_numeric($parts[1])) {
            throw new Exception("Unable to parse response code from HTTP response due to malformed response.");
        }
        return intval($parts[1]);
	}

	public function getBody() {
		if (!empty($this->parseCallback)) {
			return call_user_func($this->parseCallback, $this->bodyRaw);
		}
		return $this->bodyRaw;
	}


}

class InstagramAPI {

	use ConfigurableTrait;

	private $_login;
	private $_accessToken;

	private $_cachedResponses = [];

	public $baseUrl = 'https://api.instagram.com/';
	public $version = 'v1';

	public function __construct($login, $accessToken, array $config = []) {
		$this->_login = $login;
		$this->_accessToken = $accessToken;
		$this->configure($config);
	}

	public function getAccessToken() {
		return $this->_accessToken;
	}

	public function getLogin() {
		return $this->_login;
	}

	public function get(array $sections = [], array $params = []) {
		return $this->load(HttpRequest::HTTP_GET, $sections, $params);
	}

	public function load($method, array $sections = [], array $params = []) {
		$url = $this->buildUrl($sections, $params);
        
        $cache_dir = '/';

        $obCache = new CPHPCache();
        $useCache = $method === HttpRequest::HTTP_GET;
        
        if ($useCache && $obCache->InitCache(60 * 60, "instagram_" . $url, $cache_dir)) {
            $body = $obCache->GetVars();
        } else {
            $request = new HttpRequest([
				'method' => $method,
				'url' => $url,
			]);
			$response = $request->send();
			$response->parseCallback = function($rawBody) {
				return json_decode($rawBody);
			};

			$this->checkResponse($response);
			$body = $response->getBody();
			
			if ($useCache) {
			    $obCache->EndDataCache($body);
			}
        }
        
		/*if ($method === HttpRequest::HTTP_GET) {
			$body = $this->getCachedResponse($url);
		}
		if (empty($body)) {
			$request = new HttpRequest([
				'method' => $method,
				'url' => $url,
			]);
			$response = $request->send();
			$response->parseCallback = function($rawBody) {
				return json_decode($rawBody);
			};

			if ($method = HttpRequest::HTTP_GET) {
				$this->setCachedResponse($url, $response->getBody());
			}

			$this->checkResponse($response);
			$body = $response->getBody();
		}*/

		return $body;
	}

	public function getSelfProfile() {
		return $this->get(['users', 'self']);
	}

	public function findUser($login) {
		$data = $this->get([
			'users',
			'search',

		], [
			'q' => $login,
		]);
		return empty($data->data) ? null : $data->data[0];
	}

	public function getUserMedia($id) {
		return $this->get(['users', $id, 'media', 'recent']);
	}

	protected function getCachedResponse($url) {
		return isset($this->_cachedResponses[$url]) ? $this->_cachedResponses[$url] : null;
	}

	protected function setCachedResponse($url, $response) {
		$this->_cachedResponses[$url] = $response;
		return $this;
	}

	protected function buildUrl(array $sections = [], array $params = []) {
		if (empty($params['access_token'])) {
			$params['access_token'] = $this->getAccessToken();
		}
		$queryString = http_build_query($params);

		$baseUrl = rtrim($this->baseUrl, '/') . '/' . $this->version . '/';

		$url = rtrim(($baseUrl . implode('/', $sections)), '/') . '/';
		if (!empty($queryString)) {
			$url .= '?' . $queryString;
		}

		return $url;
	}

	protected function checkResponse(HttpResponse $response) {
		$body = $response->getBody();
		if (!empty($body->meta) && ((int) $body->meta->code) !== 200) {
			throw new Exception('Instagram API error. Code: "' 
				. $body->meta->code 
				. '". Type: "' 
				. $body->meta->error_type 
				. '". Message: "' 
				. $body->meta->error_message . '".');
		}
		if ($response->code !== 200) {
			throw new Exception('Http error: "' . $response->code . '".');
		}
	}
}

class Instagram {

	private static $_instance;

	public static function getInstance() {
		if (is_null(static::$_instance)) {
			static::$_instance = new InstagramAPI(INSTAGRAM_LOGIN, INSTAGRAM_ACCESS_TOKEN);
		}
		return static::$_instance;
	}


}
