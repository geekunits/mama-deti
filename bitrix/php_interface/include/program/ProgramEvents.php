<?php

class ProgramEvents {

	public static function onAfterIBlockElementAdd(&$arFields) {
		if (!self::checkIBlock(ProgramHelper::IBLOCK_ID, $arFields)) {
			return;
		}
		ProgramHelper::updateSort($arFields['ID']);
	}

	public static function onAfterIBlockElementUpdate(&$arFields) {
		if (!self::checkIBlock(ProgramHelper::IBLOCK_ID, $arFields)) {
			return;
		}
		ProgramHelper::updateSort($arFields['ID']);
	}

	private static function checkIBlock($iBlockId, $arFields) {
		return $iBlockId == $arFields['IBLOCK_ID'];
	}
}