<?php

class ProgramDirectionEvents {
	public static function onAfterIBlockElementUpdate(&$arFields) {
		if (!self::checkIBlock(ProgramHelper::DIRECTION_IBLOCK_ID, $arFields)) {
			return;
		}
		ProgramHelper::updateDirectionSort($arFields['ID']);
	}

	public static function onBeforeIBlockElementDelete($directionId) {
		$arFields = CIBlockElement::GetByID($directionId)->GetNext();
		if (empty($arFields) || !self::checkIBlock(ProgramHelper::DIRECTION_IBLOCK_ID, $arFields)) {
			return true;
		}
		ProgramHelper::updateDirectionSort($directionId, true);
	}

	protected static function checkIBlock($iBlockId, $arFields) {
		return $iBlockId == $arFields['IBLOCK_ID'];
	}
}