<?php
class ProgramHelper {

	const IBLOCK_ID = 27;
	const DIRECTION_IBLOCK_ID = 30;
	const DEFAULT_SORT = 1000;

	private static $_ages = null;
	
	public static $types = array(
		'limit' => array(
			'label' => 'лим',
			'description' => 'Комплексная программа, содержащая фиксированный перечень медицинских услуг. Оказание дополнительных услуг за счет средств, оплаченных по программе  не возможно. При окончании срока действия программы остаток средств за неиспользованные услуги не возвращается. При досрочном прекращении  программы  возврат средств осуществляется с удержанием стоимости фактически оказанных услуг без скидки, предоставленной по программе (за исключением досрочного прекращения программы по  медицинским показаниям).'
		),
		'deposit' => array(
			'label' => 'деп',
			'description' 	=> 'Программа имеет рекомендованный перечень услуг. Перечень может быть изменен по назначению врача и/или волеизъявлению Пациента. Возможно оказание дополнительных услуг за счет средств, оплаченных по  программе. По окончанию срока действия  программы  остаток денежных средств возвращается.'
		),
	);

	public static function findByClinicId($clinicId) {
		global $CACHE_MANAGER;
		$cache = new CPHPCache;
		$lifeTime = 3600;
		$cacheId = 'programsOfClinic' . $clinicId;
		$cacheDir = '/clinic_programs';
		if ($cache->InitCache($lifeTime, $cacheId, $cacheDir)) {
			$data = $cache->GetVars();
			return $data['items'];
		}
		$arFilter = array('PROPERTY_CLINIC' => $clinicId);

		if ($cache->StartDataCache()) {

			$cacheManager = null;
			if (defined('BX_COMP_MANAGED_CACHE')) {
				$cacheManager = $CACHE_MANAGER;
				$cacheManager->StartTagCache($cacheDir);
			}
			$items = self::getFromDb($arFilter, $cacheManager);

            if ($cacheManager) {
            	$cacheManager->RegisterTag('iblock_id_new');
                $cacheManager->EndTagCache();
            }
			$cache->EndDataCache(array(
				'items' => $items,
			));
		} else {
			$items = array();
		}
		return $items;
	}

	public static function getFromDb($arFilter, $cacheManager = null) {
		$arFilter['IBLOCK_ID'] = self::IBLOCK_ID;
		$arFilter['ACTIVE'] = 'Y';
		$arFilter['ACTIVE_DATE'] = 'Y';
		$arSelect = array('ID', 
			'IBLOCK_ID', 
			'NAME', 
			'PREVIEW_TEXT', 
			'PREVIEW_PICTURE', 
			'DETAIL_PICTURE', 
            'DETAIL_TEXT', 
            'DETAIL_PAGE_URL', 
            'ACTIVE_FROM', 
            'ACTIVE_TO',
            'PROPERTY_CLINIC',
            'PROPERTY_DIRECTION',
            'PROPERTY_PRICE',
            'PROPERTY_AGE',
            'PROPERTY_TYPE',
        );
        $arClinics = array();
		$arDirections = array();
		$arItems = array();

        $dbResult = CIBlockElement::GetList(array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);

		$types = [];

		$result = CIBlockPropertyEnum::GetList(['SORT' => 'ASC'], ['IBLOCK_ID' => self::IBLOCK_ID, 'CODE' => 'TYPE']);
		while($type = $result->GetNext()) {
			$types[$type['ID']] = $type;
		}

        while($arItem = $dbResult->GetNext()) {
			if(isset($types[$arItem['PROPERTY_TYPE_ENUM_ID']])){
				$arItem['PROPERTY_TYPE_XML_ID'] = $types[$arItem['PROPERTY_TYPE_ENUM_ID']]['XML_ID'];
			}

        	$itemId = $arItem['ID'];
        	if ($cacheManager) {
        		$cacheManager->RegisterTag('iblock_id_' . $itemId);
        	}
        	$clinicId = $arItem['PROPERTY_CLINIC_VALUE'];
        	$directionId = $arItem['PROPERTY_DIRECTION_VALUE'];
        	if (empty($arItems[$itemId])) {
        		$arItem['DIRECTIONS'] = array();
        		$arItems[$itemId] = $arItem;
        	}

        	$arItems[$itemId]['DIRECTIONS'][$directionId] = $directionId;
        	$arDirections[$directionId] = $directionId;
        	$arClinics[$clinicId] = $clinicId;
        }

        return array(
        	'ITEMS' => $arItems,
        	'DIRECTIONS' => $arDirections,
        	'CLINICS' => $arClinics,
    	);
	}

	public static function groupByDirections($arResult) {
		$arItems = $arResult['ITEMS'];
		$directionIds = $arResult['DIRECTIONS'];
		$arDirections = self::getDirections($directionIds);
		$arGroups = array();
		foreach($arDirections as $arDirection) {
			$directionId = $arDirection['ID'];
			$arDirection['ITEMS'] = array();
			foreach($arItems as $arItem) {
				if (isset($arItem['DIRECTIONS'][$directionId])) {
					$arDirection['ITEMS'][$arItem['ID']] = $arItem;
				}
			}
			if (!empty($arDirection['ITEMS'])) {
				$arGroups[] = $arDirection;
			}
			
		}
		return $arGroups;
	}

	public static function getAges() {
		if (is_null(self::$_ages)) {
			$ages = [];
			$order = ['SORT' => 'ASC'];
			$filter = ['IBLOCK_ID' => self::IBLOCK_ID, 'CODE' => 'AGE'];
			$result = CIBlockPropertyEnum::GetList($order, $filter);
			while($age = $result->GetNext()) {
				$ages[] = $age;
			}
			self::$_ages = $ages;
		}
		return self::$_ages;
	}

	public static function groupByAges($arGroups) {
		$ages = self::getAges();

		$defaultValue = 'default';

		$ageGroups = [$defaultValue => [
			'NAME' => '',
			'ITEMS' => [],
		]];
		
		foreach($ages as $age) {
			$ageGroups[$age['VALUE']] = [
				'NAME' => $age['VALUE'],
				'ITEMS' => [], 
			];
		}

		foreach($arGroups as &$arGroup) {
			$subGroups = $ageGroups;

			foreach($arGroup['ITEMS'] as $arItem) {
				$value = $arItem['PROPERTY_AGE_VALUE'];

				if (is_null($value)) {
					$value = $defaultValue;
				}
				if (array_key_exists($value, $subGroups)) {
					$subGroups[$value]['ITEMS'][] = $arItem;
				}
			}
			$arGroup['AGES'] = $subGroups;
		}
		return $arGroups;
	}

	public static function getList($arGroups, $directionId = null) {
		$arList = array();
		$arSelectedGroups = array();
		foreach($arGroups as $arGroup) {
			if ($arGroup['ID'] == $directionId) {
				$arSelectedGroups[] = $arGroup;
			}
		}
		if (empty($arSelectedGroups)) {
			$arSelectedGroups = $arGroups;
		}
		foreach($arSelectedGroups as $arGroup) {
			foreach($arGroup['AGES'] as $ageGroup) {
				foreach($ageGroup['ITEMS'] as $arItem) {
					$arList[] = [
						'ITEM' => $arItem,
						'GROUP' => $arGroup,
						'AGE' => $ageGroup['NAME'],
					];
				}
			}
		}
		return $arList;
	}

	public static function getDirections(array $directionIds, $useCache = true) {
		global $CACHE_MANAGER;
		if (empty($directionIds)) {
			return array();
		}
		$lifeTime = 3600;
		$cacheId = 'programDirections' . implode(',', $directionIds);
		$cacheDir = '/';
		$cache = new CPHPCache;
		if ($useCache && $cache->InitCache($lifeTime, $cacheId)) {
			$data = $cache->GetVars();
			return $data['result'];
		}
		$arFilter = array(
			'ID' => $directionIds,
			'IBLOCK_ID' => self::DIRECTION_IBLOCK_ID,
		);
		$arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'SORT');
		$arResult = array();
		$dbResult = CIBlockElement::GetList(array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
		$CACHE_MANAGER->StartTagCache($cacheDir);
		while($arItem = $dbResult->GetNext()) {
			$itemId = $arItem['ID'];
			$CACHE_MANAGER->RegisterTag('iblock_id_' . $itemId);
			$arResult[$itemId] = $arItem;
		}
		$CACHE_MANAGER->RegisterTag('iblock_id_new');
		$CACHE_MANAGER->EndTagCache();
		if ($useCache && $cache->StartDataCache()) {
			$cache->EndDataCache(array(
				'result' => $arResult,
			));
		}
		return $arResult;
	}

	public static function applyPagination(array $arList, $perPage = 20, $paginationNumber = 1) {
		$paginationName = 'PAGEN_' . $paginationNumber;
		$totalCount = count($arList);
		$currentPage = isset($_REQUEST[$paginationName]) ? intval($_REQUEST[$paginationName]) - 1 : 0;
		if ($currentPage < 0) {
			$currentPage = 0;
		}
		$pageCount = ceil($totalCount / $perPage);
		$offset = $currentPage * $perPage;
		if ($offset > $totalCount) {
			$offset = 0;
			$currentPage = 0;
		}
		$arPaginatedList = array_slice($arList, $offset, $perPage);
		$pagination = new CDBResult();
		$pagination->NavPageCount = $pageCount;
		$pagination->NavPageNomer = $currentPage + 1;
		$pagination->NavNum = $paginationNumber;
		$pagination->NavPageSize = $perPage;
		$pagination->NavRecordCount = $totalCount;
		return array(
			'PAGINATION' => $pagination,
			'LIST' => $arPaginatedList,
		);
	}

	public static function getPropertyByCode($code) {
		$dbResult = CIBlockProperty::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => self::IBLOCK_ID, 'CODE' => $code));
		return $dbResult->GetNext();
	}

	public static function updateDirectionSort($directionId, $exclude = false) {
		$arFilter = array(
			'PROPERTY_DIRECTION.ID' => $directionId,
		);
		$arSelect = array('ID', 'IBLOCK_ID');
		$dbResult = CIBlockElement::GetList(array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
		$excludeDirectionIds = array();
		if ($exclude) {
			$excludeDirectionIds[] = $directionId;
		}
		while($arProgram = $dbResult->GetNext()) {
			self::updateSort($arProgram['ID'], $excludeDirectionIds);
		}

	}

	public static function updateSort($programId, array $excludeDirectionsIds = null) {
		$directionIds = self::getDirectionIds($programId);
		if (!empty($excludeDirectionsIds)) {
			$directionIds = array_diff($directionIds, $excludeDirectionsIds);
		}
		$arDirections = self::getDirections($directionIds, false);
		$sort = self::countSort($arDirections);
		CIBlockElement::SetPropertyValuesEx($programId, self::IBLOCK_ID, array('DIRECTION_SORT' => $sort));
	}

	public static function countSort($arDirections) {
		$sort = null;
		foreach($arDirections as $arDirection) {
			$directionSort = intval($arDirection['SORT']);
			if (is_null($sort) || $directionSort < $sort) {
				$sort = $directionSort;
			}
		}
		if (is_null($sort)) {
			$sort = self::DEFAULT_SORT;
		}
		return $sort;

	}

	public static function getDirectionIds($programId) {
		$arFilter = array(
			'CODE' => 'DIRECTION',
		);
		$dbResult = CIBlockElement::GetProperty(self::IBLOCK_ID, $programId, array('SORT' => 'ASC'), array('CODE' => 'DIRECTION'));
		$arResult = array();
		while($arItem = $dbResult->GetNext()) {
			$directionId = $arItem['VALUE'];
			if (empty($directionId)) {
				continue;
			}
			$arResult[$directionId] = $directionId;
		}
		return $arResult;
	}
}