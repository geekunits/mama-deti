<?php

class ForPressSection {

	private static $_cache = array();
	private $_arSection;
	private $_arClinic;
	private $_itemsCount;

	public static function factory($arSection) {
		$id = $arSection['ID'];
		if (!isset(self::$_cache[$id])) {
			self::$_cache[$id] = new static($arSection);
		}
		return self::$_cache[$id];
	}

	protected function __construct($arSection) {
		$this->_arSection = $arSection;
	}

	public function getClinicId() {
		return $this->_arSection['UF_CLINIC'];
	}

	public function getClinic() {
		return $this->_arClinic;
	}

	public function setClinic($arClinic) {
		$this->_arClinic = $arClinic;
		return $this;
	}

	public function getRaw() {
		return $this->_arSection;
	}

	public function getRawField($name, $default = null) {
		$raw = $this->getRaw();
		return isset($raw[$name]) ? $raw[$name] : $default;
	}

	public function getUrl($template = null) {
		$arSection = $this->getRaw();
		if (is_null($template)) {
			$template = $arSection['SECTION_PAGE_URL'];
		}
		$replace = array(
			'#SECTION_ID#' => $arSection['ID'],
			'#SECTION_CODE#' => $arSection['CODE'],
		);
		return str_replace(array_keys($replace), array_values($replace), $template);
		// return CIBlock::ReplaceDetailUrl($template, $arSection);
	}

	public function getItems($limit = null) {
		$arSection = $this->getRaw();
		$arFilter = array(
			'SECTION_ID' => $arSection['ID'],
		);
		$arOrder = array(
			'SORT' => 'ASC',
			'DATE_CREATE' => 'DESC',
		);
		$arNavStartParams = false;
		if (!is_null($limit)) {
			$arNavStartParams = array(
				'nTopCount' => $limit,
			);
		}
		$arSelect = array();
		return ForPressHelper::getElementsRaw($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
	}

	public function getItemsCount() {
		if (is_null($this->_itemsCount)) {
			$this->_itemsCount = ForPressHelper::getElementsCount($this->getRawField('ID'));
		}
		return $this->_itemsCount;
	}

}