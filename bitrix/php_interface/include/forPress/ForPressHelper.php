<?php

class ForPressHelper {

	private static $_sections = array();

	public static function getSections(array $arFilter = array()) {

		global $CACHE_MANAGER;
		$cache = new CPHPCache;
		$lifeTime = 3600;
		$cacheId = 'forPressSections' . serialize($arFilter);
		$cacheDir = '/';
		$sections = array();
		$arSections = array();

		if ($cache->InitCache($lifeTime, $cacheId, $cacheDir)) {
				$data = $cache->GetVars();
				$arSections = $data['arSections'];
		} else {
			$arFilter['IBLOCK_ID'] = FOR_PRESS_IBLOCK_ID;
			$arFilter['ACTIVE'] = 'Y';
			$arOrder = array('LEFT_MARGIN' => 'ASC');
			$arSelect = array('ID', 
				'IBLOCK_ID', 
				'NAME', 
				'CODE', 
				'PICTURE', 
				'DESCRIPTION', 
				'SECTION_PAGE_URL', 
				'DETAIL_PICTURE', 
				'UF_CLINIC',
				'UF_FILE',
				'UF_FILE_NAME',
			);
			
			if ($cache->StartDataCache()) {

				$CACHE_MANAGER->StartTagCache($cacheDir);

				$dbResult = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
				while ($arSection = $dbResult->Fetch()) {
					$CACHE_MANAGER->RegisterTag('iblock_id_' . $arSection['ID']);
					$arSections[] = $arSection;
				}

	        	$CACHE_MANAGER->RegisterTag('iblock_id_new');
	            $CACHE_MANAGER->EndTagCache();
				$cache->EndDataCache(array(
					'arSections' => $arSections,
				));
			}
		}
		$clinicIds = array();
		$arClinics = CMamaDetiAPI::GetClinics(array('ID' => $clinicIds));
		$arIndexedClinics = array();

		foreach($arSections as $arSection) {
			$clinicId = $arSection['UF_CLINIC'];
			$clinicIds[$clinicId] = $clinicId;
		}

		foreach($arClinics as $arClinic) {
			$arIndexedClinics[$arClinic['ID']] = $arClinic;
		}

		foreach($arSections as $arSection) {
			$section = ForPressSection::factory($arSection);
			$clinicId = $section->getClinicId();
			if (!empty($clinicId) && isset($arIndexedClinics[$clinicId])) {
				$section->setClinic($arIndexedClinics[$clinicId]);
			}
			$sections[] = $section;
		}

		return $sections;
	}

	public static function getElementsRaw($arOrder = array('SORT' => 'ASC'), $arFilter = array(), $arGroup = false, $arNavStartParams = false, $arSelect = array()) {

		$arFilter['IBLOCK_ID'] = FOR_PRESS_IBLOCK_ID;
		$arFilter['ACTIVE'] = 'Y';
		$arSelect = $arSelect + array(
			'ID', 
			'IBLOCK_ID', 
			'NAME', 
			'CODE', 
			'PREVIEW_PICTURE', 
			'DETAIL_PICTURE',
			'PREVIEW_TEXT',
			'DETAIL_TEXT',
		);

		global $CACHE_MANAGER;
		$cache = new CPHPCache;
		$lifeTime = 3600;
		$cacheId = 'forPressElements' . serialize(func_get_args());
		$cacheDir = '/';
		if ($cache->InitCache($lifeTime, $cacheId, $cacheDir)) {
			$data = $cache->GetVars();
			return $data['items'];
		}

		if ($cache->StartDataCache()) {

			$CACHE_MANAGER->StartTagCache($cacheDir);

			$dbResult = CIBlockElement::GetList($arOrder, $arFilter, $arGroup, $arNavStartParams, $arSelect);
			$arItems = array();
			while($arItem = $dbResult->GetNext()) {
				$arItems[] = $arItem;
				$CACHE_MANAGER->RegisterTag('iblock_id_' . $arItem['ID']);
			}

        	$CACHE_MANAGER->RegisterTag('iblock_id_new');
            $CACHE_MANAGER->EndTagCache();

			$cache->EndDataCache(array(
				'items' => $arItems,
			));

		} else {
			$arItems = array();
		}
		return $arItems;
	}

	public static function getElementsCount($sectionId) {
		$arFilter['IBLOCK_ID'] = FOR_PRESS_IBLOCK_ID;
		$arFilter['SECTION_ID'] = $sectionId;
		$arFilter['ACTIVE'] = 'Y';
		return CIBlockElement::GetList(array(), $arFilter, array());
	}

	public static function getElementProperties($elementId) {
		$dbResult = CIBlockElement::GetProperty(FOR_PRESS_IBLOCK_ID, $elementId);
		$arProperties = array();
		while($arProperty = $dbResult->GetNext()) {
			$code = $arProperty['CODE'];
			if ($arProperty['MULTIPLE'] === 'Y') {
				if (empty($arProperties[$code])) {
					$arProperties[$code] = array();
				}
				$arProperties[$code][] = $arProperty;
			} else {
				$arProperties[$code] = $arProperty;
			}
		}
		return $arProperties;
	}
}