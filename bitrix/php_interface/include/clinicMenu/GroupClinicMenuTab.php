<?php

class GroupClinicMenuTab extends ClinicMenuTab {

	protected $tabs = [];

	protected function checkActive() {
		foreach($this->getTabs() as $tab) {
			if ($tab->isActive()) {
				return true;
			}
		}
		return parent::checkActive();
	}
	public function isCurrent() {
		foreach($this->getTabs() as $tab) {
			if ($tab->isCurrent()) {
				return true;
			}
		}
		return parent::isCurrent();
	}

	public function getTabs() {
		return $this->tabs;
	}

	public function addTab(ClinicMenuTab $tab) {
		$name = $tab->name;
		$this->tabs[$name] = $tab;
		return $this;
	}
}