<?php

class PropertyClinicMenuRenderer extends ClinicMenuRenderer {

	public $property;

	public function getProperty() {
		if (is_null($this->property)) {
			$this->property = strtoupper($this->getTab()->getName());
		}
		return $this->property;
	}

	public function hasContent() {
		return !empty($this->getPropertyValue());
	}

	public function getPropertyValue() {
		$clinic = $this->getClinic();
		return html_entity_decode($clinic['PROPERTIES'][$this->getProperty()]['VALUE']['TEXT']);
	}

    public function render(array $params = array()) {
        echo $this->getPropertyValue();
    }

}