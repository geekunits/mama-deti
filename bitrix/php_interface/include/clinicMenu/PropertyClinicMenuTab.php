<?php

class PropertyClinicMenuTab extends ClinicMenuTab {
    protected $property;

    public function __construct($clinic, $name, array $tabOptions = array()) {
        if (empty($tabOptions['property'])) {
            $tabOptions['property'] = mb_strtoupper($name);
        }
        parent::__construct($clinic, $name, $tabOptions);
    }

    public function getPropertyValue() {
        $property = $this->getProperty();
        $clinic = $this->getClinic();
        return $clinic["PROPERTIES"][$property]["VALUE"]["TEXT"];
    }

    public function getProperty() {
        return $this->property;
    }

    public function isActive() {
        $propertyValue = $this->getPropertyValue();
        return !empty($propertyValue);
    }

    public function renderContent(array $params = array()) {
        echo html_entity_decode($this->getPropertyValue());
    }

}