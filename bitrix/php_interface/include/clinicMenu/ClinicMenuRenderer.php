<?php

abstract class ClinicMenuRenderer {

	protected $tab;

	public function __construct(array $config) {
		foreach($config as $property => $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
		}
	}

	public function setTab(ClinicMenuTab $tab) {
		$this->tab = $tab;
		return $this;
	}

	public function getClinic() {
		return $this->getTab()->getClinic();
	}

	public function getTab() {
		return $this->tab;
	}

	abstract public function hasContent();
	abstract public function render(array $params = []);
}