<?php

class ServiceClinicMenuTab extends GroupClinicMenuTab {

	public function getTabsOptions() {
		$options = [];
		$clinic = $this->getClinic();
		$services = $this->fetchServices();
		foreach($services as $service) {
			$name = $service['NAME'];
			$options[$name] = [
				'title' => $name,
				'class' => 'ClinicMenuTab',
				'rewriteUrl' => $this->buildTabUrl($clinic, $service),
				'ajaxTab' => false,
				'isActiveCallback' => function() {
					return true;
				},
			];
		}
		return $options;
	}



	protected function fetchServices() {
		$clinic = $this->getClinic();
		$services = [];
		foreach($clinic['SERVICES_MENU'] as $service) {
			if (((int) $service['DEPTH_LEVEL']) !== 1) {
				continue;
			}
			$services[] = [
				'NAME' => $service['TEXT'],
				'URL' => $service['LINK'],
			]; 
		}
		return $services;
	}

	protected function buildTabUrl($clinic, $service) {
		return rtrim($service['URL'], '/') . '/clinic/' . $clinic['CODE'] . '/';
	}

}