<?php

class FileClinicMenuRenderer extends ClinicMenuRenderer {

    public $file;
    public $directory = '/includes/clinics/content/';

    public function getFile() {
    	if (empty($this->file)) {
    		$this->file = $this->getTab()->getName();
    	}
    	return $this->file;
    }

    public function getPath() {
        return $this->directory . $this->getFile() . '.php';
    }

    public function hasContent() {
    	return file_exists($_DOCUMENT['SERVER_ROOT'] . $this->getPath());
    }

    public function render(array $params = array()) {
        global $APPLICATION;
        $APPLICATION->IncludeFile($this->getPath(), $params);
    }

}