<?php

class FileClinicMenuTab extends ClinicMenuTab {
    protected $file;
    protected $directory = '/includes/clinics/content/';

    public function __construct($clinic, $name, array $tabOptions = array()) {
        if (empty($tabOptions['file'])) {
            $tabOptions['file'] = $name;
        }
        parent::__construct($clinic, $name, $tabOptions);
    }

    public function getPath() {
        return $this->directory . $this->file . '.php';
    }

    public function renderContent(array $params = array()) {
        global $APPLICATION;
        $APPLICATION->IncludeFile($this->getPath(), $params);
    }
}