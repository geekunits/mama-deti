<?php

class ClinicMenuTab {
    protected $menu;
    protected $name;
    protected $title;
    protected $ajaxTab = true;
    protected $cssClasses = [];
    protected $rewriteUrl;
    protected $isActiveCallback;
    protected $renderer;
    protected $active;
    protected $showTitle = true;

    public function __construct(ClinicMenu $menu, $name, array $attributes = []) {
        $this->menu = $menu;
        $this->name = $name;

        foreach($attributes as $attribute => $value) {
            $this->$attribute = $value;
        }
    }

    protected function buildLink(array $cssClasses, $rewriteTitle = null) {
        $url = $this->getUrl();
        $title = empty($rewriteTitle) ? $this->title : $rewriteTitle;
        $attributes = [
            'class' => implode(' ' , $cssClasses),
        ];
        if (!$this->isLink()) {
            $tag = 'span';
        } else {
            $tag = 'a';
            $attributes['href'] = $url;
        }
        $buildAttributes = [];
        foreach($attributes as $name => $value) {
            $buildAttributes[] = $name . '="' . $value . '"';
        }
        $html = '<' . $tag;

        if (!empty($buildAttributes)) {
            $html .= ' ' . implode(' ' , $buildAttributes);
        }
        $html .= '>' . $title . '</' . $tag . '>';
        return $html;
    }

    public function isLink() {
        return !empty($this->rewriteUrl) || $this->hasRenderer();
    }

    public function hasRenderer() {
        return !empty($this->getRenderer());
    }

    public function hasContent() {
        $hasContent = $this->hasRenderer() && $this->getRenderer()->hasContent();

        return $hasContent;
    }

    public function getUrl() {
        if (!is_null($this->rewriteUrl)) {
            return $this->rewriteUrl;
        } 
        if (!$this->isActive()) {
            return null;
        }
        return rtrim($this->getClinic()['DETAIL_PAGE_URL'], '/') . '/' . $this->name . '/';
    }

    public function getClinic() {
        return $this->getMenu()->getClinic();
    }

    public function getName() {
        return $this->name;
    }

    public function getTitle() {
        return $this->title;
    }

    protected function checkActive() {
         if (!empty($this->isActiveCallback) && is_callable($this->isActiveCallback)) {
            return call_user_func($this->isActiveCallback, $this->getClinic(), $this);
        }
        return $this->hasContent();
    }

    public function isActive() {
        if (is_null($this->active)) {
            $this->active = $this->checkActive();
        }
        return $this->active;
    }

    public function setRenderer(ClinicMenuRenderer $renderer) {
        $this->renderer = $renderer;
        $renderer->setTab($this);
        return $this;
    }

    public function getRenderer() {
        return $this->renderer;
    }

    public function setMenu(ClinicMenu $menu) {
        $this->menu = $menu;
        return $this;
    }

    public function getMenu() {
        return $this->menu;
    }

    public function addTab(ClinicMenuTab $tab) {
        throw new Exception('Cannot add tab.');
    }

    public function getTabs() {
        return [];
    }

    public function isCurrent() {
        return $this->getMenu()->getCurrentTabName() === $this->getName();
    }

    public function renderTab() {

        $isCurrent = $this->isCurrent();

        $defaultCssClasses = [
            'b-clinics_tab__link'
        ];
        $cssClasses = array_merge($this->cssClasses, $defaultCssClasses);
        if ($isCurrent) {
            $cssClasses[] = 'b-clinics_tab__link-active';
            $cssClasses[] = 'active';
        }
        if ($this->ajaxTab && $this->isLink()) {
            $cssClasses[] = 'js-tab';
        }
        
        echo $this->buildLink($cssClasses);
    }

    public function renderTitle() {
        echo '<h2 class="clinic-subtitle">' . $this->title . '</h2>';
    }

    public function renderContent(array $params = array()) {
        $renderer = $this->getRenderer();
        if (!empty($renderer)) {
            if ($this->showTitle) {
                $this->renderTitle();
            }
            $renderer->render($params);
        }
    }
}