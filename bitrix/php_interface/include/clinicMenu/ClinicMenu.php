<?php

class ClinicMenu {

    protected $mainTabsCount = 6;
    protected $detectCurrentTab = true;
    public $tabs = [];
    public $groups;
    protected $clinic;
    protected $currentTabName;
    protected $useAjax = true;
    
    public function __construct($clinic, array $attributes = array()) {
        global $APPLICATION;

        $APPLICATION->SetPageProperty('CLINIC', $clinic);

        $this->clinic = $clinic;

        foreach($attributes as $attribute => $value) {
            $this->$attribute = $value;
        }
        $this->groups = $this->buildTabs($this->getTabOptions());

        PageManager::getInstance()->addVar('CURRENT_CLINIC', (int) $clinic['ID']);

    }

    protected function buildTabs($tabOptions) {
        $tabs = [];

        foreach($tabOptions as $name => $options) {
            $class = $options['class'];

            $tabsCallback = null;

            $subTabs = [];
            
            $renderer = !empty($options['renderer']) ? $this->buildRenderer($options['renderer']) : null;
        
            if (isset($options['tabs'])) {
                $subTabs = array_merge($subTabs, $this->buildTabs($options['tabs']));
            } 

            if (isset($options['tabsCallback'])) {
                $tabsCallback = $options['tabsCallback'];
            }
          
            unset($options['class'], $options['tabs'], $options['renderer'], $options['cabsCallback']);

            if (!$this->useAjax) {
                $options['ajaxTab'] = false;
            }

            $tab = new $class($this, $name, $options);

            if (!empty($tabsCallback)) {
                $subTabOptions = call_user_func([$tab, $tabsCallback]);
                $subTabs = array_merge($subTabs, $this->buildTabs($subTabOptions));
            }

            if (!empty($renderer)) {
                $tab->setRenderer($renderer);
            }

            foreach($subTabs as $subTab) {
                $tab->addTab($subTab);
            }

            $this->tabs[$name] = $tab;
            $tabs[$name] = $tab;
        }

        return $tabs;
    }

    protected function buildRenderer($options) {
        if (!is_array($options)) {
            $options = ['class' => $options];
        }
        $class = $options['class'];
        unset($options['class']);
        return new $class($options);

    }

    public function getClinic() {
        return $this->clinic;
    }


    protected function getTabOptions() {
        $clinic = $this->getClinic();
   //     echo '<pre>';

                	//var_dump($clinic["PROPERTIES"]["SERVICES"]);
     //   echo '</pre>';
        // $isDefault = CityManager::getInstance()->isCurrentCityDefault();

        $doctorsHeader = 'Врачи клиники';
        $actionsHeader = 'Акции';
        $servicesHeader = 'Услуги';

        // if ($isDefault) {
        //     $doctorsHeader .= ' клиники';
        //     $actionsHeader .= ' клиники';
        //     $servicesHeader .= ' в клинике';
        // }
        return [

            'about' => [
                'title' => 'О нас',
                'class' => 'GroupClinicMenuTab',
                'tabs' => [
                    'description' => [
                        'title' => 'Основное',
                        'class' => 'ClinicMenuTab',
                        'rewriteUrl' => $clinic['DETAIL_PAGE_URL'],
                        'renderer' => 'FileClinicMenuRenderer',
                        'showTitle' => false,
                        'isActiveCallback' => function($clinic, $tab) {
                            return true;
                        },
                    ],
                    'reviews' => [
                        'title' => 'Отзывы',
                        'class' => 'ClinicMenuTab',
                        'isActiveCallback' => function($clinic, $tab) {
                            return count($clinic["REVIEWS"]) > 0;
                        },
                        'ajaxTab' => false,
                        'rewriteUrl' => '/reviews/?clinic=' . $clinic['ID'],
                    ],
                    'news' => [
                        'title' => 'Новости',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'FileClinicMenuRenderer',
                        'isActiveCallback' => function($clinic, $tab) {
		                    $dbResult = CIBlockElement::GetList(
		                        array('SORT' => 'ASC'),
		                        array(
		                            'IBLOCK_ID' => 8,
		                            'PROPERTY_CLINIC' => $clinic['ID'],
		                            'ACTIVE' => 'Y',
		                            'ACTIVE_DATE' => 'Y',
		                        ),
		                        array(),
		                        false,
		                        array('ID', 'IBLOCK_ID')
		                    );
		                    return (int) $dbResult['CNT'] > 0;
                        },
                    ],
                    'contacts' => [
                        'title' => 'Контакты',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'FileClinicMenuRenderer',
                        'isActiveCallback' => function($clinic, $tab) {
                            return !empty($clinic["PROPERTIES"]["CONTACTS"]["VALUE"]["TEXT"]) || !empty($clinic["PROPERTIES"]["COORD"]["VALUE"]);
                        },
                    ],
                    'license' => [
                        'title' => 'Лицензии',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'FileClinicMenuRenderer',
                        'isActiveCallback' => function($clinic, $tab) {
                            return is_array($clinic["PROPERTIES"]["LICENSE"]["VALUE"]) && !empty($clinic["PROPERTIES"]["LICENSE"]["VALUE"]);
                        },
                    ],
                    'leading' => [
                        'title' => 'Руководство',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'corporate' => [
                        'title' => 'Для корпоративных клиентов',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'partners' => [
                        'title' => 'Партнеры',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                ],
            ],
            'services' => [
                'title' => $servicesHeader,
                'class' => 'ServiceClinicMenuTab',
                'renderer' => 'FileClinicMenuRenderer',
                'tabsCallback' => 'getTabsOptions',
                'isActiveCallback' => function($clinic, $tab) {
                    return !empty($clinic["PROPERTIES"]["SERVICES"]["VALUE"]);
                },
            ],
            'actions' => [
                'title' => $actionsHeader,
                'class' => 'ClinicMenuTab',
                'renderer' => 'FileClinicMenuRenderer',
                'isActiveCallback' => function($clinic, $tab) {
                    $dbResult = CIBlockElement::GetList(
                        array('SORT' => 'ASC'),
                        array(
                            'IBLOCK_ID' => 32,
                            'PROPERTY_CLINIC' => $clinic['ID'],
                            'ACTIVE' => 'Y',
                            'ACTIVE_DATE' => 'Y',
                        ),
                        array(),
                        false,
                        array('ID', 'IBLOCK_ID')
                    );
                    return (int) $dbResult['CNT'] > 0;
                },
            ],
            'birth' => [
                'title' => 'Роды',
                'class' => 'ClinicMenuTab',
                'renderer' => [
                    'class' => 'PropertyClinicMenuRenderer',
                    'property' => 'BIRTH',
                ],
                'cssClasses' => array('b-clinics_tab__link-selected'),
            ],
            'pregnancy' => [
                'title' => 'Беременность',
                'class' => 'ClinicMenuTab',
                'renderer' => 'PropertyClinicMenuRenderer',
                'cssClasses' => array('b-clinics_tab__link-selected'),
            ],

            'doctors' => [
                'title' => $doctorsHeader,
                'class' => 'ClinicMenuTab',
                'renderer' => 'FileClinicMenuRenderer',
                'isActiveCallback' => function($clinic, $tab) {
                   return $clinic['DOCTOR_CNT'] > 0;
                },
            ],

            'patients' => [
                'title' => 'Пациентам',
                'class' => 'GroupClinicMenuTab',
                'tabs' => [

                    'programs' => [
                        'title' => 'Программы',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'FileClinicMenuRenderer',
                        'isActiveCallback' => function($clinic, $tab) {
                            return !empty($clinic['PROGRAMS']);
                        },
                    ],
                    'skypeconsultation' => [
                        'title' => 'Консультации по Skype',
                        'class' => 'ClinicMenuTab',
                        'renderer' => [
                            'class' => 'PropertyClinicMenuRenderer',
                            'property' => 'SKYPE_CONSULTATION',
                        ],
                    ],
                    'webinars' => [
                        'title' => 'Вебинары',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'tour' => [
                        'title' => 'Экскурсия',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'accompanying' => [
                        'title' => 'Размещение сопровождающих',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'meeting' => [
                        'title' => 'Популярная педиатрия',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],


                    'doctorofmonth' => [
                        'title' => 'Врачи месяца',
                        'class' => 'ClinicMenuTab',
                        'renderer' => [
                            'class' => 'PropertyClinicMenuRenderer',
                            'property' => 'DOCTOR_OF_MONTH',
                        ],
                    ],
                    'articles' => [
                        'title' => 'Статьи',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'FileClinicMenuRenderer',
                        'isActiveCallback' => function($clinic, $tab) {
                            $doctorIds = CMamaDetiAPI::GetDoctorsID([
                                'PROPERTY_CLINIC.ID' => $clinic['ID'],
                            ]);

                            $sort = ['SORT' => 'ASC'];

                            $filter = [
                                'IBLOCK_ID' => ARTICLE_IBLOCK_ID,
                                'ACTIVE' => 'Y',
                                'ACTIVE_DATE' => 'Y',
                            ];

                            if(!empty($doctorIds)){
                                $arrArticlesFilter[] = array(
                                    'LOGIC' => 'OR',
                                    array('PROPERTY_CLINIC.ID' => $clinic['ID']),
                                    array('PROPERTY_DOCTOR.ID' => $doctorIds),
                                );
                            }else{
                                $arrArticlesFilter['PROPERTY_CLINIC.ID'] = $clinic['ID'];
                            }

                            $count = (int) CIBlockElement::GetList($sort, $filter, []);

                            return $count > 0;
                        }
                    ],
                    'health_security' => [
                        'title' => 'Контакты органов исполнительной власти в сфере охраны здоровья граждан',
                        'class' => 'ClinicMenuTab',
                        'renderer' => [
                            'class' => 'PropertyClinicMenuRenderer',
                            'property' => 'HEALTH_PROTECTION_CONTACTS',
                        ],
                    ],
                    'prices' => [
                        'title' => 'Цены',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'rules' => [
                        'title' => 'Памятки пациенту',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'information' => [
                        'title' => 'Персональные данные',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'law' => [
                        'title' => 'Законодательство',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'for-press' => [
                        'title' => 'Для прессы',
                        'class' => 'ClinicMenuTab',
                        'renderer' => [
                            'class' => 'FileClinicMenuRenderer',
                            'file' => 'forPress',
                        ],
                        'isActiveCallback' => function($clinic, $tab) {
                            return !empty($clinic['FOR_PRESS']) && $clinic['FOR_PRESS']->getItemsCount() > 0;
                        }
                    ],
                    'quality_control' => [
                        'title' => 'Независимая оценка качества',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                ],
            ],

            'multimedia' => [
                'title' => 'Мультимедиа',
                'class' => 'GroupClinicMenuTab',
                'tabs' => [
                    'photos' => [
                        'class' => 'ClinicMenuTab',
                        'title' => 'Фото',
                        'rewriteUrl' => '/gallery/?clinic=' . $clinic['ID'],
                        'ajaxTab' => false,
                        'isActiveCallback' => function($clinic, $tab) {
                            $sort = ['SORT' => 'ASC'];
                            $filter = [
                                'IBLOCK_ID' => 13,
                                'ACTIVE' => 'Y',
                                'ACTIVE_DATE' => 'Y',
                                'PROPERTY_CLINIC' => $clinic['ID'],
                            ];

                            $count = (int) CIBlockElement::GetList($sort, $filter, []);

                            return $count > 0;
                        },
                    ],
                    'videos' => [
                        'title' => 'Видео',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'FileClinicMenuRenderer',
                        'isActiveCallback' => function($clinic, $tab) {
                            return !empty($clinic['PROPERTIES']['VIDEO']['VALUE']);
                        }
                    ],
                    'virtualtour' => [
                        'title' => 'Виртуальный тур',
                        'class' => 'ClinicMenuTab',
                        'renderer' => [
                            'class' => 'PropertyClinicMenuRenderer',
                            'property' => 'VIRTUAL_TOUR',
                        ],
                    ],                    
                ],
            ],

            'pricelist' => [
                'title' => 'Цены',
                'class' => 'ClinicMenuTab',
                'renderer' => [
                    'class' => 'FileClinicMenuRenderer',
                    'file' => 'priceList',
                ],
                'isActiveCallback' => function($clinic, $tab) {
                    return is_array(CIBlockSection::GetList([], [
                        'IBLOCK_ID' => 31,
                        'UF_CLINIC' => $clinic['ID'],
                        'ACTIVE' => 'Y',
                    ])->Fetch());
                }
            ],

            'omsall' => [
                'title' => 'ОМС',
                'class' => 'GroupClinicMenuTab',
                'tabs' => [
                    'oms' => [
                        'title' => 'ЭКО по ОМС',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'oms_obstetrics' => [
                        'title' => !empty($clinic["PROPERTIES"]["OMS_OBSTETRICS"]["DESCRIPTION"]) ? $clinic["PROPERTIES"]["OMS_OBSTETRICS"]["DESCRIPTION"] : 'Акушерство и гинекология по ОМС',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'oms_mrt' => [
                        'title' => 'МРТ по ОМС',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                    'oms_surgery' => [
                        'title' => !empty($clinic["PROPERTIES"]["OMS_SURGERY"]["DESCRIPTION"]) ? $clinic["PROPERTIES"]["OMS_SURGERY"]["DESCRIPTION"] : 'Хирургия по ОМС',
                        'class' => 'ClinicMenuTab',
                        'renderer' => 'PropertyClinicMenuRenderer',
                    ],
                ],            
            ],
         
        ];
    }
    public function getGroups() {
        return $this->groups;
    }

    public function getActiveGroups() {
        $activeGroups = [];
        foreach($this->getGroups() as $group) {
            if ($group->isActive()) {
                $activeGroups[] = $group;
            }
        }
        return $activeGroups;
    }

    public function getActiveTabs() {
        return $this->filterActiveTabs($this->getTabs());
    }

    public function getTabs() {
        return $this->tabs;
    }

    public function getGroup($name) {
        return isset($this->groups[$name]) ? $this->groups[$name] : null;
    }

    public function getTab($name) {
        return isset($this->tabs[$name]) ? $this->tabs[$name] : null;
    }

    public function getActiveTab($name) {
        foreach($this->getActiveTabs() as $tab) {
            if ($tab->getName() === $name) {
                return $tab;
            }
        } 
        return null;
    }

    public function setCurrentTabName($name) {
        $this->currentTabName = $name;
    }

    public function detectCurrentTabName() {
        $activeTabs = $this->getActiveTabs();
        $requestName = strtolower($_REQUEST['TAB_NAME']);
        if (empty($requestName)) {
            if (count($activeTabs) > 0) {
                $tab = reset($activeTabs);
                $this->currnetTabName = $tab->getName();
            } else {
                $this->currentTabName = false;
            }
        } else {
            $requestTab = $this->getActiveTab($requestName);
            $this->currentTabName = empty($requestTab) ? false : $requestTab->getName();
        }        
    }

    public function getCurrentTabName() {
        if (is_null($this->currentTabName) && $this->detectCurrentTab) {
            $this->detectCurrentTabName();
        }
        if ($this->currentTabName === false) {
            return null;
        }
        return $this->currentTabName;

    }

    public function getCurrentTab() {
        $currentTabName = $this->getCurrentTabName();
        if (empty($currentTabName)) {
            return null;
        }
        return $this->getActiveTab($currentTabName);
    }

    public function getContent() {
        ob_start();
        $this->render();
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    protected function filterActiveTabs($tabs) {
        return array_filter($tabs, function($tab) {
            return $tab->isActive();
        });
    }

    public function render() {
        $groups = $this->getActiveGroups();
        $this->renderBeginWrapper();
        foreach($groups as $group) {
            $tabs = $this->filterActiveTabs($group->getTabs());

            $this->renderBeginGroup();

            $group->renderTab();

            if (!empty($tabs)) {
                $this->renderBeginSubMenu();
                foreach($tabs as $tab) {
                    $tab->renderTab();
                }
                $this->renderEndSubMenu();
            }

            $this->renderEndGroup();
        }
        $this->renderEndWrapper();
        return $this;
    }

    protected function renderBeginWrapper() {
        $html = '<div class="b-clinics_tabs__wrap"><div class="b-clinics_tab"';
        if ($this->useAjax) {
            $html .= 'data-ajax-zone="clinic-menu"';
        }
        $html .= '>';
        echo $html;
    }

    protected function renderEndWrapper() {
        echo '</div></div>';
    }

    protected function renderBeginSubMenu() {
        echo '<div class="b-clinics_tab__sub">';
    }

    protected function renderEndSubMenu() {
        echo '</div>';
    }

    protected function renderBeginGroup() {
        echo '<div class="b-clinics_tab__cell"><div class="b-clinics_tab__box">';
    }

    protected function renderEndGroup() {
        echo '</div></div>';
    }

    protected function renderAdditionalTabHeader() {
        echo '<span class="b-clinics_tab__link">Дополнительная информация...</span>';
    }

    // public function render() {
    //     $activeTabs = $this->getActiveTabs();
    //     $mainTabs = [];
    //     $additionalTabs = [];
    //     $count = 0;
    //     $headerAdditionalTab = null;
    //     foreach($activeTabs as $tab) {
    //         if ($count < $this->mainTabsCount) {
    //             $mainTabs[] = $tab;
    //             $count++;
    //         } else {
    //             if ($tab->getName() === $this->getCurrentTabName()) {
    //                 $headerAdditionalTab = $tab;
    //             } else {
    //                 $additionalTabs[] = $tab;
    //             }
                
    //         }
    //     }
    //     $this->beginMainWrapper();
    //     foreach($mainTabs as $mainTab) {
    //         $this->beginMainTabWrapper();
    //         $mainTab->renderMainItem($mainTab->getName() === $this->getCurrentTabName());
    //         $this->endMainTabWrapper();
    //     }
    //     if (empty($headerAdditionalTab) && count($additionalTabs) === 1) {
    //         $headerAdditionalTab = array_pop($additionalTabs);
    //     }
    //     if (!empty($headerAdditionalTab) || count($additionalTabs) > 0) {
    //         $this->beginMainTabWrapper();
    //         if (!empty($headerAdditionalTab)) {
    //             $headerAdditionalTab->renderMainItem($headerAdditionalTab->getName() === $this->getCurrentTabName(), $headerAdditionalTab->getTitle() . '...');
    //         } else {
    //             $this->renderAdditionalTabHeader();
    //         }
    //         if (!empty($additionalTabs)) {
    //             $this->beginAdditionalWrapper();
    //             foreach($additionalTabs as $additionalTab) {
    //                 $additionalTab->renderAdditionalItem();
    //             }
    //             $this->endAdditionalWrapper();
    //         }
    //         $this->endMainTabWrapper();
    //     }

    //     $this->endMainWrapper();
    // }

    public function getTitleContent() {
        return '<div class="b-header_h1 align-center name-clinic-nocontent">' . $this->clinic['NAME'] . '</div>';
    }

    public function renderTitle() {
        echo $this->getTitleContent();
    }



}
