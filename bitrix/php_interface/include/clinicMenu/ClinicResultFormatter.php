<?php

class ClinicResultFormatter {
	protected $clinic;
	protected $clinicId;
	protected $properties;
	protected $programs;
	protected $reviews;
	protected $doctorsCount;
	protected $skypeConsultations;
	protected $forPress;
	protected $propertyCodes = [
		'CITY',
		'SERVICES',
		'BIRTH',
		'PREGNANCY',
		'ACTIONS',
		'VIDEOS',
		'REVIEWS',
		'PROGRAMS',
		'MEETING',
		'WEBINARS',
		'TOUR',
		'ACCMPANYING',
		'RULES',
		'CONTACTS',
		'LICENSE',
		'INFORMATION',
		'LAW',
		'DOCTOROFMONTH',
		'OMS',
		'LEADING',
		'CORPORATE',
	];

	public function __construct($clinic) {
		if (is_array($clinic)) {
			$this->clinic = $clinic;
			$this->clinicId = $clinic['ID'];
		} else {
			$this->clinicId = $clinic;
			$this->clinic = CIBlockElement::GetByID($clinic)->GetNext();
		}
	}

	public function getProperties() {
		if (is_null($this->properties)) {
			$arFilter = [
				'ACTIVE' => 'Y',
			];
			if (!empty($this->propertyCodes)) {
				$arFilter['CODE'] = $this->propertyCode;
			}
			$dbProperties = CIBlockElement::GetProperty(CLINIC_IBLOCK_ID, $this->clinicId, [
					'SORT' => 'ASC',
			], $arFilter);
			$properties = [];
			while($property = $dbProperties->GetNext()) {
				$propertyCode = $property['CODE'];
				if (isset($properties[$propertyCode])) {
					if (!is_array($properties[$propertyCode]['VALUE'])) {
						$properties[$propertyCode]['VALUE'] = [$properties[$propertyCode]['VALUE']];
					}
					$properties[$propertyCode]['VALUE'][] = $property['VALUE'];
				} else {
					$properties[$propertyCode] = $property;
				}
			}
			$this->properties = $properties;
		}

		return $this->properties;
	}

	public function getPrograms() {
		if (is_null($this->programs)) {
			$programs = [];
			$dbPrograms = CIBlockElement::GetList(
			    array('SORT' => 'ASC'),
			    array(
			        'IBLOCK_ID' => PROGRAM_IBLOCK_ID,
			        'PROPERTY_CLINIC' => $this->clinicId,
			        'ACTIVE' => 'Y',
			    ),
			    false,
			    false,
			    array(
			        'IBLOCK_ID',
			        'ID',
			        'PROPERTY_SERVICES',
			        'PROPERTY_CLINIC',
			    )
			);
			while($program = $dbPrograms->GetNext()) {
				$programs[] = $program;
			}
			$this->programs = $programs;
		}


		return $this->programs;
	}

	public function getReviews() {
		if (is_null($this->reviews)) {
			$arFilter = [
				'IBLOCK_ID' => REVIEW_IBLOCK_ID, 
				'ACTIVE' => 'Y', 
				'PROPERTY_CLINIC' => $this->clinicId
			];
			$dbReviews = CIBlockElement::GetList(
				['SORT' => 'ASC', 'NAME' => 'ASC'], 
				$arFilter, 
				false, 
				['nTopCount' => 6], 
				['PREVIEW_TEXT', 'DETAIL_PAGE_URL']
			);
			$this->reviews = [];
			while ($review = $dbReviews->GetNext()) {
			    $this->reviews[] = $review;
			}
		}
		return $this->reviews;
	}

	public function getDoctorsCount() {
		if (is_null($this->doctorsCount)) {
			$this->doctorsCount = (int) CIBlockElement::GetList(
				['SORT' => 'ASC'],
				[
					'IBLOCK_ID' => DOCTOR_IBLOCK_ID,
					'PROPERTY_CLINIC' => $this->clinicId,
					'ACTIVE' => 'Y',
				],
				[],
				false,
				['ID', 'IBLOCK_ID']
			);
		}
		return $this->doctorsCount;
	}

	public function getSkypeConsultations() {
		if (is_null($this->skypeConsultations)) {
			$this->skypeConsultations = SkypeConsultationFinder::findByClinicId($this->clinic['PROPERTY_CITY_VALUE'], $this->clinicId);
		}
		return $this->skypeConsultations;
	}

	public function getForPress() {
		if (is_null($this->forPress)) {
			$sections = ForPressHelper::getSections(['UF_CLINIC' => $this->clinicId]);
			$this->forPress = empty($sections) ? false : $sections[0];
		}
		return $this->forPress;
	}

	public function getClinic() {
		return $this->clinic;
	}

	public function format() {
		$clinic = $this->getClinic();
		if (empty($clinic)) {
			return null;
		}
		if (!isset($clinic['PROPERTIES'])) {
			$clinic['PROPERTIES'] = $this->getProperties();
		}
		if (!isset($clinic['PROGRAMS'])) {
			$clinic['PROGRAMS'] = $this->getPrograms();
		}
		if (!isset($clinic['REVIEWS'])) {
			$clinic['REVIEWS'] = $this->getReviews();
		}
		if (!isset($clinic['DOCTOR_CNT'])) {
			$clinic['DOCTOR_CNT'] = $this->getDoctorsCount();
		}
		if (!isset($clinic['SKYPE_CONSULTATIONS'])) {
			$clinic['SKYPE_CONSULTATIONS'] = $this->getSkypeConsultations();
		}
		if (!isset($clinic['FOR_PRESS'])) {
			$clinic['FOR_PRESS'] = $this->getForPress();
		}
		if (!isset($clinic['SERVICES_MENU'])) {
			if (is_array($clinic["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($clinic["PROPERTIES"]["SERVICES"]["VALUE"])) {
    			$clinic["SERVICES_MENU"] = CMamaDetiAPI::GetSelectedServiceTreeMenu($clinic["PROPERTIES"]["SERVICES"]["VALUE"]);
			} else {
				$clinic["SERVICES_MENU"] = [];
			}
		}
		$this->clinic = $clinic;
		return $clinic;
	}
}