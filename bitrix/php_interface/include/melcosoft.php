<?php

class CMelcosoft
{
    static $arQAElements = array();

    public function OnAdminListDisplayHandler(&$lAdmin)
    {
        if ($lAdmin->table_id != 'tbl_iblock_list_a00c73b1b8958d09b9e61304a221df4b')
            return;

        $arHeaderReplace = array(
            "PREVIEW_TEXT" => "Вопрос",
            "DETAIL_TEXT" => "Ответ",
        );

        foreach ($lAdmin->aHeaders as &$header)
            if (array_key_exists($header["id"], $arHeaderReplace))
                $header["content"] = $arHeaderReplace[$header["id"]];

        foreach ($lAdmin->aVisibleHeaders as &$header)
            if (array_key_exists($header["id"], $arHeaderReplace))
                $header["content"] = $arHeaderReplace[$header["id"]];

        //AddMessage2Log(print_r($lAdmin,true));
    }

    public function BeforePostingSendMailHandler($arFields)
    {
        global $APPLICATION;

        static $arDefSite = false;

    if (!CModule::IncludeModule("subscribe"))
        return;

        if (!$arDefSite) {
            $dbDefSites = CSite::GetDefList();
            $arDefSite = $dbDefSites->Fetch();
        }

        $link = 'http://'.$arDefSite["SERVER_NAME"].'/about/subscr_edit.php?ID='.$arFields["EMAIL_EX"]["SUBSCRIPTION_ID"];

    $rsSubscription = CSubscription::GetByID($arFields["EMAIL_EX"]["SUBSCRIPTION_ID"]);

    if ($arSubscription = $rsSubscription->Fetch())
        $link.="&AUTH_PASS=".$arSubscription["CONFIRM_CODE"];

        $arFields["BODY"] = str_replace('#UNSUBSCRIBE_LINK#', $link, $arFields["BODY"]);

        return $arFields;
    }

    public function Redirect404()
    {
        if (
                !defined('ADMIN_SECTION') &&
                defined("ERROR_404") && 
                ERROR_404 == 'Y'
        ) {
            global $APPLICATION;
            $APPLICATION->RestartBuffer();
            include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/header.php';
            include $_SERVER['DOCUMENT_ROOT'].'/404.php';
            include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/footer.php';
        }
    }
}
