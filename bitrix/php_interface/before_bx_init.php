<?php
// Файл подключается до загрузки ядра bitrix через auto_prepend_file. 
// Используется для временного переопределения текущего хоста (заменяем m.*.mamadeti.ru на mobile.*.mamadeti.ru), чтобы bitrix
// мог корректно определить текущий сайт.
$_SERVER['OLD_HTTP_HOST'] = $_SERVER['HTTP_HOST'];
$_SERVER['HTTP_HOST'] = preg_replace('/^m\./', 'mobile.', $_SERVER['HTTP_HOST']);
