<?php

interface RegionLookup
{
    public function __construct($property);

    public function lookup($iblockId, $elementId);
}

class RegionByCityLookup implements RegionLookup
{
    protected $property = null;

    public function __construct($property)
    {
        $this->property = $property;
    }

    public function lookup($iblockId, $elementId)
    {
        $rsElement = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'ID' => $elementId),
            false,
            false,
            array(
                'IBLOCK_ID',
                'ID',
                'PROPERTY_'.$this->property,
            )
        );

        $element = $rsElement->GetNextElement();
        unset($rsElement);

        if (!$element) {
            return;
        }

        $props = $element->GetProperties();

        if (!$props[$this->property]['VALUE']) {
            return;
        }

        $regionIds = $props[$this->property]['VALUE'];
        if (!is_array($regionIds)) {
            $regionIds = array($regionIds);
        }

        return $regionIds;
    }
}

class RegionByClinicLookup implements RegionLookup
{
    protected $property = null;

    public function __construct($property)
    {
        $this->property = $property;
    }

    public function lookup($iblockId, $elementId)
    {
        $rsElement = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'ID' => $elementId),
            false,
            false,
            array(
                'IBLOCK_ID',
                'ID',
                'PROPERTY_'.$this->property,
            )
        );

        $element = $rsElement->GetNextElement();
        unset($rsElement);

        if (!$element) {
            return;
        }

        $props = $element->GetProperties();
        if (!$props[$this->property]['VALUE']) {
            return;
        }

        $clinicsId = $props[$this->property]['VALUE'];
        if (!is_array($clinicsId)) {
            $clinicsId = array($clinicsId);
        }

        $lookup = new RegionByCityLookup('CITY');

        $regionIds = array();
        foreach ($clinicsId as $clinicId) {
            $ids = $lookup->lookup(2, $clinicId);
            if (!$ids) {
                continue;
            }
            $regionIds = array_merge($regionIds, $ids);
        }

        $regionIds = array_unique($regionIds);

        return $regionIds;
    }
}

class RegionSearchIndexer
{
    protected $rules = array();

    protected $regionIds = array();

    protected function getRegionIds()
    {
        if (!$this->regionIds) {
            $rsRegions = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 7));
            while ($region = $rsRegions->GetNext()) {
                $this->regionIds[] = $region['ID'];
            }
            unset($rsRegions);
        }

        return $this->regionIds;
    }

    public function addRule($iblockId, RegionLookup $lookup)
    {
        $this->rules[$iblockId] = $lookup;
        return $this;
    }

    public function beforeIndex($arFields) {
        CModule::IncludeModule('iblock');
        if (
            ($arFields['MODULE_ID'] === 'iblock')
            && (substr($arFields['ITEM_ID'], 0, 1) !== 'S') 
            && isset($this->rules[$arFields['PARAM2']])
        ) {
            $arFields["PARAMS"]["region"] = 
                $this->rules[$arFields['PARAM2']]->lookup(
                    $arFields['PARAM2'], 
                    $arFields['ITEM_ID']
                ) ?: $this->getRegionIds();
        } else {
            $arFields["PARAMS"]["region"] = $this->getRegionIds();
        }

        return $arFields;
    }
}