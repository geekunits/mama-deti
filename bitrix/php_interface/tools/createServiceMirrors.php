<?php
$_SERVER["DOCUMENT_ROOT"] = __DIR__.'/../../../';

require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";

CModule::IncludeModule('iblock');

while (ob_get_level()) {
    ob_get_clean();
}

$itemCount = 0;

$sm = ServiceMirror::getInstance();

$rsSections = CIBlockSection::GetList([], ['IBLOCK_ID' => SERVICE_IBLOCK_ID]);
while ($section = $rsSections->GetNext()) {
    echo "Creating mirror for section ".$section['NAME']."...\n";

    $sm->makeMirrorForServiceSection($section['ID']);

    $itemCount++;
}
unset($rsSections);

$rsElements = CIBlockElement::GetList([], ['IBLOCK_ID' => SERVICE_IBLOCK_ID]);
while ($element = $rsElements->GetNext()) {
    echo "Creating mirror for element ".$element['NAME']."...\n";

    $sm->makeMirrorForServiceElement($element['ID']);

    $itemCount++;
}
unset($rsElements);

echo "$itemCount items processed.\n";
