<?php

function generateCitySitemap($cityCode, $isMobile) {
	$cm = CityManager::getInstance();
	$city = $cm->getCityByCode($cityCode);
	if (!$city) {
	    echo "City $cityCode not found.\n";
	    exit(1);
	}

	$sg = new SitemapGenerator($cityCode, $isMobile);
	$um = new SitemapSectionManager($city['ID']);

	echo 'Generating sitemap for ' . $city['NAME'] . '.' . PHP_EOL;
	foreach($um->getSections() as $name => $section) {
		echo 'Generating section ' . $name . '... ';
		while(($record = $section->getRecord()) !== false) {
			list($loc, $lastmod, $priority) = $record;
			$sg->addUrl($name, $loc, $lastmod, $priority);
		}
		echo 'OK' . PHP_EOL;
	}
	$sg->save();
}

$start = microtime(true);

$arguments = array();

if(count($argv) > 1){
    array_shift($argv);

    foreach($argv AS $arg){
        list($argName, $argValue) = explode('=', $arg, 2);

        $arguments[$argName] = $argValue;
    }
}

if (!isset($arguments['--cities'])) {
    echo "No city defined.\nYou can use \"--cities=all\" to generate all available sitemaps.\n";
    exit(1);
}

$isMobile = (isset($arguments['--mobile']) && $arguments['--mobile']);

$rootPath = realpath(__DIR__.'/../../..');

if($isMobile){
    $arPathParts = explode('/', $rootPath);

    $hostPath = array_pop($arPathParts);

    $hostPath = $isMobile ? 'm.' . $hostPath : $hostPath ;

    $rootPath = implode('/', $arPathParts) . '/' . $hostPath;
}

$_SERVER["DOCUMENT_ROOT"] = $rootPath;
// /home/bitrix/ext_www/dev.mamadeti.ru
// /home/bitrix/ext_www/m.dev.mamadeti.ru

require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";

echo 'Bitrix header included: ' . round(microtime(true) - $start, 2). 's.' . PHP_EOL;

$cityCodes = [];

if ($arguments['--cities'] === 'all') {
	$cities = CityManager::getInstance()->getCities();

	foreach($cities as $city) {
		$cityCodes[] = $city['CODE'];
	}
} else {
	$cityCodes = array($arguments['--cities']);
}

foreach($cityCodes as $cc) {
	generateCitySitemap($cc, $isMobile);
	echo PHP_EOL;
}

echo 'Done!' . PHP_EOL;
echo 'Time left: ' . round(microtime(true) - $start, 2) . 's.' . PHP_EOL;