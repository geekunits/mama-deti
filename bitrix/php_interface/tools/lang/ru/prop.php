<?
$MESS["USER_TYPE_HTML_DESCRIPTION"] = "HTML поле";
$MESS["USER_TYPE_HTML_DEFAULT_VALUE"] = "Значение по умолчанию";
$MESS["USER_TYPE_HTML_MIN_LEGTH"] = "Минимальная длинна";
$MESS["USER_TYPE_HTML_MAX_LENGTH"] = "Максимальная длинна";
$MESS["USER_TYPE_HTML_REGEXP"] = "Выражение Regexp";
$MESS["USER_TYPE_HTML_MIN_LEGTH_ERROR"] = "Минемальное значение #FIELD_NAME# должно быть #MIN_LENGHT#";
$MESS["USER_TYPE_HTML_MAX_LEGTH_ERROR"] = "Максимальное значение #FIELD_NAME# должно быть #MAX_LENGHT#";
$MESS["USER_TYPE_HTML_REGEXP_ERROR"] = "Поле #FIELD_NAME# заполнено неправильно";
$MESS["USER_TYPE_CHECKBOX_DESCRIPTION"] = "CHECKBOX";
$MESS["USER_TYPE_CHECKBOX_CHECKED_TITLE"] = "Да";
$MESS["USER_TYPE_CHECKBOX_NOTCHECKED_TITLE"] = "Нет";
?>