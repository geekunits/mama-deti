<?

__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/prop.php");
/*

  RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', 'main', 'CIBlockPropertyCheckbox', 'GetUserTypeDescription', 100, '/php_interface/tools/prop.php');
  RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', 'main', 'CIBlockPropertyStringReadOnly', 'GetUserTypeDescription', 100, '/php_interface/tools/prop.php');
  RegisterModuleDependences('iblock', 'OnIBlockPropertyBuildList', 'main', 'CIBlockPropertyOutPrice', 'GetUserTypeDescription', 100, '/php_interface/tools/prop.php');


 */
if (!class_exists("CIBlockPropertyCheckbox")) {

    class CIBlockPropertyCheckbox {

        function GetUserTypeDescription() {
            return array(
                'PROPERTY_TYPE' => 'N',
                'USER_TYPE' => 'Melcosoft_IntCheckBox',
                'DESCRIPTION' => 'CHECKBOX',
                'GetPropertyFieldHtml' => array('CIBlockPropertyCheckbox', 'GetPropertyFieldHtml'),
                'ConvertToDB' => array('CIBlockPropertyCheckbox', 'ConvertToDB'),
                'ConvertFromDB' => array('CIBlockPropertyCheckbox', 'ConvertFromDB')
            );
        }

//\\ function GetUserTypeDescription

        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
            global $USER;
            $strTemp = "";
            if (intval($value['VALUE']) > 0)
                $strTemp = ' checked="checked"';
            $return = '<input type="checkbox" name="' . $strHTMLControlName['VALUE'] . '" id="' . $strHTMLControlName['VALUE'] . '" value="1"' . $strTemp . '>';
            return $return;
        }

//\\ function GetPropertyFieldHtml

        function ConvertToDB($arProperty, $value) {
            $return = array();
            if (intVal($value['VALUE']) > 0)
                $return['VALUE'] = intVal($value['VALUE']);
            else
                $return['VALUE'] = '';

            return $return;
        }

//\\ function ConvertToDB

        function ConvertFromDB($arProperty, $value) {
            $return = array();
            if (intVal($value['VALUE']) > 0)
                $return['VALUE'] = intVal($value['VALUE']);
            else
                $return['VALUE'] = '';

            return $return;
        }

//\\ function ConvertFromDB
    }

}

if (!class_exists("CIBlockPropertySchedule")) {

    class CIBlockPropertySchedule {

        function GetUserTypeDescription() {
            return array(
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'Melcosoft_Schedule',
                'DESCRIPTION' => 'Schedule',
                'GetPropertyFieldHtml' => array('CIBlockPropertySchedule', 'GetPropertyFieldHtml'),
                'ConvertToDB' => array('CIBlockPropertySchedule', 'ConvertToDB'),
                'ConvertFromDB' => array('CIBlockPropertySchedule', 'ConvertFromDB')
            );
        }

//\\ function GetUserTypeDescription

        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
            global $USER;

	    CJSCore::Init(array("jquery","json"));

ob_start();
	$div_id = "schedule_".md5($strHTMLControlName['VALUE']);
	$inp_id = "inp_".md5($strHTMLControlName['VALUE']);
?>
<div id="<?=$div_id?>">
<input type="hidden" name="<?=$strHTMLControlName['VALUE']?>" value="<?=htmlspecialcharsbx(CUtil::PhpToJSObject($value['VALUE']))?>">
<table>
<tr><td>Понедельник</td><td><input type="text" name="<?=$inp_id.'_1'?>" value="<?=htmlspecialcharsbx($value['VALUE'][0])?>" data-id="0"></td></tr>
<tr><td>Вторник</td><td><input type="text" name="<?=$inp_id.'_2'?>" value="<?=htmlspecialcharsbx($value['VALUE'][1])?>" data-id="1"></td></tr>
<tr><td>Среда</td><td><input type="text" name="<?=$inp_id.'_3'?>" value="<?=htmlspecialcharsbx($value['VALUE'][2])?>" data-id="2"></td></tr>
<tr><td>Четверг</td><td><input type="text" name="<?=$inp_id.'_4'?>" value="<?=htmlspecialcharsbx($value['VALUE'][3])?>" data-id="3"></td></tr>
<tr><td>Пятница</td><td><input type="text" name="<?=$inp_id.'_5'?>" value="<?=htmlspecialcharsbx($value['VALUE'][4])?>" data-id="4"></td></tr>
<tr><td>Суббота</td><td><input type="text" name="<?=$inp_id.'_6'?>" value="<?=htmlspecialcharsbx($value['VALUE'][5])?>" data-id="5"></td></tr>
<tr><td>Воскресенье</td><td><input type="text" name="<?=$inp_id.'_7'?>" value="<?=htmlspecialcharsbx($value['VALUE'][6])?>" data-id="6"></td></tr>
</table>

<script>

$(function(){
	$('div#<?=$div_id?> input[type=text]').on('keyup', function()
	{
		arData = [];
		$('div#<?=$div_id?> input[type=text]').each(function() {
			arData[$(this).data('id')] = $(this).val();
		});
		$('div#<?=$div_id?> input[name="<?=$strHTMLControlName['VALUE']?>"]').val(JSON.stringify(arData));
		//console.log($('div#<?=$div_id?> input[name="<?=$strHTMLControlName['VALUE']?>"]').val());
	});
	
});

</script>

</div>
<?
	    $return = ob_get_contents();
	    ob_end_clean();

            return $return;
        }

//\\ function GetPropertyFieldHtml

        function ConvertToDB($arProperty, $value) {
            $return = array();
	    if (is_array($value['VALUE']))
	            $return['VALUE'] = CUtil::PhpToJSObject($value['VALUE']); else
		    $return['VALUE'] = $value['VALUE'];

            return $return;
        }

//\\ function ConvertToDB

        function ConvertFromDB($arProperty, $value) {
            $return = array();
            $return['VALUE'] = CUtil::JsObjectToPhp($value['VALUE']);

            return $return;
        }

//\\ function ConvertFromDB
    }

}

if (!class_exists("CIBlockPropertyStringReadOnly")) {

    class CIBlockPropertyStringReadOnly {

        function GetUserTypeDescription() {
            return array(
                'PROPERTY_TYPE' => 'S',
                'USER_TYPE' => 'Melcosoft_StringReadOnly',
                'DESCRIPTION' => 'Строка readonly',
                'GetPropertyFieldHtml' => array('CIBlockPropertyStringReadOnly', 'GetPropertyFieldHtml'),
                'ConvertToDB' => array('CIBlockPropertyStringReadOnly', 'ConvertToDB'),
                'ConvertFromDB' => array('CIBlockPropertyStringReadOnly', 'ConvertFromDB')
            );
        }

        function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
            CUtil::InitJSCore();
            CJSCore::Init(array("jquery"));
            $strTemp = " readonly";
            $return = '<input type="text" size="40" name="' . $strHTMLControlName['VALUE'] . '" id="' . $strHTMLControlName['VALUE'] . '" value="' . $value['VALUE'] . '"' . $strTemp . '>';
            return $return;
        }

        function ConvertToDB($arProperty, $value) {
            return $value['VALUE'];
        }

        function ConvertFromDB($arProperty, $value) {
            return $value['VALUE'];
        }

    }

}

if (!class_exists("CUserTypeHTML")) {
	class CUserTypeHTML {
	function GetUserTypeDescription()
	{
		return array(
			"USER_TYPE_ID" => "userhtml",
			"CLASS_NAME" => "CUserTypeHTML",
			"DESCRIPTION" => GetMessage("USER_TYPE_HTML_DESCRIPTION"),
			"BASE_TYPE" => "string",
		);
	}

	/**
	 * Эта функция вызывается при добавлении нового свойства.
	 *
	 * <p>Эта функция вызывается для конструирования SQL запроса
	 * создания колонки для хранения не множественных значений свойства.</p>
	 * <p>Значения множественных свойств хранятся не в строках, а столбиках (как в инфоблоках)
	 * и тип такого поля в БД всегда text.</p>
	 * @param array $arUserField Массив описывающий поле
	 * @return string
	 * @static
	 */
	function GetDBColumnType($arUserField)
	{
		global $DB;
		switch(strtolower($DB->type))
		{
			case "mysql":
				return "text";
			case "oracle":
				return "varchar2(2000 char)";
			case "mssql":
				return "varchar(2000)";
		}
	}

	/**
	 * Эта функция вызывается перед сохранением метаданных свойства в БД.
	 *
	 * <p>Она должна "очистить" массив с настройками экземпляра типа свойства.
	 * Для того что бы случайно/намеренно никто не записал туда всякой фигни.</p>
	 * @param array $arUserField Массив описывающий поле. <b>Внимание!</b> это описание поля еще не сохранено в БД!
	 * @return array Массив который в дальнейшем будет сериализован и сохранен в БД.
	 * @static
	 */
	function PrepareSettings($arUserField)
	{
		$min = intval($arUserField["SETTINGS"]["MIN_LENGTH"]);
		$max = intval($arUserField["SETTINGS"]["MAX_LENGTH"]);

		return array(
			"REGEXP" => $arUserField["SETTINGS"]["REGEXP"],
			"MIN_LENGTH" => $min,
			"MAX_LENGTH" => $max,
			"DEFAULT_VALUE" => $arUserField["SETTINGS"]["DEFAULT_VALUE"],
		);
	}

	/**
	 * Эта функция вызывается при выводе формы настройки свойства.
	 *
	 * <p>Возвращает html для встраивания в 2-х колоночную таблицу.
	 * в форму usertype_edit.php</p>
	 * <p>т.е. tr td bla-bla /td td edit-edit-edit /td /tr </p>
	 * @param array $arUserField Массив описывающий поле. Для нового (еще не добавленного поля - <b>false</b>)
	 * @param array $arHtmlControl Массив управления из формы. Пока содержит только один элемент NAME (html безопасный)
	 * @return string HTML для вывода.
	 * @static
	 */
	function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
	{
		$result = '';
		if($bVarsFromForm)
			$value = htmlspecialcharsbx($GLOBALS[$arHtmlControl["NAME"]]["DEFAULT_VALUE"]);
		elseif(is_array($arUserField))
			$value = htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
		else
			$value = "";
		$result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_HTML_DEFAULT_VALUE").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[DEFAULT_VALUE]" size="20"  maxlength="225" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = intval($GLOBALS[$arHtmlControl["NAME"]]["MIN_LENGTH"]);
		elseif(is_array($arUserField))
			$value = intval($arUserField["SETTINGS"]["MIN_LENGTH"]);
		else
			$value = 0;
		$result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_HTML_MIN_LEGTH").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[MIN_LENGTH]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = intval($GLOBALS[$arHtmlControl["NAME"]]["MAX_LENGTH"]);
		elseif(is_array($arUserField))
			$value = intval($arUserField["SETTINGS"]["MAX_LENGTH"]);
		else
			$value = 0;
		$result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_HTML_MAX_LENGTH").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[MAX_LENGTH]" size="20"  maxlength="20" value="'.$value.'">
			</td>
		</tr>
		';
		if($bVarsFromForm)
			$value = htmlspecialcharsbx($GLOBALS[$arHtmlControl["NAME"]]["REGEXP"]);
		elseif(is_array($arUserField))
			$value = htmlspecialcharsbx($arUserField["SETTINGS"]["REGEXP"]);
		else
			$value = "";
		$result .= '
		<tr>
			<td>'.GetMessage("USER_TYPE_HTML_REGEXP").':</td>
			<td>
				<input type="text" name="'.$arHtmlControl["NAME"].'[REGEXP]" size="20"  maxlength="200" value="'.$value.'">
			</td>
		</tr>
		';
		return $result;
	}

	/**
	 * Эта функция вызывается при выводе формы редактирования значения свойства.
	 *
	 * <p>Возвращает html для встраивания в ячейку таблицы.
	 * в форму редактирования сущности (на вкладке "Доп. свойства")</p>
	 * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
	 * @return string HTML для вывода.
	 * @static
	 */
	function GetEditFormHTML($arUserField, $arHtmlControl)
	{

        if (!CModule::IncludeModule("fileman"))
            return GetMessage("IBLOCK_PROP_HTML_NOFILEMAN_ERROR");

        $id = preg_replace("/[^a-z0-9]/i", '', $arHtmlControl["NAME"]);

        ob_start();
	print_r($arHtmlControl);
        echo '<input type="hidden" name="'.$arHtmlControl["NAME"].'[TYPE]" value="html">';
        $LHE = new CLightHTMLEditor;
        $LHE->Show(array(
            'id' => $id,
            'width' => '100%',
            'height' => '200px',
            'inputName' => $arHtmlControl["NAME"],
            'content' => $arHtmlControl["VALUE"],
            'bUseFileDialogs' => false,
            'bFloatingToolbar' => false,
            'bArisingToolbar' => false,
            'toolbarConfig' => array(
                'Bold', 'Italic', 'Underline', 'RemoveFormat',
                'CreateLink', 'DeleteLink', 'Image', 'Video',
                'BackColor', 'ForeColor',
                'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
                'StyleList', 'HeaderList',
                'FontList', 'FontSizeList',
            ),
        ));
        $s = ob_get_contents();
        ob_end_clean();
        return  $s;

	}

	/**
	 * Эта функция вызывается при выводе формы редактирования значения <b>множественного</b> свойства.
	 *
	 * <p>Если класс не предоставляет такую функцию,
	 * то менеджер типов "соберет" требуемый html из вызовов GetEditFormHTML</p>
	 * <p>Возвращает html для встраивания в ячейку таблицы.
	 * в форму редактирования сущности (на вкладке "Доп. свойства")</p>
	 * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
	 * <p>Поле VALUE $arHtmlControl - массив.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
	 * @return string HTML для вывода.
	 * @static
	 */
/*
	function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
	{
		if($arUserField["VALUE"]===false && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"])>0)
			$arHtmlControl["VALUE"] = array(htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]));
		$result = array();
		foreach($arHtmlControl["VALUE"] as $value)
		{
			if($arUserField["SETTINGS"]["ROWS"] < 2)
				$result[] = '<input type="text" '.
					'name="'.$arHtmlControl["NAME"].'" '.
					'size="'.$arUserField["SETTINGS"]["SIZE"].'" '.
					($arUserField["SETTINGS"]["MAX_LENGTH"]>0? 'maxlength="'.$arUserField["SETTINGS"]["MAX_LENGTH"].'" ': '').
					'value="'.$value.'" '.
					($arUserField["EDIT_IN_LIST"]!="Y"? 'disabled="disabled" ': '').
					'>';
			else
				$result[] = '<textarea '.
					'name="'.$arHtmlControl["NAME"].'" '.
					'cols="'.$arUserField["SETTINGS"]["SIZE"].'" '.
					'rows="'.$arUserField["SETTINGS"]["ROWS"].'" '.
					($arUserField["SETTINGS"]["MAX_LENGTH"]>0? 'maxlength="'.$arUserField["SETTINGS"]["MAX_LENGTH"].'" ': '').
					($arUserField["EDIT_IN_LIST"]!="Y"? 'disabled="disabled" ': '').
					'>'.$value.'</textarea>';
		}
		return implode("<br>", $result);
	}
*/
	/**
	 * Эта функция вызывается при выводе фильтра на странице списка.
	 *
	 * <p>Возвращает html для встраивания в ячейку таблицы.</p>
	 * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
	 * @return string HTML для вывода.
	 * @static
	 */
	function GetFilterHTML($arUserField, $arHtmlControl)
	{
		return '<input type="text" '.
			'name="'.$arHtmlControl["NAME"].'" '.
			'size="'.$arUserField["SETTINGS"]["SIZE"].'" '.
			'value="'.$arHtmlControl["VALUE"].'"'.
			'>';
	}

	/**
	 * Эта функция вызывается при выводе значения свойства в списке элементов.
	 *
	 * <p>Возвращает html для встраивания в ячейку таблицы.</p>
	 * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
	 * @return string HTML для вывода.
	 * @static
	 */
	function GetAdminListViewHTML($arUserField, $arHtmlControl)
	{
		if(strlen($arHtmlControl["VALUE"])>0)
			return $arHtmlControl["VALUE"];
		else
			return '&nbsp;';
	}

	/**
	 * Эта функция вызывается при выводе значения <b>множественного</b> свойства в списке элементов.
	 *
	 * <p>Возвращает html для встраивания в ячейку таблицы.</p>
	 * <p>Если класс не предоставляет такую функцию,
	 * то менеджер типов "соберет" требуемый html из вызовов GetAdminListViewHTML</p>
	 * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
	 * <p>Поле VALUE $arHtmlControl - массив.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
	 * @return string HTML для вывода.
	 * @static
	 */
/*
	function GetAdminListViewHTMLMulty($arUserField, $arHtmlControl)
	{
		return implode(", ", $arHtmlControl["VALUE"]);
	}
*/
	/**
	 * Эта функция вызывается при выводе значения свойства в списке элементов в режиме <b>редактирования</b>.
	 *
	 * <p>Возвращает html для встраивания в ячейку таблицы.</p>
	 * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
	 * @return string HTML для вывода.
	 * @static
	 */
	function GetAdminListEditHTML($arUserField, $arHtmlControl)
	{
			return '<textarea '.
				'name="'.$arHtmlControl["NAME"].'" '.
				'cols="10" '.
				'rows="20" '.
				($arUserField["SETTINGS"]["MAX_LENGTH"]>0? 'maxlength="'.$arUserField["SETTINGS"]["MAX_LENGTH"].'" ': '').
				'>'.$arHtmlControl["VALUE"].'</textarea>';
	}

	/**
	 * Эта функция вызывается при выводе <b>множественного</b> свойства в списке элементов в режиме <b>редактирования</b>.
	 *
	 * <p>Возвращает html для встраивания в ячейку таблицы.</p>
	 * <p>Если класс не предоставляет такую функцию,
	 * то менеджер типов "соберет" требуемый html из вызовов GetAdminListEditHTML</p>
	 * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
	 * <p>Поле VALUE $arHtmlControl - массив.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
	 * @return string HTML для вывода.
	 * @static
	 */
/*
	function GetAdminListEditHTMLMulty($arUserField, $arHtmlControl)
	{
		$result = array();
		foreach($arHtmlControl["VALUE"] as $value)
		{
			if($arUserField["SETTINGS"]["ROWS"] < 2)
				$result[] = '<input type="text" '.
					'name="'.$arHtmlControl["NAME"].'" '.
					'size="'.$arUserField["SETTINGS"]["SIZE"].'" '.
					($arUserField["SETTINGS"]["MAX_LENGTH"]>0? 'maxlength="'.$arUserField["SETTINGS"]["MAX_LENGTH"].'" ': '').
					'value="'.$value.'" '.
					'>';
			else
				$result[] = '<textarea '.
					'name="'.$arHtmlControl["NAME"].'" '.
					'cols="'.$arUserField["SETTINGS"]["SIZE"].'" '.
					'rows="'.$arUserField["SETTINGS"]["ROWS"].'" '.
					($arUserField["SETTINGS"]["MAX_LENGTH"]>0? 'maxlength="'.$arUserField["SETTINGS"]["MAX_LENGTH"].'" ': '').
				'>'.$value.'</textarea>';
		}
		return '&nbsp;'.implode("<br>", $result);
	}
*/
	/**
	 * Эта функция валидатор.
	 *
	 * <p>Вызывается из метода CheckFields объекта $USER_FIELD_MANAGER.</p>
	 * <p>Который в свою очередь может быть вызван из меторов Add/Update сущности владельца свойств.</p>
	 * <p>Выполняется 2 проверки:</p>
	 * <ul>
	 * <li>на минимальную длину (если в настройках минимальная длина больше 0).
	 * <li>на регулярное выражение (если задано в настройках).
	 * </ul>
	 * @param array $arUserField Массив описывающий поле.
	 * @param array $value значение для проверки на валидность
	 * @return array массив массивов ("id","text") ошибок.
	 * @static
	 */
	function CheckFields($arUserField, $value)
	{
		$aMsg = array();
		if(strlen($value)<$arUserField["SETTINGS"]["MIN_LENGTH"])
		{
			$aMsg[] = array(
				"id" => $arUserField["FIELD_NAME"],
				"text" => GetMessage("USER_TYPE_HTML_MIN_LEGTH_ERROR",
					array(
						"#FIELD_NAME#"=>$arUserField["EDIT_FORM_LABEL"],
						"#MIN_LENGTH#"=>$arUserField["SETTINGS"]["MIN_LENGTH"]
					)
				),
			);
		}
		if($arUserField["SETTINGS"]["MAX_LENGTH"]>0 && strlen($value)>$arUserField["SETTINGS"]["MAX_LENGTH"])
		{
			$aMsg[] = array(
				"id" => $arUserField["FIELD_NAME"],
				"text" => GetMessage("USER_TYPE_HTML_MAX_LEGTH_ERROR",
					array(
						"#FIELD_NAME#"=>$arUserField["EDIT_FORM_LABEL"],
						"#MAX_LENGTH#"=>$arUserField["SETTINGS"]["MAX_LENGTH"]
					)
				),
			);
		}
		if(strlen($arUserField["SETTINGS"]["REGEXP"])>0 && !preg_match($arUserField["SETTINGS"]["REGEXP"], $value))
		{
			$aMsg[] = array(
				"id" => $arUserField["FIELD_NAME"],
				"text" => (strlen($arUserField["ERROR_MESSAGE"])>0?
						$arUserField["ERROR_MESSAGE"]:
						GetMessage("USER_TYPE_HTML_REGEXP_ERROR",
						array(
							"#FIELD_NAME#"=>$arUserField["EDIT_FORM_LABEL"],
						)
					)
				),
			);
		}
		return $aMsg;
	}

	/**
	 * Эта функция должна вернуть представление значения поля для поиска.
	 *
	 * <p>Вызывается из метода OnSearchIndex объекта $USER_FIELD_MANAGER.</p>
	 * <p>Который в свою очередь вызывается и функции обновления поискового индекса сущности.</p>
	 * <p>Для множественных значений поле VALUE - массив.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @return string посковое содержимое.
	 * @static
	 */
	function OnSearchIndex($arUserField)
	{
		if(is_array($arUserField["VALUE"]))
			return implode("\r\n", $arUserField["VALUE"]);
		else
			return $arUserField["VALUE"];
	}

	/**
	 * Эта функция вызывается перед сохранением значений в БД.
	 *
	 * <p>Вызывается из метода Update объекта $USER_FIELD_MANAGER.</p>
	 * <p>Для множественных значений функция вызывается несколько раз.</p>
	 * @param array $arUserField Массив описывающий поле.
	 * @param mixed $value Значение.
	 * @return string значение для вставки в БД.
	 * @static
	 */
/*
	function OnBeforeSave($arUserField, $value)
	{
		if(strlen($value)>0)
			return "".round(doubleval($value), $arUserField["SETTINGS"]["PRECISION"]);
	}
*/
}
}


if (!class_exists("CUserTypeCheckbox")) {
	class CUserTypeCheckbox {
	function GetUserTypeDescription()
	{
		return array(
			"USER_TYPE_ID" => "usercheckbox",
			"CLASS_NAME" => "CUserTypeCheckbox",
			"DESCRIPTION" => GetMessage("USER_TYPE_CHECKBOX_DESCRIPTION"),
			"BASE_TYPE" => "int",
		);
	}

	function GetDBColumnType($arUserField)
	{
		global $DB;
		switch(strtolower($DB->type))
		{
			case "mysql":
				return "tinyint";
			case "oracle":
				return "int";
			case "mssql":
				return "int";
		}
	}

	function PrepareSettings($arUserField)
	{
		return array(
		);
	}

	function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
	{
		$result = '';
		return $result;
	}

	function GetEditFormHTML($arUserField, $arHtmlControl)
	{
		return '<select name="'.$arHtmlControl["NAME"].'"><option value="0"'.(!$arHtmlControl["VALUE"] ? ' selected' : '').'>'.GetMessage('USER_TYPE_CHECKBOX_NOTCHECKED_TITLE').'</option>'.
			'<option value="1"'.($arHtmlControl["VALUE"] ? ' selected' : '').'>'.GetMessage('USER_TYPE_CHECKBOX_CHECKED_TITLE').'</option></select>';
	}

	function GetFilterHTML($arUserField, $arHtmlControl)
	{
		return '<input type="text" '.
			'name="'.$arHtmlControl["NAME"].'" '.
			'size="'.$arUserField["SETTINGS"]["SIZE"].'" '.
			'value="'.$arHtmlControl["VALUE"].'"'.
			'>';
	}

	function GetAdminListViewHTML($arUserField, $arHtmlControl)
	{
		return $arHtmlControl["VALUE"] ? GetMessage('USER_TYPE_CHECKBOX_CHECKED_TITLE') : GetMessage('USER_TYPE_CHECKBOX_NOTCHECKED_TITLE');
	}

	function GetAdminListEditHTML($arUserField, $arHtmlControl)
	{
		return '<select name="'.$arHtmlControl["NAME"].'"><option value="0"'.(!$arHtmlControl["VALUE"] ? ' selected' : '').'>'.GetMessage('USER_TYPE_CHECKBOX_NOTCHECKED_TITLE').'</option>'.
			'<option value="1"'.($arHtmlControl["VALUE"] ? ' selected' : '').'>'.GetMessage('USER_TYPE_CHECKBOX_CHECKED_TITLE').'</option></select>';
	}

/*	function CheckFields($arUserField, $value)
	{
		return array();
	}

	function OnSearchIndex($arUserField)
	{
		if(is_array($arUserField["VALUE"]))
			return implode("\r\n", $arUserField["VALUE"]);
		else
			return $arUserField["VALUE"];
	}*/

}
}

?>