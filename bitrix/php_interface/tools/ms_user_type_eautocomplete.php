<?
__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/".basename(__FILE__));

define ('BT_UT_AUTOCOMPLETE_REP_SYM_OTHER','other');

class CMSUserTypeEAutocomplete
{
	function GetUserTypeDescription()
	{
		return array(
	                "USER_TYPE_ID" => "ms_user_type_eautocomplete",
	                "CLASS_NAME" => __CLASS__,
			"DESCRIPTION" => GetMessage("MS_USER_TYPE_EAUTOCOMPLETE_DESC"),
                	"BASE_TYPE" => "int",
		);
	}

	function GetDBColumnType($arUserField)
	{
		global $DB;
		switch(strtolower($DB->type))
		{
			case "mysql":
				return "int(18)";
			case "oracle":
				return "number(18)";
			case "mssql":
				return "int";
		}
		return "int";
	}

	function CheckFields($arUserField, $value)
	{
		$aMsg = array();
		return $aMsg;
	}

	function OnSearchIndex($arUserField)
	{
		$res = '';

		if(is_array($arUserField["VALUE"]))
			$val = $arUserField["VALUE"];
		else
			$val = array($arUserField["VALUE"]);

		$val = array_filter($val, "strlen");
		if(count($val) && CModule::IncludeModule('iblock'))
		{
			$ob = new CIBlockElement;
			$rs = $ob->GetList(array("sort" => "asc", "id" => "asc"), array(
				"=ID" => $val
			), false, false, array("NAME"));

			while($ar = $rs->Fetch())
				$res .= $ar["NAME"]."\r\n";
		}

		return $res;
	}

        protected function GetLinkElement($ElementXMLID, $intIBlockID)
        {
            static $cache = array();

            $arResult = false;
            $intIBlockID = intval($intIBlockID);
            if (0 >= $intIBlockID)
                $intIBlockID = 0;
            if (!array_key_exists($ElementXMLID, $cache)) {
                $arFilter = array();
                if (0 < $intIBlockID)
                    $arFilter['IBLOCK_ID'] = $intIBlockID;
                $arFilter['ID'] = $ElementXMLID;
                $arFilter['SHOW_HISTORY'] = 'Y';
                $rsElements = CIBlockElement::GetList(array(), $arFilter, false, false, array('IBLOCK_ID', 'ID', 'NAME', 'XML_ID'));
                if ($arElement = $rsElements->GetNext(true, true)) {
                    $arResult = array(
                        'ID' => $arElement['ID'],
                        'NAME' => $arElement['NAME'],
                        '~NAME' => $arElement['~NAME'],
                        'XML_ID' => $arElement['XML_ID'],
                        '~XML_ID' => $arElement['~XML_ID'],
                        'IBLOCK_ID' => $arElement['IBLOCK_ID'],
                        'IBLOCK_TYPE_ID' => CIBlock::GetArrayByID($arElement['IBLOCK_ID'], 'IBLOCK_TYPE_ID'),
                    );
                    $cache[$ElementXMLID] = $arResult;
                } else {
                    $cache[$ElementXMLID] = false;
                }
            }
            return $cache[$ElementXMLID];
        }

        protected function GetPropertyValue($IBLOCK_ID, $value)
        {
            $mxResult = false;

            if (0 < intval($value)) {
                $mxResult = self::GetLinkElement($value, $IBLOCK_ID);
            }
            return $mxResult;
        }

        protected function GetPropertyViewsList($boolFull)
        {
            $boolFull = (true == $boolFull);
            if ($boolFull) {
                return array(
                    'REFERENCE' => array(
                        GetMessage('BT_UT_EAUTOCOMPLETE_VIEW_AUTO'),
                        GetMessage('BT_UT_EAUTOCOMPLETE_VIEW_TREE'),
                        GetMessage('BT_UT_EAUTOCOMPLETE_VIEW_ELEMENT'),
                    ),
                    'REFERENCE_ID' => array(
                        'A', 'T', 'E'
                    ),
                );
            }
            return array('A', 'T', 'E');
        }

        protected function GetReplaceSymList($boolFull = false)
        {
            $boolFull = (true == $boolFull);
            if ($boolFull) {
                return array(
                    'REFERENCE' => array(
                        GetMessage('BT_UT_AUTOCOMPLETE_SYM_SPACE'),
                        GetMessage('BT_UT_AUTOCOMPLETE_SYM_GRID'),
                        GetMessage('BT_UT_AUTOCOMPLETE_SYM_STAR'),
                        GetMessage('BT_UT_AUTOCOMPLETE_SYM_UNDERLINE'),
                        GetMessage('BT_UT_AUTOCOMPLETE_SYM_OTHER'),

                    ),
                    'REFERENCE_ID' => array(
                        ' ',
                        '#',
                        '*',
                        '_',
                        BT_UT_AUTOCOMPLETE_REP_SYM_OTHER,
                    ),
                );
            }
            return array(' ', '#', '*', '_');
        }

	public function GetValueForAutoComplete($arProperty,$arValue,$arBanSym="",$arRepSym="")
	{
		$strResult = '';
		$mxResult = self::GetPropertyValue($arProperty,$arValue);
		if (is_array($mxResult))
		{
			$strResult = htmlspecialcharsbx(str_replace($arBanSym,$arRepSym,$mxResult['~NAME'])).' ['.$mxResult['ID'].']';
		}
		return $strResult;
	}

	public function GetValueForAutoCompleteMulti($IBLOCK_ID,$arValues,$arBanSym="",$arRepSym="")
	{
		$arResult = false;

		if (is_array($arValues))
		{
			foreach ($arValues as $intPropertyValueID => $arOneValue)
			{
	                    $mxResult = self::GetPropertyValue($IBLOCK_ID, $arOneValue);
				if (is_array($mxResult))
				{
					$arResult[$intPropertyValueID] = htmlspecialcharsbx(str_replace($arBanSym,$arRepSym,$mxResult['~NAME'])).' ['.$mxResult['ID'].']';
				}
			}
		}
		return $arResult;
	}

        protected function GetSymbols($arSettings)
        {
            $arResult = false;
            $strBanSym = $arSettings['BAN_SYM'];
            $strRepSym = (BT_UT_AUTOCOMPLETE_REP_SYM_OTHER == $arSettings['REP_SYM'] ? $arSettings['OTHER_REP_SYM'] : $arSettings['REP_SYM']);
            $arBanSym = str_split($strBanSym, 1);
            $arRepSym = array_fill(0, sizeof($arBanSym), $strRepSym);
            $arResult = array(
                'BAN_SYM' => $arBanSym,
                'REP_SYM' => array_fill(0, sizeof($arBanSym), $strRepSym),
                'BAN_SYM_STRING' => $strBanSym,
                'REP_SYM_STRING' => $strRepSym,
            );
            return $arResult;
        }

        public function PrepareSettings($arUserFields)
        {
            /*
             * VIEW				- view type
             * SHOW_ADD			- show button for add new values in linked iblock
             * MAX_WIDTH		- max width textarea and input in pixels
             * MIN_HEIGHT		- min height textarea in pixels
             * MAX_HEIGHT		- max height textarea in pixels
             * BAN_SYM			- banned symbols string
             * REP_SYM			- replace symbol
             * OTHER_REP_SYM	- non standart replace symbol
             * IBLOCK_MESS		- get lang mess from linked iblock
             */
            $iblock_id = intval($arUserFields["SETTINGS"]["IBLOCK_ID"]);
            if ($iblock_id <= 0)
                $iblock_id = "";

            $arViewsList = self::GetPropertyViewsList(false);
            $strView = '';
            $strView = (isset($arUserFields['SETTINGS']['VIEW']) && in_array($arUserFields['SETTINGS']['VIEW'], $arViewsList) ? $arUserFields['SETTINGS']['VIEW'] : current($arViewsList));

            $strShowAdd = (isset($arUserFields['SETTINGS']['SHOW_ADD']) ? $arUserFields['SETTINGS']['SHOW_ADD'] : '');
            $strShowAdd = ('Y' == $strShowAdd ? 'Y' : 'N');

            $intMaxWidth = intval(isset($arUserFields['SETTINGS']['MAX_WIDTH']) ? $arUserFields['SETTINGS']['MAX_WIDTH'] : 0);
            if (0 >= $intMaxWidth) $intMaxWidth = 0;

            $intMinHeight = intval(isset($arUserFields['SETTINGS']['MIN_HEIGHT']) ? $arUserFields['SETTINGS']['MIN_HEIGHT'] : 0);
            if (0 >= $intMinHeight) $intMinHeight = 24;

            $intMaxHeight = intval(isset($arUserFields['SETTINGS']['MAX_HEIGHT']) ? $arUserFields['SETTINGS']['MAX_HEIGHT'] : 0);
            if (0 >= $intMaxHeight) $intMaxHeight = 1000;

            $strBannedSymbols = trim(isset($arUserFields['SETTINGS']['BAN_SYM']) ? $arUserFields['SETTINGS']['BAN_SYM'] : ',;');
            $strBannedSymbols = str_replace(' ', '', $strBannedSymbols);
            if (false === strpos($strBannedSymbols, ','))
                $strBannedSymbols .= ',';
            if (false === strpos($strBannedSymbols, ';'))
                $strBannedSymbols .= ';';

            $strOtherReplaceSymbol = '';
            $strReplaceSymbol = (isset($arUserFields['SETTINGS']['REP_SYM']) ? $arUserFields['SETTINGS']['REP_SYM'] : ' ');
            if (BT_UT_AUTOCOMPLETE_REP_SYM_OTHER == $strReplaceSymbol) {
                $strOtherReplaceSymbol = (isset($arUserFields['SETTINGS']['OTHER_REP_SYM']) ? substr($arUserFields['SETTINGS']['OTHER_REP_SYM'], 0, 1) : '');
                if ((',' == $strOtherReplaceSymbol) || (';' == $strOtherReplaceSymbol))
                    $strOtherReplaceSymbol = '';
                if (('' == $strOtherReplaceSymbol) || in_array($strOtherReplaceSymbol, self::GetReplaceSymList())) {
                    $strReplaceSymbol = $strOtherReplaceSymbol;
                    $strOtherReplaceSymbol = '';
                }
            }
            if ('' == $strReplaceSymbol) {
                $strReplaceSymbol = ' ';
                $strOtherReplaceSymbol = '';
            }

            $strIBlockMess = (isset($arUserFields['SETTINGS']['IBLOCK_MESS']) ? $arUserFields['SETTINGS']['IBLOCK_MESS'] : '');
            if ('Y' != $strIBlockMess) $strIBlockMess = 'N';

            return array(
                'IBLOCK_ID' => $iblock_id,
                'VIEW' => $strView,
                'SHOW_ADD' => $strShowAdd,
                'MAX_WIDTH' => $intMaxWidth,
                'MIN_HEIGHT' => $intMinHeight,
                'MAX_HEIGHT' => $intMaxHeight,
                'BAN_SYM' => $strBannedSymbols,
                'REP_SYM' => $strReplaceSymbol,
                'OTHER_REP_SYM' => $strOtherReplaceSymbol,
                'IBLOCK_MESS' => $strIBlockMess,
            );
        }

        public function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
        {
            if ($bVarsFromForm)
                $arSettings = self::PrepareSettings(array("SETTINGS" => $GLOBALS[$arHtmlControl["NAME"]])); else
                if (is_array($arUserField))
                    $arSettings = self::PrepareSettings($arUserField); else
                    $arSettings = self::PrepareSettings(array());

            $result = '';

            if (CModule::IncludeModule('iblock')) {
                $result .= '
			<tr>
				<td>' . GetMessage("USER_TYPE_IBEL_DISPLAY") . ':</td>
				<td>
					' . GetIBlockDropDownList($arSettings["IBLOCK_ID"], $arHtmlControl["NAME"] . '[IBLOCK_TYPE_ID]', $arHtmlControl["NAME"] . '[IBLOCK_ID]', false, 'class="adm-detail-iblock-types"', 'class="adm-detail-iblock-list"') . '
				</td>
			</tr>
			';
            } else {
                $result .= '
			<tr>
				<td>' . GetMessage("USER_TYPE_IBEL_DISPLAY") . ':</td>
				<td>
					<input type="text" size="6" name="' . $arHtmlControl["NAME"] . '[IBLOCK_ID]" value="' . htmlspecialcharsbx($arSettings["IBLOCK_ID"]) . '">
				</td>
			</tr>
			';
            }


            return $result . '<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_VIEW') . '</td>
		<td>' . SelectBoxFromArray($arHtmlControl["NAME"] . '[VIEW]', self::GetPropertyViewsList(true), htmlspecialcharsbx($arSettings['VIEW'])) . '</td>
		</tr>
		<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_SHOW_ADD') . '</td>
		<td>' . InputType('checkbox', $arHtmlControl["NAME"] . '[SHOW_ADD]', 'Y', htmlspecialcharsbx($arSettings["SHOW_ADD"])) . '</td>
		</tr>
		<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_IBLOCK_MESS') . '</td>
		<td>' . InputType('checkbox', $arHtmlControl["NAME"] . '[IBLOCK_MESS]', 'Y', htmlspecialcharsbx($arSettings["IBLOCK_MESS"])) . '</td>
		</tr>
		<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_MAX_WIDTH') . '</td>
		<td><input type="text" name="' . $arHtmlControl["NAME"] . '[MAX_WIDTH]" value="' . intval($arSettings['MAX_WIDTH']) . '">&nbsp;' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_COMMENT_MAX_WIDTH') . '</td>
		</tr>
		<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_MIN_HEIGHT') . '</td>
		<td><input type="text" name="' . $arHtmlControl["NAME"] . '[MIN_HEIGHT]" value="' . intval($arSettings['MIN_HEIGHT']) . '">&nbsp;' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_COMMENT_MIN_HEIGHT') . '</td>
		</tr>
		<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_MAX_HEIGHT') . '</td>
		<td><input type="text" name="' . $arHtmlControl["NAME"] . '[MAX_HEIGHT]" value="' . intval($arSettings['MAX_HEIGHT']) . '">&nbsp;' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_COMMENT_MAX_HEIGHT') . '</td>
		</tr>
		<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_BAN_SYMBOLS') . '</td>
		<td><input type="text" name="' . $arHtmlControl["NAME"] . '[BAN_SYM]" value="' . htmlspecialcharsbx($arSettings['BAN_SYM']) . '"></td>
		</tr>
		<tr>
		<td>' . GetMessage('BT_UT_EAUTOCOMPLETE_SETTING_REP_SYMBOL') . '</td>
		<td>' . SelectBoxFromArray($arHtmlControl["NAME"] . '[REP_SYM]', self::GetReplaceSymList(true), htmlspecialcharsbx($arSettings['REP_SYM'])) . '&nbsp;<input type="text" name="' . $arHtmlControl["NAME"] . '[OTHER_REP_SYM]" size="1" maxlength="1" value="' . $arSettings['OTHER_REP_SYM'] . '"></td>
		</tr>
		';
        }

        function GetEditFormHTML($arUserField, $arHtmlControl)
        {
            return 'TO-DO';
        }


        //public function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName)
        function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
        {
            global $APPLICATION;

            $arSettings = self::PrepareSettings($arUserField);
            $arSymbols = self::GetSymbols($arSettings);

            $strResult = '';

            $mxResultValue = self::GetValueForAutoCompleteMulti($arSettings["IBLOCK_ID"], $arHtmlControl["VALUE"], $arSymbols['BAN_SYM'], $arSymbols['REP_SYM']);

            $strResultValue = (is_array($mxResultValue) ? htmlspecialcharsback(implode("\n", $mxResultValue)) : '');

            /*return $strResultValue.'<input type="hidden" name="' . $arHtmlControl["NAME"] . '" value="100">' .
            '<input type="hidden" name="' . $arHtmlControl["NAME"] . '" value="500">' .
            '<pre>ee' . print_r($arUserField, true) . print_r($arHtmlControl, true) . '</pre>';*/

            ob_start();
            ?><?
            $strRandControlID = $arHtmlControl["NAME"] . '_' . mt_rand(0, 10000);
            $control_id = $APPLICATION->IncludeComponent(
                "bitrix:main.lookup.input",
                "iblockedit",
                array(
                    "CONTROL_ID" => preg_replace("/[^a-zA-Z0-9_]/i", "x", $strRandControlID),
                    "INPUT_NAME" => $arHtmlControl["NAME"],
                    "INPUT_NAME_STRING" => "inp_" . $arHtmlControl["NAME"],
                    "INPUT_VALUE_STRING" => $strResultValue,
                    "START_TEXT" => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_INVITE'),
                    "MULTIPLE" => "Y",
                    "MAX_WIDTH" => $arSettings['MAX_WIDTH'],
                    "MIN_HEIGHT" => $arSettings['MIN_HEIGHT'],
                    "MAX_HEIGHT" => $arSettings['MAX_HEIGHT'],
                    "IBLOCK_ID" => $arSettings['IBLOCK_ID'],
                    'BAN_SYM' => $arSymbols['BAN_SYM_STRING'],
                    'REP_SYM' => $arSymbols['REP_SYM_STRING'],
                    'FILTER' => 'Y',
                ), null, array("HIDE_ICONS" => "Y")
            );
            ?><?
            if ('T' == $arSettings['VIEW']) {
                $name = $APPLICATION->IncludeComponent(
                    'bitrix:main.tree.selector',
                    'iblockedit',
                    array(
                        "INPUT_NAME" => $arHtmlControl["NAME"],
                        'ONSELECT' => 'jsMLI_' . $control_id . '.SetValue',
                        'MULTIPLE' => "Y",
                        'SHOW_INPUT' => 'N',
                        'SHOW_BUTTON' => 'Y',
                        'GET_FULL_INFO' => 'Y',
                        "START_TEXT" => GetMessage("BT_UT_EAUTOCOMPLETE_MESS_LIST_INVITE"),
                        'BUTTON_CAPTION' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_CHOOSE_ELEMENT'),
                        'BUTTON_TITLE' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_CHOOSE_ELEMENT_MULTI_DESCR'),
                        "NO_SEARCH_RESULT_TEXT" => GetMessage("BT_UT_EAUTOCOMPLETE_MESS_NO_SEARCH_RESULT_TEXT"),
                        "IBLOCK_ID" => $arSettings["IBLOCK_ID"],
                        'BAN_SYM' => $arSymbols['BAN_SYM_STRING'],
                        'REP_SYM' => $arSymbols['REP_SYM_STRING'],
                    ), null, array("HIDE_ICONS" => "Y")
                );
                ?><?
            } elseif ('E' == $arSettings['VIEW']) {
                ?><?
                $APPLICATION->IncludeComponent(
                    'bitrix:iblock.button.element.search',
                    'iblockedit',
                    array(
                        'ONSELECT' => 'jsMLI_' . $control_id,
                        'MULTIPLE' => "Y",
                        'LANG' => LANGUAGE_ID,
                        "IBLOCK_ID" => $arSettings["IBLOCK_ID"],
                        'BUTTON_CAPTION' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_SEARCH_ELEMENT'),
                        'BUTTON_TITLE' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_SEARCH_ELEMENT_MULTI_DESCR'),
                    ), null, array("HIDE_ICONS" => "Y")
                );
                ?><?
            }
            if ('Y' == $arSettings['SHOW_ADD']) {
                $strButtonCaption = '';
                if ('Y' == $arSettings['IBLOCK_MESS']) {
                    $arLangMess = CIBlock::GetMessages($arSettings["IBLOCK_ID"]);
                    $strButtonCaption = $arLangMess['ELEMENT_ADD'];
                } else {
                    $strButtonCaption = GetMessage('BT_UT_EAUTOCOMPLETE_MESS_NEW_ELEMENT');
                }
                ?><?
                $APPLICATION->IncludeComponent(
                    'bitrix:iblock.button.element.add',
                    'iblockedit',
                    array(
                        'LANG' => LANGUAGE_ID,
                        "IBLOCK_ID" => $arSettings["IBLOCK_ID"],
                        "ONSELECT" => 'jsMLI_' . $control_id,
                        "MULTIPLE" => "Y",
                        'BUTTON_CAPTION' => $strButtonCaption,
                        'BUTTON_TITLE' => GetMessage('BT_UT_EAUTOCOMPLETE_MESS_NEW_ELEMENT_MULTI_DESCR'),
                    ), null, array("HIDE_ICONS" => "Y")
                );
                ?><?
            }
            $strResult = ob_get_contents();
            ob_end_clean();

            return $strResult;
        }
}

?>