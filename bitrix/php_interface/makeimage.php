<?php
/**
 * Генерация превьюшек для больших изображений
 *
 * @param string $src путь от корня сайта к исходной картинке
 * @param array $params массив параметров phpThumb
 * @return string
 */
function MakeImage($src, $params = "")
{
    if (is_numeric($src) && (int) $src > 0) {
        $src = CFile::GetPath($src);
    }

    if (file_exists($_SERVER['DOCUMENT_ROOT'].$src)) {
        $ext = pathinfo($_SERVER['DOCUMENT_ROOT'].$src, PATHINFO_EXTENSION);
        $base_name = basename($src, ".".$ext);
        if (!defined("MAKEIMAGE_CODE_GEN_FUNCTION")) {
            define ("MAKEIMAGE_CODE_GEN_FUNCTION", false);
        }

        switch (MAKEIMAGE_CODE_GEN_FUNCTION) { // filesize || md5_file
            case "filesize":
                $code = md5(serialize($params).filesize($_SERVER['DOCUMENT_ROOT'].$src));
                break;
            case "md5_file":
                $code = md5(serialize($params).md5_file($_SERVER['DOCUMENT_ROOT'].$src));
                break;
            default:
                $code = md5(serialize($params).$_SERVER['DOCUMENT_ROOT'].$src);
                break;
        }
        $thumb_file = dirname($src)."/".$base_name."_thumb_".$code.".".$ext;

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$thumb_file)) {
            return $thumb_file;
        } else {
            // Подключаем и иннициализируем phpThumb
            require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/php_interface/include/phpThumb/phpthumb.class.php");
            $phpThumb = new phpThumb();
            $phpThumb->config_allow_src_above_docroot = true;
            $phpThumb->src = $src;
            switch ($ext) {
                case "jpg":
                    $phpThumb->f = "jpeg";
                    break;
                case "gif":
                    $phpThumb->f = "gif";
                    break;
                case "png":
                    $phpThumb->f = "png";
                    break;
                default:
                    $phpThumb->f = "jpeg";
                    break;
            }
            $phpThumb->q = 80;
            $phpThumb->bg = "ffffff";
            $phpThumb->far = "C";
            $phpThumb->aoe = 0;
            if (is_array($params)) {
                foreach ($params as $param=>$value) {
                    $phpThumb->$param = $value;
                }
            }
            $phpThumb->GenerateThumbnail();
            $success = $phpThumb->RenderToFile($_SERVER['DOCUMENT_ROOT'].$thumb_file);

            if ($success) {
                return $thumb_file;
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
}
