<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $SUBSCRIBE_TEMPLATE_RUBRIC;
$SUBSCRIBE_TEMPLATE_RUBRIC=$arRubric;
global $APPLICATION;
?> 

<?$SUBSCRIBE_TEMPLATE_RESULT = $APPLICATION->IncludeComponent(
	"melcosoft:subscribe.news",
	"",
	Array(
		"SITE_ID" => $arRubric["SITE_ID"],
		"IBLOCK_TYPE" => "contentsite",
		"ID" => [NEWS_IBLOCK_ID, ARTICLE_IBLOCK_ID],
		"SORT_BY" => "ACTIVE_FROM",
		"SORT_ORDER" => "DESC",
	),
	null,
	array(
		"HIDE_ICONS" => "Y",
	)
);?>
 
 <?

if($SUBSCRIBE_TEMPLATE_RESULT)
	return array(
		"SUBJECT"=>'ГК «Мать и дитя» - Пресс-центр',
		"BODY_TYPE"=>"html",
		"CHARSET"=>"Windows-1251",
		"DIRECT_SEND"=>"Y",
		"FROM_FIELD"=>$SUBSCRIBE_TEMPLATE_RUBRIC["FROM_FIELD"],
	);
else
	return false;
?>