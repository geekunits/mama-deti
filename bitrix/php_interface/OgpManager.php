<?php

class OgpManager
{
    protected static $instance = null;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
    }

    public function add($name, $value)
    {
        global $APPLICATION;

        if (!is_array($value)) {
            $value = array($value);
        }

        $name = htmlspecialchars($name);
        foreach ($value as $v) {
            $APPLICATION->AddHeadString('<meta property="og:'.$name.'" content="'.htmlspecialchars($v).'" />');
        }
    }
}
