<?php

class UrlReplacer
{
    protected function normalizeUrl($url)
    {
        if (strpos($url, '//') === 0) {
            $url = 'http:'.$url;
        }

        return $url;
    }

    protected function buildUrl($parts)
    {
        return 
            $parts['scheme'].'://'
            .$parts['host']
            .(!empty($parts['port']) ? ':'.$parts['port'] : '')
            .$parts['path']
            .(!empty($parts['query']) ? '?'.$parts['query'] : '')
            .(!empty($parts['fragment']) ? '#'.$parts['fragment'] : '');
    }

    protected function handleHost($host)
    {
        if (!preg_match('/mamadeti\.ru$/', $host)) {
            return $host;
        }

        $cm = CityManager::getInstance();

        $cityId = $cm->getCityIdByDomain($host);
        $mobileDomain = $cm->getDomainByCityId($cityId, true);

        return $mobileDomain;
    }

    protected function handleStaticUrl($parts)
    {
        $parts['scheme'] = 'http';
        $parts['host'] = 'mamadeti.ru';

        return $this->buildUrl($parts);
    }

    protected function handleUrl($url)
    {
        $parts = parse_url($url);
        if (
            (!isset($parts['host']) || preg_match('/mamadeti\.ru$/', $parts['host']))
            && !empty($parts['path'])
            && !(strpos($parts['path'], '/upload/') === 0)
            && preg_match('/\.(jpe?g|gif|png|bmp|docx?|xlsx?|pdf|html?)$/i', $parts['path'])
        ) {
            return $this->handleStaticUrl($parts);
        }

        if (empty($parts['scheme'])) {
            return $url;
        }

        if (empty($parts['host'])) {
            return $url;
        }

        $parts['host'] = $this->handleHost($parts['host']);

        return $this->buildUrl($parts);
    }

    public function replaceDesktopLinks($text)
    {
        $text = preg_replace_callback('/(src|href)\s*=\s*"([^"]+)"/imu', function($matches) {
            $url = $this->normalizeUrl($matches[2]);
            $url = $this->handleUrl($url);

            return $matches[1].'="'.$this->handleUrl($url).'"';
        }, $text);

        return $text;
    }

    public static function toMobile($text)
    {
        $replacer = new self;

        $text = $replacer->replaceDesktopLinks($text);

        return $text;
    }  
}