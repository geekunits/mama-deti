<?php

class ClinicManager
{
    protected static $instance = null;

    protected $clinics = null;
    protected $currentClinicId = nulll;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    protected function __construct()
    {
        CModule::IncludeModule('iblock');
    }

    public function getClinics()
    {
        if ($this->clinics === null) {
            $rawClinics = CMamaDetiAPI::GetClinics([
                'PROPERTY_CITY' => CityManager::getInstance()->getCurrentCityId(),
            ], [
                'PROPERTY_ADDRESS',
                'PROPERTY_PHONE',
                'PROPERTY_CONTACT_EMAIL',
                'PROPERTY_COORD',
            ]);
            $this->clinics = [];
            foreach ($rawClinics as $clinic) {
                $this->clinics[$clinic['ID']] = $clinic;
            }
        }

        return $this->clinics;
    }

    public function getClinicById($clinicId)
    {
        $clinics = $this->getClinics();

        return $clinics[$clinicId];
    }

    public function getClinicByCode($clinicCode) {
        $clinics = $this->getClinics();
        foreach($clinics as $clinic) {
            if ($clinic['CODE'] === $clinicCode) {
                return $clinic;
            }
        }
        return null;
    }

    public function setCurrentClinicId($clinicId)
    {
        $this->currentClinicId = $clinicId;
    }

    public function getCurrentClinic()
    {
        $clinics = $this->getClinics();

        return isset($clinics[$this->currentClinicId]) ? $clinics[$this->currentClinicId] : null;

    }
}
