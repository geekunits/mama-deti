<?php

class CMamaDetiAPI
{
    /*
     * $arSelectedServices = список ID элементов которые выбраны, по ним будут включенны разделы в меню
     * $includeElementsToMenu = включать в меню элементы
     */
    public static function GetSelectedServiceTreeMenu($arSelectedServices, $includeElementsToMenu = true, $maxDepth = false)
    {
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "selected_service_tree_menu_" . serialize(array($arSelectedServices, $includeElementsToMenu, $maxDepth)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $aMenuLinksNew = $vars[0];
        } else {
            $aMenuLinksNew = array();

            $arFilter = array("IBLOCK_ID" => 1, "GLOBAL_ACTIVE" => "Y", "ACTIVE" => "Y");

            $arSelect = array(
                "ID",
                "DEPTH_LEVEL",
                "NAME",
                "SECTION_PAGE_URL",
                "IBLOCK_SECTION_ID",
            );

            if ($maxDepth)
                $arFilter["<=DEPTH"] = $maxDepth;

            $arSections = array();

            $previousDepthLevel = 1;
            $previousSectionID = 0;

            $rsSection = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, false, $arSelect);
            while ($arSection = $rsSection->GetNext()) {
                if ($previousSectionID > 0)
                    $arSections[$previousSectionID]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
                $previousDepthLevel = $arSection["DEPTH_LEVEL"];
                $previousSectionID = $arSection["ID"];
                $arSections[$arSection["ID"]] = array(
                    "ID" => $arSection["ID"],
                    "~NAME" => $arSection["~NAME"],
                    "~SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"],
                    "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
                    "IBLOCK_SECTION_ID" => $arSection["IBLOCK_SECTION_ID"],
                    "TOTAL_CNT" => 0,
                );
            }

            $arSelect = array("ID", "NAME", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID");
            $rsElement = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "ID" => $arSelectedServices), false, false, $arSelect);
            while ($arElement = $rsElement->GetNext()) {
                if (!array_key_exists($arElement["IBLOCK_SECTION_ID"], $arSections))
                    continue;
                $arSections[$arElement["IBLOCK_SECTION_ID"]]["ELEMENTS"][] = $arElement;

                $PARENT_SECTION_ID = $arElement["IBLOCK_SECTION_ID"];
                while ($PARENT_SECTION_ID > 0) {
                    $arSections[$PARENT_SECTION_ID]["TOTAL_CNT"]++;
                    $PARENT_SECTION_ID = intval($arSections[$PARENT_SECTION_ID]["IBLOCK_SECTION_ID"]);
                }
            }

            $menuIndex = 0;
            $previousDepthLevel = 1;
            foreach ($arSections as $arSection) {
                if ($arSection["TOTAL_CNT"] == 0)
                    continue;
                if ($menuIndex > 0)
                    $aMenuLinksNew[$menuIndex - 1]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
                $previousDepthLevel = $arSection["DEPTH_LEVEL"];

                $aMenuLinksNew[$menuIndex++] = array(
                    "TEXT" => htmlspecialcharsbx($arSection["~NAME"]),
                    "LINK" => $arSection["~SECTION_PAGE_URL"],
                    "FROM_IBLOCK" => true,
                    "IS_PARENT" => false,
                    "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
                    'SECTION_ID' => $arSection['ID'],
                );

                if ($includeElementsToMenu && count($arSection["ELEMENTS"]) > 0 && (!$maxDepth || $arSection["DEPTH_LEVEL"] + 1 <= $maxDepth)) {

                    $aMenuLinksNew[$menuIndex - 1]["IS_PARENT"] = true;
                    $previousDepthLevel = $arSection["DEPTH_LEVEL"] + 1;

                    foreach ($arSection["ELEMENTS"] as $arElement) {
                        $aMenuLinksNew[$menuIndex++] = array(
                            "TEXT" => htmlspecialcharsbx($arElement["~NAME"]),
                            "LINK" => $arElement["~DETAIL_PAGE_URL"],
                            "FROM_IBLOCK" => true,
                            "IS_PARENT" => false,
                            "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"] + 1,
                            'ELEMENT_ID' => $arElement['ID'],
                        );
                    }
                }

            }
            
            if ($obCache->StartDataCache()) {
                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache($cache_dir);
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . 1);
                    $CACHE_MANAGER->EndTagCache();
                }
                $obCache->EndDataCache(array($aMenuLinksNew));
            }
        }

        return $aMenuLinksNew;
    }

    public static function GetSelectedServiceTree($arSelectedServices, $includeElementsToMenu = true, $maxDepth = false)
    {
        if (!$arSelectedServices) {
            return array();
        }

        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "selected_service_tree_" . serialize(array($arSelectedServices, $includeElementsToMenu, $maxDepth)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $aMenuLinksNew = $vars[0];
        } else {
            $aMenuLinksNew = array();

            $arFilter = array("IBLOCK_ID" => 1, "GLOBAL_ACTIVE" => "Y", "ACTIVE" => "Y");

            $arSelect = array(
                "ID",
                "DEPTH_LEVEL",
                "NAME",
                "SECTION_PAGE_URL",
                "IBLOCK_SECTION_ID",
            );

            if ($maxDepth)
                $arFilter["<=DEPTH"] = $maxDepth;

            $arSections = array();

            $previousDepthLevel = 1;
            $previousSectionID = 0;

            $rsSection = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, false, $arSelect);
            while ($arSection = $rsSection->GetNext()) {
                if ($previousSectionID > 0)
                    $arSections[$previousSectionID]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
                $previousDepthLevel = $arSection["DEPTH_LEVEL"];
                $previousSectionID = $arSection["ID"];
                $arSections[$arSection["ID"]] = array(
                    "ID" => $arSection["ID"],
                    "~NAME" => $arSection["~NAME"],
                    "~SECTION_PAGE_URL" => $arSection["~SECTION_PAGE_URL"],
                    "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
                    "IBLOCK_SECTION_ID" => $arSection["IBLOCK_SECTION_ID"],
                    "TOTAL_CNT" => 0,
                );
            }

            $arSelect = array("ID", "NAME", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID");
            $rsElement = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "ID" => $arSelectedServices), false, false, $arSelect);
            while ($arElement = $rsElement->GetNext()) {
                if (!array_key_exists($arElement["IBLOCK_SECTION_ID"], $arSections))
                    continue;
                $arSections[$arElement["IBLOCK_SECTION_ID"]]["ELEMENTS"][] = $arElement;

                $PARENT_SECTION_ID = $arElement["IBLOCK_SECTION_ID"];
                while ($PARENT_SECTION_ID > 0) {
                    $arSections[$PARENT_SECTION_ID]["TOTAL_CNT"]++;
                    $PARENT_SECTION_ID = intval($arSections[$PARENT_SECTION_ID]["IBLOCK_SECTION_ID"]);
                }
            }

            $menuIndex = 0;
            $previousDepthLevel = 1;
            foreach ($arSections as $arSection) {
                if ($arSection["TOTAL_CNT"] == 0)
                    continue;
                if ($menuIndex > 0)
                    $aMenuLinksNew[$menuIndex - 1]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
                $previousDepthLevel = $arSection["DEPTH_LEVEL"];

                $aMenuLinksNew[$menuIndex++] = array(
                    "ID" => "S" . $arSection["ID"],
                    "TEXT" => htmlspecialcharsbx($arSection["~NAME"]),
                    "LINK" => $arSection["~SECTION_PAGE_URL"],
                    "FROM_IBLOCK" => true,
                    "IS_PARENT" => false,
                    "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
                );

                if ($includeElementsToMenu && count($arSection["ELEMENTS"]) > 0 && (!$maxDepth || $arSection["DEPTH_LEVEL"] + 1 <= $maxDepth)) {

                    $aMenuLinksNew[$menuIndex - 1]["IS_PARENT"] = true;
                    $previousDepthLevel = $arSection["DEPTH_LEVEL"] + 1;

                    foreach ($arSection["ELEMENTS"] as $arElement) {
                        $aMenuLinksNew[$menuIndex++] = array(
                            "ID" => "E" . $arElement["ID"],
                            "TEXT" => htmlspecialcharsbx($arElement["~NAME"]),
                            "LINK" => $arElement["~DETAIL_PAGE_URL"],
                            "FROM_IBLOCK" => true,
                            "IS_PARENT" => false,
                            "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"] + 1,
                        );
                    }
                }

            }
            
            
            if ($obCache->StartDataCache()) {
                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache($cache_dir);
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . 1);
                    $CACHE_MANAGER->EndTagCache();
                }
                $obCache->EndDataCache(array($aMenuLinksNew));
            }
        }

        return $aMenuLinksNew;
    }

    public static function getServicesID($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y", 'SECTION_GLOBAL_ACTIVE' => 'Y', 'SECTION_ACTIVE' => 'Y');
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "services_id_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("NAME" => "ASC","SORT"=>"ASC"), array_merge($arrFilter, $arFilter),false,false,array("ID"));
            while ($arElement = $rsElement->Fetch())
                $arRes[] = $arElement["ID"];

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 1);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    /*Получение ID разделов услуг по ID элементов*/
    public static function getServicesIblockSection($arID)
    {
        $arFilter = array("IBLOCK_ID" => 1, "ACTIVE" => "Y", 'GLOBAL_ACTIVE' => 'Y');
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "services_iblock_section_" . serialize($arID), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arSectionID = array();
            $rsElement = CIBlockElement::GetList(array("NAME" => "ASC","SORT"=>"ASC"), array("IBLOCK_ID"=>1,"ID"=>$arID),array("IBLOCK_SECTION_ID"));
            while ($arElement = $rsElement->Fetch())
                $arSectionID[] = $arElement["IBLOCK_SECTION_ID"];

            $arRes = $arSectionID;

            while (!empty($arSectionID)) {
                $rsSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ID"=>$arSectionID,">DEPTH_LEVEL"=>1),false,array("ID","IBLOCK_SECTION_ID"));
                $arSectionID = array();
                while ($arSection = $rsSection->Fetch()) {
                    if (intval($arSection["IBLOCK_SECTION_ID"])>0) {
                        $arSectionID[] = $arSection["IBLOCK_SECTION_ID"];
                        if (!in_array($arSection["IBLOCK_SECTION_ID"], $arRes))
                            $arRes[] = $arSection["IBLOCK_SECTION_ID"];
                    }
                }

            }

            if ($obCache->StartDataCache()) {
                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache($cache_dir);
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . 1);
                    $CACHE_MANAGER->EndTagCache();
                }
                $obCache->EndDataCache(array($arRes));
            }
        
        }

        return $arRes;
    }

    public static function getClinicsID($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 2, "ACTIVE" => "Y");
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "clinics_id_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("NAME" => "ASC","SORT"=>"ASC"), array_merge($arrFilter, $arFilter),false,false,array("ID","IBLOCK_ID","PROPERTY_CITY","PROPERTY_SERVICES"));
            while ($arElement = $rsElement->Fetch()) {
                $arRes[$arElement["ID"]] = $arElement["ID"];
            }
            $arRes = array_values($arRes);

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 2);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;

    }

    public static function getSocialGroupId($url, $type)
    {
        $cache_dir = '/';
        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "clinic_vk_group_id_".$url, $cache_dir)) {
            $vars = $obCache->GetVars();
            $gid = $vars[0];
        } else {
            preg_match("/([^\/]+)$/", $url, $name);
            if (strpos($name[0], 'club') !== false || strpos($name[0], 'public') !== false) {
                $gid = str_replace(array('club','public'), '', $name[0]);
            } else {
                if ($type == 'vk') {
                    $group = json_decode(file_get_contents('https://api.vk.com/method/groups.getById?group_id='.$name[0]));
                    $gid = !empty($group->response[0]->gid) ? $group->response[0]->gid : false;
                } elseif ($type == 'ok') {
                    $gid = $name[0];
                }
                /*} else {
                    $group = json_decode(file_get_contents('http://api.odnoklassniki.ru/group.getInfo?uids&fields=group.id'));
                }*/
            }

            if ($gid) {
                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache($cache_dir);
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . 2);
                    $CACHE_MANAGER->EndTagCache();
                }
                $obCache->EndDataCache(array($gid));
            }
        }

        return $gid;
    }

    public static function getClinics($arrFilter = array(), $arrSelect = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 2, "ACTIVE" => "Y");
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "clinic_id_3_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(
                array("NAME" => "ASC","SORT"=>"ASC"), 
                array_merge($arrFilter, $arFilter),
                false,
                false,
                array("ID","IBLOCK_ID","NAME", 'DETAIL_PAGE_URL', "PROPERTY_CITY")
            );
            while ($oElement = $rsElement->GetNextElement()) {                                                                 
                $el = $oElement->GetFields();
                $el['PROPERTIES'] = $oElement->GetProperties();
                if (!empty($arrSelect)) {
                    foreach ($arrSelect as $name) {
                        $el[$name] = $el['PROPERTIES'][preg_replace('/^PROPERTY_/', '', $name)]['VALUE'];
                    }
                } else {
                    //$el = $arElement;
                }
                $arRes[] = $el;
            }
            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 2);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    public static function getClinicsServicesID($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => CLINIC_IBLOCK_ID, "ACTIVE" => "Y", "!PROPERTY_SERVICES" => false);
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "clinics_services_id_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array(), array_merge($arrFilter, $arFilter), array("PROPERTY_SERVICES.ID"));
            while ($arElement = $rsElement->Fetch())
                $arRes[$arElement["PROPERTY_SERVICES_ID"]] = 1;

            $arRes = array_keys($arRes);

            $rsElement =
                CIBlockElement::GetList(
                    array(),
                    array(
                        'IBLOCK_ID' => SERVICE_IBLOCK_ID,
                        'ACTIVE' => 'Y',
                        'SECTION_GLOBAL_ACTIVE' => 'Y',
                        'ID' => $arRes
                    ),
                    false,
                    false,
                    array('ID')
                );
            $arRes = [];
            while ($arElement = $rsElement->Fetch())
                $arRes[$arElement["ID"]] = 1;

            $arRes = array_keys($arRes);

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . SERVICE_IBLOCK_ID);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));
        }

        return $arRes;
    }

    public static function getClinicsServiceSectionsID($arrFilter = array())
    {
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "clinics_service_sections_id_" . serialize($arrFilter), $cache_dir)) {
            $vars = $obCache->GetVars();
            $clinicServiceSectionIds = $vars[0];
        } else {

            CModule::IncludeModule("iblock");
            $clinicServiceIds = self::getClinicsServicesID($arrFilter);

            // Выбираем все секции привязанных к клинике услуг.
            $obElement = new CIBlockElement;
            $rsServices =
                $obElement->GetList(
                    [],
                    ['IBLOCK_ID' => SERVICE_IBLOCK_ID, 'ID' => $clinicServiceIds, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'],
                    false,
                    false,
                    ['IBLOCK_ID', 'IBLOCK_SECTION_ID', 'ID']
                );
            $clinicServiceSectionIds = [];
            while ($arService = $rsServices->GetNext()) {
                $clinicServiceSectionIds[] = $arService['IBLOCK_SECTION_ID'];
            }
            unset($rsServices);
            $clinicServiceSectionIds = array_unique($clinicServiceSectionIds);

            // Выбираем всех родителей найденных секций.
            $obSection = new CIBlockSection;
            $parentClinicServiceSectionIds = [];
            foreach ($clinicServiceSectionIds as $clinicServiceSectionId) {
                // Если идентификатор секции уже есть в массиве родительских идентификаторов, не делаем выборку
                // родителей этой секции.
                if (in_array($clinicServiceSectionId, $parentClinicServiceSectionIds)) {
                    continue;
                }

                $rsNav = $obSection->GetNavChain(SERVICE_IBLOCK_ID, $clinicServiceSectionId);
                while ($arSection = $rsNav->GetNext()) {
                    if ($arSection['ID'] == $clinicServiceSectionId) {
                        continue;
                    }

                    $parentClinicServiceSectionIds[] = $arSection['ID'];
                }
            }
            $clinicServiceSectionIds = array_merge($clinicServiceSectionIds, $parentClinicServiceSectionIds);
            $clinicServiceSectionIds = array_unique($clinicServiceSectionIds);

            if ($obCache->StartDataCache()) {
                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache($cache_dir);
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . SERVICE_IBLOCK_ID);
                    $CACHE_MANAGER->EndTagCache();
                }
                $obCache->EndDataCache(array($clinicServiceSectionIds));
            }
        }

        return $clinicServiceSectionIds;
    }

    /*
     * Возвращает услуги отфильтрованные по arrFilter в формате массива
     *
    [1396] => Абляция эндометрия
    [1507] => Амниоцентез
    [1509] => Аппендэктомия
    [1768] => Беременных женщин
    [1506] => Биопсия хориона
     */
    public static function getDoctorsServices($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 3, "ACTIVE" => "Y", "!PROPERTY_SERVICES" => false);
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "doctors_services_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("PROPERTY_SERVICES.NAME" => "ASC"), array_merge($arrFilter, $arFilter), array("PROPERTY_SERVICES.ID", "PROPERTY_SERVICES.NAME"));
            while ($arElement = $rsElement->Fetch())
                $arRes[$arElement["PROPERTY_SERVICES_ID"]] = $arElement["PROPERTY_SERVICES_NAME"];

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 3);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    public static function getDoctorsID($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 3, "ACTIVE" => "Y");
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "doctors_id_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("NAME" => "ASC"), array_merge($arrFilter, $arFilter), array("ID"));
            while ($arElement = $rsElement->Fetch())
                $arRes[] = $arElement["ID"];

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 3);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    /*
     * Возвращает список врачей отфильтрованные по arrFilter в формате массива
     */
    public static function getDoctors($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => DOCTOR_IBLOCK_ID, "ACTIVE" => "Y");
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "doctors_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("NAME" => "ASC"), array_merge($arrFilter, $arFilter));
            while ($arElement = $rsElement->GetNext())
                $arRes[] = $arElement;

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 3);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    /*
     * Возвращает список первых букв названия врачей отфильтрованные по arrFilter в формате массива
     */

    public static function getDoctorsFirstAlphabet($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 3, "ACTIVE" => "Y");
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "doctors_f_alphabet" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("NAME" => "ASC"), array_merge($arrFilter, $arFilter), array("NAME"));
            while ($arElement = $rsElement->Fetch()) {
                $a = mb_strtoupper(mb_substr($arElement["NAME"], 0, 1));
                if (!in_array($a, $arRes))
                    $arRes[] = $a;
            }

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 3);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    /*
     * Возвращает клиники отфильтрованные по arrFilter в формате массива
     *
    [425] => Клиника «Мать и дитя» - Клиника Здоровья
    [423] => Клиника «Мать и дитя» Кунцево
    [427] => Клиника «Мать и дитя» Новогиреево
    [426] => Клиника «Мать и дитя» Сокол
    [424] => Клиника «Мать и дитя» Юго-Запад
    [421] => Клинический госпиталь «Лапино»
    [422] => Перинатальный медицинский центр (ПМЦ)
*/

    public static function getDoctorsClinics($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 3, "ACTIVE" => "Y", "!PROPERTY_CLINIC" => false);
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "doctors_clinics_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {
            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("PROPERTY_CLINIC.NAME" => "ASC"), array_merge($arrFilter, $arFilter), array("PROPERTY_CLINIC.ID", "PROPERTY_CLINIC.NAME"));
            while ($arElement = $rsElement->Fetch())
                $arRes[$arElement["PROPERTY_CLINIC_ID"]] = $arElement["PROPERTY_CLINIC_NAME"];

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 3);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    /*
     * Возвращает специальности отфильтрованные по arrFilter в формате массива
     *
    [728] => Акушер-гинеколог
    [1028] => Аллерголог
    [847] => Андролог
    [1748] => Анестезиолог-реаниматолог
    [2101] => Врач лобарант
    [1841] => Врач общей практики
    [755] => Гастроэнтеролог
    [798] => Гематолог
         */

    public static function getDoctorsSpecialty($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => 3, "ACTIVE" => "Y", "!PROPERTY_SPECIALTY" => false);
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "doctors_specialty_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(array("PROPERTY_SPECIALTY.NAME" => "ASC"), array_merge($arrFilter, $arFilter), array("PROPERTY_SPECIALTY.ID", "PROPERTY_SPECIALTY.NAME"));
            while ($arElement = $rsElement->GetNext())
                $arRes[$arElement["PROPERTY_SPECIALTY_ID"]] = $arElement["PROPERTY_SPECIALTY_NAME"];

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . 3);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    public static function getProgramDirections($arrFilter = array())
    {
        if (!is_array($arrFilter))
            $arrFilter = array();

        $arFilter = array("IBLOCK_ID" => PROGRAM_IBLOCK_ID, "ACTIVE" => "Y", "!PROPERTY_DIRECTION" => false);
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "program_directions_" . serialize(array_merge($arrFilter, $arFilter)), $cache_dir)) {
            $vars = $obCache->GetVars();
            $arRes = $vars[0];
        } else {

            CModule::IncludeModule("iblock");

            $arRes = array();
            $rsElement = CIBlockElement::GetList(
                array("PROPERTY_SPECIALTY.NAME" => "ASC"), 
                array_merge($arrFilter, $arFilter), 
                false,
                false,
                array("IBLOCK_ID", "ID", "PROPERTY_DIRECTION")
            );
            $directionIds = [];
            while ($oElement = $rsElement->GetNextElement()) {
                $props = $oElement->getProperties();
                $directionIds = array_merge($directionIds, $props['DIRECTION']['VALUE']);
            }

            $rsElement = CIBlockElement::GetList(
                array('NAME' => 'ASC'),
                array('IBLOCK_ID' => PROGRAM_DIRECTION_IBLOCK_ID, 'ID' => $directionIds)
            );
            while ($arElement = $rsElement->GetNext()) {
                $arRes[$arElement['ID']] = $arElement['NAME'];
            }

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);
                $CACHE_MANAGER->RegisterTag("iblock_id_" . PROGRAM_IBLOCK_ID);
                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($arRes));

        }

        return $arRes;
    }

    public static function getDoctorStats($doctorId)
    {
        $cache_dir = '/';

        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "doctors_stats_" . $doctorId, $cache_dir)) {
            $vars = $obCache->GetVars();
            $stats = $vars[0];
        } else {
            CModule::IncludeModule("iblock");

            $arFilter = Array("IBLOCK_ID"=>REVIEW_IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$doctorId);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
            $stats["REVIEW"] = $res->SelectedRowsCount();

            $arFilter = Array("IBLOCK_ID"=>STORY_IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$doctorId);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
            $stats["STORY"] = $res->SelectedRowsCount();

            $arFilter = Array("IBLOCK_ID"=>QA_IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_DOCTOR"=>$doctorId);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
            $stats["QA"] = $res->SelectedRowsCount();

            $arFilter = Array("IBLOCK_ID"=>GALLERY_IBLOCK_ID, "ACTIVE"=>"Y", "PROPERTY_DOCTORS"=>$doctorId);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array("ID"));
            $stats["PHOTO"] = $res->SelectedRowsCount();

            if (defined("BX_COMP_MANAGED_CACHE")) {
                global $CACHE_MANAGER;
                $CACHE_MANAGER->StartTagCache($cache_dir);

                $iblockIds = [
                    DOCTOR_IBLOCK_ID,
                    REVIEW_IBLOCK_ID,
                    STORY_IBLOCK_ID,
                    QA_IBLOCK_ID,
                    GALLERY_IBLOCK_ID
                ];
                foreach ($iblockIds as $iblockId) {
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . $iblockId);
                }

                $CACHE_MANAGER->EndTagCache();
            }
            $obCache->EndDataCache(array($stats));
        }

        return $stats;
    }

    public static function hasActionsInRegion($regionId = null)
    {
        if (empty($regionId)) {
            $regionId = intval($GLOBALS['CITY_ID']);
        }
        $arClinicIds = static::getClinicsID(array('PROPERTY_CITY' => $regionId));
        if (empty($arClinicIds)) {
            return false;
        }
        $arActionFilter = array(
            'IBLOCK_ID' => 32,
            'ACTIVE' => 'Y',
            'ACTIVE_DATE' => 'Y',
            'PROPERTY_CLINIC' =>$arClinicIds,
        );
        $count = intval(CIBlockElement::GetList(array('SORT' => 'ASC'), $arActionFilter, array(), false, array('ID', 'IBLOCK_ID')));

        return $count > 0;
    }

    public static function getSectionParentIds($sectionId, $filterInactive = false)
    {
        $sectionIds = [];
        while ($sectionId) {
            $dbResult = CIBlockSection::GetById($sectionId);
            $arSection = $dbResult->GetNext();
            if (!empty($arSection) && !empty($arSection['IBLOCK_SECTION_ID'])) {
                $sectionId = $arSection['IBLOCK_SECTION_ID'];
                $sectionIds[] = $sectionId;
                if ($filterInactive && $arSection['ACTIVE'] === 'N') {
                    return false;
                }
            } else {
                break;
            }
        }

        return $sectionIds;
    }

    public static function getElementParentIds($elementId, $filterInactive = false)
    {
        $dbResult = CIBlockElement::GetById($elementId);
        $arElement = $dbResult->GetNext();
        $sectionIds = [];
        if (empty($arElement)) {
            return false;
        }
        $sectionId = $arElement['IBLOCK_SECTION_ID'];
        if (!empty($sectionId)) {
            $sectionIds[] = $sectionId;
            $parentIds = self::getSectionParentIds($sectionId, $filterInactive);
            if ($parentIds === false) {
                return false;
            }
            $sectionIds = array_merge($sectionIds, $parentIds);
        }

        return $sectionIds;
    }

    public static function resortPriceList($items, $sections) {
        $groups = [];
        $sorted = [];
        foreach($items as $item) {
            $topSectionId = $item['SECTION_NAV'][0];
            if (empty($groups[$topSectionId])) {
                $groups[$topSectionId] = [];
            }
            $groups[$topSectionId][] = $item;
        }
        uksort($groups, function($l, $r) use ($sections) {
            $lSection = $sections[$l];
            $rSection = $sections[$r];
            return strcmp($lSection['NAME'], $rSection['NAME']);
        });
        foreach($groups as $group) {
            foreach($group as $item) {
                $sorted[] = $item;
            }
        }
        return $sorted;
    }
    
    public static function getReviewDoctorFilter()
    {
        $cache_dir = '/';
        
        $obCache = new CPHPCache();
        if ($obCache->InitCache(60 * 60 * 24, "review_doctor_filter", $cache_dir)) {
            $vars = $obCache->GetVars();
            $filter = $vars[0];
        } else {
            CModule::IncludeModule("iblock");

            $arDoctorClinics = array();
            $filter = array();
            
            $rsElement = CIBlockElement::GetList(
	            array(),
	            array(
		            "IBLOCK_ID" => REVIEW_IBLOCK_ID,
		            "ACTIVE"=>"Y",
		            "!PROPERTY_DOCTOR"=>false
	            ),
	            array(
		            "ID",
		            "IBLOCK_ID",
		            "PROPERTY_DOCTOR",
		            "PROPERTY_DOCTOR.NAME",
		            "PROPERTY_DOCTOR.PROPERTY_CLINIC"
	            )
            );
            
            while ($arElement = $rsElement->Fetch()) {
	            $clinicId = $arElement['PROPERTY_DOCTOR_PROPERTY_CLINIC_VALUE'];
	            if (!isset($filter[$clinicId])) {
		            $filter[$clinicId] = array();
	            }

	            if(!isset($arDoctorClinics[$arElement['PROPERTY_DOCTOR_VALUE']])){
		            $arDoctorClinics[$arElement['PROPERTY_DOCTOR_VALUE']] = 
		                array_keys(CMamaDetiAPI::getDoctorsClinics(array('ID'=>$arElement['PROPERTY_DOCTOR_VALUE'])));
	            }
	
	            $filter[$clinicId][$arElement['PROPERTY_DOCTOR_VALUE']] = $arElement['PROPERTY_DOCTOR_NAME'];
	            if(count($arDoctorClinics[$arElement['PROPERTY_DOCTOR_VALUE']]) > 1){
		            foreach ($arDoctorClinics[$arElement['PROPERTY_DOCTOR_VALUE']] as $clinic) {
			            $filter[$clinic][$arElement['PROPERTY_DOCTOR_VALUE']] = $arElement['PROPERTY_DOCTOR_NAME'];
		            }
	            }
            }
            
            if ($obCache->StartDataCache()) {
                if (defined("BX_COMP_MANAGED_CACHE")) {
                    global $CACHE_MANAGER;
                    $CACHE_MANAGER->StartTagCache($cache_dir);

                    $iblockIds = [
                        REVIEW_IBLOCK_ID,
                    ];
                    foreach ($iblockIds as $iblockId) {
                        $CACHE_MANAGER->RegisterTag("iblock_id_" . $iblockId);
                    }

                    $CACHE_MANAGER->EndTagCache();
                }
                $obCache->EndDataCache(array($filter));
            }
        }
        
        return $filter;
    }

}
