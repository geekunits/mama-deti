<?php

class ProcedureManager
{
    protected static $instance = null;

    protected $groups = [];
    protected $items = [];

    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    protected function __construct()
    {
        CModule::IncludeModule('iblock');
    }

    protected function sanitizeCode($code)
    {
        $codeParts = explode('.', $code);
        $codeParts = array_filter($codeParts);
        $code = implode('.', $codeParts);

        return $code;
    }

    protected function getParentGroup($code)
    {
        $codeParts = explode('.', $code);

        if (count($codeParts) <= 1) {
            return null;
        }

        do {
            array_pop($codeParts);

            $parentCode = implode('.', $codeParts);
            $parentGroup = $this->getGroupByCode($parentCode);

        } while (count($codeParts) > 1 && !$parentGroup);

        return $parentGroup;
    }

    public function getGroupByCode($code)
    {
        $code = $this->sanitizeCode($code);

        if (empty($this->groups[$code])) {
            $ib = new CIBlockSection;
            $group = $ib->GetList([], ['IBLOCK_ID' => PROCEDURES_IBLOCK_ID, 'CODE' => $code])->GetNext();

            $this->groups[$code] = $group;
        }

        return $this->groups[$code];
    }

    public function getItemByCode($code)
    {
        $code = $this->sanitizeCode($code);

        if (empty($this->items[$code])) {
            $ib = new CIBlockElement;
            $item = $ib->GetList([], ['IBLOCK_ID' => PROCEDURES_IBLOCK_ID, 'CODE' => $code])->GetNext();

            $this->items[$code] = $item;
        }

        return $this->items[$code];
    }

    public function addGroup($code, $name)
    {
        $code = $this->sanitizeCode($code);

        $group = $this->getGroupByCode($code);
        if ($group) {
            if ($group['NAME'] != $name) {
                $ib = new CIBlockSection;
                $ib->Update($group['ID'], ['NAME' => $name]);

                $group['NAME'] = $name;
                $this->groups[$code] = $group;
            }

            return $group;
        }

        $parentGroup = $this->getParentGroup($code);

        $ib = new CIBlockSection;

        $ib->Add(array_filter([
            'IBLOCK_ID' => PROCEDURES_IBLOCK_ID,
            'IBLOCK_SECTION_ID' => $parentGroup ? $parentGroup['ID'] : null,
            'CODE' => $code,
            'NAME' => $name,
        ]));

        return $this->getGroupByCode($code);
    }

    public function addItem($code, $name)
    {
        $code = $this->sanitizeCode($code);

        $item = $this->getItemByCode($code);
        if ($item) {
            if ($item['NAME'] != $name) {
                $ib = new CIBlockElement;
                $ib->Update($item['ID'], ['NAME' => $name]);

                $item['NAME'] = $name;
                $this->items[$code] = $item;
            }

            return $item;
        }

        $parentGroup = $this->getParentGroup($code);

        $ib = new CIBlockElement;

        $ib->Add(array_filter([
            'IBLOCK_ID' => PROCEDURES_IBLOCK_ID,
            'IBLOCK_SECTION_ID' => $parentGroup ? $parentGroup['ID'] : null,
            'CODE' => $code,
            'NAME' => $name,
        ]));

        return $this->getItemByCode($code);
    }

}
