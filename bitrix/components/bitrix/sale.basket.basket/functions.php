<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Highloadblock as HL;

function checkQuantity($arBasketItem, $desiredQuantity)
{
	$arResult = array();

	/** @var $productProvider IBXSaleProductProvider */
	if ($productProvider = CSaleBasket::GetProductProvider($arBasketItem))
	{
		$arFieldsTmp = $productProvider::GetProductData(array(
			"PRODUCT_ID" => $arBasketItem["PRODUCT_ID"],
			"QUANTITY"   => $desiredQuantity,
			"RENEWAL"    => "N",
			"USER_ID"    => $userId,
			"SITE_ID"    => $siteId,
			"CHECK_QUANTITY" => "Y"
		));
	}
	else
	{
		$arFieldsTmp = CSaleBasket::ExecuteCallbackFunction(
			$arBasketItem["CALLBACK_FUNC"],
			$arBasketItem["MODULE"],
			$arBasketItem["PRODUCT_ID"],
			$desiredQuantity,
			"N",
			$userId,
			$siteId
		);
	}

	if (empty($arFieldsTmp) || !isset($arFieldsTmp["QUANTITY"]))
	{
		$arResult["ERROR"] = GetMessage("SBB_PRODUCT_NOT_AVAILABLE", array("#PRODUCT#" => $arBasketItem["NAME"]));
	}
	else if ($desiredQuantity > doubleval($arFieldsTmp["QUANTITY"]))
	{
		$arResult["ERROR"] = GetMessage("SBB_PRODUCT_NOT_ENOUGH_QUANTITY", array("#PRODUCT#" => $arBasketItem["NAME"], "#NUMBER#" => $desiredQuantity));
	}

	return $arResult;
}

function getSkuPropsData($arBasketItems, $arParents, $arSkuProps = array())
{
	$bUseHLIblock = CModule::IncludeModule('highloadblock');

	$arRes = array();
	$arSkuIblockID = array();

	if (is_array($arParents))
	{
		foreach ($arBasketItems as &$arItem)
		{
			if (array_key_exists($arItem["PRODUCT_ID"], $arParents))
			{
				$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParents[$arItem["PRODUCT_ID"]]["IBLOCK_ID"]);

				if (!array_key_exists($arSKU["IBLOCK_ID"], $arSkuIblockID))
					$arSkuIblockID[$arSKU["IBLOCK_ID"]] = $arSKU;

				$arItem["IBLOCK_ID"] = $arSKU["IBLOCK_ID"];
				$arItem["SKU_PROPERTY_ID"] = $arSKU["SKU_PROPERTY_ID"];
			}
		}
		unset($arItem);

		foreach ($arSkuIblockID as $skuIblockID => $arSKU)
		{
			// possible props values
			$rsProps = CIBlockProperty::GetList(
				array('SORT' => 'ASC', 'ID' => 'ASC'),
				array('IBLOCK_ID' => $skuIblockID, 'ACTIVE' => 'Y')
			);

			while ($arProp = $rsProps->Fetch())
			{
				if ($arProp['PROPERTY_TYPE'] == 'L' || $arProp['PROPERTY_TYPE'] == 'E' || ($arProp['PROPERTY_TYPE'] == 'S' && $arProp['USER_TYPE'] == 'directory'))
				{
					if ($arProp['XML_ID'] == 'CML2_LINK')
						continue;

					if (!in_array($arProp['CODE'], $arSkuProps))
						continue;

					$arRes[$skuIblockID][$arProp['ID']] = array(
						'ID' => $arProp['ID'],
						'CODE' => $arProp['CODE'],
						'NAME' => $arProp['NAME'],
						'TYPE' => $arProp['PROPERTY_TYPE'],
						'VALUES' => array()
					);

					if ($arProp['PROPERTY_TYPE'] == 'L')
					{
						$arValues = array();
						$rsPropEnums = CIBlockProperty::GetPropertyEnum($arProp['ID']);
						while ($arEnum = $rsPropEnums->Fetch())
						{
							$arValues[$arEnum['ID']] = array(
								'ID' => $arEnum['ID'],
								'NAME' => $arEnum['VALUE'],
								'PICT' => false
							);
						}

						$arRes[$skuIblockID][$arProp['ID']]['VALUES'] = $arValues;
					}
					elseif ($arProp['PROPERTY_TYPE'] == 'E')
					{
						$arValues = array();
						$rsPropEnums = CIBlockElement::GetList(
							array('SORT' => 'ASC'),
							array('IBLOCK_ID' => $arProp['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y'),
							false,
							false,
							array('ID', 'NAME', 'PREVIEW_PICTURE')
						);
						while ($arEnum = $rsPropEnums->Fetch())
						{
							$arEnum['PREVIEW_PICTURE'] = CFile::GetFileArray($arEnum['PREVIEW_PICTURE']);

							if (!is_array($arEnum['PREVIEW_PICTURE']))
								continue;

							$productImg = CFile::ResizeImageGet($arEnum['PREVIEW_PICTURE'], array('width'=>80, 'height'=>80), BX_RESIZE_IMAGE_PROPORTIONAL, false, false);

							$arEnum['PREVIEW_PICTURE']['SRC'] = $productImg['src'];

							$arValues[$arEnum['ID']] = array(
								'ID' => $arEnum['ID'],
								'NAME' => $arEnum['NAME'],
								'SORT' => $arEnum['SORT'],
								'PICT' => $arEnum['PREVIEW_PICTURE']
							);
						}

						$arRes[$skuIblockID][$arProp['ID']]['VALUES'] = $arValues;
					}
					elseif ($arProp['PROPERTY_TYPE'] == 'S' && $arProp['USER_TYPE'] == 'directory')
					{
						$arValues = array();
						if ($bUseHLIblock)
						{
							$hlblock = HL\HighloadBlockTable::getList(array("filter" => array("TABLE_NAME" => $arProp["USER_TYPE_SETTINGS"]["TABLE_NAME"])))->fetch();
							if ($hlblock)
							{
								$entity = HL\HighloadBlockTable::compileEntity($hlblock);
								$entity_data_class = $entity->getDataClass();
								$rsData = $entity_data_class::getList();

								while ($arData = $rsData->fetch())
								{
									$arValues[$arData['ID']] = array(
										'ID' => $arData['ID'],
										'NAME' => $arData['UF_NAME'],
										'SORT' => $arData['UF_SORT'],
										'FILE' => $arData['UF_FILE'],
										'PICT' => '',
										'XML_ID' => $arData['UF_XML_ID']
									);
								}

								$arRes[$skuIblockID][$arProp['ID']]['VALUES'] = $arValues;
							}
						}
					}
				}
			}
		}

		foreach ($arBasketItems as &$arItem)
		{
			$arSelectSkuProps = array();
			foreach ($arSkuProps as $prop)
				$arSelectSkuProps[] = "PROPERTY_".$prop;

			if (isset($arItem["IBLOCK_ID"]) && intval($arItem["IBLOCK_ID"]) > 0 && array_key_exists($arItem["IBLOCK_ID"], $arRes))
			{
				$arItem["SKU_DATA"] = $arRes[$arItem["IBLOCK_ID"]];

				$arUsedValues = array();
				$arTmpRes = array();

				$arOfFilter = array(
					"IBLOCK_ID" => $arItem["IBLOCK_ID"],
					"PROPERTY_".$arSkuIblockID[$arItem["IBLOCK_ID"]]["SKU_PROPERTY_ID"] => $arParents[$arItem["PRODUCT_ID"]]["PRODUCT_ID"]
				);

				$rsOffers = CIBlockElement::GetList(
					array(),
					$arOfFilter,
					false,
					false,
					array_merge(array("ID"), $arSelectSkuProps)
				);
				while ($arOffer = $rsOffers->GetNext())
				{
					foreach ($arSkuProps as $prop)
					{
						if (!empty($arOffer["PROPERTY_".$prop."_VALUE"]) &&
							(!is_array($arUsedValues[$arItem["PRODUCT_ID"]][$prop]) || !in_array($arOffer["PROPERTY_".$prop."_VALUE"], $arUsedValues[$arItem["PRODUCT_ID"]][$prop])))
							$arUsedValues[$arItem["PRODUCT_ID"]][$prop][] = $arOffer["PROPERTY_".$prop."_VALUE"];
					}
				}

				if (!empty($arUsedValues))
				{
					// add only used values to the item SKU_DATA
					foreach ($arRes[$arItem["IBLOCK_ID"]] as $propId => $arProp)
					{
						if (!array_key_exists($arProp["CODE"], $arUsedValues[$arItem["PRODUCT_ID"]]))
							continue;

						$arTmpRes[$propId] = array();
						foreach ($arProp["VALUES"] as $valId => $arValue)
						{
							// properties of various type have different values in the used values data
							if (($arProp["TYPE"] == "L" && in_array($arValue["NAME"], $arUsedValues[$arItem["PRODUCT_ID"]][$arProp["CODE"]]))
								|| ($arProp["TYPE"] == "E" && in_array($arValue["ID"], $arUsedValues[$arItem["PRODUCT_ID"]][$arProp["CODE"]]))
								|| ($arProp["TYPE"] == "S" && in_array($arValue["XML_ID"], $arUsedValues[$arItem["PRODUCT_ID"]][$arProp["CODE"]]))
							)
							{
								if ($arProp["TYPE"] == "S")
								{
									$arTmpFile = CFile::GetFileArray($arValue["FILE"]);
									$tmpImg = CFile::ResizeImageGet($arTmpFile, array('width'=>80, 'height'=>80), BX_RESIZE_IMAGE_PROPORTIONAL, false, false);
									$arValue['PICT']['SRC'] = $tmpImg['src'];
								}

								$arTmpRes[$propId]["CODE"] = $arProp["CODE"];
								$arTmpRes[$propId]["NAME"] = $arProp["NAME"];
								$arTmpRes[$propId]["VALUES"][$valId] = $arValue;
							}
						}
					}
				}

				$arItem["SKU_DATA"] = $arTmpRes;
			}
		}
		unset($arItem);
	}

	return $arBasketItems;
}

function getAvailableQuantity($arBasketItems)
{
	if (CModule::IncludeModule("catalog"))
	{
		$arElementId = array();
		foreach ($arBasketItems as $arItem)
			$arElementId[] = $arItem["PRODUCT_ID"];

		if (!empty($arElementId))
		{
			$dbres = CCatalogProduct::GetList(
				array(),
				array("ID" => array_unique($arElementId)),
				false,
				false,
				array("ID", "QUANTITY")
			);
			while ($arRes = $dbres->GetNext())
			{
				foreach ($arBasketItems as &$basketItem)
				{
					if ($basketItem["PRODUCT_ID"] == $arRes["ID"])
						$basketItem["AVAILABLE_QUANTITY"] = $arRes["QUANTITY"];
				}
				unset($basketItem);
			}
		}

		return $arBasketItems;
	}
	else
		return false;
}

?>