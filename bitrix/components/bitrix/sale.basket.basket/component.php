<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("sale"))
{
	ShowError(GetMessage("SALE_MODULE_NOT_INSTALL"));
	return;
}

$bUseCatalog = (CModule::IncludeModule("catalog")) ? true : false;
$bUseIblock = (CModule::IncludeModule("iblock")) ? true : false;

include(dirname(__FILE__)."/functions.php");

// PARAMETERS

$arParams["PATH_TO_ORDER"] = Trim($arParams["PATH_TO_ORDER"]);
if (strlen($arParams["PATH_TO_ORDER"]) <= 0)
	$arParams["PATH_TO_ORDER"] = "order.php";

if($arParams["SET_TITLE"] == "Y")
	$APPLICATION->SetTitle(GetMessage("SBB_TITLE"));

$arParams["HIDE_COUPON"] = (($arParams["HIDE_COUPON"] == "Y" || !$bUseCatalog) ? "Y" : "N");

$arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] = (($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N");

$arParams['PRICE_VAT_SHOW_VALUE'] = $arParams['PRICE_VAT_SHOW_VALUE'] == 'N' ? 'N' : 'Y';
$arParams["USE_PREPAYMENT"] = $arParams["USE_PREPAYMENT"] == 'Y' ? 'Y' : 'N';

$arParams["WEIGHT_UNIT"] = htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_unit', "", SITE_ID));
$arParams["WEIGHT_KOEF"] = htmlspecialcharsbx(COption::GetOptionString('sale', 'weight_koef', 1, SITE_ID));

// custom product table columns
$arResult["GRID"]["HEADERS"] = array();
$arResult["GRID"]["ROWS"] = array();

// default columns
if (!isset($arParams["COLUMNS_LIST"]) || !is_array($arParams["COLUMNS_LIST"]) || empty($arParams["COLUMNS_LIST"]))
	$arParams["COLUMNS_LIST"] = array("NAME", "DISCOUNT", "WEIGHT", "QUANTITY", "DELETE", "DELAY", "TYPE", "PRICE");

// required columns
if (!in_array("NAME", $arParams["COLUMNS_LIST"]))
	$arParams["COLUMNS_LIST"] = array_merge(array("NAME"), $arParams["COLUMNS_LIST"]);

if (!in_array("QUANTITY", $arParams["COLUMNS_LIST"]))
	$arParams["COLUMNS_LIST"][] = "QUANTITY";

if (!in_array("PRICE", $arParams["COLUMNS_LIST"]))
	$arParams["COLUMNS_LIST"][] = "PRICE";

$arCustomSelectFields = array();
$arIblockProps = array();
$propertyCount = 0;
define("PROPERTY_COUNT_LIMIT", 24); // too much properties cause sql join error

foreach ($arParams["COLUMNS_LIST"] as $key => $value) // making grid headers array
{
	if (strpos($value, "PROPERTY_") !== false)
	{
		$propertyCount++;
		if ($propertyCount > PROPERTY_COUNT_LIMIT)
			continue;

		$arCustomSelectFields[] = $value; // array of iblock properties to select
		$id = $value."_VALUE";

		if ($bUseIblock)
		{
			$dbres = CIBlockProperty::GetList(array(), array("CODE" => substr($value, 9)));
			if ($arres = $dbres->GetNext())
			{
				$name = $arres["NAME"];
				$arIblockProps[substr($value, 9)] = $arres;
			}
		}
	}
	else
	{
		$id = $value;
		$name = "";
	}

	$arColumn = array(
		"id" => $id,
		"name" => $name
	);

	$arResult["GRID"]["HEADERS"][] = $arColumn;
}

$arResult["WARNING_MESSAGE"] = array();

$GLOBALS['CATALOG_ONETIME_COUPONS_BASKET'] = null;
$GLOBALS['CATALOG_ONETIME_COUPONS_ORDER'] = null;

// BASKET REFRESH
if (strlen($_REQUEST["BasketRefresh"]) > 0 || strlen($_REQUEST["BasketOrder"]) > 0 || strlen($_REQUEST["action"]))
{
	// tmp hack until ajax recalculation is made
	if (isset($_REQUEST["BasketRefresh"]) && strlen($_REQUEST["BasketRefresh"]) > 0)
		unset($_REQUEST["BasketOrder"]);

	if (strlen($_REQUEST["action"]) > 0)
	{
		$id = intval($_REQUEST["id"]);
		if ($id > 0)
		{
			$dbBasketItems = CSaleBasket::GetList(
				array(),
				array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL",
					"ID" => $id,
				),
				false,
				false,
				array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "CURRENCY")
			);
			if ($arItem = $dbBasketItems->Fetch())
			{
				if (CSaleBasketHelper::isSetItem($arItem))
					continue;

				if ($_REQUEST["action"] == "delete" && in_array("DELETE", $arParams["COLUMNS_LIST"]))
				{
					CSaleBasket::Delete($arItem["ID"]);
				}
				elseif ($_REQUEST["action"] == "delay" && in_array("DELAY", $arParams["COLUMNS_LIST"]))
				{
					if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
						CSaleBasket::Update($arItem["ID"], array("DELAY" => "Y"));
				}
				elseif ($_REQUEST["action"] == "add" && in_array("DELAY", $arParams["COLUMNS_LIST"]))
				{
					if ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y")
						CSaleBasket::Update($arItem["ID"], array("DELAY" => "N"));
				}
				unset($_SESSION["SALE_BASKET_NUM_PRODUCTS"][SITE_ID]);
			}
		}

		LocalRedirect($APPLICATION->GetCurPage());
	}
	else
	{
		if ($arParams["HIDE_COUPON"] != "Y")
		{
			$COUPON = trim($_REQUEST["COUPON"]);
			if (strlen($COUPON) > 0)
			{
				$res = CCatalogDiscountCoupon::SetCoupon($COUPON);
				$arResult["VALID_COUPON"] = $res;
			}
			else
				CCatalogDiscountCoupon::ClearCoupon();
		}

		$arTmpItems = array();
		$dbBasketItems = CSaleBasket::GetList(
			array("PRICE" => "DESC"),
			array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL"
			),
			false,
			false,
			array(
				"ID", "NAME", "PRODUCT_PROVIDER_CLASS", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID",
				"QUANTITY", "DELAY", "CAN_BUY", "CURRENCY", "SUBSCRIBE", "TYPE", "SET_PARENT_ID"
			)
		);
		while ($arItem = $dbBasketItems->Fetch())
		{
			if (CSaleBasketHelper::isSetItem($arItem))
				continue;

			$arTmpItems[] = $arItem;
		}

		if (!empty($arTmpItems) && $bUseCatalog)
			$arTmpItems = getRatio($arTmpItems);

		foreach ($arTmpItems as &$arItem)
		{
			$arItem["QUANTITY"] = (isset($arItem["MEASURE_RATIO"]) && floatval($arItem["MEASURE_RATIO"]) > 0 && $arItem["MEASURE_RATIO"] != 1) ? doubleval($arItem["QUANTITY"]) : intval($arItem["QUANTITY"]);

			if(!isset($_REQUEST["QUANTITY_".$arItem["ID"]]) || doubleval($_REQUEST["QUANTITY_".$arItem["ID"]]) <= 0)
				$quantityTmp = $arItem["QUANTITY"];
			else
				$quantityTmp = (isset($arItem["MEASURE_RATIO"]) && floatval($arItem["MEASURE_RATIO"]) > 0 && $arItem["MEASURE_RATIO"] != 1) ? doubleval($_REQUEST["QUANTITY_".$arItem["ID"]]) : intval($_REQUEST["QUANTITY_".$arItem["ID"]]);

			if ($arItem["CAN_BUY"] == "Y")
			{
				$res = checkQuantity($arItem, $quantityTmp);
				if (!empty($res))
					$arResult["WARNING_MESSAGE"][] = $res["ERROR"];
			}

			$deleteTmp = ($_REQUEST["DELETE_".$arItem["ID"]] == "Y") ? "Y" : "N";
			$delayTmp = ($_REQUEST["DELAY_".$arItem["ID"]] == "Y") ? "Y" : "N";

			if ($deleteTmp == "Y" && in_array("DELETE", $arParams["COLUMNS_LIST"]))
			{
				if ($arItem["SUBSCRIBE"] == "Y" && is_array($_SESSION["NOTIFY_PRODUCT"][$USER->GetID()]))
				{
					unset($_SESSION["NOTIFY_PRODUCT"][$USER->GetID()][$arItem["PRODUCT_ID"]]);
				}
				CSaleBasket::Delete($arItem["ID"]);
			}
			elseif ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
			{
				unset($arFields);
				$arFields = array();
				if (in_array("QUANTITY", $arParams["COLUMNS_LIST"]))
					$arFields["QUANTITY"] = $quantityTmp;
				if (in_array("DELAY", $arParams["COLUMNS_LIST"]))
					$arFields["DELAY"] = $delayTmp;

				if (count($arFields) > 0
					&&
						($arItem["QUANTITY"] != $arFields["QUANTITY"] && in_array("QUANTITY", $arParams["COLUMNS_LIST"])
							|| $arItem["DELAY"] != $arFields["DELAY"] && in_array("DELAY", $arParams["COLUMNS_LIST"]))
					)
					CSaleBasket::Update($arItem["ID"], $arFields);
			}
			elseif ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y")
			{
				unset($arFields);
				$arFields = array();
				if (in_array("DELAY", $arParams["COLUMNS_LIST"]))
					$arFields["DELAY"] = $delayTmp;

				if (count($arFields) > 0
					&&
						($arItem["DELAY"] != $arFields["DELAY"] && in_array("DELAY", $arParams["COLUMNS_LIST"]))
					)
					CSaleBasket::Update($arItem["ID"], $arFields);
			}
		}
		unset($arItem);

		unset($_SESSION["SALE_BASKET_NUM_PRODUCTS"][SITE_ID]);

		if (strlen($_REQUEST["BasketOrder"]) > 0 && empty($arResult["WARNING_MESSAGE"]))
		{
			LocalRedirect($arParams["PATH_TO_ORDER"]);
		}
		else
		{
			unset($_REQUEST["BasketRefresh"]);
			unset($_REQUEST["BasketOrder"]);

			if (!empty($arResult["WARNING_MESSAGE"]))
				$_SESSION["SALE_BASKET_MESSAGE"] = $arResult["WARNING_MESSAGE"];

			LocalRedirect($APPLICATION->GetCurPage());
		}
	}
}

CSaleBasket::UpdateBasketPrices(CSaleBasket::GetBasketUserID(), SITE_ID);

$bShowReady = False;
$bShowDelay = False;
$bShowSubscribe = False;
$bShowNotAvail = False;
$allSum = 0;
$allWeight = 0;
$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);
$allVATSum = 0;

$arResult["ITEMS"]["AnDelCanBuy"] = array();
$arResult["ITEMS"]["DelDelCanBuy"] = array();
$arResult["ITEMS"]["nAnCanBuy"] = array();
$arResult["ITEMS"]["ProdSubscribe"] = array();
$DISCOUNT_PRICE_ALL = 0;

// BASKET PRODUCTS (including measures, ratio, iblock properties data)

$arImgFields = array("PREVIEW_PICTURE", "DETAIL_PICTURE");
$arBasketItems = array();
$arSku2Parent = array();
$arSetParentWeight = array();
$dbBasketItems = CSaleBasket::GetList(
	array("NAME" => "ASC", "ID" => "ASC"),
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL"
	),
	false,
	false,
	array(
		"ID", "NAME", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY",
		"PRICE", "WEIGHT", "DETAIL_PAGE_URL", "NOTES", "CURRENCY", "VAT_RATE", "CATALOG_XML_ID",
		"PRODUCT_XML_ID", "SUBSCRIBE", "DISCOUNT_PRICE", "PRODUCT_PROVIDER_CLASS", "TYPE", "SET_PARENT_ID"
	)
);
while ($arItem = $dbBasketItems->GetNext())
{
	$arBasketItems[] = $arItem;

	if (CSaleBasketHelper::isSetItem($arItem))
		continue;

	$arElementId[] = $arItem["PRODUCT_ID"];

	if ($bUseCatalog)
	{
		$arParent = CCatalogSku::GetProductInfo($arItem["PRODUCT_ID"]);

		if ($arParent)
		{
			$arElementId[] = $arParent["ID"];
			$arSku2Parent[$arItem["PRODUCT_ID"]] = $arParent["ID"];

			$arParents[$arItem["PRODUCT_ID"]]["PRODUCT_ID"] = $arParent["ID"];
			$arParents[$arItem["PRODUCT_ID"]]["IBLOCK_ID"] = $arParent["IBLOCK_ID"];
		}
	}
}

// get measures, ratio, sku props data and available quantity
if (!empty($arBasketItems) && $bUseCatalog)
{
	$arBasketItems = getMeasures($arBasketItems);
	$arBasketItems = getRatio($arBasketItems);
	$arBasketItems = getAvailableQuantity($arBasketItems);
}

// get product properties data
$arProductData = getProductProps($arElementId, array_merge(array("ID"), $arImgFields, $arCustomSelectFields));

foreach ($arBasketItems as &$arItem)
{
	$arItem['QUANTITY'] = (isset($arItem["MEASURE_RATIO"]) && floatval($arItem["MEASURE_RATIO"]) > 0 && $arItem["MEASURE_RATIO"] != 1) ? number_format(doubleval($arItem['QUANTITY']), 2, '.', '') : intval($arItem['QUANTITY']);

	$arItem["PROPS"] = array();

	$dbProp = CSaleBasket::GetPropsList(
		array("SORT" => "ASC", "ID" => "ASC"),
		array("BASKET_ID" => $arItem["ID"], "!CODE" => array("CATALOG.XML_ID", "PRODUCT.XML_ID"))
	);
	while ($arProp = $dbProp->GetNext())
		$arItem["PROPS"][] = $arProp;

	$arItem["PRICE_VAT_VALUE"] = (($arItem["PRICE"] / ($arItem["VAT_RATE"] +1)) * $arItem["VAT_RATE"]);
	$arItem["PRICE_FORMATED"] = SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);

	$arItem["WEIGHT"] = doubleval($arItem["WEIGHT"]);
	$arItem["WEIGHT_FORMATED"] = roundEx(doubleval($arItem["WEIGHT"] / $arParams["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arParams["WEIGHT_UNIT"];

	if (CSaleBasketHelper::isSetItem($arItem))
		$arSetParentWeight[$arItem["SET_PARENT_ID"]] += $arItem["WEIGHT"] * $arItem['QUANTITY'];

	if (array_key_exists($arItem["PRODUCT_ID"], $arProductData) && is_array($arProductData[$arItem["PRODUCT_ID"]]))
	{
		foreach ($arProductData[$arItem["PRODUCT_ID"]] as $key => $value)
		{
			if (strpos($key, "PROPERTY_") !== false || in_array($key, $arImgFields))
				$arItem[$key] = $value;
		}
	}

	if (array_key_exists($arItem["PRODUCT_ID"], $arSku2Parent)) // if sku element doesn't have value of some property - we'll show parent element value instead
	{
		$arFieldsToFill = array_merge($arCustomSelectFields, $arImgFields); // fields to be filled with parents' values if empty
		foreach ($arFieldsToFill as $field)
		{
			$fieldVal = (in_array($field, $arImgFields)) ? $field : $field."_VALUE";
			$parentId = $arSku2Parent[$arItem["PRODUCT_ID"]];

			if ((!isset($arItem[$fieldVal]) || (isset($arItem[$fieldVal]) && strlen($arItem[$fieldVal]) == 0))
				&& (isset($arProductData[$parentId][$fieldVal]) && !empty($arProductData[$parentId][$fieldVal]))) // can be array or string
			{
				$arItem[$fieldVal] = $arProductData[$parentId][$fieldVal];
			}
		}
	}

	foreach ($arItem as $key => $value) // format properties' values
	{
		if ((strpos($key, "PROPERTY_", 0) === 0) && (strrpos($key, "_VALUE") == strlen($key) - 6))
		{
			$code = str_replace(array("PROPERTY_", "_VALUE"), "", $key);
			$propData = $arIblockProps[$code];
			$arItem[$key] = CSaleHelper::getIblockPropInfo($value, $propData);
		}
	}

	$arItem["PREVIEW_PICTURE_SRC"] = "";
	if (isset($arItem["PREVIEW_PICTURE"]) && intval($arItem["PREVIEW_PICTURE"]) > 0)
	{
		$arImage = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
		if ($arImage)
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arImage,
				array("width" => "110", "height" =>"110"),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);

			$arItem["DETAIL_PICTURE_SRC"] = $arFileTmp["src"];
		}
	}

	$arItem["DETAIL_PICTURE_SRC"] = "";
	if (isset($arItem["DETAIL_PICTURE"]) && intval($arItem["DETAIL_PICTURE"]) > 0)
	{
		$arImage = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
		if ($arImage)
		{
			$arFileTmp = CFile::ResizeImageGet(
				$arImage,
				array("width" => "110", "height" =>"110"),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);

			$arItem["DETAIL_PICTURE_SRC"] = $arFileTmp["src"];
		}
	}

	// if (isset($arItem["PROPERTY_BRAND_VALUE"]) && intval($arItem["PROPERTY_BRAND_VALUE"]) > 0)
	// {
	// 	$arImage = CFile::GetFileArray($arItem["PROPERTY_BRAND_VALUE"]);

	// 	if ($arImage)
	// 	{
	// 		$arFileTmp = CFile::ResizeImageGet(
	// 			$arImage,
	// 			array("width" => "120", "height" =>"120"),
	// 			BX_RESIZE_IMAGE_PROPORTIONAL,
	// 			true
	// 		);

	// 		$arItem["BRAND"] = $arFileTmp["src"];
	// 	}
	// }
}
unset($arItem);

// get sku props data
if (!empty($arBasketItems) && $bUseCatalog && isset($arParams["OFFERS_PROPS"]) && !empty($arParams["OFFERS_PROPS"]))
	$arBasketItems = getSkuPropsData($arBasketItems, $arParents, $arParams["OFFERS_PROPS"]);

// count weight for set parent products
foreach ($arBasketItems as &$arItem)
{
	if (CSaleBasketHelper::isSetParent($arItem))
	{
		$arItem["WEIGHT"] = $arSetParentWeight[$arItem["ID"]] / $arItem["QUANTITY"];
		$arItem["WEIGHT_FORMATED"] = roundEx(doubleval($arItem["WEIGHT"] / $arParams["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arParams["WEIGHT_UNIT"];
	}
}
unset($arItem);

// fill item arrays for old templates
foreach ($arBasketItems as &$arItem)
{
	if (CSaleBasketHelper::isSetItem($arItem))
		continue;

	if ($arItem["CAN_BUY"] == "Y" && $arItem["DELAY"] == "N")
	{
		$allSum += ($arItem["PRICE"] * $arItem["QUANTITY"]);
		$allWeight += ($arItem["WEIGHT"] * $arItem["QUANTITY"]);
		$allVATSum += roundEx($arItem["PRICE_VAT_VALUE"] * $arItem["QUANTITY"], SALE_VALUE_PRECISION);

		$bShowReady = True;
		if(doubleval($arItem["DISCOUNT_PRICE"]) > 0)
		{
			$arItem["DISCOUNT_PRICE_PERCENT"] = $arItem["DISCOUNT_PRICE"]*100 / ($arItem["DISCOUNT_PRICE"] + $arItem["PRICE"]);
			$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"] = roundEx($arItem["DISCOUNT_PRICE_PERCENT"], SALE_VALUE_PRECISION)."%";
			$DISCOUNT_PRICE_ALL += $arItem["DISCOUNT_PRICE"] * $arItem["QUANTITY"];
		}

		$arResult["ITEMS"]["AnDelCanBuy"][] = $arItem;
	}
	elseif ($arItem["CAN_BUY"] == "Y" && $arItem["DELAY"] == "Y")
	{
		$bShowDelay = True;

		$arResult["ITEMS"]["DelDelCanBuy"][] = $arItem;
	}
	elseif ($arItem["CAN_BUY"] == "N" && $arItem["SUBSCRIBE"] == "Y")
	{
		$bShowSubscribe = True;

		$arResult["ITEMS"]["ProdSubscribe"][] = $arItem;
	}
	else
	{
		$bShowNotAvail = True;
		$arItem["NOT_AVAILABLE"] = true;

		$arResult["ITEMS"]["nAnCanBuy"][] = $arItem;
	}
}
unset($arItem);

$arResult["ShowReady"] = (($bShowReady)?"Y":"N");
$arResult["ShowDelay"] = (($bShowDelay)?"Y":"N");
$arResult["ShowNotAvail"] = (($bShowNotAvail)?"Y":"N");
$arResult["ShowSubscribe"] = (($bShowSubscribe)?"Y":"N");

$arOrder = array(
	'SITE_ID' => SITE_ID,
	'USER_ID' => $USER->GetID(),
	'ORDER_PRICE' => $allSum,
	'ORDER_WEIGHT' => $allWeight,
	'BASKET_ITEMS' => $arResult["ITEMS"]["AnDelCanBuy"]
);

$arOptions = array(
	'COUNT_DISCOUNT_4_ALL_QUANTITY' => $arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"],
);

$arErrors = array();

CSaleDiscount::DoProcessOrder($arOrder, $arOptions, $arErrors);

$allSum = 0;
$allWeight = 0;
$allVATSum = 0;

$DISCOUNT_PRICE_ALL = 0;
$priceWithoutDiscount = 0;

foreach ($arOrder["BASKET_ITEMS"] as &$arOneItem)
{
	$allWeight += ($arOneItem["WEIGHT"] * $arOneItem["QUANTITY"]);
	$allSum += ($arOneItem["PRICE"] * $arOneItem["QUANTITY"]);

	if (array_key_exists('VAT_VALUE', $arOneItem))
		$arOneItem["PRICE_VAT_VALUE"] = $arOneItem["VAT_VALUE"];
	$allVATSum += roundEx($arOneItem["PRICE_VAT_VALUE"] * $arOneItem["QUANTITY"], SALE_VALUE_PRECISION);
	$arOneItem["PRICE_FORMATED"] = SaleFormatCurrency($arOneItem["PRICE"], $arOneItem["CURRENCY"]);

	$arOneItem["FULL_PRICE"] = $arOneItem["PRICE"] + $arOneItem["DISCOUNT_PRICE"];
	$arOneItem["FULL_PRICE_FORMATED"] = SaleFormatCurrency($arOneItem["FULL_PRICE"], $arOneItem["CURRENCY"]);

	$arOneItem["DISCOUNT_PRICE_PERCENT"] = $arOneItem["DISCOUNT_PRICE"]*100 / ($arOneItem["DISCOUNT_PRICE"] + $arOneItem["PRICE"]);
	$arOneItem["DISCOUNT_PRICE_PERCENT_FORMATED"] = roundEx($arOneItem["DISCOUNT_PRICE_PERCENT"], SALE_VALUE_PRECISION)."%";
	$DISCOUNT_PRICE_ALL += $arOneItem["DISCOUNT_PRICE"] * $arOneItem["QUANTITY"];
}
unset($arOneItem);

$arResult["ITEMS"]["AnDelCanBuy"] = $arOrder["BASKET_ITEMS"];

// fill grid data (for new templates with custom columns)
foreach ($arResult["ITEMS"] as $type => $arItems)
{
	foreach ($arItems as $k => $arItem)
		$arResult["GRID"]["ROWS"][$arItem["ID"]] = $arItem;
}

$arResult["allSum"] = $allSum;
$arResult["allWeight"] = $allWeight;
$arResult["allWeight_FORMATED"] = roundEx(doubleval($allWeight/$arParams["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION)." ".$arParams["WEIGHT_UNIT"];
$arResult["allSum_FORMATED"] = SaleFormatCurrency($allSum, $allCurrency);
$arResult["DISCOUNT_PRICE_FORMATED"] = SaleFormatCurrency($arResult["DISCOUNT_PRICE"], $allCurrency);
$arResult["PRICE_WITHOUT_DISCOUNT"] = SaleFormatCurrency($allSum + $DISCOUNT_PRICE_ALL, $allCurrency);

if ($arParams["PRICE_VAT_SHOW_VALUE"] == 'Y')
{
	$arResult["allVATSum"] = $allVATSum;
	$arResult["allVATSum_FORMATED"] = SaleFormatCurrency($allVATSum, $allCurrency);
	$arResult["allSum_wVAT_FORMATED"] = SaleFormatCurrency(doubleval($arResult["allSum"]-$allVATSum), $allCurrency);
}

if ($arParams["HIDE_COUPON"] != "Y")
	$arCoupons = CCatalogDiscountCoupon::GetCoupons();

if (count($arCoupons) > 0)
	$arResult["COUPON"] = htmlspecialcharsbx($arCoupons[0]);
if(count($arBasketItems)<=0)
	$arResult["ERROR_MESSAGE"] = GetMessage("SALE_EMPTY_BASKET");

$arResult["DISCOUNT_PRICE_ALL"] = $DISCOUNT_PRICE_ALL;
$arResult["DISCOUNT_PRICE_ALL_FORMATED"] = SaleFormatCurrency($DISCOUNT_PRICE_ALL, $allCurrency);

if($arParams["USE_PREPAYMENT"] == "Y")
{
	if(doubleval($arResult["allSum"]) > 0)
	{
		$personType = array();
		$dbPersonType = CSalePersonType::GetList(array("SORT" => "ASC", "NAME" => "ASC"), array("LID" => SITE_ID, "ACTIVE" => "Y"));
		while($arPersonType = $dbPersonType->GetNext())
		{
			$personType[] = $arPersonType["ID"];
		}

		if(!empty($personType))
		{
			$dbPaySysAction = CSalePaySystemAction::GetList(
					array(),
					array(
							"PS_ACTIVE" => "Y",
							"HAVE_PREPAY" => "Y",
							"PERSON_TYPE_ID" => $personType,
						),
					false,
					false,
					array("ID", "PAY_SYSTEM_ID", "PERSON_TYPE_ID", "NAME", "ACTION_FILE", "RESULT_FILE", "NEW_WINDOW", "PARAMS", "ENCODING", "LOGOTIP")
				);
			if ($arPaySysAction = $dbPaySysAction->Fetch())
			{
				CSalePaySystemAction::InitParamarrays(false, false, $arPaySysAction["PARAMS"]);

				$pathToAction = $_SERVER["DOCUMENT_ROOT"].$arPaySysAction["ACTION_FILE"];

				$pathToAction = str_replace("\\", "/", $pathToAction);
				while (substr($pathToAction, strlen($pathToAction) - 1, 1) == "/")
					$pathToAction = substr($pathToAction, 0, strlen($pathToAction) - 1);

				if (file_exists($pathToAction))
				{
					if (is_dir($pathToAction) && file_exists($pathToAction."/pre_payment.php"))
						$pathToAction .= "/pre_payment.php";

					include_once($pathToAction);
					$psPreAction = new CSalePaySystemPrePayment;

					if($psPreAction->init())
					{
						$orderData = array(
								"PATH_TO_ORDER" => $arParams["PATH_TO_ORDER"],
								"AMOUNT" => $arResult["allSum"],
								"BASKET_ITEMS" => $arResult["ITEMS"]["AnDelCanBuy"],
							);
						if(!$psPreAction->BasketButtonAction($orderData))
						{
							if($e = $APPLICATION->GetException())
								$arResult["WARNING_MESSAGE"][] = $e->GetString();
						}

						$arResult["PREPAY_BUTTON"] = $psPreAction->BasketButtonShow();
					}
				}
			}
		}
	}
}

if (is_array($_SESSION["SALE_BASKET_MESSAGE"]))
{
	foreach ($_SESSION["SALE_BASKET_MESSAGE"] as $message)
		$arResult["WARNING_MESSAGE"][] = $message;

	unset($_SESSION["SALE_BASKET_MESSAGE"]);
}

$this->IncludeComponentTemplate();
?>