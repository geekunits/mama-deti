<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["DEPTH_LEVEL"] = intval($arParams["DEPTH_LEVEL"]);
if($arParams["DEPTH_LEVEL"]<=0)
    $arParams["DEPTH_LEVEL"]=1;

if (strlen($arParams["SECTION_FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["SECTION_FILTER_NAME"])) {
    $arrSectionFilter = array();
} else {
    global ${$arParams["SECTION_FILTER_NAME"]};
    $arrSectionFilter = ${$arParams["SECTION_FILTER_NAME"]};
    if(!is_array($arrSectionFilter))
        $arrSectionFilter = array();
}

if (strlen($arParams["ELEMENT_FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ELEMENT_FILTER_NAME"])) {
    $arrElementFilter = array();
} else {
    global ${$arParams["ELEMENT_FILTER_NAME"]};
    $arrElementFilter = ${$arParams["ELEMENT_FILTER_NAME"]};
    if(!is_array($arrElementFilter))
        $arrElementFilter = array();
}

$arResult["SECTIONS"] = array();
$arResult["ELEMENT_LINKS"] = array();

if ($this->StartResultCache($arParams["CACHE_TIME"],serialize(array($arrSectionFilter, $arrElementFilter)))) {
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
    } else {
        $arFilter = array(
            "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
            "GLOBAL_ACTIVE"=>"Y",
            "IBLOCK_ACTIVE"=>"Y",
            "<="."DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
        );

        $arFilter = array_merge($arFilter, $arrSectionFilter);

        $arOrder = array(
            "UF_MENU_HIGHLIGHT" => 'DESC',
            "left_margin"=>"asc",
        );

        $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, array(
            "ID",
            "DEPTH_LEVEL",
            "NAME",
            "SECTION_PAGE_URL",
            "UF_MENU_HIGHLIGHT",
            "UF_MENU_DESCRIPTION",
        ));
        if($arParams["IS_SEF"] !== "Y")
            $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
        else
            $rsSections->SetUrlTemplates("", $arParams["SEF_BASE_URL"].$arParams["SECTION_PAGE_URL"]);
        while ($arSection = $rsSections->GetNext()) {
            $arResult["SECTIONS"][] = $arSection;

        }
        $this->EndResultCache();
    }
}

$aMenuLinksNew = array();
$menuIndex = 0;
$previousDepthLevel = 1;
foreach ($arResult["SECTIONS"] as $arSection) {
    if ($menuIndex > 0)
        $aMenuLinksNew[$menuIndex - 1][3]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
    $previousDepthLevel = $arSection["DEPTH_LEVEL"];

    $aMenuLinksNew[$menuIndex++] = array(
        htmlspecialcharsbx($arSection["~NAME"]),
        $arSection["~SECTION_PAGE_URL"],
        array(
        ),
        array(
            "FROM_IBLOCK" => true,
            "IS_PARENT" => false,
            "DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
            "HIGHLIGHT" => (bool) $arSection['UF_MENU_HIGHLIGHT'],
            "DESCRIPTION" => $arSection['UF_MENU_DESCRIPTION'],
        ),
    );
}

return $aMenuLinksNew;
