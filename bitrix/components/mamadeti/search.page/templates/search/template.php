<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search-page">
	<form action="" method="get" id="search_form_md" class="search-page__form">

		<div class="search-block">
			<input class="button04" type="submit" value="">
			<div class="text02">
				<input name="q" type="text" value="<?=$arResult["REQUEST"]["~QUERY"]?>">
			</div>
		</div>
		<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />

		<?if($arParams["SHOW_WHERE"]):?>
			<br>
			<h2>Область поиска</h2>
			<ul class="search-page__tabs">
				<li class="search-page__tabs-item<?php if (!$arResult["REQUEST"]["WHERE"]) {echo ' search-page__tabs-item--active js-search-tab-active';} ?> js-search-tab" data-id=""><?=GetMessage("SEARCH_ALL")?></li>
				<?foreach($arResult["DROPDOWN"] as $key=>$value):?>
				<li class="search-page__tabs-item<?php if ($arResult["REQUEST"]["WHERE"]==$key) {echo ' search-page__tabs-item--active';} ?> js-search-tab" data-id="<?=$key?>"><?=$value?></li>
				<?endforeach?>
			</ul>
			<input id="where_search_field" type="hidden" name="where" value="<?php echo $arResult["REQUEST"]["WHERE"] ? : ''; ?>" />
		<?endif;?>
	</form>
	<?if(count($arResult["SEARCH"])>0):?>
		<h2>Сортировка результатов</h2>
		<p class="search-page__sort">
			<?if($arResult["REQUEST"]["HOW"]=="d"):?>
				<a href="<?=$arResult["URL"]?>&amp;how=r<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>" class="search-page__sort-item"><?=GetMessage("SEARCH_SORT_BY_RANK")?></a><span class="search-page__sort-item"><?=GetMessage("SEARCH_SORT_BY_DATE")?></span>
			<?else:?>
				<span class="search-page__sort-item"><?=GetMessage("SEARCH_SORT_BY_RANK")?></span><a href="<?=$arResult["URL"]?>&amp;how=d<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>" class="search-page__sort-item"><?=GetMessage("SEARCH_SORT_BY_DATE")?></a>
			<?endif;?>
		</p>
	<?endif;?>

	<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
		?>
		<div class="search-language-guess">
			<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
		</div><br /><?
	endif;?>

	<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
	<?elseif($arResult["ERROR_CODE"]!=0):?>
		<p><?=GetMessage("SEARCH_ERROR")?></p>
		<?ShowError($arResult["ERROR_TEXT"]);?>
		<p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
		<br /><br />
		<p><?=GetMessage("SEARCH_SINTAX")?><br /><b><?=GetMessage("SEARCH_LOGIC")?></b></p>
		<table border="0" cellpadding="5">
			<tr>
				<td align="center" valign="top"><?=GetMessage("SEARCH_OPERATOR")?></td><td valign="top"><?=GetMessage("SEARCH_SYNONIM")?></td>
				<td><?=GetMessage("SEARCH_DESCRIPTION")?></td>
			</tr>
			<tr>
				<td align="center" valign="top"><?=GetMessage("SEARCH_AND")?></td><td valign="top">and, &amp;, +</td>
				<td><?=GetMessage("SEARCH_AND_ALT")?></td>
			</tr>
			<tr>
				<td align="center" valign="top"><?=GetMessage("SEARCH_OR")?></td><td valign="top">or, |</td>
				<td><?=GetMessage("SEARCH_OR_ALT")?></td>
			</tr>
			<tr>
				<td align="center" valign="top"><?=GetMessage("SEARCH_NOT")?></td><td valign="top">not, ~</td>
				<td><?=GetMessage("SEARCH_NOT_ALT")?></td>
			</tr>
			<tr>
				<td align="center" valign="top">( )</td>
				<td valign="top">&nbsp;</td>
				<td><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
			</tr>
		</table>
	<?elseif(count($arResult["SEARCH"])>0):?>
		<?foreach($arResult["SEARCH"] as $arItem):?>
		<div class="search-page__item">
			<a href="<?echo $arItem["URL"]?>" class="search-page__title"><?echo $arItem["TITLE_FORMATED"]?></a>
			<?echo $arItem["BODY_FORMATED"]?>
		</div>
		<?endforeach;?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
	<?else:?>
		<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
	<?endif;?>
</div>
