<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CUtil::InitJSCore(array('popup'));

if ($arParams["USE_FILTER"]=="Y") {
    if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
        $arParams["FILTER_NAME"] = "arrFilter";
} else
    $arParams["FILTER_NAME"] = "";

$arDefaultUrlTemplates404 = array(
    "sections" => "",
    "section" => "#SECTION_ID#/",
    "element" => "#SECTION_ID#/#ELEMENT_ID#/",
    "compare" => "compare.php?action=COMPARE",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
    "SECTION_ID",
    "SECTION_CODE",
    "ELEMENT_ID",
    "ELEMENT_CODE",
    "action",
);

if ($arParams["SEF_MODE"] == "Y") {
    $arVariables = array();

    $engine = new CComponentEngine($this);
    if (CModule::IncludeModule('iblock')) {
        $engine->addGreedyPart("#SECTION_CODE_PATH#");
        $engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
    }
    $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
    $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

    $componentPage = $engine->guessComponentPath(
        $arParams["SEF_FOLDER"],
        $arUrlTemplates,
        $arVariables
    );

    if(!$componentPage && isset($_REQUEST["q"]))
        $componentPage = "search";

    $b404 = false;
    if (!$componentPage) {
        $componentPage = "sections";
        $b404 = true;
    }

    if ($componentPage == "section") {
        if (isset($arVariables["SECTION_ID"]))
            $b404 |= (intval($arVariables["SECTION_ID"])."" !== $arVariables["SECTION_ID"]);
        else
            $b404 |= !isset($arVariables["SECTION_CODE"]);
    }

    if ($b404 && $arParams["SET_STATUS_404"]==="Y") {
        $folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
        if ($folder404 != "/")
            $folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
        if (substr($folder404, -1) == "/")
            $folder404 .= "index.php";

            if($folder404 != $APPLICATION->GetCurPage(true))
            CHTTP::SetStatus("404 Not Found");
    }

    CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
    $arResult = array(
        "FOLDER" => $arParams["SEF_FOLDER"],
        "URL_TEMPLATES" => $arUrlTemplates,
        "VARIABLES" => $arVariables,
        "ALIASES" => $arVariableAliases
    );
} else {
    $arVariables = array();

    $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
    CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

    $componentPage = "";

    $arCompareCommands = array(
        "COMPARE",
        "DELETE_FEATURE",
        "ADD_FEATURE",
        "DELETE_FROM_COMPARE_RESULT",
        "ADD_TO_COMPARE_RESULT",
        "COMPARE_BUY",
        "COMPARE_ADD2BASKET",
    );

    if(isset($arVariables["action"]) && in_array($arVariables["action"], $arCompareCommands)){
        $componentPage = "compare";
    }elseif(isset($arVariables["ELEMENT_ID"]) && intval($arVariables["ELEMENT_ID"]) > 0){
        $componentPage = "element";
    }elseif(isset($arVariables["ELEMENT_CODE"]) && strlen($arVariables["ELEMENT_CODE"]) > 0){
        $componentPage = "element";
    }elseif(isset($arVariables["SECTION_ID"]) && intval($arVariables["SECTION_ID"]) > 0){
        $componentPage = "section";
    }elseif(isset($arVariables["SECTION_CODE"]) && strlen($arVariables["SECTION_CODE"]) > 0){
        $componentPage = "section";
    }elseif(isset($_REQUEST["q"])){
        $componentPage = "search";
    }else{
        $componentPage = "sections";
    }

    $b404 = false;

    if($arVariables["ELEMENT_ID"] || $arVariables["ELEMENT_CODE"]){
        $resElement = CIBlockElement::GetList(array(), array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ACTIVE"    => "Y",
            "ID"        => $arVariables["ELEMENT_ID"],
            "CODE"      => $arVariables["ELEMENT_CODE"]
        ), false, array("ID"), array("nTopCount" => 1));

        if(!$resElement->Fetch()){
            $b404 = true;
        }
    }

    if($arVariables["SECTION_ID"] || $arVariables["SECTION_CODE"]){
        $resSection = CIBlockSection::GetList(array(), array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ACTIVE"    => "Y",
            "ID"        => $arVariables["SECTION_ID"],
            "CODE"      => $arVariables["SECTION_CODE"]
        ), false, array("ID"), array("nTopCount" => 1));

        if(!$resSection->Fetch()){
            $b404 = true;
        }
    }

    if($b404){
        error404();
    }

    $urlTemplates = $arParams['SEF_URL_TEMPLATES'];
    $arResult = array(
        "FOLDER" => $arParams['SEF_FOLDER'],
        "URL_TEMPLATES" => $arParams['SEF_URL_TEMPLATES'],
        "VARIABLES" => $arVariables,
        "ALIASES" => $arVariableAliases
    );
}

$this->IncludeComponentTemplate($componentPage);
?>
