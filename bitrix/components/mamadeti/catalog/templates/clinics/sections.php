<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
$bSearchPage = false;
$ajaxCatalog = isset($_REQUEST["ajax_catalog"]) && $_REQUEST["ajax_catalog"] == 'y';
if ($ajaxCatalog)
    $APPLICATION->RestartBuffer();
?>
<?if (!$ajaxCatalog):?>
<?php
$cm = CityManager::getInstance();
$city = $cm->getCurrentCity();

CModule::IncludeModule("iblock");

$tags_get = array();
$service_get = array();
if (!empty($_REQUEST["tags"])) $tags_get = explode(",", $_REQUEST["tags"]);
if (!empty($_REQUEST["service"])) $service_get = explode(",", $_REQUEST["service"]);

$CITY_ID = GetCurrentCity();
if (is_array($tags_get) && !empty($tags_get)) {
    $rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ACTIVE"=>"Y","NAME"=>$tags_get),false,false,array("IBLOCK_ID","ID","NAME","PROPERTY_CLINIC"));
    while ($arElement = $rsElement->Fetch()) {
        if (is_array($arElement["PROPERTY_CLINIC_VALUE"]))
            $GLOBALS["arFilterClinic"]["ID"] = array_merge((array) $GLOBALS["arFilterClinic"]["ID"],(array) $arElement["PROPERTY_CLINIC_VALUE"]); else
            $GLOBALS["arFilterClinic"]["ID"][] = $arElement["PROPERTY_CLINIC_VALUE"];
        $bSearchPage = true;
    }
}

//$GLOBALS["arFilterClinic"] = array("PROPERTY_CITY"=>$CITY_ID);

foreach ($service_get as $service) {
    if (strncmp('E',$service,1) == 0) {
        $GLOBALS["arFilterClinic"]["PROPERTY_SERVICES"][] = substr($service,1);
        $bSearchPage = true;
    }
}

$GLOBALS["arFilterClinic"]["IBLOCK_ID"] = 2;
$GLOBALS["arFilterClinic"]["ACTIVE"] = "Y";

global $arFilterClinic;
$bShowAll = (isset($_REQUEST["show_all"]) && $_REQUEST["show_all"] == 'y') ? 'Y' : 'N';
if ($bShowAll == 'Y') {
    $arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_CITY.NAME";
    $arParams["ELEMENT_SORT_ORDER"] = "ASC";
    $arFilterClinic["!PROPERTY_CITY"] = $GLOBALS["CURRENT_CITY"]["ID"];
} else {
    $arFilterClinic["PROPERTY_CITY"] = $GLOBALS["CURRENT_CITY"]["ID"];
}

$countClinic = CIBlockElement::GetList(array(),$GLOBALS[$arParams['FILTER_NAME']])->SelectedRowsCount();
?>
<div class="sector-holder" id="search-clinics">
    <h1>Клиники &laquo;Мать и дитя&raquo; в <?=$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE']?></h1>
    <?if($countClinic > 1):?>
    <div class="sector-block">
        <div class="topic-panel">
            <div class="align-left">
                <span class="name-title">Выберите клинику</span>
            </div>
            <div class="block-holder align-right">
                <ul class="sort-list">
                    <li<?if (($_REQUEST["sort"]=="list")) {?> class="active"<?}?>>
                        <a href="<?=$APPLICATION->GetCurPageParam("sort=list", array("sort"));?>" class="button07"><span class="icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon21.png" width="13" height="10" alt=""></span><span class="title">списком</span></a>
                    </li>
                    <li<?if ((empty($_REQUEST["sort"])) || $_REQUEST["sort"]=="dist") {?> class="active"<?}?>>
                        <a href="<?=$APPLICATION->GetCurPageParam("sort=dist", array("sort"));?>" class="button07"><span class="icon"><img src="<?=SITE_TEMPLATE_PATH?>/images/icon22.png" width="10" height="16" alt=""></span><span class="title">по расстоянию</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!--<span class="content-title">Введите ФИО врача</span>
        <div class="search-panel">
            <form action="#">
                <fieldset>
                    <div class="search-field02">
                        <div class="search-sector active">
                            <div class="search-input">
                                <div class="align-left">
                                    <ul class="search-list">
                                    <?foreach ($tags_get as $t) {
                                        if (!empty($t)) {
                                        ?>
                                            <li><span class="title"><?=$t?></span><a class="remove-button" href="#">remove</a></li>
                                        <?}
                                    }
                                    ?>
                                    </ul>
                                </div>
                                <input type="button" class="button06 bt_search_clinic" value="">
                                <div class="text03">
                                    <input type="text" class="input_search_clinic" placeholder="Введите ФИО врача" data-city="<?=$CITY_ID?>">
                                </div>
                            </div>
                            <div id="search_result" class="search-drop" style="display:none;">
                                <div class="drop-frame">
                                    <div class="scrollpane">
                                        <ul class="search-listing"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-holder">
                        <span class="button02 block-button"><input type="submit" value="">найти</span>
                    </div>
                </fieldset>
            </form>
        </div>-->
        <?php $arServiceID = CMamaDetiAPI::getClinicsServicesID(array("PROPERTY_CITY"=>$CITY_ID)); ?>
        <?php if ($arServiceID): ?>
            <span class="content-title">по услугам</span>
            <form action="#">
                <fieldset>
                    <?php
                    /*$arItems = array();
                    $rsElement = CIBlockElement::GetList(array("SORT"=>"ASC","NAME"=>"ASC"),array("IBLOCK_ID"=>1,"ACTIVE"=>"Y"),false,false,array("ID","NAME","IBLOCK_SECTION_ID"));
                    while ($arElement = $rsElement->GetNext()) {
                        $arItems[$arElement["IBLOCK_SECTION_ID"]][] = array("VALUE"=>"E".$arElement["ID"],"NAME"=>$arElement["NAME"],"SELECTED"=>in_array("E".$arElement["ID"],$service_get));
                    }

                    $arTree = array();
                    $rsSection = CIBlockSection::GetList(Array("left_margin"=>"asc"), array("IBLOCK_ID"=>1,"ACTIVE"=>"Y"),false,array("ID","NAME","DEPTH_LEVEL"));
                    while ($arSection = $rsSection->GetNext()) {
                        $arTree[] = array("VALUE"=>"S".$arSection["ID"],"NAME"=>$arSection["NAME"],"DEPTH_LEVEL"=>$arSection["DEPTH_LEVEL"],"SELECTED"=>in_array("S".$arSection["ID"],$service_get));
                        if (array_key_exists($arSection["ID"],$arItems)) {
                            $DEPTH_LEVEL = $arSection["DEPTH_LEVEL"];

                            foreach ($arItems[$arSection["ID"]] as $arItem) {
                                $arItem["DEPTH_LEVEL"] = $DEPTH_LEVEL+1;
                                $arTree[] = $arItem;
                            }
                        }
                    }
                    unset($arItems);

                    $arPrevKey = array();

                    foreach ($arTree as $key=>$arItem) {
                        if ($key>0)
                            $arTree[$key - 1]["IS_PARENT"] = $arItem["DEPTH_LEVEL"] > $previousDepthLevel;
                        $previousDepthLevel = $arItem["DEPTH_LEVEL"];
                        if ($arItem["DEPTH_LEVEL"] == 1)
                            $arPrevKey = array();

                        if ($arItem["SELECTED"]) {
                            foreach ($arPrevKey as $prevKey)
                                $arTree[$prevKey]["SELECTED"] = true;
                            $arPrevKey = array();
                        } else
                            $arPrevKey[] = $key;
                    }*/

                    //echo '<pre>'; print_r($arItems); echo '</pre>';
                    //echo '<pre>'; print_r($arTree); echo '</pre>';

                    $arServiceID = CMamaDetiAPI::getClinicsServicesID(array("PROPERTY_CITY"=>$CITY_ID));
                    $arTree = CMamaDetiAPI::GetSelectedServiceTree($arServiceID);

                    $arPrevKey = array();
                    foreach ($arTree as $key=>&$arItem) {
                        $arItem["SELECTED"] = in_array($arItem["ID"], $service_get);
                        if ($arItem["DEPTH_LEVEL"] == 1)
                            $arPrevKey = array();

                        if ($arItem["SELECTED"]) {
                            foreach ($arPrevKey as $prevKey)
                                $arTree[$prevKey]["SELECTED"] = true;
                            $arPrevKey = array();
                        } else
                            $arPrevKey[] = $key;
                    }
                    if (isset($arItem))
                        unset($arItem);

                    ?>
                                    <ul class="service-tree-menu service-tree-menu-search">
                    <?php
                    $previousLevel = 0;
                    foreach($arTree as $arItem):
                    ?>

                        <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                            <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
                        <?endif?>

                        <?if ($arItem["IS_PARENT"]):?>
                            <li class="tree expand<?if ($arItem["SELECTED"]):?> collapse<?endif?>"><i></i>
                                <div class="holder">
                                    <?=$arItem["TEXT"]?>
                                </div>
                                <ul<?if ($arItem["SELECTED"]):?> style="display:block;"<?endif?>>
                        <?else:?>
                            <li>
                                <div class="holder">
                                    <input type="checkbox"<?if ($arItem["SELECTED"]):?> checked<?endif?> class="checkbox01" id="id<?=$arItem["ID"]?>" value="<?=$arItem["ID"]?>">
                                    <label for="id<?=$arItem["ID"]?>"><?=$arItem["TEXT"]?></label>
                                </div>
                            </li>
                        <?endif?>

                        <?$previousLevel = $arItem["DEPTH_LEVEL"];?>

                    <?endforeach?>

                    <?if ($previousLevel > 1)://close last item tags?>
                        <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
                    <?endif?>
                                    </ul>

                    <?/*<script>
                    $(function () {
                        $('.check-list input').change(function (e) {
                            var li = $(this).closest('.holder').parent();
                            if ($(this).prop("checked"))
                                li.find('ul:first').slideDown(); else li.find('ul:first').slideUp();
                        });
                    });
                    </script>*/?>

                </fieldset>
            </form>
        <?php endif; ?>
        <div class="button-holder" style="float:none;">
            <span class="button02 block-button"><input type="submit" value="">найти</span>
        </div>
    </div>
    <?endif;?>
    <div class="sector-frame sector-frame-zindex">
        <div class="topic-panel">
            <div class="align-right">
                <div class="place-block"<?php if($countClinic == 1): ?> style="margin-top:-60px;"<?endif;?>>
                    <div class="point-block">
                        <div class="align-left">
                            <span class="point-title">Ваше местоположение: </span>
                        </div>
                        <div class="align-left">
                            <div class="point-select">
                                <a href="#" class="point-link red-color change_city"><?=GetNameCity($CITY_ID)?></a>
                                <div class="point-drop">
                                    <ul class="point-list">
                                        <?foreach ($GLOBALS["CITY_LIST"] as $id=>$arCity) {?>
                                            <li>
                                                <a href="#" data-link="<?php echo CityManager::getInstance()->getUrlByCityId($id); ?>" data-id="<?=$id?>" class="js-city-selector-link">
                                                    <?=$arCity["NAME"]?>
                                                </a>
                                            </li>
                                        <?}?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            if($countClinic > 1):
        ?>
            <div class="map-block">
                <div id="map" style="width: 590px; height: 360px"></div>
            </div>
        <?endif;?>
    </div>
    <?php
        if($countClinic == 1):
    ?>
        <div class="map-block">
            <div id="map" style="height: 360px"></div>
        </div>
    <?endif;?>
</div>

<?endif//if (!$ajaxCatalog)?>
<?php
global $arFilterClinic;
$bShowAll = (isset($_REQUEST["show_all"]) && $_REQUEST["show_all"] == 'y') ? 'Y' : 'N';
if ($bShowAll == 'Y') {
    $arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_CITY.NAME";
    $arParams["ELEMENT_SORT_ORDER"] = "ASC";
    $arFilterClinic["!PROPERTY_CITY"] = $GLOBALS["CURRENT_CITY"]["ID"];
} else {
    $arFilterClinic["PROPERTY_CITY"] = $GLOBALS["CURRENT_CITY"]["ID"];
}
?>

<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
        "TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"]
    ),
    $component
);
?>
<?if ($arParams["USE_COMPARE"]=="Y") {?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.compare.list",
    "",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "NAME" => $arParams["COMPARE_NAME"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        "COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
    ),
    $component
);?>
<?}
if ($arParams["SHOW_TOP_ELEMENTS"]!="N") {?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.top",
    "slider",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_SORT_FIELD" => $arParams["TOP_ELEMENT_SORT_FIELD"],
        "ELEMENT_SORT_ORDER" => $arParams["TOP_ELEMENT_SORT_ORDER"],
        "ELEMENT_SORT_FIELD2" => $arParams["TOP_ELEMENT_SORT_FIELD2"],
        "ELEMENT_SORT_ORDER2" => $arParams["TOP_ELEMENT_SORT_ORDER2"],
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        "BASKET_URL" => $arParams["BASKET_URL"],
        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
        "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
        "ELEMENT_COUNT" => $arParams["TOP_ELEMENT_COUNT"],
        "LINE_ELEMENT_COUNT" => $arParams["TOP_LINE_ELEMENT_COUNT"],
        "PROPERTY_CODE" => $arParams["TOP_PROPERTY_CODE"],
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
        "PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
        "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        "OFFERS_FIELD_CODE" => $arParams["TOP_OFFERS_FIELD_CODE"],
        "OFFERS_PROPERTY_CODE" => $arParams["TOP_OFFERS_PROPERTY_CODE"],
        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
        "OFFERS_LIMIT" => $arParams["TOP_OFFERS_LIMIT"],
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
    ),
$component
);?>
<?}?>
<?php
//echo '<pre>'; print_r($arParams); echo '</pre>';
?>
<?$intSectionID = $APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "",
    array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
        "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
        "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
        "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
        "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
        "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
        "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
        "BASKET_URL" => $arParams["BASKET_URL"],
        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
        "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
        "FILTER_NAME" => $arParams["FILTER_NAME"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
        "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
        "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
        "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

        "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
        "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
        "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
        "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
        "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
        'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

        'LABEL_PROP' => $arParams['LABEL_PROP'],
        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
        'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
        'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
        'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
        'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
        'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
        'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
        'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
        'SHOW_ALL_WO_SECTION' => 'Y',
        'SHOW_ALL_BY_GROUP' => $bShowAll
    ),
    $component
);
?>
<?php
if ($ajaxCatalog)
    die();
?>

<div class="ajax-show-all-clinics">
</div>

<?if ($bSearchPage):?>
<span class="name-title red-color align-center"><a href="<?=$arParams["SEF_FOLDER"]?>">Вернуться к списку клиник</a></span>
<?else:?>
<span class="name-title red-color align-center"><a href="#" class="show-all-clinics">Все клиники</a></span>
<?endif?>

<?/*
<?if ($arParams["SHOW_ALL_BY_GROUP"] == "Y"):?>
    <span class="name-title red-color align-center"><a href="<?=$APPLICATION->GetCurPageParam("",array("show_all"))?>">Клиники в моем городе</a></span>
<?else:?>
    <span class="name-title red-color align-center"><a href="<?=$APPLICATION->GetCurPageParam("show_all=y",array("show_all"))?>">Все клиники</a></span>
<?endif?>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<?else:?>
    <span class="name-title red-color">Для вашего города клиник не найдено</span>
<?endif;?>
