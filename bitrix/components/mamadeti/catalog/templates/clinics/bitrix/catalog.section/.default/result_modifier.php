<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
CModule::IncludeModule("iblock");

$coord_current = $GLOBALS["CURRENT_CITY"]["COORD"];

$arResult["ICONS"] = array();

$rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => 20), false, false, array("ID", "NAME", "PREVIEW_PICTURE"));
while ($arElement = $rsElement->Fetch()) {
    $arElement["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
    $arResult["ICONS"][$arElement["ID"]] = $arElement;
}

$arResult["ITEMS_NEW"] = array();
foreach ($arResult["ITEMS"] as $key => $arItem) {
    /*	$service_arr = array();
    if (is_array($arItem["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SERVICES"]["VALUE"])) {
        $arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y", "ID"=>$arItem["PROPERTIES"]["SERVICES"]["VALUE"]);
        $res = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
        while ($arres = $res->GetNext()) {
            $service_arr[] = array("URL"=>$arres["DETAIL_PAGE_URL"],"NAME"=>$arres["NAME"]);
        }
    }
    $arResult["ITEMS"][$key]["SERVICES"] = $service_arr;*/

    /*if (is_array($arItem["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SERVICES"]["VALUE"])) {
        $arResult["ITEMS"][$key]["SERVICES"] = CMamaDetiAPI::GetSelectedServiceTreeMenu($arItem["PROPERTIES"]["SERVICES"]["VALUE"],true,2);

        $arTable = array(0,0,0);
        $row = 0;

        foreach ($arResult["ITEMS"][$key]["SERVICES"] as $arService)
            if ($arService["DEPTH_LEVEL"] == 1) {
                $arTable[$row++]++;
                if ($row == 3) $row = 0;
            }

        $arResult["ITEMS"][$key]["SERVICES_TABLE"] = $arTable;

    }*/

    $arResult["ITEMS"][$key]["SERVICES_MENU"] = CMamaDetiAPI::GetSelectedServiceTreeMenu($arItem["PROPERTIES"]["SERVICES"]["VALUE"], true, 2);

    //echo '<pre>'; print_r($arResult["ITEMS"][$key]["SERVICES"]); echo '</pre>';

    /*$service_arr = array();
    if (is_array($arItem["PROPERTIES"]["SERVICE"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SERVICE"]["VALUE"])) {
        $arFilter = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y", "ID"=>$arItem["PROPERTIES"]["SERVICE"]["VALUE"], "DEPTH_LEVEL" => 1);
        $rsSection = CIBlockSection::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, array("NAME", "SECTION_PAGE_URL","UF_ICON"));
        while ($arSection = $rsSection->GetNext()) {
            $arFile = CFile::GetFileArray($arSection["UF_ICON"]);
            $str = '<a class="red-color" href="'.$arSection["SECTION_PAGE_URL"].'">';
            if (is_array($arFile))
                $str.='<img src="'.$arFile["SRC"].'" alt="'.$arSection["NAME"].'" title="'.$arSection["NAME"].'">'; else
                $str.=$arSection["NAME"];
            $str.='</a>';
            $service_arr[] = $str;
        }
    }
    $arResult["ITEMS"][$key]["SERVICE"] = implode(" ", $service_arr);*/

    $dist = "";
    if (!empty($arItem["PROPERTIES"]["COORD"]["VALUE"])) {
        $dist = GetDistanceForCoord($coord_current, $arItem["PROPERTIES"]["COORD"]["VALUE"]);
    }
    $arResult["ITEMS"][$key]["DISTANCE"] = $dist;
}

if ((empty($_REQUEST["sort"]) || $_REQUEST["sort"] == "dist") && $arParams["SHOW_ALL_BY_GROUP"] != 'Y') {
    usort($arResult["ITEMS"], function ($a, $b) {
        if ($a["SORT"] != $b["SORT"]) {
            return ($a["SORT"] > $b["SORT"]) ? 1 : -1;
        }
        if ($a["DISTANCE"] == $b["DISTANCE"]) {
            return 0;
        }

        return ($a["DISTANCE"] > $b["DISTANCE"]) ? 1 : -1;
    });
    //sort($arResult["ITEMS"]);
}

$arResult["FIRST_CLINIC_COORD"] = false;

foreach ($arResult["ITEMS"] as $arItem) {
    if (!empty($arItem["PROPERTIES"]["COORD"]["VALUE"])) {
        $arResult["FIRST_CLINIC_COORD"] = $arItem["PROPERTIES"]["COORD"]["VALUE"];
        break;
    }
}

$this->__component->SetResultCacheKeys(array("FIRST_CLINIC_COORD"));

if ($arResult["ITEMS"]):
    if (count($arResult["ITEMS"]) == 1) {
        $maps_center = $arResult["ITEMS"][0]["COORD"];
        $maps_zoom = 14;
    } else {
        $x = 0;
        $y = 0;
        foreach ($arResult["ITEMS"] as $m) {
            $xy = explode(",", $m["PROPERTIES"]["COORD"]['VALUE']);
            $x += trim($xy[0]);
            $y += trim($xy[1]);
        }
        $x = $x/count($arResult["ITEMS"]);
        $y = $y/count($arResult["ITEMS"]);
        $maps_center = $x.",".$y;
        $maps_zoom = 9;
    }
?>
    <script type="text/javascript">
        ymaps.ready(init);

        function init()
        {
            var myPlacemark;

            if (typeof window.myMap === 'undefined') {
                window.myMap = new ymaps.Map ("map", {
                    center: [<?=$arResult["FIRST_CLINIC_COORD"]?>],
                    zoom: <?=$maps_zoom?>,
                    controls: ["smallMapDefaultSet"]
                });
            }
            <?foreach ($arResult["ITEMS"] as $m) {?>
                <?php
                    $phoneValues = '';
                    foreach ($m["PROPERTIES"]['PHONE']['VALUE'] as $phoneIndex => $phoneValue) {
                        if(PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phoneValue)){
                            $phoneValue = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                        }
                                                    
                        $phoneDescription = empty($m["PROPERTIES"]['PHONE']['DESCRIPTION'][$phoneIndex]) ? '' : ' (' . $m["PROPERTIES"]['PHONE']['DESCRIPTION'][$phoneIndex] . ')';
                        $phoneValues .= '<span>' .$phoneValue.$phoneDescription. '</span><br>';
                    }
                ?>
                myPlacemark = new ymaps.Placemark([<?=$m["PROPERTIES"]["COORD"]['VALUE']?>], {
                    content: <?=CUtil::PhpToJSObject($m["NAME"])?>,
                    balloonContent: <?=CUtil::PhpToJSObject('<a href="'.$m['DETAIL_PAGE_URL'].'">'.$m['NAME'].'</a><br/>'. $phoneValues . reset($m["PROPERTIES"]["ADDRESS"]['VALUE']))?>
                });
                myPlacemark.events.add('mouseenter', function (e) {
                    if (!myMap.balloon.isOpen()) {
                        e.get('target').balloon.open();
                    }
                });
                myMap.geoObjects.add(myPlacemark);
            <?}?>

            <?php if (count($arResult["ITEMS"]) > 1): ?>
            myMap.setBounds(myMap.geoObjects.getBounds());
            myMap.setZoom(myMap.getZoom() - 1);
            <?php endif; ?>
        }
    </script>
<?php
endif;
?>
