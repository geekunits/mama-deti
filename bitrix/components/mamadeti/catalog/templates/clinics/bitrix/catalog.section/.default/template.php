<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php if (!empty($arResult["ITEMS"])): ?>
    <?php if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <?php endif; ?>

    <ul class="workers-list">
        <?php
        $prevTitle = "";
        $cm = CityManager::getInstance();
        ?>
        <?php foreach ($arResult["ITEMS"] as $arItem): ?>
            <?php
                $phones = is_array($arItem["PROPERTIES"]["PHONE"]["VALUE"]) ?
                    $arItem["PROPERTIES"]["PHONE"]["VALUE"] : array($arItem["PROPERTIES"]["PHONE"]["VALUE"]);
                $phoneDescriptions = is_array($arItem["PROPERTIES"]["PHONE"]["DESCRIPTION"]) ?
                    $arItem["PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($arItem["PROPERTIES"]["PHONE"]["DESCRIPTION"]);

                $isCurrentCity = $cm->getCurrentCityId() == $arItem['PROPERTIES']['CITY']['VALUE'];
                $serviceUrlPrefix = $isCurrentCity ? '' : 'http://'.$cm->getDomainByCityId($arItem['PROPERTIES']['CITY']['VALUE']);
            ?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?php if ($arParams["SHOW_ALL_BY_GROUP"] == "Y" && $prevTitle != $arItem["PROPERTY_CITY_NAME"]): ?>
                <?php $prevTitle = $arItem["PROPERTY_CITY_NAME"] ?>
                <li>
                    <span class="name-title red-color"><?= $prevTitle ?></span>
                </li>
            <?php endif ?>

            <li id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="block-holder">
                    <div class="frame-holder clin-list-holder">
                        <div class="frame-image">
                            <?php if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) { ?>
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                    <?php
                                        $arImgClinic = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width' => 190, 'height' => 190), BX_RESIZE_IMAGE_EXACT)
                                    ?>
                                    <img src="<?php echo $arImgClinic["src"]; ?>" alt="<?php echo $arItem["NAME"]; ?>">
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="block-holder">
                        <div class="topic-panel clin-list-panel">
                            <div class="align-left">
                                <span class="name-title clin-list-width red-color"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></span>
                            </div>
                            <div class="align-right">
                                <?php if(!$arItem['PROPERTIES']['HIDE_BOOK_BUTTON']['VALUE']): ?>
                                    <a href="#" data-name="<?= $arItem["NAME"] ?>" data-clinic="<?= $arItem["ID"] ?>"
                                       class="button03 button08 new-make-btn link_reception_clinic js-goal" data-target="Appointment_<?php echo $arItem['PROPERTIES']['METRIC_ID']['VALUE']; ?>">Записаться<br/> на прием</a>
                                <?php endif ?>
                            </div>
                        </div>
                        <address class="list-address">
                            <?php /*if(!empty($arItem["PROPERTIES"]["EMAIL"]["VALUE"])):?><span class="row"><strong>E-mail:</strong><span> <?=$arItem["PROPERTIES"]["EMAIL"]["VALUE"]?></span></span><?endif;*/ ?>
                            <?php if (!empty($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"])): ?><span class="row">
                                <strong>Адрес:</strong><span> <?= htmlspecialchars_decode($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"]) ?></span>
                                </span><?php endif; ?>
                            <?php if (!empty($phones)): ?>
                                <?php foreach($phones as $index => $phone): ?>
                                    <?php
                                    $isEqual = PhoneFormatter::comparePhones(HOTLINE_PHONE_NUMBER, $phone);
                                    if($isEqual){
                                        $phone = PhoneFormatter::formatPhoneForDisplay(GLOBAL_PHONE_NUMBER);
                                    }
                                    $phoneDescription = empty($phoneDescriptions[$index]) ? 'Телефон' : $phoneDescriptions[$index];
                                    ?>
                                    <span class="row"><strong><?php echo $phoneDescription; ?>:</strong>
                                        <span class="clinics-phone js-phone" data-clinic-id="<?=$arItem['ID'];?>">
                                            <?php echo $phone; ?>
                                        </span>
                                        <?php if ($isEqual): ?>
                                            <?php if(CLIENT_FROM_ABROAD): ?>
                                                <div class="phone-free">звонок из-за рубежа</div>
                                            <?php else: ?>
                                                <div class="phone-free">звонок бесплатный</div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </address>
                        <?if(count($arResult['ITEMS']) == 1):?>
                            <?php
                                $resel = CIBlockElement::GetList(Array(), array('IBLOCK_ID' => $arResult['ITEMS'][0]['IBLOCK_ID'],'ID' => $arResult['ITEMS'][0]['ID']), false, false, array("NAME", "PROPERTY_ADDRESS", "PROPERTY_COORD","DETAIL_PAGE_URL","PROPERTY_PHONE"));
                                $countel = $resel->SelectedRowsCount();
                                while ($arresel = $resel->GetNext()) {
                                    $address = str_replace("\n", "", $arresel["PROPERTY_ADDRESS_VALUE"]["TEXT"]);
                                    $address = str_replace("\r", "", $address);

                                    $maps[] = array(
                                        "NAME"=>$arresel["NAME"],
                                        "ADDRESS"=>htmlspecialchars_decode($address),
                                        "COORD"=>$arresel["PROPERTY_COORD_VALUE"],
                                        "URL" =>$arresel["DETAIL_PAGE_URL"],
                                        "PHONE" => implode(', ', $arresel['PROPERTY_PHONE_VALUE'])
                                    );
                                }
                                ?>
                        <?endif;?>
                        <ul class="info-list">
                            <li>
                                <p>
                                    <?= $arItem["PREVIEW_TEXT"] ?>
                                </p>
                            </li>
                        </ul>

                        <?php if (!empty($arItem["SERVICES_MENU"])): ?>
                            <ul class="service-tree-menu service-tree-menu-clinics">
                        <?php
                        $previousLevel = 0;
                        foreach($arItem["SERVICES_MENU"] as $serviceItem):
                        ?>
                            <?if ($previousLevel && $serviceItem["DEPTH_LEVEL"] < $previousLevel):?>
                                <?=str_repeat("</ul></li>", ($previousLevel - $serviceItem["DEPTH_LEVEL"]));?>
                            <?endif?>

                            <?if ($serviceItem["IS_PARENT"]):?>
                                        <li class="tree expand"><i></i><a href="<?=$serviceUrlPrefix.$serviceItem["LINK"].'clinic/'.$arItem['CODE'].'/';?>"><?=$serviceItem["TEXT"]?></a>
                                        <ul>

                            <?else:?>
                                        <li>
                                            <a href="<?=$serviceUrlPrefix.$serviceItem["LINK"].'clinic/'.$arItem['CODE'].'/'?>"><?=$serviceItem["TEXT"]?></a>
                                        </li>
                            <?endif?>

                            <?$previousLevel = $serviceItem["DEPTH_LEVEL"];?>

                        <?endforeach?>

                        <?if ($previousLevel > 1)://close last item tags?>
                            <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
                        <?endif?>
                            </ul>
                        <?endif?>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    <span class="name-title red-color">Для вашего города клиник не найдено</span>
<?endif; ?>
