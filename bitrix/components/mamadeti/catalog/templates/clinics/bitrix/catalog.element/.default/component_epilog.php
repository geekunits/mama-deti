<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
//$APPLICATION->AddHeadScript('http://api-maps.yandex.ru/2.1/?lang=ru_RU');
$APPLICATION->SetPageProperty('CHANGE-REGION-URL','/clinics/');
// $APPLICATION->SetPageProperty('CLINIC', $arResult);

global $currentClinicId;
$currentClinicId = $arResult['ID'];

global $currentClinicCityId;
$currentClinicCityId = (int) $arResult['PROPERTIES']['CITY']['VALUE'];

redirectCityId($arResult['PROPERTIES']['CITY']['VALUE']);
$formatter = new ClinicResultFormatter($arResult['ID']);
$clinicMenu = isset($arResult['CLINIC_MENU']) ? $arResult['CLINIC_MENU'] : new ClinicMenu($formatter->format());
$currentTab = $clinicMenu->getCurrentTab();

$GLOBALS['CLINIC_MENU'] = $clinicMenu;

if (!empty($currentTab)) {
	$APPLICATION->SetPageProperty('title', $arResult['NAME'] . ' - ' . $currentTab->getTitle());
}

// if (!empty($arResult['NAME'])) {


// 	if (empty($currentTab) || $currentTab->getName() === 'description') {

// 		// $APPLICATION->AddChainItem($arResult['NAME']);

// 		$arResult['SECTION']['PATH'][] = array(
// 		   'NAME' => $arResult['NAME'],
// 		   'PATH' => ' '
// 		);
// 	} else {

// 		// $APPLICATION->AddChainItem($arResult['NAME'], $arResult['DETAIL_PAGE_URL']);
// 		// $APPLICATION->AddChainItem($currentTab->getTitle());


// 		$arResult['SECTION']['PATH'][] = array(
// 		   'NAME' => $arResult['NAME'],
// 		   'PATH' => $arResult['DETAIL_PAGE_URL'],
// 		);

// 		$arResult['SECTION']['PATH'][] = array(
// 		   'NAME' => $currentTab->getTitle(),
// 		   'PATH' => ' '
// 		);
// 	}

// }

if(isset($arResult['TIMESTAMP_X'])){
    $timestamp = MakeTimeStamp($arResult['TIMESTAMP_X'], CSite::GetDateFormat());
    setLastModified($timestamp);
}