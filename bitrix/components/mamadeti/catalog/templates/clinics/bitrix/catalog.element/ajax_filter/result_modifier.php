<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["SEL_SPECIALTY"] = intval($_REQUEST["specialty"]);

$arResult["SEL_SERVICES"] = array();
$arResult['SEL_SUB_SERVICES'] = array();
if (is_array($_REQUEST["services"])) {
    foreach ($_REQUEST["services"] as $val) {
        if (substr($val, 0, 1) == 'E') {
            $serviceId = intval(substr($val, 1));
            $arResult["SEL_SERVICES"][] = $serviceId;
            $arResult['SEL_SUB_SERVICES'] = substr($val, 0, 1) . $serviceId;
        }
    }
}

$arResult["SEL_ALPHABET"] = $_REQUEST["alphabet"];

$arrFilter = array();
$arrFilter["=PROPERTY_CLINIC"] = $arResult["ID"];

/**********************************/

$arFilter = array("PROPERTY_CLINIC"=>$arResult["ID"]);
if ($arResult["SEL_ALPHABET"]) {
    $arFilter["NAME"] = $arResult["SEL_ALPHABET"] . "%";
}
if (!empty($arResult["SEL_SERVICES"])){
    $arFilter["PROPERTY_SERVICES"] = $arResult["SEL_SERVICES"];
}

$arResult["SPECIALTY"] = CMamaDetiAPI::getDoctorsSpecialty($arFilter);

/**********************************/

$arFilter = array("PROPERTY_CLINIC"=>$arResult["ID"]);
$arResult["ALL_SERVICES"] = CMamaDetiAPI::getDoctorsServices($arFilter);
if ($arResult["SEL_SPECIALTY"] > 0) {
    $arFilter["PROPERTY_SPECIALTY"] = $arResult["SEL_SPECIALTY"];
}
if ($arResult["SEL_ALPHABET"]) {
    $arFilter["NAME"] = $arResult["SEL_ALPHABET"] . "%";
}

$arResult["SERVICES"] = CMamaDetiAPI::getDoctorsServices($arFilter);

/**********************************/

$arFilter = array("PROPERTY_CLINIC"=>$arResult["ID"]);
$arResult["ALL_ALPHABET"] = CMamaDetiAPI::getDoctorsFirstAlphabet($arFilter);
if ($arResult["SEL_SPECIALTY"] > 0){
    $arFilter["PROPERTY_SPECIALTY"] = $arResult["SEL_SPECIALTY"];
}
if (!empty($arResult["SEL_SERVICES"])) {
    $arFilter["PROPERTY_SERVICES"] = $arResult["SEL_SERVICES"];
}

$arResult["ALPHABET"] = CMamaDetiAPI::getDoctorsFirstAlphabet($arFilter);
if (!empty($arResult['ALPHABET']) && !empty($arResult['SEL_ALPHABET']) && !in_array($arResult['SEL_ALPHABET'], $arResult['ALPHABET'])) {
    $arResult['SEL_ALPHABET'] = $arResult['ALPHABET'][0];
}

/**********************************/

$arFilter = array("PROPERTY_CLINIC"=>$arResult["ID"]);
if ($arResult["SEL_SPECIALTY"] > 0) {
    $arFilter["PROPERTY_SPECIALTY"] = $arResult["SEL_SPECIALTY"];
}
if (!empty($arResult["SEL_SERVICES"])) {
    $arFilter["PROPERTY_SERVICES"] = $arResult["SEL_SERVICES"];
}

if (empty($arResult["SEL_ALPHABET"])) {
    //$arResult['SEL_ALPHABET'] = 'А';
}
if (!empty($arResult['SEL_ALPHABET'])) {
    $arFilter["NAME"] = $arResult["SEL_ALPHABET"] . "%";
}

$arResult["DOCTORS"] = CMamaDetiAPI::getDoctors($arFilter);

/**********************************/

$arResult["TREE_ALL_SERVICES"] = CMamaDetiAPI::GetSelectedServiceTree(array_keys($arResult["ALL_SERVICES"]));
$arResult["TREE_SERVICES"] = CMamaDetiAPI::GetSelectedServiceTree(array_keys($arResult["SERVICES"]));

$arResult["TREE_VISIBLE"] = array();

foreach ($arResult["TREE_SERVICES"] as $arItem) {
    $arResult["TREE_VISIBLE"][] = $arItem["ID"];
}

if ($arResult["SEL_SPECIALTY"] > 0) {
    $arrFilter["PROPERTY_SPECIALTY"] = $arResult["SEL_SPECIALTY"];
}

$arrFilter[] = array("LOGIC" => "AND", array("NAME" => $arResult["SEL_ALPHABET"] . "%", "PROPERTY_SERVICES" => $arResult["SEL_SERVICES"]));

$arResult["DOCTORS_FILTER"] = $arrFilter;

$arResult["FILTER_APPLY"] = isset($_REQUEST["filter_apply"]) && $_REQUEST["filter_apply"] == 'y';
?>