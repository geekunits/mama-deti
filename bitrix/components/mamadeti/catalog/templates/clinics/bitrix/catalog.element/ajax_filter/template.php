<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php if ($arResult["FILTER_APPLY"]):?>
    <?php

    $jsSpecialty = array();
    foreach ($arResult["SPECIALTY"] as $id => $name) {
        $jsSpecialty[] = array("ID" => $id, "NAME" => htmlspecialcharsbx($name));
    }

    $jsDoctors = array();
    foreach ($arResult["DOCTORS"] as $arDoctor) {
        $jsDoctors[] = array("NAME" => $arDoctor["NAME"], "DETAIL_PAGE_URL" => $arDoctor["DETAIL_PAGE_URL"] . '?clinic=' . $arResult['ID']);
    }

    $jsAvailService = array();
    foreach ($arResult["TREE_SERVICES"] as $arItem) {
        $jsAvailService[] = $arItem["ID"];
    }
    $selSpecialty = isset($arResult['SEL_SPECIALTY']) && isset($arResult['SPECIALTY'][$arResult['SEL_SPECIALTY']]) ? $arResult['SEL_SPECIALTY'] : null;

    ?>
    <div id="ajax_script">
        <script>
            var selAlphabet = <?=CUtil::PhpToJSObject($arResult["SEL_ALPHABET"])?>;
            var arSpecialty = <?=CUtil::PhpToJSObject($jsSpecialty)?>;
            var arDoctors = <?=CUtil::PhpToJSObject($jsDoctors)?>;
            var arService = <?=CUtil::PhpToJSObject($jsAvailService)?>;
            var arAlphabet = <?=CUtil::PhpToJSObject($arResult["ALPHABET"])?>;

            var selSpecialty = $('select[name="specialty"]'),
                curSpecialty = parseInt(selSpecialty.val());

            selSpecialty.find('option:not(:first)').remove();
            $(arSpecialty).each(function () {
                this.ID;
                if (this.ID == curSpecialty) {
                    selSpecialty.append('<option value="' + this.ID + '" selected="selected">' + this.NAME + '</option>'); 
                } else {
                    selSpecialty.append('<option value="' + this.ID + '">' + this.NAME + '</option>');
                }
            });
            selSpecialty.trigger('refresh');

            $('.b-dest_alph__letter-wrap a').removeClass('b-dest_alph__letter-active').filter(function () {
                return $(this).data("id") == selAlphabet;
            }).addClass('b-dest_alph__letter-active');

            var doctorTree = $('.b-dest_alph__list-scroll');
            doctorTree.find('div').remove();

            $(arDoctors).each(function () {
                    doctorTree.append('<div class="b-dest_alph__list-doctors___item">'
                        + '<a href="' + this.DETAIL_PAGE_URL + '" target="_blank">' + this.NAME + '</a>'
                        + '</div>'
                    );
                }
            );

            $('.b-tree_links__wrap [id^=st_]').each(function () {
                if ($.inArray($(this).attr("id").substr(3), arService) == -1) {
                    $(this).css('display', 'none'); 
                } else {
                    $(this).css('display', '');
                }
            });

            $('.b-dest_alph__letter-wrap a').each(function () {
                if ($.inArray($(this).data("id"), arAlphabet) == -1) {
                    $(this).css('display', 'none'); 
                } else {
                    $(this).css('display', '');
                }
            });


        </script>
    </div>
		<div id="ajax_section">
        <?php 
            $selectedServices = [];
            foreach($arResult['ALL_SERVICES'] as $id => $name) {
                if (in_array($id, $arResult['SEL_SERVICES'])) {
                    $selectedServices[$id] = $name;
                }
            }
            $searchTitle = 'Врачи ';
            if ($selSpecialty) {
                $searchTitle .= ' по специальности ' . lowerCaseFirst($arResult['SPECIALTY'][$selSpecialty]);
            } else {
                $searchTitle .= ' всех специальностей';
            }
            if ($arResult["SEL_ALPHABET"]) {
                $searchTitle .= ' на букву ' . $arResult['SEL_ALPHABET'];
            }
            if (!empty($selectedServices)) {
                $searchTitle .= ', оказывающих услуги: ';
                $selectedServicesList = implode('</li><li>', array_map(function($item) {
                    return lowerCaseFirst($item);
                }, $selectedServices));
            } else {
                $searchTitle .= ' во всех отделениях';
            }
        ?>
            <div class="b-clinic_doctors b-clinic_margin-bottom">

                <h2><?php echo $searchTitle; ?></h2> 
                <?php if(!empty($selectedServicesList)) { ?>
                    <ul class="b-clinic_doctors_selected__services">
                        <li><?php echo $selectedServicesList; ?></li>
                    </ul>
                <?php } ?>
        		<?php
                    $GLOBALS["arrFilterDoctor"] = $arResult["DOCTORS_FILTER"];
                    $GLOBALS["arrFilterDoctor"]['PROPERTY_CLINIC'] = $arResult['ID'];
                ?>
        		<?$APPLICATION->IncludeComponent("bitrix:news.list", "clinics-doctors", Array(
        			"DISPLAY_DATE" => "N",	// Выводить дату элемента
        			"DISPLAY_NAME" => "Y",	// Выводить название элемента
        			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
        			"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
        			"AJAX_MODE" => "N",	// Включить режим AJAX
        			"IBLOCK_TYPE" => "content",	// Тип информационного блока (используется только для проверки)
        			"IBLOCK_ID" => "3",	// Код информационного блока
        			"NEWS_COUNT" => "100",	// Количество новостей на странице
        			"SORT_BY1" => "NAME",	// Поле для первой сортировки новостей
        			"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
        			"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        			"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        			"FILTER_NAME" => "arrFilterDoctor",	// Фильтр
        			"FIELD_CODE" => "",	// Поля
        			"PROPERTY_CODE" => array(	// Свойства
        				0 => "POST",
        			),
        			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        			"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
        			"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        			"SET_TITLE" => "N",	// Устанавливать заголовок страницы
        			"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
        			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
        			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
        			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
        			"PARENT_SECTION" => "",	// ID раздела
        			"PARENT_SECTION_CODE" => "",	// Код раздела
        			"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
        			"CACHE_TYPE" => "A",	// Тип кеширования
        			"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
        			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
        			"CACHE_GROUPS" => "Y",	// Учитывать права доступа
        			"PAGER_TEMPLATE" => "mamadeti",	// Шаблон постраничной навигации
        			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        			"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
        			"PAGER_TITLE" => "",	// Название категорий
        			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
        			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
        			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
        			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
        			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        			),
        			false
        		);?>
    		</div>
        </div>
<?php else: ?>
    <?php
        $previousLevel = 0;
        $serviceId = null;
        if (!empty($arResult["SEL_SUB_SERVICES"])) {
            $serviceId = array_pop($arResult['SEL_SUB_SERVICES']);
        }
    ?>
<form id="search-tree-filter" action="<?= $APPLICATION->GetCurPage(); ?>">
    <input type="hidden" name="alphabet" value="<?= htmlspecialcharsbx($arResult["SEL_ALPHABET"]) ?>">
    <div class="b-filters_doctors__left">
    
        <div class="jq-selectbox__select__red b-clinic_program__select">
            <select name="specialty">
                <option value="0">Все специальности</option>
                <?php foreach ($arResult["SPECIALTY"] as $id => $name): ?>
                    <option value="<?= $id; ?>"<?php if ($id == $selSpecialty): ?> selected="selected"<?php endif ?>><?= htmlspecialcharsbx($name); ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="b-tree_links__wrap">
              <?php for ($index = 0, $arItems = $arResult['TREE_ALL_SERVICES'], $count = count($arResult['TREE_ALL_SERVICES']); $index < $count; $index++): ?>
                    <?php
                        $arItem = $arItems[$index];
                        $arNextItem = $index + 1 < $count ? $arItems[$index + 1] : null;
                        $hasSubItems = $arNextItem && $arNextItem['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'];
                    ?>
                    <?php for ($depth = $previousLevel; $depth > $arItem['DEPTH_LEVEL']; $depth--): ?>
                        </div></div>
                    <?php endfor; ?>
                    <div class="b-tree_link__item" id="st_<?php echo $arItem['ID']; ?>">
                        <div class="b-tree_link">
                            <?php if ($hasSubItems): ?>
                                <div class="b-tree_link__plus js-tree-click">
                                    <div class="js-tree-plus">+</div>
                                </div>
                                <span class="js-tree-select" href="#" data-id="<?= $arItem["ID"]; ?>"><?php echo $arItem['TEXT']; ?></span>
                            <?php else: ?>
                                 <input type="checkbox" 
                                    name="services[]"
                                    value="<?= $arItem["ID"] ?>"
                                    <?php if (in_array(substr($arItem["ID"], 1), $arResult["SEL_SERVICES"])): ?> checked="checked" <?php endif; ?>>&nbsp;<?= $arItem["TEXT"]; ?>
                            <?php endif; ?>
                        </div>
                        <?php if ($hasSubItems): ?>
                            <div class="b-tree_links__sub js-tree_links__sub-<?php echo $arItem['DEPTH_LEVEL']; ?>">
                        <?php else: ?>
                            </div>
                        <?php endif; ?>
                    <?php $previousLevel = $arItem['DEPTH_LEVEL']; ?>
                <?php endfor; ?>
                <?php while($previousLevel-- > 1): ?>
                    </div></div>
                <?php endwhile; ?>         
        </div>
    </div>
    <div class="b-filters_doctors__right">
        <div class="b-dest_alph">
            <div class="b-dest_alph__letter-wrap">
                <?php foreach ($arResult["ALL_ALPHABET"] as $val): ?>
                    <a 
                        class="b-dest_alph__letter <?= $val == $arResult["SEL_ALPHABET"] ? 'b-dest_alph__letter-active' : ''; ?>" 
                        href="<?= $APPLICATION->GetCurPageParam('alphabet=' . htmlspecialcharsbx($val), array('alphabet')); ?>"
                        data-id="<?= htmlspecialcharsbx($val); ?>"
                        <?php if (!in_array($val, $arResult["ALPHABET"])):?> 
                            style="display:none;"
                        <?php endif; ?>
                    >
                        <span class="b-dest_alph__letter___wrap"><?= $val; ?></span>
                    </a>
                <?php endforeach; ?>
            </div>
            <div class="b-dest_alph__list-doctors">
                <div class="b-dest_alph__list-scroll">
                    <?php foreach ($arResult["DOCTORS"] as $arDoctor): ?>
                        <div class="b-dest_alph__list-doctors___item">
                            <a href="<?= $arDoctor['DETAIL_PAGE_URL'] . '?clinic=' . $arResult['ID']; ?>" target="_blank"><?= $arDoctor['NAME']; ?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</form>

    <script>

        var form = $('#search-tree-filter').submit(
            function (e) {

                BX.showWait(BX('search-tree-filter'));

                $.ajax({
                    url: window.location.pathname+'?filter_apply=y&' + form.serialize(), 
                    dataType: 'html', 
                    type: 'POST', 
                    data: {ajax_filter: "y"}
                }).done(function (data) {
                        data = '<div>' + data + '</div>';
                        $('#ajax_section').html($(data).find('#ajax_section'));
                        $('.sort-panel').html($(data).find('.sort-panel'));
                        eval($(data).find("#ajax_script script").html());

                }).always(function () {
                    BX.closeWait(BX('search-tree-filter'));
                });

                return false;
            }
        );

        form.find('input[name="services[]"]').change(function() {
            form.submit();
        });

        form.find(".b-dest_alph__letter-wrap a").click(function () {
            if ($(this).hasClass('b-dest_alph__letter-active')) {
                form.find('input[name="alphabet"]').val('');
            } else {
                form.find('input[name="alphabet"]').val($(this).data('id'));
            }
            form.submit();
            return false;
        });
        form.find('select').change(function () {
            form.submit();
        });

    </script>

<?php endif; ?>