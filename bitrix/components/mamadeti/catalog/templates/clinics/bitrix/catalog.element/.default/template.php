<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php

if ($arParams['ADD_SECTIONS_CHAIN'] && !empty($arResult['NAME'])) {

   $component = $this->__component;
   $component->arResult = $arResult;
}

$servicesList = array('FB' => 'Facebook','VK' => 'Vkontakte','OK' => 'Одноклассники', 'INST' => 'Instagram', 'YOUTUBE' => 'Youtube', 'TW' => "Twitter");
$arServices = array();
foreach ($servicesList as $n => $name) {
    if (!empty($arResult['PROPERTIES'][$n . '_URL']['VALUE'])) {
        $url = null;

        ob_start();
        switch ($n) {
		case 'TW':
			?>
				<a class="twitter-timeline" data-lang="ru" data-width="300" data-height="255" data-dnt="true" data-theme="light" href="<?=$arResult['PROPERTIES'][$n . '_URL']['VALUE']?>"><?=$servicesList[$n]?></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
			<?
		break;
            case 'FB':
                ?>
                <div id="fb-root"></div>
                <script>

                    setTimeout(function () {

                        (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) {
                                return;
                            }

                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.3";
                            fjs.parentNode.insertBefore(js, fjs);
                        })(document, "script", "facebook-jssdk");
                    }, 1);
                </script>
                <style>
                    .fb-page {
                        width: 302px;
                    }
                    .fb-page span,
                    .fb-page iframe {
                        width: 302px! important;
                        height: 230px! important;
                    }
                </style>
                <div class="fb-page"
                    data-href="<?php echo $arResult['PROPERTIES']['FB_URL']['VALUE']; ?>"
                    data-width="302"
                    data-adapt-container="false"
                    data-hide-cover="false"
                    data-show-facepile="true"
                    data-show-posts="false">
                    <div class="fb-xfbml-parse-ignore">
                        <blockquote cite="<?=$arResult['PROPERTIES']['FB_URL']['VALUE']?>">
                            <a href="<?=$arResult['PROPERTIES']['FB_URL']['VALUE']?>"><?=$arResult['NAME']?></a>
                        </blockquote>
                    </div>
                </div>
                <?break;
            case 'YOUTUBE':
                if (!empty($arResult['PROPERTIES']['YOUTUBE_URL']['VALUE'])) {
                    $url = $arResult['PROPERTIES']['YOUTUBE_URL']['VALUE'];
                }
            break;

            case 'VK':
                preg_match("/([^\/]+)$/", $arResult['PROPERTIES']['VK_URL']['VALUE'], $url);
                ?>
                <iframe src="http://vk.com/widget_community.php?gid=<?=CMamaDetiAPI::getSocialGroupId($arResult['PROPERTIES']['VK_URL']['VALUE'],'vk')?>&width=302&height=250" width="302" height="250" scrolling="no" frameborder="0"></iframe>
                <?break;

            case 'OK':
                ?>
                <div id="ok_group_widget_clinic2"></div>
                <script>
                    !function (d, id, did, st) {
                        var js = d.createElement("script");
                        js.src = "http://connect.ok.ru/connect.js";
                        js.onload = js.onreadystatechange = function () {
                            if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                                if (!this.executed) {
                                    this.executed = true;
                                    setTimeout(function () {
                                        OK.CONNECT.insertGroupWidget(id,did,st);
                                    }, 0);
                                }
                            }}
                        d.documentElement.appendChild(js);
                    }(document,"ok_group_widget_clinic2","<?=CMamaDetiAPI::getSocialGroupId($arResult['PROPERTIES']['OK_ID']['VALUE'],'ok')?>","{width:302,height:250}");
                </script>
                <?break;

            case 'INST':

                /*
                 *
                 * Any app created before Nov 17, 2015 will continue to function
                 * until June 1, 2016. On that date, the app will automatically
                 * be moved to Sandbox Mode if it wasn't approved through the review process.
                 *

                $instagramUrl = $arResult['PROPERTIES']['INST_URL']['VALUE'];
                $parts = array_filter(explode('/', $instagramUrl));
                if (!empty($parts)) {
                    $instagramLogin = end($parts);
                }
                if (!empty($instagramLogin)) {

                    $api = Instagram::getInstance();
                    try {
                        $instagramUser = $api->findUser($instagramLogin);
                        if (!empty($instagramUser)) {
                            $instagramMedia = $api->getUserMedia($instagramUser->id);
                            $instagramImages = $instagramMedia->data;
                        } else {
                            $instagramImages = [];
                        }
                    } catch (Exception $e) {

                    }
                    if (!empty($instagramUser)) {
                        $APPLICATION->IncludeFile('/includes/clinics/instagram.php', compact('instagramUser', 'instagramImages'));
                    }
                }
                */

                if (!empty($arResult['PROPERTIES']['INST_URL']['VALUE'])) {
                  $instagramUrl = $arResult['PROPERTIES']['INST_URL']['VALUE'];
                  $parts = array_filter(explode('/', $instagramUrl));
                  if (!empty($parts)) {
                      $instagramLogin = end($parts);
                  }
                  $APPLICATION->IncludeFile('/includes/clinics/instagramDummy.php', compact('instagramLogin', 'instagramUrl'));
                }

                break;
        }
        $arServices[] = array($name, ob_get_contents(), $url);
        ob_end_clean();
    }
}
$clinicMenu = new ClinicMenu($arResult);
$clinicMenu->getActiveTabs();
$currentTabName = $clinicMenu->getCurrentTabName();
$currentTab = $clinicMenu->getCurrentTab();
$arResult['CLINIC_MENU'] = $clinicMenu;
?>
<div class="b-bg_white b-clinic_card__wrap js-clinic-card" itemscope itemtype="http://schema.org/Organization">
    <div class="b-clinic_card__padding">
        <h1 class="b-header_h1 align-center" itemprop="name"><?php echo $arResult["NAME"]; ?></h1>
    </div>
    <?php $clinicMenu->render(); ?>
    <div class="b-clinic_card__padding js-clinic-content clearfix">
        <div class="b-clinic_card__left js-left-content" data-ajax-zone="clinic-left" data-ajax-key="<?php echo $currentTabName === 'description' ? 'main' : 'additional'; ?>">
            <?php if (!empty($arResult['RESIZED'])): ?>
                <div class="b-clinic_card__logo">
                    <img src="<?php echo $arResult["RESIZED"]["src"]; ?>" alt="<?php echo $arResult["NAME"]; ?>">
                </div>
            <?php endif; ?>
            <?php if ($currentTabName === 'description'): ?>
                <div class="b-clinic_gallery__wrap">
                    <?php $APPLICATION->IncludeFile('/includes/clinics/gallery.php', compact('arResult')); ?>
                </div>
            <?php endif; ?>
            <?php if ($currentTabName !== 'description'): ?>
                <div class="b-clinic_address__wrap b-clinic_address__left">
                    <div class="b-clinic_address__padding">
                        <?php $APPLICATION->IncludeFile('/includes/clinics/address.php', array(
                            'arResult' => $arResult,
                            'usePhoneColumns' => false,
                        )); ?>
                    </div>
                </div>
                <?php if (count($arServices > 0)): ?>
                    <div class="b-clinic_social__wrap">
                        <?php $APPLICATION->IncludeFile('/includes/clinics/social.php', compact('arServices', 'arResult')); ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="b-clinic_card__right js-right-content" data-ajax-zone="clinic-right">
            <?php if ($currentTab): ?>
                <?php $currentTab->renderContent(compact('arResult', 'arParams', 'arServices', 'component')); ?>
            <?php endif; ?>
        </div>

    </div>
</div>
<div class="js-bottom-content" data-ajax-zone="clinic-bottom">
    <?php if ($currentTabName === 'description'): ?>
        <div class="b-bg_white b-clinic_news__wrap <?php echo count($arServices) > 0 ? 'b-clinic_new__withsocial' : ''; ?> b-clinic_card__padding">
            <?php if (count($arServices) > 0): ?>
                <div class="b-clinic_social__wrap b-clinic_social__fixed">
                    <?php $APPLICATION->IncludeFile('/includes/clinics/social.php', compact('arServices', 'arResult')); ?>
                </div>
            <?php endif; ?>
            <?php $APPLICATION->IncludeFile('/includes/clinics/news.php', compact('arResult')); ?>
        </div>

        <?php $APPLICATION->IncludeFile('/includes/clinics/articles.php', compact('arResult')); ?>
    <?php endif; ?>
</div>