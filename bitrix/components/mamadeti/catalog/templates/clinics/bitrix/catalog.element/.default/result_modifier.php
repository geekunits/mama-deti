<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
if (is_array($arResult["PROPERTIES"]["SERVICES"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SERVICES"]["VALUE"])) {
    $arResult["SERVICES_MENU"] = CMamaDetiAPI::GetSelectedServiceTreeMenu($arResult["PROPERTIES"]["SERVICES"]["VALUE"]);
}

$rsDoctors = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ACTIVE"=>"Y","PROPERTY_CLINIC"=>$arResult["ID"]),false,false,array("ID"));
$arResult["DOCTOR_CNT"] = $rsDoctors->SelectedRowsCount();

/**REVIEWS**/
$arResult["REVIEWS"] = array();
$arFilter = Array("IBLOCK_ID"=>11, "ACTIVE"=>"Y", "PROPERTY_CLINIC"=>$arResult["ID"]);
$rsReview = CIBlockElement::GetList(Array("SORT"=>"asc", "NAME"=>"asc"), $arFilter, false, array("nTopCount"=>6), array("PREVIEW_TEXT", "DETAIL_PAGE_URL"));
while ($arReview = $rsReview->GetNext())
    $arResult["REVIEWS"][] = $arReview;

/**PROGRAMS*/
$arResult['PROGRAMS'] = array();
$rsElement = CIBlockElement::GetList(
    array("SORT"=>"ASC"),
    array(
        "IBLOCK_ID"=>27,
        "PROPERTY_CLINIC" =>$arResult["ID"],
    ),
    false,
    false,
    array(
        'IBLOCK_ID',
        'ID',
        'PROPERTY_SERVICES',
        'PROPERTY_CLINIC',
    )
);
$arResult['RESIZED'] = empty($arResult['DETAIL_PICTURE']) ? $arResult['PREVIEW_PICTURE'] : $arResult['DETAIL_PICTURE'];
if ($arResult['RESIZED']) {
    $arResult['RESIZED'] = CFile::ResizeImageGet($arResult['RESIZED'], array('width' => 302, 'height' => 302), BX_RESIZE_IMAGE_EXACT);
}

/**FILTER FOR PROGRAMS**/
global $arrProgramFilter;
$arrProgramFilter['PROPERTY_CLINIC'] = $arResult['ID'];
while ($arElement = $rsElement->GetNext()) {
    $arResult["PROGRAMS"][] = $arElement;
}
$formatter = new ClinicResultFormatter($arResult['ID']);
$arResult['FOR_PRESS'] = $formatter->getForPress();

$this->__component->SetResultCacheKeys(array(
    "TIMESTAMP_X",
    "CACHED_TPL"
));