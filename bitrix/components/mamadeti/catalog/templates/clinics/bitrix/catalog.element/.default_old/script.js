function handleDoctorsTab()
{
	if (!$('#ajax_filter_doctors').length) {
		return;
	}

	$.ajax({url:window.location.pathname+'?ajax_filter=y'}).done(function(data)
	{
		$('#ajax_filter_doctors').html(data);
		$('input, select').styler();
	});

	$('#doctors_tab').on('tabchange', function()
	{
		if ($(this).hasClass('active'))
			$('#main-doctors-slider').hide(); else
			$('#main-doctors-slider').show();
	});
}

jQuery(document).ready(function($){
	handleDoctorsTab();

	


	$(document).on('click', '.ajax-price-list ul.pricelist-list li p.priceSectionName2, ul.pricelist-list i', function() {
		console.log($(this).closest('li').find('ul:first'));
		$(this).closest('li').find('ul:first').slideToggle();
		$(this).closest('li').toggleClass('open');
	});
});