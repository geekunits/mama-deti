<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
if ($arParams['ADD_SECTIONS_CHAIN'] && !empty($arResult['NAME'])) {
   $arResult['SECTION']['PATH'][] = array(
   'NAME' => $arResult['NAME'],
   'PATH' => ' ');

   $component = $this->__component;
   $component->arResult = $arResult;
}
?>
<div class="frame-holder clin-items-l">
    <div class="frame-image">
        <?if (!empty($arResult["DETAIL_PICTURE"]["SRC"])) {?>
            <img src="<?=MakeImage($arResult["DETAIL_PICTURE"]["SRC"],array("z"=>1, "w"=>280, "h"=>260))?>" alt="<?=$arResult["NAME"]?>">
        <?} elseif (!empty($arResult["PREVIEW_PICTURE"]["SRC"])) {?>
            <img src="<?=MakeImage($arResult["PREVIEW_PICTURE"]["SRC"],array("z"=>1, "w"=>280, "h"=>260))?>" alt="<?=$arResult["NAME"]?>">
        <?}?>
    </div>
    <div class="contacts-block">
        <a href="/ajax/clinic_location.php?id=<?=$arResult["ID"]?>" class="fancybox-map" data-fancybox-type="iframe">Показать на карте</a>
        <?/*if(!empty($arResult["PROPERTIES"]["WWW"]["VALUE"])):?>
        <div class="block">
            <a href="<?=$arResult["PROPERTIES"]["WWW"]["VALUE"]?>" class="red-color" target="_blank"><?=$arResult["PROPERTIES"]["WWW"]["VALUE"]?></a>
        </div>
        <?endif*/?>
        <?if (!empty($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"])) {?>
        <address class="address">
            <span><?=htmlspecialchars_decode($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"])?></span>
        </address>
        <?}?>
        <?if (!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"])) {?>
            <strong class="contact-phone"><?=$arResult["PROPERTIES"]["PHONE"]["VALUE"]?></strong>
        <?}?>
        <?if (!empty($arResult["PROPERTIES"]["CONTACT_EMAIL"]["VALUE"])) {?>
            <a href="mailto:<?=$arResult["PROPERTIES"]["CONTACT_EMAIL"]["VALUE"]?>" class="contact-mail"><?=$arResult["PROPERTIES"]["CONTACT_EMAIL"]["VALUE"]?></a>
        <?}?>
        </div>
    <ul class="button-list">
        <?php /*if ($arResult["PROPERTIES"]["VK_URL"]["VALUE"] || $arResult["PROPERTIES"]["FB_URL"]["VALUE"]): ?>
            <li>
                <span class="soc-title">Присоединяйтесь</span>
                <noindex>
                    <ul class="soc-list">
                        <?php if ($arResult["PROPERTIES"]["VK_URL"]["VALUE"]): ?>
                            <li>
                                <a href="<?php echo $arResult["PROPERTIES"]["VK_URL"]["VALUE"]; ?>" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/soc01.png" width="30" height="29" alt=""></a>
                            </li>
                        <?php endif; ?>
                        <?php if ($arResult["PROPERTIES"]["FB_URL"]["VALUE"]): ?>
                            <li>
                                <a href="<?php echo $arResult["PROPERTIES"]["FB_URL"]["VALUE"]; ?>" rel="nofollow"><img src="<?=SITE_TEMPLATE_PATH?>/images/soc03.png" width="30" height="29" alt=""></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </noindex>
            </li>
        <?php endif; */?>
        <li>
            <?/*<a href="#" class="button05 block-button link_ask" data-clinic="<?=$arResult["ID"]?>">Задать вопрос врачу</a>*/?>
            <a href="#" class="button05 block-button link_ask_direction" data-clinic="<?=$arResult["ID"]?>">Задать вопрос</a>
        </li>
        <li>
            <a href="#" data-name="<?=$arResult["NAME"]?>" class="button01 block-button link_reception_clinic" data-clinic="<?=$arResult["ID"]?>">Записаться на прием</a>
        </li>
        <li>
            <a href="#" class="button05 block-button link_review" data-type="clinic" data-clinic="<?=$arResult['ID']?>">Оставить отзыв</a>
        </li>
    </ul>
    <?php
    $servicesList = array('FB','VK','OK');
    $services = array();
    foreach ($servicesList as $n) {
        if (!empty($arResult['PROPERTIES'][$n.'_URL']['VALUE'])) {
            switch ($n) {
                case 'FB':
                    ob_start();?>
                    <div class="sect">
                        <div class="wrap in-bl">
                            <div id="fb-root"></div>
                            <script>
                                setTimeout(function () {
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.3";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    })(document, "script", "facebook-jssdk");
                                }, 1);
                            </script>

                            <div class="fb-page"
                                data-href="<?=$arResult['PROPERTIES']['FB_URL']['VALUE']?>"
                                data-width="280"
                                data-height="250"
                                data-hide-cover="false"
                                data-show-facepile="true"
                                data-show-posts="false">
                                <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="<?=$arResult['PROPERTIES']['FB_URL']['VALUE']?>">
                                        <a href="<?=$arResult['PROPERTIES']['FB_URL']['VALUE']?>"><?=$arResult['NAME']?></a>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?break;

                case 'VK':
                    preg_match("/([^\/]+)$/", $arResult['PROPERTIES']['VK_URL']['VALUE'], $url);
                    ob_start();?>
                    <div class="sect">
                        <div class="wrap in-bl">
                            <iframe src="http://vk.com/widget_community.php?gid=<?=CMamaDetiAPI::getSocialGroupId($arResult['PROPERTIES']['VK_URL']['VALUE'],'vk')?>&width=280&height=250" width="280" height="250" scrolling="no" frameborder="0"></iframe>
                        </div>
                    </div>
                    <?break;

                case 'OK':
                    ob_start();?>
                        <div class="sect">
                            <div class="wrap in-bl">
                                <div id="ok_group_widget"></div>
                                <script>
                                    !function (d, id, did, st) {
                                        var js = d.createElement("script");
                                        js.src = "http://connect.ok.ru/connect.js";
                                        js.onload = js.onreadystatechange = function () {
                                            if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                                                if (!this.executed) {
                                                    this.executed = true;
                                                    setTimeout(function () {
                                                        OK.CONNECT.insertGroupWidget(id,did,st);
                                                    }, 0);
                                                }
                                            }}
                                        d.documentElement.appendChild(js);
                                    }(document,"ok_group_widget","<?=CMamaDetiAPI::getSocialGroupId($arResult['PROPERTIES']['OK_ID']['VALUE'],'ok')?>","{width:280,height:250}");
                                </script>
                            </div>
                        </div>
                    <?break;
                    /*case 'INST':
                        ob_start();?>
                            <div class="sect">
                                <div class="wrap in-bl">
                                    <iframe src="http://widget.websta.me/in/hospitalmdufa/?s=140&w=2&h=2&b=0&p=5" allowtransparency="true" frameborder="0" scrolling="no" style="border:none;overflow:hidden;width:280px; height: 250px;" ></iframe>
                                </div>
                            </div>
                        <?break;*/
            }
            $services[$n] = ob_get_contents();
            ob_end_clean();
        }
    }
    if(count($services) != 0):?>
        <div class="social-tabs">
            <div class="title">Присоединяйтесь</div>
            <?php
            if(count($services) > 1):?>
            <div class="switchs">
                <?foreach ($services as $n => $value):?>
                        <div class="switch"><?=ucfirst(strtolower($n));?></div>
                <?endforeach;?>
            </div>
            <div class="sects">
                <?foreach ($services as $n => $value):?>
                        <?=$value;?>
                <?endforeach;?>
            </div>
            <?else:?>
            <div class="sects">
                <?=current($services)?>
            </div>
            <?endif;?>
        </div>
    <?endif;?>
</div>
<?php
ob_start();
global $arrActionsFilter;
$arrActionsFilter = array('PROPERTY_CLINIC' => $arResult['ID']);
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "actions",
    Array(
        "IBLOCK_TYPE"   =>      'contentsite',
        "IBLOCK_ID"     =>      32,
        "NEWS_COUNT"    =>      12,
        'FILTER_NAME'	=>		'arrActionsFilter',
        "SORT_BY1"      =>      'ACTIVE_FROM',
        "SORT_ORDER1"   =>      'DESC',
        "FIELD_CODE"    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
        "PROPERTY_CODE" =>      array('CLINIC', 'DATE_FROM', 'DATE_TO'),
        "SET_TITLE"     =>      'N',
        "SET_STATUS_404" => 'N',
        "INCLUDE_IBLOCK_INTO_CHAIN"     =>      'N',
        "DISPLAY_BOTTOM_PAGER"  =>      'Y',

        "DISPLAY_DATE"  =>      'N',
        "DISPLAY_NAME"  =>      "Y",
        "DISPLAY_PICTURE"       =>      'Y',
        "DISPLAY_PREVIEW_TEXT"  =>      'Y',
        //'CHECK_DATES' => 'Y',
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "mamadeti",
    ),
    $component
);

function separateRow(&$i,$average)
{
    if ($i == $average && $average > 3) {
        echo '</div></div><div class="bl-table"><div class="row">';
    }
    $i++;
}
$actionsBlock = ob_get_clean();

ob_start();
global $arrNewsFilter;
$arrNewsFilter['PROPERTY_CLINIC'] = $arResult['ID'];
$el = $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "clinic-news",
    Array(
        "IBLOCK_TYPE"   =>      'contentsite',
        "IBLOCK_ID"     =>      8,
        "NEWS_COUNT"    =>      10,
        'FILTER_NAME'	=>		'arrNewsFilter',
        "SORT_BY1"      =>      'ACTIVE_FROM',
        "SORT_ORDER1"   =>      'DESC',
        "FIELD_CODE"    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
        "PROPERTY_CODE" =>      array('CLINIC', 'DATE_FROM', 'DATE_TO'),
        "SET_TITLE"     =>      'N',
        "SET_STATUS_404" => 'N',
        "INCLUDE_IBLOCK_INTO_CHAIN"     =>      'N',
        "DISPLAY_BOTTOM_PAGER"  =>      'Y',

        "DISPLAY_DATE"  =>      'N',
        "DISPLAY_NAME"  =>      "Y",
        "DISPLAY_PICTURE"       =>      'Y',
        "DISPLAY_PREVIEW_TEXT"  =>      'Y',
        //'CHECK_DATES' => 'Y',
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "mamadeti",
        //"AJAX_MODE" => "Y",
        //"AJAX_OPTION_JUMP" => "N",
        //"AJAX_OPTION_STYLE" => "Y",
        //"AJAX_OPTION_HISTORY" => "N",
    ),
    $component
);

$newsBlock = ob_get_contents();
ob_end_clean();

$tabs = array(
    'Description' => 'Описание',
    'Actions' => 'Акции<span class="action-ico"></span>',
    'Services' => 'Выбрать услугу',
    'Doctors' => 'Выбрать врача',
    'Reviews' => 'Отзывы',
    'Schedule' => 'Расписание работы',
    'News' => 'Новости',
    'Contacts' => 'Контакты',
    'Information' => 'Персональные данные',
    'Tour' => 'Экскурсия',
    'OMS' => 'ОМС',
    'Leading' => 'Руководство',
    'PriceList' => 'Цены',
    'Programs' => 'Программы',
    'Meeting' => 'Популярная педиатрия',
    'Rules' => 'Информация',
    'Corporate' => 'Для корпоративных клиентов',
    'Law' => 'Законодательство',
    'DoctorOfMonth' => 'Врачи месяца',
    'Birth' => 'Роды',
    'Pregnancy' => 'Беременность',
);
$bTabDescription = !empty($arResult["PREVIEW_TEXT"]) || !empty($arResult["DETAIL_TEXT"]) || (!empty($arResult["PROPERTIES"]["LICENSE"]["VALUE"]) && is_array($arResult["PROPERTIES"]["LICENSE"]["VALUE"]));
$bTabActions = mb_strlen($actionsBlock)>10;
$bTabNews = mb_strlen($newsBlock) > 10;
$bTabServices = is_array($arResult["SERVICES_MENU"]) && !empty($arResult["SERVICES_MENU"]);
$bTabDoctors = $arResult["DOCTOR_CNT"]>0;
$bTabReviews = count($arResult["REVIEWS"])>0;
$bTabSchedule = (is_array($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"])) || (!empty($arResult["PROPERTIES"]["SCHEDULE_TEXT"]["VALUE"]["TEXT"]));
$bTabContacts = !empty($arResult["PROPERTIES"]["CONTACTS"]["VALUE"]["TEXT"]) || !empty($arResult["PROPERTIES"]["COORD"]["~VALUE"]);
$bTabPriceList = is_array(CIBlockSection::GetList(array(),array("IBLOCK_ID"=>31,"UF_CLINIC"=>$arResult["ID"],"ACTIVE"=>"Y"))->Fetch());
$bTabInformation = !empty($arResult["PROPERTIES"]["INFORMATION"]["VALUE"]["TEXT"]);
$bTabTour = !empty($arResult["PROPERTIES"]["TOUR"]["VALUE"]["TEXT"]);
$bTabOMS = !empty($arResult["PROPERTIES"]["OMS"]["VALUE"]["TEXT"]);
$bTabLeading = !empty($arResult["PROPERTIES"]["LEADING"]["VALUE"]["TEXT"]);
$bTabPrograms = !empty($arResult["PROGRAMS"]);
$bTabMeeting = !empty($arResult["PROPERTIES"]["MEETING"]["VALUE"]['TEXT']);
$bTabRules = !empty($arResult["PROPERTIES"]["RULES"]["VALUE"]['TEXT']);
$bTabCorporate = !empty($arResult["PROPERTIES"]["CORPORATE"]["VALUE"]['TEXT']);
$bTabLaw = !empty($arResult["PROPERTIES"]["LAW"]["VALUE"]['TEXT']);
$bTabDoctorOfMonth = !empty($arResult["PROPERTIES"]["DOCTOR_OF_MONTH"]["VALUE"]['TEXT']);
$bTabBirth = !empty($arResult["PROPERTIES"]["BIRTH"]["VALUE"]['TEXT']);
$bTabPregnancy = !empty($arResult["PROPERTIES"]["PREGNANCY"]["VALUE"]['TEXT']);
$existTabs = array();
foreach ($tabs as $tab => $title) {
    if (!empty(${'bTab'.$tab})) {
        $existTabs[strtolower($tab)] = $title;
    }
}
$average = ceil(count($existTabs)/2);

$currentTab = $_REQUEST['TAB_NAME'];
if (!isset($existTabs[$currentTab])) {
    $currentTab = 'description';
}

$i = 0;?>
<div class="block-holder">
    <h1 class="name-title red-color"><?=$arResult["NAME"]?></h1>
    <div class="clinics-tabs" data-title="<?php echo htmlspecialchars($APPLICATION->GetTitle()); ?>">
        <div class="triggers">
            <div class="bl-table">
                <div class="row">
                    <?php foreach ($existTabs as $tab => $title): ?>
                        <?php separateRow($i, $average); ?>
                        <div class="trigger cell <?php echo $tab === 'actions' ? 'action' : ''; ?> <?php echo $tab === $currentTab ? 'active' : ''; ?>">
                            <span class="wrap">
                                <span class="bl-table">
                                    <a href="<?php echo $arResult['DETAIL_PAGE_URL'].($tab !== 'description' ? $tab.'/' : ''); ?>" class="cell">
                                        <?php echo $title; ?>
                                    </a>
                                </span>
                            </span>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="">
            <?if ($currentTab === 'description'):?>
                <div class="section active">
                    <p><strong class="title"><?=$arResult["PREVIEW_TEXT"]?></strong></p>
                    <?/*<ul class="info-list">
                        <li></li>
                    </ul>*/?>
                    <div class="text-block">
                        <?=$arResult["DETAIL_TEXT"]?>
                    </div>
                    <?if(is_array($arResult["PROPERTIES"]["LICENSE"]["VALUE"]) && !empty($arResult["PROPERTIES"]["LICENSE"]["VALUE"])):?>
                        <span class="name-title red-color">Лицензии</span>

                        <div class="visual-gallery">
                            <a href="#" class="prev">prev</a>
                            <a href="#" class="next">next</a>
                            <div class="gallery-frame">
                                <ul class="gallery">
                                    <?foreach($arResult["PROPERTIES"]["LICENSE"]["VALUE"] as $key=>$photo):?>
                                    <li>
                                        <a class="fancybox-service-detail" rel="group1" href="<?=CFile::GetPath($photo)?>">
                                            <img src="<?=MakeImage($photo, array("z"=>1,"w"=>260,"h"=>230))?>" alt="<?=$arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"][$key]?>">
                                        </a>
                                    </li>
                                    <?endforeach;?>
                                </ul>
                            </div>
                        </div>
                    <?endif?>
                </div>
            <?endif?>
            <?if ($currentTab === 'actions'):?>
                <div class="section">
                    <div class="text-block">
                            <?=$actionsBlock;?>
                    </div>
                </div>
            <?endif?>
            <?if ($currentTab === 'services'):?>
                <div class="section">
                    <?if (is_array($arResult["SERVICES_MENU"]) && !empty($arResult["SERVICES_MENU"])):?>
                        <ul class="service-tree-menu service-tree-menu-clinics">
                        <?php
                        $previousLevel = 0;
                        foreach($arResult["SERVICES_MENU"] as $arItem):?>
                            <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                                <?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
                            <?endif?>

                            <?if ($arItem["IS_PARENT"]):?>
                                        <li class="tree expand"><i></i><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                                        <ul>

                            <?else:?>
                                        <li>
                                            <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                                        </li>
                            <?endif?>

                            <?$previousLevel = $arItem["DEPTH_LEVEL"];?>

                        <?endforeach?>

                        <?if ($previousLevel > 1)://close last item tags?>
                            <?=str_repeat("</ul></li>", ($previousLevel-1) );?>
                        <?endif?>
                        </ul>
                    <?endif?>
                </div>
            <?endif?>
            <?if ($currentTab === 'doctors'):?>
                <div class="section">
                    <div id="ajax_filter_doctors"></div>
                    <div id="ajax_section">
                    <?$GLOBALS["arrFilterDoctor"] = array("PROPERTY_CLINIC"=>$arResult["ID"]);?>
                    <?$APPLICATION->IncludeComponent("bitrix:news.list", "clinics-doctors", Array(
                        "DISPLAY_DATE" => "N",	// Выводить дату элемента
                        "DISPLAY_NAME" => "Y",	// Выводить название элемента
                        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                        //"AJAX_MODE" => "N",	// Включить режим AJAX
                        "IBLOCK_TYPE" => "content",	// Тип информационного блока (используется только для проверки)
                        "IBLOCK_ID" => "3",	// Код информационного блока
                        "NEWS_COUNT" => "100",	// Количество новостей на странице
                        "SORT_BY1" => "NAME",	// Поле для первой сортировки новостей
                        "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                        "FILTER_NAME" => "arrFilterDoctor",	// Фильтр
                        "FIELD_CODE" => "",	// Поля
                        "PROPERTY_CODE" => array(	// Свойства
                            0 => "POST",
                        ),
                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                        "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                        "SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                        "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                        "PARENT_SECTION" => "",	// ID раздела
                        "PARENT_SECTION_CODE" => "",	// Код раздела
                        "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                        "CACHE_TYPE" => "A",	// Тип кеширования
                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                        "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                        "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                        "PAGER_TITLE" => "",	// Название категорий
                        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                        //"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                        //"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                        //"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        "_REQUEST" => $_REQUEST,
                        ),
                        false
                    );?>
                    </div>
                </div>
            <?endif?>
            <?if ($currentTab === 'reviews'):?>
                <div class="section">
                    <a href="/reviews/?clinic=<?=$arResult["ID"]?>" class="button01 block-button" style="display:inline-block;padding: 0px 10px;">Все отзывы о <?=$arResult["NAME"]?></a>
                    <a href="/reviews/" class="button01 block-button" style="display:inline-block;padding: 0px 10px;">Все отзывы о врачах и клиниках</a>
                </div>
            <?endif?>
            <?if ($currentTab === 'schedule'):?>
                <div class="section">
                    <?if ($arResult["PROPERTIES"]["SCHEDULE_TEXT"]["VALUE"]["TYPE"] == "html"):?>
                        <?=$arResult["PROPERTIES"]["SCHEDULE_TEXT"]["~VALUE"]["TEXT"]?>
                    <?else:?>
                        <?=$arResult["PROPERTIES"]["SCHEDULE_TEXT"]["VALUE"]["TEXT"]?>
                    <?endif?>
                    <div class="tab-t-title">Прием врачей в клинике осуществляется по предварительной записи</div>
                    <br>
                    <?if (is_array($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"]) && !empty($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"])):?>
                        <table class="web-table schedule-table" border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <th>День недели</th>
                                <th>Время работы</th>
                            </tr>
                            <?$arDayOfWeek = array("Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье");
                            foreach ($arResult["PROPERTIES"]["SCHEDULE"]["VALUE"] as $key=>$value):?>
                                <tr>
                                    <td><?=$arDayOfWeek[$key]?></td>
                                    <td><?=$value?></td>
                                </tr>
                            <?endforeach?>
                        </table>
                    <?endif?>
                </div>
            <?endif?>
            <?if($currentTab === 'news'):?>
                <div class="section">
                    <div class="text-block">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "clinic-news",
                                Array(
                                    "IBLOCK_TYPE"   =>      'contentsite',
                                    "IBLOCK_ID"     =>      8,
                                    "NEWS_COUNT"    =>      10,
                                    'FILTER_NAME'	=>		'arrNewsFilter',
                                    "SORT_BY1"      =>      'ACTIVE_FROM',
                                    "SORT_ORDER1"   =>      'DESC',
                                    "FIELD_CODE"    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
                                    "PROPERTY_CODE" =>      array('CLINIC', 'DATE_FROM', 'DATE_TO'),
                                    "SET_TITLE"     =>      'N',
                                    "SET_STATUS_404" => 'N',
                                    "INCLUDE_IBLOCK_INTO_CHAIN"     =>      'N',
                                    "DISPLAY_BOTTOM_PAGER"  =>      'Y',
                                    //"CACHE_TYPE" => "N",
                                    //"CACHE_FILTER" => "N",
                                    "DISPLAY_DATE"  =>      'N',
                                    "DISPLAY_NAME"  =>      "Y",
                                    "DISPLAY_PICTURE"       =>      'Y',
                                    "DISPLAY_PREVIEW_TEXT"  =>      'Y',
                                    //'CHECK_DATES' => 'Y',
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => "mamadeti",
                                //	"AJAX_MODE" => "Y",
                                    //"AJAX_OPTION_JUMP" => "Y",
                                    //"AJAX_OPTION_STYLE" => "Y",
                                    //"AJAX_OPTION_HISTORY" => "N",
                                    "_REQUEST" => $_REQUEST,
                                ),
                                $component
                            );?>
                    </div>
                </div>
            <?endif;?>
            <?if ($currentTab === 'contacts'):?>
                <div class="section">
                    <?php
                    $arCoord = explode(',',$arResult["PROPERTIES"]["COORD"]["~VALUE"]);
                    ?>
                    <?if (count($arCoord) == 2):?>
                        <div class="right-block fl-r">
                            <div id="clinic_map" style="width:300px;height:300px;"></div>
                            <script>

                                var clinicMap;

                                ymaps.ready(clinic_map_init);

                                function clinic_map_init()
                                {
                                    clinicMap = new ymaps.Map('clinic_map', {
                                        center: [<?=$arCoord[0]?>, <?=$arCoord[1]?>], // Москва
                                        zoom: 16,
                                        controls: ['zoomControl']
                                    });

                                    var firstButton = new ymaps.control.Button({data: {content: "Печать"}, options: {selectOnClick: false}});

                                    firstButton.events.add(
                                        'press',
                                        function () {
                                            $.fn.clinicMapPrint('<?=$arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"]?>','<?=$arCoord[1].','.$arCoord[0]?>');
                                        }
                                    );

                                    clinicMap.controls.add(firstButton, {float: 'right'});

                                    myPlacemark = new ymaps.Placemark(clinicMap.getCenter(), {
                                        hintContent: '<?=$arResult["NAME"]?>'
                                    }, {
                                        // Опции.
                                        // Необходимо указать данный тип макета.
                                        iconLayout: 'default#image',
                                        // Своё изображение иконки метки.
                                        iconImageHref: '<?=SITE_TEMPLATE_PATH.'/images/hospital_ya_map.png'?>',
                                        // Размеры метки.
                                        iconImageSize: [87, 53],
                                        // Смещение левого верхнего угла иконки относительно
                                        // её "ножки" (точки привязки).
                                        //iconImageOffset: [-3, -42]
                                        iconImageOffset: [-43, -26]
                                    });

                                    clinicMap.geoObjects.add(myPlacemark);

                                    CustomControlClass = function (options) {
                                        CustomControlClass.superclass.constructor.call(this, options);
                                        this._$content = null;
                                        this._geocoderDeferred = null;
                                    };

                                    ymaps.util.augment(CustomControlClass, ymaps.collection.Item, {
                                        onAddToMap: function (map) {
                                            CustomControlClass.superclass.onAddToMap.call(this, map);
                                            this._lastCenter = null;
                                            this.getParent().getChildElement(this).then(this._onGetChildElement, this);
                                        },

                                        onRemoveFromMap: function (oldMap) {
                                            this._lastCenter = null;
                                            if (this._$content) {
                                                this._$content.remove();
                                                this._mapEventGroup.removeAll();
                                            }
                                            CustomControlClass.superclass.onRemoveFromMap.call(this, oldMap);
                                        },

                                        _onGetChildElement: function (parentDomContainer) {
                                            // Создаем HTML-элемент с текстом.
                                            this._$content = $('<div class="customMapControl"><?=$arResult["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"]?></div>').appendTo(parentDomContainer);
                                            this._mapEventGroup = this.getMap().events.group();
                                            // Запрашиваем данные после изменения положения карты.
                                            //this._mapEventGroup.add('boundschange', this._createRequest, this);
                                            // Сразу же запрашиваем название места.
                                            //this._createRequest();
                                        }
                                    });

                                    var customControl = new CustomControlClass();
                                    clinicMap.controls.add(customControl, {
                                        float: 'none',
                                        position: {
                                            top: 10,
                                            left: 10
                                        }
                                    });
                                };
                            </script>
                        </div>
                    <?endif;?>
                    <div class="block-holder">
                        <?if ($arResult["PROPERTIES"]["CONTACTS"]["VALUE"]["TYPE"] == "text"):?>
                            <?=$arResult["PROPERTIES"]["CONTACTS"]["VALUE"]["TEXT"]?>
                        <?else:?>
                            <?=$arResult["PROPERTIES"]["CONTACTS"]["~VALUE"]["TEXT"]?>
                        <?endif?>
                    </div>
                </div>
            <?endif;?>
            <?if ($currentTab === 'information'):?>
                <div class="section">
                    <?=html_entity_decode($arResult["PROPERTIES"]["INFORMATION"]["VALUE"]["TEXT"])?>
                </div>
            <?endif;?>
            <?if ($currentTab === 'tour'):?>
                <div class="section">
                    <?=html_entity_decode($arResult["PROPERTIES"]["TOUR"]["VALUE"]["TEXT"])?>
                </div>
            <?endif;?>
            <?if ($currentTab === 'oms'):?>
                <div class="section">
                    <?=html_entity_decode($arResult["PROPERTIES"]["OMS"]["VALUE"]["TEXT"])?>
                </div>
            <?endif;?>
            <?if($currentTab === 'leading'):?>
                <div class="section">
                    <?=html_entity_decode($arResult["PROPERTIES"]["LEADING"]["VALUE"]["TEXT"])?>
                </div>
            <?endif;?>
            <?if ($currentTab === 'pricelist'):?>
                <div class="section">
                    <span class="content-title">Выберите раздел</span>
                    <div class="ajax-price-list">
                        <?php
                            global $priceFilter;
                            $priceFilter = array(
                                "!PROPERTY_PRICE" => false,
                            );
                        ?>
                        <?$APPLICATION->IncludeComponent("melcosoft:price.list", "clinic-detail", Array(
                            "IBLOCK_TYPE" => "contentsite", // Òèï èíôîáëîêà
                            "IBLOCK_ID" => "31",    // Èíôîáëîê
                            "FILTER_NAME" => "priceFilter",   // Èìÿ ìàññèâà ñî çíà÷åíèÿìè ôèëüòðà äëÿ ôèëüòðàöèè ýëåìåíòîâ
                            "CLINIC_ID" => $arResult["ID"], // ID Êëèíèêè
                            "PAGE_ELEMENT_COUNT" => "50000",    // Êîëè÷åñòâî ýëåìåíòîâ íà ñòðàíèöå
                            "AJAX_MODE" => "N", // Âêëþ÷èòü ðåæèì AJAX
                            "AJAX_OPTION_JUMP" => "N",  // Âêëþ÷èòü ïðîêðóòêó ê íà÷àëó êîìïîíåíòà
                            "AJAX_OPTION_STYLE" => "Y", // Âêëþ÷èòü ïîäãðóçêó ñòèëåé
                            "AJAX_OPTION_HISTORY" => "N",   // Âêëþ÷èòü ýìóëÿöèþ íàâèãàöèè áðàóçåðà
                            "CACHE_TYPE" => "A",    // Òèï êåøèðîâàíèÿ
                            "CACHE_TIME" => "36000000", // Âðåìÿ êåøèðîâàíèÿ (ñåê.)
                            "CACHE_GROUPS" => "Y",  // Ó÷èòûâàòü ïðàâà äîñòóïà
                            "CACHE_FILTER" => "Y",  // Êåøèðîâàòü ïðè óñòàíîâëåííîì ôèëüòðå
                            "PAGER_TEMPLATE" => "mamadeti", // Øàáëîí ïîñòðàíè÷íîé íàâèãàöèè
                            "DISPLAY_TOP_PAGER" => "N", // Âûâîäèòü íàä ñïèñêîì
                            "DISPLAY_BOTTOM_PAGER" => "Y",  // Âûâîäèòü ïîä ñïèñêîì
                            "PAGER_TITLE" => "",    // Íàçâàíèå êàòåãîðèé
                            "PAGER_SHOW_ALWAYS" => "N", // Âûâîäèòü âñåãäà
                            "PAGER_DESC_NUMBERING" => "N",  // Èñïîëüçîâàòü îáðàòíóþ íàâèãàöèþ
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",   // Âðåìÿ êåøèðîâàíèÿ ñòðàíèö äëÿ îáðàòíîé íàâèãàöèè
                            "PAGER_SHOW_ALL" => "N",    // Ïîêàçûâàòü ññûëêó "Âñå"
                            "AJAX_OPTION_ADDITIONAL" => "", // Äîïîëíèòåëüíûé èäåíòèôèêàòîð
                            "DISABLE_PRICE_LIST_AJAX" => "Y",
                            "_REQUEST" => $_REQUEST,
                            ),
                            false
                        );?>
                        <a class="more clinic-price-expand-open" href="/price-list2/<?php echo $arResult['CODE']; ?>/price/">Посмотреть весь прайс</a>
                    </div>
                </div>
            <?endif?>
            <?if ($currentTab === 'programs'):?>
                <div class="section">
                    <?$APPLICATION->IncludeComponent("melcosoft:catalog", "program", array(
                        "IBLOCK_TYPE" => "contentsite",
                        "IBLOCK_ID" => "27",
                        "BRAND_IBLOCK_TYPE" => "content",
                        "BRAND_IBLOCK_ID" => "",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "BASKET_URL" => "/personal/basket.php",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "SEF_MODE" => "Y",
                        "SEF_FOLDER" => "/program/",
                        //"AJAX_MODE" => "N",
                        //"AJAX_OPTION_JUMP" => "N",
                        //"AJAX_OPTION_STYLE" => "Y",
                        //"AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "Y",
                        "CACHE_GROUPS" => "Y",
                        "SET_TITLE" => "Y",
                        "SET_STATUS_404" => "N",
                        "USE_ELEMENT_COUNTER" => "Y",
                        "USE_FILTER" => "Y",
                        "FILTER_NAME" => "arrProgramFilter",
                        "FILTER_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_PROPERTY_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "FILTER_PRICE_CODE" => array(
                        ),
                        "USE_REVIEW" => "N",
                        "USE_COMPARE" => "N",
                        "PRICE_CODE" => array(
                        ),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRICE_VAT_SHOW_VALUE" => "N",
                        "PRODUCT_PROPERTIES" => array(
                        ),
                        "USE_PRODUCT_QUANTITY" => "N",
                        "CONVERT_CURRENCY" => "N",
                        "SHOW_TOP_ELEMENTS" => "N",
                        "SECTION_COUNT_ELEMENTS" => "Y",
                        "SECTION_TOP_DEPTH" => "2",
                        "PAGE_ELEMENT_COUNT" => "30",
                        "LINE_ELEMENT_COUNT" => "3",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "LIST_PROPERTY_CODE" => array(
                            0 => "CLINIC",
                            1 => "DIRECTION",
                            2 => "",
                        ),
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "LIST_META_KEYWORDS" => "-",
                        "LIST_META_DESCRIPTION" => "-",
                        "LIST_BROWSER_TITLE" => "-",
                        "DETAIL_PROPERTY_CODE" => array(
                            0 => "",
                            1 => "URL",
                            2 => "",
                        ),
                        "DETAIL_META_KEYWORDS" => "-",
                        "DETAIL_META_DESCRIPTION" => "-",
                        "DETAIL_BROWSER_TITLE" => "-",
                        "LINK_IBLOCK_TYPE" => "",
                        "LINK_IBLOCK_ID" => "",
                        "LINK_PROPERTY_SID" => "",
                        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                        "USE_ALSO_BUY" => "N",
                        "USE_STORE" => "N",
                        "PAGER_TEMPLATE" => "mamadeti",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Товары",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "SEF_URL_TEMPLATES" => array(
                            "sections" => "",
                            "brand" => "",
                            "filter" => "",
                            "section" => "#SECTION_CODE#/",
                            "element" => "#ELEMENT_CODE#/",
                            "compare" => "",
                        )
                        ),
                        false
                    ); ?>
                </div>
            <?endif?>
            <?if($currentTab === 'meeting'):?>
                <div class="section">
                    <?=$arResult['PROPERTIES']['MEETING']['~VALUE']['TEXT'];?>
                </div>
            <?endif;?>
            <?if($currentTab === 'rules'):?>
                <div class="section">
                    <?=$arResult['PROPERTIES']['RULES']['~VALUE']['TEXT'];?>
                </div>
            <?endif;?>
            <?if($currentTab === 'corporate'):?>
                <div class="section">
                    <?=$arResult['PROPERTIES']['CORPORATE']['~VALUE']['TEXT'];?>
                </div>
            <?endif;?>
            <?if($currentTab === 'law'):?>
                <div class="section">
                    <?=$arResult['PROPERTIES']['LAW']['~VALUE']['TEXT'];?>
                </div>
            <?endif;?>
            <?if($currentTab === 'doctorofmonth'):?>
                <div class="section">
                    <?=$arResult['PROPERTIES']['DOCTOR_OF_MONTH']['~VALUE']['TEXT'];?>
                </div>
            <?endif;?>
            <?if($currentTab === 'birth'):?>
                <div class="section">
                    <?=$arResult['PROPERTIES']['BIRTH']['~VALUE']['TEXT'];?>
                </div>
            <?endif;?>
            <?if($currentTab === 'pregnancy'):?>
                <div class="section">
                    <?=$arResult['PROPERTIES']['PREGNANCY']['~VALUE']['TEXT'];?>
                </div>
            <?endif;?>
        </div>
    </div>
</div>
