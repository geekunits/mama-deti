<div class="b-bg_white b-clinic_card__wrap">
    <div class="b-clinic_card__padding"><h1 class="b-header_h1 align-center">Клинический Госпиталь «Мать и дитя» Уфа</h1></div>
    <div class="b-clinics_tabs__wrap">
        <div class="b-clinics_tab">
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link b-clinics_tab__link-active">Основное</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Услуги</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Врачи</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Акции</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Программы</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Отзывы</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Дополнительная информация...</a>
                    <div class="b-clinics_tab__sub">
                        <a href="" class="b-clinics_tab__link">Персональные данные</a>
                        <a href="" class="b-clinics_tab__link">Юридическая информация</a>
                        <a href="" class="b-clinics_tab__link">Лицензии</a>
                        <a href="" class="b-clinics_tab__link">Для корпоративных клиентов</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="b-clinic_card__padding clearfix">
        <div class="b-clinic_card__left">
            <div class="b-clinic_card__logo"><img src="/upload/resize_cache/iblock/eed/294_294_2/eed6cd83646d2cba3f79e4535cdc750a.jpg" alt=""></div>
            <div class="b-clinic_gallery__wrap">
                <a href="/upload/iblock/63a/63a3c7141b5dd0c08c003ceee258c5ef.JPG" rel="gallery-2475" class="fancybox b-clinic_gallery__item in-bl">
                    <img alt="" src="http://ufa.dev.mamadeti.ru/upload/iblock/de6/de60dadea751ec1292d5d892881f7321.jpg">
                </a><!--
             --><a href="/upload/iblock/5f3/5f320ea0a9b620172a5d0ea661931251.jpg" rel="gallery-2475" class="fancybox b-clinic_gallery__item in-bl">
                    <img alt="" src="/upload/iblock/5f3/5f320ea0a9b620172a5d0ea661931251.jpg">
                </a><!--
             --><a href="/upload/iblock/c17/c1736c95b1cc4c4384cb025b9e53d906.jpg" rel="gallery-2475" class="fancybox b-clinic_gallery__item in-bl">
                    <img alt="" src="/upload/iblock/c17/c1736c95b1cc4c4384cb025b9e53d906.jpg">
                </a><!--
             --><a href="/upload/iblock/a25/a25c3ab1b8b87352bb9693c4ca4bd08a.jpg" rel="gallery-2475" class="fancybox b-clinic_gallery__item in-bl">
                    <img alt="" src="/upload/iblock/a25/a25c3ab1b8b87352bb9693c4ca4bd08a.jpg">
                </a>
            </div>
        </div>
        <div class="b-clinic_card__right">
            <div class="b-filters_doctors clearfix">
                <div class="b-filters_doctors__left">
                    <div class="jq-selectbox__select__red b-clinic_program__select">
                        <select name="direction">
                            <option value="-1">Все направления</option>
                            <option value="4405">Check Up</option>
                            <option value="4402">Детские программы</option>
                            <option value="293221">Комплексные обследования</option>
                            <option value="4403">Подготовка и ведение беременности</option>
                            <option value="4406">Программы по поддержке грудного вскармливания</option>
                            <option value="4404">Программы семейного прикрепления</option>
                            <option value="4407">Роды</option>
                            <option value="4409">Спа и косметология</option>
                            <option value="4408">Физиотерапия в гинекологии</option>
                            <option value="4411">ЭКО</option>
                        </select>
                    </div>
                    <div class="b-tree_links__wrap">
                        <div class="b-tree_link__item">
                            <div class="b-tree_link">
                                <div class="b-tree_link__plus js-tree-click">
                                    <div class="js-tree-plus">+</div>
                                </div>
                                <a href="">Родильный дом</a>
                            </div>
                            <div class="b-tree_links__sub b-tree_links__sub-1">
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Женская консультация</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Родильный дом</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Родильный дом</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click">+</div><a href="">Консультативно-диагностический центр</a></div>
                            <div class="b-tree_links__sub b-tree_links__sub-1">
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Женская консультация</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Диагностические программы</a></div>
                                    <div class="b-tree_links__sub b-tree_links__sub-2">
                                        <div class="b-tree_link__item">
                                            <div class="b-tree_link"><a href="">Стимуляция овуляции</a></div>
                                        </div>
                                        <div class="b-tree_link__item">
                                            <div class="b-tree_link"><a href="">Внутриматочная инсеминация</a></div>
                                        </div>
                                        <div class="b-tree_link__item">
                                            <div class="b-tree_link"><a href="">Донорские программы и суррогатное материнство</a></div>
                                        </div>
                                        <div class="b-tree_link__item">
                                            <div class="b-tree_link"><a href="">Предимплантационная генетическая диагностика</a></div>
                                        </div>
                                        <div class="b-tree_link__item">
                                            <div class="b-tree_link"><a href="">Криоконсервация эмбрионов, яйцеклеток, спермы</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Отделение лечения бесплодия</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Отделение лучевой диагностики</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Отделение ультразвуковой диагностики</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Эндоскопическое отделение</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Центр детского здоровья</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Хирургический центр</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Скорая помощь</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Банк стволовых клеток</a></div>
                        </div>
                    </div>
                </div>
                <div class="b-filters_doctors__right">
                    <div class="b-dest_alph">
                        <div class="b-dest_alph__letter-wrap">
                            <a class="b-dest_alph__letter b-dest_alph__letter-active" href="#"><span class="b-dest_alph__letter___wrap">A</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Б</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Г</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Д</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Е</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Ж</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">З</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">И</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">К</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Л</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">М</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Н</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">П</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Р</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">С</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Т</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Ф</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Х</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Ш</span></a>
                            <a class="b-dest_alph__letter" href="#"><span class="b-dest_alph__letter___wrap">Я</span></a>
                        </div>
                        <div class="b-dest_alph__list-doctors">
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Абдуллина Эльза Альбертовна</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Акульшина Анастасия Викторовна</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Алымова Светлана Николаевна</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Анварова Алсу Ринатовна</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Анисимова Татьяна Валерьевна</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Архипов Вячеслав Владимирович</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Афанасьева Елена Камилевна</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Ахметов Артур Замирович</a>
                            </div>
                            <div class="b-dest_alph__list-doctors___item">
                                <a href="#">Ахметова Римма Раулевна</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="b-clinic_doctors b-clinic_margin-bottom">
                <h2 class="b-header_h2 align-center b-clinic_doctors__title">Врачи всех специальностей во всех отделениях на букву <span class="b-clinic_doctors-list__title-letter">A</span></h2>
                <div class="b-clinic_doctors-list">
                    <div class="b-clinic_doctors-list__item">
                        <div class="b-clinic_doctors__part-left">
                            <div class="b-doctor b-doctor_extra-big">
                                <a class="b-doctor_avatar" href="#">
                                    <img src="/upload/iblock/39a/39a54feaa5ee2b15052698272b238168_thumb_6625dca21be75e044dd3876e9e966d46.jpg" alt="">
                                </a>
                            </div>
                            <div class="b-clinic_doctors__buttons">
                                <a class="b-btn_white b-clinic_doctors__btn" href="#">Задать вопрос</a>
                                <a class="b-btn_white b-clinic_doctors__btn" href="#">Записаться на прием</a>
                            </div>
                        </div>
                        <div class="b-clinic_doctors__part-right">
                            <div class="b-clinic_doctors__description">
                                <div class="b-clinic_doctors__description-name">
                                    <a href="#">Абдуллина Эльза Альбертовна</a>
                                </div>
                                <div class="b-clinic_doctors__description-list">
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Специальности:</div>
                                        <div class="b-clinic_doctors__description-value">Терапевт</div>
                                    </div>
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Услуги:</div>
                                        <div class="b-clinic_doctors__description-value">
                                            <a href="#">Диспансеризация (программы CHECK UP)</a>
                                        </div>
                                    </div>
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Отделение:</div>
                                        <div class="b-clinic_doctors__description-value">Взрослый клинико-диагностический центр (ВКДЦ)</div>
                                    </div>
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Ведет прием в:</div>
                                        <div class="b-clinic_doctors__description-value">Госпиталь «Мать и дитя» Уфа</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-clinic_doctors-list__item">
                        <div class="b-clinic_doctors__part-left">
                            <div class="b-doctor b-doctor_extra-big">
                                <a class="b-doctor_avatar" href="#">
                                    <img src="/upload/iblock/39a/39a54feaa5ee2b15052698272b238168_thumb_6625dca21be75e044dd3876e9e966d46.jpg" alt="">
                                </a>
                            </div>
                            <div class="b-clinic_doctors__buttons">
                                <a class="b-btn_white b-clinic_doctors__btn" href="#">Задать вопрос</a>
                                <a class="b-btn_white b-clinic_doctors__btn" href="#">Записаться на прием</a>
                            </div>
                        </div>
                        <div class="b-clinic_doctors__part-right">
                            <div class="b-clinic_doctors__description">
                                <div class="b-clinic_doctors__description-name">
                                    <a href="#">Абдуллина Эльза Альбертовна</a>
                                </div>
                                <div class="b-clinic_doctors__description-list">
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Специальности:</div>
                                        <div class="b-clinic_doctors__description-value">
                                            <div>Неонатолог, педиатр</div>
                                            <br>
                                            <ul class="b-clinic_doctors__description-specialty">
                                                <li class="b-clinic_doctors__description-specialty___item i-common-pseudo">Высшая врачебная категория</li>
                                                <li class="b-clinic_doctors__description-specialty___item i-common-pseudo">Кандидат медицинских наук</li>
                                                <li class="b-clinic_doctors__description-specialty___item i-common-pseudo">Заместитель главного врача по педиатрической помощи</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Услуги:</div>
                                        <div class="b-clinic_doctors__description-value">
                                            <ul class="b-clinic_doctors__description-service">
                                                <li class="b-clinic_doctors__description-service___item">
                                                    <a href="#">Диспансеризация (программы CHECK UP)</a>
                                                </li>
                                                <li class="b-clinic_doctors__description-service___item">
                                                    <a href="#">Вакцинация</a>
                                                </li>
                                                <li class="b-clinic_doctors__description-service___item">
                                                    <a href="#">Интенсивная терапия для новорожденных</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Отделение:</div>
                                        <div class="b-clinic_doctors__description-value">Взрослый клинико-диагностический центр (ВКДЦ)</div>
                                    </div>
                                    <div class="b-clinic_doctors__description-list___item">
                                        <div class="b-clinic_doctors__description-title">Ведет прием в:</div>
                                        <div class="b-clinic_doctors__description-value">Госпиталь «Мать и дитя» Уфа</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="b-clinic_schedule__wrap">
                <div class="b-header_h2 align-center">Расписание</div>
                <div class="b-clinic_schedule">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>Пн</td>
                            <td rowspan="5" class="b-clinic_schedule__time">08:00 – 20:00</td>
                        </tr>
                        <tr>
                            <td>Вт</td>
                        </tr>
                        <tr>
                            <td>Ср</td>
                        </tr>
                        <tr>
                            <td>Чт</td>
                        </tr>
                        <tr>
                            <td>Пт</td>
                        </tr>
                        <tr class="b-clinic_schedule__holiday b-clinic_schedule__sunday">
                            <td>Сб</td>
                            <td class="b-clinic_schedule__time">08:00 – 20:00</td>
                        </tr>
                        <tr class="b-clinic_schedule__holiday">
                            <td>Вс</td>
                            <td class="b-clinic_schedule__time">08:00 – 20:00</td>
                        </tr>
                    </table>
                </div>
                <a href="" class="b-clinic_schedule__download">Скачать расписание врачей</a>
            </div>
            <div class="b-clinic_card__text">
                <div class="js-clinic-text-cut">
                    <p> Клиника «Мать и дитя» Уфа предоставляет полный комплекс услуг в области медицинской диагностики, консервативной и оперативной гинекологии, лечения бесплодия с помощью вспомогательных репродуктивных технологий. Акушерские услуги по ведению беременности, в том числе беременности после ЭКО, многоплодной беременности, беременности при сопутствующей патологии &ndash; также в компетенции центра.</p>
         
                    <p> Врачи центра &ndash; акушеры-гинекологи, репродуктологи, урологи, андрологи, генетики, эндокринологи, терапевты &ndash; ведущие специалисты в своих областях, обладают многолетним опытом работы и регулярно совершенствуют свои навыки в ведущих российских и международных медицинских центрах. </p>
                     
                    <p> Оснащение клиники позволяет проводить все виды лабораторных и функциональных исследований, существующих в медицинской диагностической практике. Мощнейшая технологическая база и высокий профессионализм врачей также дают нам возможность проводить уникальные малоинвазивные органосохраняющие гинекологические операции. </p>
                     
                    <p> Клиника «Мать и Дитя» Уфа - лауреат Национальной премии "Хрустальная пробирка". Высокая оценка нашей работы пациентами &ndash; высшая ценность для нас. Мы делаем все возможное, чтобы оправдывать доверие наших пациентов, восстанавливать и сохранять их здоровье, помогать становиться счастливыми родителями здоровых малышей, прекрасно себя чувствовать и быть уверенными в благополучном завтрашнем дне. Клиника «Мать и дитя» Уфа &ndash; здесь рождается будущее!</p>
                    <div class="js-clinic-text-cut-more align-center b-clinic_card__text-more">Читать дальше</div>
                </div>
            </div>
            <div class="b-clinic_address__wrap b-clinic_address__right">
                <div class="b-clinic_address__padding">
                    <div class="b-header_h2 align-center">Контакты и запись на прием</div>
                    <div class="b-clinic_address__txt">
                        <strong>Адрес:</strong><br>Уфа, Лесной проезд, д. 4
                    </div>
                    <div class="b-clinic_address__txt">
                        <strong>Телефон:</strong><br>(347) 216-03-03                                
                        <span data-clinic-id="5126" class="b-clinic_address__txt-call js-show-call-me">Перезвоните мне</span>
                    </div>
                    <div class="b-clinic_address__txt">
                        <strong>E-mail:</strong><br><a href="" class="b-clinic_address__mail">d.shaihutdinova@mcclinics.ru</a>                            
                    </div>
                    <div class="b-btn_white in-bl link_reception_clinic" data-clinic="428" data-name="Клиника «Мать и дитя» Уфа">Записаться на приём</div><div class="b-btn_white in-bl link_review" data-clinic="428" data-type="clinic">Оставить отзыв</div>
                    <script type="text/javascript">
                        ymaps.ready(function () {
                            var map;

                            map = new ymaps.Map ("clinic-map-card", {
                                center: [54.73867158405,56.020095239426],
                                zoom: 13,
                                controls: ["smallMapDefaultSet"]
                            });

                            map.geoObjects.add(new ymaps.Placemark([54.73867158405,56.020095239426], {
                                content: "\u0413\u043e\u0441\u043f\u0438\u0442\u0430\u043b\u044c \u00ab\u041c\u0430\u0442\u044c \u0438 \u0434\u0438\u0442\u044f\u00bb \u0423\u0444\u0430",
                                balloonContent: "<a href=\"\/clinics\/ufa\/hospital-mother-and-child-ufa\/\">\u0413\u043e\u0441\u043f\u0438\u0442\u0430\u043b\u044c \u00ab\u041c\u0430\u0442\u044c \u0438 \u0434\u0438\u0442\u044f\u00bb \u0423\u0444\u0430<\/a><br\/><span>(347) 216-03-03<\/span><br\/>\u0423\u0444\u0430, \u041b\u0435\u0441\u043d\u043e\u0439 \u043f\u0440\u043e\u0435\u0437\u0434, \u0434. 4"                    
                            }));

                        });
                    </script>
                    <div class="b-clinic_address__map" id="clinic-map-card"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="b-bg_white b-clinic_news__wrap b-clinic_new__withsocial b-clinic_card__padding">
    <div class="b-clinic_social__wrap b-clinic_social__fixed">
        <div class="b-clinic_social__padding">
            <div class="align-center b-header_h2">Присоединяйтесь к нам</div>
            <div class="b-clinic_social__tabs">
                <div class="b-clinic_social__tab b-clinic_social__tab-active in-bl" data-id="1">Facebook</div><div class="b-clinic_social__tab in-bl" data-id="2">Vkontakte</div><div class="b-clinic_social__tab in-bl" data-id="3">Одноклассники</div>
            </div>
            <div class="b-clinic_social__items">
                <div class="b-clinic_social__item b-clinic_social__item-active" data-id="1">
                    <script>
                        setTimeout(function () {
                            (function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.3";
                                fjs.parentNode.insertBefore(js, fjs);
                            })(document, "script", "facebook-jssdk");
                        }, 1);
                    </script>
                    <div class="fb-page" data-href="https://www.facebook.com/pages/%D0%9A%D0%BB%D0%B8%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9-%D0%B3%D0%BE%D1%81%D0%BF%D0%B8%D1%82%D0%B0%D0%BB%D1%8C-%D0%9C%D0%B0%D1%82%D1%8C-%D0%B8-%D0%94%D0%B8%D1%82%D1%8F-%D0%A3%D1%84%D0%B0/256117467905572" data-width="302" data-height="250" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
                </div>
                <div class="b-clinic_social__item" data-id="2">
                    <iframe src="http://vk.com/widget_community.php?gid=56606296&width=302&height=250" width="302" height="250" scrolling="no" frameborder="0"></iframe>
                </div>
                <div class="b-clinic_social__item" data-id="3">
                    <div id="ok_group_widget_clinic"></div>
                    <script>
                        !function (d, id, did, st) {
                            var js = d.createElement("script");
                            js.src = "http://connect.ok.ru/connect.js";
                            js.onload = js.onreadystatechange = function () {
                                if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                                    if (!this.executed) {
                                        this.executed = true;
                                        setTimeout(function () {
                                            OK.CONNECT.insertGroupWidget(id,did,st);
                                        }, 0);
                                    }
                                }}
                            d.documentElement.appendChild(js);
                        }(document,"ok_group_widget_clinic","52532580122798","{width:302,height:250}");
                    </script>
                </div>
            </div>
        </div>
    </div>
    <div class="align-center b-header_h2">Новости</div>
    <div data-id="1" class="b-news js-news">
        <div class="b-news_item clearfix">
            <a class="b-news_col" href="/news/open-day-on-25-july-/">
                <span class="b-news_pic">
                    <img alt="День открытых дверей 25 июля " src="/upload/resize_cache/iblock/7f8/325_155_2/7f812c6ab5f90caea2616a80d0e28cf2.jpg">
                </span>
                <span class="b-news_date">21.07.2015</span>
                <span class="b-news_name">День открытых дверей 25 июля </span>
                <span class="b-news_txt">Клинический госпиталь "Мать и Дитя" приглашает всех будущих мам и пап на День открытых дверей 25 июля в 12:00. </span>
            </a>
            <a class="b-news_col" href="/news/the-group-of-companies-mother-and-child-and-russian-national-research-medical-university-n-a-n-and-p/">
                <span class="b-news_pic">
                    <img alt="ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы." src="/upload/resize_cache/iblock/dfc/325_155_2/dfccdfb5b88d00387beba2ea930b8f6a.JPG">
                </span>
                <span class="b-news_date">13.07.2015</span>
                <span class="b-news_name">ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.</span>
                <span class="b-news_txt">«Мать и дитя» становится клинической базой ведущих в Российской Федерации кафедр.</span>
            </a>
            <a class="b-news_col" href="/news/july-9th-clinical-hospital-mother-and-child-was-visited-by-the-plenipotentiary-of-rf-president-in-vo/">
                <span class="b-news_pic">
                    <img alt="9 июля Клинический госпиталь «Мать и Дитя» посетил  полпред президента РФ в ПФО Михаил Бабич" src="/upload/resize_cache/iblock/3dc/325_155_2/3dc3ad274dc4b6f865ae5cc2abb24e06.jpg">
                </span>
                <span class="b-news_date">10.07.2015</span>
                <span class="b-news_name">9 июля Клинический госпиталь «Мать и Дитя» посетил  полпред президента РФ в ПФО Михаил Бабич</span>
                <span class="b-news_txt">9 июля Клинический госпиталь «Мать и Дитя» посетил полпред президента РФ в ПФО Михаил Бабич</span>
            </a>
        </div>
        <div class="b-news_item clearfix" style="display:none;">
            <a class="b-news_col" href="/news/high-attention-during-his-visit-to-novosibirsk-avicenna-was-visited-by-general-director-of-state-cor/">
                <span class="b-news_pic">
                    <img alt="Высокое внимание. Во время визита в Новосибирск «АВИЦЕННУ» посетил Генеральный директор Госкорпорации Ростех С.В. Чемезов" src="/upload/resize_cache/iblock/aa1/325_155_2/aa1dfd715351f1c52139ca235e5c4c62.JPG">
                </span>
                <span class="b-news_date">09.07.2015</span>
                <span class="b-news_name">Высокое внимание. Во время визита в Новосибирск «АВИЦЕННУ» посетил Генеральный директор Госкорпорации Ростех С.В. Чемезов</span>
                <span class="b-news_txt">Сергей Викторович Чемезов высоко оценил широкое применение и использование в госпитале возможностей хирургии одного дня</span>
            </a>
            <a class="b-news_col" href="/news/a-new-contract-for-the-delivery/">
                <span class="b-news_pic">
                    <img alt="Новый контракт на роды" src="/upload/resize_cache/iblock/515/325_155_2/51509adc246933b96922be9ac779e41b.jpg">
                </span>
                <span class="b-news_date">30.06.2015</span>
                <span class="b-news_name">Новый контракт на роды</span>
                <span class="b-news_txt">Клинический госпиталь «Мать и дитя» Уфа объявляет о запуске с 1 июля нового контракта на роды по цене 58 000 руб!</span>
            </a>
            <a class="b-news_col" href="/news/mark-kurtser-took-part-in-st-petersburg-international-economic-forum-2015/">
                <span class="b-news_pic">
                    <img alt="Марк Аркадьевич Курцер принял участие в Петербургском Международном Экономическом Форуме 2015" src="/upload/resize_cache/iblock/1a1/325_155_2/1a1120cd55270611d22e39134c525871.png">
                </span>
                <span class="b-news_date">23.06.2015</span>
                <span class="b-news_name">Марк Аркадьевич Курцер принял участие в Петербургском Международном Экономическом Форуме 2015</span>
                <span class="b-news_txt">Как работает частный бизнес и как работает Группа Компаний, в которой 23 лечебных учреждения?</span>
            </a>
        </div>
        <div class="align-center">
            <a class="b-link_more in-bl" href="/news/">Показать еще</a>
        </div>
    </div>
</div>

<div class="b-bg_white b-clinic_articles__wrap b-clinic_card__padding">
    <div class="align-center b-header_h2">Статьи</div>
    <div class="b-news">
        <div class="b-news_item clearfix">
            <div class="b-news_col">
                <a class="b-news_link" href="/news/open-day-on-25-july-/">
                    <span class="b-news_name">День открытых дверей 25 июля </span>
                    <span class="b-news_txt">Клинический госпиталь "Мать и Дитя" приглашает всех будущих мам и пап на День открытых дверей 25 июля в 12:00. </span>
                </a>
                <a href="" class="b-clinic_articles__author">
                    <span class="b-clinic_articles__author-pic in-bl"><img alt="" src="/upload/resize_cache/iblock/517/45_45_2/517dcc269a7c167bc036fd7a93a55df9.jpg"></span>
                    <span class="in-bl">Автор: <br><span class="b-clinic_articles__author-name">Коноплев Б. А.</span></span>
                </a>
            </div>
            <div class="b-news_col">
                <a class="b-news_link" href="/news/the-group-of-companies-mother-and-child-and-russian-national-research-medical-university-n-a-n-and-p/">
                    <span class="b-news_name">ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.</span>
                    <span class="b-news_txt">«Мать и дитя» становится клинической базой ведущих в Российской Федерации кафедр.</span>
                </a>
                <a href="" class="b-clinic_articles__author">
                    <span class="b-clinic_articles__author-pic in-bl b-doctor_nophoto__female"></span>
                    <span class="in-bl">Автор: <br><span class="b-clinic_articles__author-name">Коноплев Б. А.</span></span>
                </a>
            </div>
            <div class="b-news_col">
                <a class="b-news_link" href="/news/july-9th-clinical-hospital-mother-and-child-was-visited-by-the-plenipotentiary-of-rf-president-in-vo/">
                    <span class="b-news_name">9 июля Клинический госпиталь «Мать и Дитя» посетил  полпред президента РФ в ПФО Михаил Бабич</span>
                    <span class="b-news_txt">9 июля Клинический госпиталь «Мать и Дитя» посетил полпред президента РФ в ПФО Михаил Бабич</span>
                </a>
                <a href="" class="b-clinic_articles__author">
                    <span class="b-clinic_articles__author-pic in-bl b-doctor_nophoto__male"></span>
                    <span class="in-bl">Автор: <br><span class="b-clinic_articles__author-name">Коноплев Б. А.</span></span>
                </a>
            </div>
            <div class="b-news_col">
                <a class="b-news_link" href="/news/july-9th-clinical-hospital-mother-and-child-was-visited-by-the-plenipotentiary-of-rf-president-in-vo/">
                    <span class="b-news_name">9 июля Клинический госпиталь «Мать и Дитя» посетил  полпред президента РФ в ПФО Михаил Бабич</span>
                    <span class="b-news_txt">9 июля Клинический госпиталь «Мать и Дитя» посетил полпред президента РФ в ПФО Михаил Бабич</span>
                </a>
                <a href="" class="b-clinic_articles__author">
                    <span class="b-clinic_articles__author-pic in-bl"><img alt="" src="/upload/resize_cache/iblock/517/45_45_2/517dcc269a7c167bc036fd7a93a55df9.jpg"></span>
                    <span class="in-bl">Автор: <br><span class="b-clinic_articles__author-name">Коноплев Б. А.</span></span>
                </a>
            </div>
        </div>
        <div class="align-center">
            <a class="b-link_more in-bl" href="/news/">Показать еще</a>
        </div>
    </div>
</div>
<br>
<div class="b-bg_white b-clinic_card__wrap">
    <div class="b-clinic_card__padding"><h1 class="b-header_h1 align-center">Клинический Госпиталь «Мать и дитя» Уфа</h1></div>
    <div class="b-clinics_tabs__wrap">
        <div class="b-clinics_tab">
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link b-clinics_tab__link-active">Основное</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Услуги</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Врачи</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Акции</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Программы</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Отзывы</a>
                </div>
            </div>
            <div class="b-clinics_tab__cell">
                <div class="b-clinics_tab__box">
                    <a href="" class="b-clinics_tab__link">Дополнительная информация...</a>
                    <div class="b-clinics_tab__sub">
                        <a href="" class="b-clinics_tab__link">Персональные данные</a>
                        <a href="" class="b-clinics_tab__link">Юридическая информация</a>
                        <a href="" class="b-clinics_tab__link">Лицензии</a>
                        <a href="" class="b-clinics_tab__link">Для корпоративных клиентов</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="b-clinic_card__padding clearfix">
        <div class="b-clinic_card__left">
            <div class="b-clinic_card__logo"><img src="/upload/resize_cache/iblock/eed/294_294_2/eed6cd83646d2cba3f79e4535cdc750a.jpg" alt=""></div>
            <div class="b-clinic_address__wrap b-clinic_address__left">
                <div class="b-clinic_address__padding">
                    <div class="b-header_h2 align-center">Контакты и запись на прием</div>
                    <div class="b-clinic_address__txt">
                        <strong>Адрес:</strong><br>Уфа, Лесной проезд, д. 4
                    </div>
                    <div class="b-clinic_address__txt">
                        <strong>Телефон:</strong><br>(347) 216-03-03                                
                        <span data-clinic-id="5126" class="b-clinic_address__txt-call js-show-call-me">Перезвоните мне</span>
                    </div>
                    <div class="b-clinic_address__txt">
                        <strong>E-mail:</strong><br><a href="" class="b-clinic_address__mail">d.shaihutdinova@mcclinics.ru</a>                            
                    </div>
                    <div class="b-btn_white in-bl link_reception_clinic" data-clinic="428" data-name="Клиника «Мать и дитя» Уфа">Записаться на приём</div><div class="b-btn_white in-bl link_review" data-clinic="428" data-type="clinic">Оставить отзыв</div>
                </div>
            </div>
            <div class="b-clinic_social__wrap">
                <div class="b-clinic_social__padding">
                    <div class="align-center b-header_h2">Присоединяйтесь к нам</div>
                    <div class="b-clinic_social__tabs">
                        <div class="b-clinic_social__tab b-clinic_social__tab-active in-bl" data-id="1">Facebook</div><div class="b-clinic_social__tab in-bl" data-id="2">Vkontakte</div><div class="b-clinic_social__tab in-bl" data-id="3">Одноклассники</div>
                    </div>
                    <div class="b-clinic_social__items">
                        <div class="b-clinic_social__item b-clinic_social__item-active" data-id="1">
                            <script>
                                setTimeout(function () {
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s); js.id = id;
                                        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.3";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    })(document, "script", "facebook-jssdk");
                                }, 1);
                            </script>
                            <div class="fb-page" data-href="https://www.facebook.com/pages/%D0%9A%D0%BB%D0%B8%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9-%D0%B3%D0%BE%D1%81%D0%BF%D0%B8%D1%82%D0%B0%D0%BB%D1%8C-%D0%9C%D0%B0%D1%82%D1%8C-%D0%B8-%D0%94%D0%B8%D1%82%D1%8F-%D0%A3%D1%84%D0%B0/256117467905572" data-width="302" data-height="250" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
                        </div>
                        <div class="b-clinic_social__item" data-id="2">
                            <iframe src="http://vk.com/widget_community.php?gid=56606296&width=302&height=250" width="302" height="250" scrolling="no" frameborder="0"></iframe>
                        </div>
                        <div class="b-clinic_social__item" data-id="3">
                            <div id="ok_group_widget_clinic2"></div>
                            <script>
                                !function (d, id, did, st) {
                                    var js = d.createElement("script");
                                    js.src = "http://connect.ok.ru/connect.js";
                                    js.onload = js.onreadystatechange = function () {
                                        if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                                            if (!this.executed) {
                                                this.executed = true;
                                                setTimeout(function () {
                                                    OK.CONNECT.insertGroupWidget(id,did,st);
                                                }, 0);
                                            }
                                        }}
                                    d.documentElement.appendChild(js);
                                }(document,"ok_group_widget_clinic2","52532580122798","{width:302,height:250}");
                            </script>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="b-clinic_card__right">
            <div class="b-clinic_actions__wrap">
                <div class="b-columns_two b-news_item clearfix">
                    <div class="b-column">
                        <a href="/news/open-day-on-25-july-/" class="b-news_link">
                            <span class="b-news_pic">
                                <img src="/upload/resize_cache/iblock/7f8/325_155_2/7f812c6ab5f90caea2616a80d0e28cf2.jpg" alt="День открытых дверей 25 июля ">
                                <span class="b-clinic_action__days">Осталось <strong>48</strong> дней</span>
                            </span>
                            <span class="b-news_date">21.07.2015</span>
                            <span class="b-news_name">День открытых дверей 25 июля </span>
                            <span class="b-news_txt">Клинический госпиталь "Мать и Дитя" приглашает всех будущих мам и пап на День открытых дверей 25 июля в 12:00. </span>
                        </a>
                    </div>
                    <div class="b-column">
                        <a href="/news/the-group-of-companies-mother-and-child-and-russian-national-research-medical-university-n-a-n-and-p/" class="b-news_link">
                            <span class="b-news_pic">
                                <img src="/upload/resize_cache/iblock/dfc/325_155_2/dfccdfb5b88d00387beba2ea930b8f6a.JPG" alt="ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.">
                                <span class="b-clinic_action__days">Осталось <strong>48</strong> дней</span>
                            </span>
                            <span class="b-news_date">13.07.2015</span>
                            <span class="b-news_name">ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.</span>
                            <span class="b-news_txt">«Мать и дитя» становится клинической базой ведущих в Российской Федерации кафедр.</span>
                        </a>
                    </div>
                </div>
                <div class="b-columns_two b-news_item clearfix">
                    <div class="b-column">
                        <a href="/news/open-day-on-25-july-/" class="b-news_link">
                            <span class="b-news_pic">
                                <img src="/upload/resize_cache/iblock/7f8/325_155_2/7f812c6ab5f90caea2616a80d0e28cf2.jpg" alt="День открытых дверей 25 июля ">
                                <span class="b-clinic_action__days">Осталось <strong>48</strong> дней</span>
                            </span>
                            <span class="b-news_date">21.07.2015</span>
                            <span class="b-news_name">День открытых дверей 25 июля </span>
                            <span class="b-news_txt">Клинический госпиталь "Мать и Дитя" приглашает всех будущих мам и пап на День открытых дверей 25 июля в 12:00. </span>
                        </a>
                    </div>
                    <div class="b-column">
                        <a href="/news/the-group-of-companies-mother-and-child-and-russian-national-research-medical-university-n-a-n-and-p/" class="b-news_link">
                            <span class="b-news_pic">
                                <img src="/upload/resize_cache/iblock/dfc/325_155_2/dfccdfb5b88d00387beba2ea930b8f6a.JPG" alt="ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.">
                                <span class="b-clinic_action__days">Осталось <strong>48</strong> дней</span>
                            </span>
                            <span class="b-news_date">13.07.2015</span>
                            <span class="b-news_name">ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.</span>
                            <span class="b-news_txt">«Мать и дитя» становится клинической базой ведущих в Российской Федерации кафедр.</span>
                        </a>
                    </div>
                </div>
                <div class="b-columns_two b-news_item clearfix">
                    <div class="b-column">
                        <a href="/news/open-day-on-25-july-/" class="b-news_link">
                            <span class="b-news_pic">
                                <img src="/upload/resize_cache/iblock/7f8/325_155_2/7f812c6ab5f90caea2616a80d0e28cf2.jpg" alt="День открытых дверей 25 июля ">
                                <span class="b-clinic_action__days">Осталось <strong>48</strong> дней</span>
                            </span>
                            <span class="b-news_date">21.07.2015</span>
                            <span class="b-news_name">День открытых дверей 25 июля </span>
                            <span class="b-news_txt">Клинический госпиталь "Мать и Дитя" приглашает всех будущих мам и пап на День открытых дверей 25 июля в 12:00. </span>
                        </a>
                    </div>
                    <div class="b-column">
                        <a href="/news/the-group-of-companies-mother-and-child-and-russian-national-research-medical-university-n-a-n-and-p/" class="b-news_link">
                            <span class="b-news_pic">
                                <img src="/upload/resize_cache/iblock/dfc/325_155_2/dfccdfb5b88d00387beba2ea930b8f6a.JPG" alt="ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.">
                            </span>
                            <span class="b-news_date">13.07.2015</span>
                            <span class="b-news_name">ГК «Мать и дитя» и РНИМУ им. Н.И. Пирогова объединили свои ресурсы.</span>
                            <span class="b-news_txt">«Мать и дитя» становится клинической базой ведущих в Российской Федерации кафедр.</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="b-tree_links__wrap">
                <div class="b-tree_link__item">
                    <div class="b-tree_link">
                        <div class="b-tree_link__plus js-tree-click">
                            <div class="js-tree-plus">+</div>
                        </div>
                        <a href="">Родильный дом</a>
                    </div>
                    <div class="b-tree_links__sub b-tree_links__sub-1">
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Женская консультация</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Родильный дом</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Родильный дом</a></div>
                        </div>
                    </div>
                </div>
                <div class="b-tree_link__item">
                    <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click">+</div><a href="">Консультативно-диагностический центр</a></div>
                    <div class="b-tree_links__sub b-tree_links__sub-1">
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Женская консультация</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Диагностические программы</a></div>
                            <div class="b-tree_links__sub b-tree_links__sub-2">
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Стимуляция овуляции</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Внутриматочная инсеминация</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Донорские программы и суррогатное материнство</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Предимплантационная генетическая диагностика</a></div>
                                </div>
                                <div class="b-tree_link__item">
                                    <div class="b-tree_link"><a href="">Криоконсервация эмбрионов, яйцеклеток, спермы</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Отделение лечения бесплодия</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Отделение лучевой диагностики</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Отделение ультразвуковой диагностики</a></div>
                        </div>
                        <div class="b-tree_link__item">
                            <div class="b-tree_link"><a href="">Эндоскопическое отделение</a></div>
                        </div>
                    </div>
                </div>
                <div class="b-tree_link__item">
                    <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Центр детского здоровья</a></div>
                </div>
                <div class="b-tree_link__item">
                    <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Хирургический центр</a></div>
                </div>
                <div class="b-tree_link__item">
                    <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Скорая помощь</a></div>
                </div>
                <div class="b-tree_link__item">
                    <div class="b-tree_link"><div class="b-tree_link__plus js-tree-click"><div class="js-tree-plus">+</div></div><a href="">Банк стволовых клеток</a></div>
                </div>
            </div>

            <br>
            <br>

            <div class="b-clinic_program__wrap">
                <div class="jq-selectbox__select__red b-clinic_program__select">
                    <select name="direction">
                        <option value="-1">Все направления</option>
                        <option value="4405">Check Up</option>
                        <option value="4402">Детские программы</option>
                        <option value="293221">Комплексные обследования</option>
                        <option value="4403">Подготовка и ведение беременности</option>
                        <option value="4406">Программы по поддержке грудного вскармливания</option>
                        <option value="4404">Программы семейного прикрепления</option>
                        <option value="4407">Роды</option>
                        <option value="4409">Спа и косметология</option>
                        <option value="4408">Физиотерапия в гинекологии</option>
                        <option value="4411">ЭКО</option>
                    </select>
                </div>
                <div class="b-clinic_program b-columns_two">
                    <div class="b-header_h2">Детские программы</div>
                    <div class="b-clinic_program__item clearfix">
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для мужчин
                            <span class="b-clinic_program__price">43 407 р.</span>
                        </a>
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для женщин
                            <span class="b-clinic_program__price">44 674 р.</span>
                        </a>
                    </div>
                    <div class="b-clinic_program__item clearfix">
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для мужчин
                            <span class="b-clinic_program__price">43 407 р.</span>
                        </a>
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для женщин
                            <span class="b-clinic_program__price">44 674 р.</span>
                        </a>
                    </div>
                    <div class="b-clinic_program__item clearfix">
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для мужчин
                            <span class="b-clinic_program__price">43 407 р.</span>
                        </a>
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для женщин
                            <span class="b-clinic_program__price">44 674 р.</span>
                        </a>
                    </div>
                </div>
                <div class="b-clinic_program b-columns_two">
                    <div class="b-header_h2">Детские программы</div>
                    <div class="b-clinic_program__item clearfix">
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для мужчин
                            <span class="b-clinic_program__price">43 407 р.</span>
                        </a>
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для женщин
                            <span class="b-clinic_program__price">44 674 р.</span>
                        </a>
                    </div>
                    <div class="b-clinic_program__item clearfix">
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для мужчин
                            <span class="b-clinic_program__price">43 407 р.</span>
                        </a>
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для женщин
                            <span class="b-clinic_program__price">44 674 р.</span>
                        </a>
                    </div>
                    <div class="b-clinic_program__item clearfix">
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для мужчин
                            <span class="b-clinic_program__price">43 407 р.</span>
                        </a>
                        <a href="" class="b-clinic_program__link b-column">
                            Комплексное обследование в стационаре для женщин
                            <span class="b-clinic_program__price">44 674 р.</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="b-clinic_license__wrap b-slider_content b-slider__arrow__center">
                <ul class="js-slider-carousel b-slider_no" data-countrow="3">
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/aac/aacfa5b9dbe1b8e09acde928425ac6d3.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/aac/aacfa5b9dbe1b8e09acde928425ac6d3_thumb_9fe2b2e3a65b40ad662c8cb996edc942.JPG">
                        </a>
                    </li>
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/d3f/d3f0e99c4f4c86ef57d89908f08ff40b.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/d3f/d3f0e99c4f4c86ef57d89908f08ff40b_thumb_8c324750d693b3f76ae0a09651d9e4bd.JPG">
                        </a>
                    </li>
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/fc2/fc28656c3fe5d741f660ea40bad18903.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/fc2/fc28656c3fe5d741f660ea40bad18903_thumb_2e086322be8a4e54fc38d5302aefa6dd.JPG">
                        </a>
                    </li>
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/540/5406645993f62542225cd8c48aff266d.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/540/5406645993f62542225cd8c48aff266d_thumb_a5c8d56b74d0ae8c745c39bc738b033b.JPG">
                        </a>
                    </li>
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/41e/41efd32451f29e59c7dbe2f825d7ec56.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/41e/41efd32451f29e59c7dbe2f825d7ec56_thumb_b4a7e87687c513fcab15c202acd76d90.JPG">
                        </a>
                    </li>
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/fac/facbab7b206bc382674a427a90e162f0.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/fac/facbab7b206bc382674a427a90e162f0_thumb_7dfa2c62648b0bb189f49c70be0585a8.JPG">
                        </a>
                    </li>
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/94d/94d3638f78dfd60d51bd062072c65703.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/94d/94d3638f78dfd60d51bd062072c65703_thumb_7953245a89c051f9189b739f58f30080.JPG">
                        </a>
                    </li>
                    <li class="b-clinic_license__item">
                        <a href="/upload/iblock/a25/a25e0169f9f15744e6e1753f1a62792f.JPG" rel="group1" class="fancybox">
                            <img alt="" src="/upload/iblock/a25/a25e0169f9f15744e6e1753f1a62792f_thumb_002316f3dd07364e3eb1ac64e7aee780.JPG">
                        </a>
                    </li>
                </ul>
            </div>

            <br>
            <br>


            <div class="b-tree_links__wrap b-tree_price__wrap">
                <div class="b-tree_link__item">
                    <div class="b-tree_link js-tree-click">
                        <div class="b-tree_link__plus">
                            <div class="js-tree-plus">+</div>
                        </div>
                        ВВЕДЕНИЕ ПРЕПАРАТОВ, ОПЛАЧИВАЕМЫХ ДОПОЛНИТЕЛЬНО
                    </div>
                    <div class="b-tree_links__sub">
                        <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Верхняя блефаропластика</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Нижняя жиросохраняющая блефаропластика</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Частичная или полная реконструкция ушной раковины</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Эндопротезирование подбородка силиконовыми имплантантами</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="b-tree_link__item">
                    <div class="b-tree_link js-tree-click">
                        <div class="b-tree_link__plus">
                            <div class="js-tree-plus">+</div>
                        </div>
                        КОНСУЛЬТАЦИИ СПЕЦИАЛИСТОВ (ВЗРОСЛЫЕ)
                    </div>
                    <div class="b-tree_links__sub">
                        <strong>МСКТ (МУЛЬТИСПИРАЛЬНАЯ КОМПЬЮТЕРНАЯ ТОМОГРАФИЯ)</strong>
                        <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Верхняя блефаропластика</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Нижняя жиросохраняющая блефаропластика</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Частичная или полная реконструкция ушной раковины</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="b-price_dot">
                                    <span class="b-price_bg">Эндопротезирование подбородка силиконовыми имплантантами</span>
                                </td>
                                <td class="b-price_dot b-price_cost">
                                    <span class="b-price_bg">от 20 000руб.</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>