<div class="b-service_doctors">
    <ul class="b-service_doctors-items js-slider-carousel b-slider_no" data-countrow="4">
        <?php foreach ($arResult['ITEMS'] as $arItem): ?>
            <?php
                $image = null;
                if ($arItem['PREVIEW_PICTURE']) {
                    $image = MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z" => 1, "w" => 129, "h" => 129));
                }



                $specialities = $arItem['DISPLAY_PROPERTIES']['SPECIALTY']['DISPLAY_VALUE'];
                if (!is_array($specialities)) {
                    $specialities = [$specialities];
                }
                $specialities = implode(', ', $specialities);
                $specialities = strip_tags($specialities);

                $isFemale = $arItem['DISPLAY_PROPERTIES']['SEX']['DISPLAY_VALUE'] === 'Ж';
            ?>

            <li class="b-service_doctors-item">
                <div class="b-doctor_big">
                    <a class="b-doctor_avatar<?php if (!$image): ?> b-doctor_nophoto__female <?php echo $isFemale ? '' : ' b-doctor_nophoto__male'; ?><?php endif; ?>" href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>">
                        <?php if ($image): ?><img src="<?php echo $image; ?>" alt="<?php echo htmlspecialchars($arItem['NAME']); ?>"><?php endif; ?>
                    </a>
                </div>
                <div class="b-service_doctors-head">
                    <div class="b-service_doctors-name"><a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"><?php echo shortenName($arItem['NAME']); ?></a></div>
                    <?if ($arItem["DISPLAY_PROPERTIES"]["POST_FULL"]["VALUE"]):?>
                        <div class="b-service_doctors-special">
                            <?=$arItem["PROPERTIES"]["POST_FULL"]["VALUE"]?>
                        </div>
                    <?endif?>
                    <div class="b-service_doctors-special"><?php echo $specialities; ?></div>
                    <?
                    //echo"POST = <pre>";print_r( $arItem['DISPLAY_PROPERTIES']['POST']['VALUE']);echo"</pre>";
                    ?>
                    <?if ($arItem['DISPLAY_PROPERTIES']['POST']['VALUE'][0]):?>
                        <?foreach ($arItem['DISPLAY_PROPERTIES']['POST']['VALUE'] as $key => $val) {?>
                            <div class="b-service_doctors-post">
                                - <?=$val?>
                            </div>
                        <?}?>
                    <?endif?>
                </div>
                <?/*
                <div class="b-service_doctors-intro">
                    <?php if ($arItem['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE']): ?>
                        <div class="b-service_doctors-category"><?php echo $arItem['DISPLAY_PROPERTIES']['CATEGORY']['DISPLAY_VALUE']; ?></div>
                    <?php endif; ?>
                    <?php if ($arItem['DISPLAY_PROPERTIES']['POST_FULL']['DISPLAY_VALUE']): ?>
                        <div class="b-service_doctors-post"><?php echo $arItem['DISPLAY_PROPERTIES']['POST_FULL']['DISPLAY_VALUE']; ?></div>
                    <?php endif; ?>
                </div>
                */?>
                <div class="b-service_doctors-activities">
                    <?php if ($arItem['REVIEW_COUNT']): ?>
                        <div class="b-service_doctors-review"><a href="/reviews/?region=<?php echo reset($arItem['PROPERTIES']['CITY']['VALUE']); ?>&clinic=<?php echo reset($arItem['PROPERTIES']['CLINIC']['VALUE']); ?>&doctor=<?php echo $arItem['ID']; ?>"><?php echo $arItem['REVIEW_COUNT']; ?> <?php echo plural($arItem['REVIEW_COUNT'], ['отзыв', 'отзыва', 'отзывов']); ?></a></div>
                    <?php endif; ?>
                    <?php if ($arItem['QA_COUNT']): ?>
                        <div class="b-service_doctors-reply"><a href="/qa/?doctor=<?php echo $arItem['ID']; ?>"><?php echo $arItem['QA_COUNT']; ?> <?php echo plural($arItem['QA_COUNT'], ['ответ', 'ответа', 'ответов']); ?></a></div>
                    <?php endif; ?>
                </div>
                <a class="b-service_doctors-btn b-btn_white in-bl link_ask_direction" href="#" data-service="<?php echo $arParams['SERVICE']['ID']; ?>" data-doctor="<?php echo $arItem['ID']; ?>">Задать вопрос</a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
