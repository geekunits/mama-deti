<div class="b-bg_white b-service_padding b-service_menu b-columns_three b-service_margin__bottom">
    <div class="b-service_menu__items clearfix">
        <div class="b-column">
            <a href="/doctors/<?php echo $arResult['CLINIC'] ? '?clinic='.$arResult['CLINIC']['ID'] : ''; ?>">Врачи</a>
        </div>
        <div class="b-column">
            <a href="/article/">Статьи</a>
        </div>
        <div class="b-column">
            <a href="/program/<?php echo $arResult['CLINIC'] ? '?clinic='.$arResult['CLINIC']['ID'] : ''; ?>">Программы</a>
        </div>
    </div>
    <div class="b-service_menu__items clearfix">
        <div class="b-column">
            <a href="/qa/<?php echo $arResult['CLINIC'] ? '?clinic='.$arResult['CLINIC']['ID'] : ''; ?>">Ответы на вопросы</a>
        </div>
        <div class="b-column">
            <a href="/news/">Новости</a>
        </div>
    </div>
</div>
