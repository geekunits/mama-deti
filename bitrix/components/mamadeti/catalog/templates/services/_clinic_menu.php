<?php

$formatter = new ClinicResultFormatter($arResult['CLINIC']);
$clinicMenu = new ClinicMenu($formatter->format(), [
	'useAjax' => false,
	'mainTabsCount' => 4,
	'detectCurrentTab' => false,
	'currentTabName' => 'services',
]);
?>
<?php /*$clinicMenu->renderTitle() . */$clinicMenu->render(); ?>
