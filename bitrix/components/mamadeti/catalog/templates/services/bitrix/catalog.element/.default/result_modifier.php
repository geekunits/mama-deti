<?php
$arResult['CLINIC_IDS'] = array();
$arResult["CLINICS"] = array();
$arResult["DOCTORS"] = array();
$arResult["ARTICLES"] = array();
$arResult["NEWS"] = array();
$arResult['PROGRAMS'] = array();
$arResult['SERVICE_IDS'] = array();

$CITY_ID = GetCurrentCity();

$arRegionClinic = CMamaDetiAPI::getClinicsID(array("PROPERTY_CITY" => $CITY_ID, "PROPERTY_SERVICES" => $arResult["ID"]));

if ($arRegionClinic) {
    $rsElement = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => 2, "ID" => $arRegionClinic));
    while ($obElement = $rsElement->GetNextElement()) {
        $arItem = $obElement->GetFields();
        $arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
        $arItem["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
        if (!is_array($arItem["PREVIEW_PICTURE"])) {
            $arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
        }
        $arItem["PROPERTIES"] = $obElement->GetProperties();

        $arResult["CLINIC_IDS"][] = $arItem['ID'];
        $arResult["CLINICS"][] = $arItem;
    }

    $rsDoctors = CIBlockElement::GetList(
        array("NAME" => "ASC"),
        array(
            'ACTIVE' => 'Y',
            "IBLOCK_ID" => 3,
            "PROPERTY_SERVICES" => $arResult["ID"],
            "PROPERTY_CLINIC" => $arRegionClinic,
        )
    );
    while ($obElement = $rsDoctors->GetNextElement()) {
        $arItem = $obElement->GetFields();
        $arItem["PREVIEW_PICTURE"] = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
        $arItem["DETAIL_PICTURE"] = CFile::GetFileArray($arItem["DETAIL_PICTURE"]);
        if (!is_array($arItem["PREVIEW_PICTURE"])) {
            $arItem["PREVIEW_PICTURE"] = $arItem["DETAIL_PICTURE"];
        }
        $arItem["PROPERTIES"] = $obElement->GetProperties();

        $arItem["CLINIC"] = '';
        $arFilter = array("IBLOCK_ID" => 2, "ACTIVE" => "Y", "ID" => $arItem["PROPERTIES"]["CLINIC"]["VALUE"]);
        $res = CIBlockElement::GetList(array("SORT" => "asc", "NAME" => "asc"), $arFilter, false, false, array("NAME", "DETAIL_PAGE_URL"));
        while ($arres = $res->GetNext()) {
            $arItem["CLINIC"][] = '<a class="red-color" href="'.$arres["DETAIL_PAGE_URL"].'">'.$arres["NAME"].'</a>';
        }

        if (is_array($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"])) {
            $arFilter = array(
                "IBLOCK_ID" => $arItem["PROPERTIES"]["SPECIALTY"]["LINK_IBLOCK_ID"],
                "ACTIVE" => "Y",
                "ID" => array_values($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]),
                );
            $arItem["PROPERTIES"]["SPECIALTY"]["VALUE"] = array();
            $rsElement = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, array("IBLOCK_ID", "ID", "NAME", "DETAIL_PAGE_URL"));
            while ($arElement = $rsElement->GetNext()) {
                $arItem["PROPERTIES"]["SPECIALTY"]["VALUE"][] = $arElement;
            }
        }

        if (is_array($arItem["PROPERTIES"]["SECTION"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SECTION"]["VALUE"])) {
            $arFilter = array(
                "IBLOCK_ID" => $arItem["PROPERTIES"]["SECTION"]["LINK_IBLOCK_ID"],
                "ACTIVE" => "Y",
                "ID" => array_values($arItem["PROPERTIES"]["SECTION"]["VALUE"]),
                );
            $arItem["PROPERTIES"]["SECTION"]["VALUE"] = array();
            $rsElement = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, array("IBLOCK_ID", "ID", "NAME", "DETAIL_PAGE_URL"));
            while ($arElement = $rsElement->GetNext()) {
                $arItem["PROPERTIES"]["SECTION"]["VALUE"][] = $arElement;
            }
        }

        $arResult["DOCTORS"][] = $arItem;
    }
}

$rsElement = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => 8, "PROPERTY_SERVICES" => $arResult["ID"], 'PROPERTY_CLINIC' => $arResult['CLINIC_IDS']));
while ($arElement = $rsElement->GetNext()) {
    $arResult["NEWS"][] = $arElement;
}

/*if (is_array($arResult["PROPERTIES"]["ARTICLE"]["VALUE"]) && !empty($arResult["PROPERTIES"]["ARTICLE"]["VALUE"])) {
    $rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),array("IBLOCK_ID"=>9,"PROPERTY_SERVICES"=>$arResult["ID"]),false,false,array('IBLOCK_ID', 'ID', 'NAME', 'DETAIL_PAGE_URL','PREVIEW_TEXT','PROPERTY_SERVICES'));
    while ($arItem = $rsElement->GetNext())
        $arResult["ARTICLES"][] = $arItem;
}*/

$rsElement = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => 9, "PROPERTY_SERVICES" => $arResult["ID"]));
    while ($arItem = $rsElement->GetNext()) {
        $arResult["ARTICLES"][] = $arItem;
    }

if ($arResult['CLINIC_IDS']) {
    $serviceIds = array($arResult['ID']);
    $rsSection = CIBlockSection::GetList(
        array(),
        array(
            'IBLOCK_ID' => 1,
            'SECTION_ID' => $arResult['ID'],
        )
    );
    while ($arSection = $rsSection->GetNext()) {
        $serviceIds[] = $arSection['ID'];
    }
    $rsElement = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array(
            "IBLOCK_ID" => 27,
            "PROPERTY_SERVICES" => $serviceIds,
            "PROPERTY_CLINIC" => $arResult["CLINIC_IDS"],
        ),
        false,
        false,
        array(
            'IBLOCK_ID',
            'ID',
            'PROPERTY_SERVICES',
            'PROPERTY_CLINIC',
        )
    );
    $arResult['SERVICE_IDS'] = $serviceIds;
    global $arrProgramFilter;
    $arrProgramFilter['ID'] = array();
    while ($arElement = $rsElement->GetNext()) {
        $arResult["PROGRAMS"][] = $arElement;
        $arrProgramFilter['ID'][] = $arElement['ID'];
    }
}

$this->__component->SetResultCacheKeys(array(
    "TIMESTAMP_X"
));
