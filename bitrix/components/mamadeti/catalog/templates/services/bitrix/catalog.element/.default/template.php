<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
ob_start();
if ($arResult['SERVICE_IDS'] && $arResult['CLINIC_IDS']) {
    global $arrActionsFilter;
    $arrActionsFilter = array('PROPERTY_SERVICES' => $arResult['SERVICE_IDS'], 'PROPERTY_CLINIC' => $arResult['CLINIC_IDS']);
    $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "actions",
        array(
            "IBLOCK_TYPE"   =>      'contentsite',
            "IBLOCK_ID"     =>      32,
            "NEWS_COUNT"    =>      12,
            'FILTER_NAME'    =>        'arrActionsFilter',
            "SORT_BY1"      =>      'ACTIVE_FROM',
            "SORT_ORDER1"   =>      'DESC',
            "FIELD_CODE"    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
            "PROPERTY_CODE" =>      array('SERVICES', 'CLINIC', 'DATE_FROM', 'DATE_TO'),
            "SET_TITLE"     =>      'N',
            "SET_STATUS_404" => 'N',
            "INCLUDE_IBLOCK_INTO_CHAIN"     =>      'N',
            "DISPLAY_BOTTOM_PAGER"  =>      'Y',

            "DISPLAY_DATE"  =>      'N',
            "DISPLAY_NAME"  =>      "Y",
            "DISPLAY_PICTURE"       =>      'Y',
            "DISPLAY_PREVIEW_TEXT"  =>      'Y',
            //'CHECK_DATES' => 'Y',
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "mamadeti",
        ),
        $component
    );
}
$actionsBlock = ob_get_clean();

ob_start();
$clinicsIds = array_unique(CMamaDetiAPI::getClinicsID(array('PROPERTY_CITY' => GetCurrentCity())));
global $arrMemoFilter;
$arrMemoFilter = array('PROPERTY_CLINICS' => $clinicsIds, 'PROPERTY_SERVICE' => $arResult['ID']);
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "service-memo",
    array(
        "IBLOCK_TYPE"                    =>      'contentsite',
        "IBLOCK_ID"                      =>      33,
        "NEWS_COUNT"                     =>      12,
        'FILTER_NAME'                    =>        'arrMemoFilter',
        "SORT_BY1"                       =>      'ACTIVE_FROM',
        "SORT_ORDER1"                    =>      'DESC',
        "FIELD_CODE"                    =>      array('ID', 'NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'DETAIL_PAGE_URL', 'ACTIVE_FROM', 'ACTIVE_TO'),
        "PROPERTY_CODE"                =>      array('CITY', 'CLINIC'),
        "SET_TITLE"                      =>      'N',
        "SET_STATUS_404"                =>        'N',
        "DETAIL_URL"                     =>        '/memo/#ELEMENT_CODE#/',
        "INCLUDE_IBLOCK_INTO_CHAININ"    =>      'N',
        "DISPLAY_BOTTOM_PAGER"        =>      'Y',
        "DISPLAY_DATE"                =>      'N',
        "DISPLAY_NAME"                =>      "Y",
        "DISPLAY_PICTURE"               =>      'Y',
        "DISPLAY_PREVIEW_TEXT"        =>      'Y',
        "INCLUDE_IBLOCK_INTO_CHAIN"    =>        "N",
        "ADD_SECTIONS_CHAIN"            =>        "N",
        "ADD_ELEMENT_CHAIN"            =>        "N",
        //'CHECK_DATES' => 'Y',
        "PAGER_SHOW_ALWAYS"            =>        "N",
        "PAGER_TEMPLATE"                =>        "mamadeti",
    ),
    $component
);
$memoBlock = ob_get_clean();

if ($arParams['ADD_SECTIONS_CHAIN'] && !empty($arResult['NAME'])) {
    $arResult['SECTION']['PATH'][] = array(
   'NAME' => $arResult['NAME'],
   'PATH' => ' ', );

    $component = $this->__component;
    $component->arResult = $arResult;
}

$bTabActions = mb_strlen($actionsBlock)>10;
$bTabClinics = !empty($arResult["CLINICS"]);
$bTabDoctors = !empty($arResult["DOCTORS"]);
$bTabArticles = !empty($arResult["ARTICLES"]);
$bTabNews = !empty($arResult["NEWS"]);
$bTabPrograms = !empty($arResult["PROGRAMS"]);
$bTabMemo = strpos($memoBlock, 'href=') !== false;
$arTabs = array(
    "ОПИСАНИЕ",
    );

if ($bTabClinics) {
    $arTabs[] = "Клиники";
}
if ($bTabDoctors) {
    $arTabs[] = "Врачи";
}
if ($bTabArticles) {
    $arTabs[] = "Статьи";
}
if ($bTabNews) {
    $arTabs[] = "Новости";
}
if ($bTabPrograms) {
    $arTabs[] = "Программы";
}
if ($bTabMemo) {
    $arTabs[] = 'Памятка пациентам';
}
if ($bTabActions) {
    $arTabs[] = array("Акции", ' tabcontrols_a_action button01');
}
?>

<section class="content-col">
    <h1 class="name-title"><?=$arResult["NAME"]?></h1>

    <div class="tabcontrols-holder fixed-holder">
        <ul class="tabcontrols">
        <?foreach ($arTabs as $num=>$tab):$bLast = ++$num == count($arTabs); if (is_array($tab)) {list($tab, $class) = $tab; } else { $class = ''; } ?>
            <li class="tabcontrols_li<?if ($num==1):?> active<?endif?>">
            <?if (!$bLast):?>
                <a class="<?if ($num==1):?>tabcontrols_a_f<?endif?><?=$class;?>" href="#"><?=$tab?></a>
            <?else:?>
                <a class="tabcontrols_a_last<?=$class;?>" href="#"><?=$tab?></a>
            <?endif?>
            </li>
        <?endforeach?>
        </ul>
    </div>
    <div class="tab">
        <div class="sector-content">
            <div class="text-block">
                <p><?=$arResult["PREVIEW_TEXT"]?></p>
            </div>

            <?if (!empty($arResult["PROPERTIES"]["PHOTO"]["VALUE"])) {?>
            <div class="visual-gallery">
                <a href="#" class="prev">prev</a>
                <a href="#" class="next">next</a>
                <div class="gallery-frame">
                    <ul class="gallery">
                        <?foreach ($arResult["PROPERTIES"]["PHOTO"]["VALUE"] as $key=>$photo) {?>
                        <li>
                            <a class="fancybox-service-detail" href="<?=CFile::GetPath($photo)?>">
                                <img src="<?=MakeImage($photo, array("z" => 1, "w" => 260, "h" => 230))?>" alt="<?=$arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"][$key]?>">
                            </a>
                        </li>
                        <?}?>
                    </ul>
                </div>
            </div>
            <?}?>

            <?if (is_array($arResult["PROPERTIES"]["BENEFITS"]["VALUE"]) && !empty($arResult["PROPERTIES"]["BENEFITS"]["VALUE"])):?>
            <p class="name">Преимущества</p>
            <ul>
                <?foreach ($arResult["PROPERTIES"]["BENEFITS"]["VALUE"] as $n=>$val):?>
                    <li><?=++$n?>. <?=$val?></li>
                <?endforeach?>
            </ul>
            <?endif?>

            <?=$arResult["DETAIL_TEXT"]?>

            <ul class="button-listing">
                <?/*<li>
                    <a href="#" class="button05 block-button">расписание занятий</a>
                </li>
                <li>
                    <a href="#" class="button02 block-button">Анонс семинаров</a>
                </li>*/?>
                <li>
                    <a href="#" class="button01 block-button link_zapis_priem">Записаться на прием</a>
                </li>
                <li>
                    <a href="#" class="button05 block-button link_ask_direction" data-service="<?=$arResult["ID"]?>">Задать вопрос врачу</a>
                </li>
            </ul>
        </div>
        <?if(!empty($arResult["CLINICS"])):?>
        <strong class="strong-title">Услуга “<?=$arResult["NAME"]?>” предоставляется в клиниках:</strong>
        <div class="list-cols">
            <?foreach ($arResult["CLINICS"] as $key=>$c):?>
                <?if ($key%7==0):?>
                    <div class="col"><ul class="link-list">
                <?endif?>
                        <li><a href="<?=$c["DETAIL_PAGE_URL"]?>"><?=$c["NAME"]?></a></li>
                <?$key++?>
                <?if ($key%7==0):?>
                    </ul></div>
                <?endif?>
            <?endforeach?>
                <?if ($key%7!=0):?>
                    </ul></div>
                <?endif?>
            <?/*
            $count=count($arResult["CLINICS"]);
            foreach ($arResult["CLINICS"] as $i=>$c) {?>
                <?if ((($i+1)%8==0) || ($i==0)) {?>
                    <div class="col"><ul class="link-list">
                <?}?>
                        <li><a href="<?=$c["DETAIL_PAGE_URL"]?>"><?=$c["NAME"]?></a></li>
                <?if (($i+1)%7==0) {?>
                    </ul></div>
                <?}?>
            <?}?>
            <?if ($count%7!=0) {?></ul></div><?}*/?>
        </div>
        <?endif;?>
    </div> <?//<div class="tab">?>

<?if ($bTabClinics):?>
    <div class="tab">
        <ul class="workers-list">
        <?foreach ($arResult["CLINICS"] as $arItem):?>
            <li>
                <div class="block-holder">
                    <div class="frame-holder clin-list-holder">
                        <div class="frame-image">
                            <?php if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) {
    ?>
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img
                                        src="<?= MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z" => 1, "w" => 190, "h" => 210)) ?>"
                                        alt="<?= $arItem["NAME"] ?>"></a>
                            <?php

} ?>
                        </div>
                    </div>

                    <div class="block-holder">
                        <div class="topic-panel clin-list-panel">
                            <div class="align-left">
                                <span class="name-title clin-list-title red-color"><a
                                        href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                                <!--<?php if (!empty($arItem["DISTANCE"])) {
    ?>
                                    <span class="distance-text">~<?= $arItem["DISTANCE"] ?>км</span>
                                <?php

} ?>-->
                                </span>
                            </div>
                            <?php if (is_array($arItem["PROPERTIES"]["ICONS"]["VALUE"]) && !empty($arItem["PROPERTIES"]["ICONS"]["VALUE"])): ?>
                                <div class="align-center-icons">
                                    <ul class="serv-icon-list">
                                        <?php foreach ($arItem["PROPERTIES"]["ICONS"]["VALUE"] as $value): ?>
                                            <?php $arIcon = $arResult["ICONS"][$value] ?>
                                            <li><img src="<?= $arIcon["PREVIEW_PICTURE"]["SRC"] ?>"
                                                     alt="<?= $arIcon["NAME"] ?>" title="<?= $arIcon["NAME"] ?>"/></li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            <?php endif ?>
                            <?/*<div class="align-right">
                                <a href="#" data-name="<?= $arItem["NAME"] ?>"
                                   class="button03 button08 new-make-btn link_reception_clinic">Записаться<br/> на прием</a>
                            </div>*/?>
                        </div>
                        <address class="list-address">
                            <?php /*if(!empty($arItem["PROPERTIES"]["EMAIL"]["VALUE"])):?><span class="row"><strong>E-mail:</strong><span> <?=$arItem["PROPERTIES"]["EMAIL"]["VALUE"]?></span></span><?endif;*/ ?>
                            <?php if (!empty($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"])): ?><span class="row">
                                <strong>Адрес:</strong><span> <?= htmlspecialchars_decode($arItem["PROPERTIES"]["ADDRESS"]["VALUE"]["TEXT"]) ?></span>
                                </span><?php endif; ?>
                            <?php if (!empty($arItem["PROPERTIES"]["PHONE"]["VALUE"])): ?><span class="row"><strong>Телефон:</strong><span
                                    class="clinics-phone js-phone"> <?= $arItem["PROPERTIES"]["PHONE"]["VALUE"] ?></span>
                                </span><?php endif; ?>
                        </address>
                        <ul class="info-list">
                            <li>
                                <p>
                                    <?= $arItem["PREVIEW_TEXT"] ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        <?endforeach?>
        </ul>
    </div>
<?endif?>
<?if ($bTabDoctors):?>
    <div class="tab">
        <ul class="workers-list">
        <?foreach ($arResult["DOCTORS"] as $arItem):?>
            <li>
                <div class="block-holder">
                    <div class="frame-holder">
                        <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                            <div class="frame-image left-block">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=MakeImage($arItem["PREVIEW_PICTURE"]["SRC"], array("z" => 1, "w" => 190, "h" => 210))?>" alt="<?=$arItem["NAME"]?>"></a>
                            </div>
                        <?else:?>
                            <div class="frame-image">
                                <?if ($arItem["PROPERTIES"]["SEX"]["VALUE_XML_ID"] == "F"):?>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/female.gif" alt="<?=$arItem["NAME"]?>">
                                <?else:?>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/male.gif" alt="<?=$arItem["NAME"]?>">
                                <?endif?>
                            </div>
                        <?endif?>
                    </div>
                    <div class="block-holder">
                        <span class="name-title red-color"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></span>
                        <ul class="info-list">
                        <?if (is_array($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"])):?>
                            <li>
                                    <strong class="title">Специальности:</strong>
                            </li>
                            <?foreach ($arItem["PROPERTIES"]["SPECIALTY"]["VALUE"] as $arValue):?>
                            <li>
                                    <?=$arValue["NAME"]?>
                            </li>
                            <?endforeach?>
                            <li>&nbsp;</li>
                        <?endif?>
                        <?if ($arItem["PROPERTIES"]["CATEGORY"]["VALUE"]):?>
                            <li>
                                    <?=$arItem["PROPERTIES"]["CATEGORY"]["VALUE"]?>
                            </li>
                        <?endif?>
                        <?if ($arItem["PROPERTIES"]["POST"]["VALUE"]):?>
                            <li>
                                    <?=$arItem["PROPERTIES"]["POST"]["VALUE"]?>
                            </li>
                        <?endif?>
                        <?if ($arItem["PROPERTIES"]["POST_FULL"]["VALUE"]):?>
                            <li>
                                    <?=$arItem["PROPERTIES"]["POST_FULL"]["VALUE"]?>
                            </li>
                        <?endif?>
                        <?if ($arItem["PROPERTIES"]["SKILLS"]["VALUE"]["TEXT"]):?>
                            <li>
                                <p>
                                    <strong class="title">Профессиональные навыки:</strong>
                                <?if ($arItem["PROPERTIES"]["SKILLS"]["VALUE"]["TYPE"] == "html"):?>
                                    <?=$arItem["PROPERTIES"]["SKILLS"]["~VALUE"]["TEXT"]?>
                                <?else:?>
                                    <?=$arItem["PROPERTIES"]["SKILLS"]["VALUE"]["TEXT"]?>
                                <?endif?>
                                </p>
                            </li>
                        <?endif?>

                        <?if (is_array($arItem["PROPERTIES"]["SECTION"]["VALUE"]) && !empty($arItem["PROPERTIES"]["SECTION"]["VALUE"])):?>
                            <li>
                                    <strong class="title">Отделение:</strong>
                            </li>
                            <li>
                            <?foreach ($arItem["PROPERTIES"]["SECTION"]["VALUE"] as $key=>$arValue):?><?if ($key>0):?>, <?endif?><?=$arValue["NAME"]?><?endforeach?>
                            </li>
                            <li>&nbsp;</li>
                        <?endif?>
                        </ul>
                        <ul class="listing-info">
                            <li>
                                <span>Ведет прием в:</span>
                                <?=implode('<br>', $arItem["CLINIC"])?>
                            </li>
                        </ul>
                        <ul class="button-listing">
                        <?if ($arItem["PROPERTIES"]["BUTTON_QUESTION"]["VALUE"]):?>
                            <li>
                                <a href="#" data-name="<?=$arItem["NAME"]?>" data-service="<?=$arResult['ID']?>" class="button05 block-button link_ask_direction">Задать вопрос</a>
                            </li>
                        <?endif?>
                            <?if (!empty($arItem["PROPERTIES"]["TABLE"]["VALUE"])) {?>
                            <li>
                                <a href="<?=$arItem["PROPERTIES"]["TABLE"]["VALUE"]?>" class="button02 block-button" target="_blank">Расписание приема</a>
                            </li>
                            <?}?>
                            <li>
                                <a href="#" data-doctor="<?=$arItem["ID"]?>" class="button01 block-button link_reception_doc">Записаться на прием</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        <?endforeach?>
        </ul>
    </div>
<?endif?>
<?if ($bTabArticles):?>
    <div class="tab">

        <ul class="articles-listing">
<?foreach ($arResult["ARTICLES"] as $arItem):?>
            <li>
                <div class="block-holder">
                    <div class="red-onhover add-able">
                        <div class="item-visual">

                        </div>
                        <div class="block-holder">
                            <div class="item-link">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                            </div>
                            <div class="text-item">
                                <p><?=$arItem["PREVIEW_TEXT"]?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
<?endforeach?>
        </ul>
    </div>
<?endif?>

<?if ($bTabNews):?>
    <div class="tab">
        <ul class="articles-listing">
        <?foreach ($arResult["NEWS"] as $arItem):?>
            <li>
                <div class="block-holder">
                    <div class="red-onhover add-able">
                        <div class="item-visual">

                        </div>
                        <div class="block-holder">
                            <div class="item-link">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                            </div>
                            <div class="text-item">
                                <p><?=$arItem["PREVIEW_TEXT"]?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        <?endforeach?>
        </ul>
    </div>
<?endif?>

<?if ($bTabPrograms):?>
    <div class="tab">
        <?php
            global $arrProgramFilter;
            $arrProgramFilter = array('PROPERTY_SERVICES' => $arResult['SERVICE_IDS']);
        ?>
        <?php $APPLICATION->IncludeComponent("melcosoft:catalog", "program", array(
            "IBLOCK_TYPE" => "contentsite",
            "IBLOCK_ID" => "27",
            "BRAND_IBLOCK_TYPE" => "content",
            "BRAND_IBLOCK_ID" => "",
            "HIDE_NOT_AVAILABLE" => "N",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "SEF_MODE" => "Y",
            "SEF_FOLDER" => "/program/",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "SET_TITLE" => "Y",
            "SET_STATUS_404" => "Y",
            "USE_ELEMENT_COUNTER" => "Y",
            "USE_FILTER" => "Y",
            "FILTER_NAME" => "arrProgramFilter",
            "FILTER_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "FILTER_PROPERTY_CODE" => array(
                0 => "",
                1 => "",
            ),
            "FILTER_PRICE_CODE" => array(
            ),
            "USE_REVIEW" => "N",
            "USE_COMPARE" => "N",
            "PRICE_CODE" => array(
            ),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "PRICE_VAT_SHOW_VALUE" => "N",
            "PRODUCT_PROPERTIES" => array(
            ),
            "USE_PRODUCT_QUANTITY" => "N",
            "CONVERT_CURRENCY" => "N",
            "SHOW_TOP_ELEMENTS" => "N",
            "SECTION_COUNT_ELEMENTS" => "Y",
            "SECTION_TOP_DEPTH" => "2",
            "PAGE_ELEMENT_COUNT" => "30",
            "LINE_ELEMENT_COUNT" => "3",
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_ORDER" => "asc",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER2" => "desc",
            "LIST_PROPERTY_CODE" => array(
                0 => "CLINIC",
                1 => "DIRECTION",
                2 => "SERVICES",
            ),
            "INCLUDE_SUBSECTIONS" => "Y",
            "LIST_META_KEYWORDS" => "-",
            "LIST_META_DESCRIPTION" => "-",
            "LIST_BROWSER_TITLE" => "-",
            "DETAIL_PROPERTY_CODE" => array(
                0 => "",
                1 => "URL",
                2 => "",
            ),
            "DETAIL_META_KEYWORDS" => "-",
            "DETAIL_META_DESCRIPTION" => "-",
            "DETAIL_BROWSER_TITLE" => "-",
            "LINK_IBLOCK_TYPE" => "",
            "LINK_IBLOCK_ID" => "",
            "LINK_PROPERTY_SID" => "",
            "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
            "USE_ALSO_BUY" => "N",
            "USE_STORE" => "N",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Товары",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "SEF_URL_TEMPLATES" => array(
                "sections" => "",
                "brand" => "",
                "filter" => "",
                "section" => "#SECTION_CODE#/",
                "element" => "#ELEMENT_CODE#/",
                "compare" => "",
            ),
            ),
            false
        ); ?>
    </div>
<?endif?>

<?if($bTabMemo):?>
    <div class="tab">
        <div class="text-block">
            <?=$memoBlock;?>
        </div>
    </div>
<?endif;?>
<?if ($bTabActions):?>
    <div class="tab">
        <div class="text-block">
                <?php echo $actionsBlock; ?>
        </div>
    </div>
<?endif?>

</section>
