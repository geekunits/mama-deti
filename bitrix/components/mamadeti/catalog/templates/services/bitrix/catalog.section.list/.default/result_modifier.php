<?php
$CITY_ID = GetCurrentCity();

$serviceChildIds = array();
$rsSection = CIBlockSection::GetList(array(), array('IBLOCK_ID' => 1));
while ($arSection = $rsSection->GetNext()) {
	$serviceChildIds[$arSection['ID']] = $arSection['IBLOCK_SECTION_ID'] ? $arSection['IBLOCK_SECTION_ID'] : $arSection['ID'];
}

$rsElement = CIBlockElement::GetList(
    array(), 
    array(
		'IBLOCK_ID' => 1,
		'INCLUDE_SUBSECTIONS' => 'Y',
	)
);
$serviceIds = array();
while ($arElement = $rsElement->GetNext()) {
	$rootId = $serviceChildIds[$arElement['IBLOCK_SECTION_ID']];
	while ($rootId !== $serviceChildIds[$rootId]) {
		$rootId = $serviceChildIds[$rootId];
	}
    $serviceIds[$arElement['ID']] = $rootId;
}
unset($rsElement);

$rsClinic = CIBlockElement::GetList(
	array(),
	array(
		'IBLOCK_ID' => 2,
		'PROPERTY_CITY' => $CITY_ID,
		'ACTIVE' => 'Y',
	),
	false,
	false,
	array(
		'IBLOCK_ID',
		'ID',
		'PROPERTY_CITY',
		''
	)
);

$usedServiceIds = array();
while ($clinic = $rsClinic->GetNextElement()) {
	$props = $clinic->GetProperties();
	$clinicServiceIds = $props['SERVICES']['VALUE'];
	foreach ($clinicServiceIds as $id) {
		$usedServiceIds[] = $serviceIds[$id];
	}
}
unset($rsClinic);

$usedServiceIds = array_values(array_unique($usedServiceIds));

$arResult['SECTIONS'] = array_filter($arResult['SECTIONS'], function($service) use ($usedServiceIds) {
	return in_array($service['ID'], $usedServiceIds);
});