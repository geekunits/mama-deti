<?php
    $image = null;
    if ($arResult['PREVIEW_PICTURE']) {
        $image =
            CFile::ResizeImageGet(
                $arResult['PREVIEW_PICTURE']['ID'],
                ['height' => 320, 'width' => 320],
                BX_RESIZE_IMAGE_EXACT
            );
        $image = $image['src'];
    }
    $phones = is_array($arResult["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]) ? 
        $arResult["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"] : array($arResult["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]);
    $phoneDescriptions = is_array($arResult["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]) ? 
        $arResult["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($arResult["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]);
    $numPhoneColumns = 3;
?>
<div class="b-service_wrap clearfix">
    <div class="b-bg_white b-service_clinic b-service_margin__bottom clearfix">
        <div class="b-service_clinic__left">
            <div class="b-service_clinic__pic"><a href="<?php echo $arResult['DETAIL_PAGE_URL']; ?>"><?php if ($image): ?><img src="<?php echo $image; ?>" alt="<?php echo htmlspecialchars($arResult['NAME']); ?>"><?php endif; ?></a></div>
        </div>
        <div class="b-service_padding b-service_clinic__right">
            <h2 class="b-header_h2"><?php echo $arResult['NAME']; ?></h2>
            <div class="b-service_clinic__about">
                <?php echo mb_substr(strip_tags($arResult['PREVIEW_TEXT']), 0, 125); ?>...
                <a href="<?php echo $arResult['DETAIL_PAGE_URL']; ?>">Подробнее</a>
            </div>
            <div class="b-clinic_address__txt">
                <strong>Адрес:</strong><br><?php echo $arResult['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE']; ?>
            </div>
            <div class="b-clinic_address__txt">

                <strong>Телефон:</strong>
                <div>
                    <?php foreach($phones as $index => $phone): ?>
                        <div><span class="js-phone"><?php echo $phone; ?></span><?php echo empty($phoneDescriptions[$index]) ? '' : '<span class="b-clinic_address__phone-note">'.$phoneDescriptions[$index].'</span>'; ?></div>
                    <?php endforeach; ?>
                </div>

            </div>
            <div class="b-clinic_address__txt">
                <strong>E-mail:</strong><br><a class="b-clinic_address__mail" href="mailto:<?php echo $arResult['DISPLAY_PROPERTIES']['CONTACT_EMAIL']['DISPLAY_VALUE']; ?>"><?php echo $arResult['DISPLAY_PROPERTIES']['CONTACT_EMAIL']['DISPLAY_VALUE']; ?></a>
            </div>
        </div>
    </div>

    <?php if ($arParams['SERVICE']['SERVICE_TYPE'] === 'section'): ?>
        <?php
            global $arrFilterClinicDoctors;
            $arrFilterClinicDoctors = ['PROPERTY_SERVICES' => $arParams['SERVICE']['SERVICE_IDS'], 'PROPERTY_CLINIC' => $arResult['ID']];
        ?>

        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "clinic-doctors-inside",
            Array(
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => DOCTOR_IBLOCK_ID,
                "NEWS_COUNT" => "100",
                "SORT_BY1" => "RAND",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "PREVIEW_PICTURE",
                "SORT_ORDER2" => "DESC",
                "FILTER_NAME" => "arrFilterClinicDoctors",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('SERVICES','CLINIC','CATEGORY','POST_FULL', 'SPECIALTY', 'CITY', 'SEX'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "60",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "SERVICE" => $arParams['SERVICE'],
                'CLINIC_ID' => $arResult['ID'],
            ),
        $component->__parent
        ); ?>
    <?php endif; ?>
</div>
