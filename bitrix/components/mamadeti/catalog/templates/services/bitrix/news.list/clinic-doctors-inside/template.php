<?php if ($arResult['ITEMS']): ?>
    <?php
        $itemCount = count($arResult['ITEMS']);
    ?>
    <div class="b-bg_white b-service_margin__bottom b-service_doctors__wrap">
        <div class="b-service_padding b-slider_content">
            <h2 class="b-header_h2 align-center">Услугу «<?php echo $arParams['SERVICE']['NAME']; ?>» оказывают врачи:</h2>
            <?php require __DIR__.'/../../../_doctor_list.php'; ?>
        </div>
    </div>
<?php endif; ?>
