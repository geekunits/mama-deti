<?php if ($arResult['ITEMS']): ?>
    <div class="b-bg_white b-service_clinics b-service_margin__bottom">
        <?php 
            $maps = [];
            $mapId = 'map-clinics'; 
        ?>
        <div class="b-service_clinics__map l-loader" id="<?php echo $mapId; ?>"></div>
        <div class="b-service_clinics__list">
            <div class="b-header_h2 align-center">Услуга «<?php echo $arParams['SERVICE']['NAME'] ?>» в <span class="in-bl">«Мать и дитя»</span> <?php echo CityManager::getInstance()->getCurrentCity()['NAME']; ?></div>
            <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                <a href="<?php echo $arParams['SERVICE']['URL'].'clinic/'.$arItem['CODE'].'/'; ?>" class="b-service_clinics__item js-main-clinic-map" data-placemark="<?php echo $arItem["ID"]; ?>">
                    <span class="b-service_clinics__name"><?php echo htmlspecialchars($arItem['NAME']); ?></span> 
                    <?php 
                        $phones = is_array($arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"] : array($arItem["DISPLAY_PROPERTIES"]["PHONE"]["VALUE"]);
                        $phoneDescriptions = is_array($arItem["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]) ? $arItem["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"] : array($arItem["DISPLAY_PROPERTIES"]["PHONE"]["DESCRIPTION"]);
                    ?>
                    <?php if (!empty($phones)): ?>
                        <?php foreach($phones as $key => $phone): ?>
                            <?php if($key > 0): ?>, <?php endif; ?><span class="js-phone" itemprop="telephone" data-clinic-id="<?php echo $arItem['ID']; ?>"><?php echo $phone; ?></span><?php echo empty($phoneDescriptions[$key]) ? '' : '<span class="b-clinics_main__phone-note">'.$phoneDescriptions[$key].'</span>'; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php/* if (in_array($arItem['ID'], Service::getInstance()->getEkoClinicIds($arParams['SERVICE']))): ?>
                        <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>oms/" class="b-service_hosp-centrs_label">ЭКО ПО ОМС</a>
                    <?php endif; */?>
                </a>
                <?php

                    $arClinicPhones = is_array($arItem['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']) ? $arItem['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE'] : array($arItem['DISPLAY_PROPERTIES']['PHONE']['DISPLAY_VALUE']);
                    $arClinicDescriptions = is_array($arItem['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']) ? $arItem['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION'] : array($arItem['DISPLAY_PROPERTIES']['PHONE']['DESCRIPTION']);
                    $clinicPhones = '';
                    
                    foreach($arClinicPhones as $phoneIndex => $phoneValue) {
                        $phoneDescription = empty($arClinicDescriptions[$phoneIndex]) ? '' : ' (' . $arClinicDescriptions[$phoneIndex] . ')';
                        $clinicPhones .= '<span>' . $phoneValue . $phoneDescription . '<span><br>';
                    }

                    $maps[$mapId][] = [
                        'coords' => $arItem['PROPERTIES']['COORD']['VALUE'],
                        'name' => $arItem['NAME'],
                        'id' => $arItem['ID'],
                        'content' =>
                            '<a href="'.$arItem['DETAIL_PAGE_URL'].'">'.$arItem['NAME'].'</a><br/>'
                            . $clinicPhones
                            . $arItem['DISPLAY_PROPERTIES']['ADDRESS']['DISPLAY_VALUE'],
                    ];
                ?>
            <?php endforeach; ?>
        </div>
    </div>
    <?php if ($maps): ?>
        <script type="text/javascript">
            ymaps.ready(function () {
                var map,
                    placemarks = {};

                <?php foreach ($maps as $mapId => $placemarks): ?>
                    <?php $firstCoords = $placemarks[0]['coords']; ?>
                    map = new ymaps.Map (<?php echo json_encode($mapId) ?>, {
                        center: [<?php echo $firstCoords; ?>],
                        zoom: 9,
                        controls: ["smallMapDefaultSet"]
                    });

                    <?php foreach ($placemarks as $placemark): ?>
                        var placemark = new ymaps.Placemark([<?php echo $placemark['coords']; ?>], {
                                // iconContent: <?php echo json_encode($placemark['name']); ?>,
                                // hintContent: <?php echo json_encode($placemark['name']); ?>, 
                                balloonContent: <?php echo json_encode($placemark['content']); ?>
                                
                            });
                        placemarks[<?php echo $placemark['id'] ;?>] = placemark;
                        placemark.events.add('mouseenter', function(e) {
                            var placemark = e.get('target');
                            placemark.balloon.open();
                        });
                        placemark.events.add('mouseleave', function(e) {
                            var placemark = e.get('target');
                            // placemark.balloon.close();
                        });
                        map.geoObjects.add(placemark);
                    <?php endforeach; ?>

                    <?php if (count($placemarks) > 1): ?>
                        map.setBounds(map.geoObjects.getBounds());
                        map.setZoom(map.getZoom() - 1);
                    <?php else: ?>
                        map.setZoom(13);
                    <?php endif; ?>
                <?php endforeach; ?>
                $('#<?php echo $mapId; ?>').removeClass('l-loader');

                $('.js-main-clinic-map').mouseenter(function() {
                    placemark = placemarks[$(this).data('placemark')];
                    placemark.balloon.open();
                });
            });
        </script>
    <?php endif; ?>

<?php endif; ?>
