<?php if ($arResult['ITEMS']): ?>
    <div class="b-service_padding">
        <h2 class="b-header_h2 align-center">Акции</h2>
        <div class="b-new b-slider_content b-service_actions__wrap">
            <ul class="clearfix js-slider-carousel b-slider_no" data-countrow="2">
                <?php foreach($arResult['ITEMS'] as $arItem): ?>
                    <?php
                        $image = null;
                        if ($arItem['DETAIL_PICTURE']) {
                            $image =
                                CFile::ResizeImageGet(
                                    $arItem['DETAIL_PICTURE']['ID'],
                                    ['height' => 155, 'width' => 315],
                                    BX_RESIZE_IMAGE_EXACT
                                );
                            $image = $image['src'];
                        }

                        $text = strip_tags($arItem['PREVIEW_TEXT']);
                        if (mb_strlen($text) > 100) {
                            $text = trim(mb_substr($text, 0, 100)).'...';
                        }

                        $date = [];
                        if ($arItem['DISPLAY_PROPERTIES']['DATE_FROM']['DISPLAY_VALUE']) {
                            $date[] = 'с '.$arItem['DISPLAY_PROPERTIES']['DATE_FROM']['DISPLAY_VALUE'];
                        }
                        if ($arItem['DISPLAY_PROPERTIES']['DATE_TO']['DISPLAY_VALUE']) {
                            $date[] = 'по '.$arItem['DISPLAY_PROPERTIES']['DATE_TO']['DISPLAY_VALUE'];
                        }
                        $date = upperCaseFirst(implode(' ', $date));
                    ?>

                    <li class="b-service_actions__item">
                        <a class="b-news_link" href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>">
                            <span class="b-news_pic">
                                <?php if ($image): ?>
                                    <img alt="<?php echo htmlspecialchars($arItem['NAME']); ?>" src="<?php echo $image; ?>">
                                <?php endif; ?>
                            </span>
                            <span class="b-news_date"><?php echo $date; ?></span>
                            <span class="b-news_name"><?php echo $arItem['NAME']; ?></span>
                            <span class="b-news_txt"><?php echo $text; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>
