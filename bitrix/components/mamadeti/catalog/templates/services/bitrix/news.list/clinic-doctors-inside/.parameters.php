<?php
$arTemplateParameters['SERVICE'] = array(
    'PARENT' => 'DATA_SOURCE',
    'NAME' => 'Услуга',
    'TYPE' => 'STRING',
    'REFRESH' => 'Y',
    'DEFAULT' => 'N',
);
$arTemplateParameters['CLINIC_ID'] = array(
    'PARENT' => 'DATA_SOURCE',
    'NAME' => 'Клиника',
    'TYPE' => 'STRING',
    'REFRESH' => 'Y',
    'DEFAULT' => 'N',
);
