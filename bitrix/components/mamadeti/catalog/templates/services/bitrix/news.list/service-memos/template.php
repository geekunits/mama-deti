<?php if ($arResult['ITEMS']): ?>
    <div class="b-bg_yellow b-service_padding b-service_margin__bottom">
        <h2 class="b-header_h2 align-center">Памятки по услуге</h2>
        <div class="b-service_checklist b-slider_content b-slider__arrow__center">
            <ul class="b-service_checklist-items clearfix js-slider-carousel" data-countrow="2">
                <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                    <?php
                        $text = $arItem['PREVIEW_TEXT'];
                        $text = strip_tags($text);
                        if (strlen($text) > 200) {
                            $text = trim(substr($text, 0, 200)).'...';
                        }
                    ?>

                    <li class="b-service_checklist-item">
                        <div class="b-service_checklist-name">
                            <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"><?php echo $arItem['NAME']; ?></a>
                        </div>
                        <div class="b-service_checklist-intro">
                            <?php echo $text; ?>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>
