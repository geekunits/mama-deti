<?php if ($arResult['ITEMS']): ?>
    <div class="b-bg_white b-service_padding b-service_articles b-service_margin__bottom">
        <h2 class="b-service_articles-title b-header_h2 align-center">Статьи</h2>
        <div class="b-service_articles-items clearfix">
            <?php
                $index = 0;

                switch (count($arResult['ITEMS'])) {
                    case 1:
                        $classPrefix = 'one';
                        break;

                    case 2:
                        $classPrefix = 'two';
                        break;

                    default:
                        $classPrefix = 'two';
                        break;
                }
            ?>
            <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                <?php
                    $index++;

                    $doctors = $arItem['DISPLAY_PROPERTIES']['DOCTOR']['DISPLAY_VALUE'];
                    if (!is_array($doctors)) {
                        $doctors = [$doctors];
                    }

                    $doctors = array_map(function ($doctor) {
                        $name = strip_tags($doctor);
                        $shortenName = shortenName($name);

                        return str_replace($name, $shortenName, $doctor);
                    }, $doctors);
                ?>
                <?php if (!(($index - 1) % 2)): ?>
                    <?php if (($index - 1) == 2): ?>
                        <div class="b-service_artices__hidden js-article-hidden">
                    <?php endif; ?>

                    <div class="b-service_articles-item">
                        <div class="b-columns_<?php echo $classPrefix; ?> clearfix">
                <?php endif; ?>

                            <div class="b-column">
                                <?php /*if ($doctors): ?>
                                    <div class="b-doctor b-doctor_small clearfix">
                                        <?php if (count($doctors) === 1): ?>
                                            <?php $doctor = $arItem['DOCTOR']; $image = $arItem['DOCTOR']['THUMB_PICTURE']; ?>
                                            <div class="b-doctor_part-left">
                                                <a class="b-doctor_avatar<?php if (!$image): ?> b-doctor_nophoto__female <?php echo $isFemale ? '' : ' b-doctor_nophoto__male'; ?><?php endif; ?>" href="<?php echo $doctor['DETAIL_PAGE_URL']; ?>">
                                                    <?php if ($image): ?><img src="<?php echo $image['SRC']; ?>" alt="<?php echo htmlspecialchars($doctor['SHORTEN_NAME']); ?>"><?php endif; ?>
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="b-doctor_intro">
                                            <div class="b-doctor_text"><?php echo count($doctors) === 1 ? 'Автор' : 'Авторы'; ?>:</div>
                                            <div class="b-doctor_name"><?php echo implode(', ', $doctors); ?></div>
                                        </div>
                                    </div>
                                <?php endif; */?>
                                <div class="b-doctor b-doctor_small">
                                    <?php foreach($arItem['DOCTORS'] as $arDoctor): ?>
                                        <div class="b-service_articles-doctor clearfix">
                                            <?php
                                                $image = $arDoctor['THUMB_PICTURE'];
                                            ?>
                                            <div class="b-doctor_part-left">
                                                <a class="b-doctor_avatar<?php if (!$image): ?> b-doctor_nophoto__female <?php echo $isFemale ? '' : ' b-doctor_nophoto__male'; ?><?php endif; ?>" href="<?php echo $arDoctor['DETAIL_PAGE_URL']; ?>">
                                                    <?php if ($image): ?>
                                                        <img src="<?php echo $image['SRC']; ?>" alt="<?php echo htmlspecialchars($arDoctor['SHORT_NAME']); ?>">
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                            <div class="b-doctor_intro">
                                                <div class="b-doctor_text">Автор:</div>
                                                <div class="b-doctor_name"><?php echo $arDoctor['SHORT_NAME']; ?></div>
                                            </div>
                                        </div>                                    
                                    <?php endforeach; ?>
                                </div>
                                <div class="b-service_articles-name"><a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"><?php echo shortenString($arItem['NAME'], 30); ?></a></div>
                                <div class="b-service_articles-intro"><?php echo shortenString(strip_tags($arItem['PREVIEW_TEXT']), 150); ?></div>
                            </div>

                <?php if (($index > 1 && !($index % 2)) || $index === count($arResult['ITEMS'])): ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <?php if (count($arResult['ITEMS']) > 2): ?>
            </div>
            <div class="align-center js-article-show-all" data-toggle-title="Свернуть"><span class="in-bl b-link_more">Показать еще</span></div>
        <?php endif; ?>
    </div>
<?php endif; ?>
