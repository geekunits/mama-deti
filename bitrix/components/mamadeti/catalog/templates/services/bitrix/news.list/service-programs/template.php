<?php if ($arResult['ITEMS']): ?>
    <div class="b-bg_yellow b-service_padding">
        <h2 class="b-header_h2 align-center">Программы</h2>
        <div class="b-service_program__wrap">
            <?php
                $index = 0;
            ?>
            <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                <?php
                    $index++;
                ?>
                <?php if (!(($index - 1) % 3)): ?>
                    <?php if (($index - 1) == 3): ?>
                        <div class="b-service_program__hidden js-program-hidden">
                    <?php endif; ?>

                    <div class="b-service_program__item">
                        <div class="b-columns_three clearfix">
                <?php endif; ?>

                            <div class="b-column">
                                <a class="b-service_program__link" href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>">
                                    <span class="b-service_program__name"><?php echo $arItem['NAME']; ?></span>
                                    <?php if ($arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE']): ?>
                                        <span class="b-service_program__cost"><?php echo formatCurrencyAmount($arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE'],true); ?></span>
                                    <?php endif; ?>
                                </a>
                            </div>

                <?php if (($index > 1 && !($index % 3)) || $index === count($arResult['ITEMS'])): ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <?php if (count($arResult['ITEMS']) > 3): ?>
            </div>
            <div class="align-center js-program-show-all" data-toggle-title="Скрыть"><span class="in-bl b-link_more">Показать еще</span></div>
        <?php endif; ?>
    </div>
<?php endif; ?>
