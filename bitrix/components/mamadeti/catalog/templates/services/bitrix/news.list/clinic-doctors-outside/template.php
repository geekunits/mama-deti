<?php if ($arResult['ITEMS']): ?>
    <?php
        $itemCount = count($arResult['ITEMS']);
    ?>
    <div class="b-bg_white b-service_wrap b-service_margin__bottom">
        <div class="b-service_padding b-slider_content">
            <div class="b-header_h2 align-center">Услугу «<?php echo $arParams['SERVICE']['NAME']; ?>» оказывают врачи:</div>
            <?php include __DIR__.'/../../../_doctor_list.php'; ?>
        </div>
    </div>
<?php endif; ?>
