<?php
/*
 * Костыль работает следующим образом.
 * Если Мы находимся в услугах в разделе Скорой помощи, либо в элементах раздела Скорой помощи,
 * то поднимаем клинику Кунцево на первую позицию в выдаче
 */

if(!empty($arParams['SERVICE'])){
    $arService = $arParams['SERVICE'];

    $serviceSectionCode = 'ambulance-and-help-at-home';

    $isAmbulance = false;

    if(isset($arService['IBLOCK_SECTION_ID'])){ //это элемент, нам нужно получить раздел, чтобы проверить скорая помощь это или нет
        $rsSection = CIBlockSection::GetList(
            array(),
            array(
                'IBLOCK_ID' => $arService['IBLOCK_ID'],
                'ACTIVE'    => 'Y',
                'ID'        => $arService['IBLOCK_SECTION_ID']
            ),
            false,
            array('CODE')
        );

        if(($arSection = $rsSection->Fetch()) && $arSection['CODE'] == $serviceSectionCode){
            $isAmbulance = true;
        }
    }elseif($arService['CODE'] == $serviceSectionCode){ //иначе, если это раздел скорой медицинской помощи
        $isAmbulance = true;
    }

    if($isAmbulance){ //если это раздел или элемент из скорой помощи, то поднимаем клинику кунцево
        $needClinicCode = 'the-clinic-mother-and-child-kuntsevo';

        foreach($arResult['ITEMS'] AS $index => $arClinic){
            if($arClinic['CODE'] == $needClinicCode){
                unset($arResult['ITEMS'][$index]);

                array_unshift($arResult['ITEMS'], $arClinic);

                break;
            }
        }
    }
}