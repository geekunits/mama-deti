<?php

$doctors = [];
foreach ($arResult['ITEMS'] as $arItem) {
    $doctors[$arItem['ID']] = $arItem;
}
$arResult['ITEMS'] = $doctors;

$rsReviewCount =
    CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => REVIEW_IBLOCK_ID,
            'ACTIVE' => 'Y',
            'PROPERTY_DOCTOR' => array_keys($arResult['ITEMS']),
        ],
        ['PROPERTY_DOCTOR'],
        false,
        ['IBLOCK_ID', 'ID', 'PROPERTY_DOCTOR']
    );

while ($arReviewCount = $rsReviewCount->GetNext()) {
    if (!$arReviewCount['PROPERTY_DOCTOR_VALUE']) {
        continue;
    }

    if (!isset($arResult['ITEMS'][$arReviewCount['PROPERTY_DOCTOR_VALUE']])) {
        continue;
    }

    $arResult['ITEMS'][$arReviewCount['PROPERTY_DOCTOR_VALUE']]['REVIEW_COUNT'] = $arReviewCount['CNT'];
}

$rsQaCount =
    CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => QA_IBLOCK_ID,
            'ACTIVE' => 'Y',
            'PROPERTY_DOCTOR' => array_keys($arResult['ITEMS']),
        ],
        ['PROPERTY_DOCTOR'],
        false,
        ['IBLOCK_ID', 'ID', 'PROPERTY_DOCTOR']
    );

while ($arQaCount = $rsQaCount->GetNext()) {
    if (!$arQaCount['PROPERTY_DOCTOR_VALUE']) {
        continue;
    }

    if (!isset($arResult['ITEMS'][$arQaCount['PROPERTY_DOCTOR_VALUE']])) {
        continue;
    }

    $arResult['ITEMS'][$arQaCount['PROPERTY_DOCTOR_VALUE']]['QA_COUNT'] = $arQaCount['CNT'];
}
