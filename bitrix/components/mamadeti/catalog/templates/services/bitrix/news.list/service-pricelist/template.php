<?php if ($arResult['ITEMS']): ?>
    <div class="b-bg_white b-service_wrap b-service_margin__bottom">
        <div class="b-service_padding">
            <h2 class="b-header_h2 align-center">Прайс-лист услуги &laquo;<?php echo $arParams['SERVICE']['NAME']; ?>&raquo;</h2>
            <div class="b-service_price__wrap">
                <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
                    <?php $index = 0; ?>

                    <?php foreach ($arResult['ITEMS'] as $arItem): ?>
                        <?php $index++; ?>

                        <?php if ($index === 5): ?>
                            </table>
                            <div class="b-service_price__hidden js-price-hidden">
                                <table width="100%" cellspacing="0" cellpadding="0" class="b-price">
                        <?php endif; ?>

                        <tr>
                            <td class="b-price_dot">
                                <span class="b-price_bg"><?php echo $arItem['NAME']; ?></span>
                            </td>
                            <td class="b-price_dot b-price_cost">
                                <span class="b-price_bg"><?php echo formatCurrencyAmount($arItem['DISPLAY_PROPERTIES']['PRICE']['DISPLAY_VALUE']); ?></span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <?php if ($index >= 5): ?>
                </div>
                <div class="align-center b-service_price__more js-price-show-all"><span class="in-bl b-link_more">Показать еще</span></div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
