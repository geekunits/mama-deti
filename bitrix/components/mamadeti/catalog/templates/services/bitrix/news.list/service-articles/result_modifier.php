<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
foreach ($arResult["ITEMS"] as &$arItem) {
    $arItem['DOCTORS'] = array();
    foreach($arItem["PROPERTIES"]["DOCTOR"]["VALUE"] as $doctorId) {
        $rsElement =
            CIBlockElement::GetList(
                array(),
                array('ID' => $doctorId),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'PROPERTY_SEX')
            );
        if ($obElement = $rsElement->GetNextElement()) {
            $arDoctor = $obElement->GetFields();
            $arDoctor["PROPERTIES"] = $obElement->GetProperties();

            if (intval($arDoctor["PROPERTIES"]["CLINIC"]["VALUE"])>0) {
                $rsElement = CIBlockElement::GetByID($arDoctor["PROPERTIES"]["CLINIC"]["VALUE"]);
                if ($obElement = $rsElement->GetNextElement()) {
                    $arItem["CLINIC"] = $obElement->GetFields();
                    $arItem["CLINIC"]["PROPERTIES"] = $obElement->GetProperties();
                }
            }

            $arDoctor['SHORT_NAME'] = shortenName($arDoctor['NAME']);

            $imageId = !empty($arDoctor["PREVIEW_PICTURE"]) ? $arDoctor["PREVIEW_PICTURE"] : $arDoctor["DETAIL_PICTURE"];
            if (!empty($imageId)) {
                $arFileTmp = CFile::ResizeImageGet(
                            $imageId, array("width" => 45, "height" => 45), BX_RESIZE_IMAGE_EXACT, true
                );
                $arDoctor["THUMB_PICTURE"] = array_change_key_case($arFileTmp, CASE_UPPER);
            }
        }
        $arItem['DOCTORS'][] = $arDoctor;
    }

    if (isset($arItem)) {
        unset($arItem);
    }
}
?>
