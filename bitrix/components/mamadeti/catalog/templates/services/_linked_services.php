<?php
    $linkedServiceIds = Service::getInstance()->getLinkedServiceIds($arResult['SERVICE'], $arResult['CLINIC']['ID']);

    $linkedServiceSections = array_map(function ($sectionId) use ($arResult) {
        return Service::getInstance()->findService('section', $sectionId, $arResult['CLINIC']['ID']);
    }, $linkedServiceIds['SECTION_IDS']);

    $linkedServiceElements = array_map(function ($elementId) {
        return Service::getInstance()->findService('element', $elementId, $arResult['CLINIC']['ID']);
    }, $linkedServiceIds['ELEMENT_IDS']);

    $linkedServices = array_merge($linkedServiceSections, $linkedServiceElements);
?>

<?php if ($linkedServices): ?>

    <div class="b-bg_white b-service_padding b-service_related b-service_margin__bottom">
        <h2 class="b-service_related-title b-header_h2 align-center">Связанные услуги</h2>
        <div class="b-columns_three clearfix">
            <div class="b-service_related__items clearfix">
                <?php foreach ($linkedServices as $index => $linkedService): ?>
                    <?php if ($index && !($index % 3)): ?>
                        </div>
                        <div class="b-service_related__items clearfix">
                    <?php endif; ?>
                    <div class="b-column">
                        <div class="b-service_related__name"><a href="<?php echo $linkedService['URL']; ?><?php if ($arResult['CLINIC']): ?>clinic/<?php echo $arResult['CLINIC']['CODE']; ?>/<?php endif; ?>"><?php echo $linkedService['NAME']; ?></a></div>
                        <?php echo shortenString(strip_tags($linkedService['DESCRIPTION']), 150); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
