<?php

ob_start();
require_once __DIR__.'/_actions.php';
$actions = trim(ob_get_clean());

ob_start();
require_once __DIR__.'/_programs.php';
$programs = trim(ob_get_clean());

if ($actions || $programs): ?>
<div class="b-bg_white b-service_wrap b-service_margin__bottom">
    <?php echo $actions; ?>
    <?php echo $programs; ?>
</div>
<?php endif;
