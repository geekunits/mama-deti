<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<div class="b-service_margin__bottom">
    <div class="b-service_priceform js-service-price-form">
        <div class="b-service_priceform__txt">Узнать цены на услуги <br>в этой клинике</div>
        <form class="search-price-list js-search-price-from">
            <div style="position: relative;">
                <div class="text04">
                    <input type="text" value="" placeholder="Название услуги" name="q" autocomplete="off">
                </div>
                <div id="search_result" class="search-drop" style="display: none; margin: 0px 0px 0px -9999px; z-index: auto;">
                    <div class="drop-frame">
                        <div class="scrollpane">
                            <ul class="search-listing"></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button02"><input type="submit">найти</div>
        </form>
    </div>

    <script>
        $(function(){
            $('.js-search-price-from').on('submit', function(){
                var $form = $(this);
                
                $.ajax({
                    method: 'GET',
                    data: $form.serialize(),
                    dataType: 'html',
                    beforeSend: function(){
                        $('.js-service-price-form').addClass('l-loader').find('#search_result').hide();
                    },
                    success: function(r){
                        $('.js-service-price-result').html(r).show();
                        $('.js-service-price-form #search_result').hide();
                    },
                    complete: function(){
                        $('.js-service-price-form').removeClass('l-loader');
                    }
                });

                return false;
            });
        });
    </script>

    <?php
    $isAjax = isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest";
    ?>
    <div class="js-service-price-result b-bg_white b-service_padding b-service_pricelist" style="display:none;">
        <?php
            if ($isAjax) {
                $APPLICATION->RestartBuffer();

                global $priceFilter;

                $priceFilter = array(
                    "!PROPERTY_PRICE" => false,
                );

                $searchText = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';

                if (!empty($searchText)) {
                    $priceFilter['?NAME'] = $searchText;
                }

                $APPLICATION->IncludeComponent("melcosoft:price.list",
                    "clinic-detail-new", array(
                        "IBLOCK_TYPE" => "contentsite",
                        "IBLOCK_ID" => "31",
                        "FILTER_NAME" => "priceFilter",
                        "CLINIC_ID" => $arResult["CLINIC"]["ID"],
                        "PAGE_ELEMENT_COUNT" => "50000",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_FILTER" => "Y",
                        "PAGER_TEMPLATE" => "mamadeti",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "DISABLE_PRICE_LIST_AJAX" => "N",
                        "_REQUEST" => $_REQUEST,
                        'SHOW_MORE_BUTTON' => 'Y',
                        'SHOW_PRINT_BUTTON' => 'Y',
                        'SHOW_EXPANDED' => 'Y',
                    ),
                    false
                );
                die();
            }
        ?>
    </div>
</div>