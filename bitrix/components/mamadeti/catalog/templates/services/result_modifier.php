<?php

if ($_REQUEST['CLINIC_CODE']) {
    $arClinic =
        CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => CLINIC_IBLOCK_ID, 'CODE' => $_REQUEST['CLINIC_CODE']],
            false,
            false,
            ['IBLOCK_ID', 'ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PROPERTY_NAME_GENITIVE', 'PROPERTY_NAME_PREPOSITIONAL', 'PROPERTY_PHONE', 'PROPERTY_METRIC_ID', 'PROPERTY_CITY', 'PROPERTY_OMS']
        )->GetNext();
    $arClinic['DOCTOR_IDS'] = CMamaDetiAPI::getDoctorsID(['PROPERTY_CLINIC' => $arClinic['ID']]);

    if ($arClinic['ID']) {
        $arClinic['SERVICE_IDS'] = array();
        $dbProperty = CIBlockElement::GetProperty(CLINIC_IBLOCK_ID, $arClinic['ID'], array('SORT' => 'ASC'), array('CODE' => 'SERVICES'));
        while ($arService = $dbProperty->GetNext()) {
            $arClinic['SERVICE_IDS'][] = $arService['VALUE'];
        }

    }
    $arResult['CLINIC'] = $arClinic;

    $cm = CityManager::getInstance();

    if ($arClinic['PROPERTY_CITY_VALUE'] && $cm->getSavedCityId() != $arClinic['PROPERTY_CITY_VALUE']) {
        // LocalRedirect($cm->getUrlByCityId($arClinic['PROPERTY_CITY_VALUE']).$arResult['SERVICE']['DETAIL_PAGE_URL'], false, 301);

    }
}

$arResult['SERVICE_TYPE'] = isset($arResult['VARIABLES']['ELEMENT_CODE']) ? 'element' : 'section';

$arResult['SERVICE_ID'] = isset($arResult['VARIABLES']['ELEMENT_CODE']) ? $arResult['VARIABLES']['ELEMENT_CODE'] : $arResult['VARIABLES']['SECTION_CODE'];

$arResult['SERVICE'] = Service::getInstance()->findService($arResult['SERVICE_TYPE'], $arResult['SERVICE_ID'], empty($arResult['CLINIC']) ? null : $arResult['CLINIC']['ID']);

if (!empty($arResult['CLINIC'])) {
    $arClinicServices = array_intersect($arResult['CLINIC']['SERVICE_IDS'], $arResult['SERVICE']['SERVICE_IDS']);
    if (empty($arClinicServices)) {
        $clinicRedireuctUrl = $arResult['SERVICE_TYPE'] === 'section' ? $arResult['SERVICE']['SECTION_PAGE_URL'] : $arResult['SERVICE']['DETAIL_PAGE_URL'];
        LocalRedirect($clinicRedireuctUrl, false, 301);

        // include $_SERVER['DOCUMENT_ROOT'] . '/header.php';
        // define('ERROR_404', 'Y');
        // CHTTP::SetStatus("404 Not Found");
        // include($_SERVER["DOCUMENT_ROOT"] . "/404.php");
        // include $_SERVER['DOCUMENT_ROOT'] . '/footer.php';
        // die();
    }


    $cm = CityManager::getInstance();
    if (!empty($arResult['SERVICE']) && $arResult['CLINIC']['PROPERTY_CITY_VALUE'] && $cm->getSavedCityId() != $arResult['CLINIC']['PROPERTY_CITY_VALUE']) {

        $redirectUrl = Service::getInstance()->getRedirectUrl($arResult['SERVICE_TYPE'], $arResult['SERVICE']['ID']);
        if (empty($redirectUrl)) {
            $redirectUrl = $arResult['SERVICE_TYPE'] === 'section' ? $arResult['SERVICE']['SECTION_PAGE_URL'] : $arResult['SERVICE']['DETAIL_PAGE_URL'];
        }
        LocalRedirect($redirectUrl, false, 301);
    }
}

if (!empty($arResult['SERVICE'])) {
    if(isset($arResult['SERVICE']['TIMESTAMP_X'])){
        $timestamp = MakeTimeStamp($arResult['SERVICE']['TIMESTAMP_X'], CSite::GetDateFormat());
        setLastModified($timestamp);
    }

    $redirectUrl = Service::getInstance()->getRedirectUrl($arResult['SERVICE_TYPE'], $arResult['SERVICE']['ID']);
    if (!empty($redirectUrl)) {
        LocalRedirect($redirectUrl, false, 301);
    }
}
