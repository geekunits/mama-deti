<?php

$cm = CityManager::getInstance();
$city = $cm->getCurrentCity();

if (!$arResult['CLINIC']) {
    $APPLICATION->AddChainItem($arResult['SERVICE']['NAME']);
    $title = $arResult['SERVICE']['NAME'];

    $metaTitle = empty($arResult['SERVICE']['META_TITLE']) ? $title : $arResult['SERVICE']['META_TITLE'];

    if ($city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE']) {
        // $title .= ' в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'];
        $metaTitle .= ' в '.$city['PROPERTIES']['NAME_PREPOSITIONAL']['VALUE'];
    }

} else {
    setCanonicalPage($arResult['SERVICE']['DETAIL_PAGE_URL']);
    $APPLICATION->arAdditionalChain = [];
    $APPLICATION->AddChainItem('Клиники', '/clinics/');
    $APPLICATION->AddChainItem($arResult['CLINIC']['NAME'], $arResult['CLINIC']['DETAIL_PAGE_URL']);
    $APPLICATION->AddChainItem($arResult['SERVICE']['NAME']);
    $title = $arResult['SERVICE']['NAME'];
    $metaTitle = empty($arResult['SERVICE']['META_TITLE']) ? $title : $arResult['SERVICE']['META_TITLE'];

    if ($arResult['CLINIC']['PROPERTY_NAME_PREPOSITIONAL_VALUE']) {
        $title .= ' в '.$arResult['CLINIC']['PROPERTY_NAME_PREPOSITIONAL_VALUE'];
        $metaTitle .= ' в '.$arResult['CLINIC']['PROPERTY_NAME_PREPOSITIONAL_VALUE'];
    }
}
$APPLICATION->SetTitle($title);
$APPLICATION->SetPageProperty('title', $metaTitle);
$APPLICATION->SetPageProperty("keywords", $arResult['SERVICE']['META_KEYWORDS']);
$APPLICATION->SetPageProperty("description", $arResult['SERVICE']['META_DESCRIPTION']);

include __DIR__.'/_description.php';

$APPLICATION->SetPageProperty('SHOW_H1','N');

if (!$arResult['CLINIC']) {
    include __DIR__.'/_clinics.php';
    include __DIR__.'/_doctors.php';
} else {
    $rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID"=> 31, "ACTIVE"=>"Y", "UF_CLINIC" => $arResult['CLINIC']['ID']));

    if($rsSection->Fetch()){
        include __DIR__.'/_pricelist.php';
    }

    include __DIR__.'/_clinic.php';

    /*if ($arResult['SERVICE']['SERVICE_TYPE'] === 'element') {
        require_once __DIR__.'/_pricelist.php';
    }*/

    require_once __DIR__.'/_actions_programs.php';

    if ($arResult['SERVICE']['SERVICE_TYPE'] === 'element') {
        require_once __DIR__.'/_doctors.php';
    }

    require_once __DIR__.'/_articles.php';

    if ($arResult['SERVICE']['SERVICE_TYPE'] === 'element') {
        require_once __DIR__.'/_memo.php';
    }
}

require_once __DIR__.'/_links.php';

require_once __DIR__.'/_linked_services.php';
