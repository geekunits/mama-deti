<?php

$cityId = $GLOBALS['CITY_ID'];
$skypeConsultations = [];
$clinicId = !empty($arResult['CLINIC']) ? $arResult['CLINIC']['ID'] : null;
if ($arResult['SERVICE']['SERVICE_TYPE'] === 'element') {
    $skypeConsultations = SkypeConsultationFinder::findByServiceId($cityId, $arResult['SERVICE']['ID'], $clinicId);
} elseif ($arResult['SERVICE']['SERVICE_TYPE'] === 'section') {
    $skypeConsultations = SkypeConsultationFinder::findByServiceSectionId($cityId, $arResult['SERVICE']['ID'], $clinicId);
}

    $showOmsBanner = false;
    if ($arResult['CLINIC']) {
        $phone = $arResult['CLINIC']['PROPERTY_PHONE_VALUE'];
        $phone = explode(',', $phone);
        $phone = reset($phone);
        $showOmsBanner = in_array($arResult['CLINIC']['ID'], Service::getInstance()->getEkoClinicIds($arResult['SERVICE']));
    } else {
        $omsClinicIds = Service::getInstance()->getEkoClinicIds($arResult['SERVICE']);
        $omsClinics = [];
        if ($omsClinicIds) {
            $rsClinics = CIBlockElement::GetList([], [
                'IBLOCK_ID' => CLINIC_IBLOCK_ID,
                'ACTIVE' => 'Y',
                'PROPERTY_CITY' => GetCurrentCity(),
                'PROPERTY_SERVICES' => $arResult['SERVICE']['SERVICE_IDS'],
                'ID' => Service::getInstance()->getEkoClinicIds($arResult['SERVICE']),
            ]);
            while ($arClinic = $rsClinics->GetNext()) {
                $omsClinics[] = $arClinic;
            }
            unset($rsClinics);
        }
        
        $showOmsBanner = count($omsClinics) > 0;
    
        /*$city = CityManager::getInstance()->getCurrentCity();

        $phone = $city['PROPERTIES']['PHONE']['VALUE'];

        $phone = html_entity_decode(reset($phone));*/
        $phone = null;
    }
?>
<div class="b-bg_white b-service_wrap b-service_margin__bottom js-element-wrap" data-id="<?=$arResult["SERVICE"]["ID"]?>">
    <?php 
        if ($arResult['CLINIC']) {
            require_once __DIR__ . '/_clinic_menu.php';
        }
    ?>

    <div class="b-service_padding b-service_about">
        <h1 class="b-header_h1 align-center"><?php echo $APPLICATION->GetTitle(); ?></h1>
        <div class="js-service-text-cut b-service_about__text clearfix">
        <div class="service-txt-right js-service-sidebar">
            <?php
                $showPregnantCount = false;

                if($arResult['SERVICE']['SERVICE_TYPE'] === 'element'){
                    $showPregnantCount = !empty($arResult['SERVICE']['PROPERTIES']['SHOW_PREGN_COUNT']['VALUE']);
                }else{
                    $showPregnantCount = !empty($arResult['SERVICE']['UF_SHOW_PREGN_COUNT']);
                }

                if($showPregnantCount){
                    $arCurrentCity = $GLOBALS['CURRENT_CITY'];

                    if($arCurrentCity['PROPERTIES']['PREGNANT_COUNT']['VALUE'] || $arCurrentCity['PROPERTIES']['BORN_COUNT']['VALUE']){
                        ?>
                        <div class="service-banner-eko js-banner-eko">
                            <?php
                            if($arCurrentCity['PROPERTIES']['PREGNANT_COUNT']['VALUE']){
                                ?>
                                <div class="service-banner-eko__item">
                                    <div class="service-banner-eko__num"><?php echo number_format($arCurrentCity['PROPERTIES']['PREGNANT_COUNT']['VALUE'], 0, '.', ' ')?></div>
                                    <div class="service-banner-eko__note">наступивших Беременностей</div>
                                </div>
                                <?php
                            }

                            if($arCurrentCity['PROPERTIES']['BORN_COUNT']['VALUE']){
                                ?>
                                <div class="service-banner-eko__item service-banner-eko__item--child">
                                    <div class="service-banner-eko__num"><?php echo number_format($arCurrentCity['PROPERTIES']['BORN_COUNT']['VALUE'], 0, '.', ' ')?></div>
                                    <div class="service-banner-eko__note">рожденных детей</div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                }
            ?>
            <?php if (!empty($skypeConsultations)): ?>
                <div class="skype-consult skype-consult--services js-skype-consult">
                    <div class="skype-consult__head">Skype-консультации<br>бесплатно</div>
                    <div class="skype-consult__content">
                        <?php foreach($skypeConsultations as $skypeConsultation): ?>
                            <?php
                                $arDoctor = $skypeConsultation->getDoctorRaw();
                                $doctorImageId = !empty($arDoctor['PREVIEW_PICTURE']) ? $arDoctor['PREVIEW_PICTURE'] : $arDoctor['DETAIL_PICTURE'];
                                $doctorImageSrc = MakeImage($doctorImageId, ['w' => 100, 'h' => 100, 'z' => 1]);
                                $arSpecialities = [];
                                $arSpecialityNames = [];
                                if (!empty($arDoctor['PROPERTIES']['SPECIALTY']['VALUE'])) {
                                    $dbResult = CIBlockElement::GetList(['SORT' => 'ASC'],
                                        [
                                            'IBLOCK_ID' => SPECIALTY_IBLOCK_ID,
                                            'ID' => $arDoctor['PROPERTIES']['SPECIALTY']['VALUE'],
                                        ],
                                        false,
                                        false,
                                        ['ID', 'IBLOCK_ID', 'NAME']
                                    );
                                    while($arSpeciality = $dbResult->GetNext()) {
                                        $arSpecialities[] = $arSpeciality;
                                        $arSpecialityNames[] = mb_strtolower($arSpeciality['NAME']);
                                    }
                                }
                                $doctorName = shortenName($arDoctor['NAME']);
                                $scheduleDays = $skypeConsultation->getSchedule()->getDays();
                                
                            ?>
                            <div class="skype-consult__item">
                                <div class="skype-consult__doctor clearfix">
                                    <a href="<?php echo $arDoctor['DETAIL_PAGE_URL']; ?>" class="skype-consult__avatar b-doctor_avatar"><img alt="<?php echo $doctorName; ?>" src="<?php echo $doctorImageSrc; ?>"></a>
                                    <a href="<?php echo $arDoctor['DETAIL_PAGE_URL']; ?>" class="skype-consult__name"> <?php echo $doctorName; ?></a>
                                    <?php echo implode(', ', $arSpecialityNames); ?>
                                </div>
                                <a href="skype:<?php echo $skypeConsultation->getNumber(); ?>?call" class="skype-consult-num"><?php echo $skypeConsultation->getNumber(); ?></a>
                                <table cellspacing="0" cellpadding="0" class="skype-consult__schedule">
                                    <?php foreach($scheduleDays as $day): ?>
                                        <?php if (!empty($day->raw)): ?>
                                        <tr>
                                            <td>
                                                <?php echo $day->name; ?>
                                            </td>
                                            <td>
                                                <?php echo $day->raw; ?>
                                            </td>
                                        </tr>
                                        <?php endif;?>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="skype-consult__note">Ответ специалиста не является <br>медицинской консультацией!</div>
                </div>
            <?php endif; ?>
            <?php if ($showOmsBanner): ?>
                 <?php if ($arResult['CLINIC']): ?>
                    <a href="<?php echo $arResult['CLINIC']['DETAIL_PAGE_URL'] ?>oms/" class="banner-oms">
                        <span class="banner-oms__head">ЭКО по ОМС</span>
                        <span class="banner-oms__clinics">
                            <span class="banner-oms__clinics-list">
                                в <?php echo $arResult['CLINIC']['PROPERTY_NAME_PREPOSITIONAL_VALUE']; ?>
                            </span>
                        </span>
                    </a>
                 <?php else: ?>
                    <?php if ($omsClinics): ?>
                        <div class="banner-oms">
                            <div class="banner-oms__head">ЭКО по ОМС</div>
                            <div class="banner-oms__clinics">
                                <div class="banner-oms__clinics-show js-show-oms-clinics">список клиник</div>
                                <div class="banner-oms__clinics-list banner-oms__clinics-list--hidden js-list-oms-clinics">
                                    <?php foreach ($omsClinics as $arClinic): ?>
                                        <a href="<?php echo $arClinic['DETAIL_PAGE_URL']; ?>oms/"><?php echo $arClinic['NAME']; ?></a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                 <?php endif; ?>
            <?php endif; ?>
        </div>
            <?php echo $arResult['SERVICE']['DESCRIPTION']; ?>
        </div>
        <div class="js-service-text-cut-more align-center" style="display:none"><span class="in-bl b-link_more">Читать дальше</span></div>
        <div class="js-service-text-cut-less align-center" style="display:none"><span class="in-bl b-link_more">Скрыть полное описание</span></div>
    </div>
    <div class="b-service_appointment__wrap">
        <div class="in-bl b-service_appointment__head">Задать вопрос или<br>записаться на прием</div>
        <div class="in-bl b-service_appointment__phone"><?php if ($phone): ?><span class="js-phone"><?php echo $phone; ?></span> <br><?php endif; ?><div class="b-service_appointment__call in-bl js-show-call-me js-goal" data-clinic-id="<?php echo $arResult['CLINIC']['ID']; ?>" data-service-ids="<?php echo htmlspecialchars(json_encode($arResult['SERVICE']['SERVICE_IDS'] ?: [])); ?>" data-target="CallBack<?php echo $arResult['CLINIC']['PROPERTY_METRIC_ID_VALUE'] ? '_'.$arResult['CLINIC']['PROPERTY_METRIC_ID_VALUE'] : ''; ?>">Перезвоните мне</div></div>
        <div class="b-btn_white in-bl link_ask_direction" data-service="<?php echo $arResult['SERVICE']['ID']; ?>">Задать вопрос</div>
        <div class="b-btn_white in-bl link_reception_clinic js-goal" href="#" data-clinic="<?php echo $arResult['CLINIC']['ID']; ?>" data-target="Appointment<?php echo $arResult['CLINIC']['PROPERTY_METRIC_ID_VALUE'] ? '_'.$arResult['CLINIC']['PROPERTY_METRIC_ID_VALUE'] : ''; ?>">Записаться на прием</div>
    </div>
</div>