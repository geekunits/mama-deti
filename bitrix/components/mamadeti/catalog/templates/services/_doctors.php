<?php
global $arrFilterClinicDoctors;
$arrFilterClinicDoctors = ['PROPERTY_SERVICES' => $arResult['SERVICE']['SERVICE_IDS']];
if ($arResult['CLINIC']['ID']) {
    $clinicIds = $arResult['CLINIC']['ID'];
} else {
    $clinicIds = CMamaDetiAPI::getClinicsID(array(
        'PROPERTY_CITY' => $GLOBALS['CITY_ID'],
    ));
}
$arrFilterClinicDoctors['PROPERTY_CLINIC'] = $clinicIds;
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "clinic-doctors-outside",
    Array(
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => DOCTOR_IBLOCK_ID,
        "NEWS_COUNT" => "100",
        "SORT_BY1" => "RAND",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "PREVIEW_PICTURE",
        "SORT_ORDER2" => "DESC",
        "FILTER_NAME" => "arrFilterClinicDoctors",
        "FIELD_CODE" => array(),
        "PROPERTY_CODE" => array('SERVICES','CLINIC','CATEGORY','POST_FULL', 'SPECIALTY', 'CITY', 'CLINIC', 'SEX', 'POST'),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "0",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "SERVICE" => $arResult['SERVICE'],
        'CLINIC_ID' => $clinicIds,
    ),
$component
);
