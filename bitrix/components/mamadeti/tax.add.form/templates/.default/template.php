<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<script type="text/html" id="formDate-template">
	<div class="row p-t">
		<span class="btn close"></span>
		<div class="label-block">
			<span class="middle">Оказание услуги с <span class="starrequired">*</span></span>
		</div>
		<div class="row-sector date">
			<div class="text04">
				<input type="text" name="PROPERTY[155][#index#][VALUE]" size="25" value="">
				<img
					src="/bitrix/js/main/core/images/calendar-icon.gif"
					alt="Выбрать дату в календаре"
					class="calendar-icon"
					onclick="BX.calendar({node:this, field:'PROPERTY[155][#index#][VALUE]', form: 'iblock_add', bTime: false, currentTime: '1432041754', bHideTime: false});"
					onmouseover="BX.addClass(this, 'calendar-icon-hover');" onmouseout="BX.removeClass(this, 'calendar-icon-hover');"
					border="0">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="label-block">
			<span class="middle">Оказание услуги по <span class="starrequired">*</span></span>
		</div>
		<div class="row-sector date">
			<div class="text04">
				<input type="text" name="PROPERTY[156][#index1#][VALUE]" size="25" value="">
					<img
						src="/bitrix/js/main/core/images/calendar-icon.gif"
						alt="Выбрать дату в календаре"
						class="calendar-icon"
						onclick="BX.calendar({node:this, field:'PROPERTY[156][#index1#][VALUE]', form: 'iblock_add', bTime: false, currentTime: '1432041754', bHideTime: false});"
						onmouseover="BX.addClass(this, 'calendar-icon-hover');"
						onmouseout="BX.removeClass(this, 'calendar-icon-hover');" border="0">
			</div>
		</div>
	</div>
</script>
<script type="text/html" id="formFile-template">
	<div class="row p-t">
		<span class="btn close"></span>
		<div class="label-block">
			<span class="middle">Приложить необходимые документы</span>
		</div>
		<div class="row-sector">
			<input type="hidden" name="PROPERTY[162][#index#]" value="" />
			<input type="file" size="30"  name="PROPERTY_FILE_162_#index#" /><br />
		</div>
	</div>
</script>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
/*$formField = array(
'146',
'147',
'148',
'149',
'150',
'151',
'152',
'153',
'154',
'155',
'156',
'157',
'158',
'159',
'160',
'161',
'162',
'163',
'164',
'165',
'166'
);*/
function printDateInput($propertyID,$i,$arParams,$arResult){
	if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
	{
		$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
		$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
	}
	elseif ($i == 0)
	{
		$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
		$description = "";
	}
	else
	{
		$value = "";
		$description = "";
	}
	echo call_user_func_array('GetCustomPublicEditHTML',
		array(
			$arResult["PROPERTY_LIST_FULL"][$propertyID],
			array(
				"VALUE" => $value,
				"DESCRIPTION" => $description,
			),
			array(
				"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
				"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
				"FORM_NAME"=>"iblock_add",
			),
		));
}
$title = array(
	146 => 'ФИО налогоплательщика',
	149 => 'Данные пациента',
	155 => 'Год оказания услуги',
	157 => 'Необходимые данные',
	163 => 'Контактная информация',
);

asort($arResult["PROPERTY_LIST"]);
if (count($arResult["ERRORS"])):?>
	<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

	<?=bitrix_sessid_post()?>

	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
	<div class="form data">
		<div class="form-rows">
			<?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):?>
				<?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>
					<?
					if($propertyID == 156) continue;
					$isRecieverField = in_array($propertyID,array(165,166));
					if(in_array($propertyID,array_keys($title))):?>
						<div class="row">
							<div class="title">
								<h3><?=$title[$propertyID]?></h3>
							</div>
						</div>
					<?endif;?>
					<?if($propertyID == 165):?>
						<div class="row">
							<p>Как вы хотите поулчить документы?</p>
							<div class="bl">
								<div class="in-bl">
									<input type="radio" name="reciever" value="clinic" id="rt1"> <label for="rt1" class="label">Получить в клинике</label>
								</div>
								<div class="in-bl">
									<input type="radio" name="reciever" value="post" id="rt2"> <label for="rt2" class="label">Получить почтой</label>
								</div>
							</div>
						</div>
					<?endif;?>
					<div class="row<?=$isRecieverField ? ' hidden' : ''?>">
						<div class="label-block">
							<span class="middle">
								<?if (intval($propertyID) > 0):?><?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?><?else:?><?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID)?><?endif?>
								<?if(in_array($propertyID, $arResult["PROPERTY_REQUIRED"])):?><span class="starrequired">*</span><?endif?>
							</span>
						</div>
						<div class="row-sector<?=in_array($propertyID,array(155,156,153)) ? ' date': ''?>">
							<?
							//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"]); echo "</pre>";
							if (intval($propertyID) > 0)
							{
								if (
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
									&&
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
								)
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
								elseif (
									(
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
										||
										$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
									)
									&&
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
								)
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
							}
							elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

							if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
							{
								//var_dump($arResult["ELEMENT_PROPERTIES"][$propertyID],$arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"]);
								$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
								$inputNum += $inputNum == 0 ? $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"] : 0;
							}
							else
							{
								$inputNum = 1;
							}

							if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
								$INPUT_TYPE = "USER_TYPE";
							else
								$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];
							switch ($INPUT_TYPE):
								case "USER_TYPE":
									if(in_array($propertyID,array(155))){
										for ($i = 0; $i < $inputNum; $i++)
										{
											if($i !=0):?>
											<div class="row p-t">
												<span class="btn close"></span>
												<div class="label-block">
													<span class="middle">Оказание услуги c <span class="starrequired">*</span></span>
												</div>
												<div class="row-sector date">
											<?endif;
											printDateInput($propertyID,$i,$arParams,$arResult);?>
											</div></div>
											<div class="row">
												<div class="label-block">
													<span class="middle">Оказание услуги по <span class="starrequired">*</span></span>
												</div>
												<div class="row-sector date">
												<?printDateInput(156,$i,$arParams,$arResult);
												if($i+1 != $inputNum):?>
												</div></div>
												<?endif;/*if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
												$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
												$description = "";
											}
											else
											{
												$value = "";
												$description = "";
											}
											echo call_user_func_array('GetCustomPublicEditHTML',
												array(
													$arResult["PROPERTY_LIST_FULL"][$propertyID],
													array(
														"VALUE" => $value,
														"DESCRIPTION" => $description,
													),
													array(
														"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
														"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
														"FORM_NAME"=>"iblock_add",
													),
												));*/
										}
									} else {
										for ($i = 0; $i<$inputNum; $i++)
										{
											if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
											{
												$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
												$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
											}
											elseif ($i == 0)
											{
												$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
												$description = "";
											}
											else
											{
												$value = "";
												$description = "";
											}
											echo call_user_func_array('GetCustomPublicEditHTML',
												array(
													$arResult["PROPERTY_LIST_FULL"][$propertyID],
													array(
														"VALUE" => $value,
														"DESCRIPTION" => $description,
													),
													array(
														"VALUE" => "PROPERTY[".$propertyID."][".$i."][VALUE]",
														"DESCRIPTION" => "PROPERTY[".$propertyID."][".$i."][DESCRIPTION]",
														"FORM_NAME"=>"iblock_add",
													),
												));
										?><?
										}
									}
										if(in_array($propertyID,array(155))):?>
											<br/>
											</div>
											</div>
											<div class="row">
												<div class="row-sector">
												<span class="button01 block-button">
													<input type="button" class="add-more" value="Добавить">
													Добавить
												</span>
										<?endif;
								break;
								case "TAGS":
									$APPLICATION->IncludeComponent(
										"bitrix:search.tags.input",
										"",
										array(
											"VALUE" => $arResult["ELEMENT"][$propertyID],
											"NAME" => "PROPERTY[".$propertyID."][0]",
											"TEXT" => 'size="'.$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"].'"',
										), null, array("HIDE_ICONS"=>"Y")
									);
									break;
								case "HTML":
									$LHE = new CLightHTMLEditor;
									$LHE->Show(array(
										'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[".$propertyID."][0]"),
										'width' => '100%',
										'height' => '200px',
										'inputName' => "PROPERTY[".$propertyID."][0]",
										'content' => $arResult["ELEMENT"][$propertyID],
										'bUseFileDialogs' => false,
										'bFloatingToolbar' => false,
										'bArisingToolbar' => false,
										'toolbarConfig' => array(
											'Bold', 'Italic', 'Underline', 'RemoveFormat',
											'CreateLink', 'DeleteLink', 'Image', 'Video',
											'BackColor', 'ForeColor',
											'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
											'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
											'StyleList', 'HeaderList',
											'FontList', 'FontSizeList',
										),
									));
									break;
								case "T":
									for ($i = 0; $i<$inputNum; $i++)
									{

										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
										{
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
										}
										elseif ($i == 0)
										{
											$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										}
										else
										{
											$value = "";
										}
									?>
							<textarea cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
									<?
									}
								break;
								case "E":?>
									<select name="PROPERTY[<?=$propertyID?>]">
										<option value="">Выберите клинику</option>
										<?
										$clinicList = array_unique(CMamaDetiAPI::GetClinics(array('PROPERTY_CITY' => $GLOBALS["CURRENT_CITY"])));
										foreach ($clinicList as $id => $clinic):?>
										<option value="<?=$clinic['ID']?>"><?=$clinic['NAME']?></option>
										<?endforeach;?>
									</select>
								<?break;
								case "S":
								case "N":
									for ($i = 0; $i<$inputNum; $i++)
									{
										if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
										{
											$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
										}
										elseif ($i == 0)
										{
											$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

										}
										else
										{
											$value = "";
										}
									?>
									<div class="text04">
										<input type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value?>" />
									</div>
										<?
									if($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
										$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => 'iblock_add',
												'INPUT_NAME' => "PROPERTY[".$propertyID."][".$i."]",
												'INPUT_VALUE' => $value,
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);
										?><small><?=GetMessage("IBLOCK_FORM_DATE_FORMAT")?><?=FORMAT_DATETIME?></small><?
									endif
									?><?
									}
								break;

								case "F":
									for ($i = 0; $i<$inputNum; $i++)
									{
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
										?>
							<input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" value="<?=$value?>" />
							<input type="file" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"  name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>" /><br />
										<?

										if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
										{
											?>
						<input type="checkbox" name="DELETE_FILE[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]" id="file_delete_<?=$propertyID?>_<?=$i?>" value="Y" /><label for="file_delete_<?=$propertyID?>_<?=$i?>"><?=GetMessage("IBLOCK_FORM_FILE_DELETE")?></label><br />
											<?

											if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"])
											{
												?>
						<img src="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>" height="<?=$arResult["ELEMENT_FILES"][$value]["HEIGHT"]?>" width="<?=$arResult["ELEMENT_FILES"][$value]["WIDTH"]?>" border="0" /><br />
												<?
											}
											else
											{
												?>
						<?=GetMessage("IBLOCK_FORM_FILE_NAME")?>: <?=$arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"]?><br />
						<?=GetMessage("IBLOCK_FORM_FILE_SIZE")?>: <?=$arResult["ELEMENT_FILES"][$value]["FILE_SIZE"]?> b<br />
						[<a href="<?=$arResult["ELEMENT_FILES"][$value]["SRC"]?>"><?=GetMessage("IBLOCK_FORM_FILE_DOWNLOAD")?></a>]<br />
												<?
											}
										}
									}?>

								</div>
								</div>
								<div class="row">
									<div class="row-sector">
									<span class="button01 block-button">
										<input type="button" class="add-more" value="Добавить">
										Добавить
									</span>
								<?break;
								case "L":

									if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
										$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
									else
										$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

									switch ($type):
										case "checkbox":
										case "radio":

											//echo "<pre>"; print_r($arResult["PROPERTY_LIST_FULL"][$propertyID]); echo "</pre>";

											foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
											{
												$checked = false;
												if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
												{
													if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
													{
														foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
														{
															if ($arElEnum["VALUE"] == $key) {$checked = true; break;}
														}
													}
												}
												else
												{
													if ($arEnum["DEF"] == "Y") $checked = true;
												}

												?>
												<input type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> /><label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label><br />
												<?
											}
										break;

										case "dropdown":
										case "multiselect":
										?>
								<select name="PROPERTY[<?=$propertyID?>]<?=$type=="multiselect" ? "[]\" size=\"".$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]."\" multiple=\"multiple" : ""?>">
									<option value=""><?echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
										<?
											if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
											else $sKey = "ELEMENT";

											foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
											{
												$checked = false;
												if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
												{
													foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum)
													{
														if ($key == $arElEnum["VALUE"]) {$checked = true; break;}
													}
												}
												else
												{
													if ($arEnum["DEF"] == "Y") $checked = true;
												}
												?>
									<option value="<?=$key?>" <?=$checked ? " selected=\"selected\"" : ""?>><?=$arEnum["VALUE"]?></option>
												<?
											}
										?>
								</select>
										<?
										break;

									endswitch;
								break;
							endswitch;?>
						</div>
					</div>
				<?endforeach;?>
				<?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
					<div class="row">
						<div class="label-block"><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></div>
						<div class="row-sector">
							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
						</div>
					</div>
					<div class="row">
						<div class="label-block"><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</div>
						<div class="row-sector"><input type="text" name="captcha_word" maxlength="50" value=""></div>
					</div>
				<?endif?>
			<?endif?>
			<div class="row">
				<div class="label-block"><br/></div>
				<div class="row-sector">
					<span class="button01 block-button">
					<input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
						<?=GetMessage("IBLOCK_FORM_SUBMIT")?>
				</span>
					<?if (strlen($arParams["LIST_URL"]) > 0 && $arParams["ID"] > 0):?>
						<span class="button01 block-button">
						<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
							<?=GetMessage("IBLOCK_FORM_APPLY")?>
					</span>
					<?endif?>
					<?/*<input type="reset" value="<?=GetMessage("IBLOCK_FORM_RESET")?>" />*/?>
				</div>
			</div>
		</div>
	</div>
	<?if (strlen($arParams["LIST_URL"]) > 0):?><a href="<?=$arParams["LIST_URL"]?>"><?=GetMessage("IBLOCK_FORM_BACK")?></a><?endif?>
</form>