$(function(){
	var forms = {
		'date' : $('script#formDate-template').html(),
		'file' : $('script#formFile-template').html()
	};

	$('#rt1,#rt2').change(function(e){
		$wrapper = $(this).closest('.row');
		$wrapper.next().addClass('hidden').next().addClass('hidden');
		if(this.value == 'clinic'){
			$wrapper.next().removeClass('hidden');
		} else {
			$wrapper.next().next().removeClass('hidden');
		}
	});

	$('.add-more').click(function(){
		$this = $(this);
		$containerPrev = $this.closest('.row').prev();
		if($containerPrev.find('[name^="PROPERTY[156]"]').length){
			var config = {
				'names' :  ['[name^="PROPERTY[156]"]', /6\]\[(\d*)\]/],
				'type' : 'date'
			};
		} else if($containerPrev.find('[type="file"]').length) {
			var config = {
				'names' : ['[type="file"]', /162_(\d*)/],
				'type' : 'file'
			};
		}
		var index = parseInt($containerPrev.find(config.names[0]).attr('name').match(config.names[1])[1]);
		var section = forms[config.type].replace(/#index#/g,index+1);
		if(config.type == 'date') section = section.replace(/#index1#/g,index+2);
		$containerPrev.next().before(section);
		if(config.type == 'file') $containerPrev.next().find('input').styler();
	});
	$('.content-sector').on('click','.row .close',function(e){
		var $container = $(this).closest('.row');
		if($container.find('[type="file"]').length == 0){
			$container.next().remove();
		}
		$container.remove();
	});
});