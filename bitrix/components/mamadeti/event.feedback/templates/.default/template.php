<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<a href="" name="feedback" id="feedback"></a>
<div class="mfeedback">
<?if (!empty($arResult["ERROR_MESSAGE"])) {
    foreach($arResult["ERROR_MESSAGE"] as $v)
        ShowError($v);
}
if (strlen($arResult["OK_MESSAGE"]) > 0) {
    ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?php
}?>
<form action="<?=POST_FORM_ACTION_URI?>#feedback" method="POST">
<?=bitrix_sessid_post()?>
    <div class="mf-name">
        <div class="mf-text">
            <?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        </div>
        <div class="text04">
            <input type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
        </div>
    </div>
    <div class="mf-name">
        <?=GetMessage("MFT_PHONE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        <div class="text04">
            <input type="text" name="user_phone" value="<?=$arResult["AUTHOR_PHONE"]?>">
        </div>
    </div>
    <div class="mf-email">
        <?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        <div class="text04">
            <input type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
        </div>
    </div>
    <div class="mf-message">
        <?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
        <div class="textarea01">
            <textarea name="MESSAGE" rows="5" cols="40"><?=$arResult["MESSAGE"]?></textarea>
        </div>
    </div>

    <?if($arParams["USE_CAPTCHA"] == "Y"):?>
    <div class="mf-captcha">
        <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
        <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
        <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
        <div class="text04"><input type="text" name="captcha_word" size="30" maxlength="50" value=""></div>
    </div>
    <?endif;?>
    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
            <!--<input type="submit" name="vote" style="border:none;" class="button01" value="Голосовать">-->
            <input type="submit" name="submit" style="border:none;" class="button01" value="<?=GetMessage("MFT_SUBMIT")?>">
</form>
</div>
<script>
    $(function() {
        $('input[name=user_phone]').mask('8(999) 999-99-99', {
            placehloder: '8(___) ___-__-__'
        });
    });
</script>
