<?php
if (!function_exists('createMenuArray')){
    function createMenuArray(&$res,&$menuItems,$arParent,$depthLevel){
        uasort($arParent, function($a, $b){
            if ($a['SORT'] == $b['SORT']) {
                return 0;
            }

            return ($a['SORT'] < $b['SORT']) ? -1 : 1;
        });

        foreach($arParent as $item){
            $isParent = ($item['IS_SECTION']&&isset($menuItems[$item['ID']]));
            $res[] = array(
                htmlspecialchars($item['~NAME']),
                $item['LINK'],
                array(), //массив доп ссылок
                array(
                    'FROM_IBLOCK' => true,
                    'IS_PARENT' => $isParent,
                    'DEPTH_LEVEL' => $depthLevel,
                    'SORT' =>  $item ['SORT'],
                ),
            );
            if ($isParent){
                createMenuArray($res,$menuItems,$menuItems[$item['ID']],$depthLevel+1);
            }
        }
    }
}

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 36000000;
}

$arParams["ID"] = intval($arParams["ID"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams['URL_SUFFIX'] = isset($arParams['URL_SUFFIX']) ? $arParams['URL_SUFFIX'] : '';

$arParams["DEPTH_LEVEL"] = intval($arParams["DEPTH_LEVEL"]);

if ($arParams["DEPTH_LEVEL"] <= 0) {
    $arParams["DEPTH_LEVEL"] = 1;
}

$i = $arParams['DEPTH_LEVEL'];

if (strlen($arParams["SECTION_FILTER_NAME"]) <= 0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["SECTION_FILTER_NAME"])) {
    $arrSectionFilter = array();
} else {
    global ${$arParams["SECTION_FILTER_NAME"]};
    $arrSectionFilter = ${$arParams["SECTION_FILTER_NAME"]};
    if (!is_array($arrSectionFilter)) {
        $arrSectionFilter = array();
    }
}

if (strlen($arParams["ELEMENT_FILTER_NAME"]) <= 0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["ELEMENT_FILTER_NAME"])) {
    $arrElementFilter = array();
} else {
    global ${$arParams["ELEMENT_FILTER_NAME"]};
    $arrElementFilter = ${$arParams["ELEMENT_FILTER_NAME"]};
    if (!is_array($arrElementFilter)) {
        $arrElementFilter = array();
    }
}

$arResult["SECTIONS"] = array();
$arResult["ELEMENT_LINKS"] = array();

if ($this->StartResultCache($arParams["CACHE_TIME"], serialize(array($arrSectionFilter, $arrElementFilter, $arParams['URL_SUFFIX'])))) {
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
    } else {
        $arFilter = array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "GLOBAL_ACTIVE" => "Y",
            "IBLOCK_ACTIVE" => "Y",
            "<="."DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
        );

        $arFilter = array_merge($arFilter, $arrSectionFilter);

        $arOrder = array(
            "left_margin" => "asc",
        );

        /************************************/
        $arSectionId = array();
        $rsSections = CIBlockSection::GetList($arOrder, $arFilter, false, array(
            'ID',
            'DEPTH_LEVEL',
            'NAME',
            'SECTION_PAGE_URL',
            'IBLOCK_SECTION_ID',
            'SORT',
        ));

        if ($arParams["IS_SEF"] !== "Y") {
            $rsSections->SetUrlTemplates("", $arParams["SECTION_URL"]);
        } else {
            $rsSections->SetUrlTemplates("", $arParams["SEF_BASE_URL"].$arParams["SECTION_PAGE_URL"]);
        }

        $menuItems = array();

        while($arSection = $rsSections->GetNext()){
            $arSection['IS_SECTION'] = 1;
            $arSection['SECTION_PAGE_URL'].=$arParams['URL_SUFFIX'];
            $arSection['LINK'] = $arSection['SECTION_PAGE_URL'];

            if ($arSection['IBLOCK_SECTION_ID']){
                $menuItems[$arSection['IBLOCK_SECTION_ID']][] = $arSection;
            } else {
                $menuItems['ROOT'][] = $arSection;
            }
            $arSectionId[] = $arSection['ID'];
        }

        //In "SEF" mode we'll try to parse URL and get ELEMENT_ID from it
        if ($arParams["IS_SEF"] === "Y") {
            $componentPage = CComponentEngine::ParseComponentPath(
                $arParams["SEF_BASE_URL"],
                array(
                    "section" => $arParams["SECTION_PAGE_URL"],
                    "detail" => $arParams["DETAIL_PAGE_URL"],
                ),
                $arVariables
            );
            if ($componentPage === "detail") {
                CComponentEngine::InitComponentVariables(
                    $componentPage,
                    array("SECTION_ID", "ELEMENT_ID"),
                    array(
                        "section" => array("SECTION_ID" => "SECTION_ID"),
                        "detail" => array("SECTION_ID" => "SECTION_ID", "ELEMENT_ID" => "ELEMENT_ID"),
                    ),
                    $arVariables
                );
                $arParams["ID"] = intval($arVariables["ELEMENT_ID"]);
            }
        }

        //Получим элементы
        $i++;

        $arSelect = array("ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "PROPERTY_URL", "SORT");

        $arFilter = Array(
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'ACTIVE' => 'Y',
        );

        $arFilter = array_merge($arFilter, $arrElementFilter);

        $rsElements = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);

        if (($arParams["IS_SEF"] === "Y") && (strlen($arParams["DETAIL_PAGE_URL"]) > 0)) {
            $rsElements->SetUrlTemplates($arParams["SEF_BASE_URL"].$arParams["DETAIL_PAGE_URL"]);
        }

        while ($arElement = $rsElements->GetNext()) {
            if (!empty($arElement["PROPERTY_URL_VALUE"])) {
                $arElement["DETAIL_PAGE_URL"] = $arElement["PROPERTY_URL_VALUE"];
                $arElement["~DETAIL_PAGE_URL"] = $arElement["~PROPERTY_URL_VALUE"];
            }

            $arElement["DETAIL_PAGE_URL"].= $arParams['URL_SUFFIX'];
            $arElement["~DETAIL_PAGE_URL"].= $arParams['URL_SUFFIX'];

            $arElement['IS_SECTION'] = 0;
            $arElement['LINK'] = $arElement['DETAIL_PAGE_URL'];

            if ($arElement['IBLOCK_SECTION_ID']){
                $menuItems[$arElement['IBLOCK_SECTION_ID']][] = $arElement;
            } else {
                // добавялет елементы на одном уровне с разделами инфоблока
                $menuItems['ROOT'][] = $arElement;
            }
        }

        //Рекурсивно сформируем итоговый массив для меню
        $arResult = array();
        //echo "Рекурсивно сформируем итоговый массив для меню".'<br>';
        createMenuArray($arResult,$menuItems,$menuItems['ROOT'],1);
        /************************************/

        $this->EndResultCache();
    }
}

return $arResult;
