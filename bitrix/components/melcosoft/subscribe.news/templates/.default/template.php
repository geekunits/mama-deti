<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
defined('SITE_TEMPLATE_PATH') || define('SITE_TEMPLATE_PATH', '/bitrix/templates/mamadeti');
?>
<style type="text/css">
    body {
        font-family: 'PT Sans', Arial, sans-serif;
        text-align: left;
        margin: 0;
    }

    body, td, th {
        font-size: 15px;
        line-height: 22px;
        font-family: 'PT Sans', Arial, sans-serif;
    }
</style>
<table bgcolor="#e3e3e3" border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: 'PT Sans', Arial, sans-serif; line-height: 22px; font-size: 15px; color:#000; background: #e3e3e3;">
    <tr>
        <td height="20" width="20" colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td width="20" height="20">&nbsp;</td>
        <td align="center" width="100%">
            <table border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#ffffff" style="background: #ffff;">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="600">
                            <tr>
                                <td height="10" colspan="2">&nbsp;</td>
                            </tr>
                            <tr valign="middle">
                                <td width="20" height="20">&nbsp;</td>
                                <td>
                                    <a href="http://<?=$arResult["SERVER_NAME"]?>/"><img src="http://<?=$arResult["SERVER_NAME"].SITE_TEMPLATE_PATH?>/images/subscribe/logo-subscribe.jpg?2" alt="" style="display:block" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td height="10" colspan="2">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="border-top:1px solid #eeeeee; line-height:1px;" height="1">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="600">
                            <tr>
                                <td width="20" height="20">&nbsp;</td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="540">
										<?

                                        $cm = CityManager::getInstance();
                                        $arCities = $cm->getCities();

										foreach($arResult["IBLOCKS"] as $arIBlock):
											if(count($arIBlock["ITEMS"]) > 0):
                                                /*заполняем массив клиника -> город*/
                                                if($arIBlock["ID"] == NEWS_IBLOCK_ID){
                                                    $arClinicCity = array();

                                                    $arClinicIDs = array();

                                                    foreach($arIBlock["ITEMS"] AS $arItem){
                                                        if(empty($arItem['PROPERTIES']['GLOBAL']['VALUE']) && !empty($arItem['PROPERTIES']['CLINIC']['VALUE'])){//если не глобальная новость, то выведем город
                                                            if(is_array($arItem['PROPERTIES']['CLINIC']['VALUE'])){
                                                                foreach($arItem['PROPERTIES']['CLINIC']['VALUE'] AS $clinicID){
                                                                    $arClinicIDs[] = $clinicID;
                                                                }
                                                            }
                                                        }
                                                    }

                                                    //получим клиники и у них город
                                                    $arClinics = CMamaDetiAPI::getClinics(array('ID' => $arClinicIDs));

                                                    foreach($arClinics AS $arClinic){
                                                        if(!empty($arClinic['PROPERTIES']['CITY']['VALUE'])){
                                                            $cityID = $arClinic['PROPERTIES']['CITY']['VALUE'];

                                                            $arClinicCity[$arClinic['ID']] = $arCities[$cityID];
                                                        }
                                                    }
                                                }
										?>
                                            <tr>
                                                <td width="20" height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><span style="font-weight: bold; margin:0; font-size: 29px; font-family: 'PT Sans', Arial, sans-serif;"><?=$arIBlock['NAME']?></span></td>
                                            </tr>
										<?
											foreach($arIBlock["ITEMS"] as $arItem):
										?>
											<tr>
                                                <td width="20" height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="560">
                                                        <tr valign="top">
                                                            <td width="155"<?if(!$arItem["PREVIEW_PICTURE"]){ ?> valign="middle"<? } ?> height="110" align="center" style="padding-top:5px;">
                                                                <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                                                            	<?
                                                                    if($arItem["PREVIEW_PICTURE"]){
                                                                        $arPicture = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], array(
                                                                            'width'  => 155,
                                                                            'height' => 110
                                                                        ), BX_RESIZE_IMAGE_EXACT, true);
                                                                        ?>
                                                                            <img style="display:block; border:0;" align='left' border='0' src="http://<?=$arResult["SERVER_NAME"] . $arPicture['src'];?>" alt="<?echo $arItem["NAME"]?>" title="<?echo $arItem["NAME"]?>">
                                                                        <?
                                                                    } else {
                                                                ?>
                                                                    <img src="http://<?=$arResult["SERVER_NAME"].SITE_TEMPLATE_PATH?>/images/subscribe/logo-subscribe.jpg?2" alt="" style="display:block" width="130" />
                                                                <? 
                                                                    }
                                                                ?>
                                                                </a>
                                                            </td>
                                                            <td width="15">&nbsp;</td>
                                                            <td>
                                                                <div style="margin: 0 0 3px 0; font-size: 13px; font-family: 'PT Sans', Arial, sans-serif;">
                                                                    <?php
                                                                        if($arIBlock["ID"] == NEWS_IBLOCK_ID && empty($arItem['PROPERTIES']['GLOBAL']['VALUE']) ){
                                                                            $clinicID = $arItem['PROPERTIES']['CLINIC']['VALUE'];

                                                                            if(is_array($clinicID)){
                                                                                $clinicID = reset($clinicID);
                                                                            }

                                                                            if(isset($arClinicCity[$clinicID])){
                                                                                echo $arClinicCity[$clinicID]['NAME'] . '  ';
                                                                            }
                                                                        }
                                                                    ?>
																	<?if(strlen($arItem["DATE_ACTIVE_FROM"])>0):?>
																		<span style="color:#929292; font-family: 'PT Sans', Arial, sans-serif;"><?echo $arItem["DATE_ACTIVE_FROM"]?></span>
																	<?endif;?>
																</div>
                                                                <div style="margin: 0 0 8px 0;"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" style="text-decoration:none; font-size:20px; color:#931368; font-family: 'PT Sans', Arial, sans-serif;"><?echo $arItem["NAME"]?></a></div>
                                                                <div style="color:#000 !important; font-family: 'PT Sans', Arial, sans-serif !important;"><?echo $arItem["PREVIEW_TEXT"];?></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20" height="20">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="border-top:1px solid #eeeeee; line-height:1px;" height="1">&nbsp;</td>
                                            </tr>
										<?
											endforeach;
											endif;
										?>
										<?endforeach?>
										<tr>
                                            <td width="20" height="20">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#UNSUBSCRIBE_LINK#" style="color:#868686; font-size: 13px; font-family: 'PT Sans', Arial, sans-serif;">Отписаться от рассылки</a></td>
                                        </tr>
                                        <tr>
                                            <td width="20" height="30">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="20" height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td width="20" height="20">&nbsp;</td>
    </tr>
    <tr>
        <td height="20" width="20" colspan="3">&nbsp;</td>
    </tr>
</table>