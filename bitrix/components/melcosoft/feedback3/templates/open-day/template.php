<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?= $arResult["FORM_HEADER"] ?>
<div class="bg">
    &nbsp;
</div>
<div class="popup" style="display:none">
    <div class="popup-sector">
        <div class="popup-frame">
            <a href="#" class="close">close</a>
            <span class="popup-title">Заявка на день открытых дверей</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
            <fieldset>
                <div class="form-block">
                    <div class="form-rows">
                    <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                        <div class="js-form-sent">
                            Ваша заявка получена!
                            <br><br>
                            Наши сотрудники перезвонят Вам в часы работы клиники.
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_NAME"><span class="required-star"></span>Ф.И.О.</label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_NAME" type="text" placeholder="Иван Иванович" value="<?=$arResult["FORM"]["NAME"]?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PHONE"><span class="required-star"></span>Номер телефона</label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_PHONE" type="text" placeholder="Телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PHONE"><span class="required-star"></span>E-mail</label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_EMAIL" type="text" placeholder="E-mail" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="textarea01<?=(in_array("P_COMMENT", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                <textarea name="MS_P_COMMENT" cols="30" rows="10" placeholder="Комментарий"><?=$arResult["FORM"]["P_COMMENT"]?></textarea>
                            </div>
                        </div>

                        <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                            <div class="row">
                                <div class="label-block">
                                    <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                    <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                    <a href="#" onclick="refreshCaptcha(this); return false;">Обновить</a>
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="row align-right">
                            <input type="reset" value="Очистить" class="reset-button">
                            <div class="button-holder01">
                                <span class="button01 block-button"><input type="submit" name="form" value="open-day">Отправить</span>
                            </div>
                        </div>
                    <?endif?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<?= $arResult["FORM_FOOTER"] ?>

<script>
    // $(function () {
    //   setTimeout(function () {$('.popup input, .popup select').styler();  }, 1);
    // });

    $(function() {
        
        $('#open-day-popup .popup').show();
        centrateDiv($('#open-day-popup'));

        $('#open-day-popup .close, #open-day-popup .bg').click(function (e) {
            e.preventDefault();
            $('#open-day-popup').hide();
        });

        // $("#callback-popup .button09").click(function () {
        //     $(".search-drop").css({"margin": "0 0 0 -9999px", "z-index": "auto"});
        //     $(this).closest(".search-sector").find(".search-drop").css({"margin": "0", "z-index": "100"});
        //     $(".scrollpane").jScrollPane({
        //         verticalDragMinHeight: 50,
        //         verticalDragMaxHeight: 50,
        //         showArrows: true
        //     });

        //     return false;
        // });
    });

    $(function() {
        $("#open-day-popup input[name='MS_P_PHONE']").mask("8(999) 999-99-99");
    });

    $(function() {
        if ($('#open-day-popup .js-form-sent').size() > 0) {
            window.reachGoal('SENDDL');
        } 
    });
    
</script>
