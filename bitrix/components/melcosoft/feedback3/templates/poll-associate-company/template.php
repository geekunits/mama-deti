<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?= $arResult["FORM_HEADER"] ?>
    <div class="bg">
        &nbsp;
    </div>
    <div class="popup" style="display:none;">
        <div class="popup-sector">
            <div class="popup-frame">
                <a href="#" class="close">close</a>
                <span class="popup-title">Примите участие в опросе</span>
                <?php if ($arResult["FORM_ERROR"]): ?>
                    <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
                <?php endif ?>
                <fieldset>
                    <div class="form-block">
                        <div class="form-rows">
                        <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                            <div>
                                Спасибо за участие в опросе!
                            </div>
                            <script>
                                $.cookie("poll_associate_company", true, { expires: 30, path: '/' });
                            </script>
                        <?php else: ?>
                            <div style="font-size: 16px; margin-bottom:15px;">Не могли бы Вы, пожалуйста, принять участие в небольшом опросе. Это займет не более двух минут.</div>
                            <div style="font-size: 16px;">Назовите, пожалуйста, ваши ассоциации с брендом «Мать и дитя»</div>
                            <div class="row">
                                <div class="text04<?=(in_array("P_OPTION_1", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_OPTION_1" type="text" id="poll-option-1" value="<?=$arResult["FORM"]["P_OPTION_1"]?>">
                                </div>>
                            </div>
                            <div class="row">
                                <div style="font-size: 16px; margin-bottom: 10px;"<?=(in_array("P_GENDER", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error-txt"' : '');?>>Укажите, пожалуйста, ваш пол <span class="required-star"></span></div>
                                <div style="margin-bottom: 6px;">
                                    <label for="form-review-type-clinic"><input type="radio" name="MS_P_GENDER" value="Мужской" id="poll-type-gender-male"> Мужской</label>
                                </div>
                                <div class="label-margin-bottom">
                                    <label for="form-review-type-doctor"><input type="radio" name="MS_P_GENDER" value="Женский" id="poll-type-gender-female"> Женский</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="button-holder01" style="margin-left:0;">
                                    <span class="button01 block-button"><input type="submit" name="form" value="callback">Отправить</span>
                                </div>
                            </div>
                        <?endif?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>

<?= $arResult["FORM_FOOTER"] ?>

<script>
    $(function () {
      setTimeout(function () {$('.popup input, .popup select').styler();  }, 1);
    });

    $('#poll-associate-company .popup').show();
    centrateDiv($('#poll-associate-company'));        

    $('#poll-associate-company .close, #poll-associate-company .bg').on('click', function(){
        $.cookie("poll_associate_company", true, { expires: 30, path: '/' });
    });

</script>
