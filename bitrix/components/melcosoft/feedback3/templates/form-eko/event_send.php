<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

$userEmailTypeId = 'FORM_ECO_USER_NOTIFY';
// $userEmailTemplateId = 53;

$arSendFields['CLINIC_EMAIL'] = $arParams['_DEFAULT_EMAIL'];
$selectedClinicId = $arResult['FORM']['P_CLINIC'];

$clinic = isset($arParams['_CLINICS'][$clinicId]) ? $arParams['_CLINICS'][$clinicId] : null;

$userFields = [
	'USER_EMAIL' => $arSendFields['P_EMAIL'],
];

if (!empty($clinic)) {
	if (!empty($clinic['email'])) {
		$arSendFields['CLINIC_EMAIL'] = $clinic['email'];
	}
	$arSendFields['P_CLINIC'] = $clinic['name'];

	if (!empty($clinic['address'])) {
		$userFields['CLINIC_ADDRESS'] = $clinic['address'];
	}
}

if (!empty($userFields['USER_EMAIL']) && !empty($userFields['CLINIC_ADDRESS'])) {
	CEvent::Send($userEmailTypeId, SITE_ID, $userFields);
}



// CModule::IncludeModule("iblock");

// $arSendFields["CITY"] = $GLOBALS["CURRENT_CITY"]["~NAME"];
// $arSendFields["CITY_EMAIL"] = $GLOBALS["CURRENT_CITY"]["~EMAIL"];

// if (intval($arResult["FORM"]["P_CLINIC"])>0)
// {
// 	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$arResult["FORM"]["P_CLINIC"]),false,false,array("IBLOCK_ID","ID","PROPERTY_CALLBACK_EMAIL"));
// 	if ($arElement = $rsElement->GetNext())
// 	{
// 		$arSendFields["CLINIC_EMAIL"] = $arElement["~PROPERTY_CALLBACK_EMAIL_VALUE"];
// 	}
// }

// //AddMessage2Log(print_r($arSendFields,true));

?>