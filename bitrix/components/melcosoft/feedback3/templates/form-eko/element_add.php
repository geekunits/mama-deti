<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
$clinicId = $arResult['FORM']['P_CLINIC'];
$arLoadProductArray['PROPERTY_VALUES']['CLINIC'] = isset($arParams['_CLINICS'][$clinicId]) ? $arParams['_CLINICS'][$clinicId]['name'] : 'Unknown';

?>