<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php $formId = 'form-eko'; ?>
<?php echo $arResult["FORM_HEADER"]; ?>
<div class="bg">
    &nbsp;
</div>
<!-- <div class="popup" style="display:none"> -->
<div class="popup">
    <div class="popup-sector">
        <div class="popup-frame">
            <a href="#" class="close">close</a>
            <span class="popup-title">Регистрация на мероприятие</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
            <fieldset>
                <div class="form-block">
                    <div class="form-rows">
                    <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                        <div class="js-form-sent">
                            Ваша заявка получена!
                            <br><br>
                            Наши сотрудники перезвонят Вам в часы работы клиники.
                        </div>
                    <?php else: ?>
                        <?php if (!empty($arResult['REQUEST_CLINIC_TITLE'])): ?>
                            Регистрация в клинику <strong><?php echo $arResult['REQUEST_CLINIC_TITLE']; ?></strong>
                        <?php endif; ?>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_SURNAME">
                                    Фамилия<?php if (in_array("P_SURNAME",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_SURNAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_SURNAME" type="text" placeholder="" value="<?=$arResult["FORM"]["P_SURNAME"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_NAME">
                                    Имя<?php if (in_array("NAME",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_NAME" type="text" placeholder="" value="<?=$arResult["FORM"]["NAME"]?>">
                                </div>
                            </div>
                        </div>

                            <div class="row">
                            <div class="label-block">
                                <label for="MS_P_LASTNAME">
                                    Отчество<?php if (in_array("P_LASTNAME",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_LASTNAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_LASTNAME" type="text" placeholder="" value="<?=$arResult["FORM"]["P_LASTNAME"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_EMAIL">
                                    E-mail<?php if (in_array("P_EMAIL", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_EMAIL" type="text" placeholder="E-mail" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PHONE">
                                    Ваш телефон<?php if (in_array("P_PHONE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_PHONE" class="js-phone-input" type="text" placeholder="Телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_DURATION_OF_INFERTILITY">
                                    Длительность бесплодия в браке<?php if (in_array("P_DURATION_OF_INFERTILITY", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_DURATION_OF_INFERTILITY", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_DURATION_OF_INFERTILITY" type="text" placeholder="" value="<?=$arResult["FORM"]["P_DURATION_OF_INFERTILITY"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_EKO_PROGRAMS">
                                    Программы ЭКО в анамнезе<?php if (in_array("P_EKO_PROGRAMS", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="textarea01<?=(in_array("P_EKO_PROGRAMS", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <textarea cols="30" rows="10" name="MS_P_EKO_PROGRAMS" placeholder=""><?=$arResult["FORM"]["P_EKO_PROGRAMS"]?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                Источник информации<?php if (in_array("P_INFO_SOURCE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                            </div>
                            <div class="row-sector">
                                <?php foreach($arResult['PROPERTY_OPTIONS']['INFO_SOURCE']['OPTIONS'] as $option): ?>
                                    <div>
                                        <label for="MS_P_INFO_SOURCE">
                                            <input type="radio" name="MS_P_INFO_SOURCE" value="<?php echo $option['ID']; ?>"
                                                <?php if ($arResult['FORM']['P_INFO_SOURCE'] === $option['ID'] || (empty($arResult['FORM']['P_INFO_SOURCE']) && $option['DEF'] === 'Y')): ?> checked="checked" <?php endif; ?>
                                            > 
                                            <?php echo $option['VALUE']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                                <div class="text04<?=(in_array("P_INFO_SOURCE_OTHER", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_INFO_SOURCE_OTHER" type="text" placeholder="" value="<?=$arResult["FORM"]["P_INFO_SOURCE_OTHER"]?>">
                                </div>
                            </div>
                        </div>   

                        <?php if (!is_null($arResult['REQUEST_CLINIC_ID'])): ?>
                                <input type="hidden" value="<?php echo $arResult['REQUEST_CLINIC_ID']; ?>" name="MS_P_CLINIC">
                            <?else:?>
                                <div class="row">
                                    <div class="label-block">
                                        <label for="id103">
                                            Выберите клинику<?php if (in_array("P_CLINIC", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                        </label>
                                    </div>
                                    <div class="row-sector">
                                        <select name="MS_P_CLINIC"<?=(in_array("P_CLINIC", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error"' : '');?>>
                                            <?php foreach ($arParams['_CLINICS'] as $clinicId => $clinic):?>
                                                <option value="<?php echo $clinicId; ?>"<?php if ($arResult["FORM"]["P_CLINIC"] == $clinicId):?> selected="selected"<?endif?>><?=$clinic['name']?></option>
                                            <?endforeach?>
                                        </select>
                                    </div>
                                </div>
                            <?php endif; ?>

                        <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                            <div class="row">
                                <div class="label-block">
                                    <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                    <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                </div>
                            </div>
                            <a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
                        <?php endif; ?>

                        <div class="row align-right">
                            <div class="button-holder01">
                                <span class="button01 block-button"><input type="submit" name="form" value="<?php echo $formId; ?>">Отправить</span>
                            </div>
                        </div>
                    <?endif?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<?= $arResult["FORM_FOOTER"] ?>

<script>

    $(function() {
        
        $('#<?php echo $formId; ?>-popup .popup').show();
        centrateDiv($('#<?php echo $formId; ?>-popup'));
        $("#form-eko-popup .js-phone-input").mask("8(999) 999-99-99");

        $('#<?php echo $formId; ?>-popup .close, #<?php echo $formId; ?>-popup .bg').click(function (e) {
            e.preventDefault();
            $('#<?php echo $formId; ?>-popup').hide();
        });
    });
    
</script>
