<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

CModule::IncludeModule('iblock');

//$GLOBALS["CURRENT_CITY"]
$arResult['CLINIC'] = array();
$arResult['DIRECTION'] = array();


$arFilter = array("IBLOCK_ID"=>2,"ACTIVE"=>"Y", "PROPERTY_CITY" => GetCurrentCity());

if($arParams['ASET_P_CLIENT_DOCTOR']){
	$arFilter = array_merge($arFilter,array("ID" => array_keys(CMamaDetiAPI::getDoctorsClinics(array('ID'=>$arParams['ASET_P_CLIENT_DOCTOR'])))));
}
$arService = array();
$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","PROPERTY_SERVICES"));
while ($arElement = $rsElement->GetNext())
{
	$arService = array_merge($arService, array($arElement['PROPERTY_SERVICES_VALUE']));
	$arResult['CLINIC'][$arElement["ID"]] = $arElement;
}

$arFilter = array("IBLOCK_ID"=>1,"ACTIVE"=>"Y","DEPTH_LEVEL"=>1, "ID" => CMamaDetiAPI::getServicesIblockSection($arService));

$rsSection = CIBlockSection::GetList(array("SORT"=>"ASC"),$arFilter,false,array("ID","IBLOCK_ID","NAME"));
while ($arSection = $rsSection->GetNext())
{
	$arResult['DIRECTION'][$arSection["ID"]] = $arSection;
}?>