<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
	$hideClinics = strpos($_SERVER['HTTP_REFERER'], '/clinics/') === false && count($arResult['CLINIC']) != 1  ? false : true;
	$hideDir = strpos($_SERVER['HTTP_REFERER'], '/services/') === false ? false : true;
?>
<?= $arResult["FORM_HEADER"] ?>
	<div class="bg">
		&nbsp;
	</div>
	<div class="popup" style="display:none;">
		<div class="popup-sector">
			<div class="popup-frame">
				<a href="#" class="close">close</a>
				<span class="popup-title">Задать вопрос</span>
            <? if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <? endif ?>
				<form method="post">
					<fieldset>
						<div class="form-block">
							<div class="form-rows">
	                        <? if ($arResult["EVENT_SEND_ID"] > 0): ?>
	                        	<div>
	                            	Благодарим Вас за обращение. Мы ответим Вам в ближайшее время.
	                            </div>
	                        <? else: ?>
	                        	<div style="margin-top:-15px;"><b>Уважаемые пациенты!</b><br><br></div>
								ГК «Мать и дитя» обращает ваше внимание, что рубрика «Вопросы-ответы» функционирует в режиме «открытого доступа».
								<br>При этом контактная информация, предоставленная вами &ndash; адрес электронной почты и телефоны &ndash; является строго конфиденциальной и на сайте не размещается.
	                        	<?if(!$hideDir):?>
								<div class="row">
		                            <div class="label-block">
		                                <label for="id103">Выберите тематику</label>
		                            </div>
		                            <div class="row-sector">
										<select name="MS_P_DIRECTION">
											<?foreach ($arResult["DIRECTION"] as $arDirection):?>
											<option value="<?=$arDirection["ID"]?>"<?if ($arParams['ASET_P_CLIENT_DIRECTION'] ==  $arDirection["ID"]):?> selected="selected"<?endif?>><?=$arDirection["NAME"]?></option>
											<?endforeach?>
										</select>
		                            </div>
								</div>
		                        <?else:?>
									<?php
										$direction = $arParams['ASET_P_CLIENT_DIRECTION'];

										if($sectionServices = CMamaDetiAPI::getServicesIblockSection($arParams['ASET_P_CLIENT_DIRECTION'])){
											$direction = current($sectionServices);
										}
									?>
		                        	<input type="hidden" name="MS_P_DIRECTION" value="<?=$direction;?>">
		                        <?endif;?>
								<?if(!$hideClinics):?>
								<div class="row">
	                            	<div class="label-block">
	                                	<label><span class="required-star"></span> Выберите клинику :</label>
	                            	</div>
	                            	<div class="row-sector">
										<select name="MS_P_CLIENT_CLINIC">
											<?foreach ($arResult["CLINIC"] as $arClinic):?>
												<option value="<?=$arClinic["ID"]?>"<?if ($arResult["FORM"]["P_CLIENT_CLINIC"] ==  $arClinic["ID"]):?> selected="selected"<?endif?>><?=$arClinic["NAME"]?></option>
											<?endforeach?>
										</select>
	                            	</div>
								</div>
								<?else:?>
									<input type="hidden" name="MS_P_CLIENT_CLINIC" value="<?=$arResult["FORM"]["P_CLIENT_CLINIC"] ? : key($arResult["CLINIC"]); ?>">
								<?endif;?>
								<div class="row">
									<div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
										<input type="text" name="MS_NAME" placeholder="Ваше имя*" value="<?=$arResult["FORM"]["NAME"]?>">
									</div>
								</div>
								<div class="row">
									<div class="text04<?=(in_array("P_AGE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
										<input type="text" name="MS_P_AGE" placeholder="Ваш возраст" value="<?=$arResult["FORM"]["P_AGE"]?>">
									</div>
								</div>
								<div class="row">
									<div class="text04<?=(in_array("P_CLIENT_CITY", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
										<input type="text" name="MS_P_CLIENT_CITY" placeholder="Город" value="<?=$arResult["FORM"]["P_CLIENT_CITY"]?>">
									</div>
								</div>
								<div class="row">
									<div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
										<input type="text" name="MS_P_PHONE" placeholder="Ваш телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
									</div>
								</div>
								<div class="row">
									<div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
										<input type="text" name="MS_P_EMAIL" placeholder="E-mail*" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
									</div>
								</div>
								<div class="row">
									<div class="textarea01<?=(in_array("PREVIEW_TEXT", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
										<textarea cols="30" rows="10" name="MS_PREVIEW_TEXT" placeholder="Сообщение*"><?=$arResult["FORM"]["PREVIEW_TEXT"]?></textarea>
									</div>
								</div>
								<? if (isset($arResult["CAPTCHA_IMAGE"])): ?>
								    <div class="row">
								        <div class="label-block">
											<input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
											<img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
								        </div>
								        <div class="row-sector">
								        	<div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
								        </div>
								    </div>
                                	<a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
								<? endif; ?>
								<div class="row align-right">
									<input type="reset" value="Очистить" class="reset-button">
									<div class="button-holder01">
										<span class="button01 block-button ya-event" data-ya-event="ASK"><input type="submit" name="form" value="ask">Отправить</span>
									</div>
								</div>
							<?endif?>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
<?= $arResult["FORM_FOOTER"] ?>

<script>
	$(function() {  
	  setTimeout(function() {$('.popup input, .popup select').styler();  }, 1);
	});

    $('#ask-popup .popup').show();
    centrateDiv($('#ask-popup'));

    $("#ask-popup input[name='MS_P_PHONE']").mask("8(999) 999-99-99");

</script>