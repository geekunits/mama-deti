<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

CModule::IncludeModule("iblock");

$arSendFields["CITY"] = $GLOBALS["CURRENT_CITY"]["~NAME"];
$arSendFields["CITY_EMAIL"] = $GLOBALS["CURRENT_CITY"]["~EMAIL"];

$arDoctorEmail = "";

if (intval($arLoadProductArray["PROPERTY_VALUES"]["DIRECTION"])>0)
{
$rsSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>1,"ID"=>$arLoadProductArray["PROPERTY_VALUES"]["DIRECTION"]),false,array("ID","UF_QA_DOCTOR"));
if ($arSection = $rsSection->Fetch())
{
	if (is_array($arSection["UF_QA_DOCTOR"]) && !empty($arSection["UF_QA_DOCTOR"]))
	{
		$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ID"=>$arSection["UF_QA_DOCTOR"]),false,false,array("ID","PROPERTY_EMAIL"));
		while ($arElement = $rsElement->Fetch())
		{
			if (!empty($arElement["PROPERTY_EMAIL_VALUE"]))
			{
				if (is_array($arElement["PROPERTY_EMAIL_VALUE"]))
				{
					foreach ($arElement["PROPERTY_EMAIL_VALUE"] as $val)
					{
						$val = trim($val);
						if (check_email($val))
							$arDoctorEmail[] = $val;
					}
				} else
				{
					$val = trim($arElement["PROPERTY_EMAIL_VALUE"]);
					if (check_email($val))
						$arDoctorEmail[] = $val;
				}
			}
		}
	}
}
}

$arSendFields["DOCTORS_EMAIL"] = implode(', ',$arDoctorEmail);

/******* SEND TO CLIENT MAIL ******/
$arSendFields["CITY"] = $GLOBALS["CURRENT_CITY"]["~NAME"];
$arSendFields["CITY_EMAIL"] = $GLOBALS["CURRENT_CITY"]["~EMAIL"];

if (intval($arParams["ASET_P_CLINIC"])>0)
{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$arParams["ASET_P_CLINIC"]),false,false,array("IBLOCK_ID","ID","PROPERTY_EMAIL"));
	if ($arElement = $rsElement->GetNext())
	{
		$arSendFields["CLINIC_EMAIL"] = $arElement["~PROPERTY_EMAIL_VALUE"];
	}
}

?>