<?php
$arTemplateParameters['SERVICE_IDS'] = array(
    'PARENT' => 'DATA_SOURCE',
    'NAME' => 'Услуги',
    'TYPE' => 'STRING',
    'REFRESH' => 'Y',
);


$arTemplateParameters['SEND_CLIENT_EMAIL_MESSAGE'] = array(
    "PARENT" => "DATA_SOURCE",
    "NAME" => "Отправить сообщение по указанному email",
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "Y",
);

$arTemplateParameters['CLIENT_EMAIL_EVENT'] = array(
    "PARENT" => "DATA_SOURCE",
    "NAME" => "Событие для отправки клиенту на указанный email",
    "TYPE" => "STRING"
);
