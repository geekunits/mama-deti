<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
$userFields = $arSendFields;

$clinicID = $arResult['FORM']['P_CLINIC'];

if (!empty($clinicID)) {
    $rsElement = CIBlockElement::GetList(
        array(),
        array(
            "IBLOCK_ID" => CLINIC_IBLOCK_ID,
            "ID"        => $clinicID
        ),
        false,
        false,
        array("IBLOCK_ID", "ID", "NAME", "PROPERTY_CALLBACK_EMAIL")
    );

    $arClinicEmails = array(
        421  => 'research.lapino@mcclinics.ru',//Клинический Госпиталь Лапино «Мать и дитя»
        422  => 'n.marilova@mcclinics.ru'//Перинатальный Медицинский Центр «Мать и дитя»
    );

 	if ($arElement = $rsElement->GetNext()){
        $arSendFields["P_CLINIC"]     = $arElement['NAME'];
        $arSendFields["CLINIC_EMAIL"] = $arClinicEmails[$arElement['ID']];//$arElement["~PROPERTY_CALLBACK_EMAIL_VALUE"];
 	}

    $rsElement = CIBlockElement::GetList(
        array(),
        array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ID" => $arResult["ELEMENT_ID"]
        )
    );

    if ($obElement = $rsElement->GetNextElement()){
        $properties = $obElement->GetProperties();

        $fileLinks = array();

        if(isset($properties['FILES']['VALUE'])){
            foreach($properties['FILES']['VALUE'] AS $fileID){
                $fileLinks[] = 'http://' . $_SERVER['HTTP_HOST'] . CFile::GetPath($fileID);
            }
        }

        $arSendFields["P_FILES"] = '<br/>' . implode(',<br/>', $fileLinks);
    }

    /*Добавим данные ответов для анкеты на E-mail*/
    $arRequestData = $_POST;
    $arQuestions = array(
        'Выявлен ли у Вас фактор бесплодия и есть ли показания к проведению программы ЭКО?' => ($arRequestData['MS_P_1'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Укажите, пожалуйста, выявленный фактор бесплодия' => ($arRequestData['MS_P_2'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'У Вас есть показания к проведению ПГД (предимплантационный скрининг)?' => ($arRequestData['MS_P_3'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Проходили ли Вы ранее лечение методом ЭКО?' => ($arRequestData['MS_P_4_1'] == 1 ? '<strong>Да, ' . $arRequestData['MS_P_4_2'] . '</strong>' : '<strong>Нет</strong>'),
        'Был ли у Вас в предыдущем протоколе ЭКО зарегистрирован СГСЯ (синдром гиперстимуляции яичников) тяжелой степени (стационарное лечение с проведением пункции)' => ($arRequestData['MS_P_5'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Сколько клеток было получено при проведении пункции? (стимуляция в протоколе с антагонистами с дозой гормонов 150МЕ и более)' => '<strong>' . $arRequestData['MS_P_6'] . '</strong>',
        'Укажите, пожалуйста, Ваш возраст (сколько полных лет)' => '<strong>' . $arRequestData['MS_P_7'] . '</strong>',
        'Укажите, пожалуйста, Ваш Рост (в см.) и вес (в кг.)' => '<strong>Рост: ' . $arRequestData['MS_P_8_1'] . ', Вес: ' . $arRequestData['MS_P_8_2'] . '</strong>',
        'Укажите, пожалуйста, продолжительность Вашего менструального цикла (от 1го дня этого кровотечения до 1го дня следующего)' => '<strong>' . $arRequestData['MS_P_9'] . '</strong>',
        'Вы курите?' => ($arRequestData['MS_P_10_1'] == 1 ? '<strong>Да, ' . $arRequestData['MS_P_10_2'] . '</strong>' : '<strong>Нет</strong>'),
        'Проходили ли Вы когда-либо лечение по поводу наркомании и/или алкоголизма?' => ($arRequestData['MS_P_11'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Посмотрите, пожалуйста, результаты исследования Вашего гормонального профиля за последние 6 месяцев' => '<strong>АМГ: ' . $arRequestData['MS_P_12_1'] . '<br/>ФСГ: ' . $arRequestData['MS_P_12_2'] . '<br/>Эстрадиол: ' . ($arRequestData['MS_P_12_3'] == 1 ? $arRequestData['MS_P_12_4'] . ' пг/мл' : $arRequestData['MS_P_12_5'] . ' пмоль/л') . '</strong>',
        'Есть ли у Вас СПКЯ (синдром поликистозных яичников)?' => ($arRequestData['MS_P_13'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Были ли у Вас операции с удалением яичника?' => ($arRequestData['MS_P_14'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Ставят ли Вам диагноз: патология эндометрия/патология полости матки? (полип эндометрия/гиперплазия эндометрия/синехии полости матки)' => ($arRequestData['MS_P_15'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Ставят ли Вам диагноз: порок развития матки?' => ($arRequestData['MS_P_16'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Ставят ли Вам диагноз: эндометриоз яичников/эндометриоидные кисты яичников?' => ($arRequestData['MS_P_17'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Ставят ли Вам диагноз: гидросальпингс/сактосальпингс?' => ($arRequestData['MS_P_18'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Ставят ли Вашему супругу диагноз азооспермия?' => ($arRequestData['MS_P_19'] == 1 ? '<strong>Да</strong>' : '<strong>Нет</strong>'),
        'Укажите, пожалуйста, основные показатели спермограммы супруга за 2016 год (обязательная оценка по критериям ВОЗ 2010)' => '<strong>Концентрация сперматозоидов (содержание млн в 1 мл): ' . $arRequestData['MS_P_20_1'] . '<br/>Общее количество подвижных сперматозоидов (в %): ' . $arRequestData['MS_P_20_2'] . '<br/>Количество морфологически нормальных сперматозоидов (в %): ' . $arRequestData['MS_P_20_3'] . '</strong>',
    );
    /*Добавим данные ответов для анкеты на E-mail*/

    $answers = array();
    $k = 1;
    foreach($arQuestions AS $question => $answer){
        $answers[] = $k . '. ' . $question . ': <br/>' . $answer;
        $k++;
    }

    $arSendFields['ANSWERS'] = implode('<br/><br/>', $answers);
}