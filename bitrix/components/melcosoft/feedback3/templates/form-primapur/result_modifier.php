<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
$iBlockId = $arParams['IBLOCK_ID'];
$sort = ['SORT' => 'ASC'];
$filter = ['ACTIVE' => 'Y', 'IBLOCK_ID' => $iBlockId, 'PROPERTY_TYPE' => 'L'];
$result = CIBlockProperty::GetList($sort, $filter);
$properties = [];
while($property = $result->GetNext()) {
	$enumResult = CIBlockProperty::GetPropertyEnum($property['ID']);
	$values = [];
	while($value = $enumResult->GetNext()) {
		$values[] = $value;
	}
	$property['OPTIONS'] = $values;
	$properties[$property['CODE']] = $property;
}

$properties['CLINIC'] = array(
	'OPTIONS' => array(
		array(
			'ID' => 422, //Перинатальный Медицинский Центр «Мать и дитя»
			'VALUE' => 'Перинатальный Медицинский Центр «Мать и дитя»'
		),
		array(
			'ID' => 421, //Клинический Госпиталь Лапино «Мать и дитя»
			'VALUE' => 'Клинический Госпиталь Лапино «Мать и дитя»'
		)
	)
);

$arResult['PROPERTY_OPTIONS'] = $properties;
?>