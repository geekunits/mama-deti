<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php $formId = 'form-eko';?>
<?php echo $arResult["FORM_HEADER"]; ?>
<div class="bg">
    &nbsp;
</div>
<div class="popup">
    <div class="popup-sector">
        <div class="popup-frame">
            <a href="#" class="close">close</a>
            <span class="popup-title">Клинические исследования</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
            <fieldset>
                <div class="form-block">
                    <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                        <div class="form-rows">
                            <div class="row js-form-sent">
                                <div><?php echo $arParams["~SUCCESS_MESSAGE"];?></div>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="js-part" style="<?php echo (isset($arResult["FORM"]) ? ' display:none; ' : '');?>" data-part="1">
                            <div style="min-height: 200px;">
                                <div class="form-rows form-rows-nocolumn">
                                    <p>
                                        <div class="js-step-number"><strong>Вопрос <span class="js-step-current">1</span> из 20</strong></div>
                                    </p>
                                </div>
                                <div class="form-rows form-rows-nocolumn step step__show js-step" data-step="1">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Выявлен ли у Вас фактор бесплодия и есть ли показания к проведению программы ЭКО?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_1" value="1"<?php echo (isset($_POST['MS_P_1']) && $_POST['MS_P_1'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_1" value="0"<?php echo (isset($_POST['MS_P_1']) && $_POST['MS_P_1'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="2">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Установленная у вас причина бесплодия обусловлена трубным и/или мужским фактором?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_2" value="1"<?php echo (isset($_POST['MS_P_2']) && $_POST['MS_P_2'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_2" value="0"<?php echo (isset($_POST['MS_P_2']) && $_POST['MS_P_2'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="3">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                У Вас есть показания к проведению ПГД (предимплантационный скрининг)?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_3" value="1"<?php echo (isset($_POST['MS_P_3']) && $_POST['MS_P_3'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_3" value="0"<?php echo (isset($_POST['MS_P_3']) && $_POST['MS_P_3'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="4">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Проходили ли Вы ранее лечение методом ЭКО?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_4_1" value="1"<?php echo (isset($_POST['MS_P_4_1']) && $_POST['MS_P_4_1'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_4_1" value="0"<?php echo (isset($_POST['MS_P_4_1']) && $_POST['MS_P_4_1'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row js-step-quantity" style="display: none;">
                                        <div class="label-block">
                                            <label for="MS_P_4_2">Сколько протоколов в течение жизни у Вас было проведено?</label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_4_2" type="text" data-type="integer" value="<?php echo $_POST['MS_P_4_2'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="5">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Если был протокол ЭКО Был ли у Вас в предыдущем протоколе ЭКО зарегистрирован СГСЯ (синдром гиперстимуляции яичников) тяжелой степени (стационарное лечение с проведением пункции)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_5" value="1"<?php echo (isset($_POST['MS_P_5']) && $_POST['MS_P_5'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_5" value="0"<?php echo (isset($_POST['MS_P_5']) && $_POST['MS_P_5'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="6">
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_6">
                                                Если был протокол ЭКО Сколько клеток было получено при проведении пункции? (стимуляция в протоколе с антагонистами с дозой гормонов 150МЕ и более)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_6" type="text" data-type="integer" value="<?php echo $_POST['MS_P_6'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="7">
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_7">
                                                Укажите, пожалуйста, Ваш возраст (сколько полных лет)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_7" type="text" data-type="integer" value="<?php echo $_POST['MS_P_7'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="8">
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_8_1">
                                                Укажите, пожалуйста, Ваш Рост (в см.)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_8_1" type="text" data-type="integer" value="<?php echo $_POST['MS_P_8_1'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_8_2">
                                                Укажите, пожалуйста, Ваш Вес (в кг.)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_8_2" type="text" data-type="integer" value="<?php echo $_POST['MS_P_8_2'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="9">
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_9">
                                                Укажите, пожалуйста, продолжительность Вашего менструального цикла (от 1го дня этого кровотечения до 1го дня следующего)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_9" type="text" data-type="integer" value="<?php echo $_POST['MS_P_9'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="10">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Вы курите?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_10_1" value="1"<?php echo (isset($_POST['MS_P_10_1']) && $_POST['MS_P_10_1'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_10_1" value="0"<?php echo (isset($_POST['MS_P_10_1']) && $_POST['MS_P_10_1'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row js-step-cigarettes" style="display: none;">
                                        <div class="label-block">
                                            <label for="MS_P_10_2">Сколько сигарет в день?</label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_10_2" type="text" data-type="integer" value="<?php echo $_POST['MS_P_10_2'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="11">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Проходили ли Вы когда-либо лечение по поводу наркомании и/или алкоголизма?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_11" value="1"<?php echo (isset($_POST['MS_P_11']) && $_POST['MS_P_11'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_11" value="0"<?php echo (isset($_POST['MS_P_11']) && $_POST['MS_P_11'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="12">
                                    <div>Посмотрите, пожалуйста, результаты исследования Вашего гормонального профиля за последние 6 месяцев</div>
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_12_1">
                                                Укажите Ваш АМГ<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_12_1" type="text" data-type="float" value="<?php echo $_POST['MS_P_12_1'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_12_2">
                                                Укажите Ваш ФСГ на 2-5 д.ц.<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_12_2" type="text" data-type="float" value="<?php echo $_POST['MS_P_12_2'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Укажите Ваш эстрадиол на 2й день м.ц.<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_12_3" value="1"<?php echo (isset($_POST['MS_P_12_3']) && $_POST['MS_P_12_3'] == 1 ? ' checked="checked"' : '');?> />
                                                    пг/мл
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_12_3" value="2"<?php echo (isset($_POST['MS_P_12_3']) && $_POST['MS_P_12_3'] == 2 ? ' checked="checked"' : '');?> />
                                                    пмоль/л
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row js-step-estradiol-1" style="display: none;">
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_12_4" type="text" data-type="float" value="<?php echo $_POST['MS_P_12_4'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row js-step-estradiol-2" style="display: none;">
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_12_5" type="text" data-type="float" value="<?php echo $_POST['MS_P_12_5'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="13">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Есть ли у Вас СПКЯ (синдром поликистозных яичников)?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_13" value="1"<?php echo (isset($_POST['MS_P_13']) && $_POST['MS_P_13'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_13" value="0"<?php echo (isset($_POST['MS_P_13']) && $_POST['MS_P_13'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="14">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Были ли у Вас операции с удалением яичника?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_14" value="1"<?php echo (isset($_POST['MS_P_14']) && $_POST['MS_P_14'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_14" value="0"<?php echo (isset($_POST['MS_P_14']) && $_POST['MS_P_14'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="15">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Ставят ли Вам диагноз: патология эндометрия/патология полости матки? (полип эндометрия/гиперплазия эндометрия/синехии полости матки)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_15" value="1"<?php echo (isset($_POST['MS_P_15']) && $_POST['MS_P_15'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_15" value="0"<?php echo (isset($_POST['MS_P_15']) && $_POST['MS_P_15'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="16">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Ставят ли Вам диагноз: порок развития матки?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_16" value="1"<?php echo (isset($_POST['MS_P_16']) && $_POST['MS_P_16'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_16" value="0"<?php echo (isset($_POST['MS_P_16']) && $_POST['MS_P_16'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="17">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Ставят ли Вам диагноз: эндометриоз яичников/эндометриоидные кисты яичников?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_17" value="1"<?php echo (isset($_POST['MS_P_17']) && $_POST['MS_P_17'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_17" value="0"<?php echo (isset($_POST['MS_P_17']) && $_POST['MS_P_17'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="18">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Ставят ли Вам диагноз: гидросальпингс/сактосальпингс?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_18" value="1"<?php echo (isset($_POST['MS_P_18']) && $_POST['MS_P_18'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_18" value="0"<?php echo (isset($_POST['MS_P_18']) && $_POST['MS_P_18'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="19">
                                    <div class="row">
                                        <div class="label-block">
                                            <label>
                                                Ставят ли Вашему супругу диагноз азооспермия?<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="label-margin-bottom">
                                                <label>
                                                    <input type="radio" name="MS_P_19" value="1"<?php echo (isset($_POST['MS_P_19']) && $_POST['MS_P_19'] == 1 ? ' checked="checked"' : '');?> />
                                                    Да
                                                </label>
                                            </div>
                                            <div>
                                                <label>
                                                    <input type="radio" name="MS_P_19" value="0"<?php echo (isset($_POST['MS_P_19']) && $_POST['MS_P_19'] == 0 ? ' checked="checked"' : '');?> />
                                                    Нет
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-rows form-rows-nocolumn step js-step" data-step="20">
                                    <div>Укажите, пожалуйста, основные показатели спермограммы супруга за 2016 год (обязательная оценка по критериям ВОЗ 2010)</div>
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_20_1">
                                                Концентрация сперматозоидов (содержание млн в 1 мл)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_20_1" type="text" data-type="float" value="<?php echo $_POST['MS_P_20_1'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_20_2">
                                                Общее количество подвижных сперматозоидов (в %)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_20_2" type="text" data-type="float" value="<?php echo $_POST['MS_P_20_2'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="label-block">
                                            <label for="MS_P_20_3">
                                                Количество морфологически нормальных сперматозоидов (в %)<span class="required-star"></span>
                                            </label>
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04">
                                                <input name="MS_P_20_3" type="text" data-type="float" value="<?php echo $_POST['MS_P_20_3'];?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-rows" style="margin-top:10px;">
                                <div class="row align-right">
                                    <div class="button-holder01">
                                        <span class="button01 block-button js-next-step">Далее</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-rows js-part"<?php echo (isset($arResult["FORM"]) ? '' : ' style="display:none;"');?> data-part="2">
                            <div class="row">
                                <div class="label-block">
                                    <label for="MS_P_FIO">
                                        ФИО<?php if (in_array("P_FIO",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?php endif; ?>
                                    </label>
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("P_FIO", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input name="MS_P_FIO" type="text" placeholder="" value="<?=$arResult["FORM"]["P_FIO"]?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="label-block">
                                    <label for="MS_P_PHONE">
                                        Ваш телефон<?php if (in_array("P_PHONE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                    </label>
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input name="MS_P_PHONE" class="js-phone-input" type="text" placeholder="Телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="label-block">
                                    <label for="MS_P_EMAIL">
                                        E-mail<?php if (in_array("P_EMAIL", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                    </label>
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input name="MS_P_EMAIL" type="text" placeholder="E-mail" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="label-block">
                                    <label for="MS_P_CLINIC">
                                        Выбериту клинику <?php if (in_array("P_CLINIC", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                    </label>
                                </div>
                                <div class="row-sector">
                                    <select name="MS_P_CLINIC"<?=(in_array("P_CLINIC", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error"' : '');?>>
                                        <option value="" disabled="disabled" selected="selected">Выберите клинику</option>
                                        <?php foreach ($arResult['PROPERTY_OPTIONS']['CLINIC']['OPTIONS'] as $option):?>
                                            <option value="<?php echo $option['ID']; ?>"<?php if ($arResult['FORM']['P_CLINIC'] == $option['ID']):?> selected="selected"<?endif?>><?=$option['VALUE']?></option>
                                        <?endforeach?>
                                    </select>
                                </div>
                            </div>
                            <?php /*
                            <div class="row">
                                <div>Приложите реузьтаты анализов к письму на рассмотрение репродуктологу</div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>Уровни гормонов (ФСГ, эстрадиол, АМГ) на 2-5 дни м.ц.</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>УЗИ органов малого таза, включая яичники.</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>Заключение маммолога, УЗИ молочных желез.</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>Наличие результатов  TORCH-инфекции, флюорография, кольпоскопия.</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>Результат мазка на онкоцитологию (ПАП-тест).</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>Наличие результатов определения хламидий, микоплазмы, уреаплазмы, ВПЧ 16/18, ВПГ 1 и 2 типов, ЦМВ.</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>Наличие результатов RW, анти-ВИЧ, анти-HBcor, HBSAg, анти-HCV, антиген ВПГ.</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label>Наличие результатов ЭКГ.</label>
                                </div>
                                <div class="row-sector">
                                    <input name="MS_P_FILES[]" type="file" placeholder="" />
                                </div>
                            </div>
                            */ ?>
                            <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                                <div class="row">
                                    <div class="label-block">
                                        <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                        <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                    </div>
                                    <div class="row-sector">
                                        <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                    </div>
                                </div>
                                <a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
                            <?php endif; ?>

                            <div class="row align-right">
                                <div class="button-holder01">
                                    <span class="button01 block-button"><input type="submit" name="form" value="<?php echo $formId; ?>">Отправить</span>
                                </div>
                            </div>
                        </div>
                        <div class="js-form-fail" style="display: none;">К сожалению, предоставленные данные не отвечают критериям включения в клинические исследования. Мы приглашаем вас на консультативный прием, чтобы ответить на все ваши вопросы о том, что такое ЭКО и бесплодие, и будем рады помочь вам обрести счастье материнства</div>
                    <?endif?>
                </div>
            </fieldset>
        </div>
    </div>
    <style>
        .popup .step{
            display: none;
        }

        .popup .step__show{
            display: block;
        }

        .label-margin-bottom {
            margin-bottom: 5px;
        }
    </style>
</div>

<?= $arResult["FORM_FOOTER"] ?>

<script>
    $(function() {
        function getQuestionValue(questionNumber){
            var $el = $('input[name="MS_P_' + questionNumber + '"]');
            var val;

            if(!$el.size()){
                return undefined;
            }

            switch($el.get(0).type){
                case 'radio':
                    val = $el.filter(':checked').val();
                    break;
                default:
                    val = $el.val()
            }

            return val;
        }

        function toFloat(str){
            return parseFloat(str.toString().replace(',', '.'));
        }

        function checkQuestion(questionNumber){
            var questions = {
                1: 1,
                2: 1,
                3: 0,
                4: function(questionNumber){
                    var val = getQuestionValue(questionNumber + '_1');
                    var val2 = getQuestionValue(questionNumber + '_2');

                    if(val == undefined || val2 == undefined){
                        return null;
                    }

                    if(val == 1){
                        if(val2 < 2){
                            return true;
                        }
                    }else{
                        return true;
                    }

                    return false;
                },
                5: function(questionNumber){
                    var val = getQuestionValue(questionNumber);

                    if(val == undefined){
                        return null;
                    }

                    if(val == 0){
                        currentStep++;
                    }

                    return true;
                },
                6: function(questionNumber){
                    var val = getQuestionValue(questionNumber);

                    if(val == undefined){
                        return null;
                    }

                    return val >= 4;
                },
                7: function(questionNumber){
                    var age = getQuestionValue(questionNumber);

                    if(!age.length){
                        return null;
                    }

                    return age >= 20 && age <= 35;
                },
                8: function(questionNumber){
                    var growth = getQuestionValue(questionNumber + '_1');
                    var weight = getQuestionValue(questionNumber + '_2');

                    if(!growth.length || !weight.length){
                        return null;
                    }

                    growth = toFloat(growth);
                    weight = toFloat(weight);

                    var imtIndex = weight / (Math.pow((growth / 100), 2));

                    return imtIndex >= 18 && imtIndex <= 30;//18 ≤ ИМТ ≤ 30 кг/м2
                },
                9: function(questionNumber){
                    var val = getQuestionValue(questionNumber);

                    if(val == undefined || !val.length){
                        return null;
                    }

                    return val >= 20 && val <= 35;
                },
                10: function(questionNumber){
                    var val = getQuestionValue(questionNumber + '_1');

                    if(val == undefined){
                        return null;
                    }

                    if(val == 1){
                        var val2 = getQuestionValue(questionNumber + '_2');

                        if(val2 == undefined || !val2.length){
                            return null;
                        }

                        if(val2 <= 10){
                            return true;
                        }
                    }else{
                        return true;
                    }

                    return false;
                },
                11: 0,
                12: function(questionNumber){
                    var amg             = getQuestionValue(questionNumber + '_1');
                    var fsg             = getQuestionValue(questionNumber + '_2');
                    var estradiolType   = getQuestionValue(questionNumber + '_3');

                    var estradiol;

                    if(estradiolType){
                        estradiolType = parseInt(estradiolType);
                        estradiol = getQuestionValue(questionNumber + '_' + (3 + estradiolType));
                    }

                    if(amg == undefined || fsg == undefined || estradiol == undefined || !amg.length || !fsg.length || !estradiol.length){
                        return null;
                    }

                    amg = toFloat(amg);
                    fsg = toFloat(fsg);
                    estradiol = toFloat(estradiol);

                    return amg >= 1 && fsg <= 10 && ((estradiolType == 1 && estradiol <= 50) || (estradiolType == 2 && estradiol <= 180));
                },
                13: 0,
                14: 0,
                15: 0,
                16: 0,
                17: 0,
                18: 0,
                19: 0,
                20: function(questionNumber){
                    var val1    = getQuestionValue(questionNumber + '_1');
                    var val2    = getQuestionValue(questionNumber + '_2');
                    var val3    = getQuestionValue(questionNumber + '_3');

                    if(val1 == undefined || val2 == undefined || val3 == undefined || !val1.length || !val2.length || !val3.length){
                        return null;
                    }

                    val1 = toFloat(val1);
                    val2 = toFloat(val2);
                    val3 = toFloat(val3);

                    return val1 >= 2 && val2 >= 10 && val3 >= 1;
                }
            };

            if(typeof questions[questionNumber] !== "undefined"){
                var result = questions[questionNumber];

                if(typeof result == 'function'){
                    return result(questionNumber);
                }else{
                    var val = getQuestionValue(questionNumber);

                    return val == undefined ? null : val.toString() == result.toString();
                }
            }else{
                return false;
            }
        }

        $('#<?php echo $formId; ?>-popup .popup').show();
        centrateDiv($('#<?php echo $formId; ?>-popup'));
        $("#form-eko-popup .js-phone-input").mask("8(999) 999-99-99");

        $('#<?php echo $formId; ?>-popup .close, #<?php echo $formId; ?>-popup .bg').click(function (e) {
            e.preventDefault();
            $('#<?php echo $formId; ?>-popup').hide();
        });

        var currentStep = 1;

        $('[data-type="integer"]').on('keydown', function(e){
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13/*, 110, 190*/]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('[data-type="float"]').on('keydown', function(e){
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 188]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('.js-next-step').on('click', function(e){
            e.preventDefault();

            var result = checkQuestion(currentStep);

            if(result === true){
                $('.js-step.step__show').removeClass('step__show');

                currentStep++;

                $nextStep = $('.js-step').filter('[data-step="' + currentStep + '"]');

                if($nextStep.size()){
                    $nextStep.addClass('step__show');

                    $('.js-step-current').html(currentStep);
                }else{
                    //показываем part2 и скрываем part1
                    $('.js-part').filter('[data-part="1"]').hide();
                    $('.js-part').filter('[data-part="2"]').show();
                }
            }else if(result === false){
                $('.js-part').remove();
                $('.js-form-fail').show();
            }

            centrateDiv($('#<?php echo $formId; ?>-popup'));
        });

        $('.js-step1').find('input[name="MS_P_INFERTILITY_CAUSES"]').on('change', function(e){
            var method = $(this).data('value') == 'да' ? 'show' : 'hide' ;
            $('.js-infertility-factor')[method]();
            centrateDiv($('#<?php echo $formId; ?>-popup'));
        });

        $('.js-step').find('input[name="MS_P_4_1"]').on('change', function(){
            var method = $(this).val() == 1 ? 'show' : 'hide' ;

            $(this).closest('.js-step').find('.js-step-quantity')[method]();
        });

        $('.js-step').find('input[name="MS_P_10_1"]').on('change', function(){
            var method = $(this).val() == 1 ? 'show' : 'hide' ;

            $(this).closest('.js-step').find('.js-step-cigarettes')[method]();
        });

        $('.js-step').find('input[name="MS_P_12_3"]').on('change', function(){
            var $step = $(this).closest('.js-step');

            $step.find('.js-step-estradiol-1,.js-step-estradiol-2').hide();

            var val = $(this).val();

            if(val){
                $step.find('.js-step-estradiol-' + val).show();
            }
        });
    });
</script>
