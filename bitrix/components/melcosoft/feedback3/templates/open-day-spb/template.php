<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?= $arResult["FORM_HEADER"] ?>
<div class="bg">
    &nbsp;
</div>
<div class="popup" style="display:none">
    <div class="popup-sector">
        <div class="popup-frame">
            <a href="#" class="close">close</a>
            <span class="popup-title">Заявка на день открытых дверей</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
            <fieldset>
                <div class="form-block">
                    <div class="form-rows">
                    <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                        <div class="js-form-sent">
                            Ваша заявка получена!
                            <br><br>
                            Наши сотрудники перезвонят Вам в часы работы клиники.
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_NAME">Ф.И.О.<span class="required-star"></span></label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_NAME" type="text" placeholder="Иван Иванович" value="<?=$arResult["FORM"]["NAME"]?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PHONE">Номер телефона<span class="required-star"></span></label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_PHONE" type="text" placeholder="Телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PHONE">E-mail<span class="required-star"></span></label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_EMAIL" type="text" placeholder="E-mail" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="label-block">
                                Проводили ли вы процедуру ЭКО ранее?<?php if (in_array("P_EKO", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                            </div>
                            <div class="row-sector <?=(in_array("P_EKO", $arResult["FORM_ERROR_FIELDS"]) ? ' error-txt' : '');?>">
                                <?php foreach($arResult['PROPERTY_OPTIONS']['EKO']['OPTIONS'] as $option): ?>
                                    <div>
                                        <label for="MS_P_EKO">
                                            <input type="radio" name="MS_P_EKO" value="<?php echo $option['ID']; ?>"
                                                <?php if ($arResult['FORM']['P_EKO'] === $option['ID'] || (empty($arResult['FORM']['P_EKO']) && $option['DEF'] === 'Y')): ?> checked="checked" <?php endif; ?>
                                            > 
                                            <?php echo $option['VALUE']; ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block" style="vertical-align: top">
                                Источник информации<?php if (in_array("P_INFO_SOURCE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                            </div>
                            <div class="row-sector">
                                <div class="js-open-day-source-radio">
                                    <?php foreach($arResult['PROPERTY_OPTIONS']['INFO_SOURCE']['OPTIONS'] as $option): ?>
                                        <div>
                                            <label for="MS_P_INFO_SOURCE">
                                                <input type="radio" name="MS_P_INFO_SOURCE" value="<?php echo $option['ID']; ?>"
                                                    <?php if ($arResult['FORM']['P_INFO_SOURCE'] === $option['ID'] || (empty($arResult['FORM']['P_INFO_SOURCE']) && $option['DEF'] === 'Y')): ?> checked="checked" <?php endif; ?>
                                                > 
                                                <?php echo $option['VALUE']; ?>
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="text04<?=(in_array("P_INFO_SOURCE_OTHER", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_INFO_SOURCE_OTHER" class="js-open-day-source-other" type="text" placeholder="Другое" value="<?=$arResult["FORM"]["P_INFO_SOURCE_OTHER"]?>">
                                </div>
                            </div>
                        </div>   

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_OTHERS">Если с вами идёт супруг/супруга, впишите фамилию, имя, отчество в строку справа</label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_OTHERS", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_OTHERS" type="text" placeholder="ФИО человека" value="<?=$arResult["FORM"]["P_OTHERS"]?>">
                                </div>
                            </div>
                        </div>

                        <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                            <div class="row">
                                <div class="label-block">
                                    <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                    <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                    <a href="#" onclick="refreshCaptcha(this); return false;">Обновить</a>
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="row align-right">
                            <input type="reset" value="Очистить" class="reset-button">
                            <div class="button-holder01">
                                <span class="button01 block-button"><input type="submit" name="form" value="open-day">Отправить</span>
                            </div>
                        </div>
                    <?endif?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<?= $arResult["FORM_FOOTER"] ?>

<script>
    $(function() {
        
        $('#open-day-spb .popup').show();
        centrateDiv($('#open-day-spb'));

        $('#open-day-spb .close, #open-day-spb .bg').click(function (e) {
            e.preventDefault();
            $('#open-day-spb').hide();
        });

        $('#open-day-spb .js-open-day-source-other').focus(function() {
            $('#open-day-spb .js-open-day-source-radio input:checked').prop('checked', false);
            $('#open-day-spb .js-open-day-source-radio .checked').removeClass('checked');
        });
    });

    $(function() {
        $("#open-day-spb input[name='MS_P_PHONE']").mask("8(999) 999-99-99");
    });
    
</script>
