<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

CModule::IncludeModule('iblock');

$arFilter = array("IBLOCK_ID"=>2,"ACTIVE"=>"Y","PROPERTY_CITY"=>$GLOBALS["CURRENT_CITY"]["ID"]);
if ($arParams['SERVICE_IDS']) {
    $arFilter['PROPERTY_SERVICES'] = $arParams['SERVICE_IDS'];
}

$arResult['CLINICS'] = array();
$arResult['METRIC_ID'] = null;
$arResult['CONFIDENTIAL_CLINICS'] = array();

$rsElement = CIBlockElement::GetList(array("RAND"=>"ASC"),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","PROPERTY_*"));
while ($obElement = $rsElement->GetNextElement()) {
    $arFields = $obElement->GetFields();
    $arFields['PROPERTIES'] = $obElement->GetProperties();

    if($arFields['PROPERTIES']['SHOW_CONFIDENTIAL_CONFIRM']['VALUE'] && $arFields['PROPERTIES']['CONFIDENTIAL_CONFIRM_TEXT']['VALUE']){
        $arResult['CONFIDENTIAL_CLINICS'][$arFields['ID']] = $arFields['PROPERTIES']['CONFIDENTIAL_CONFIRM_TEXT']['~VALUE'];
    }

    $arResult['CLINICS'][] = $arFields;

    if ($_REQUEST['CLINIC'] && $arFields['ID'] == $_REQUEST['CLINIC']) {
        $arResult['METRIC_ID'] = $arFields['PROPERTIES']['METRIC_ID']['VALUE'];
    }
}

