<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

$arTime2 = array();
$t = intval(date("H"));
($t >= 20 || $t < 9) ? $h = 9 : $h = $t + 1 ;
$m = 0;
while (true) {
    $arTime2[] = $h . ":" . ($m < 10 ? '0' . $m : $m);
    $m += 30;
    if ($m == 60) {
        $h++;
        $m = 0;
    }
    if ($h == 20 && $m == 30)
        break;
}

$clinicId = isset($arResult["FORM"]["P_CLINIC"]) ? $arResult["FORM"]["P_CLINIC"] : (int) $_REQUEST['CLINIC'] ;

$clinic = null;

if ($clinicId) {
    $clinics = CMamaDetiAPI::GetClinics(['ID' => $clinicId]);
    if (!empty($clinics)) {
        $clinic = $clinics[0];
    }
}else{
    $clinic = $arResult['CLINICS'][0];
}

?>
<?= $arResult["FORM_HEADER"] ?>
    <div class="bg">
        &nbsp;
    </div>
    <div class="popup" style="display:none;">
        <div class="popup-sector">
            <div class="popup-frame">
                <a href="#" class="close">close</a>
                <span class="popup-title">Перезвоните мне</span>
                <?php if ($arResult["FORM_ERROR"]): ?>
                    <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
                <?php endif ?>
                <fieldset>
                    <div class="form-block">
                        <div class="form-rows">
                        <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                            <div>
                                Ваша заявка получена!
                                <br><br>
                                Наши сотрудники перезвонят Вам в часы работы клиники.
                            </div>
                        <?php else: ?>
                            <?php if (!empty($_REQUEST['CLINIC'])): ?>
                                Запись в <strong><?php echo $clinic['NAME']; ?></strong>
                            <?php endif; ?>
                            <div class="row">
                                <div class="label-block">
                                    <label for="id100">
                                        <?php if (in_array("NAME", $arParams["REQUIRED_FIELDS"])): ?>
                                            Ваше имя<span class="required-star"></span><?php endif; ?>
                                    </label>
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input name="MS_NAME" type="text" id="id100" placeholder="Иван Иванович" value="<?=$arResult["FORM"]["NAME"]?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="label-block">
                                    <label for="id101">
                                        <?php if (in_array("P_PHONE", $arParams["REQUIRED_FIELDS"])): ?>
                                            Ваш телефон<span class="required-star"></span><?php endif; ?>
                                    </label>
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input name="MS_P_PHONE" type="text" id="id101" placeholder="Телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                    </div>
                                </div>
                            </div>
                            <?if (isset($_REQUEST["CLINIC"]) && intval($_REQUEST["CLINIC"])>0):?>
                                <input type="hidden" value="<?=intval($_REQUEST["CLINIC"])?>" name="MS_P_CLINIC">
                            <?else:?>
                                <div class="row">
                                    <div class="label-block">
                                        <label for="id103">
                                            Выберите клинику<?php if (in_array("P_CLINIC", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                        </label>
                                    </div>
                                    <div class="row-sector">
                                        <select name="MS_P_CLINIC"<?=(in_array("P_CLINIC", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error"' : '');?>>
                                            <?foreach ($arResult['CLINICS'] as $arClinic):?>
                                            <option data-url="<?=$arClinic['DETAIL_PAGE_URL'];?>" value="<?=$arClinic["ID"]?>"<?if ($arResult["FORM"]["P_CLINIC"] == $arClinic["ID"]):?> selected="selected"<?endif?>><?=$arClinic["NAME"]?></option>
                                            <?endforeach?>
                                        </select>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="row">
                                <div class="label-block">
                                    <label>
                                        Удобное время для звонка<?php if (in_array("P_CALLBACK_TIME", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                    </label>
                                </div>
                                <div class="row-sector">
                                    <?php /* <div class="text04">
                                         <input
                                            class="js-time-input"
                                            type="text"
                                            name="MS_P_CALLBACK_TIME"
                                            placeholder="XX:XX"
                                            value="<?php echo $arResult['FORM']['P_CALLBACK_TIME']; ?>">
                                    </div> */ ?>
                                    <select name="MS_P_CALLBACK_TIME">
                                        <option value=""<?php if (!$arResult["FORM"]["P_CALLBACK_TIME"]):?> selected="selected"<?php endif?>>Не выбрано</option>
                                        <?php foreach ($arTime2 as $value): ?>
                                        <option value="<?=$value?>"<?php if ($arResult["FORM"]["P_CALLBACK_TIME"] == $value):?> selected="selected"<?php endif?>><?=$value?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="textarea01<?=(in_array("PREVIEW_TEXT", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <textarea name="MS_PREVIEW_TEXT" cols="30" rows="10" placeholder="Комментарий"><?=$arResult["FORM"]["PREVIEW_TEXT"]?></textarea>
                                </div>
                            </div>
                            <?php
                                $propEnumValue = null;

                                $rsProperty = CIBlockProperty::GetList(
                                    array(),
                                    array(
                                        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                        'CODE' => 'SHOW_CONFIDENTIAL_CONFIRM'
                                    )
                                );

                                if($arProperty = $rsProperty->Fetch()){
                                    $rsPropEnums = CIBlockProperty::GetPropertyEnum($arProperty['ID']);

                                    if($arEnum = $rsPropEnums->Fetch()){
                                        $propEnumValue = $arEnum['ID'];
                                    }
                                }
                            ?>
                            <div class="row js-confidential-confirm" style="<?=($arResult['CONFIDENTIAL_CLINICS'][$clinic['ID']] ? '' : 'display:none;');?>">
                                <div>
                                    <input type="checkbox" name="MS_P_SHOW_CONFIDENTIAL_CONFIRM" id="MS_P_SHOW_CONFIDENTIAL_CONFIRM" value="<?=$propEnumValue;?>"<?php echo (isset($_POST['MS_P_SHOW_CONFIDENTIAL_CONFIRM']) && $_POST['MS_P_SHOW_CONFIDENTIAL_CONFIRM'] == $propEnumValue ? ' checked="checked"' : '');?> />
                                    <label for="MS_P_SHOW_CONFIDENTIAL_CONFIRM" class="js-confidential-text"><?=$clinic['PROPERTIES']['CONFIDENTIAL_CONFIRM_TEXT']['~VALUE'];?></label>
                                    <?php if(in_array("P_SHOW_CONFIDENTIAL_CONFIRM", $arResult["FORM_ERROR_FIELDS"])): ?>
                                        <div style="color: #ce156d;">Вы должны принять условия на обработку персональных данных</div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                                <div class="row">
                                    <div class="label-block">
                                        <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                        <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                    </div>
                                    <div class="row-sector">
                                        <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                    </div>
                                </div>
                                <a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
                            <?php endif; ?>
                            <div class="row align-right">
                                <input type="reset" value="Очистить" class="reset-button">
                                <div class="button-holder01">
                                    <span class="button01 block-button js-goal" data-target="CallBack2<?php echo $arResult['METRIC_ID'] ? '_'.$arResult['METRIC_ID'] : ''; ?>"><input type="submit" name="form" value="callback">Отправить</span>
                                </div>
                            </div>
                        <?endif?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>

<?= $arResult["FORM_FOOTER"] ?>

<script>
    var confidentialClinics = <?=json_encode($arResult['CONFIDENTIAL_CLINICS']);?>;
    $(function () {
      setTimeout(function () {$('.popup input, .popup select').styler();  }, 1);
    });

    $('#callback-popup .popup').show();
    centrateDiv($('#callback-popup'));

    $("#callback-popup input[name='MS_P_PHONE']").mask("8(999) 999-99-99");

    $('.js-time-input').each(function() {
        $(this).mask('99:99');
    });

    $("#callback-popup select[name='MS_P_CLINIC']").on('change', function(){
        var clinicId = $(this).val();

        if(confidentialClinics[clinicId]){
            $('.js-confidential-text').html(confidentialClinics[clinicId]);
            $('.js-confidential-confirm').show();
        }else{
            $('.js-confidential-confirm').hide();
        }

        $('.js-confidential-confirm')
            .find("input[name='MS_P_SHOW_CONFIDENTIAL_CONFIRM']")
            .prop('checked', false)
            .trigger('change');
    });

    $('.js-confidential-text').on('click', 'a', function(e){
        e.preventDefault();

        window.open($(this).attr('href'), '_blank');
    });
</script>
