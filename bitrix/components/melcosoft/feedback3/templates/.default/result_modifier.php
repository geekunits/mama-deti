<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

CModule::IncludeModule('iblock');

//$GLOBALS["CURRENT_CITY"]
$arResult['CLINICS'] = array();
$arResult['METRIC_ID'] = null;
$arResult['CONFIDENTIAL_CLINICS'] = array();

$rsElement = CIBlockElement::GetList(
    array("RAND" => "ASC"),
    array(
        "IBLOCK_ID" => 2,
        "ACTIVE" => "Y",
        "PROPERTY_CITY" => $GLOBALS["CURRENT_CITY"]["ID"]
    ),
    false,
    false,
    array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*")
);

while ($obElement = $rsElement->GetNextElement()) {
    $arFields = $obElement->GetFields();
    $arFields['PROPERTIES'] = $obElement->GetProperties();

    if($arFields['PROPERTIES']['SHOW_CONFIDENTIAL_CONFIRM']['VALUE'] && $arFields['PROPERTIES']['CONFIDENTIAL_CONFIRM_TEXT']['VALUE']){
        $arResult['CONFIDENTIAL_CLINICS'][$arFields['ID']] = $arFields['PROPERTIES']['CONFIDENTIAL_CONFIRM_TEXT']['~VALUE'];
    }

    $arResult['CLINICS'][] = $arFields;

    if ($_REQUEST['CLINIC'] && $arFields['ID'] == $_REQUEST['CLINIC']) {
        $arResult['METRIC_ID'] = $arFields['PROPERTIES']['METRIC_ID']['VALUE'];
    }
}

shuffle($arResult['CLINICS']);

if (!empty($_REQUEST["DOCTOR"]) && intval($_REQUEST["DOCTOR"])>0 && intval($arResult["FORM"]["P_CLINIC"])<=0) {
    $rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>3,"ID"=>$_REQUEST["DOCTOR"],"ACTIVE"=>"Y"),false,false,array("IBLOCK_ID","ID","PROPERTY_CLINIC"));
    if ($arElement = $rsElement->GetNext()) {
        $arResult["FORM"]["P_CLINIC"] = $arElement["PROPERTY_CLINIC_VALUE"];
    }
}