<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
$arTime = array();
$h = 9;
$m = 0;
while (true) {
    $arTime[] = $h . ":" . ($m < 10 ? '0' . $m : $m);
    $m += 30;
    if ($m == 60) {
        $h++;
        $m = 0;
    }
    if ($h == 19 && $m == 30)
        break;
}
$arTime2 = array();
$t = intval(date("H"));
($t >= 20 || $t < 9) ? $h = 9 : $h = $t + 1 ;
$m = 0;
while (true) {
    $arTime2[] = $h . ":" . ($m < 10 ? '0' . $m : $m);
    $m += 30;
    if ($m == 60) {
        $h++;
        $m = 0;
    }
    if ($h == 20 && $m == 30)
        break;
}
//echo '<pre>';echo htmlspecialcharsbx(print_r($arResult,true)); echo '</pre>';

$clinicId = isset($arResult["FORM"]["P_CLINIC"]) ? $arResult["FORM"]["P_CLINIC"] : (int) $_REQUEST['CLINIC'] ;

$clinic = null;

if ($clinicId) {
    $clinics = CMamaDetiAPI::GetClinics(['ID' => $clinicId]);
    if (!empty($clinics)) {
        $clinic = $clinics[0];
    }
}else{
    $clinic = $arResult['CLINICS'][0];
}

?>
<script>
    $(function () {
      $('input, select').styler();
    });
</script>

<?= $arResult["FORM_HEADER"] ?>
<div class="bg">
    &nbsp;
</div>
<div class="popup" style="display:none;">
    <div class="popup-sector">
        <div class="popup-frame">
            <a href="#" class="close">close</a>
            <span class="popup-title">Заявка на запись на прием</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
            <fieldset>
                <div class="form-block">
                    <div class="form-rows">
                    <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                        <div>
                            Ваше обращение получено и будет обработано! <br><br>
                            Данная форма является ЗАЯВКОЙ на запись на прием. Для подтверждения факта записи на прием с Вами свяжутся наши сотрудники в часы работы клиники.
                        </div>
                    <?php else: ?>
                        <div>Данная форма является ЗАЯВКОЙ на запись на прием. Для подтверждения факта записи на прием с Вами свяжутся наши сотрудники.</div>
                        <?if(!empty($_REQUEST['DOCTOR'])):?>
                            <?$doc = CMamaDetiAPI::getDoctors(array('ID' => $_REQUEST['DOCTOR']));?>
                            <p>
                                Запись к доктору <strong><?=$doc[0]['NAME'];?> </strong>
                            </p>
                        <?endif;?>
                        <?php if (!empty($_REQUEST['CLINIC'])): ?>
                            <p>
                                Запись в <strong><?php echo $clinic['NAME']; ?></strong>
                            </p>
                        <?php endif; ?>
                        <div class="row">
                            <div class="label-block">
                            <label for="id100">Имя<?if (in_array("NAME",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?endif?></label>
                            </div>
                            <div class="row-sector">
                            <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                <input name="MS_NAME" type="text" id="id100" placeholder="Иван Иванович"
                                   value="<?= $arResult["FORM"]["NAME"] ?>">
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label-block">
                            <label for="id102">Ваш телефон<?if (in_array("P_PHONE",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?endif?></label>
                            </div>
                            <div class="row-sector">
                            <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                <input name="MS_P_PHONE" type="text" id="id102" placeholder="Телефон для связи"
                                   value="<?= $arResult["FORM"]["P_PHONE"] ?>">
                            </div>
                            </div>
                        </div>
                        <?if (isset($_REQUEST["CLINIC"]) && intval($_REQUEST["CLINIC"])>0):?>
                            <input type="hidden" value="<?=intval($_REQUEST["CLINIC"])?>" name="MS_P_CLINIC">
                        <?else:?>
                            <?if (!empty($_REQUEST['DOCTOR']) && empty($_REQUEST['CLINIC'])) {
                            $doctorClinics = CMamaDetiAPI::getDoctorsClinics(array('ID' => $_REQUEST['DOCTOR']));
                            }?>
                            <div class="row">
                            <div class="label-block">
                                <label for="id103">Выберите клинику<?if (in_array("P_CLINIC",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?endif?></label>
                            </div>
                            <div class="row-sector">
                                <select name="MS_P_CLINIC"<?=(in_array("P_CLINIC", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error"' : '');?>>
                                <?foreach ($arResult['CLINICS'] as $arClinic):?>
                                    <?if(!empty($doctorClinics)):?>
                                        <?if(in_array($arClinic['NAME'], $doctorClinics)):?>
                                            <option value="<?=$arClinic["ID"]?>"<?if ($arResult["FORM"]["P_CLINIC"] == $arClinic["ID"]):?> selected="selected"<?endif?>><?=$arClinic["NAME"]?></option>
                                        <?endif;?>
                                    <?else:?>
                                        <option value="<?=$arClinic["ID"]?>"<?if ($arResult["FORM"]["P_CLINIC"] == $arClinic["ID"]):?> selected="selected"<?endif?>><?=$arClinic["NAME"]?></option>
                                    <?endif;?>
                                <?endforeach?>
                                </select>
                            </div>
                            </div>
                        <?endif?>
                        <?if (isset($_REQUEST["DOCTOR"]) && intval($_REQUEST["DOCTOR"])>0):?>
                            <input type="hidden" value="<?=intval($_REQUEST["DOCTOR"])?>" name="MS_P_DOCTOR">
                        <?endif?>

                        <div class="row">
                            <div class="label-block">
                            <label for="id104">Укажите дату<?if (in_array("P_DATE",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?endif?></label>
                            </div>
                            <div class="row-sector">
                                <div class="text04 text-calendar js-calendar-click<?=(in_array("P_DATE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input name="MS_P_DATE" id="id104" class="js-calendar-input" value="<?= $arResult["FORM"]["P_DATE"] ?>" placeholder="ДД.ММ.ГГГГ"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label-block">
                            <label>Желаемое время<?if (in_array("P_TIME",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?endif?></label>
                            </div>
                            <div class="row-sector">
                                <select name="MS_P_TIME"<?=(in_array("P_TIME", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error"' : '');?>>
                                    <?php foreach ($arTime as $value): ?>
                                        <option value="<?=$value?>"<?if ($arResult["FORM"]["P_TIME"] == $value):?> selected="selected"<?endif?>><?=$value?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="label-block">
                                <label>
                                    Удобное время для звонка<?php if (in_array("P_CALLBACK_TIME", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                               <?php /* <div class="text04">
                                     <input
                                        class="js-time-input"
                                        type="text"
                                        name="MS_P_CALLBACK_TIME"
                                        placeholder="XX:XX"
                                        value="<?php echo $arResult['FORM']['P_CALLBACK_TIME']; ?>">
                                </div> */ ?>
                                <select name="MS_P_CALLBACK_TIME">
                                    <option value=""<?php if (!$arResult["FORM"]["P_CALLBACK_TIME"]):?> selected="selected"<?php endif?>>Не выбрано</option>
                                    <?php foreach ($arTime2 as $value): ?>
                                        <option value="<?=$value?>"<?php if ($arResult["FORM"]["P_CALLBACK_TIME"] == $value):?> selected="selected"<?php endif?>><?=$value?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="textarea01">
                            <textarea cols="30" rows="10" placeholder="Какие услуги клиники вас интересуют"
                                  name="MS_PREVIEW_TEXT"><?= $arResult["FORM"]["PREVIEW_TEXT"] ?></textarea>
                            </div>
                        </div>
                        <?php
                            $propEnumValue = null;

                            $rsProperty = CIBlockProperty::GetList(
                                array(),
                                array(
                                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                    'CODE' => 'SHOW_CONFIDENTIAL_CONFIRM'
                                )
                            );

                            if($arProperty = $rsProperty->Fetch()){
                                $rsPropEnums = CIBlockProperty::GetPropertyEnum($arProperty['ID']);

                                if($arEnum = $rsPropEnums->Fetch()){
                                    $propEnumValue = $arEnum['ID'];
                                }
                            }
                        ?>
                        <div class="row js-confidential-confirm" style="<?=($arResult['CONFIDENTIAL_CLINICS'][$clinic['ID']] ? '' : 'display:none;');?>">
                            <div>
                                <input type="checkbox" name="MS_P_SHOW_CONFIDENTIAL_CONFIRM" id="MS_P_SHOW_CONFIDENTIAL_CONFIRM" value="<?=$propEnumValue;?>"<?php echo (isset($_POST['MS_P_SHOW_CONFIDENTIAL_CONFIRM']) && $_POST['MS_P_SHOW_CONFIDENTIAL_CONFIRM'] == $propEnumValue ? ' checked="checked"' : '');?> />
                                <label for="MS_P_SHOW_CONFIDENTIAL_CONFIRM" class="js-confidential-text"><?=$clinic['PROPERTIES']['CONFIDENTIAL_CONFIRM_TEXT']['~VALUE'];?></label>
                                <?php if(in_array("P_SHOW_CONFIDENTIAL_CONFIRM", $arResult["FORM_ERROR_FIELDS"])): ?>
                                    <div style="color: #ce156d;">Вы должны принять условия на обработку персональных данных</div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                            <div class="row">
                                <div class="label-block">
                                    <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                    <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                </div>
                            </div>
                            <a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
                        <?php endif; ?>
                        <div class="row align-right">
                            <input type="reset" value="Очистить" class="reset-button">

                            <div class="button-holder01">
                            <span class="button01 block-button js-goal js-gasend" data-targetga="event/zapis1/zapis2" data-target="SEND<?php echo $arResult['METRIC_ID'] ? '_'.$arResult['METRIC_ID'] : ''; ?>"><input type="submit" name="form" value="app">Отправить</span>
                            </div>
                        </div>
                    <?endif?>
                </div>
            </div>
            </fieldset>
        </div>
        </div>
    </div>

    <?= $arResult["FORM_FOOTER"] ?>

    <script>
        var confidentialClinics = <?=json_encode($arResult['CONFIDENTIAL_CLINICS']);?>;

        $('#zapisnapriem-popup .popup').show();

        centrateDiv($('#zapisnapriem-popup'));

        $("#zapisnapriem-popup input[name='MS_P_PHONE']").mask("8(999) 999-99-99");

        $.datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3c;Пред',
            nextText: 'След&#x3e;',
            currentText: 'Сегодня',
            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false
        };

        $.datepicker.setDefaults($.datepicker.regional['ru']);

        $("#zapisnapriem-popup input[name='MS_P_DATE']").mask("99.99.9999");

        $('.js-calendar-click').click(function () {
            $(this).find('.js-calendar-input').focus();
        });

        $('.js-time-input').each(function() {
            $(this).mask('99:99');
        });

        $('.js-calendar-input').each(function () {
            var $this = $(this);
            $this.datepicker({
                minDate: 2
            });
        });

        $("#zapisnapriem-popup select[name='MS_P_CLINIC']").on('change', function(){
            var clinicId = $(this).val();

            if(confidentialClinics[clinicId]){
                $('.js-confidential-text').html(confidentialClinics[clinicId]);
                $('.js-confidential-confirm').show();
            }else{
                $('.js-confidential-confirm').hide();
            }

            $('.js-confidential-confirm')
                .find("input[name='MS_P_SHOW_CONFIDENTIAL_CONFIRM']")
                .prop('checked', false)
                .trigger('change');
        });

        $('.js-confidential-text').on('click', 'a', function(e){
            e.preventDefault();

            window.open($(this).attr('href'), '_blank');
        });
</script>
