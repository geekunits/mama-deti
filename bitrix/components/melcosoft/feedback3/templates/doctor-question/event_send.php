<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

CModule::IncludeModule("iblock");

$arSendFields["CITY"] = $GLOBALS["CURRENT_CITY"]["~NAME"];
$arSendFields["CITY_EMAIL"] = $GLOBALS["CURRENT_CITY"]["~EMAIL"];

if (intval($arParams["ASET_P_CLINIC"])>0)
{
	$rsElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>2,"ID"=>$arParams["ASET_P_CLINIC"]),false,false,array("IBLOCK_ID","ID","PROPERTY_EMAIL"));
	if ($arElement = $rsElement->GetNext())
	{
		$arSendFields["CLINIC_EMAIL"] = $arElement["~PROPERTY_EMAIL_VALUE"];
	}
}

?>