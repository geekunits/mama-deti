<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

CModule::IncludeModule('iblock');

//$GLOBALS["CURRENT_CITY"]
$arResult['DOCTORS'] = array();

$arFilter = array("IBLOCK_ID"=>3,"ACTIVE"=>"Y","PROPERTY_BUTTON_QUESTION"=>1);
if ($_REQUEST["CLINIC"]>0){
	$arFilter["PROPERTY_CLINIC"]=$_REQUEST["CLINIC"];
} else {
	$clinicIds = CMamaDetiAPI::getClinicsId(array('PROPERTY_CITY' => GetCurrentCity()));
	$arFilter['PROPERTY_CLINIC'] = array_unique($clinicIds);
}
if ($_REQUEST["SERVICE"]>0)
	$arFilter["PROPERTY_SERVICES"]= $_REQUEST["SERVICE"];

$rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter,false,false,array("ID","IBLOCK_ID","NAME","PROPERTY_*"));
while ($obElement = $rsElement->GetNextElement())
{
	$arFields = $obElement->GetFields();
	$arResult['DOCTORS'][$arFields["ID"]] = $arFields;
}

?>