<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"ASET_P_CLINIC" => array(
		"NAME" => "ID клиники",
		"TYPE" => "STRING",
	),
	"ASET_P_CITY" => array(
		"NAME" => "ID города",
		"TYPE" => "STRING",
	),
);
?>
