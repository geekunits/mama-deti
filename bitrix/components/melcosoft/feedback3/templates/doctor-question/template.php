<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?= $arResult["FORM_HEADER"] ?>
	<div class="bg">
		&nbsp;
	</div>
	<div class="popup" style="display:none;">
		<div class="popup-sector">
			<div class="popup-frame">
				<a href="#" class="close">close</a>
				<span class="popup-title">Задать вопрос врачу</span>
            <? if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <? endif ?>
				<form method="post">
					<fieldset>
						<div class="form-block">
							<div class="form-rows">
	                        <? if ($arResult["EVENT_SEND_ID"] > 0): ?>
	                        	<div>
	                            	Спасибо, Ваша заявка принята.
	                            </div>
	                        <? else: ?>
								<?if (intval($_REQUEST["DOCTOR"])>0):?>
									<input type="hidden" name="MS_P_DOCTOR" value="<?=intval($_REQUEST["DOCTOR"])?>">
										<div class="row">
								            Врач: <?=$arResult["DOCTORS"][$_REQUEST["DOCTOR"]]["NAME"]?>
										</div>
								<?else:?>
									<div class="row">
			                            <div class="label-block">
			                                <label for="id103">Выберите врача</label>
			                            </div>
	                            		<div class="row-sector">
											<select name="MS_P_DOCTOR">
											<?foreach ($arResult["DOCTORS"] as $arDoctor):?>
													<option value="<?=$arDoctor["ID"]?>"<?if ($arResult["FORM"]["P_DOCTOR"] ==  $arDoctor["ID"]):?> selected="selected"<?endif?>><?=$arDoctor["NAME"]?></option>
											<?endforeach?>
											</select>
	                            		</div>
									</div>
								<?endif?>
								<div class="row">
									<div class="text04">
										<input type="text" name="MS_NAME" placeholder="Ваше имя" value="<?=$arResult["FORM"]["NAME"]?>">
									</div>
								</div>
								<div class="row">
									<div class="text04">
										<input type="text" name="MS_P_PHONE" placeholder="Ваш телефон или e-mail" value="<?=$arResult["FORM"]["P_PHONE"]?>">
									</div>
								</div>
								<div class="row">
									<div class="text04">
										<input type="text" name="MS_P_THEME" placeholder="Тема вопроса" value="<?=$arResult["FORM"]["P_THEME"]?>">
									</div>
								</div>
								<div class="row">
									<div class="textarea01">
										<textarea cols="30" rows="10" name="MS_PREVIEW_TEXT" placeholder="Сообщение"><?=$arResult["FORM"]["PREVIEW_TEXT"]?></textarea>
									</div>
								</div>
								<div class="row align-right">
									<input type="reset" value="Очистить" class="reset-button">
									<div class="button-holder01">
										<span class="button01 block-button"><input type="submit" name="form" value="ask">Отправить</span>
									</div>
								</div>
							<?endif?>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
<?= $arResult["FORM_FOOTER"] ?>
<script>
	$(function() {  
	  setTimeout(function() {$('.popup input, .popup select').styler();  }, 1);
	});

    $('#ask-popup .popup').show();
    centrateDiv($('#ask-popup'));
</script>