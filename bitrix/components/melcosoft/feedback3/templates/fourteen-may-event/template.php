<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php $formId = 'fourteen-may-event'; ?>
<?php echo $arResult["FORM_HEADER"]; ?>
<div class="bg">
    &nbsp;
</div>
<div class="popup" style="display:none">
    <input name="MS_P_URL" type="hidden" value="<?=$arParams['NEWS_ID']?>">
    <div class="popup-sector">
        <div class="popup-frame">
            <a href="#" class="close">close</a>
            <span class="popup-title">Заявка на участие в мероприятии</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
            <fieldset>
                <div class="form-block">
                    <div class="form-rows">
                    <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                        <div class="js-form-sent">
                            Ваша заявка получена!
                            <br><br>
                            Наши сотрудники перезвонят Вам в часы работы клиники.
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <div class="label-block">
                                <label for="MS_NAME">
                                    Ф.И.О.<?php if (in_array("NAME",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_NAME" type="text" placeholder="" value="<?=$arResult["FORM"]["NAME"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_EMAIL">
                                    E-mail<?php if (in_array("P_EMAIL", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_EMAIL" type="text" placeholder="E-mail" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PHONE">
                                    Ваш телефон<?php if (in_array("P_PHONE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_PHONE" class="js-phone-input" type="text" placeholder="Телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_ESCORT">
                                    С кем Вы собираетесь посетить мероприятие<?php if (in_array("P_ESCORT", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="textarea01<?=(in_array("P_ESCORT", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <textarea cols="30" rows="10" name="MS_P_ESCORT" placeholder=""><?=$arResult["FORM"]["P_ESCORT"]?></textarea>
                                </div>
                            </div>
                        </div>

                        <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                            <div class="row">
                                <div class="label-block">
                                    <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                    <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                </div>
                            </div>
                            <a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
                        <?php endif; ?>

                        <div class="row align-right">
                            <input type="reset" value="Очистить" class="reset-button">
                            <div class="button-holder01">
                                <span class="button01 block-button"><input type="submit" name="form" value="<?php echo $formId; ?>">Отправить</span>
                            </div>
                        </div>
                    <?endif?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<?= $arResult["FORM_FOOTER"] ?>

<script>
    $(function() {
        $('#<?php echo $formId; ?>-popup .popup').show();
        centrateDiv($('#<?php echo $formId; ?>-popup'));
        $('#<?php echo $formId; ?>-popup .popup').find('.js-phone-input').mask('8(999) 999-99-99');

        $('#<?php echo $formId; ?>-popup .close, #<?php echo $formId; ?>-popup .bg').click(function (e) {
            e.preventDefault();
            $('#<?php echo $formId; ?>-popup').hide();
        });
    });   
</script>
