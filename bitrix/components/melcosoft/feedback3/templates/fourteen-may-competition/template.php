<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php $formId = 'fourteen-may-competition'; ?>
<?php echo $arResult["FORM_HEADER"]; ?>
<div class="bg">
    &nbsp;
</div>
 <div class="popup" style="display:none">
    <div class="popup-sector">
        <div class="popup-frame">
            <a href="#" class="close">close</a>
            <span class="popup-title">Заявка на участие в розыгрыше бесплатных родов в Клиническом Госпитале Лапино «Мать и дитя»</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
            <fieldset>
                <div class="form-block">
                    <div class="form-rows">
                    <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                        <div class="row js-form-sent">
                            Ваша заявка получена!
                            <br><br>
                            Наши сотрудники перезвонят Вам в часы работы клиники.
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <div class="label-block">
                               <label for="MS_NAME">
                                    Ф.И.О.<?php if (in_array("NAME",$arParams["REQUIRED_FIELDS"])):?><span class="required-star"></span><?php endif; ?>
                               </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_NAME" type="text" placeholder="Алла Борисовна" value="<?=$arResult["FORM"]["NAME"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_BIRTH_DATE">
                                    Дата рождения<?php if (in_array("P_BIRTH_DATE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04 text-calendar js-calendar-click<?=(in_array("P_BIRTH_DATE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input class="js-calendar-input" name="MS_P_BIRTH_DATE" type="text" placeholder="XX.XX.XXXX" value="<?=$arResult["FORM"]["P_BIRTH_DATE"]?>">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PREGNANCY_PERIOD">
                                    Срок беременности<?php if (in_array("P_PREGNANCY_PERIOD", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_PREGNANCY_PERIOD", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_PREGNANCY_PERIOD" type="text" placeholder="" value="<?=$arResult["FORM"]["P_PREGNANCY_PERIOD"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_HAS_CHRONIC">
                                    Наличие хронических заболеваний<?php if (in_array("P_HAS_CHRONIC", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <?php foreach($arResult['PROPERTY_OPTIONS']['HAS_CHRONIC']['OPTIONS'] as $option): ?>
                                    <input type="radio" name="MS_P_HAS_CHRONIC" value="<?php echo $option['ID']; ?>"
                                        <?php if ($arResult['FORM']['P_HAS_CHRONIC'] === $option['ID'] || (empty($arResult['FORM']['P_HAS_CHRONIC']) && $option['DEF'] === 'Y')): ?> checked="checked" <?php endif; ?>
                                    > 
                                    <?php echo $option['VALUE']; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>   

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_BIRTH_COUNT">
                                    Какие роды по счету<?php if (in_array("P_BIRTH_COUNT", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_BIRTH_COUNT", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_BIRTH_COUNT" type="text" placeholder="" value="<?=$arResult["FORM"]["P_BIRTH_COUNT"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_HAS_CESAREAN">
                                    Было ли кесарево сечение<?php if (in_array("P_HAS_CESAREAN", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <?php foreach($arResult['PROPERTY_OPTIONS']['HAS_CESAREAN']['OPTIONS'] as $option): ?>
                                    <input type="radio" name="MS_P_HAS_CESAREAN" value="<?php echo $option['ID']; ?>"
                                        <?php if ($arResult['FORM']['P_HAS_CESAREAN'] === $option['ID'] || (empty($arResult['FORM']['P_HAS_CESAREAN']) && $option['DEF'] === 'Y')): ?> checked="checked" <?php endif; ?>
                                    > 
                                    <?php echo $option['VALUE']; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_BIRTH_TYPE">
                                    Какие роды предстоят<?php if (in_array("P_BIRTH_TYPE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <select name="MS_P_BIRTH_TYPE">
                                    <?php foreach($arResult['PROPERTY_OPTIONS']['BIRTH_TYPE']['OPTIONS'] as $option): ?>
                                         <option value="<?php echo $option['ID']; ?>"<?if ($arResult["FORM"]["P_BIRTH_TYPE"] === $option['ID']):?> selected="selected"<?endif?>>
                                            <?php echo $option['VALUE']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_EMAIL">
                                    E-mail<?php if (in_array("P_EMAIL", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input name="MS_P_EMAIL" type="text" placeholder="E-mail" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="label-block">
                                <label for="MS_P_PHONE">
                                    Ваш телефон<?php if (in_array("P_PHONE", $arParams["REQUIRED_FIELDS"])): ?><span class="required-star"></span><?php endif; ?>
                                </label>
                            </div>
                            <div class="row-sector">
                                <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                    <input class="js-phone-input" name="MS_P_PHONE" type="text" placeholder="Телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                </div>
                            </div>
                        </div>

                        <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                            <div class="row">
                                <div class="label-block">
                                    <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                    <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                </div>
                                <div class="row-sector">
                                    <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                </div>
                            </div>
                            <a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
                        <?php endif; ?>

                        <div class="row align-right">
                            <input type="reset" value="Очистить" class="reset-button">
                            <div class="button-holder01">
                                <span class="button01 block-button"><input type="submit" name="form" value="<?php echo $formId; ?>">Отправить</span>
                            </div>
                        </div>
                    <?endif?>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<?= $arResult["FORM_FOOTER"] ?>

<script>
    $(function() {
        
        $('#<?php echo $formId; ?>-popup .popup').show();
        centrateDiv($('#<?php echo $formId; ?>-popup'));

        $('#<?php echo $formId; ?>-popup .close, #<?php echo $formId; ?>-popup .bg').click(function (e) {
            e.preventDefault();
            $('#<?php echo $formId; ?>-popup').hide();
        });

        
        $("#<?php echo $formId; ?>-popup input[name='MS_P_PHONE']").mask("8(999) 999-99-99");

        if ($('#<?php echo $formId; ?> .js-form-sent').size() > 0) {
            window.reachGoal('send_k_14');
        }
    });
</script>