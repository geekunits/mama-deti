<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?= $arResult["FORM_HEADER"] ?>
    <div class="bg">
        &nbsp;
    </div>
    <div class="popup" style="display:none;">
        <div class="popup-sector">
            <div class="popup-frame">
                <a href="#" class="close">close</a>
                <span class="popup-title">Оставить отзыв</span>
            <?php if ($arResult["FORM_ERROR"]): ?>
                <span class="popup-error"><?= $arResult["FORM_ERROR_MSG"] ?></span>
            <?php endif ?>
                <form method="post">
                    <fieldset>
                        <div class="form-block">
                            <div class="form-rows">
                            <?php if ($arResult["EVENT_SEND_ID"] > 0): ?>
                            <div>
                                Спасибо, Ваш отзыв принят.
                            </div>
                            <?php else: ?>
                                <div class="row">
                                    <div class="text04<?=(in_array("NAME", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input type="text" name="MS_NAME" placeholder="Ваше имя" value="<?=$arResult["FORM"]["NAME"]?>">
                                    </div>
                                </div>

                                <div class="row">
                                    <label for="form-review-type-clinic"><input type="radio" name="review_type" value="clinic"<?=(empty($_REQUEST["review_type"]) || $_REQUEST["review_type"] == 'clinic' ? ' checked="checked"' : '')?> id="form-review-type-clinic"> Отзыв о клинике </label>
                                    <label for="form-review-type-doctor"><input type="radio" name="review_type" value="doctor"<?=(!empty($_REQUEST["review_type"]) && $_REQUEST["review_type"] == 'doctor' ? ' checked="checked"' : '')?> id="form-review-type-doctor"> Отзыв о враче</label>
                                </div>

                                <div class="row">
                                    <div class="label-block">
                                        <label for="id103">Выберите клинику</label>
                                    </div>
                                    <div class="row-sector">
                                        <select name="MS_P_CLINIC"<?=(in_array("P_CLINIC", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error"' : '');?>>
                                            <option value="">Выберите клинику</option>
                                            <?foreach ($arResult["CLINICS"] as $arClinic):?>
                                                <option value="<?=$arClinic["ID"]?>"<?if ($arResult["FORM"]["P_CLINIC"] ==  $arClinic["ID"]):?> selected="selected"<?endif?>><?=$arClinic["NAME"]?></option>
                                            <?endforeach?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row" <?=(!empty($_REQUEST["review_type"]) && $_REQUEST["review_type"] == 'doctor' ? '' : ' style="display:none;"')?> id="div-doctors">
                                    <div class="label-block">
                                        <label for="id103">Выберите врача</label>
                                    </div>
                                    <div class="row-sector">
                                        <select name="MS_P_DOCTOR"<?=(in_array("P_DOCTOR", $arResult["FORM_ERROR_FIELDS"]) ? ' class="error"' : '');?>>
                                            <option value="">Выберите врача</option>
                                            <?foreach ($arResult["CLINIC_DOCTORS"] as $arDoctor):?>
                                                <option value="<?=$arDoctor["ID"]?>"<?if ($arResult["FORM"]["P_DOCTOR"] ==  $arDoctor["ID"]):?> selected="selected"<?endif?>><?=$arDoctor["NAME"]?></option>
                                            <?endforeach?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="textarea01<?=(in_array("PREVIEW_TEXT", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <textarea cols="30" rows="10" name="MS_PREVIEW_TEXT" placeholder="Отзыв"><?=$arResult["FORM"]["PREVIEW_TEXT"]?></textarea>
                                    </div>
                                </div>

                                <div style="margin-top: 40px;">Для получения персонального ответа оставьте, пожалуйста, свои контактные данные</div>

                                <div class="row">
                                    <div class="text04<?=(in_array("P_EMAIL", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input type="text" name="MS_P_EMAIL" placeholder="E-mail" value="<?=$arResult["FORM"]["P_EMAIL"]?>">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="text04<?=(in_array("P_PHONE", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>">
                                        <input type="text" name="MS_P_PHONE" placeholder="Ваш телефон" value="<?=$arResult["FORM"]["P_PHONE"]?>">
                                    </div>
                                </div>

                                <?php if (isset($arResult["CAPTCHA_IMAGE"])): ?>
                                    <div class="row">
                                        <div class="label-block">
                                            <input class="js-captcha_sid" name="captcha_sid" type="hidden" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>">
                                            <img class="js-captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"])?>" width="160" height="40" style="display:block;">
                                        </div>
                                        <div class="row-sector">
                                            <div class="text04<?=(in_array("CAPTCHA", $arResult["FORM_ERROR_FIELDS"]) ? ' error' : '');?>"><input type="text" name="CAPTCHA" value="" placeholder="Код с картинки"/></div>
                                        </div>
                                    </div>
                                    <a href="#" onclick="refreshCaptcha(this); return false;" class="refresh-captcha">Обновить код</a>
                                <?php endif; ?>

                                <div class="row align-right">
                                    <input type="reset" value="Очистить" class="reset-button">
                                    <div class="button-holder01">
                                        <span class="button01 block-button"><input type="submit" name="form" value="ask">Отправить</span>
                                    </div>
                                </div>
                            <?endif?>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
<?= $arResult["FORM_FOOTER"] ?>

<script>
    $(function () {
        setTimeout(function () {
            $('.popup input, .popup select').styler();
        }, 1);

        $('select[NAME="MS_P_CLINIC"]').change(function () {
            $s = $('select[name="MS_P_DOCTOR"]');
            if ($('input:radio[name="review_type"]:checked').val() == 'doctor' && $(this).val()) {
                fillDoctorsList($(this).val());
            } else {
                 $s.find('option').remove();

                 $s.append($("<option></option>")
                    .attr("value", '')
                    .text('Выберите врача'));

                 $s.trigger('refresh');
            }
        });

        $('input:radio[name="review_type"]').change(function () {
            if ($(this).val() == 'doctor') {
                $('select[NAME="MS_P_CLINIC"]').trigger('change');
                $('div#div-doctors').show();
            } else {
                $('select[NAME="MS_P_DOCTOR"]').find('option').remove();
                $('select[NAME="MS_P_DOCTOR"]').trigger('refresh');
                $('div#div-doctors').hide();
            }
        });

        $('#ask-popup .popup').show();
        centrateDiv($('#ask-popup'));

        $("#ask-popup input[name='MS_P_PHONE']").mask("8(999) 999-99-99");

        function fillDoctorsList(clinicId){
            $s = $('select[name="MS_P_DOCTOR"]');

            $.ajax({
                url: "/ajax/search_doctor.php",
                data: {
                    clinic: clinicId
                },
                dataType: 'json'
            }).done(function (data) {
                $s.find('option').remove();

                $s.append($("<option></option>")
                    .attr("value", '')
                    .text('Выберите врача'));

                $.each(data.DOCTORS, function (key, value) {
                    $s.append($("<option></option>")
                        .attr("value",value.ID)
                        .text(value.NAME));
                });

                if ($s.data('default')) {
                    $s.val($s.data('default'));
                }

                $s.trigger('refresh');
            });
        }
    });

</script>
