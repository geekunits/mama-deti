<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php

CModule::IncludeModule('iblock');

//$GLOBALS["CURRENT_CITY"]
$arResult['CLINICS'] = array();

$rsElement = CIBlockElement::GetList(
    array("SORT" => "ASC"),
    array(
        "IBLOCK_ID" => CLINIC_IBLOCK_ID,
        "ACTIVE" => "Y",
        "PROPERTY_CITY" => $GLOBALS["CURRENT_CITY"]["ID"]
    ),
    false,
    false,
    array("ID", "NAME")
);

while ($obElement = $rsElement->GetNextElement()){
	$arFields = $obElement->GetFields();
	$arResult['CLINICS'][$arFields["ID"]] = $arFields;
}

$arResult['CLINIC_DOCTORS'] = array();

if($_REQUEST['review_type'] == 'doctor' && $arResult['FORM']['P_CLINIC']){
    $rsElement = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array(
            "IBLOCK_ID" => DOCTOR_IBLOCK_ID,
            "ACTIVE" => "Y",
            "PROPERTY_CLINIC" => $arResult['FORM']['P_CLINIC']
        ),
        false,
        false,
        array("ID", "NAME")
    );
    
    while ($obElement = $rsElement->GetNextElement()){
        $arFields = $obElement->GetFields();
        $arResult['CLINIC_DOCTORS'][$arFields["ID"]] = $arFields;
    }
}