<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Почтовое событие",
	"DESCRIPTION" => "Форма и отправка почтового события",
	"ICON" => "/images/feedback.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);

?>