<?
$MESS["FORM_FIELDS_ERROR"] = "Required fields are empty";
$MESS["FORM_CAPTCHA_ERROR"] = "Incorrectly entered characters from the image";
$MESS["FORM_INVALID_EMAIL"] = "Incorrect E-Mail";
$MESS["CP_EVENT_NOT_FOUND"] = "Event not found";
?>