<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arParams["IBLOCK_ELEMENT_ACTIVE"] = $arParams["IBLOCK_ELEMENT_ACTIVE"] === "N" ? "N" : "Y";
$arParams["SUCCESS_MESSAGE"] = isset($arParams["SUCCESS_MESSAGE"]) ? $arParams["SUCCESS_MESSAGE"] : "Ваша заявка получена!<br><br>Наши сотрудники перезвонят Вам в часы работы клиники." ;

if ($this->InitComponentTemplate())
    $template = & $this->GetTemplate();
else
    return;

$folderPath = $template->GetFolder();

if (!is_array($arParams["EMAIL_FIELDS"]))
    $arParams["EMAIL_FIELDS"] = array();

global $USER;

$arResult = array();

$arResult["AJAX"] = $_REQUEST['ms_ajax'] == "Y" ? true : false;

$arParams["EVENT_ID"] = intval($arParams["EVENT_ID"]);
$rsEM = CEventMessage::GetByID($arParams["EVENT_ID"]);
if (!$arEM = $rsEM->Fetch()) {
    ShowError(GetMessage("CP_EVENT_NOT_FOUND"));

    return 0;
}

$arResult["EVENT"] = $arEM;

$rsET = CEventType::GetByID($arEM["EVENT_NAME"], LANGUAGE_ID);
if ($arET = $rsET->Fetch()) {
    $arResult["EVENT_TYPE"] = $arET;

    $cnt = preg_match_all("|#(.*)#( *)-( *)(.*)|m", $arET["DESCRIPTION"], $match);
    for ($i = 0; $i < $cnt; $i++) {
        $arResult["EVENT_FIELDS"][$match[1][$i]] = $match[4][$i];
    }
}

$formID = !empty($arParams["FORM_ID"]) ? $arParams["FORM_ID"] : 'feedback' ;

$FORM_SESSION_KEY = 'ms_feedback_3_' . $formID;

if ($arResult["AJAX"] && $_POST[$arParams["FORM_VARS_PREFIX"] . "FORM_ID"] !== $formID)
    return;

if ($arResult["AJAX"])
    $APPLICATION->RestartBuffer();

$arResult["FORM_ERROR"] = false;
$arResult["FORM_ERROR_FIELDS"] = array();
//print_r($_POST[$arParams["FORM_VARS_PREFIX"] . "FORM_ID"] === $FORM_MD5);exit;
//if (check_bitrix_sessid() && $_POST["fl_id"] == $arResult["EVENT"]["ID"]) {
if (isset($_POST) && $_POST[$arParams["FORM_VARS_PREFIX"] . "FORM_ID"] === $formID) {

    $arResult["FORM_SESSION_ID"] = $_POST[$arParams["FORM_VARS_PREFIX"] . "FORM_SESSION_ID"];

    $arEventFields = array();
    foreach ($_POST as $name => $value) {
        if (substr($name, 0, strlen($arParams["FORM_VARS_PREFIX"])) == $arParams["FORM_VARS_PREFIX"]) {
            $name = substr($name, strlen($arParams["FORM_VARS_PREFIX"]));
            if (is_array($value)) {

            } else {

                /*if (defined("BX_UTF"))
                    $value = trim($value);
                else
                    $value = trim(iconv("utf-8", "cp1251", $value));*/

                if (in_array($name, $arParams["EMAIL_FIELDS"])) {
                    if (!check_email($value)) {
                        $arResult["FORM_ERROR"] = true;
                        $arResult["FORM_ERROR_MSG"] = GetMessage("FORM_INVALID_EMAIL");
                        if (!in_array($name, $arResult["FORM_ERROR_FIELDS"]))
                            $arResult["FORM_ERROR_FIELDS"][] = $name;
                    }
                }
            }
            $arEventFields[$name] = $value;
        }
    }

    foreach ($arParams["REQUIRED_FIELDS"] as $fieldName) {
        $fName = $arParams["FORM_VARS_PREFIX"] . $fieldName;
        if (!isset($_POST[$fName]) || strlen(trim($_POST[$fName])) == 0) {
            $arResult["FORM_ERROR"] = true;
            $arResult["FORM_ERROR_MSG"] = GetMessage("FORM_FIELDS_ERROR");
            if (!in_array($fieldName, $arResult["FORM_ERROR_FIELDS"]))
                $arResult["FORM_ERROR_FIELDS"][] = $fieldName;
        }
    }

    $arResult["FORM"] = $arEventFields;

    if ($arParams["USE_GUEST_CAPTCHA"] === "Y" && !$USER->IsAuthorized()) {
        if (!$arResult["FORM_ERROR"] && !$APPLICATION->CaptchaCheckCode($_POST["CAPTCHA"], $_POST["captcha_sid"])) {
            $arResult["FORM_ERROR"] = true;
            $arResult["FORM_ERROR_MSG"] = GetMessage("FORM_CAPTCHA_ERROR");
            $arResult["FORM_ERROR_FIELDS"][] = 'CAPTCHA';
        }
    }

    if (!$arResult["FORM_ERROR"]) {

        if (!function_exists(multiple_files)) {

            function multiple_files(array $_files, $top = TRUE)
            {
                $files = array();
                foreach ($_files as $name => $file) {
                    if ($top)
                        $sub_name = $file['name'];
                    else
                        $sub_name = $name;

                    if (is_array($sub_name)) {
                        foreach (array_keys($sub_name) as $key) {
                            $files[$name][$key] = array(
                                'name' => $file['name'][$key],
                                'type' => $file['type'][$key],
                                'tmp_name' => $file['tmp_name'][$key],
                                'error' => $file['error'][$key],
                                'size' => $file['size'][$key],
                            );
                            $files[$name] = multiple_files($files[$name], FALSE);
                        }
                    } else {
                        $files[$name] = $file;
                    }
                }

                return $files;
            }

        }

        $arLoadProductArray = array();

        $arSendFields = $arResult["FORM"]; //��� CEvent::Send
        if (array_key_exists("EVENT_FIELDS", $arResult)) {
            foreach ($arResult["EVENT_FIELDS"] as $key => $value) {
                if (!array_key_exists($key, $arSendFields))
                    $arSendFields[$key] = "";
            }
        }

        $MS_FILES = multiple_files($_FILES);

        if ($arParams["WRITE_IBLOCK"] == "Y" && CModule::IncludeModule("iblock")) {

            $PROP = array();

            foreach ($arParams as $key => $value) {
                if (substr($key, 0, 5) === "ASET_") {
                    $key = substr($key, 5);
                    if (!isset($arResult["FORM"][$key])) $arResult["FORM"][$key] = $value;
                    /*if (substr($key,0,2) == 'P_')
                        $PROP[substr($key, 2)] = $value; else
                        $arLoadProductArray[$key] = $value;
$arSendFields[$key] = $value;*/
                }
            }

            $properties = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arParams["IBLOCK_ID"]));
            while ($prop_fields = $properties->GetNext()) {

                if (!isset($arResult["FORM"]["P_" . $prop_fields["CODE"]]) && !isset($MS_FILES[$arParams["FORM_VARS_PREFIX"] . "P_" . $prop_fields["CODE"]]))
                    continue;

                if ($prop_fields["PROPERTY_TYPE"] === "L") {

                    if (is_array($arResult["FORM"]["P_" . $prop_fields["CODE"]])) {
                        $arSendFields["P_" . $prop_fields["CODE"]] = array();
                        foreach ($arResult["FORM"]["P_" . $prop_fields["CODE"]] as $ENUM_ID) {
                            $arEnum = CIBlockPropertyEnum::GetByID($ENUM_ID);
                            if ($arEnum) {
                                $arSendFields["P_" . $prop_fields["CODE"]][] = $arEnum["VALUE"];
                            }
                        }
                        $arSendFields["P_" . $prop_fields["CODE"]] = implode(", ", $arSendFields["P_" . $prop_fields["CODE"]]);
                    } else {

                        $arEnum = CIBlockPropertyEnum::GetByID($arResult["FORM"]["P_" . $prop_fields["CODE"]]);
                        if ($arEnum) {
                            $arSendFields["P_" . $prop_fields["CODE"]] = $arEnum["VALUE"];
                        }
                    }
                }
                if ($prop_fields["PROPERTY_TYPE"] === "G") {
                    $rsSection = CIBlockSection::GetByID($arResult["FORM"]["P_" . $prop_fields["CODE"]]);
                    if ($arSection = $rsSection->GetNext()) {
                        $arSendFields["P_" . $prop_fields["CODE"]] = $arSection["NAME"];
                    }
                }
                if ($prop_fields["PROPERTY_TYPE"] === "E") {

                    $arSendFields["P_" . $prop_fields["CODE"]] = "";

                    $rsElement = CIBlockElement::GetList(array(),array("ID"=>$arResult["FORM"]["P_" . $prop_fields["CODE"]]));
                    while ($arElement = $rsElement->GetNext()) {
                        if (!empty($arSendFields["P_" . $prop_fields["CODE"]]))
                            $arSendFields["P_" . $prop_fields["CODE"]].="\r\n";
                        if (!empty($arSendFields["P_" . $prop_fields["CODE"] . "_ADMIN_LINK"]))
                            $arSendFields["P_" . $prop_fields["CODE"] . "_ADMIN_LINK"].="\r\n";
                        if (!empty($arSendFields["P_" . $prop_fields["CODE"] . "_DETAIL_PAGE"]))
                            $arSendFields["P_" . $prop_fields["CODE"] . "_DETAIL_PAGE"].="\r\n";

                        $arSendFields["P_" . $prop_fields["CODE"]] .= $arElement["~NAME"];
                        $arSendFields["P_" . $prop_fields["CODE"] . "_ADMIN_LINK"] .= "http://" . SITE_SERVER_NAME . "/bitrix/admin/" .
                            CIBlock::GetAdminElementEditLink($arElement["IBLOCK_ID"], $arElement["ID"]);
                        $arSendFields["P_" . $prop_fields["CODE"] . "_DETAIL_PAGE"] .= $arElement["~DETAIL_PAGE_URL"];
                    }
                }

                if ($prop_fields["PROPERTY_TYPE"] === "F") { //��������� �������� ���� ����, ������ ���� �������������
                    if (is_array($MS_FILES[$arParams["FORM_VARS_PREFIX"] . "P_" . $prop_fields["CODE"]])) {
                        $PROP[$prop_fields["CODE"]] = Array();

                        if($prop_fields["MULTIPLE"] == "Y"){
                            foreach ($MS_FILES[$arParams["FORM_VARS_PREFIX"] . "P_" . $prop_fields["CODE"]] as $arId => $arFile) {
                                $PROP[$prop_fields["CODE"]]["n" . $arId] = Array(
                                    "VALUE" => $arFile,
                                    "DESCRIPTION" => ""
                                );
                            }
                        }else{
                            $PROP[$prop_fields["CODE"]] = Array(
                                "VALUE" => $MS_FILES[$arParams["FORM_VARS_PREFIX"] . "P_" . $prop_fields["CODE"]],
                                "DESCRIPTION" => ""
                            );
                        }
                    }
                } elseif (isset($arResult["FORM"]["P_" . $prop_fields["CODE"]])) {
                    if ($prop_fields["USER_TYPE"] == "HTML") {
                        $PROP[$prop_fields["CODE"]] = array("VALUE" => array(
                            "TYPE" => "text",
                            "TEXT" => $arResult["FORM"]["P_" . $prop_fields["CODE"]],
                        ));
                    } else
                        $PROP[$prop_fields["CODE"]] = $arResult["FORM"]["P_" . $prop_fields["CODE"]];
                }
            }

            if (((!isset($arSendFields["P_DOCTOR"]) || empty($arSendFields['P_DOCTOR'])) && $arParams['IBLOCK_ID'] == 14)) {
                $arSendFields["P_DOCTOR"] = 'Не заполнено';
            }

            $arFind = array();
            $arReplace = array();
            foreach ($arResult["FORM"] as $key => $value) {
                $arFind[] = "#" . $key . "#";
                $arReplace[] = $value;
            }
            $ELEMENT_NAME = str_replace(
                $arFind, $arReplace, $arParams["ELEMENT_NAME"]
            );

            $el = new CIBlockElement;

            //$PROP = $arResult["FORM"];

            $arLoadProductArray["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
            $arLoadProductArray["NAME"] = $ELEMENT_NAME;
            $arLoadProductArray["ACTIVE"] = $arParams["IBLOCK_ELEMENT_ACTIVE"];

            if (array_key_exists($arParams["FORM_VARS_PREFIX"] . "PREVIEW_PICTURE", $MS_FILES)) {
                $arPicture = $MS_FILES[$arParams["FORM_VARS_PREFIX"] . "PREVIEW_PICTURE"];
                if (array_shift(array_keys($arPicture)) == 0) {
                    foreach ($arPicture as $key => $arFile) {
                        if ($key != 0) {
                            if (!is_set($PROP["MORE_PHOTO"]))
                                $PROP["MORE_PHOTO"] = array();
                            $PROP["MORE_PHOTO"]["n" . count($PROP["MORE_PHOTO"])] = Array(
                                "VALUE" => $arFile,
                                "DESCRIPTION" => "");
                        } else
                            $arLoadProductArray["PREVIEW_PICTURE"] = $arFile;
                    }
                } else {
                    $arLoadProductArray["PREVIEW_PICTURE"] = $arPicture;
                }
            }

            //AddMessage2Log(print_r($arResult["FORM"],true));

            if (array_key_exists("PREVIEW_TEXT", $arResult["FORM"])) {
                $arLoadProductArray["PREVIEW_TEXT"] = $arResult["FORM"]["PREVIEW_TEXT"];
            }

            if (array_key_exists("DETAIL_TEXT", $arResult["FORM"])) {
                $arLoadProductArray["DETAIL_TEXT"] = $arResult["FORM"]["DETAIL_TEXT"];
            }

            $arLoadProductArray["PROPERTY_VALUES"] = $PROP;

            if (file_exists($_SERVER["DOCUMENT_ROOT"] . $folderPath . "/element_add.php"))
                include($_SERVER["DOCUMENT_ROOT"] . $folderPath . "/element_add.php");

            $arResult["ELEMENT_ID"] = $el->Add($arLoadProductArray);

            if (!$arResult["ELEMENT_ID"]) {
                $arResult["FORM_ERROR"] = true;
                $arResult["FORM_ERROR_MSG"] = $el->LAST_ERROR;
            }
        }

        if ($arResult["ELEMENT_ID"] > 0) {
            $rsIBlock = CIBlock::GetByID($arParams["IBLOCK_ID"]);
            $arIBlock = $rsIBlock->GetNext();

            $rsElement = CIBlockElement::GetByID($arResult["ELEMENT_ID"]);
            if ($arElement = $rsElement->GetNext()) {
                    $arSendFields["ELEMENT_EDIT_LINK"] = "http://" . SITE_SERVER_NAME . "/bitrix/admin/" .
                        CIBlock::GetAdminElementEditLink($arParams["IBLOCK_ID"], $arResult["ELEMENT_ID"]);
                $arSendFields["ELEMENT_DETAIL_PAGE_URL"] = $arElement["DETAIL_PAGE_URL"];
            }
        }

        if (!$arResult["FORM_ERROR"]) {

            $arSendFields["EVENT_DATE"] = ConvertTimeStamp(time(), "FULL");

            foreach ($arParams["EVENT_DEF_FIELDS"] as $key => $value) {
                if (!array_key_exists($key, $arSendFields))
                    $arSendFields[$key] = $value;
            }

            if (file_exists($_SERVER["DOCUMENT_ROOT"] . $folderPath . "/event_send.php"))
                include($_SERVER["DOCUMENT_ROOT"] . $folderPath . "/event_send.php");

            $id = CEvent::Send(
                $arResult["EVENT"]["EVENT_NAME"], SITE_ID, $arSendFields, "Y", $arResult["EVENT"]["ID"]
            );

            if(isset($arParams['SEND_CLIENT_EMAIL_MESSAGE']) && $arParams['SEND_CLIENT_EMAIL_MESSAGE'] == 'Y'){
                $arParams["CLIENT_EMAIL_EVENT"] = intval($arParams["CLIENT_EMAIL_EVENT"]);
                $rsEM = CEventMessage::GetByID($arParams["CLIENT_EMAIL_EVENT"]);

                if ($arEventMessage = $rsEM->Fetch()) {
                    CEvent::Send(
                        $arEventMessage["EVENT_NAME"], SITE_ID, $arSendFields, "Y", $arEventMessage["ID"]
                    ); //print_r($arEM["EVENT_NAME"]); print_r($arSendFields); print_r($arParams['CLIENT_EMAIL_EVENT']);exit;
                }
            }

            $arResult["EVENT_SEND_ID"] = $id;

            $allowClinics =
                [
                    423, // Клиника «Мать и дитя» Кунцево
                    424, // Клиника «Мать и дитя» Юго-Запад
                    425, // Клиника «Мать и дитя» Савёловская (ранее «Клиника ЗДОРОВЬЯ»)
                    5944982, // Клиника «Мать и дитя» Ходынское поле
                    429, // Клиника «Мать и дитя» Санкт-Петербург
                    13173254, // Поликлиника Клинического Госпиталя «Лапино» в г. Одинцово (Филиал)
                    427, // Клиника «Мать и дитя» Новогиреево
                    5129, // Клиника «Мать и дитя» Рязань
                    434, // Клиника «Мать и дитя» Ярославль
                    8515031, // Клиника «Мать и дитя» Кострома
                    13854826, // Клиника «Мать и дитя» Владимир
                    5126, // Госпиталь «Мать и дитя» Уфа
                    428, // Клиника «Мать и дитя» Уфа
                    430, // Клиника «Мать и дитя» Пермь
                    422, // Перинатальный Медицинский Центр «Мать и дитя»
                    13884276, // Клиника «Мать и дитя» Тюмень
                ];

            if (
                in_array(intval($arResult["FORM"]["P_CLINIC"]), $allowClinics)
                &&
                (
                    $arResult["EVENT"]["EVENT_NAME"] == 'CALLBACK'
                    ||
                    $arResult["EVENT"]["EVENT_NAME"] == 'ZAPIS_NA_PRIEM'
                )
            ) {
                // Отправляемые данные по SOAP
                $callDateTime = date("Y-m-d H:i:s");

                $soapData = array(
                        "FIO"           => $arResult["FORM"]["NAME"],
                        "Phone"         => preg_replace("/^8/", '', preg_replace("/[^0-9]/", '', $arResult["FORM"]["P_PHONE"])),
                        "ClinicName"    => $arResult["CLINIC_NAME"] ? : getClinicNameById($arResult["FORM"]["P_CLINIC"]),
                        "Comment"       => $arResult["FORM"]["PREVIEW_TEXT"],
                        // дата + желаемое клиентом время звонка
                        //"CallDateTime"  => $callDateTime
                );

                if(!empty($arResult["FORM"]["P_DOCTOR"])){
                    $doc = CMamaDetiAPI::getDoctors(array('ID' => $arResult["FORM"]["P_DOCTOR"]));

                    $soapData["Doctor"] = $doc[0]['NAME'];
                }

                if ($arResult["FORM"]["P_CALLBACK_TIME"] == 'none') {
                    $arResult["FORM"]["P_CALLBACK_TIME"] = null;
                }
                $soapData['CallDateTime'] =
                    (
                        $arResult["FORM"]["P_CALLBACK_TIME"]
                        ?
                        date("Y-m-d") . ' ' . trim($arResult["FORM"]["P_CALLBACK_TIME"]).':00'
                        :
                        ''
                    );

                if ($arResult["EVENT"]["EVENT_NAME"] == 'ZAPIS_NA_PRIEM') {
                    $soapData['AppointmentDateTime'] =
                        date("Y-m-d", strtotime(str_replace('.', '-', trim($arResult["FORM"]["P_DATE"]))))
                        . ' ' .
                        trim($arResult["FORM"]["P_TIME"]).':00';
                }

                //AddMessage2Log('FORM:  '.print_r($arResult["FORM"],true));
                //AddMessage2Log('$soapData:  '.print_r($soapData,true)); //$soap = 'test';

                $soap = callToSoapService($arResult["EVENT"]["EVENT_NAME"], $soapData);

                // Обновление данных в записе об отправке данных по SOAP
                $elUpdate = new CIBlockElement;
                $arLoadProductArray = Array(
                    "DETAIL_TEXT"    => "SOAP code: " . json_encode($soap)
                );
                $res = $elUpdate->Update($arResult["ELEMENT_ID"], $arLoadProductArray);
            }

            if ($arParams["REDIRECT_IF_SUCCESS"] === "Y") {
                LocalRedirect($arParams["REDIRECT_URL"]);
            }

            unset($arResult["FORM"]);
        }
    }
} else {
    $arResult["FORM_SESSION_ID"] = uniqid() . uniqid();

    foreach ($arParams as $key => $value) {
        if (substr($key, 0, 5) === "ASET_") {
            $key = substr($key, 5);
            $arResult["FORM"][$key] = $value;
        }
    }
}

foreach ($arResult["FORM"] as $key => $value) {
    $arResult["FORM"][$key] = htmlspecialcharsEx($value);
    $arResult["FORM"]["~" . $key] = $value;
}

if ($arParams["USE_GUEST_CAPTCHA"] === "Y" && !$USER->IsAuthorized()) {
    $arResult["CAPTCHACode"] = $APPLICATION->CaptchaGetCode();
    $arResult["CAPTCHA_IMAGE"] = "<input type=\"hidden\" name=\"captcha_sid\" value=\"" . htmlspecialcharsbx($arResult["CAPTCHACode"]) . "\" /><img src=\"/bitrix/tools/captcha.php?captcha_sid=" . htmlspecialcharsbx($arResult["CAPTCHACode"]) . "\" width=\"180\" height=\"40\" />";
}
ob_start();
?>
<form name="<?= $formID ?>" id="<?= $formID ?>"<?php if (!empty($arParams["FORM_CLASS"])): ?> class="<?= $arParams["FORM_CLASS"] ?>"<?php endif; ?>
    action="<?= POST_FORM_ACTION_URI ?>" method="POST" enctype="multipart/form-data">
<?php /*
      <?= bitrix_sessid_post() ?>
      <input type="hidden" name="fl_id" id="fl_id" value="<?= $arResult["EVENT"]["ID"] ?>" />
     */
?>
    <input type="hidden" name="<?= $arParams["FORM_VARS_PREFIX"] ?>FORM_ID"
           id="<?= $arParams["FORM_VARS_PREFIX"] ?>FORM_ID" value="<?= $formID ?>">
    <input type="hidden" name="<?= $arParams["FORM_VARS_PREFIX"] ?>FORM_SESSION_ID"
           id="<?= $arParams["FORM_VARS_PREFIX"] ?>FORM_SESSION_ID" value="<?= $arResult["FORM_SESSION_ID"] ?>">
<?php
$arResult["FORM_HEADER"] = ob_get_contents();
ob_end_clean();
$arResult["FORM_FOOTER"] = "</form>";
$this->IncludeComponentTemplate();
?>

<?php
if ($arResult["AJAX"])
    die();
?>
