<?
$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_IBLOCK"] = "Инфоблок";
$MESS["IBLOCK_PAGE_ELEMENT_COUNT"] = "Количество элементов на странице";
$MESS["IBLOCK_FILTER_NAME_IN"] = "Имя массива со значениями фильтра для фильтрации элементов";
$MESS["IBLOCK_CACHE_FILTER"] = "Кешировать при установленном фильтре";
$MESS["IBLOCK_CLINIC_ID"] = "ID Клиники";
$MESS["CP_BCS_CACHE_GROUPS"] = "Учитывать права доступа";
$MESS["DISABLE_PRICE_LIST_AJAX"] = "Отключить ajax запрос компонента";
?>