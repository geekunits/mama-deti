<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$selClinic = (isset($_REQUEST["clinic"]) ? intval($_REQUEST["clinic"]) : 0);
$searchText = (isset($_REQUEST["q"]) ? htmlspecialcharsbx($_REQUEST["q"]) : "");
?>
<form action="<?=POST_FORM_ACTION_URI?>">
<div class="search-panel search-price-list">
<fieldset>
	<div class="search-field">
	<select name="clinic">
		<option value="0">Все клиники</option>
	<?foreach ($arResult["CLINICS"] as $arClinicSection):?>
		<option value="<?=$arClinicSection["CLINIC"]["ID"]?>"<?if ($selClinic == $arClinicSection["CLINIC"]["ID"]):?> selected="selected"<?endif?>><?=$arClinicSection["CLINIC"]["NAME"]?></option>
	<?endforeach?>
	</select>
	</div>
	<br>
	<div class="search-field">

                    <div class="search-sector active">
                        <div class="search-input">
                            <div class="align-left">
                                <ul class="search-list">
                                                                    </ul>
                            </div>
                            <?/*<input type="button" class="button06 bt_search_doctor" value="">*/?>

                            <div class="text03">
                                <input type="text" class="input_search_text" name="q" placeholder="Название услуги" value="<?=$searchText?>">
                            </div>
                        </div>
                        <div id="search_result" class="search-drop" style="display: none; margin: 0px 0px 0px -9999px; z-index: auto;">
                            <div class="drop-frame">
                                <div class="scrollpane">
                                    <ul class="search-listing"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

	<div class="button-holder">
		<span class="button02 block-button"><input type="submit">найти</span>
	</div>
</fieldset>
</div>
</form>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?if (count($arResult["ITEMS"])):

/*echo '<pre>';
print_r($arResult["ITEMS"][0]);
echo '</pre>';*/

$arCurNav = array();
$CUR_CLINIC = false;

?>
<ul>
<?foreach ($arResult["ITEMS"] as $arItem):?>
	<?if ($arItem["CLINIC_SECTION"] != $CUR_CLINIC):?>
		<?=str_repeat('</ul></li>',count($arCurNav));?>
		<?
			$arCurNav = array();
			$CUR_CLINIC = $arItem["CLINIC_SECTION"];
			$arClinic = $arResult["CLINICS"][$CUR_CLINIC]["CLINIC"];
		?>
		<li class="priceClinicName"><p class="clinicName"><a href="<?=$arClinic["DETAIL_PAGE_URL"]?>"><?=$arClinic["NAME"]?></a></p></li>
	<?endif?>
	<?
	$arDiff = array_diff_assoc($arItem["SECTION_NAV"],$arCurNav);
	if (count($arDiff))
	{
		$idx = key($arDiff);
		echo str_repeat('</ul></li>',count($arCurNav)-$idx);
		$arCurNav = $arItem["SECTION_NAV"];
		for ($i=$idx;$i<count($arCurNav);$i++)
		{
			$arSection = $arResult["SECTIONS"][$arCurNav[$i]];
?>
	<li>
		<p class="priceSectionName<?=$arSection["DEPTH_LEVEL"]?>"><?=$arSection["NAME"]?></p>
		<ul>
<?
		}
	}
	?>
	<li>
		<span class="priceElementName"><?=$arItem["NAME"]?></span>
		<?if ($arItem["PROPERTY_PRICE_VALUE"]):?>
			<span class="priceElementPrice"><?=number_format($arItem["PROPERTY_PRICE_VALUE"],0,',',' ')?> р</span>
			<span class="priceC"></span>
		<?endif?>
	</li>
<?endforeach?>
<?=str_repeat('</ul></li>',count($arCurNav));?>
</ul>
<?else:?>
<b>По данному запросу услуг не найдено</b>
<?endif?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
