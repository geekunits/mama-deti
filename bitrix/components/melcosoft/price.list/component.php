<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;
/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

/*************************************************************************
    Processing of received parameters
*************************************************************************/
if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

if (empty($arParams["FILTER_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"])) {
    $arrFilter = array();
} else {
    global ${$arParams["FILTER_NAME"]};
    $arrFilter = ${$arParams["FILTER_NAME"]};
    if(!is_array($arrFilter))
        $arrFilter = array();
}

$arParams["CLINIC_ID"] = is_array($arParams["CLINIC_ID"]) ? $arParams["CLINIC_ID"] : [$arParams["CLINIC_ID"]];

$arParams["PAGE_ELEMENT_COUNT"] = intval($arParams["PAGE_ELEMENT_COUNT"]);
if($arParams["PAGE_ELEMENT_COUNT"]<=0)
    $arParams["PAGE_ELEMENT_COUNT"]=100;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"]!="N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

    $arNavParams = array(
        "nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
        "bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
        "bShowAll" => $arParams["PAGER_SHOW_ALL"],
    );
    $arNavigation = CDBResult::GetNavParams($arNavParams);
    if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
        $arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];

$arParams['CACHE_GROUPS'] = trim($arParams['CACHE_GROUPS']);
if ('N' != $arParams['CACHE_GROUPS'])
    $arParams['CACHE_GROUPS'] = 'Y';

$ajaxRequest = ($arParams["DISABLE_PRICE_LIST_AJAX"] != "Y" && isset($_REQUEST["ajax"]) && $_REQUEST["ajax"] === "y");

/*************************************************************************
            Work with cache
*************************************************************************/
if ($ajaxRequest)
    ob_start();

if ($this->StartResultCache(false, array($ajaxRequest,$arrFilter,($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation))) {
    if (!CModule::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));

        return;
    }

    $arResult["ITEMS"] = array();
    $arResult["CLINICS"] = array();
    $arResult["SECTIONS"] = array();

    $arSelect = array("ID","NAME","DEPTH_LEVEL","IBLOCK_SECTION_ID","SECTION_PAGE_URL","CODE","UF_*");
    $rsSection = CIBlockSection::GetList(Array("left_margin"=>"asc"), array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y"),false,$arSelect);
    while ($arSection = $rsSection->GetNext()) {
        if ($arSection["DEPTH_LEVEL"] == 1)
            $arResult["CLINICS"][$arSection["ID"]] = $arSection; else
            $arResult["SECTIONS"][$arSection["ID"]] = $arSection;
    }
    foreach ($arResult["CLINICS"] as $key=>$arSection) {
        if (intval($arSection["UF_CLINIC"])>0) {
            $rsElement = CIBlockElement::GetByID($arSection["UF_CLINIC"]);
            if ($obElement = $rsElement->GetNextElement()) {
                $arElement = $obElement->GetFields();
                $arElement["PROPERTIES"] = $obElement->GetProperties();

                $arResult["CLINICS"][$key]["CLINIC"] = $arElement;
            }
        }
    }

    $arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ACTIVE"=>"Y");

    if ($arParams["CLINIC_ID"] && empty($arrFilter['SECTION_ID'])) {
        $CLINIC_SECTION_ID = [];
        foreach ($arResult["CLINICS"] as $arClinicSection) {
            if (in_array($arClinicSection["CLINIC"]["ID"], $arParams["CLINIC_ID"])) {
                $CLINIC_SECTION_ID[] = $arClinicSection["ID"];
            }
        }

        if ($CLINIC_SECTION_ID) {
            $CLINIC_SUBSECTION_IDS = [];

            $foundClinic = false;
            foreach ($arResult["SECTIONS"] as $arSection) {
                if ($foundClinic && $arSection['DEPTH_LEVEL'] <= 2) {
                    $foundClinic = false;
                }

                if (!$foundClinic) {
                    $foundClinic = in_array($arSection['IBLOCK_SECTION_ID'], $CLINIC_SECTION_ID);
                }

                if ($foundClinic) {
                    $CLINIC_SUBSECTION_IDS[] = $arSection['ID'];
                }
            }

            $arFilter["SECTION_ID"] = $CLINIC_SUBSECTION_IDS;
        }
    }

    $arElements = array();
    $rsElement = CIBlockElement::GetList(array("SORT"=>"ASC"),array_merge($arrFilter, $arFilter),false,false,array("ID","IBLOCK_SECTION_ID","PROPERTY_PRICE"));
    while ($arElement = $rsElement->Fetch()) {
        $arElements[$arElement["IBLOCK_SECTION_ID"]][] = $arElement["ID"];
    }
    $arItems = array();

    foreach ($arResult["SECTIONS"] as $arSection) {
        if (array_key_exists($arSection["ID"],$arElements)) {
            foreach ($arElements[$arSection["ID"]] as $ID)
                $arItems[] = array("ID"=>$ID);
        }
    }
    $rsData = new CDBResult;
    $rsData->InitFromArray($arItems);
    $rsData->NavStart($arNavigation["SIZEN"], $arNavigation["SHOW_ALL"], $arNavigation["PAGEN"]);

    $arID = array();

    while ($arData = $rsData->Fetch()) {
        $arID[] = $arData["ID"];
    }
    $arSelect = array("ID","XML_ID","NAME","IBLOCK_SECTION_ID","PROPERTY_PRICE");

    $arElements = array();
    
    if (empty($arID)) {
        $arID[] = 0;
    }
    $rsElements = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ID"=>$arID),false,false,$arSelect);

    while ($arElement = $rsElements->GetNext()) {
        $arSectionNav = array();
        $SECTION_ID = $arElement["IBLOCK_SECTION_ID"];
        while (array_key_exists($SECTION_ID, $arResult["SECTIONS"])) {
            $arSectionNav[] = $SECTION_ID;
            $SECTION_ID = $arResult["SECTIONS"][$SECTION_ID]["IBLOCK_SECTION_ID"];
        }

        $arSectionNav = array_reverse($arSectionNav);
        $arElement["SECTION_NAV"] = $arSectionNav;
        $arElement["CLINIC_SECTION"] = $arResult["SECTIONS"][$arSectionNav[0]]["IBLOCK_SECTION_ID"];

        $arElements[$arElement["ID"]] = $arElement;
    }
    $arResult["ITEMS"] = array();
    foreach ($arID as $ID) {
        if (array_key_exists($ID,$arElements))
            $arResult["ITEMS"][] = $arElements[$ID];
    }
    $arResult["NAV_STRING"] = $rsData->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
    $arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
    //$arResult["NAV_RESULT"] = $rsData;

    $this->SetResultCacheKeys(array(
        "NAV_CACHED_DATA",
    ));

    if ($ajaxRequest)
        $this->IncludeComponentTemplate("ajax"); else
        $this->IncludeComponentTemplate();

    $this->EndResultCache();
}
if ($ajaxRequest) {
    $data = ob_get_contents();
    $APPLICATION->RestartBuffer();
    while(ob_end_clean());
    echo $data;
    //require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_after.php");
    die();
}

$this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);
