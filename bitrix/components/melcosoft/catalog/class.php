<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/*DEMO CODE for component inheritance
CBitrixComponent::includeComponentClass("bitrix::news.base");
class CBitrixCatalogSmartFilter extends CBitrixNewsBase
*/

class CMelcosoftCatalogUtils
{
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    public function executeComponent()
    {
        return parent::executeComponent();
    }

    public static function resolveComponentEngine(CComponentEngine $engine, $pageCandidates, &$arVariables)
    {
        global $APPLICATION, $CACHE_MANAGER;
        $component = $engine->GetComponent();
        if ($component) {
            $iblock_id = intval($component->arParams["IBLOCK_ID"]);
            $brand_iblock_id = intval($component->arParams["BRAND_IBLOCK_ID"]);
        } else {
            $iblock_id = 0;
            $brand_iblock_id = 0;
        }

        foreach ($pageCandidates as $page => $arVariablesTmp) {
            if (isset($arVariablesTmp["FILTER_CODE"])) {
                    $rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $iblock_id, "CODE" => $arVariablesTmp["SECTION_CODE"]));
                    if ($arSection = $rsSection->GetNext()) {

                        if (substr($arVariablesTmp["FILTER_CODE"],0,6) == 'brand-') {
                            $rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $brand_iblock_id, "CODE" => substr($arVariablesTmp["FILTER_CODE"],6)));
                            if ($arElement = $rsElement->GetNext()) {
                                $arVariablesTmp["SECTION_ID"] = $arSection["ID"];
                                $arVariablesTmp["SECTION_CODE"] = $arSection["CODE"];
                                $arVariablesTmp["BRAND_ID"] = $arElement["ID"];
                                $arVariablesTmp["BRAND_CODE"] = $arElement["CODE"];
                                $arVariables = $arVariablesTmp;
                                return $page;
                            }

                        }
                    }

            }
            if (isset($arVariablesTmp["BRAND_CODE"])) {
                $rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $brand_iblock_id, "CODE" => $arVariablesTmp["BRAND_CODE"]));
                if ($arElement = $rsElement->GetNext()) {
                    $arVariablesTmp["BRAND_ID"] = $arElement["ID"];
                    $arVariablesTmp["BRAND_CODE"] = $arElement["CODE"];
                    $arVariables = $arVariablesTmp;
                    return $page;
                }
            }
            if (isset($arVariablesTmp["SECTION_CODE"])) {
                $rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $iblock_id, "CODE" => $arVariablesTmp["SECTION_CODE"]));
                if ($arSection = $rsSection->GetNext()) {
                    $arVariables = $arVariablesTmp;
                    return $page;
                }
            }

            if (isset($arVariablesTmp["ELEMENT_CODE"])) {
                $rsElement = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $iblock_id, "CODE" => $arVariablesTmp["ELEMENT_CODE"]));
                if ($arElement = $rsElement->GetNext()) {
                    $arVariables = $arVariablesTmp;
                    return $page;
                }
            }
        }

//print_r($pageCandidates);
//print_r($arVariables);

    }
}

?>