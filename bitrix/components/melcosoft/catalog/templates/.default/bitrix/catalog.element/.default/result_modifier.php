<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

$arEmptyPreview = false;
$strEmptyPreview = $this->GetFolder().'/images/no_photo.png';
if (file_exists($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview))
{
	$arSizes = getimagesize($_SERVER['DOCUMENT_ROOT'].$strEmptyPreview);
	if (!empty($arSizes))
	{
		$arEmptyPreview = array(
			'SRC' => $strEmptyPreview,
			'WIDTH' => intval($arSizes[0]),
			'HEIGHT' => intval($arSizes[1])
		);
	}
	unset($arSizes);
}
unset($strEmptyPreview);

global $APPLICATION;

$arResult["SECTION"]["PATH"][] = array(
    "NAME" => $arResult["NAME"]
); //��������� � ������� ������ �������

//��������� ���� ������� ���������, ��������� ��������, ������� ��� �������� ���������

foreach ($arResult["PROPERTIES"] as &$arProp) {
    if ($arProp["PROPERTY_TYPE"] === "E" && !empty($arProp["VALUE"])) {

        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "CODE",
            "XML_ID",
            "NAME",
            "ACTIVE",
            "DATE_ACTIVE_FROM",
            "DATE_ACTIVE_TO",
            "SORT",
            "PREVIEW_TEXT",
            "PREVIEW_TEXT_TYPE",
            "DETAIL_TEXT",
            "DETAIL_TEXT_TYPE",
            "DATE_CREATE",
            "CREATED_BY",
            "TIMESTAMP_X",
            "MODIFIED_BY",
            "TAGS",
            "IBLOCK_SECTION_ID",
            "DETAIL_PAGE_URL",
            "LIST_PAGE_URL",
            "DETAIL_PICTURE",
            "PREVIEW_PICTURE",
            "PROPERTY_*",
        );

        $arSort = array();

        $arFilter = array(
            "IBLOCK_ID" => $arProp["LINK_IBLOCK_ID"],
            "ID" => $arProp["VALUE"],
            "ACTIVE" => "Y",
        );

        if (CModule::IncludeModule("catalog"))
            $IS_CATALOG = is_array(CCatalog::GetByID($arProp["LINK_IBLOCK_ID"]));

        if ($IS_CATALOG) {
            $arResultPrices = CIBlockPriceTools::GetCatalogPrices($arProp["LINK_IBLOCK_ID"], $arParams["PRICE_CODE"]);

            foreach ($arResultPrices as &$value) {
                $arSelect[] = $value["SELECT"];
                $arFilter["CATALOG_SHOP_QUANTITY_" . $value["ID"]] = 1;
            }

            $arConvertParams = array();
        }

        $arValues = array();
        $rsElement = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
        while ($obElement = $rsElement->GetNextElement()) {

	    $arElement = $obElement->GetFields();
            $arElement["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
            $arElement["DETAIL_PICTURE"] = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
	    $arElement["PROPERTIES"] = $obElement->GetProperties();

            if ($IS_CATALOG) {
                $arElement["PRICES"] = CIBlockPriceTools::GetItemPrices($arElement["IBLOCK_ID"], $arResult["CAT_PRICES"], $arElement, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
                $arElement["CAN_BUY"] = CIBlockPriceTools::CanBuy($arElement["IBLOCK_ID"], $arResultPrices, $arElement);
                $arElement["BUY_URL"] = htmlspecialcharsbx($APPLICATION->GetCurPageParam($arParams["ACTION_VARIABLE"] . "=BUY&" . $arParams["PRODUCT_ID_VARIABLE"] . "=" . $arElement["ID"], array($arParams["PRODUCT_ID_VARIABLE"], $arParams["ACTION_VARIABLE"])));
                $arElement["ADD_URL"] = htmlspecialcharsbx($APPLICATION->GetCurPageParam($arParams["ACTION_VARIABLE"] . "=ADD2BASKET&" . $arParams["PRODUCT_ID_VARIABLE"] . "=" . $arElement["ID"], array($arParams["PRODUCT_ID_VARIABLE"], $arParams["ACTION_VARIABLE"])));
            }

            if ($arProp["MULTIPLE"] === "Y")
                $arValues[] = $arElement; else
                $arValues = $arElement;
        }
        $arProp["VALUE"] = $arValues;
    }


    if ($arProp["PROPERTY_TYPE"] === "F") {
        if ($arProp["MULTIPLE"] === "Y") {
            $arValues = array();
            foreach ($arProp["VALUE"] as $fileID) {
                $arValues[] = CFile::GetFileArray($fileID);
            }

            $arProp["VALUE"] = $arValues;
        } else {
            $arProp["VALUE"] = CFile::GetFileArray($arProp["VALUE"]);
        }
    }


    unset($arProp);
}

//�����


if ($arParams["RESIZE_PREVIEW_ELEMENT"] == "Y" && is_array($arResult["PREVIEW_PICTURE"])) {
    $arFileTmp = CFile::ResizeImageGet(
        $arResult["PREVIEW_PICTURE"], array("width" => $arParams["RESIZE_PREVIEW_ELEMENT_WIDTH"], "height" => $arParams["RESIZE_PREVIEW_ELEMENT_HEIGHT"]), BX_RESIZE_IMAGE_PROPORTIONAL, true
    );
    $arResult["PREVIEW_PICTURE"] = array(
        "SRC" => $arFileTmp["src"],
        "~SRC" => $arResult["PREVIEW_PICTURE"]["SRC"],
        "WIDTH" => $arFileTmp["width"],
        "HEIGHT" => $arFileTmp["height"],
    );
}

if ($arParams["RESIZE_DETAIL_ELEMENT"] == "Y" && is_array($arResult["DETAIL_PICTURE"])) {
    $arFileTmp = CFile::ResizeImageGet(
        $arResult["DETAIL_PICTURE"], array("width" => $arParams["RESIZE_DETAIL_ELEMENT_WIDTH"], "height" => $arParams["RESIZE_DETAIL_ELEMENT_HEIGHT"]), intval($arParams['RESIZE_DETAIL_ELEMENT_METHOD']), true
    );
    if ($arParams["RESIZE_THUMBNAIL"] == "Y") {
        $arFileThumb = CFile::ResizeImageGet(
            $arResult["DETAIL_PICTURE"], array("width" => $arParams["RESIZE_THUMBNAIL_WIDTH"], "height" => $arParams["RESIZE_THUMBNAIL_HEIGHT"]), intval($arParams['RESIZE_THUMBNAIL_METHOD']), true
        );
        $arResult["DETAIL_PICTURE"]["THUMB"] = $arFileThumb["src"];
    }
    $arResult["DETAIL_PICTURE"]["~SRC"] = $arResult["DETAIL_PICTURE"]["SRC"];
    $arResult["DETAIL_PICTURE"]["SRC"] = $arFileTmp["src"];
    $arResult["DETAIL_PICTURE"]["WIDTH"] = $arFileTmp["width"];
    $arResult["DETAIL_PICTURE"]["HEIGHT"] = $arFileTmp["height"];
}

if ($arParams["RESIZE_MORE_PHOTO"] == "Y") {
    $arMorePhoto = array();
    foreach ($arResult["MORE_PHOTO"] as $arFile) {
        $arFileTmp = CFile::ResizeImageGet(
            $arFile, array("width" => $arParams["RESIZE_MORE_PHOTO_WIDTH"], "height" => $arParams["RESIZE_MORE_PHOTO_HEIGHT"]), intval($arParams['RESIZE_MORE_PHOTO_METHOD']), true
        );
        if ($arParams["RESIZE_THUMBNAIL"] == "Y") {
            $arFileThumb = CFile::ResizeImageGet(
                $arFile, array("width" => $arParams["RESIZE_THUMBNAIL_WIDTH"], "height" => $arParams["RESIZE_THUMBNAIL_HEIGHT"]), intval($arParams['RESIZE_THUMBNAIL_METHOD']), true
            );
            $arMorePhoto[] = array(
                "SRC" => $arFileTmp["src"],
                "~SRC" => $arFile["SRC"],
                "THUMB" => $arFileThumb["src"],
                "WIDTH" => $arFileTmp["width"],
                "HEIGHT" => $arFileTmp["height"],
            );
        } else {
            $arMorePhoto[] = array(
                "SRC" => $arFileTmp["src"],
                "~SRC" => $arFile["SRC"],
                "WIDTH" => $arFileTmp["width"],
                "HEIGHT" => $arFileTmp["height"],
            );
        }
    }

    $arResult["MORE_PHOTO"] = $arMorePhoto;
}
//echo '<pre>'; print_r($arResult); echo '</pre>';


?>