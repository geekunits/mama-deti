<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $USER_FIELD_MANAGER;

foreach($arResult["SECTIONS"] as $key=>$arSection)
{
	if ($arSection["DEPTH_LEVEL"] == 1)
	{
		$rsElement = CIBlockElement::GetByID($arSection["UF_CLINIC"]);
		if ($arElement = $rsElement->GetNext())
		{
			$arResult["SECTIONS"][$key]["CLINIC"] = $arElement;
		}
	} else
	{
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"!PROPERTY_PRICE" => false, 
			"SECTION_ID" => $arSection["ID"],
			"INCLUDE_SUBSECTIONS" => "Y");
	
		$arResult["SECTIONS"][$key]["ELEMENT_CNT"] = CIBlockElement::GetList(array(),$arFilter,array());
		if ($arResult["SECTIONS"][$key]["ELEMENT_CNT"] == 0)
			unset($arResult["SECTIONS"][$key]);
	}
}

foreach($arResult["SECTIONS"] as &$arSection)
{
	$arSection["CODE"] = Cutil::translit($arSection['~NAME'],"ru",array("replace_space"=>"-","replace_other"=>"-"));
	if ($arSection["DEPTH_LEVEL"]>1)
	{
		$dbRes = CIBlockSection::GetNavChain($arParams["IBLOCK_ID"],$arSection["ID"],array("ID"));
		if ($arRes = $dbRes->Fetch())
		{
			$dbRes = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"ID"=>$arRes["ID"]),false,array("UF_CLINIC"));
			if ($arRes = $dbRes->Fetch())
			{
				$rsElement = CIBlockElement::GetByID($arRes["UF_CLINIC"]);
				if ($arElement = $rsElement->GetNext())
				{
					$arSection["SECTION_PAGE_URL"] = str_replace(array("#CLINIC_CODE#","#SECTION_CODE#"),array($arElement["CODE"],$arSection["CODE"]),$arParams["SECTION_URL"]);
				}
			}
		}
	}
}
if (isset($arSection))
	unset($arSection);

//echo $arParams["SECTION_URL"];

?>