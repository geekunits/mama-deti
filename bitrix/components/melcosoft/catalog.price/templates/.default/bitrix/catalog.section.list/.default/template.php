<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="priceListWrapper">
<?
foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li class="priceClinicName" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
<?
//echo '<pre>'; print_r($arSection["CLINIC"]); echo '</pre>';
?>
		<p class="clinicName"><a class="clinic-price-expand" href="<?=$arParams["SEF_FOLDER"].$arSection["CLINIC"]["CODE"]?>/list/"><?=$arSection["CLINIC"]["NAME"]?></a></p>
		<p class="clinicFullPrice"><a href="<?=$arSection["SECTION_PAGE_URL"]?>">Посмотреть полный прайс</a></p>
		<ul style="display:none;"></ul>
	</li>
<?endforeach;?>
</ul>