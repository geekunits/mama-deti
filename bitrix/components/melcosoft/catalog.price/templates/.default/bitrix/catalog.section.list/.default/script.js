jQuery(document).ready(function($){
	$('.clinic-price-expand').click(function(e)
	{
		e.preventDefault();

		var list = $(this).closest('li').find('ul');

		console.log(list);

		$.ajax({url: $(this).attr('href')+'?ajax_price=y', context: list}).done(function(data)
		{
			data = '<div>'+data+'</div>';
			this.html($(data).find('div.ajax-price-list ul').html());
			this.slideDown();
		});

		return false;
	});
});