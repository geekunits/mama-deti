<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult["SECTIONS"] as $key=>$arSection)
{
	if ($arSection["DEPTH_LEVEL"] == 1)
	{
		$rsElement = CIBlockElement::GetByID($arSection["UF_CLINIC"]);
		if ($arElement = $rsElement->GetNext())
		{
			$arResult["SECTIONS"][$key]["CLINIC"] = $arElement;
			$arResult["SECTIONS"][$key]["SECTION_PAGE_URL"] = str_replace("#CLINIC_CODE#",$arElement["CODE"],$arResult["SECTIONS"][$key]["SECTION_PAGE_URL"]);
		}
	} else
	{
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"!PROPERTY_PRICE" => false, 
			"SECTION_ID" => $arSection["ID"],
			"INCLUDE_SUBSECTIONS" => "Y");
	
		$arResult["SECTIONS"][$key]["ELEMENT_CNT"] = CIBlockElement::GetList(array(),$arFilter,array());
		if ($arResult["SECTIONS"][$key]["ELEMENT_CNT"] == 0)
			unset($arResult["SECTIONS"][$key]);
	}
}
?>