<?
include($_SERVER["DOCUMENT_ROOT"].$templateFolder.'/filter.php');

CModule::IncludeModule("iblock");

$arFilter = array(
	"IBLOCK_ID"=>$arParams["IBLOCK_CLINICS_ID"],
	"ID" => $arResult["VARIABLES"]["CLINIC_ID"],
	);

$rsElement = CIBlockElement::GetList(array(),$arFilter,false,false,array("ID","NAME","CODE"));
if (!$arCurClinic = $rsElement->Fetch())
{
	@define("ERROR_404", "Y");
	CHTTP::SetStatus("404 Not Found");
	return;
}

$APPLICATION->AddCHainItem($arCurClinic["NAME"]);

global $arrFilter;
$arrFilter = array(
	"!PROPERTY_PRICE" => false,
	"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
	"INCLUDE_SUBSECTIONS" => "Y",
);
?>
<?$APPLICATION->IncludeComponent("melcosoft:price.list", "", Array(
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],	// ��� ���������
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],	// ��������
	"FILTER_NAME" => "arrFilter",	// ��� ������� �� ���������� ������� ��� ���������� ���������
	"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],	// ���������� ��������� �� ��������
	"AJAX_MODE" => "N",	// �������� ����� AJAX
	"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
	"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
	"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"CACHE_FILTER" => "Y",	// ���������� ��� ������������� �������
	"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
	"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
	"PAGER_TITLE" => $arParams["PAGER_TITLE"],
	"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
	"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
	"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
	"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
	"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
	"AJAX_OPTION_ADDITIONAL" => "",	// �������������� �������������
	"SEF_FOLDER" => $arParams["SEF_FOLDER"],
	),
	$component
);?>
