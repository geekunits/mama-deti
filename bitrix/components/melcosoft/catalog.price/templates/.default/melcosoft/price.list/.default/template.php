<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?/*<form action="<?=POST_FORM_ACTION_URI?>">*/?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?if (count($arResult["ITEMS"])):

/*echo '<pre>';
print_r($arParams);
echo '</pre>';*/

$arCurNav = array();
$CUR_CLINIC = false;

?>
<ul class="pricelist-list">
<?foreach ($arResult["ITEMS"] as $arItem):?>
	<?if ($arItem["CLINIC_SECTION"] != $CUR_CLINIC):?>
		<?=str_repeat('</ul></li>',count($arCurNav));?>
		<?
			$arCurNav = array();
			$CUR_CLINIC = $arItem["CLINIC_SECTION"];
			$arClinic = $arResult["CLINICS"][$CUR_CLINIC]["CLINIC"];
		?>
		<li class="priceClinicName"><p class="clinicName"><a href="<?=$arParams["SEF_FOLDER"]?><?=$arClinic["CODE"]?>/price/"><?=$arClinic["NAME"]?></a></p></li>
	<?endif?>
	<?
	$arDiff = array_diff_assoc($arItem["SECTION_NAV"],$arCurNav);
	if (count($arDiff))
	{
		$idx = key($arDiff);
		echo str_repeat('</ul></li>',count($arCurNav)-$idx);
		$arCurNav = $arItem["SECTION_NAV"];
		for ($i=$idx;$i<count($arCurNav);$i++)
		{
			$arSection = $arResult["SECTIONS"][$arCurNav[$i]];
?>
	<li>
		<p class="priceSectionName<?=$arSection["DEPTH_LEVEL"]?>"><?=$arSection["NAME"]?></p>
		<ul>
<?
		}
	}
	?>
	<li>
		<span class="priceElementName"><?=$arItem["NAME"]?></span>
		<?if ($arItem["PROPERTY_PRICE_VALUE"]):?>
			<span class="priceElementPrice"><?=number_format($arItem["PROPERTY_PRICE_VALUE"],0,',',' ')?> р</span>
			<span class="priceC"></span>
		<?endif?>
	</li>
<?endforeach?>
<?=str_repeat('</ul></li>',count($arCurNav));?>
</ul>
<?else:?>
<b>По данному запросу услуг не найдено</b>
<?endif?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
