<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


class CPriceListUtils
{
    public static function resolveComponentEngine(CComponentEngine $engine, $pageCandidates, &$arVariables)
    {
        $component = $engine->GetComponent();
        if (!$component)
            return false;
        $IBLOCK_ID = intval($component->arParams["IBLOCK_ID"]);
        $IBLOCK_CLINICS_ID = intval($component->arParams["IBLOCK_CLINICS_ID"]);
        foreach ($pageCandidates as $page => $arVariablesTmp) {
            if (isset($arVariablesTmp["CLINIC_CODE"])) {
                CModule::IncludeModule("iblock");

                $dbRes = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $IBLOCK_CLINICS_ID, "=CODE" => $arVariablesTmp["CLINIC_CODE"]), false, array("nTopCount" => 1), array("ID"));
                if (!$arRes = $dbRes->Fetch())
                    continue;

                $rsSection = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $IBLOCK_ID, "UF_CLINIC" => $arRes["ID"]), false, array("ID", "LEFT_MARGIN", "RIGHT_MARGIN"));
                if (!$arSection = $rsSection->Fetch())
                    continue;

                $arVariablesTmp["CLINIC_ID"] = $arRes["ID"];
                $arVariablesTmp["CLINIC_SECTION_ID"] = $arSection["ID"];

                if (isset($arVariablesTmp["SECTION_CODE"])) {
                    $arFilter = array(
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "ACTIVE" => "Y",
                        "LEFT_MARGIN" => $arSection["LEFT_MARGIN"] + 1,
                        "RIGHT_MARGIN" => $arSection["RIGHT_MARGIN"],
                    );

                    $obCache = new CPHPCache();
                    if ($obCache->InitCache(60 * 60 * 24, "sef-section-".serialize($arFilter), "/mamadeti/pricefilter")) {
                        $vars = $obCache->GetVars();
                        $arSEFMap = $vars[0];
                    } else {
                        $obCache->StartDataCache();

                        $rsSection = CIBlockSection::GetList(array("left_margin" => "asc"), $arFilter, false, array("ID", "NAME"));
                        while ($arSection = $rsSection->Fetch()) {
                            $CODE = Cutil::translit($arSection['NAME'], "ru", array("replace_space" => "-", "replace_other" => "-"));
                            $arSEFMap[$CODE] = $arSection["ID"];
                        }

                        if (defined("BX_COMP_MANAGED_CACHE")) {
                            global $CACHE_MANAGER;
                            $CACHE_MANAGER->StartTagCache("/mamadeti/pricefilter");
                            $CACHE_MANAGER->RegisterTag("iblock_id_" . $IBLOCK_ID);
                            $CACHE_MANAGER->EndTagCache();
                        }
                        $obCache->EndDataCache(array($arSEFMap));
                    }

                    if (array_key_exists($arVariablesTmp["SECTION_CODE"], $arSEFMap))
                    {
                        $arVariablesTmp["SECTION_ID"] = $arSEFMap[$arVariablesTmp["SECTION_CODE"]];
                        $arVariables = $arVariablesTmp;
                        return $page;
                    }

                } else
                {
                    $arVariables = $arVariablesTmp;
                    return $page;
                }
            }
        }
        return false;
        list($pageID, $arVariables) = each($pageCandidates);
        return $pageID;
    }
}

?>