<?
CModule::IncludeModule("iblock");
if ($this->StartResultCache())
{
	$resel = CIBlockElement::GetList(Array("NAME"=>"asc"), Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y"), false, false, array("ID", "NAME"));
	while ($arresel = $resel->GetNext()){
		$arResult["ITEMS"][] = $arresel;
	}
	if ( count($arResult["ITEMS"])*40>198) $arResult["HEIGHT"] = "198px";
	else $arResult["HEIGHT"] = (count($arResult["ITEMS"])*40)."px";
	
	$this->IncludeComponentTemplate();
}

if ($_REQUEST["form"] == "ask"){
	$docId = "";
	if (!empty($_POST["doc"])){
		$resel = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>3, "=NAME"=>$_POST["doc"]), false, false, array("ID"));
		if ($arresel = $resel->GetNext()) $docId = $arresel["ID"];
	}
	
	if (empty($_POST["name"])) $_POST["name"] = "Гость";
	
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 10,  
		"ACTIVE_FROM" => date("d.m.Y H:i:s"),	
		"NAME" => $_POST["name"],
		"DETAIL_TEXT" => $_POST["text"],
		"PROPERTY_VALUES" => array(
			"CITY" => GetCurrentCity(),
			"PHONE" => $_POST["phone"],
			"THEME" => $_POST["theme"],
			"DOCTOR_NAME" => $_POST["doc"],
			"DOCTOR" => $docId
		)     
	);
	$el = new CIBlockElement;
	if ($ID = $el->Add($arLoadProductArray)) {
		$result = "Ваш вопрос успешно добавлен, №".$ID;
		$arFieldSend = array(
			"DOC_NAME" => $_POST["doc"],
			"NAME" => $_POST["name"],			
			"PHONE" => $_POST["phone"],
			"CITY" => GetNameCity(GetCurrentCity()),
			"THEME" => $_POST["theme"],			
			"TEXT" => $_POST["text"],
			"LINK" => "http://".SITE_SERVER_NAME."/bitrix/admin/iblock_element_edit.php?ID=".$ID."&type=contentsite&lang=ru&IBLOCK_ID=10"
		);
		CEvent::Send("NEW_QUESTION_DOCTOR", "s1", $arFieldSend);
	}	
	else $result = "Ошибка при добавлении вопроса, заполните все поля и попробуйте еще раз";
?>
	<div class="popup-holder active" id="success-popup">
		<div class="bg">
			&nbsp;
		</div>
		<div class="popup">
			<div class="popup-sector">
				<div class="popup-frame">
					<a href="#" class="close">close</a>
					<div class="complete-block">
						<?=$result?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?	
}
?>