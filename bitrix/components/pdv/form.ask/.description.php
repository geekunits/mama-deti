<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Задать вопрос врачу",
    "DESCRIPTION" => "Форма задания вопроса врачу",
    "ICON" => "/images/form.gif",
    "PATH" => array(
        "ID" => "pdv",
    ),
);
?>
