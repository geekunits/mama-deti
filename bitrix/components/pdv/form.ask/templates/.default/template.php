<div class="popup-holder active" id="ask-popup" style="display:none;">
	<div class="bg">
		&nbsp;
	</div>
	<div class="popup">
		<div class="popup-sector">
			<div class="popup-frame">
				<a href="#" class="close">close</a>
				<span class="popup-title">Задать вопрос врачу</span>
				<span class="content-title">Введите ФИО врача</span>
				<form method="post">
					<fieldset>
						<div class="form-block">
							<div class="form-rows">
								<div class="row">
									<div class="search-sector">
										<div class="search-input">
											<input type="button" class="button09" value=" ">
											<div class="text03">
												<input type="text" name="doc" value="Спиридонова Алена Игоревна">
											</div>
										</div>
										<div class="search-drop">
											<div class="drop-frame">
												<div class="scrollpane" style="height:<?=$arResult["HEIGHT"]?>">
													<ul class="search-listing">
														<?foreach($arResult["ITEMS"] as $arItem){?>
														<li>
															<a href="#" class="title"><span class="black-color"><?=$arItem["NAME"]?></span></a>
														</li>
														<?}?>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="text04">
										<input type="text" name="name" value="Ваше имя">
									</div>
								</div>
								<div class="row">
									<div class="text04">
										<input type="text" name="phone" value="Ваш телефон или e-mail">
									</div>
								</div>
								<div class="row">
									<div class="text04">
										<input type="text" name="theme" value="Тема вопроса">
									</div>
								</div>
								<div class="row">
									<div class="textarea01">
										<textarea cols="30" rows="10" name="text">Сообщение</textarea>
									</div>
								</div>
								<div class="row align-right">
									<input type="reset" value="Очистить" class="reset-button">
									<div class="button-holder01">
										<span class="button01 block-button"><input type="submit" name="form" value="ask">Отправить</span>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>