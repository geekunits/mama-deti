<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Выбор города",
    "DESCRIPTION" => "Форма выбора города",
    "ICON" => "/images/form.gif",
    "PATH" => array(
        "ID" => "pdv",
    ),
);
?>
