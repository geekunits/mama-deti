<?php
global $APPLICATION;
CModule::IncludeModule("iblock");
if ($this->StartResultCache())
{
	$resel = CIBlockElement::GetList(Array("NAME"=>"asc"), Array("IBLOCK_ID"=>7, "ACTIVE"=>"Y"), false, false, array("ID", "NAME"));
	$count_city = $resel->SelectedRowsCount();
	while ($arItem = $resel->GetNext()){
		$arItem['URL'] = CityManager::getInstance()->getUrlByCityId($arItem['ID']);
		$arResult["ITEMS"][] = $arItem;
	}
}

$CITY_ID = GetCurrentCity();
if (!$CITY_ID) $arResult["CLASS"] = " city";
else $arResult["CLASS"] = "";

$this->IncludeComponentTemplate();
?>