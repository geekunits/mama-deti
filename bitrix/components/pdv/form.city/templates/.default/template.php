<?php
    $currentCityId = GetCurrentCity();
?>

<?php foreach ($arResult["ITEMS"] as $arItem) {?>
    <?php
        if ($currentCityId == $arItem['ID']) {
            continue;
        }
    ?>
    <a href="#" data-id="<?=$arItem["ID"]?>" data-link="<?php echo $arItem['URL']; ?>" class="js-city-selector-link"><?=$arItem["NAME"]?></a>
<?}?>
