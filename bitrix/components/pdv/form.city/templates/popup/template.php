<div class="popup-holder active<?=$arResult["CLASS"]?>" id="city-popup" style="display:none;">
	<div class="bg">
		&nbsp;
	</div>
	<div class="popup">
		<div class="popup-sector">
			<div class="popup-frame">
				<a href="#" class="close">close</a>
				<span class="popup-title">Выберите ваш город</span>
				<div class="popup-cols">
					<?
					$i=0; $let = "";
					foreach ($arResult["ITEMS"] as $arItem) {
					?>
						<?if($i%11==0){?>
						<div class="col">
							<ul class="city-list">
						<?}?>	
							<?if (mb_substr($arItem["NAME"],0,1,'UTF-8')!=$let){?>
								<?if($i%11!=0){?>
									</ul>
								</li>
								<?}?>
								<li>
									<span class="city-marker"><?=mb_substr($arItem["NAME"],0,1,'UTF-8')?></span>
									<ul>
							<?}?>	
										<li>
											<a href="<?php echo $arItem['URL']; ?>" data-id="<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></a>
										</li>
						<?$i++;
						if($i%11==0){?>
							</ul>
						</div>
						<?}
						$let = mb_substr($arItem["NAME"],0,1,'UTF-8');?>
					<?}?>
					<?if($count_city%11!=0)?></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="popup-holder city-holder active<?=$arResult["CLASS"]?>" id="complete-popup" style="display:none;">
	<div class="bg">
		&nbsp;
	</div>
	<div class="popup">
		<div class="popup-sector">
			<div class="popup-frame">
				<a href="#" class="close">close</a>
				<div class="complete-block">
					<span class="popup-title align-center">ВЫ СОБИРАЕТЕСЬ ИЗМЕНИТЬ РЕГИОН НА: <span class="black-color">Москва</span></span>
					<div class="align-center">
						<a href="#" class="edit-link edit_city">Изменить</a>
						<div class="button-holder01">
							<span class="button01 block-button"><input class="city_success" type="submit" value=" ">Подтвердить</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>