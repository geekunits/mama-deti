<?
CModule::IncludeModule("iblock");
	
if ($this->StartResultCache())
{
	$resel = CIBlockElement::GetList(Array("NAME"=>"asc"), Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y"), false, false, array("ID", "NAME"));
	while ($arresel = $resel->GetNext()){
		$arResult["ITEMS"][] = $arresel;
	}
	if ( count($arResult["ITEMS"])*40+40>198) $arResult["HEIGHT"] = "198px";
	else $arResult["HEIGHT"] = (count($arResult["ITEMS"])*40+40)."px";
	
	$this->IncludeComponentTemplate();
}

if ($_REQUEST["form"] == "callback"){	
	if (empty($_POST["name"])) $_POST["name"] = "Гость";
	
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 15, 
		"ACTIVE_FROM" => date("d.m.Y H:i:s"),	
		"NAME" => $_POST["name"],
		"PREVIEW_TEXT" => $_POST["text"],
		"PROPERTY_VALUES" => array(			
			"PHONE" => $_POST["phone"],
			"CLINIC_NAME" => $_POST["clinic"],
			"CITY" => GetCurrentCity(),
		)     
	);
	$el = new CIBlockElement;
	if ($ID = $el->Add($arLoadProductArray)) {
		$result = "Ваш заказ звонка успешно добавлен";
		$arFieldSend = array(
			"NAME" => $_POST["name"],			
			"PHONE" => $_POST["phone"],	
			"CITY" => GetNameCity(GetCurrentCity()),
			"CLINIC" => $_POST["clinic"],
			"TEXT" => $_POST["text"]
		);
		CEvent::Send("NEW_CALLBACK", "s1", $arFieldSend);
	}	
	else $result = "Ошибка при заказе звонка, заполните все поля и попробуйте еще раз";
?>	
	<div class="popup-holder active" id="success-popup">
		<div class="bg">
			&nbsp;
		</div>
		<div class="popup">
			<div class="popup-sector">
				<div class="popup-frame">
					<a href="#" class="close">close</a>
					<div class="complete-block">
						<?=$result?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?	
}
?>