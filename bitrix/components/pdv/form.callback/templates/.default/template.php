<div class="popup-holder active" id="callback-popup" style="display:none;">
	<div class="bg">
		&nbsp;
	</div>
	<div class="popup">
		<div class="popup-sector">
			<div class="popup-frame">
				<a href="#" class="close">close</a>
				<span class="popup-title">Перезвоните мне</span>
				<form method="post">
					<fieldset>
						<div class="form-block">
							<div class="form-rows">
								<div class="row">
									<div class="label-block">
										<label for="id100">Ваше имя</label>
									</div>
									<div class="row-sector">
										<div class="text04">
											<input name="name" type="text" id="id100" value="Иван">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="label-block">
										<label for="id101">Ваш телефон</label>
									</div>
									<div class="row-sector">
										<div class="text04">
											<input name="phone" type="text" id="id101" value="Телефон или e-mail для связи">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="label-block">
										<label>Выберите клинику</label>
									</div>
									<div class="row-sector">
										<div class="search-sector">
											<div class="search-input">
												<input type="button" class="button09" value="">
												<div class="text03">
													<input name="clinic" type="text" value="Любая клиника" class="black-color">
												</div>
											</div>
											<div class="search-drop">
												<div class="drop-frame">
													<div class="scrollpane" style="height:<?=$arResult["HEIGHT"]?>">
														<ul class="search-listing">
															<li>
																<a href="#" class="title"><span class="black-color">Любая клиника</span></a>
															</li>
															<?foreach ($arResult["ITEMS"] as $arItem){?>
															<li>
																<a href="#" class="title"><span class="black-color"><?=$arItem["NAME"]?></span></a>
															</li>
															<?}?>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="textarea01">
										<textarea name="text" cols="30" rows="10">Комментарий</textarea>
									</div>
								</div>
								<div class="row align-right">
									<input type="reset" value="Очистить" class="reset-button">
									<div class="button-holder01">
										<span class="button01 block-button"><input type="submit" name="form" value="callback">Отправить</span>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>	