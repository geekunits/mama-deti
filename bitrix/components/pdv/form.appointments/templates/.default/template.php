<div class="popup-holder active" id="appointments-popup" style="display:none;">
	<div class="bg">
		&nbsp;
	</div>
	<div class="popup">
		<div class="popup-sector">
			<div class="popup-frame">
				<a href="#" class="close">close</a>
				<span class="popup-title">Заявка на запись на прием</span>
				<p>Данная форма является ЗАЯВКОЙ на запись на прием. Для подтверждения факта записи на прием с Вами свяжутся наши сотрудники.</p>
				<form method="post" name="cform">
					<fieldset>
						<div class="form-block">
							<div class="form-rows">
								<div class="row">
									<div class="label-block">
										<label for="id100">Ваше имя</label>
									</div>
									<div class="row-sector">
										<div class="text04">
											<input name="name" type="text" id="id100" value="Иван">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="label-block">
										<label for="id101">Ваш телефон или e-mail</label>
									</div>
									<div class="row-sector">
										<div class="text04">
											<input name="phone" type="text" id="id101" value="Телефон или e-mail для связи">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="label-block">
										<label>Выберите клинику</label>
									</div>
									<div class="row-sector">
										<div class="search-sector">
											<div class="search-input">
												<input type="button" class="button09" value="">
												<div class="text03">
													<input name="clinic" type="text" value="Любая клиника" class="black-color clinic_appoint">
												</div>
											</div>
											<div class="search-drop">
												<div class="drop-frame">
													<div class="scrollpane" style="height:<?=$arResult["CLINICS"]["HEIGHT"]?>">
														<ul class="search-listing">
															<li>
																<a href="#" class="title"><span class="black-color">Любая клиника</span></a>
															</li>
															<?foreach($arResult["CLINICS"]["ITEMS"] as $arItem){?>
															<li>
																<a href="#" class="title"><span class="black-color"><?=$arItem["NAME"]?></span></a>
															</li>
															<?}?>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="label-block">
										<label for="id103">Укажите дату</label>
									</div>
									<div class="row-sector">
										<div class="search-sector">
											<div class="search-input">
											<?$APPLICATION->IncludeComponent("bitrix:main.calendar","appointments",Array(
												 "SHOW_INPUT" => "N",
												 "FORM_NAME" => "cform",
												 "INPUT_NAME" => "date",
												 "INPUT_NAME_FINISH" => "",
												 "INPUT_VALUE" => "",
												 "INPUT_VALUE_FINISH" => "", 
												 "SHOW_TIME" => "N",
												 "HIDE_TIMEBAR" => "Y"
												)
											);?>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="label-block">
										<label>Выберите направление</label>
									</div>
									<div class="row-sector">
										<div class="search-sector">
											<div class="search-input">
												<input type="button" class="button09" value=" ">
												<div class="text03">
													<input name="service" type="text" value="Любое направление" class="black-color">
												</div>
											</div>
											<div class="search-drop">
												<div class="drop-frame">
													<div class="scrollpane service_appoint" style="height:<?=$arResult["SERVICE"]["HEIGHT"]?>">
														<ul class="search-listing">
															<li>
																<a href="#" class="title"><span class="black-color">Любое направление</span></a>
															</li>
															<?foreach($arResult["SERVICE"]["ITEMS"] as $arItem){?>
															<li>
																<a href="#" class="title"><span class="black-color"><?=$arItem["NAME"]?></span></a>
															</li>
															<?}?>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="textarea01">
										<textarea name="text" cols="30" rows="10">Комментарий</textarea>
									</div>
								</div>
								<div class="row align-right">
									<input type="reset" value="Очистить" class="reset-button">
									<div class="button-holder01">
										<span class="button01 block-button"><input type="submit" name="form" value="app">Отправить</span>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
					<input type="hidden" name="doc" value="">
				</form>
			</div>
		</div>
	</div>
</div>