<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Записаться на приема",
    "DESCRIPTION" => "Форма записи на прием",
    "ICON" => "/images/form.gif",
    "PATH" => array(
        "ID" => "pdv",
    ),
);
?>
