<?
CModule::IncludeModule("iblock");
if ($this->StartResultCache())
{
	$resel = CIBlockElement::GetList(Array("NAME"=>"asc"), Array("IBLOCK_ID"=>2, "ACTIVE"=>"Y"), false, false, array("ID", "NAME"));
	while ($arresel = $resel->GetNext()){
		$arResult["CLINICS"]["ITEMS"][] = $arresel;
	}

	$ressect = CIBlockSection::GetList(Array("NAME"=>"asc"), Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y"), false, array("ID", "NAME"));
	while ($arressect = $ressect->GetNext()){
		$arResult["SERVICE"]["ITEMS"][] = $arressect;
	}
	
	if ( count($arResult["CLINICS"]["ITEMS"])*40+40>198) $arResult["CLINICS"]["HEIGHT"] = "198px";
	else $arResult["CLINICS"]["HEIGHT"] = (count($arResult["CLINICS"]["ITEMS"])*40+40)."px";

	if ( count($arResult["SERVICE"]["ITEMS"])*40+40>198) $arResult["SERVICE"]["HEIGHT"] = "198px";
	else $arResult["SERVICE"]["HEIGHT"] = (count($arResult["SERVICE"]["ITEMS"])*40+40)."px";
	
	$this->IncludeComponentTemplate();
}

if ($_REQUEST["form"] == "app"){
	$clinicId = ""; $serviceId = "";
	if (!empty($_POST["clinic"])){
		$resel = CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>2, "=NAME"=>$_POST["clinic"]), false, false, array("ID"));
		if ($arresel = $resel->GetNext()) $clinicId = $arresel["ID"];
	}

	if (!empty($_POST["service"])){
		$resel = CIBlockSection::GetList(Array(), Array("IBLOCK_ID"=>1, "=NAME"=>$_POST["service"]), false, array("ID"));
		if ($arresel = $resel->GetNext()) $serviceId = $arresel["ID"];
	}
	
	if (empty($_POST["name"])) $_POST["name"] = "Гость";
	
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 14,  
		"ACTIVE_FROM" => date("d.m.Y H:i:s"),	
		"NAME" => $_POST["name"],
		"PREVIEW_TEXT" => $_POST["text"],
		"PROPERTY_VALUES" => array(
			"CITY" => GetCurrentCity(),
			"PHONE" => $_POST["phone"],
			"DATE" => $_POST["date"],
			"CLINIC_NAME" => $_POST["clinic"],
			"CLINIC" => $clinicId,
			"SERVICE_NAME" => $_POST["service"],
			"SERVICE" => $serviceId,
			"DOCTOR" => $_POST["doc"]
		)     
	);
	$el = new CIBlockElement;
	if ($ID = $el->Add($arLoadProductArray)) {
		$result = "Ваша запись успешно добавлена, №".$ID;
		
		$docname = "";
		if (!empty($_POST["doc"])) {
			$resel = CIBlockElement::GetByID($_POST["doc"]);
			if ($arresel = $resel->GetNext()) $docname = $arresel['NAME'];
		}
		$arFieldSend = array(
			"NAME" => $_POST["name"],			
			"PHONE" => $_POST["phone"],
			"CITY" => GetNameCity(GetCurrentCity()),
			"CLINIC" => $_POST["clinic"],
			"DATE" => $_POST["date"],
			"SERVICE" => $_POST["service"],
			"DOCTOR" => $docname,
			"TEXT" => $_POST["text"],
		);
		CEvent::Send("NEW_APPOINTMENTS", "s1", $arFieldSend);
	}	
	else $result = "Ошибка при добавлении записи, заполните все поля и попробуйте еще раз";
?>
	<div class="popup-holder active" id="success-popup">
		<div class="bg">
			&nbsp;
		</div>
		<div class="popup">
			<div class="popup-sector">
				<div class="popup-frame">
					<a href="#" class="close">close</a>
					<div class="complete-block">
						<?=$result?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?	
}
?>