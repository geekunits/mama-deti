<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Цены");
?><?$APPLICATION->IncludeComponent("melcosoft:catalog.price", "catalog-price", Array(
    "IBLOCK_TYPE" => "contentsite",	// Тип инфоблока
    "IBLOCK_ID" => "31",	// Инфоблок
    "IBLOCK_CLINICS_TYPE" => "content",	// Тип инфоблока клиник
    "IBLOCK_CLINICS_ID" => "2",	// Инфоблок клиник
    "SEF_MODE" => "Y",	// Включить поддержку ЧПУ
    "SEF_FOLDER" => "/price-list2/",	// Каталог ЧПУ (относительно корня сайта)
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
    "PAGE_ELEMENT_COUNT" => "10000",	// Количество элементов на странице
    "PAGER_TEMPLATE" => "mamadeti",	// Шаблон постраничной навигации
    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
    "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
    "PAGER_TITLE" => "Товары",	// Название категорий
    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "SEF_URL_TEMPLATES" => array(
        "clinics" => "",
        "full_price" => "#CLINIC_CODE#/price/",
        "section_price" => "#CLINIC_CODE#/#SECTION_CODE#/",
        "section_list" => "#CLINIC_CODE#/list/",
    ),
    'CITY_ID' => GetCurrentCity(),
    ),
    false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
