<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

global $arrMenuSectionFilter;
global $arrMenuElementFilter;
$arrMenuSectionFilter = array();
$arrMenuElementFilter = array();

$CITY_ID = GetCurrentCity();
$clinicFilter = array("PROPERTY_CITY"=>$CITY_ID);
if ($_REQUEST['CLINIC_CODE']) {
    $clinicFilter['=CODE'] = $_REQUEST['CLINIC_CODE'];
    $urlSuffix = 'clinic/'.htmlspecialchars($_REQUEST['CLINIC_CODE']).'/';
} else {
    $urlSuffix = '';
}

$arServicesID = CMamaDetiAPI::getClinicsServicesID($clinicFilter);
$arServiceSectionID = CMamaDetiAPI::getServicesIblockSection($arServicesID);
$arrMenuSectionFilter["ID"] = $arServiceSectionID;
$arrMenuElementFilter["=ID"] = $arServicesID;
//var_dump($clinicFilter,count($arServicesID),count($arServiceSectionID));

if ($arServicesID) {
    $aMenuLinksExt=$APPLICATION->IncludeComponent("melcosoft:menu.sections", "", array(
        "IS_SEF" => "Y",
        "SEF_BASE_URL" => "/services/",
        "SECTION_PAGE_URL" => "#SECTION_CODE#/",
        "DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#/",
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => "1",
        "DEPTH_LEVEL" => "10",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "SECTION_FILTER_NAME" => "arrMenuSectionFilter",
        "ELEMENT_FILTER_NAME" => "arrMenuElementFilter",
        'URL_SUFFIX' => $urlSuffix,
        ),
        false
    );

    $aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
}
