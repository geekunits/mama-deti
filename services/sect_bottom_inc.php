<?php

if (!function_exists('__ShowClinicFooterInfo')) {
function __ShowClinicFooterInfo()
{
    global $APPLICATION;

    $service = intval($APPLICATION->GetProperty('CURRENT_SERVICE',0));

    if ($service<=0)
        return '';

    $CITY_ID = GetCurrentCity();

    $arRegionClinic = CMamaDetiAPI::getClinicsID(array("PROPERTY_CITY"=>$CITY_ID));

    $GLOBALS["arrFilterDoctors"] = array("PROPERTY_SERVICES"=>$service,"PROPERTY_CLINIC"=>$arRegionClinic);

    ob_start();
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "home-doctors",
    Array(
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => "3",
        "NEWS_COUNT" => "20",
        "SORT_BY1" => "RAND",
        "SORT_ORDER1" => "ASC",
        "SORT_BY2" => "ID",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arrFilterDoctors",
        "FIELD_CODE" => array(),
        "PROPERTY_CODE" => array("POST", "SHOW_HOME"),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "60",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
    ),
false
);?>

<?php
    $html = ob_get_contents();
    ob_end_clean();

    return $html;

}
}

$APPLICATION->AddBufferContent('__ShowClinicFooterInfo');

?>
