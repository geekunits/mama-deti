<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

global $arrMenuSectionFilter;
$arrMenuSectionFilter = array("UF_SHOW_IN_MENU"=>1);
global $arrMenuElementFilter;
$arrMenuElementFilter = array("PROPERTY_SHOW_IN_MENU"=>1);

$CITY_ID = GetCurrentCity();
$arServicesID = CMamaDetiAPI::getClinicsServicesID(array("PROPERTY_CITY"=>$CITY_ID));
$arServiceSectionID = CMamaDetiAPI::getServicesIblockSection($arServicesID);
$arrMenuSectionFilter["ID"] = $arServiceSectionID;
$arrMenuElementFilter["=ID"] = $arServicesID;

if ($arServicesID) {
$aMenuLinksExt=$APPLICATION->IncludeComponent("mamadeti:menu.sections", "", array(
    "IS_SEF" => "Y",
    "SEF_BASE_URL" => "/services/",
    "SECTION_PAGE_URL" => "#SECTION_CODE#/",
    "DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#/",
    "IBLOCK_TYPE" => "content",
    "IBLOCK_ID" => "1",
    "DEPTH_LEVEL" => "2",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "SECTION_FILTER_NAME" => "arrMenuSectionFilter",
    "ELEMENT_FILTER_NAME" => "arrMenuElementFilter",
    ),
    false
);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);

$aMenuLinks[] =
    Array(
        "Программы",
        "/program/",
        Array(),
        Array("CLASS" => 'b-menu_sub__link b-menu_sub__link-border'),
        ""
    );
if (CMamaDetiAPI::hasActionsInRegion()) {
    $aMenuLinks[] =
        Array(
            "Акции",
            "/actions/",
            Array(),
            Array("CLASS" => 'b-menu_sub__link b-menu_sub__link-actions'),
            ""
        );
    }
}
