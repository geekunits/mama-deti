<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карьера в \"Мать и дитя\"");
?> Группа компаний &laquo;Мать и дитя&raquo; всегда открыта для сотрудничества. Если Вы обладаете высокой квалификацией, опытом эффективной работы, профессиональным перфекционизмом, коммуникативными навыками и хотите стать участником нашей команды - мы готовы рассмотреть Вашу кандидатуру. 
<div> 
  <br />
 </div>
 
<div>Кто мы &ndash; сотрудники «Мать и дитя»? Врачи и фельдшеры, санитары и медицинские сестры, специалисты не медицинского профиля, обеспечивающие функционирование компании. </div>
 
<div> 
  <br />
 </div>
 
<div>Все мы разные, и объединены не столько профессией, сколько общим призванием. Каждый, занимаясь своим делом, помогает сохранить то самое ценное, что есть у человека: здоровье и жизнь. Вместе мы справляемся и будем справляться, чего бы нам это ни стоило! </div>
 
<div> 
  <br />
 </div>
<?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"vacancies",
	Array(
		"IBLOCK_TYPE" => "contentsite",
		"IBLOCK_ID" => "37",
		"NEWS_COUNT" => "20",
		"USE_SEARCH" => "N",
		"USE_RSS" => "N",
		"USE_RATING" => "N",
		"USE_CATEGORIES" => "N",
		"USE_REVIEW" => "N",
		"USE_FILTER" => "Y",
		"FILTER_NAME" => "arrFilter",
		"FILTER_FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_PROPERTY_CODE" => array(0=>"",1=>"",),
		"SORT_BY1" => "timestamp_x",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"CHECK_DATES" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/about/vacancies-about-us/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"USE_PERMISSIONS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "300",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array(0=>"",1=>"",),
		"LIST_PROPERTY_CODE" => array("CLINIC",),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_NAME" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_FIELD_CODE" => array(0=>"NAME",1=>"DETAIL_TEXT",),
		"DETAIL_PROPERTY_CODE" => array("CLINIC",),
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => "mamadeti",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Отзывы",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"_REQUEST" => $_REQUEST,
		"_URL" => $_SERVER['REQUEST_URI'],
		"_CITY" => $GLOBALS['CURRENT_CITY'],
		"SEF_URL_TEMPLATES" => Array(
			"news" => "",
			"section" => "",
			"detail" => "vacancy/#ELEMENT_ID#/"
		),
		"VARIABLE_ALIASES" => Array(
			"news" => Array(),
			"section" => Array(),
			"detail" => Array(),
		)
	)
);?> <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>