<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Руководство");
?> 
<!--<tr><td colspan="2"> <strong><font color="#D73612" style="font-size: 18px;">Елена Младова &ndash; Генеральный директор</font></strong> </td></tr>
   
    <tr><td> <img id="bxid_883969" style="padding:0 0 15px 0;" title="Елена Младова &amp;mdash; генеральный директор" border="0" alt="Елена Младова &amp;mdash; генеральный директор" src="/upload/medialibrary/mladova2.jpg" vspace="10" width="209" height="260"  /> </td> 	<td valign="top" style="text-align: justify;"><blockquote style="margin: 0px 0px 0px 40px; border: none; padding: 0px;">Г-жа Елена Младова вошла в Совет директоров компании в августе 2012 г., ранее в том же году она была назначена генеральным директором. Она присоединилась к ГК &laquo;Мать и дитя&raquo; в 2008 г. в качестве руководителя отделения лечения бесплодия и ЭКО Перинатального Медицинского Центра, где отвечала за внедрение технологий лечения бесплодия и ЭКО. До прихода в Группу работала акушером-гинекологом в Центре планирования семьи и репродукции в Москве. 
          <br />
         Г-жа Младова окончила факультет фундаментальной медицины МГУ им М. В. Ломоносова.</blockquote></td></tr>-->
 
<table> 
  <tbody> 
    <tr><td colspan="2" style="border-image: initial;"> <strong><font color="#d73612" style="font-size: 18px;">Марк Курцер, д.м.н., Академик РАН </font></strong><strong style="color: rgb(238, 29, 36); font-size: large;">&mdash; </strong><strong style="font-size: 16px;"><font color="#d73612" style="font-size: 18px;">Генеральный директор, член Совета директоров</font></strong></td></tr>
   
    <tr> 	<td style="border-image: initial;"> <img style="padding:0 0 15px 0;" src="/upload/medialibrary/kurcer2.jpg" vspace="10" width="209" height="260"  /> </td> 	<td valign="top" style="text-align: justify; border-image: initial;"><blockquote style="margin: 0px 0px 0px 40px; padding: 0px; border: medium;">Марк Курцер является основателем ГК &laquo;Мать и Дитя&raquo;, Генеральным директором, членом Совета директоров.  Он начал свою карьеру на кафедре акушерства и гинекологии 2-го Московского государственного медицинского института им. Н. И. Пирогова, где прошел путь от ассистента до доцента кафедры. В период с 1994 по 2012 гг. Марк Курцер возглавлял «Центр планирования семьи и репродукции департамента здравоохранения города Москвы», крупнейшее акушерско-гинекологическое учреждение в Москве. В 2001 году Марк Курцер защитил докторскую диссертацию. 	В период с 2003 по 2013 Курцер являлся главным акушером-гинекологом Департамента 	здравоохранения города Москвы. Марк Курцер продолжает активно участвовать в деятельности ГК 	«Мать и дитя» и как руководитель, и как постоянно практикующий врач.</blockquote></td> 	</tr>
   
    <tr><td colspan="2" style="border-image: initial;"> 
        <div><font color="#ee1d24"><font size="5"><font size="4"><font color="#ee1d24"><strong>Наталья Якунина, к.м.н. — заместитель генерального директора по работе с пациентами</strong> </font></font></font></font></div>
       </td></tr>
   
    <tr><td style="border-image: initial;"> 
        <p align="left"><img width="209" height="245" style="margin-bottom: 10px;" src="http://www.mcclinics.ru/i/photo/team/yakunina.jpg" border="0"  /></p>
       
        <br />
       </td><td valign="top" style="text-align: justify; border-image: initial;"><blockquote style="margin: 0px 0px 0px 40px; padding: 0px; border: medium;"> 
          <p> </p>
         
          <table style="width: 100%; border-collapse: collapse;"> 
            <tbody> 
              <tr class="chet"><td class="first" style="vertical-align: top; border-image: initial;"></td><td style="vertical-align: top; border-image: initial;"> 
                  <div>Г-жа Наталья Якунина присоединилась к Группе в 2011 году. В 2016 г. была назначена зам. генерального директора ГК по работе с пациентами, с 2014 г. являлась главным врачом и генеральным директором клиники «Мать и дитя Савёловская», с 2012 г. была заведующим ВКДЦ в Перинатальном медицинском центре, с 2011 г. года являлась главным врачом клиники «Мать и дитя Юго-Запад». До прихода в Группу г-жа Якунина являлась главным акушером-гинекологом ЦАО г. Москвы.</div>
                 
                  <div>Опыт работы акушером-гинекологом составляет более 22 лет. </div>
                 
                  <div>Г-жа Якунина окончила ТГМУ, а также имеет степень кандидата медицинских наук и звание врача высшей категории.</div>
                 </td></tr>
             </tbody>
           </table>
         
          <p> 
            <br />
           </p>
         </blockquote> </td></tr>
   
    <tr><td colspan="2" style="border-image: initial;"> 
        <table> 
          <tbody> 
            <tr><td colspan="2" style="border-image: initial;"><strong><font color="#d73612" style="font-size: 18px;">Юлия Кутакова, </font></strong><strong style="color: rgb(238, 29, 36); font-size: large;">к.м.н. </strong><strong style="font-size: 16px;"><font color="#d73612" style="font-size: 18px;">— главный врач</font></strong></td></tr>
           
            <tr><td style="border-image: initial;"><img src="/kutakova-9.jpg" border="0" width="209" height="245" vspace="10"  /></td><td valign="top" style="text-align: justify; border-image: initial;"><blockquote style="margin: 0px 0px 0px 40px; padding: 0px; border: medium;">Г-жа Юлия Кутакова присоединилась к Группе в 2012 г. Ее практический опыт в сфере акушерства и гинекологии насчитывает более 11 лет. До прихода в ГК «Мать и дитя» г-жа Кутакова занимала должность начальника организационно-методического отдела по акушерству и гинекологии Департамента здравоохранения г. Москвы. Она имеет академическую степень по медицине Российского национального исследовательского медицинского университета им. Н.И.Пирогова, степень по менеджменту Московского института управления и является кандидатом медицинских наук.</blockquote></td></tr>
           </tbody>
         </table>
       </td></tr>
   
    <tr><td colspan="2" style="border-image: initial;"> 
        <table> 
          <tbody> 
            <tr><td colspan="2" style="border-image: initial;"><strong><font color="#d73612" style="font-size: 18px;">Андрей Хоперский — заместитель генерального диретора по финансам и экономике</font></strong></td></tr>
           
            <tr><td style="border-image: initial;"><img src="/upload/medialibrary/354/3546f8eec6ce21c3bfdea96852103040.jpg" border="0" width="209" height="245" title="hoperskiy.jpg" alt="hoperskiy.jpg"  /></td><td valign="top" style="text-align: justify; border-image: initial;"><blockquote style="margin: 0px 0px 0px 40px; padding: 0px; border: medium;"> 
                  <p>Г-н Андрей Хоперский присоединился к Группе в качестве руководителя отдела финансового контроля и казначейства в 2013 году, в 2016 году назначен Директором по финансам ГК «Мать и дитя». До прихода в Группу Андрей работал в компаниях ГК «Русагро» и АХК «Сухой» в качестве финансового менеджера, до этого работал аудитором в компании BDO Russia.  
                    <br />
                   Г-н Хоперский окончил Московский Государственный Университет Экономики Статистики и Информатики по специальности «Налоги и налогообложение». Обладает дипломами АССА Advanced Diploma in Accounting and Business и DipIFR Russian.</p>
                 </blockquote></td></tr>
           </tbody>
         </table>
       
        <table> 
          <tbody> 
            <tr><td colspan="2" style="border-image: initial;"><b><font color="#d73612" style="font-size: 18px;">Вадим Сигутин </font></b><strong><font color="#d73612" style="font-size: 18px;">— заместитель генерального директора по операционной деятельности, директор региона </font></strong><b><font color="#d73612" style="font-size: 18px;">«Мать и дитя</font></b><b style="line-height: 20px; font-size: 14px;"><font color="#d73612" style="font-size: 18px;"> Центр»</font></b></td></tr>
           
            <tr><td style="border-image: initial;"><img src="/vadim_sigutin.jpg" border="0" width="209" height="245" vspace="10"  /></td><td valign="top" style="text-align: justify; border-image: initial;"><blockquote style="margin: 0px 0px 0px 40px; padding: 0px; border: medium; text-align: left;"> 
                  <p> 
                    <br />
                   Г-н Вадим Сигутин присоединился к Группе в качестве заместителя генерального директора по операционной деятельности в апреле 2014 года. В 2016 г. назначен на должность директора региона «Мать и дитя Центр». Общий опыт работы г-на Сигутина в области медицины превышает 10 лет. До прихода в Группу он занимал должность операционного директора в сети поликлиник «Семейный доктор» на территории ЮЗАО и ЗАО районах Москвы. Г-н Сигутин окончил Смоленскую Государственную Медицинскую Академию по специальности «Педиатрия».</p>
                 </blockquote></td></tr>
           </tbody>
         </table>
       <b><font color="#d73612" style="font-size: 18px;">Александр Райт — директор региона «Мать и дитя Сибирь» </font></b></td></tr>
   
    <tr><td style="border-image: initial;"><img src="/alexander_rait.jpg" border="0" width="209" height="245" vspace="10"  /></td><td valign="top" style="text-align: justify; border-image: initial;"> <blockquote style="margin: 0px 0px 0px 40px; padding: 0px; border: medium;"> Г-н Александр Райт присоединился к Группе в 2012 г. В 2016 г. назначен на должность директора региона «Мать и дитя Сибирь»; с 2014 г. занимал должность директора финансового департамента; с 2012 г. был руководителем отдела МСФО. 
          <br />
         До прихода в Компанию г-н Райт занимал должность заместителя начальника отдела МСФО в АО «Вертолеты России», а также работал в департаменте аудита в АО «БДО Юникон». 
          <br />
         Г-н Райт окончил факультет финансы и кредит Экономической академии города Кишинева.          </blockquote></td></tr>
   
    <tr><td colspan="2" style="border-image: initial;"> 
        <table> 
          <tbody> 
            <tr><td colspan="2" style="border-image: initial;"><strong><font color="#d73612" style="font-size: 18px;">Алсу Назырова, </font></strong><strong style="color: rgb(238, 29, 36); font-size: large;">к.м.н.</strong><strong style="font-size: 16px;"><font color="#d73612" style="font-size: 18px;">— </font></strong><b style="font-size: 16px;"><font color="#d73612" style="font-size: 18px;">директор региона «Мать и дитя Урал»</font></b></td></tr>
           
            <tr><td style="border-image: initial;"><img src="/алсу.jpg" border="0" width="209" height="245"  /></td><td valign="top" style="text-align: justify; border-image: initial;"><blockquote style="margin: 0px 0px 0px 40px; padding: 0px; border: medium;"> 
                  <p>Алсу Назырова присоединилась к Группе в 2009 году. В 2016 г. была назначена на должность директора региона Урал, а также на должность руководителя департамента региональных проектов Группы компаний; с 2014 г. является генеральным директором клинического госпиталя в Уфе; с 2009 г. является генеральным директором клиники «Мать и дитя» в Уфе. </p>
                 
                  <p><span style="font-size: 16px;">Г-жа Назырова обладает более чем 15-летним опытом работы в медицине и фармацевтической отрасли и является заведующей кафедры репродуктивного здоровья человека Башкирского государственного медицинского университета. </span></p>
                 
                  <p><span style="font-size: 16px;">Алсу Назырова закончила Башкирский государственный медицинский университет по специальности «Педиатрия», а также имеет степень кандидата медицинских наук.</span></p>
                 </blockquote></td></tr>
           </tbody>
         </table>
       </td></tr>
   
    <tr><td colspan="2" style="border-image: initial;">
        <br />
      
        <br />
       </td></tr>
   
    <tr><td colspan="2" style="border-image: initial;"> 
        <br />
       </td></tr>
   </tbody>
 </table>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>