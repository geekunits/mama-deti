<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Обратная связь");
?>
    <div class="bl-feedback">
        <div class="container layout-1 fl-l js-ajax-form-container" data-ajax-source="/ajax/feedback.php">
            Загрузка...
        </div>
        <div class="test layout">
            <?$APPLICATION->IncludeComponent("bitrix:voting.form", "questions", array(
                "VOTE_ID" => "2",
                "VOTE_RESULT_TEMPLATE" => "?VOTE_ID=#VOTE_ID#",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600"
            ),
                false
            );?>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
