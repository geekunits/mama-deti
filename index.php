<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Сеть госпиталей группы компаний Мать и Дитя предоставляет комплекс медицинских услуг по направлениям акушерства и гинекологии, урологии, ЭКО и других направлений медицины");
$APPLICATION->SetPageProperty("title", "Сеть медицинских центров Мать и дитя");

$APPLICATION->SetTitle("Мать и дитя");
CModule::IncludeModule("iblock");

$city = CityManager::getInstance()->getCityById(GetCurrentCity());

global $arClinicFilter;

$arClinicFilter = ['PROPERTY_CITY' => $city['ID']];

$cityFileName = 'index_'.$city['CODE'].'.php';
$file = file_exists(__DIR__.'/'.$cityFileName) ? $cityFileName : 'index_default.php';

$APPLICATION->IncludeFile('/'.$file);

require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/footer.php");
